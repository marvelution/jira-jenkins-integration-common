/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.model.parsers;

import java.util.*;

import org.marvelution.jji.model.*;

import jakarta.json.*;

import static java.util.Collections.*;

/**
 * {@link MergingParser} for {@link DeploymentEnvironment} data.
 *
 * @author Mark Rekveld
 * @since 1.14.0
 */
class BuildDeploymentEnvironmentParser
		extends BuildActionParser
{

	private static final String ENVIRONMENT = "environment";
	private static final String DEPLOYMENT_ENVIRONMENT_ACTION = "org.marvelution.jji.export.DeploymentEnvironmentAction";

	public BuildDeploymentEnvironmentParser()
	{
		super(DEPLOYMENT_ENVIRONMENT_ACTION, ENVIRONMENT);
	}

	@Override
	protected void parse(
			JsonObject json,
			Build target)
	{
		JsonObject object = json.asJsonObject().getJsonObject(ENVIRONMENT);
		String id = object.getString("id");
		String name = object.getString("name");
		DeploymentEnvironmentType type = DeploymentEnvironmentType.fromString(object.getString("type"));
		target.getDeploymentEnvironments().add(new DeploymentEnvironment().setId(id).setName(name).setType(type));
	}

	@Override
	public List<String> fields()
	{
		return singletonList(ENVIRONMENT + "[*]");
	}
}
