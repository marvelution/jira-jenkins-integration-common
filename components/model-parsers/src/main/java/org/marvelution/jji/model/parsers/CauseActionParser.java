/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.model.parsers;

import java.util.*;

import org.marvelution.jji.model.*;

import jakarta.json.*;

import static org.marvelution.jji.model.parsers.ParserUtils.*;

import static java.util.Collections.*;

/**
 * {@link MergingParser} specific for {@link Build} causes.
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
class CauseActionParser
		extends BuildActionParser
{

	private static final String CAUSES = "causes";
	private static final String SHORT_DESCRIPTION = "shortDescription";

	CauseActionParser()
	{
		super(null, CAUSES);
	}

	@Override
	protected void parse(
			JsonObject json,
			Build build)
	{
		json.getJsonArray(CAUSES)
				.stream()
				.filter(ParserUtils::isJsonObject)
				.map(JsonValue::asJsonObject)
				.map(cause -> optString(cause, SHORT_DESCRIPTION))
				.filter(Optional::isPresent)
				.map(Optional::get)
				// Currently we only support a single build cause, so only map the first found.
				.findFirst()
				.ifPresent(build::setCause);
	}

	@Override
	public List<String> fields()
	{
		return singletonList(CAUSES + "[" + SHORT_DESCRIPTION + "]");
	}
}
