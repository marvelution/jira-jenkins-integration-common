/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.model.parsers;

import org.marvelution.jji.model.*;

/**
 * Entry-point for the model parser API.
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class ParserProvider
{

	public static MergingParser<JobsContainer> siteParser()
	{
		return new JobsContainerParser<>();
	}

	public static MergingParser<Job> jobParser()
	{
		return new BasicJobParser().andThen(new JobsContainerParser<>());
	}

	public static MergingParser<Build> buildParser()
	{
		return new BasicBuildParser().andThen(new BuildChangeSetParser())
				.andThen(new BuildActionsParser(new CauseActionParser().andThen(new TestResultsActionParser())
						                                .andThen(new BuildParentParser())
						                                .andThen(new BuildDeploymentEnvironmentParser())
						                                .andThen(new BuildScmRevisionParser())));
	}

	/**
	 * @since 1.9.0
	 */
	public static MergingParser<Build> buildParentParser()
	{
		return new BuildActionsParser(new BuildParentParser());
	}
}
