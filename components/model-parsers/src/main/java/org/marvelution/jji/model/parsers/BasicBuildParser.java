/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.model.parsers;

import java.util.*;

import org.marvelution.jji.model.*;

import jakarta.json.*;

import static org.marvelution.jji.model.parsers.ParserUtils.*;

import static java.lang.String.*;
import static java.util.Arrays.*;

/**
 * {@link MergingParser} for parsing the basic {@link Build} details.
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
class BasicBuildParser
		implements MergingParser<Build>
{

	public static final String NUMBER = "number";
	public static final String DISPLAY_NAME = "displayName";
	public static final String FULL_DISPLAY_NAME = "fullDisplayName";
	public static final String DESCRIPTION = "description";
	public static final String BUILT_ON = "builtOn";
	public static final String RESULT = "result";
	public static final String BUILDING = "building";
	public static final String TIMESTAMP = "timestamp";
	public static final String DURATION = "duration";

	@Override
	public boolean parse(
			JsonValue json,
			Build build)
	{
		if (isJsonObject(json))
		{
			JsonObject object = json.asJsonObject();
			optInt(object, NUMBER).ifPresent(build::setNumber);
			Optional<String> displayName = optString(object, DISPLAY_NAME).filter(name -> !name.equals("#" + build.getNumber()));
			if (!displayName.isPresent() && build.getJob() != null)
			{
				displayName = optString(object, FULL_DISPLAY_NAME).filter(
						name -> !name.endsWith(format("%s #%d", build.getJob().getName(), build.getNumber())));
			}
			displayName.ifPresent(build::setDisplayName);
			optString(object, DESCRIPTION).ifPresent(build::setDescription);
			optString(object, BUILT_ON).ifPresent(build::setBuiltOn);
			optString(object, RESULT).map(Result::fromString).ifPresent(build::setResult);
			optBoolean(object, BUILDING).ifPresent(build::setBuilding);
			optLong(object, TIMESTAMP).ifPresent(build::setTimestamp);
			optLong(object, DURATION).ifPresent(build::setDuration);
			return true;
		}
		else
		{
			return false;
		}
	}

	@Override
	public List<String> fields()
	{
		return asList(NUMBER, DISPLAY_NAME, FULL_DISPLAY_NAME, DESCRIPTION, BUILT_ON, RESULT, BUILDING, TIMESTAMP, DURATION);
	}
}
