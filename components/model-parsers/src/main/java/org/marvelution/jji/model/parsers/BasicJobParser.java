/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.model.parsers;

import java.util.*;
import javax.annotation.*;

import org.marvelution.jji.model.*;

import jakarta.json.*;
import org.apache.commons.lang3.*;

import static org.marvelution.jji.model.parsers.ParserUtils.stream;
import static org.marvelution.jji.model.parsers.ParserUtils.*;

import static java.util.Arrays.*;
import static java.util.Optional.*;
import static java.util.stream.Collectors.*;
import static org.apache.commons.lang3.StringUtils.*;

/**
 * {@link MergingParser} implementation to parse the basic {@link Job} details.
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
class BasicJobParser
		implements MergingParser<Job>
{

	public static final String NAME = "name";
	public static final String FULL_NAME = "fullName";
	public static final String URL = "url";
	public static final String DISPLAY_NAME = "displayName";
	public static final String DESCRIPTION = "description";
	public static final String FIRST_BUILD = "firstBuild";
	public static final String LAST_BUILD = "lastBuild";
	public static final String BUILDS = "builds";
	private final BasicBuildParser buildParser = new BasicBuildParser();

	@Override
	public boolean parse(
			JsonValue json,
			Job job)
	{
		if (isJsonObject(json))
		{
			if (job.shouldBeLinked())
			{
				job.setLinked(true);
			}
			JsonObject object = json.asJsonObject();
			optString(object, NAME).ifPresent(job::setName);
			optString(object, FULL_NAME).filter(name -> name.contains("/"))
					.map(name -> name.substring(0, name.lastIndexOf("/")))
					.ifPresent(job::setParentName);
			optString(object, URL).map(url -> stripEnd(url, "/"))
					.map(url -> url.substring(url.indexOf("/job/") + 5))
					.filter(url -> !Objects.equals(job.getName(), url))
					.ifPresent(job::setUrlName);
			optString(object, DISPLAY_NAME).filter(StringUtils::isNotBlank).ifPresent(job::setDisplayName);
			optString(object, DESCRIPTION).filter(StringUtils::isNotBlank).ifPresent(job::setDescription);
			ofNullable(object.get(FIRST_BUILD)).map(element -> parseBuild(element, job))
					.map(Build::getNumber)
					.ifPresent(job::setOldestBuild);
			ofNullable(object.get(LAST_BUILD)).map(element -> parseBuild(element, job)).map(Build::getNumber).ifPresent(job::setLastBuild);
			job.setBuilds(stream(object.getJsonArray(BUILDS)).map(element -> parseBuild(element, job))
					              .filter(Objects::nonNull)
					              .collect(toList()));
			return true;
		}
		else
		{
			return false;
		}
	}

	@Override
	public List<String> fields()
	{
		return asList(NAME, FULL_NAME, URL, DISPLAY_NAME, DESCRIPTION, FIRST_BUILD + "[" + BasicBuildParser.NUMBER + "]",
		              LAST_BUILD + "[" + BasicBuildParser.NUMBER + "]",
		              BUILDS + "[" + String.join(",", BasicBuildParser.NUMBER, BasicBuildParser.RESULT) + "]");
	}

	@Nullable
	private Build parseBuild(
			JsonValue element,
			Job job)
	{
		Build build = new Build().setJob(job);
		if (buildParser.parse(element, build))
		{
			return build;
		}
		else
		{
			return null;
		}
	}
}
