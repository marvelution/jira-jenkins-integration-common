/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.model.parsers;

import java.util.*;

import org.marvelution.jji.model.*;

import jakarta.json.*;

import static org.marvelution.jji.model.parsers.ParserUtils.*;

class BuildScmRevisionParser
		extends BuildActionParser
{

	private static final String REVISION = "revision";

	protected BuildScmRevisionParser()
	{
		super("jenkins.scm.api.SCMRevisionAction", REVISION);
	}

	@Override
	protected void parse(
			JsonObject json,
			Build target)
	{
		JsonObject revision = json.asJsonObject().getJsonObject(REVISION);
		JsonObject head = revision.getJsonObject("head");
		Optional.ofNullable(head).flatMap(h -> optString(h, "name")).ifPresent(target.getBranches()::add);
	}

	@Override
	protected boolean handleMisMatch(
			JsonValue json,
			Build target)
	{
		return new BuildGitBranchesParser().parse(json, target);
	}

	@Override
	public List<String> fields()
	{
		return Collections.singletonList(REVISION + "[*,head[*]]");
	}
}
