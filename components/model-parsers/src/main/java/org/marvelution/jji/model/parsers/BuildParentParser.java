/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.model.parsers;

import java.util.*;

import org.marvelution.jji.model.*;

import jakarta.json.*;

import static java.util.Collections.*;

/**
 * {@link MergingParser} specific for {@link Build} parent {@link Job}.
 *
 * @author Mark Rekveld
 * @since 1.9.0
 */
class BuildParentParser
		extends BuildActionParser
{

	private static final String PARENT = "parent";
	private static final String PARENT_ACTION = "org.marvelution.jji.export.ParentAction";
	private final BasicJobParser jobParser = new BasicJobParser();

	public BuildParentParser()
	{
		super(PARENT_ACTION, PARENT);
	}

	@Override
	protected void parse(
			JsonObject json,
			Build target)
	{
		if (target.getJob() == null)
		{
			Job job = new Job();
			if (jobParser.parse(json.get(PARENT), job))
			{
				target.setJob(job);
			}
		}
	}

	@Override
	public List<String> fields()
	{
		return singletonList(PARENT + "[*]");
	}
}
