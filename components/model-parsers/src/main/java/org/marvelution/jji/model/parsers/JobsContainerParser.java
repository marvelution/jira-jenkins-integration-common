/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.model.parsers;

import java.util.*;

import org.marvelution.jji.model.*;

import jakarta.json.*;

import static org.marvelution.jji.model.parsers.BasicJobParser.*;
import static org.marvelution.jji.model.parsers.ParserUtils.*;

import static java.util.Collections.*;

/**
 * @author Mark Rekveld
 * @since 1.9.0
 */
class JobsContainerParser<C extends JobsContainer>
		implements MergingParser<C>
{

	private static final String JOBS = "jobs";
	private static final String[] FIELDS = { NAME, FULL_NAME, URL };
	private final BasicJobParser jobParser = new BasicJobParser();

	@Override
	public boolean parse(
			JsonValue json,
			C target)
	{
		if (isJsonObject(json))
		{
			JsonObject object = json.asJsonObject();
			Optional<JsonArray> jobs = optMember(JOBS, object::getJsonArray);
			if (jobs.isPresent())
			{
				jobs.get().stream().map(element -> {
					Job job = new Job().setSite(target.getSite());
					if (jobParser.parse(element, job))
					{
						return job;
					}
					else
					{
						return null;
					}
				}).filter(Objects::nonNull).forEach(target::addJob);
				return true;
			}
		}

		return false;
	}

	@Override
	public List<String> fields()
	{
		return singletonList(JOBS + "[" + String.join(",", FIELDS) + "]");
	}
}
