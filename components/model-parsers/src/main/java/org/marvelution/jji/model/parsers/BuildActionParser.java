/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.model.parsers;

import javax.annotation.*;

import org.marvelution.jji.model.*;

import jakarta.json.*;

import static org.marvelution.jji.model.parsers.ParserUtils.*;

/**
 * @author Mark Rekveld
 * @since 1.16
 */
abstract class BuildActionParser
		implements MergingParser<Build>
{

	protected final String[] requiredFields;
	protected final String requireClass;

	protected BuildActionParser(
			@Nullable String requireClass,
			@Nullable String... requiredFields)
	{
		this.requireClass = requireClass;
		this.requiredFields = requiredFields;
	}

	@Override
	public boolean parse(
			JsonValue json,
			Build target)
	{
		if (isJsonObject(json) && shouldParse(json.asJsonObject()))
		{
			parse(json.asJsonObject(), target);
			return true;
		}
		else
		{
			return handleMisMatch(json, target);
		}
	}

	protected abstract void parse(
			JsonObject json,
			Build target);

	protected boolean shouldParse(JsonObject json)
	{
		if (requiredFields != null)
		{
			for (String field : requiredFields)
			{
				if (!json.containsKey(field))
				{
					return false;
				}
			}
		}
		return requireClass == null || optString(json, "_class").filter(requireClass::equals).isPresent();
	}

	protected boolean handleMisMatch(
			JsonValue json,
			Build target)
	{
		return false;
	}
}
