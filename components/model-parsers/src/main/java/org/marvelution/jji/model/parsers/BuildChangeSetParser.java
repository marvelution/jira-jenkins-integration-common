/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.model.parsers;

import java.util.*;
import java.util.stream.*;

import org.marvelution.jji.model.*;

import jakarta.json.*;

import static org.marvelution.jji.model.parsers.ParserUtils.*;

import static java.util.stream.Collectors.*;
import static java.util.stream.StreamSupport.stream;
import static org.apache.commons.lang3.StringUtils.*;

/**
 * {@link MergingParser} the parses the {@link ChangeSet} of a {@link Build}.
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
class BuildChangeSetParser
		implements MergingParser<Build>
{

	private static final String[] COMMIT_ID_FIELDS = new String[] { "id", // GIT
			"node", // Mercurial
			"revision", // Subversion
			"changeItem", // Perforce
			"commitId" // Default
	};
	private static final String[] COMMIT_MESSAGE_FIELDS = new String[] { "comment", "msg" // Default
	};
	private static final String CHANGE_SETS = "changeSets";
	private static final String CHANGE_SET = "changeSet";
	private static final String ITEMS = "items";

	@Override
	public boolean parse(
			JsonValue json,
			Build build)
	{
		if (isJsonObject(json))
		{
			JsonObject object = json.asJsonObject();

			Optional<JsonArray> changeSets = optMember(CHANGE_SETS, object::getJsonArray);
			if (changeSets.isPresent())
			{
				build.setChangeSet(changeSets.get().stream().map(this::parseChangeSet).flatMap(List::stream).collect(Collectors.toList()));
				return true;
			}
			else if (object.containsKey(CHANGE_SET))
			{
				build.setChangeSet(parseChangeSet(object.get(CHANGE_SET)));
				return true;
			}
		}
		return false;
	}

	@Override
	public List<String> fields()
	{
		return Stream.of(CHANGE_SET, CHANGE_SETS).map(field -> field + "[*," + ITEMS + "[*]]").collect(toList());
	}

	private List<ChangeSet> parseChangeSet(JsonValue json)
	{
		JsonArray changes = null;
		if (isJsonObject(json) && json.asJsonObject().containsKey(ITEMS))
		{
			changes = json.asJsonObject().getJsonArray(ITEMS);
		}
		else if (isJsonArray(json))
		{
			changes = json.asJsonArray();
		}
		if (changes != null && changes.size() > 0)
		{
			return stream(changes.spliterator(), false).filter(ParserUtils::isJsonObject).map(JsonValue::asJsonObject).map(object -> {
				Optional<String> commitId = optString(object, COMMIT_ID_FIELDS);
				return optString(object, COMMIT_MESSAGE_FIELDS).map(s -> stripEnd(s, "\n"))
						.map(s -> new ChangeSet().setCommitId(commitId.orElse(null)).setMessage(s))
						.orElse(null);
			}).filter(Objects::nonNull).collect(toList());
		}
		else
		{
			return new ArrayList<>();
		}
	}
}
