/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.model.parsers;

import java.util.*;

import jakarta.json.*;

import static java.util.Collections.*;

/**
 * @author Mark Rekveld
 * @since 1.0.0
 */
public interface MergingParser<T>
{

	boolean parse(
			JsonValue json,
			T target);

	/**
	 * @since 1.8.0
	 */
	default List<String> fields()
	{
		return emptyList();
	}

	default MergingParser<T> andThen(MergingParser<T> after)
	{
		return new MergingParser<T>()
		{
			@Override
			public boolean parse(
					JsonValue json,
					T target)
			{
				boolean result = MergingParser.this.parse(json, target);
				boolean afterResult = after.parse(json, target);
				return result || afterResult;
			}

			@Override
			public List<String> fields()
			{
				List<String> fields = new ArrayList<>(MergingParser.this.fields());
				fields.addAll(after.fields());
				return fields;
			}
		};
	}
}
