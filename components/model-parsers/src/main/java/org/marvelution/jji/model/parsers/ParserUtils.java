/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.model.parsers;

import java.util.*;
import java.util.function.*;
import java.util.stream.*;

import jakarta.json.*;

/**
 * Utilities for the model Parsers.
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
class ParserUtils
{
	static boolean isJsonObject(JsonValue json)
	{
		return json.getValueType() == JsonValue.ValueType.OBJECT;
	}

	static boolean isJsonArray(JsonValue json)
	{
		return json.getValueType() == JsonValue.ValueType.ARRAY;
	}

	static Stream<JsonValue> stream(JsonArray json)
	{
		return json != null ? json.stream() : Stream.empty();
	}

	static Optional<String> optString(
			JsonObject json,
			String member)
	{
		return optMember(member, json::getString);
	}

	static Optional<String> optString(
			JsonObject json,
			String... members)
	{
		return Arrays.stream(members).map(member -> optString(json, member)).filter(Optional::isPresent).map(Optional::get).findFirst();
	}

	static Optional<Long> optLong(
			JsonObject json,
			String member)
	{
		return optMember(member, json::getJsonNumber).map(JsonNumber::longValue);
	}

	static Optional<Integer> optInt(
			JsonObject json,
			String member)
	{
		return optMember(member, json::getInt);
	}

	static Optional<Boolean> optBoolean(
			JsonObject json,
			String member)
	{
		return optMember(member, json::getBoolean);
	}

	static <T> Optional<T> optMember(
			String member,
			Function<String, T> accessor)
	{
		try
		{
			return Optional.of(accessor.apply(member));
		}
		catch (Exception e)
		{
			return Optional.empty();
		}
	}
}
