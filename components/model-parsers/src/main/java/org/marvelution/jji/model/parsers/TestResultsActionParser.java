/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.model.parsers;

import java.util.*;

import org.marvelution.jji.model.*;

import jakarta.json.*;

import static org.marvelution.jji.model.parsers.ParserUtils.*;

/**
 * {@link MergingParser} specific for the {@link Build} {@link TestResults}.
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
class TestResultsActionParser
		extends BuildActionParser
{

	public TestResultsActionParser()
	{
		super(null, "urlName");
	}

	@Override
	protected void parse(
			JsonObject json,
			Build build)
	{
		build.setTestResults(new TestResults().setFailed(optInt(json, "failCount").orElse(0))
				                     .setSkipped(optInt(json, "skipCount").orElse(0))
				                     .setTotal(optInt(json, "totalCount").orElse(0)));
	}

	@Override
	protected boolean shouldParse(JsonObject json)
	{
		return isJsonObject(json) && optString(json.asJsonObject(), "urlName").filter(url -> Objects.equals("testReport", url)).isPresent();
	}
}
