/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.model.parsers;

import java.util.*;

import org.marvelution.jji.model.*;

import jakarta.json.*;

import static org.marvelution.jji.model.parsers.ParserUtils.*;

import static java.util.Collections.*;
import static java.util.stream.Collectors.*;

/**
 * {@link MergingParser} for parsing the {@link Build} action details.
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
class BuildActionsParser
		implements MergingParser<Build>
{

	private static final String ACTIONS = "actions";
	private final MergingParser<Build> actionParserChain;

	BuildActionsParser(MergingParser<Build> actionParserChain)
	{
		this.actionParserChain = actionParserChain;
	}

	@Override
	public boolean parse(
			JsonValue json,
			Build build)
	{
		if (isJsonObject(json))
		{
			JsonObject object = json.asJsonObject();
			Optional<JsonArray> actions = optMember(ACTIONS, object::getJsonArray);
			if (actions.isPresent())
			{
				actions.get().forEach(action -> actionParserChain.parse(action, build));
				return true;
			}
		}
		return false;
	}

	@Override
	public List<String> fields()
	{
		return singletonList(ACTIONS + actionParserChain.fields().stream().collect(joining(",", "[*,", "]")));
	}

}
