/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.model.parsers;

import org.marvelution.jji.model.*;

import org.junit.jupiter.api.*;

import static org.assertj.core.api.Assertions.*;

/**
 * Tests for {@link BuildParentParser}.
 *
 * @author Mark Rekveld
 * @since 1.15.0
 */
public class BuildParentParserTest
		extends AbstractParserTest
{
	private MergingParser<Build> parser;

	@BeforeEach
	void setUp()
	{
		parser = new BuildActionsParser(new BuildParentParser());
	}

	@Test
	void testParse()
	{
		Build build = new Build();
		parser.parse(loadJson("build-with-markers.json"), build);

		assertThat(build.getJob()).isEqualTo(new Job().setName("pipeline-test"));
	}

	@Test
	void testParse_JobAlreadySet()
	{
		Job job = new Job().setName("job 1");
		Build build = new Build().setJob(job);
		parser.parse(loadJson("build-with-markers.json"), build);

		assertThat(build.getJob()).isSameAs(job);
	}

	@Test
	void testParse_ClassMismatch()
	{
		Build build = new Build();
		parser.parse(loadJson("build-with-non-markers.json"), build);

		assertThat(build.getDeploymentEnvironments()).isEmpty();
	}
}
