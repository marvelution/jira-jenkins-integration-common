/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.model.parsers;

import org.marvelution.jji.model.*;

import org.junit.jupiter.api.*;

import static org.marvelution.jji.model.Matchers.equalTo;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

/**
 * Tests for the {@link BasicJobParser}.
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
class BasicJobParserTest
		extends AbstractParserTest
{

	private BasicJobParser parser;

	@BeforeEach
	void setUp()
	{
		parser = new BasicJobParser();
	}

	@Test
	void testParse()
	{
		Site site = new Site().setId("1").setAutoLinkNewJobs(true);
		Job job = new Job().setSite(site);
		parser.parse(loadJson("job.json"), job);

		assertThat(job, equalTo(new Job().setSite(site)
				                        .setLinked(true)
				                        .setName("jira-jenkins-integration")
				                        .setLastBuild(2144)
				                        .setOldestBuild(1623)
				                        .setDeleted(false)));
		assertThat(job.getBuilds(), hasSize(18));
		assertThat(job.getJobs(), empty());
	}

	@Test
	void testParse_SubJob()
	{
		Site site = new Site().setId("1").setAutoLinkNewJobs(true);
		Job job = new Job().setSite(site);
		parser.parse(loadJson("subjob.json"), job);

		assertThat(job, equalTo(new Job().setSite(site)
				                        .setLinked(true)
				                        .setName("jira-jenkins-integration")
				                        .setParentName("OSS")
				                        .setUrlName("OSS/job/jira-jenkins-integration")
				                        .setLastBuild(2144)
				                        .setOldestBuild(1623)
				                        .setDeleted(false)));
		assertThat(job.getBuilds(), hasSize(18));
		assertThat(job.getJobs(), empty());
	}

	@Test
	void testParse_AutoEnableOnlyNewJobs()
	{
		Site site = new Site().setId("1").setAutoLinkNewJobs(true);
		Job job = new Job().setSite(site).setId("test-id");
		parser.parse(loadJson("job.json"), job);

		assertThat(job, equalTo(new Job().setSite(site)
				                        .setId("test-id")
				                        .setLinked(false)
				                        .setName("jira-jenkins-integration")
				                        .setLastBuild(2144)
				                        .setOldestBuild(1623)
				                        .setDeleted(false)));
		assertThat(job.getBuilds(), hasSize(18));
		assertThat(job.getJobs(), empty());
	}
}
