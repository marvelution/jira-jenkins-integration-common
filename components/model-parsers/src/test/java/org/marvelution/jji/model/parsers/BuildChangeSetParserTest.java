/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.model.parsers;

import org.marvelution.jji.model.Build;

import org.junit.jupiter.api.Test;

import static org.marvelution.jji.model.ChangeSet.UNKNOWN_COMMIT_ID;
import static org.marvelution.jji.model.Matchers.changeSetEqualTo;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;

/**
 * Tests for {@link BuildChangeSetParser}.
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
class BuildChangeSetParserTest extends AbstractParserTest {

	@Test
	void testParse() {
		testParse("build.json");
	}

	@Test
	void testParse_WithChangeSets() {
		testParse("build-changesets.json");
	}

	@Test
	void testParse_WithChangeSetAsArray() {
		testParse("build-changeset-as-array.json");
	}

	private void testParse(String json) {
		Build build = new Build();
		new BuildChangeSetParser().parse(loadJson(json), build);
		assertThat(build.getChangeSet(), hasSize(2));
		assertThat(build.getChangeSet(), hasItem(
				changeSetEqualTo("9a0d506fa8982e41ed3340caeed4a605587b6aa2", "Dummy commit message for testing")));
		assertThat(build.getChangeSet(), hasItem(
				changeSetEqualTo("9a0d506fa8982e41ed3340caeed4a605587b6aa3", "Commit message for testing")));
	}

	@Test
	void testParse_Svn() {
		Build build = new Build();
		new BuildChangeSetParser().parse(loadJson("build-svn.json"), build);
		assertThat(build.getChangeSet(), hasSize(3));
		assertThat(build.getChangeSet(), hasItem(
				changeSetEqualTo("438948", "TEST00000: Jira Jenkins Integration TEST-3")));
		assertThat(build.getChangeSet(), hasItem(
				changeSetEqualTo("438707", "TEST00000: Jira Jenkins Integration TEST-3")));
		assertThat(build.getChangeSet(), hasItem(
				changeSetEqualTo("436047", "TEST00000: Vault Configuration test +createjob")));
	}

	@Test
	void testParse_WithEmptyCommitIds() {
		Build build = new Build();
		new BuildChangeSetParser().parse(loadJson("build-empty-commitIds.json"), build);
		assertThat(build.getChangeSet(), hasSize(2));
		assertThat(build.getChangeSet(), hasItem(
				changeSetEqualTo(UNKNOWN_COMMIT_ID, "Dummy commit message for testing")));
		assertThat(build.getChangeSet(), hasItem(
				changeSetEqualTo(UNKNOWN_COMMIT_ID, "Commit message for testing")));
	}

	@Test
	void testParse_NoChangeSet() {
		Build build = new Build();
		new BuildChangeSetParser().parse(loadJson("build-no-changeset.json"), build);
		assertThat(build.getChangeSet(), hasSize(0));
	}

	@Test
	void testParse_NoChangeSetItems() {
		Build build = new Build();
		new BuildChangeSetParser().parse(loadJson("build-no-changeset-items.json"), build);
		assertThat(build.getChangeSet(), hasSize(0));
	}
}
