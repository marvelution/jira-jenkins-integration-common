/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.model.parsers;

import org.marvelution.jji.model.Build;
import org.marvelution.jji.model.Job;

import org.junit.jupiter.api.*;

import static org.marvelution.jji.model.Matchers.equalTo;
import static org.marvelution.jji.model.Result.FAILURE;

import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Tests for the {@link BasicBuildParser}.
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
class BasicBuildParserTest extends AbstractParserTest {

	private BasicBuildParser parser;

	@BeforeEach
	void setUp() {
		parser = new BasicBuildParser();
	}

	@Test
	void testParse() {
		Job job = new Job().setId("1").setName("jira-jenkins-integration");

		Build build = new Build().setJob(job);
		parser.parse(loadJson("build.json"), build);

		assertThat(build, equalTo(new Build().setJob(job)
		                                     .setNumber(2144)
		                                     .setDescription("My build description")
		                                     .setDuration(135329L)
		                                     .setTimestamp(1355405446078L)
		                                     .setBuiltOn("remote-slave-6")
		                                     .setResult(FAILURE)));
	}

	@Test
	void testParse_WithDisplayName() {
		Job job = new Job().setId("1").setName("jira-jenkins-integration");
		Build build = new Build().setJob(job);
		parser.parse(loadJson("build-with-display-name.json"), build);

		assertThat(build, equalTo(new Build().setJob(job)
		                                     .setNumber(2144)
		                                     .setDisplayName("#1.55.2144")
		                                     .setDescription("My build description")
		                                     .setDuration(135329L)
		                                     .setTimestamp(1355405446078L)
		                                     .setBuiltOn("remote-slave-6")
		                                     .setResult(FAILURE)));
	}

	@Test
	void testParse_WithDisplayNameField_Default() {
		Job job = new Job().setId("1").setName("jira-jenkins-integration");
		Build build = new Build().setJob(job);
		parser.parse(loadJson("build-with-display-name-field-default.json"), build);

		assertThat(build, equalTo(new Build().setJob(job)
		                                     .setNumber(2144)
		                                     .setDescription("My build description")
		                                     .setDuration(135329L)
		                                     .setTimestamp(1355405446078L)
		                                     .setBuiltOn("remote-slave-6")
		                                     .setResult(FAILURE)));
	}

	@Test
	void testParse_WithDisplayNameField_NonDefault() {
		Job job = new Job().setId("1").setName("jira-jenkins-integration");
		Build build = new Build().setJob(job);
		parser.parse(loadJson("build-with-display-name-field-non-default.json"), build);

		assertThat(build, equalTo(new Build().setJob(job)
		                                     .setNumber(2144)
		                                     .setDisplayName("Non Default Builds Display Name")
		                                     .setDescription("My build description")
		                                     .setDuration(135329L)
		                                     .setTimestamp(1355405446078L)
		                                     .setBuiltOn("remote-slave-6")
		                                     .setResult(FAILURE)));
	}
}
