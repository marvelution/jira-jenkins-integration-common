/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.model.parsers;

import java.util.*;

import jakarta.json.*;
import org.junit.jupiter.api.*;

import static org.marvelution.jji.model.parsers.ParserUtils.*;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

/**
 * Tests for {@link ParserUtils}.
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
class ParserUtilsTest
		extends AbstractParserTest
{

	@Test
	void testOptString()
	{
		assertThat(optString(getJsonObject("{\"string\": \"string\"}"), "string"), is(Optional.of("string")));
		assertThat(optString(getJsonObject("{\"string\": \"string\", \"string1\": \"string1\"}"), "string1", "string"), is(Optional.of("string1")));
		assertThat(optString(getJsonObject("{\"string\": \"string\", \"string1\": \"string1\"}"), "string", "string1"), is(Optional.of("string")));

		assertThat(optString(getJsonObject("{\"string\": 10}"), "string"), is(Optional.empty()));

		assertThat(optString(getJsonObject("{\"string\": true}"), "string"), is(Optional.empty()));

		assertThat(optString(getJsonObject("{\"string\": [\"1\",\"2\",\"3\"]}"), "string"), is(Optional.empty()));
	}

	@Test
	void testOptLong()
	{
		assertThat(optLong(getJsonObject("{\"long\": 1}"), "long"), is(Optional.of(1L)));

		assertThat(optLong(getJsonObject("{\"long\": \"string\"}"), "long"), is(Optional.empty()));

		assertThat(optLong(getJsonObject("{\"long\": true}"), "long"), is(Optional.empty()));

		assertThat(optLong(getJsonObject("{\"long\": [\"1\",\"2\",\"3\"]}"), "long"), is(Optional.empty()));
	}

	@Test
	void testOptInt()
	{
		assertThat(optInt(getJsonObject("{\"int\": 1}"), "int"), is(Optional.of(1)));

		assertThat(optInt(getJsonObject("{\"int\": \"string\"}"), "int"), is(Optional.empty()));

		assertThat(optInt(getJsonObject("{\"int\": true}"), "int"), is(Optional.empty()));

		assertThat(optInt(getJsonObject("{\"int\": [\"1\",\"2\",\"3\"]}"), "int"), is(Optional.empty()));
	}

	@Test
	void testOptBoolean()
	{
		assertThat(optBoolean(getJsonObject("{\"boolean\": true}"), "boolean"), is(Optional.of(true)));

		assertThat(optBoolean(getJsonObject("{\"boolean\": \"string\"}"), "boolean"), is(Optional.empty()));

		assertThat(optBoolean(getJsonObject("{\"boolean\": 1}"), "boolean"), is(Optional.empty()));

		assertThat(optBoolean(getJsonObject("{\"boolean\": [\"1\",\"2\",\"3\"]}"), "boolean"), is(Optional.empty()));
	}

	private JsonObject getJsonObject(String json)
	{
		return parseJson(json).asJsonObject();
	}
}
