/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.model.parsers;

import org.marvelution.jji.model.Build;
import org.marvelution.jji.model.TestResults;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;

/**
 * Tests for {@link BuildActionsParser}.
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
class BuildActionsParserTest extends AbstractParserTest {

	@Test
	void testParse() {
		Build build = new Build();
		new BuildActionsParser(new CauseActionParser().andThen(new TestResultsActionParser())).parse(loadJson("build.json"), build);
		assertThat(build.getCause(), is("Started by an SCM change"));
		assertThat(build.getTestResults(), notNullValue());
		TestResults testResults = build.getTestResults();
		assertThat(testResults, is(notNullValue()));
		assertThat(testResults.getFailed(), is(0));
		assertThat(testResults.getSkipped(), is(0));
		assertThat(testResults.getTotal(), is(3121));
	}
}
