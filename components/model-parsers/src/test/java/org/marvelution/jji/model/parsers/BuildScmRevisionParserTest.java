/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.model.parsers;

import org.marvelution.jji.model.*;

import org.junit.jupiter.api.*;

import static org.assertj.core.api.Assertions.*;

/**
 * Test for {@link BuildScmRevisionParser}.
 *
 * @author Mark Rekveld
 * @since 1.37
 */
class BuildScmRevisionParserTest
		extends AbstractParserTest
{

	@Test
	void testParse()
	{
		Build build = parse("build-scm-revision.json");

		assertThat(build.getBranches()).containsOnly("PR-1");
	}

	@Test
	void testParse_MultipleGitBuildDataElements()
	{
		Build build = parse("build-multiple-git-branches.json");

		assertThat(build.getBranches()).containsOnly("origin/master", "origin/other-branch", "remote/master");
	}

	@Test
	void testParse_DefaultToBranchNameParser()
	{
		Build build = parse("build-git-branches.json");

		assertThat(build.getBranches()).containsOnly("origin/master");
	}

	@Test
	void testParse_NoScmRevision()
	{
		Build build = parse("build.json");

		assertThat(build.getBranches()).isEmpty();
	}

	private Build parse(String json)
	{
		Build build = new Build();
		new BuildActionsParser(new BuildScmRevisionParser().andThen(new BuildGitBranchesParser())).parse(loadJson(json), build);
		return build;
	}
}
