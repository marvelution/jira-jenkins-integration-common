/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.model.parsers;

import java.io.*;

import org.marvelution.testing.*;

import jakarta.json.*;

/**
 * Base test implementation for {@link Parser parsers} and {@link MergingParser merging parsers}
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
class AbstractParserTest
		extends TestSupport
{

	/**
	 * Parse the specified {@link String} as JSON
	 */
	JsonValue parseJson(String json)
	{
		try (JsonReader reader = Json.createReader(new StringReader(json)))
		{
			return reader.read();
		}
	}

	/**
	 * Load and parse the specified JSON from the classpath located in the 'api-json/' package
	 */
	JsonValue loadJson(String json)
	{
		try (JsonReader reader = Json.createReader(
				new InputStreamReader(getClass().getClassLoader().getResourceAsStream("api-json/" + json))))
		{
			return reader.read();
		}
	}
}
