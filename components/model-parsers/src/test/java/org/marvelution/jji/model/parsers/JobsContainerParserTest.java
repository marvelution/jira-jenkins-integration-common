/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.model.parsers;

import org.marvelution.jji.model.*;

import org.junit.jupiter.api.*;

import static org.marvelution.jji.model.Matchers.equalTo;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

/**
 * Tests for {@link JobsContainerParser}.
 *
 * @author Mark Rekveld
 * @since 1.9.0
 */
class JobsContainerParserTest extends AbstractParserTest {

	@Test
	void testParse_Site() {
		Site site = new Site().setId("1");
		new JobsContainerParser<>().parse(loadJson("job-list.json"), site);
		assertThat(site.getJobs(), hasSize(3));
		assertThat(site.getJobs(), hasItem(equalTo(new Job().setSite(site).setName("jira-jenkins-integration"))));
		assertThat(site.getJobs(), hasItem(equalTo(new Job().setSite(site).setName("jira-jenkins-integration-common"))));
		assertThat(site.getJobs(), hasItem(equalTo(new Job().setSite(site).setName("jenkins-jira-integration"))));
	}

	@Test
	void testParse_Job() {
		Job job = new Job().setName("Cave").setSite(new Site().setId("1"));
		new JobsContainerParser<>().parse(loadJson("folder-job-list.json"), job);

		assertThat(job.getJobs(), hasSize(3));
		assertThat(job.getJobs(), hasItem(equalTo(new Job().setSite(new Site().setId("1"))
		                                                   .setName("jira-jenkins-integration")
		                                                   .setUrlName("Cave/job/jira-jenkins-integration"))));
		assertThat(job.getJobs(), hasItem(equalTo(new Job().setSite(new Site().setId("1"))
		                                                   .setName("jira-jenkins-integration-common")
		                                                   .setUrlName("Cave/job/jira-jenkins-integration-common"))));
		assertThat(job.getJobs(), hasItem(equalTo(new Job().setSite(new Site().setId("1"))
		                                                   .setName("jenkins-jira-integration")
		                                                   .setUrlName("Cave/job/jenkins-jira-integration"))));
	}
}
