/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.validation.api;

import static java.util.stream.Collectors.*;

/**
 * Exception thrown in case input validation results in errors.
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class ValidationException
		extends RuntimeException
{

	private static final long serialVersionUID = -5236768273688217167L;

	private final ErrorMessages messages;

	public ValidationException(ErrorMessages messages)
	{
		this.messages = messages;
	}

	public ValidationException(String field, String message)
	{
		this(new ErrorMessages());
		messages.addError(field, message);
	}

	public ValidationException(String context, String field, String message)
	{
		this(new ErrorMessages());
		messages.addError(context, field, message);
	}

	@Override
	public String getMessage()
	{
		return messages.getErrors()
				.stream()
				.map(error -> "'" + error.getField() + "' " + error.getMessage())
				.collect(joining(", ", "Validation failed with error: ", ""));
	}

	public ErrorMessages getMessages()
	{
		return messages;
	}
}
