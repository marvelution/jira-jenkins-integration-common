/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.validation.api;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;

import static java.util.Objects.requireNonNull;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@javax.xml.bind.annotation.XmlRootElement
@javax.xml.bind.annotation.XmlAccessorType(javax.xml.bind.annotation.XmlAccessType.FIELD)
public class ErrorMessages
{

    @XmlElement
    @javax.xml.bind.annotation.XmlElement
    private List<ErrorMessage> errors;

    public ErrorMessages()
    {
        this.errors = new ArrayList<>();
    }

    public List<ErrorMessage> getErrors()
    {
        return errors;
    }

    public ErrorMessages setErrors(List<ErrorMessage> errors)
    {
        this.errors = requireNonNull(errors);
        return this;
    }

    public ErrorMessages addError(
            String field,
            String message)
    {
        return addError(null, field, message);
    }

    public ErrorMessages addError(
            String context,
            String field,
            String message)
    {
        errors.add(new ErrorMessage().setContext(context)
                .setField(field)
                .setMessage(message));
        return this;
    }

    public boolean hasErrors()
    {
        return !errors.isEmpty();
    }

    public boolean hasFieldErrors(String field)
    {
        return errors.stream()
                .anyMatch(error -> field.equals(error.field));
    }

    @XmlRootElement
    @XmlAccessorType(XmlAccessType.FIELD)
    @javax.xml.bind.annotation.XmlRootElement
    @javax.xml.bind.annotation.XmlAccessorType(javax.xml.bind.annotation.XmlAccessType.FIELD)
    public static class ErrorMessage
    {

        @XmlElement
        @javax.xml.bind.annotation.XmlElement
        private String context;
        @XmlElement
        @javax.xml.bind.annotation.XmlElement
        private String field;
        @XmlElement
        @javax.xml.bind.annotation.XmlElement
        private String message;

        public String getContext()
        {
            return context;
        }

        public ErrorMessage setContext(String context)
        {
            this.context = context;
            return this;
        }

        public String getField()
        {
            return field;
        }

        public ErrorMessage setField(String field)
        {
            this.field = field;
            return this;
        }

        public String getMessage()
        {
            return message;
        }

        public ErrorMessage setMessage(String message)
        {
            this.message = message;
            return this;
        }

        @Override
        public int hashCode()
        {
            return Objects.hash(context, field, message);
        }

        @Override
        public boolean equals(Object o)
        {
            if (this == o)
            {
                return true;
            }
            if (o == null || getClass() != o.getClass())
            {
                return false;
            }
            ErrorMessage that = (ErrorMessage) o;
            return Objects.equals(context, that.context) && Objects.equals(field, that.field) && Objects.equals(message, that.message);
        }

        @Override
        public String toString()
        {
            return new StringJoiner(", ", ErrorMessage.class.getSimpleName() + "[", "]").add("context='" + context + "'")
                    .add("field='" + field + "'")
                    .add("message='" + message + "'")
                    .toString();
        }
    }
}
