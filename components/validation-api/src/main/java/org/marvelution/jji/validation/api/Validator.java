/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.validation.api;

/**
 * Validator API.
 *
 * @author Mark Rekveld
 * @since 1.7.0
 */
public interface Validator<T> {

	/**
	 * Validates the specified object and returns any {@link ErrorMessages errors}.
	 */
	ErrorMessages validate(T object);

	/**
	 * Assert the the specified object is valid, and will throw {@link ValidationException} if there are validation errors.
	 *
	 * @see #validate(T)
	 */
	default void assertValid(T object) throws ValidationException {
		ErrorMessages errorMessages = validate(object);
		if (errorMessages.hasErrors()) {
			throw new ValidationException(errorMessages);
		}
	}
}
