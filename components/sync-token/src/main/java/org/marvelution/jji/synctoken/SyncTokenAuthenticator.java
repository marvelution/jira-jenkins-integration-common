/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.synctoken;

import java.security.NoSuchAlgorithmException;
import java.util.Optional;
import javax.annotation.Nullable;
import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.marvelution.jji.Headers;
import org.marvelution.jji.synctoken.exceptions.InvalidSyncTokenException;
import org.marvelution.jji.synctoken.exceptions.SyncTokenExpiredException;
import org.marvelution.jji.synctoken.exceptions.SyncTokenRequiredException;
import org.marvelution.jji.synctoken.exceptions.UnknownSyncTokenIssuerException;

import com.atlassian.connect.spring.internal.jwt.*;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.crypto.MACVerifier;
import com.nimbusds.jwt.JWTClaimsSet;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.atlassian.connect.spring.internal.jwt.HttpRequestCanonicalizer.computeCanonicalRequestHash;

@Named
@javax.inject.Named
public class SyncTokenAuthenticator
{

    public static final String OLD_SYNC_TOKEN_HEADER_NAME = "X-JJI-Sync-Token";
    private static final Logger LOGGER = LoggerFactory.getLogger(SyncTokenAuthenticator.class);
    private final SharedSecretProvider sharedSecretProvider;

    @Inject
    @javax.inject.Inject
    public SyncTokenAuthenticator(SharedSecretProvider sharedSecretProvider)
    {
        this.sharedSecretProvider = sharedSecretProvider;
    }

    public JWTClaimsSet authenticate(Request request)
    {
        return authenticate(Optional.ofNullable(request.getSyncToken())
                .filter(StringUtils::isNotBlank)
                .orElseThrow(SyncTokenRequiredException::new), request.getCanonicalHttpRequest());
    }

    public JWTClaimsSet authenticate(
            String token,
            CanonicalHttpRequest request)
    {
        LOGGER.debug("Authenticating request {}", request);
        try
        {
            return authenticate(token, computeCanonicalRequestHash(request));
        }
        catch (NoSuchAlgorithmException e)
        {
            throw new InvalidSyncTokenException("Unsupported token algorithm", e);
        }
    }

    private JWTClaimsSet authenticate(
            String token,
            String queryStringHash)
    {
        try
        {
            JWTClaimsSet unverifiedClaims = new JwtParser().parse(token);
            String sharedSecret = sharedSecretProvider.getSharedSecretForIssuer(unverifiedClaims.getIssuer())
                    .orElseThrow(() -> new UnknownSyncTokenIssuerException(unverifiedClaims));
            return new SymmetricJwtReader(new MACVerifier(sharedSecret)).readAndVerify(token, queryStringHash);
        }
        catch (JwtExpiredException e)
        {
            LOGGER.error(e.getMessage());
            throw new SyncTokenExpiredException(e.getMessage(), e);
        }
        catch (JOSEException | JwtParseException | JwtVerificationException e)
        {
            LOGGER.error(e.getMessage());
            throw new InvalidSyncTokenException(e.getMessage(), e);
        }
    }

    public interface SharedSecretProvider
    {
        Optional<String> getSharedSecretForIssuer(String issuer);
    }

    public static abstract class Request
    {
        public String getSyncToken()
        {
            return Optional.ofNullable(getHeader(Headers.SYNC_TOKEN))
                    .orElseGet(() -> getHeader(SyncTokenAuthenticator.OLD_SYNC_TOKEN_HEADER_NAME));
        }

        @Nullable
        protected abstract String getHeader(String header);

        public abstract CanonicalHttpRequest getCanonicalHttpRequest();
    }
}
