/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.synctoken;

import java.net.URI;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.annotation.Nullable;

import com.atlassian.connect.spring.internal.jwt.CanonicalHttpRequest;

import static java.util.stream.Collectors.toMap;
import static org.apache.commons.lang3.StringUtils.*;

public class CanonicalHttpServletRequest
        implements CanonicalHttpRequest
{

    private static final Pattern QUERY_PARAM_PATTERN = Pattern.compile("([^&=]+)(=?)([^&]+)?");
    private static final String SLASH = "/";
    private final String method;
    private final String relativePath;
    private final Map<String, String[]> parameters;

    public CanonicalHttpServletRequest(
            String method,
            URI uri,
            Optional<String> contextPath)
    {
        this.method = method.toUpperCase(Locale.ENGLISH);
        String contextPathToRemove = contextPath.filter(path -> !SLASH.equals(path))
                .map(path -> stripEnd(path, SLASH))
                .orElse("");
        relativePath = getRelativePath(uri, contextPathToRemove);
        parameters = parseQueryString(uri.getRawQuery());
    }

    public CanonicalHttpServletRequest(
            String method,
            String relativePath,
            String rawQueryString)
    {
        this.method = method;
        this.relativePath = relativePath;
        this.parameters = parseQueryString(rawQueryString);
    }

    @Override
    public String getMethod()
    {
        return method;
    }

    @Override
    public String getRelativePath()
    {
        return relativePath;
    }

    @Override
    public Map<String, String[]> getParameterMap()
    {
        return parameters;
    }

    @Override
    public String toString()
    {
        return new StringJoiner(", ", getClass().getSimpleName() + "[", "]").add("method='" + method + "'")
                .add("relativePath='" + relativePath + "'")
                .add("parameters=" + parameters)
                .toString();
    }

    public static String getRelativePath(
            URI uri,
            String contextPath)
    {
        return defaultIfBlank(removeEnd(removeStart(uri.getPath(), contextPath), SLASH), SLASH);
    }

    /**
     * transform the specified {@code queryString} to a multi value {@link Map}.
     */
    private static Map<String, String[]> parseQueryString(
            @Nullable
            String queryString)
    {
        Map<String, List<String>> parameters = new HashMap<>();
        if (isNotBlank(queryString))
        {
            Matcher matcher = QUERY_PARAM_PATTERN.matcher(queryString);
            while (matcher.find())
            {
                String name = matcher.group(1);
                String eq = matcher.group(2);
                String value = matcher.group(3);
                parameters.computeIfAbsent(name, key -> new ArrayList<>())
                        .add(value != null ? value : (length(eq) > 0 ? "" : null));
            }
        }
        return parameters.entrySet()
                .stream()
                .collect(toMap(Map.Entry::getKey,
                        entry -> entry.getValue()
                                .toArray(new String[0])));
    }
}
