/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.synctoken;

import java.net.URI;
import java.util.Locale;
import javax.annotation.Nullable;
import javax.servlet.http.HttpServletRequest;

import com.atlassian.connect.spring.internal.jwt.CanonicalHttpRequest;

import static org.marvelution.jji.synctoken.CanonicalHttpServletRequest.getRelativePath;

public class SyncTokenRequest
        extends SyncTokenAuthenticator.Request
{
    final HttpServletRequest request;

    public SyncTokenRequest(HttpServletRequest request)
    {
        this.request = request;
    }

    @Nullable
    @Override
    protected String getHeader(String header)
    {
        return request.getHeader(header);
    }

    @Override
    public CanonicalHttpRequest getCanonicalHttpRequest()
    {
        return new CanonicalHttpServletRequest(request.getMethod()
                .toUpperCase(Locale.ENGLISH),
                getRelativePath(URI.create(request.getRequestURI()), request.getContextPath()),
                request.getQueryString());
    }
}
