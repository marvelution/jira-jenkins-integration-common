/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.synctoken.jersey;

import java.io.IOException;
import java.util.Optional;
import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;

import org.marvelution.jji.Headers;
import org.marvelution.jji.synctoken.CanonicalHttpServletRequest;
import org.marvelution.jji.synctoken.SyncTokenBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SyncTokenClientFilter
        implements ClientRequestFilter
{

    private static final Logger LOGGER = LoggerFactory.getLogger(SyncTokenClientFilter.class);
    private final String identifier;
    private final String sharedSecret;
    private final String contextPath;

    public SyncTokenClientFilter(
            String identifier,
            String sharedSecret)
    {
        this(identifier, sharedSecret, null);
    }

    public SyncTokenClientFilter(
            String identifier,
            String sharedSecret,
            String contextPath)
    {
        this.identifier = identifier;
        this.sharedSecret = sharedSecret;
        this.contextPath = contextPath;
    }

    @Override
    public void filter(ClientRequestContext requestContext)
            throws IOException
    {
        CanonicalHttpServletRequest canonicalHttpRequest = new CanonicalHttpServletRequest(requestContext.getMethod(),
                requestContext.getUri(),
                Optional.ofNullable(contextPath));
        LOGGER.debug("Adding {} header for request {}", Headers.SYNC_TOKEN, canonicalHttpRequest);
        new SyncTokenBuilder().identifier(identifier)
                .sharedSecret(sharedSecret)
                .request(canonicalHttpRequest)
                .generateTokenAndAddHeaders(requestContext.getHeaders()::add);
    }
}
