/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.synctoken;

import java.security.NoSuchAlgorithmException;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.Map;
import java.util.function.BiConsumer;

import org.marvelution.jji.Headers;

import com.atlassian.connect.spring.internal.jwt.CanonicalHttpRequest;
import com.atlassian.connect.spring.internal.jwt.JwtJsonBuilder;
import com.atlassian.connect.spring.internal.jwt.JwtWriter;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.KeyLengthException;
import com.nimbusds.jose.crypto.MACSigner;

import static com.atlassian.connect.spring.internal.jwt.HttpRequestCanonicalizer.computeCanonicalRequestHash;
import static org.marvelution.jji.synctoken.SyncTokenAuthenticator.OLD_SYNC_TOKEN_HEADER_NAME;

public class SyncTokenBuilder
{

    private static final long EXPIRATION_TIME = 180;
    private final JwtJsonBuilder jwtBuilder;
    private String sharedSecret;

    public SyncTokenBuilder()
    {
        this(EXPIRATION_TIME);
    }

    public SyncTokenBuilder(long expirationTime)
    {
        jwtBuilder = new JwtJsonBuilder(Duration.of(expirationTime, ChronoUnit.SECONDS));
    }

    public SyncTokenBuilder identifier(String identifier)
    {
        jwtBuilder.issuer(identifier);
        return this;
    }

    public SyncTokenBuilder sharedSecret(String sharedSecret)
    {
        this.sharedSecret = sharedSecret;
        return this;
    }

    public SyncTokenBuilder context(Map<String, Object> context)
    {
        jwtBuilder.claim("context", context);
        return this;
    }

    public SyncTokenBuilder claim(
            String name,
            Object claim)
    {
        jwtBuilder.claim(name, claim);
        return this;
    }

    public SyncTokenBuilder request(CanonicalHttpRequest request)
    {
        try
        {
            jwtBuilder.queryHash(computeCanonicalRequestHash(request));
            return this;
        }
        catch (NoSuchAlgorithmException e)
        {
            throw new AssertionError(e);
        }
    }

    public String generateToken()
    {
        try
        {
            return new JwtWriter(JWSAlgorithm.HS256, new MACSigner(sharedSecret)).jsonToJwt(jwtBuilder.build());
        }
        catch (KeyLengthException e)
        {
            throw new AssertionError(e);
        }
    }

    public void generateTokenAndAddHeaders(BiConsumer<String, String> headerSetter)
    {
        String token = generateToken();
        headerSetter.accept(Headers.SYNC_TOKEN, token);
        // Keep setting the old header for the time being
        headerSetter.accept(OLD_SYNC_TOKEN_HEADER_NAME, token);
    }
}
