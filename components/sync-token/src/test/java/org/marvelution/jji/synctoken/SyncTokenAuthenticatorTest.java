/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.synctoken;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.marvelution.jji.Headers;
import org.marvelution.jji.synctoken.exceptions.InvalidSyncTokenException;
import org.marvelution.jji.synctoken.exceptions.SyncTokenExpiredException;
import org.marvelution.jji.synctoken.exceptions.UnknownSyncTokenIssuerException;
import org.marvelution.jji.synctoken.utils.SharedSecretGenerator;
import org.marvelution.testing.TestSupport;

import com.atlassian.connect.spring.internal.jwt.CanonicalHttpRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import static java.util.Optional.empty;
import static java.util.Optional.of;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@MockitoSettings(strictness = Strictness.LENIENT)
class SyncTokenAuthenticatorTest
        extends TestSupport
{

    @Mock
    private SyncTokenAuthenticator.SharedSecretProvider sharedSecretProvider;
    private SyncTokenAuthenticator authenticator;
    private String sharedSecret;
    private String identifier;
    private CanonicalHttpRequest request;

    @BeforeEach
    void setUp()
    {
        sharedSecret = SharedSecretGenerator.generate();
        identifier = UUID.randomUUID()
                .toString();
        authenticator = new SyncTokenAuthenticator(sharedSecretProvider);
        when(sharedSecretProvider.getSharedSecretForIssuer(anyString())).thenAnswer(invocation -> invocation.getArgument(0)
                .equals(identifier) ? of(sharedSecret) : empty());
        request = new CanonicalHttpServletRequest("GET",
                URI.create("http://localhost:2990/jira/rest/jenkins/1.0/build/job?param1=value1&param2=value2"),
                of("/jira"));
    }

    @Test
    void testGenerateTokenAndAddHeaders()
    {
        Map<String, String> headers = new HashMap<>();

        String token = new SyncTokenBuilder().identifier(identifier)
                .sharedSecret(sharedSecret)
                .request(request)
                .generateToken();

        new SyncTokenBuilder().identifier(identifier)
                .sharedSecret(sharedSecret)
                .request(request)
                .generateTokenAndAddHeaders(headers::put);

        assertThat(headers).containsKeys(Headers.SYNC_TOKEN, SyncTokenAuthenticator.OLD_SYNC_TOKEN_HEADER_NAME)
                .containsValue(token);
    }

    @Test
    void testAuthenticate()
    {
        String token = new SyncTokenBuilder().identifier(identifier)
                .sharedSecret(sharedSecret)
                .request(request)
                .generateToken();

        authenticator.authenticate(token, request);

        verify(sharedSecretProvider).getSharedSecretForIssuer(identifier);
    }

    @Test
    void testAuthenticate_ExpiredToken()
            throws Exception
    {
        String token = new SyncTokenBuilder(1).identifier(identifier)
                .sharedSecret(sharedSecret)
                .request(request)
                .generateToken();

        TimeUnit.SECONDS.sleep(35);

        assertThatThrownBy(() -> authenticator.authenticate(token, request)).isInstanceOf(SyncTokenExpiredException.class)
                .hasMessageStartingWith("JWT expired at");

        verify(sharedSecretProvider).getSharedSecretForIssuer(identifier);
    }

    @Test
    void testAuthenticate_InvalidToken_UnknownIssuer()
    {
        String token = new SyncTokenBuilder().identifier("i-am-not-known")
                .sharedSecret(sharedSecret)
                .request(request)
                .generateToken();

        assertThatThrownBy(() -> authenticator.authenticate(token, request)).isInstanceOf(UnknownSyncTokenIssuerException.class)
                .hasMessage("Unknown token issuer, i-am-not-known");

        verify(sharedSecretProvider).getSharedSecretForIssuer("i-am-not-known");
    }

    @Test
    void testAuthenticate_InvalidToken()
    {
        String token = new SyncTokenBuilder().identifier(identifier)
                .sharedSecret(sharedSecret)
                .request(request)
                .generateToken();
        String finalToken = token.substring(0, token.lastIndexOf('.'));

        assertThatThrownBy(() -> authenticator.authenticate(finalToken, request)).isInstanceOf(InvalidSyncTokenException.class)
                .hasMessageContaining("Missing second delimiter");

        verifyNoMoreInteractions(sharedSecretProvider);
    }
}
