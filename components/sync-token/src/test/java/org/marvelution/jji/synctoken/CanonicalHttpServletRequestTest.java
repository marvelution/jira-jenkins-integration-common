/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.synctoken;

import java.net.*;

import org.junit.jupiter.api.*;

import static java.util.Optional.empty;
import static java.util.Optional.*;
import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

class CanonicalHttpServletRequestTest
{

    @Test
    void testMethodTransformedToUppercaseWithoutParametersAndNoContextPath()
    {
        URI uri = URI.create("http://localhost:2990/jira");
        CanonicalHttpServletRequest request = new CanonicalHttpServletRequest("get", uri, empty());
        assertThat(request.getMethod(), is("GET"));
        assertThat(request.getRelativePath(), is("/jira"));
        assertThat(request.getParameterMap()
                .isEmpty(), is(true));
    }

    @Test
    void testURIWithParametersAndContextPath()
    {
        URI uri = URI.create("http://localhost:2990/jira/rest/jenkins/1.0/build/job?param1=value1&param2=value2");
        CanonicalHttpServletRequest request = new CanonicalHttpServletRequest("GET", uri, of("/jira"));
        assertThat(request.getMethod(), is("GET"));
        assertThat(request.getRelativePath(), is("/rest/jenkins/1.0/build/job"));
        assertThat(request.getParameterMap()
                .isEmpty(), is(false));
        assertThat(request.getParameterMap()
                .keySet(), hasItems("param1", "param2"));
        assertThat(request.getParameterMap()
                .get("param1")[0], is("value1"));
        assertThat(request.getParameterMap()
                .get("param2")[0], is("value2"));
    }

    @Test
    void testURIWithMultiValueParametersAndSlashContextPath()
    {
        URI uri = URI.create("http://localhost:2990/jira?param1=value1&param1=value2");
        CanonicalHttpServletRequest request = new CanonicalHttpServletRequest("GET", uri, of("/"));
        assertThat(request.getMethod(), is("GET"));
        assertThat(request.getRelativePath(), is("/jira"));
        assertThat(request.getParameterMap()
                .isEmpty(), is(false));
        assertThat(request.getParameterMap()
                .keySet(), hasItems("param1"));
        assertThat(request.getParameterMap()
                .get("param1")[0], is("value1"));
        assertThat(request.getParameterMap()
                .get("param1")[1], is("value2"));
    }

    @Test
    void testContextPathTrailingSlash()
    {
        URI uri = URI.create("http://localhost:2990/jira/rest/jenkins/1.0/build");
        CanonicalHttpServletRequest request = new CanonicalHttpServletRequest("get", uri, of("/jira/"));
        assertThat(request.getMethod(), is("GET"));
        assertThat(request.getRelativePath(), is("/rest/jenkins/1.0/build"));
        assertThat(request.getParameterMap()
                .isEmpty(), is(true));
    }
}
