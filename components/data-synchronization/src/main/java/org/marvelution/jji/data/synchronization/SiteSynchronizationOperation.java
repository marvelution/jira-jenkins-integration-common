/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.synchronization;

import java.util.*;

import org.marvelution.jji.data.services.api.*;
import org.marvelution.jji.data.synchronization.api.context.*;
import org.marvelution.jji.model.*;

import jakarta.inject.*;

import static org.marvelution.jji.model.Site.*;

@Named
@javax.inject.Named
public class SiteSynchronizationOperation
        extends AbstractJobListSynchronizationOperation<Site>
{

    private final SiteClient siteClient;
    private final SiteService siteService;

    @Inject
    @javax.inject.Inject
    public SiteSynchronizationOperation(
            SiteClient siteClient,
            JobService jobService,
            SiteService siteService)
    {
        super(jobService);
        this.siteClient = siteClient;
        this.siteService = siteService;
    }

    @Override
    public boolean supports(OperationId operationId)
    {
        return Objects.equals(operationId.getPrefix(), OPERATION_PREFIX) || operationId.getId()
                .startsWith(OPERATION_PREFIX);
    }

    @Override
    protected Optional<Site> locateSyncable(String id)
    {
        return siteService.get(id);
    }

    @Override
    protected void doSynchronize(SynchronizationContext<Site> context)
    {
        Site site = context.getSyncable();
        logger.debug("Checking if the Jenkins plugin is installed and if crumb security is used on {}", site);
        site.setUseCrumbs(siteClient.isCrumbSecurityEnabled(site));
        boolean jenkinsPluginInstalled = siteClient.isJenkinsPluginInstalled(site);
        if (jenkinsPluginInstalled != site.isJenkinsPluginInstalled())
        {
            site.setJenkinsPluginInstalled(jenkinsPluginInstalled);
        }
        Site saved = siteService.save(site);
        logger.debug("Synchronizing all root level jobs from {}", saved.getName());
        synchronizeJobs(siteClient.getJobs(site), jobService.getRootJobsOnSite(saved), context);
    }
}
