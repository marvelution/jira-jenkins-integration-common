/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.synchronization;

import java.util.*;

import org.marvelution.jji.data.services.api.*;
import org.marvelution.jji.data.synchronization.api.context.*;
import org.marvelution.jji.model.*;

import static java.util.function.Function.*;
import static java.util.stream.Collectors.*;

abstract class AbstractJobListSynchronizationOperation<S extends Syncable<S>>
		extends AbstractSynchronizationOperation<S>
{

	final JobService jobService;

	AbstractJobListSynchronizationOperation(JobService jobService)
	{
		this.jobService = jobService;
	}

	void synchronizeJobs(
			List<Job> jobsToSync,
			List<Job> localJobs,
			SynchronizationContext<S> context)
	{
		List<Job> duplicates = new ArrayList<>();
		Map<String, Job> localJobsByUrlName = localJobs.stream().collect(toMap(Job::getUrlName, identity(), (oldValue, newValue) -> {
			if (oldValue.getLastBuild() >= newValue.getLastBuild())
			{
				duplicates.add(newValue);
				return oldValue;
			}
			else
			{
				duplicates.add(oldValue);
				return newValue;
			}
		}));
		if (!duplicates.isEmpty())
		{
			logger.debug("Removing {} duplicate jobs", duplicates.size());
			duplicates.stream().map(job -> job.setDeleted(true)).forEach(jobService::delete);
		}
		for (Job job : jobsToSync)
		{
			Job localJob = localJobsByUrlName.remove(job.getUrlName());
			if (localJob != null)
			{
				saveAndSynchronize(localJob, context);
			}
			else
			{
				job.setLastBuild(0);
				saveAndSynchronize(job, context);
			}
		}
	}

	private void saveAndSynchronize(
			Job job,
			SynchronizationContext<S> context)
	{
		job.setDeleted(false);
		Job saved = jobService.save(job);
		if (shouldSynchronizeJob(job))
		{
			context.synchronize(saved);
		}
	}

	private boolean shouldSynchronizeJob(Job job)
	{
		if (job.getSite().isInaccessibleSite())
		{
			return false;
		}
		else if (job.getSite().isJenkinsPluginInstalled())
		{
			return job.getLastBuild() == 0;
		}
		else
		{
			return true;
		}
	}
}
