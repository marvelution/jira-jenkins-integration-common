/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.synchronization;

import javax.annotation.*;
import java.util.*;
import java.util.regex.*;

import org.marvelution.jji.data.services.api.*;
import org.marvelution.jji.data.synchronization.api.context.*;
import org.marvelution.jji.model.*;
import org.marvelution.jji.utils.*;

import jakarta.inject.*;

import static java.util.Collections.*;
import static java.util.stream.Collectors.*;
import static org.marvelution.jji.model.Build.*;

@Named
@javax.inject.Named
public class BuildSynchronizationOperation
        extends AbstractSynchronizationOperation<Build>
{

    private final BuildService buildService;
    private final IssueLinkService issueLinkService;
    private final JobService jobService;

    @Inject
    @javax.inject.Inject
    public BuildSynchronizationOperation(
            BuildService buildService,
            IssueLinkService issueLinkService,
            JobService jobService)
    {
        this.buildService = buildService;
        this.issueLinkService = issueLinkService;
        this.jobService = jobService;
    }

    @Override
    public boolean supports(OperationId operationId)
    {
        return Objects.equals(operationId.getPrefix(), OPERATION_PREFIX) || operationId.getId()
                .startsWith(OPERATION_PREFIX);
    }

    @Override
    public Optional<Build> locateSyncable(
            @Nullable
            String id,
            OperationId operationId)
    {
        if (id != null)
        {
            return buildService.get(id);
        }
        else
        {
            Pattern pattern = Pattern.compile("^" + Build.OPERATION_PREFIX + "-([\\pL\\pN\\p{Pc}]{32,40})-(\\d*)$");
            Matcher matcher = pattern.matcher(operationId.getId());
            if (matcher.matches())
            {
                String jobId = matcher.group(1);
                int buildNumber = Integer.parseInt(matcher.group(2));
                return jobService.get(jobId)
                        .map(job -> new Build().setJob(job)
                                .setNumber(buildNumber));
            }
        }
        return Optional.empty();
    }

    @Override
    protected void doSynchronize(SynchronizationContext<Build> context)
    {
        Build build = context.getSyncable();
        Optional<Build> existing = buildService.get(build.getJob(), build.getNumber());
        if (existing.isPresent())
        {
            build = existing.get();
        }
        Job job = build.getJob();
        try
        {
            int previousOldestBuild = job.getOldestBuild();
            context.populateJobDetails(job);
            if (!job.isDeleted())
            {
                synchronizeBuild(build, job, context);
                if (job.getOldestBuild() > 1 && job.getOldestBuild() != previousOldestBuild)
                {
                    logger.debug("Marking all builds of {} prior to {} as deleted", job.getName(), job.getOldestBuild());
                    buildService.markAllInJobAsDeleted(job, job.getOldestBuild());
                }
            }
            else
            {
                logger.debug("Seems job {} is no longer available, marking it as deleted and skipping synchronization of build {}",
                        job.getName(),
                        build.getNumber());
            }
            jobService.save(job);
        }
        catch (Exception e)
        {
            context.addError(e);
        }
    }

    <S extends Syncable<S>> void synchronizeBuild(
            Build build,
            Job job,
            SynchronizationContext<S> context)
    {
        Build saved = null;
        if (!context.stopRequested())
        {
            try
            {
                if (context.populateBuildDetails(build))
                {
                    if (!build.isDeleted() && build.getResult() != null)
                    {
                        if (build.getNumber() > job.getLastBuild())
                        {
                            logger.debug("Bumping job last build to {}", build.getNumber());
                            job.setLastBuild(build.getNumber());
                        }
                        logger.debug("Synchronizing Build {} from {}", build.getNumber(), job.getName());
                        saved = buildService.save(build);

                        Set<String> keys = KeyExtractor.extractIssueKeys(build, job);
                        Set<IssueReference> references = !keys.isEmpty() ? issueLinkService.relink(saved, keys) : emptySet();
                        if (!references.isEmpty())
                        {
                            context.linkedIssues(references);
                            String issueKeys = references.stream()
                                    .map(IssueReference::getIssueKey)
                                    .collect(joining(", "));
                            logger.debug("Found [{}] related to {}", issueKeys, saved);
                        }
                    }
                    else
                    {
                        logger.debug("Skipping Build {} from {}", build.getNumber(), job.getName());
                        if (build.getId() != null)
                        {
                            saved = buildService.save(build);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                logger.warn("Failed to synchronize build {} of {}: {}", build.getNumber(), job.getName(), e.getMessage(), e);
                context.addError(e);
            }
        }
        Optional.ofNullable(saved)
                .ifPresent(context::synchronizedBuild);
    }
}
