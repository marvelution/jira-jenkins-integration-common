/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.synchronization;

import java.util.*;
import java.util.function.Predicate;
import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.marvelution.jji.api.features.FeatureFlags;
import org.marvelution.jji.api.features.Features;
import org.marvelution.jji.data.services.api.BuildService;
import org.marvelution.jji.data.services.api.JobService;
import org.marvelution.jji.data.synchronization.api.context.SynchronizationContext;
import org.marvelution.jji.data.synchronization.api.trigger.JobCreatedTrigger;
import org.marvelution.jji.data.synchronization.api.trigger.JobModifiedTrigger;
import org.marvelution.jji.data.synchronization.api.trigger.JobMovedTrigger;
import org.marvelution.jji.model.Build;
import org.marvelution.jji.model.Job;
import org.marvelution.jji.model.OperationId;

import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toMap;
import static java.util.stream.IntStream.range;
import static org.marvelution.jji.model.Job.OPERATION_PREFIX;

@Named
@javax.inject.Named
public class JobSynchronizationOperation
        extends AbstractJobListSynchronizationOperation<Job>
{

    private final BuildService buildService;
    private final Features features;

    @Inject
    @javax.inject.Inject
    public JobSynchronizationOperation(
            JobService jobService,
            BuildService buildService,
            Features features)
    {
        super(jobService);
        this.buildService = buildService;
        this.features = features;
    }

    @Override
    public boolean supports(OperationId operationId)
    {
        return Objects.equals(operationId.getPrefix(), OPERATION_PREFIX) || operationId.getId()
                .startsWith(OPERATION_PREFIX);
    }

    @Override
    protected Optional<Job> locateSyncable(String id)
    {
        return jobService.get(id);
    }

    @Override
    protected void doSynchronize(SynchronizationContext<Job> context)
    {
        Job job = context.getSyncable();
        try
        {
            int previousOldestBuild = job.getOldestBuild();
            if (context.populateJobDetails(job))
            {
                if (!job.isDeleted())
                {
                    if (isShallowSync(context))
                    {
                        logger.debug("Only performing a shallow sync of job {}", job.getName());
                    }
                    else
                    {
                        if (!job.getJobs()
                                .isEmpty())
                        {
                            logger.debug("Synchronizing child-jobs of {}", job.getName());
                            synchronizeJobs(job.getJobs(), jobService.getSubJobs(job), context);
                        }
                        synchronizeBuilds(job, context);
                    }
                    if (job.getOldestBuild() > 1 && job.getOldestBuild() != previousOldestBuild)
                    {
                        logger.debug("Marking all builds of {} prior to {} as deleted", job.getName(), job.getOldestBuild());
                        buildService.markAllInJobAsDeleted(job, job.getOldestBuild());
                    }
                }
                else
                {
                    logger.debug("Seems job {} is no longer available, marking it as deleted", job.getName());
                }
                jobService.save(job);
            }
        }
        catch (Exception e)
        {
            context.addError(e);
        }
    }

    private boolean isShallowSync(SynchronizationContext<Job> context)
    {
        return context.triggerReason(JobMovedTrigger.class)
                       .isPresent() || context.triggerReason(JobModifiedTrigger.class)
                       .isPresent() || context.triggerReason(JobCreatedTrigger.class)
                       .isPresent();
    }

    private void synchronizeBuilds(
            Job job,
            SynchronizationContext<Job> context)
    {
        if (job.getBuilds()
                .isEmpty())
        {
            return;
        }

        if (job.getOldestBuild() > 0 && job.getBuilds()
                .stream()
                .noneMatch(build -> build.getNumber() == job.getOldestBuild()))
        {
            job.getBuilds()
                    .stream()
                    .map(Build::getNumber)
                    .min(Integer::compareTo)
                    .ifPresent(number -> {
                        logger.debug("Job {} has more builds available then returned, adding them to the builds sync list", job.getName());
                        range(job.getOldestBuild(), number).mapToObj(n -> new Build().setNumber(n)
                                        .setJob(job))
                                .forEach(job.getBuilds()::add);
                    });
        }

        int oldest = job.getBuilds()
                .stream()
                .map(Build::getNumber)
                .min(Integer::compareTo)
                .orElse(0);
        int newest = job.getBuilds()
                .stream()
                .map(Build::getNumber)
                .max(Integer::compareTo)
                .orElse(-1);

        // TODO Improve duplicate lookup
        List<Build> duplicates = new ArrayList<>();
        Map<Integer, Build> builds = buildService.getAllInRange(job, oldest, newest)
                .stream()
                .collect(toMap(Build::getNumber, identity(), (build, build2) -> {
                    duplicates.add(build2);
                    return build;
                }));
        if (!duplicates.isEmpty())
        {
            logger.debug("Removing {} duplicate builds for job {}", duplicates.size(), job.getName());
            duplicates.forEach(buildService::delete);
        }
        job.getBuilds()
                .stream()
                .filter(build -> !builds.containsKey(build.getNumber()))
                .forEach(build -> builds.put(build.getNumber(), build));

        Predicate<Build> filter = build -> true;
        if (!features.isFeatureEnabled(FeatureFlags.SYNC_ALL_BUILDS) && job.getLastBuild() > 0)
        {
            filter = build -> build.getId() == null;
        }

        builds.values()
                .stream()
                .filter(filter)
                .forEach(context::synchronize);
    }
}
