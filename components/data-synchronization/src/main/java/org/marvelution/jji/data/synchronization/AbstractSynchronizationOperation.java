/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.synchronization;

import java.util.*;
import javax.annotation.*;

import org.marvelution.jji.data.synchronization.api.*;
import org.marvelution.jji.data.synchronization.api.context.*;
import org.marvelution.jji.model.*;

import org.slf4j.*;

public abstract class AbstractSynchronizationOperation<S extends Syncable<S>>
		implements SynchronizationOperation<S>
{

	protected final Logger logger = LoggerFactory.getLogger(getClass());

	@Override
	public Optional<S> locateSyncable(
			@Nullable String id,
			OperationId operationId)
	{
		Optional<S> syncable = Optional.empty();
		if (id != null)
		{
			syncable = locateSyncable(id);
		}
		if (!syncable.isPresent())
		{
			syncable = locateSyncable(operationId.getUnprefixedId());
		}
		return syncable;
	}

	protected Optional<S> locateSyncable(String id)
	{
		return Optional.empty();
	}

	@Override
	public final void synchronize(SynchronizationContext<S> context)
	{
		if (context.stopRequested())
		{
			return;
		}
		logger.debug("Performing synchronization of {}", context.getOperationId());
		try
		{
			context.started();
			Status status = context.getSyncableStatus();
			if (status.isAccessible())
			{
				doSynchronize(context);
			}
			else
			{
				logger.error("Skipping synchronization operation for {}, remote is {}", context.getOperationId(), status.describe());
			}
		}
		catch (Exception e)
		{
			logger.warn("Failed to Synchronize {}: {}", context.getSyncable(), e.getMessage(), e);
			context.addError(e);
		}
		finally
		{
			context.finished();
		}
	}

	protected abstract void doSynchronize(SynchronizationContext<S> context)
			throws SynchronizationOperationException;
}
