/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.synchronization;

import java.util.*;

import org.marvelution.jji.api.features.FeatureFlag;
import org.marvelution.jji.api.features.Features;
import org.marvelution.jji.api.features.InMemoryFeatureStore;
import org.marvelution.jji.data.services.api.SiteResponseListener;
import org.marvelution.jji.data.synchronization.api.context.SynchronizationContext;
import org.marvelution.jji.data.synchronization.api.trigger.JobMovedTrigger;
import org.marvelution.jji.data.synchronization.api.trigger.JobSyncTrigger;
import org.marvelution.jji.data.synchronization.api.trigger.UpstreamTrigger;
import org.marvelution.jji.events.JobSynchronizedEvent;
import org.marvelution.jji.model.Build;
import org.marvelution.jji.model.Job;
import org.marvelution.jji.model.Site;
import org.marvelution.jji.model.Status;

import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import static java.util.Arrays.asList;
import static java.util.Collections.emptySet;
import static org.assertj.core.api.Assertions.assertThat;
import static org.marvelution.jji.api.features.FeatureFlags.SYNC_ALL_BUILDS;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

class JobSynchronizationOperationTest
        extends AbstractSynchronizationOperationTest<Job, JobSynchronizationOperation>
{

    private Features features;

    @Override
    protected JobSynchronizationOperation createOperation()
    {
        features = new Features(new InMemoryFeatureStore())
        {
            @Override
            protected Set<FeatureFlag> loadFeatureFlags()
            {
                return Collections.singleton(new FeatureFlag(SYNC_ALL_BUILDS, true, true));
            }
        };
        return new JobSynchronizationOperation(jobService, buildService, features);
    }

    @Override
    protected Job createSyncable()
    {
        return createJob(createSite());
    }

    @Test
    void testSynchronize()
    {
        when(jobService.save(any(Job.class))).then(invocation -> invocation.getArgument(0));

        Site site = createSite();
        Job job = createJob(site);
        Job subJob1 = createJob(site);
        Job subJob2 = createJob(site);
        Job subJob3 = createJob(site);
        Job subJob4 = createJob(site).setName(subJob2.getName());
        Build build1 = createBuild(job).setNumber(10);
        Build build2 = createBuild(job).setNumber(11);

        when(siteClient.getRemoteStatus(eq(job))).thenReturn(Status.ONLINE);
        when(siteClient.populateJobDetails(job)).then(invocation -> {
            ((Job) invocation.getArgument(0)).setJobs(asList(subJob1, subJob2))
                    .setBuilds(asList(build1, build2))
                    .setOldestBuild(10);
            return true;
        });
        when(jobService.getSubJobs(eq(job))).thenReturn(asList(subJob2, subJob3, subJob4));
        when(buildService.getAllInRange(job, 10, 11)).thenReturn(emptySet());
        when(result.triggerReason()).thenReturn(new JobSyncTrigger(""));
        when(result.triggerReason(any())).thenReturn(Optional.empty());

        SynchronizationContext<Job> synchronizationContext = createSynchronizationContext(job);
        operation.synchronize(synchronizationContext);

        verify(jobService).save(job);
        verify(jobService).save(subJob1);
        verify(jobService).save(subJob2);
        verify(jobService, never()).save(subJob3);
        verify(jobService, never()).save(subJob4);
        verify(jobService).delete(subJob4);
        verify(result, never()).synchronizedJob(any(Job.class));
        verify(synchronizationService).synchronize(eq(subJob1), any(UpstreamTrigger.class));
        verify(synchronizationService).synchronize(eq(subJob2), any(UpstreamTrigger.class));
        verify(synchronizationService, never()).synchronize(eq(subJob3), any());
        verify(synchronizationService, never()).synchronize(eq(subJob4), any());

        verify(buildService).markAllInJobAsDeleted(job, 10);

        verify(result).started();
        verify(result).finished();

        verify(eventPublisher).publish(any(JobSynchronizedEvent.class));

        verify(siteClient).getRemoteStatus(job);
        verify(siteClient).populateJobDetails(job);
        verify(siteClient).registerListener(any(SiteResponseListener.class));
        verify(siteClient).unregisterListener(any(SiteResponseListener.class));
        verifyNoMoreInteractions(siteClient);
    }

    @Test
    void testSynchronize_ShallowSync()
    {
        when(jobService.save(any(Job.class))).then(invocation -> invocation.getArgument(0));

        Site site = createSite();
        Job job = createJob(site);
        Job subJob1 = createJob(site);
        Job subJob2 = createJob(site);
        Job subJob3 = createJob(site);
        Job subJob4 = createJob(site).setName(subJob2.getName());
        Build build1 = createBuild(job).setNumber(10);
        Build build2 = createBuild(job).setNumber(11);

        when(siteClient.getRemoteStatus(eq(job))).thenReturn(Status.ONLINE);
        when(siteClient.populateJobDetails(job)).then(invocation -> {
            ((Job) invocation.getArgument(0)).setJobs(asList(subJob1, subJob2))
                    .setBuilds(asList(build1, build2))
                    .setOldestBuild(10);
            return true;
        });
        JobMovedTrigger triggerReason = new JobMovedTrigger("");
        when(result.triggerReason()).thenReturn(triggerReason);
        when(result.triggerReason(JobMovedTrigger.class)).thenReturn(Optional.of(triggerReason));

        SynchronizationContext<Job> synchronizationContext = createSynchronizationContext(job);
        operation.synchronize(synchronizationContext);

        verify(jobService).save(job);
        verify(jobService, never()).save(subJob1);
        verify(jobService, never()).save(subJob2);
        verify(jobService, never()).save(subJob3);
        verify(jobService, never()).save(subJob4);
        verify(jobService, never()).delete(subJob4);
        verify(result, never()).synchronizedJob(any(Job.class));
        verify(synchronizationService, never()).synchronize(eq(subJob1), any(UpstreamTrigger.class));
        verify(synchronizationService, never()).synchronize(eq(subJob2), any(UpstreamTrigger.class));
        verify(synchronizationService, never()).synchronize(eq(subJob3), any());
        verify(synchronizationService, never()).synchronize(eq(subJob4), any());

        verify(buildService).markAllInJobAsDeleted(job, 10);

        verify(result).started();
        verify(result).finished();

        verify(eventPublisher).publish(any(JobSynchronizedEvent.class));

        verify(siteClient).getRemoteStatus(job);
        verify(siteClient).populateJobDetails(job);
        verify(siteClient).registerListener(any(SiteResponseListener.class));
        verify(siteClient).unregisterListener(any(SiteResponseListener.class));
        verifyNoMoreInteractions(siteClient);
    }

    @Test
    void testSynchronize_OnlyUnknownBuilds()
    {
        features.disableFeature(SYNC_ALL_BUILDS);
        when(jobService.save(any(Job.class))).then(invocation -> invocation.getArgument(0));

        Site site = createSite();
        Job job = createJob(site).setLastBuild(10);
        Build build1 = createBuild(job).setNumber(10);
        Build build2 = createBuild(job).setNumber(11)
                .setId(null);

        when(siteClient.getRemoteStatus(eq(job))).thenReturn(Status.ONLINE);
        when(siteClient.populateJobDetails(job)).then(invocation -> {
            ((Job) invocation.getArgument(0)).setBuilds(asList(build1, build2))
                    .setOldestBuild(10);
            return true;
        });
        when(buildService.getAllInRange(job, 10, 11)).thenReturn(emptySet());
        when(result.triggerReason()).thenReturn(new JobSyncTrigger(""));
        when(result.triggerReason(any())).thenReturn(Optional.empty());

        SynchronizationContext<Job> synchronizationContext = createSynchronizationContext(job);
        operation.synchronize(synchronizationContext);

        verify(jobService).save(job);
        verify(synchronizationService, never()).synchronize(eq(build1), any(UpstreamTrigger.class));
        verify(synchronizationService).synchronize(eq(build2), any(UpstreamTrigger.class));

        verify(buildService).markAllInJobAsDeleted(job, 10);

        verify(result).started();
        verify(result).finished();

        verify(eventPublisher).publish(any(JobSynchronizedEvent.class));

        verify(siteClient).getRemoteStatus(job);
        verify(siteClient).populateJobDetails(job);
        verify(siteClient).registerListener(any(SiteResponseListener.class));
        verify(siteClient).unregisterListener(any(SiteResponseListener.class));
        verifyNoMoreInteractions(siteClient);
    }

    @Test
    void testSynchronize_OnlyUnknownBuilds_RebuildCache()
    {
        features.disableFeature(SYNC_ALL_BUILDS);
        when(jobService.save(any(Job.class))).then(invocation -> invocation.getArgument(0));

        Site site = createSite();
        Job job = createJob(site).setLastBuild(0);
        Build build1 = createBuild(job).setNumber(10)
                .setDeleted(true);
        Build build2 = createBuild(job).setNumber(11)
                .setId(null);

        when(siteClient.getRemoteStatus(eq(job))).thenReturn(Status.ONLINE);
        when(siteClient.populateJobDetails(job)).then(invocation -> {
            ((Job) invocation.getArgument(0)).setBuilds(asList(build1, build2))
                    .setOldestBuild(10);
            return true;
        });
        when(buildService.getAllInRange(job, 10, 11)).thenReturn(emptySet());
        when(result.triggerReason()).thenReturn(new JobSyncTrigger(""));
        when(result.triggerReason(any())).thenReturn(Optional.empty());

        SynchronizationContext<Job> synchronizationContext = createSynchronizationContext(job);
        operation.synchronize(synchronizationContext);

        verify(jobService).save(job);
        verify(synchronizationService).synchronize(eq(build1), any(UpstreamTrigger.class));
        verify(synchronizationService).synchronize(eq(build2), any(UpstreamTrigger.class));

        verify(buildService).markAllInJobAsDeleted(job, 10);

        verify(result).started();
        verify(result).finished();

        verify(eventPublisher).publish(any(JobSynchronizedEvent.class));

        verify(siteClient).getRemoteStatus(job);
        verify(siteClient).populateJobDetails(job);
        verify(siteClient).registerListener(any(SiteResponseListener.class));
        verify(siteClient).unregisterListener(any(SiteResponseListener.class));
        verifyNoMoreInteractions(siteClient);
    }

    @Test
    void testSynchronize_JobDeleted()
    {
        when(jobService.save(any(Job.class))).then(invocation -> invocation.getArgument(0));
        Site site = createSite();
        Job job = createJob(site);

        when(siteClient.getRemoteStatus(eq(job))).thenReturn(Status.ONLINE);
        when(siteClient.populateJobDetails(job)).then(invocation -> {
            ((Job) invocation.getArgument(0)).setDeleted(true);
            return true;
        });

        operation.synchronize(createSynchronizationContext(job));

        assertThat(job.isDeleted()).isTrue();
        verify(jobService).save(job);

        verify(result).started();
        verify(result).finished();

        verify(eventPublisher).publish(any(JobSynchronizedEvent.class));

        verify(siteClient).getRemoteStatus(job);
        verify(siteClient).populateJobDetails(job);
        verify(siteClient).registerListener(any(SiteResponseListener.class));
        verify(siteClient).unregisterListener(any(SiteResponseListener.class));
        verifyNoMoreInteractions(siteClient);
    }

    @Test
    void testSynchronize_NoBuildsOrJobs()
    {
        when(jobService.save(any(Job.class))).then(invocation -> invocation.getArgument(0));
        Site site = createSite();
        Job job = createJob(site);

        when(siteClient.getRemoteStatus(eq(job))).thenReturn(Status.ONLINE);
        when(siteClient.populateJobDetails(job)).then(invocation -> {
            ((Job) invocation.getArgument(0)).setOldestBuild(10);
            return true;
        });
        when(result.triggerReason()).thenReturn(new JobSyncTrigger(""));
        when(result.triggerReason(any())).thenReturn(Optional.empty());

        operation.synchronize(createSynchronizationContext(job));

        verify(jobService).save(job);
        verifyNoMoreInteractions(jobService);
        verify(buildService).markAllInJobAsDeleted(job, 10);

        verify(result).started();
        verify(result).finished();

        verify(eventPublisher).publish(any(JobSynchronizedEvent.class));

        verify(siteClient).getRemoteStatus(job);
        verify(siteClient).populateJobDetails(job);
        verify(siteClient).registerListener(any(SiteResponseListener.class));
        verify(siteClient).unregisterListener(any(SiteResponseListener.class));
        verifyNoMoreInteractions(siteClient);
    }

    @Test
    void testSynchronize_DuplicateBuilds()
    {
        when(jobService.save(any(Job.class))).then(invocation -> invocation.getArgument(0));
        Site site = createSite();
        Job job = createJob(site);
        Build build1 = createBuild(job).setNumber(10);
        Build build1a = createBuild(job).setNumber(10);
        Build build2 = createBuild(job).setNumber(11);

        when(siteClient.getRemoteStatus(eq(job))).thenReturn(Status.ONLINE);
        when(siteClient.populateJobDetails(job)).then(invocation -> {
            ((Job) invocation.getArgument(0)).setBuilds(asList(build1, build2))
                    .setOldestBuild(10);
            return true;
        });
        when(buildService.getAllInRange(job, 10, 11)).thenReturn(new HashSet<>(asList(build1, build1a, build2)));
        when(result.triggerReason()).thenReturn(new JobSyncTrigger(""));
        when(result.triggerReason(any())).thenReturn(Optional.empty());

        SynchronizationContext<Job> synchronizationContext = createSynchronizationContext(job);
        operation.synchronize(synchronizationContext);

        verify(jobService).save(job);

        verify(buildService).getAllInRange(job, 10, 11);
        ArgumentCaptor<Build> argumentCaptor = ArgumentCaptor.forClass(Build.class);
        verify(buildService).delete(argumentCaptor.capture());
        assertThat(argumentCaptor.getAllValues()).hasSize(1);
        Build deleted = argumentCaptor.getValue();
        if (Objects.equals(build1, deleted))
        {
            verify(buildService).delete(build1);
            verify(synchronizationService).synchronize(eq(build1a), any(UpstreamTrigger.class));
            verify(synchronizationService).synchronize(eq(build2), any(UpstreamTrigger.class));
        }
        else
        {
            verify(buildService).delete(build1a);
            verify(synchronizationService).synchronize(eq(build1), any(UpstreamTrigger.class));
            verify(synchronizationService).synchronize(eq(build2), any(UpstreamTrigger.class));
        }
        verify(buildService).markAllInJobAsDeleted(job, 10);

        verify(result).started();
        verify(result).finished();

        verify(eventPublisher).publish(any(JobSynchronizedEvent.class));

        verify(siteClient).getRemoteStatus(job);
        verify(siteClient).populateJobDetails(job);
        verify(siteClient).registerListener(any(SiteResponseListener.class));
        verify(siteClient).unregisterListener(any(SiteResponseListener.class));
        verifyNoMoreInteractions(siteClient);
    }

    @Test
    void testSynchronize_BackfillOlderAvailableBuilds()
    {
        when(jobService.save(any(Job.class))).then(invocation -> invocation.getArgument(0));

        Site site = createSite();
        Job job = createJob(site);
        Build build1 = createBuild(job).setNumber(10);
        Build build2 = createBuild(job).setNumber(11);

        when(siteClient.getRemoteStatus(eq(job))).thenReturn(Status.ONLINE);
        when(siteClient.populateJobDetails(job)).then(invocation -> {
            ((Job) invocation.getArgument(0)).setBuilds(asList(build1, build2))
                    .setOldestBuild(6);
            return true;
        });
        when(buildService.getAllInRange(job, 6, 11)).thenReturn(emptySet());
        when(result.triggerReason()).thenReturn(new JobSyncTrigger(""));
        when(result.triggerReason(any())).thenReturn(Optional.empty());

        SynchronizationContext<Job> synchronizationContext = createSynchronizationContext(job);
        operation.synchronize(synchronizationContext);

        verify(jobService).save(job);
        verify(synchronizationService, times(6)).synchronize(any(Build.class), any(UpstreamTrigger.class));

        verify(buildService).markAllInJobAsDeleted(job, 6);

        verify(result).started();
        verify(result).finished();

        verify(eventPublisher).publish(any(JobSynchronizedEvent.class));

        verify(siteClient).getRemoteStatus(job);
        verify(siteClient).populateJobDetails(job);
        verify(siteClient).registerListener(any(SiteResponseListener.class));
        verify(siteClient).unregisterListener(any(SiteResponseListener.class));
        verifyNoMoreInteractions(siteClient);
    }

    @Test
    void testLocateSyncable()
    {
        Job job = createSyncable();

        when(jobService.get(job.getId())).thenReturn(Optional.of(job));

        assertThat(operation.locateSyncable(job.getId(), job.getOperationId())
                .orElse(null)).isSameAs(job);
        assertThat(operation.locateSyncable(null, job.getOperationId())
                .orElse(null)).isSameAs(job);
        assertThat(operation.locateSyncable(job.getId(), null)
                .orElse(null)).isSameAs(job);
    }
}
