/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.synchronization;

import java.io.*;
import java.util.*;

import org.marvelution.jji.data.services.api.*;
import org.marvelution.jji.events.*;
import org.marvelution.jji.model.*;
import org.marvelution.jji.utils.*;

import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.module.jaxb.*;
import org.junit.jupiter.api.*;
import org.mockito.*;

import static java.util.Optional.*;
import static java.util.stream.Collectors.*;
import static org.assertj.core.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

class BuildSynchronizationOperationTest
		extends AbstractSynchronizationOperationTest<Build, BuildSynchronizationOperation>
{

	@Mock
	private IssueLinkService issueLinkService;
	@Captor
	private ArgumentCaptor<Set<IssueReference>> issueCapture;

	@Override
	protected BuildSynchronizationOperation createOperation()
	{
		return new BuildSynchronizationOperation(buildService, issueLinkService, jobService);
	}

	@Override
	protected Build createSyncable()
	{
		return createBuild(createJob(createSite()));
	}

	@Test
	void testSynchronize()
	{
		when(buildService.save(any(Build.class))).then(invocation -> invocation.getArgument(0));

		Build build = createSyncable().setResult(Result.SUCCESS);
		build.setDescription("BUILD-1");
		build.getJob().setDescription("JOB-1");
		build.getJob().setUrlName("JOB-2");

		when(siteClient.getRemoteStatus(eq(build))).thenReturn(Status.ONLINE);
		when(buildService.get(eq(build.getJob()), eq(build.getNumber()))).thenReturn(empty());
		when(issueLinkService.relink(eq(build), anySet())).thenAnswer(invocation -> {
			Set<String> keys = invocation.getArgument(1);
			return keys.stream()
					.map(key -> new IssueReference().setIssueKey(key).setProjectKey(KeyExtractor.extractProjectKeyFromIssueKey(key)))
					.collect(toSet());
		});
		when(siteClient.populateJobDetails(eq(build.getJob()))).thenReturn(true);
		when(siteClient.populateBuildDetails(eq(build))).thenReturn(true);

		operation.synchronize(createSynchronizationContext(build));

		verify(result).linkedIssues(issueCapture.capture());
		Set<IssueReference> issues = issueCapture.getValue();
		assertThat(issues).hasSize(3)
				.contains(new IssueReference().setIssueKey("BUILD-1").setProjectKey("BUILD"),
				          new IssueReference().setIssueKey("JOB-1").setProjectKey("JOB"),
				          new IssueReference().setIssueKey("JOB-2").setProjectKey("JOB"));

		ArgumentCaptor<Job> jobCapture = ArgumentCaptor.forClass(Job.class);
		verify(jobService).save(jobCapture.capture());
		assertThat(jobCapture.getValue().isDeleted()).isFalse();
		assertThat(jobCapture.getValue().getLastBuild()).isEqualTo(build.getNumber());

		ArgumentCaptor<Build> buildCaptor = ArgumentCaptor.forClass(Build.class);
		verify(buildService).save(buildCaptor.capture());
		assertThat(buildCaptor.getValue().isDeleted()).isFalse();

		verify(eventPublisher).publish(any(BuildSynchronizedEvent.class));

		verify(siteClient).getRemoteStatus(build);
		verify(siteClient).populateJobDetails(build.getJob());
		verify(siteClient).populateBuildDetails(build);
		verify(siteClient).registerListener(any(SiteResponseListener.class));
		verify(siteClient).unregisterListener(any(SiteResponseListener.class));
		verifyNoMoreInteractions(siteClient);
	}

	@Test
	@SuppressWarnings("unchecked")
	void testSynchronize_UpdatedJob()
			throws IOException
	{
		Build build = createSyncable().setResult(Result.SUCCESS);
		build.setDescription("BUILD-1");
		build.getJob().setDescription("JOB-1");

		// Make a copy to simulate that the updated job is not yet persisted
		ObjectMapper objectMapper = new ObjectMapper().registerModule(new JaxbAnnotationModule());
		Build savedCopy = objectMapper.readValue(objectMapper.writeValueAsString(build), Build.class);

		when(buildService.save(any(Build.class))).then(invocation -> savedCopy);

		when(siteClient.getRemoteStatus(eq(build))).thenReturn(Status.ONLINE);
		when(buildService.get(eq(build.getJob()), eq(build.getNumber()))).thenReturn(empty());
		when(issueLinkService.relink(eq(build), anySet())).thenAnswer(invocation -> {
			Set<String> keys = invocation.getArgument(1);
			return keys.stream()
					.map(key -> new IssueReference().setIssueKey(key).setProjectKey(KeyExtractor.extractProjectKeyFromIssueKey(key)))
					.collect(toSet());
		});
		when(siteClient.populateJobDetails(eq(build.getJob()))).then(invocation -> {
			Job job = invocation.getArgument(0);
			job.setDescription("JOB-2");
			return true;
		});
		when(siteClient.populateBuildDetails(eq(build))).thenReturn(true);

		operation.synchronize(createSynchronizationContext(build));

		verify(result).linkedIssues(issueCapture.capture());
		Set<IssueReference> issues = issueCapture.getValue();
		assertThat(issues).hasSize(2)
				.contains(new IssueReference().setIssueKey("BUILD-1").setProjectKey("BUILD"),
				          new IssueReference().setIssueKey("JOB-2").setProjectKey("JOB"));

		ArgumentCaptor<Job> jobCapture = ArgumentCaptor.forClass(Job.class);
		verify(jobService).save(jobCapture.capture());
		assertThat(jobCapture.getValue().isDeleted()).isFalse();
		assertThat(jobCapture.getValue().getLastBuild()).isEqualTo(build.getNumber());

		ArgumentCaptor<Build> buildCaptor = ArgumentCaptor.forClass(Build.class);
		verify(buildService).save(buildCaptor.capture());
		assertThat(buildCaptor.getValue().isDeleted()).isFalse();

		verify(eventPublisher).publish(any(BuildSynchronizedEvent.class));

		verify(siteClient).getRemoteStatus(build);
		verify(siteClient).populateJobDetails(build.getJob());
		verify(siteClient).populateBuildDetails(build);
		verify(siteClient).registerListener(any(SiteResponseListener.class));
		verify(siteClient).unregisterListener(any(SiteResponseListener.class));
		verifyNoMoreInteractions(siteClient);
	}

	@Test
	void testSynchronize_ExistingBuild()
	{
		when(buildService.save(any(Build.class))).then(invocation -> invocation.getArgument(0));
		Build build = setId(createSyncable()).setResult(Result.FAILURE);
		build.getJob().setLastBuild(build.getNumber() + 1);

		when(siteClient.getRemoteStatus(eq(build))).thenReturn(Status.ONLINE);
		when(buildService.get(eq(build.getJob()), eq(build.getNumber()))).thenReturn(Optional.of(build));
		when(siteClient.populateJobDetails(eq(build.getJob()))).thenReturn(true);
		when(siteClient.populateBuildDetails(eq(build))).thenReturn(true);

		operation.synchronize(createSynchronizationContext(build));

		ArgumentCaptor<Job> jobCapture = ArgumentCaptor.forClass(Job.class);
		verify(jobService).save(jobCapture.capture());
		assertThat(jobCapture.getValue().isDeleted()).isFalse();
		assertThat(jobCapture.getValue().getLastBuild()).isEqualTo(build.getNumber() + 1);

		ArgumentCaptor<Build> buildCaptor = ArgumentCaptor.forClass(Build.class);
		verify(buildService).save(buildCaptor.capture());
		assertThat(buildCaptor.getValue().isDeleted()).isFalse();

		verify(eventPublisher).publish(any(BuildSynchronizedEvent.class));

		verify(siteClient).getRemoteStatus(build);
		verify(siteClient).populateJobDetails(build.getJob());
		verify(siteClient).populateBuildDetails(build);
		verify(siteClient).registerListener(any(SiteResponseListener.class));
		verify(siteClient).unregisterListener(any(SiteResponseListener.class));
		verifyNoMoreInteractions(siteClient);
	}

	@Test
	void testSynchronize_BuildSyncError()
	{
		Build build = createSyncable();
		RuntimeException error = new RuntimeException();

		when(siteClient.getRemoteStatus(eq(build))).thenReturn(Status.ONLINE);
		when(buildService.get(eq(build.getJob()), eq(build.getNumber()))).thenReturn(empty());
		when(siteClient.populateJobDetails(eq(build.getJob()))).thenReturn(true);
		when(siteClient.populateBuildDetails(eq(build))).thenThrow(error);

		operation.synchronize(createSynchronizationContext(build));

		verify(result).addError(error);

		ArgumentCaptor<Job> argumentCaptor = ArgumentCaptor.forClass(Job.class);
		verify(jobService).save(argumentCaptor.capture());
		assertThat(argumentCaptor.getValue().isDeleted()).isFalse();

		verify(buildService, never()).save(eq(build));

		verify(eventPublisher).publish(any(BuildSynchronizedEvent.class));

		verify(siteClient).getRemoteStatus(build);
		verify(siteClient).populateJobDetails(build.getJob());
		verify(siteClient).populateBuildDetails(build);
		verify(siteClient).registerListener(any(SiteResponseListener.class));
		verify(siteClient).unregisterListener(any(SiteResponseListener.class));
		verifyNoMoreInteractions(siteClient);
	}

	@Test
	void testSynchronize_MissingResult()
	{
		Build build = createSyncable().setId(null);

		when(siteClient.getRemoteStatus(eq(build))).thenReturn(Status.ONLINE);
		when(buildService.get(eq(build.getJob()), eq(build.getNumber()))).thenReturn(empty());
		when(siteClient.populateJobDetails(eq(build.getJob()))).thenReturn(true);
		when(siteClient.populateBuildDetails(eq(build))).thenReturn(true);

		operation.synchronize(createSynchronizationContext(build));

		ArgumentCaptor<Job> argumentCaptor = ArgumentCaptor.forClass(Job.class);
		verify(jobService).save(argumentCaptor.capture());
		assertThat(argumentCaptor.getValue().isDeleted()).isFalse();

		verify(buildService, never()).save(eq(build));

		verify(eventPublisher).publish(any(BuildSynchronizedEvent.class));

		verify(siteClient).getRemoteStatus(build);
		verify(siteClient).populateJobDetails(build.getJob());
		verify(siteClient).populateBuildDetails(build);
		verify(siteClient).registerListener(any(SiteResponseListener.class));
		verify(siteClient).unregisterListener(any(SiteResponseListener.class));
		verifyNoMoreInteractions(siteClient);
	}

	@Test
	void testSynchronize_NewBuildDeleted()
	{
		Build build = createSyncable().setId(null);

		when(siteClient.getRemoteStatus(eq(build))).thenReturn(Status.ONLINE);
		when(buildService.get(eq(build.getJob()), eq(build.getNumber()))).thenReturn(empty());
		when(siteClient.populateJobDetails(eq(build.getJob()))).thenReturn(true);
		when(siteClient.populateBuildDetails(eq(build))).then(invocation -> {
			((Build) invocation.getArgument(0)).setDeleted(true);
			return true;
		});

		operation.synchronize(createSynchronizationContext(build));

		ArgumentCaptor<Job> argumentCaptor = ArgumentCaptor.forClass(Job.class);
		verify(jobService).save(argumentCaptor.capture());
		assertThat(argumentCaptor.getValue().isDeleted()).isFalse();

		verify(buildService, never()).save(eq(build));

		verify(eventPublisher).publish(any(BuildSynchronizedEvent.class));

		verify(siteClient).getRemoteStatus(build);
		verify(siteClient).populateJobDetails(build.getJob());
		verify(siteClient).populateBuildDetails(build);
		verify(siteClient).registerListener(any(SiteResponseListener.class));
		verify(siteClient).unregisterListener(any(SiteResponseListener.class));
		verifyNoMoreInteractions(siteClient);
	}

	@Test
	void testSynchronize_ExistingBuildDeleted()
	{
		when(buildService.save(any(Build.class))).then(invocation -> invocation.getArgument(0));
		Build build = createSyncable();

		when(siteClient.getRemoteStatus(eq(build))).thenReturn(Status.ONLINE);
		when(buildService.get(eq(build.getJob()), eq(build.getNumber()))).thenReturn(Optional.of(build));
		when(siteClient.populateJobDetails(eq(build.getJob()))).thenReturn(true);
		when(siteClient.populateBuildDetails(eq(build))).then(invocation -> {
			((Build) invocation.getArgument(0)).setDeleted(true);
			return true;
		});

		operation.synchronize(createSynchronizationContext(build));

		ArgumentCaptor<Job> jobCapture = ArgumentCaptor.forClass(Job.class);
		verify(jobService).save(jobCapture.capture());
		assertThat(jobCapture.getValue().isDeleted()).isFalse();

		ArgumentCaptor<Build> buildCaptor = ArgumentCaptor.forClass(Build.class);
		verify(buildService).save(buildCaptor.capture());
		assertThat(buildCaptor.getValue().isDeleted()).isTrue();

		verify(eventPublisher).publish(any(BuildSynchronizedEvent.class));

		verify(siteClient).getRemoteStatus(build);
		verify(siteClient).populateJobDetails(build.getJob());
		verify(siteClient).populateBuildDetails(build);
		verify(siteClient).registerListener(any(SiteResponseListener.class));
		verify(siteClient).unregisterListener(any(SiteResponseListener.class));
		verifyNoMoreInteractions(siteClient);
	}

	@Test
	void testSynchronize_JobDeleted()
	{
		Build build = createSyncable();

		when(siteClient.getRemoteStatus(eq(build))).thenReturn(Status.ONLINE);
		when(buildService.get(eq(build.getJob()), eq(build.getNumber()))).thenReturn(empty());
		when(siteClient.populateJobDetails(eq(build.getJob()))).then(invocation -> {
			((Job) invocation.getArgument(0)).setDeleted(true);
			return true;
		});

		operation.synchronize(createSynchronizationContext(build));

		ArgumentCaptor<Job> argumentCaptor = ArgumentCaptor.forClass(Job.class);
		verify(jobService).save(argumentCaptor.capture());
		assertThat(argumentCaptor.getValue().isDeleted()).isTrue();

		verify(eventPublisher).publish(any(BuildSynchronizedEvent.class));

		verify(siteClient).getRemoteStatus(build);
		verify(siteClient).populateJobDetails(build.getJob());
		verify(siteClient).registerListener(any(SiteResponseListener.class));
		verify(siteClient).unregisterListener(any(SiteResponseListener.class));
		verifyNoMoreInteractions(siteClient);
	}

	@Test
	void testLocateSyncable_ById()
	{
		Build build = createSyncable();

		when(buildService.get(build.getId())).thenReturn(Optional.of(build));

		assertThat(operation.locateSyncable(build.getId(), null).orElse(null)).isSameAs(build);
		assertThat(operation.locateSyncable(build.getId(), build.getOperationId()).orElse(null)).isSameAs(build);
	}

	@Test
	void testLocateSyncable_ByOperationId()
	{
		Build build = createSyncable();

		when(jobService.get(build.getJob().getId())).thenReturn(Optional.of(build.getJob()));

		assertThat(operation.locateSyncable(null, build.getOperationId()).orElse(null)).isEqualTo(
				new Build().setJob(build.getJob()).setNumber(build.getNumber()));
	}
}
