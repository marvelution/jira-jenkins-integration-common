/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.synchronization;

import java.util.*;

import org.marvelution.jji.data.services.api.*;
import org.marvelution.jji.data.synchronization.api.trigger.*;
import org.marvelution.jji.events.*;
import org.marvelution.jji.model.*;

import org.junit.jupiter.api.*;

import static java.util.Arrays.*;
import static org.assertj.core.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

class SiteSynchronizationOperationTest
		extends AbstractSynchronizationOperationTest<Site, SiteSynchronizationOperation>
{

	@Override
	protected SiteSynchronizationOperation createOperation()
	{
		return new SiteSynchronizationOperation(siteClient, jobService, siteService);
	}

	@Override
	protected Site createSyncable()
	{
		return createSite();
	}

	@Test
	void testSynchronize()
	{
		when(siteService.save(any(Site.class))).then(invocation -> invocation.getArgument(0));
		when(jobService.save(any(Job.class))).then(invocation -> invocation.getArgument(0));

		Site site = createSite().setAutoLinkNewJobs(true);
		Job job1 = createJob(site);
		Job job2 = createJob(site).setLastBuild(10);
		Job job3 = createJob(site);
		Job job4 = createJob(site).setName(job2.getName());

		when(siteClient.getRemoteStatus(eq((Syncable<?>) site))).thenReturn(Status.ONLINE);
		when(siteClient.isCrumbSecurityEnabled(eq(site))).thenReturn(true);
		when(siteClient.isJenkinsPluginInstalled(eq(site))).thenReturn(true);
		when(siteClient.getJobs(eq(site))).thenReturn(asList(job1, job2));
		when(jobService.getRootJobsOnSite(eq(site))).thenReturn(asList(job2, job3, job4));

		operation.synchronize(createSynchronizationContext(site));

		verify(siteClient).registerListener(any(SiteResponseListener.class));
		verify(siteClient).unregisterListener(any(SiteResponseListener.class));
		verifyNoMoreInteractions(siteClient);

		assertThat(site.isUseCrumbs()).isTrue();
		assertThat(site.isJenkinsPluginInstalled()).isTrue();
		verify(siteService).save(site);

		verify(jobService).save(job1);
		verify(jobService).save(job2);
		verify(jobService, never()).save(job3);
		verify(jobService, never()).save(job4);
		verify(jobService).delete(job4);
		verify(result, never()).synchronizedJob(any(Job.class));
		verify(synchronizationService).synchronize(eq(job1), any(UpstreamTrigger.class));
		verify(synchronizationService, never()).synchronize(eq(job2), any());
		verify(synchronizationService, never()).synchronize(eq(job3), any());
		verify(synchronizationService, never()).synchronize(eq(job4), any());

		verify(result).started();
		verify(result).finished();

		verify(eventPublisher).publish(any(SiteSynchronizedEvent.class));
	}

	@Test
	void testSynchronize_AddOnNotInstalled()
	{
		when(siteService.save(any(Site.class))).then(invocation -> invocation.getArgument(0));
		when(jobService.save(any(Job.class))).then(invocation -> invocation.getArgument(0));

		Site site = createSite();
		Job job1 = createJob(site);
		Job job2 = createJob(site).setLastBuild(10);
		Job job3 = createJob(site);

		when(siteClient.getRemoteStatus(eq((Syncable<?>) site))).thenReturn(Status.ONLINE);
		when(siteClient.isCrumbSecurityEnabled(eq(site))).thenReturn(true);
		when(siteClient.isJenkinsPluginInstalled(eq(site))).thenReturn(false);
		when(siteClient.getJobs(eq(site))).thenReturn(asList(job1, job2));
		when(jobService.getRootJobsOnSite(eq(site))).thenReturn(asList(job2, job3));

		operation.synchronize(createSynchronizationContext(site));

		verify(siteClient).registerListener(any(SiteResponseListener.class));
		verify(siteClient).unregisterListener(any(SiteResponseListener.class));
		verifyNoMoreInteractions(siteClient);

		assertThat(site.isUseCrumbs()).isTrue();
		assertThat(site.isJenkinsPluginInstalled()).isFalse();
		verify(siteService).save(site);

		verify(jobService).save(job1);
		verify(jobService).save(job2);
		verify(jobService, never()).save(job3);
		verify(result, never()).synchronizedJob(any(Job.class));
		verify(synchronizationService).synchronize(eq(job1), any(UpstreamTrigger.class));
		verify(synchronizationService).synchronize(eq(job2), any(UpstreamTrigger.class));
		verify(synchronizationService, never()).synchronize(eq(job3), any());

		verify(result).started();
		verify(result).finished();

		verify(eventPublisher).publish(any(SiteSynchronizedEvent.class));
	}

	@Test
	void testSynchronize_PluginStateUnchanged()
	{
		when(siteService.save(any(Site.class))).then(invocation -> invocation.getArgument(0));
		when(jobService.save(any(Job.class))).then(invocation -> invocation.getArgument(0));

		Site site = createSite().setJenkinsPluginInstalled(true);
		Job job1 = createJob(site);
		Job job2 = createJob(site);
		Job job3 = createJob(site);

		when(siteClient.getRemoteStatus(eq((Syncable<?>) site))).thenReturn(Status.ONLINE);
		when(siteClient.isCrumbSecurityEnabled(eq(site))).thenReturn(true);
		when(siteClient.isJenkinsPluginInstalled(eq(site))).thenReturn(true);
		when(siteClient.getJobs(eq(site))).thenReturn(asList(job1, job2));
		when(jobService.getRootJobsOnSite(eq(site))).thenReturn(asList(job2, job3));

		operation.synchronize(createSynchronizationContext(site));

		verify(siteClient).registerListener(any(SiteResponseListener.class));
		verify(siteClient).unregisterListener(any(SiteResponseListener.class));
		verifyNoMoreInteractions(siteClient);

		assertThat(site.isUseCrumbs()).isTrue();
		assertThat(site.isJenkinsPluginInstalled()).isTrue();
		verify(siteService).save(site);

		verify(jobService).save(job1);
		verify(jobService).save(job2);
		verify(jobService, never()).save(job3);
		verify(result, never()).synchronizedJob(any(Job.class));
		verify(synchronizationService).synchronize(eq(job1), any(UpstreamTrigger.class));
		verify(synchronizationService).synchronize(eq(job2), any(UpstreamTrigger.class));
		verify(synchronizationService, never()).synchronize(eq(job3), any());

		verify(result).started();
		verify(result).finished();

		verify(eventPublisher).publish(any(SiteSynchronizedEvent.class));
	}

	@Test
	void testLocateSyncable()
	{
		Site site = createSyncable();

		when(siteService.get(site.getId())).thenReturn(Optional.of(site));

		assertThat(operation.locateSyncable(site.getId(), site.getOperationId()).orElse(null)).isSameAs(site);
		assertThat(operation.locateSyncable(null, site.getOperationId()).orElse(null)).isSameAs(site);
		assertThat(operation.locateSyncable(site.getId(), null).orElse(null)).isSameAs(site);
	}
}
