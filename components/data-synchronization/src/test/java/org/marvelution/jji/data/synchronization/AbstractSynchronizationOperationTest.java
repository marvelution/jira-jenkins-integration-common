/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.synchronization;

import java.net.*;
import java.util.*;
import java.util.concurrent.atomic.*;

import org.marvelution.jji.api.events.*;
import org.marvelution.jji.data.services.api.*;
import org.marvelution.jji.data.synchronization.api.*;
import org.marvelution.jji.data.synchronization.api.context.*;
import org.marvelution.jji.data.synchronization.api.result.*;
import org.marvelution.jji.events.*;
import org.marvelution.jji.model.*;
import org.marvelution.testing.*;

import org.junit.jupiter.api.*;
import org.mockito.*;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

/**
 * Base tests for {@link SynchronizationOperation} implementations.
 *
 * @author Mark Rekveld
 * @since 1.4.0
 */
abstract class AbstractSynchronizationOperationTest<S extends Syncable<S>, O extends AbstractSynchronizationOperation<S>>
		extends TestSupport
{

	private AtomicInteger jobCounter = new AtomicInteger(1);
	private AtomicInteger buildCounter = new AtomicInteger(1);
	@Mock
	SiteClient siteClient;
	@Mock
	protected SiteService siteService;
	@Mock
	protected JobService jobService;
	@Mock
	BuildService buildService;
	@Mock(answer = Answers.RETURNS_DEEP_STUBS)
	SynchronizationService synchronizationService;
	@Mock
	EventPublisher eventPublisher;
	@Mock(answer = Answers.RETURNS_DEEP_STUBS)
	SynchronizationResult result;
	O operation;
	private SynchronizationContextBuilder<?, ?> synchronizationContextBuilder;

	@BeforeEach
	void setUp()
	{
		operation = createOperation();
		synchronizationContextBuilder = new SynchronizationContextBuilder<>(synchronizationService, eventPublisher, siteClient);
	}

	protected abstract O createOperation();

	protected abstract S createSyncable();

	Site createSite()
	{
		return setId(new Site().setRpcUrl(URI.create("http://localhost:8080")).setName(getTestMethodName()));
	}

	Job createJob(Site site)
	{
		return setId(new Job().setSite(site).setName("job" + jobCounter.getAndIncrement()));
	}

	Build createBuild(Job job)
	{
		return setId(new Build().setJob(job).setNumber(buildCounter.getAndIncrement()));
	}

	<E extends HasId<E>> E setId(E entity)
	{
		return entity.setId(UUID.randomUUID().toString().replace("-", ""));
	}

	SynchronizationContext<S> createSynchronizationContext(S syncable)
	{
		return synchronizationContextBuilder.syncable(syncable).result(result).build();
	}

	@Test
	void testSupports()
	{
		assertThat(operation.supports(createSyncable().getOperationId()), is(true));
	}

	@Test
	void testSynchronize_AddErrorsToProgress()
	{
		S syncable = createSyncable();
		RuntimeException error = new RuntimeException();
		when(siteClient.getRemoteStatus(eq(syncable))).thenThrow(error);

		operation.synchronize(createSynchronizationContext(syncable));

		verify(siteClient).getRemoteStatus(syncable);
		verify(siteClient).registerListener(any(SiteResponseListener.class));
		verify(siteClient).unregisterListener(any(SiteResponseListener.class));
		verifyNoMoreInteractions(siteClient);
		verify(result).started();
		verify(result).finished();
		verify(result).addError(error);
		verify(eventPublisher).publish(any(AbstractSynchronizedEvent.class));
	}

	@Test
	void testSynchronize_StopRequested()
	{
		when(result.stopRequested()).thenReturn(true);

		operation.synchronize(createSynchronizationContext(createSyncable()));

		verifyNoMoreInteractions(siteClient);
		verify(result, never()).started();
	}

	@Test
	void testSynchronize_RemoteNotAccessible()
	{
		S syncable = createSyncable();

		when(siteClient.getRemoteStatus(eq(syncable))).thenReturn(Status.NOT_ACCESSIBLE);
		operation.synchronize(createSynchronizationContext(syncable));

		verify(siteClient).getRemoteStatus(syncable);
		verify(siteClient).registerListener(any(SiteResponseListener.class));
		verify(siteClient).unregisterListener(any(SiteResponseListener.class));
		verifyNoMoreInteractions(siteClient);
		verify(result).started();
		verify(result).finished();
		verify(eventPublisher).publish(any(AbstractSynchronizedEvent.class));
	}

	@Test
	void testSynchronize_RemoteOffline()
	{
		S syncable = createSyncable();
		when(siteClient.getRemoteStatus(eq(syncable))).thenReturn(Status.OFFLINE);

		operation.synchronize(createSynchronizationContext(syncable));

		verify(siteClient).getRemoteStatus(syncable);
		verify(siteClient).registerListener(any(SiteResponseListener.class));
		verify(siteClient).unregisterListener(any(SiteResponseListener.class));
		verifyNoMoreInteractions(siteClient);
		verify(result).started();
		verify(result).finished();
		verify(eventPublisher).publish(any(AbstractSynchronizedEvent.class));
	}
}
