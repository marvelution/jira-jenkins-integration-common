/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.synchronization.api;

import java.util.Optional;
import javax.annotation.Nullable;

import org.marvelution.jji.data.synchronization.api.context.*;
import org.marvelution.jji.model.OperationId;
import org.marvelution.jji.model.Syncable;

/**
 * Synchronization Operation interface.
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public interface SynchronizationOperation<S extends Syncable<S>> {

	/**
	 * Returns whether this operation supports the specified {@link OperationId}.
	 *
	 * @since 1.4.0
	 */
	boolean supports(OperationId operationId);

	/**
	 * Locate the {@link Syncable} by either the specified {@code id} or the specified {@link OperationId}.
	 *
	 * @since 1.6.0
	 */
	Optional<S> locateSyncable(@Nullable String id, OperationId operationId);

	/**
	 * Execute the Synchronization Operation.
	 *
	 * @throws SynchronizationOperationException in case of synchronization errors.
	 */
	void synchronize(SynchronizationContext<S> context) throws SynchronizationOperationException;
}
