/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.helper;

import java.util.*;

import org.marvelution.jji.model.*;

public class PanelDeployment
{

	private final DeploymentEnvironment deploymentEnvironment;
	private final List<PanelBuild> builds;

	public PanelDeployment(
			DeploymentEnvironment deploymentEnvironment,
			List<PanelBuild> builds)
	{
		this.deploymentEnvironment = deploymentEnvironment;
		this.builds = builds;
	}

	public String getId()
	{
		return deploymentEnvironment.getId();
	}

	public String getName()
	{
		return deploymentEnvironment.getName();
	}

	public DeploymentEnvironmentType getType()
	{
		return deploymentEnvironment.getType();
	}

	public List<PanelBuild> getBuilds()
	{
		return builds;
	}

	public PanelBuild getLastBuild()
	{
		return builds.stream().max(Comparator.comparing(PanelBuild::getDate)).orElse(null);
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}
		PanelDeployment that = (PanelDeployment) o;
		return deploymentEnvironment.equals(that.deploymentEnvironment);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(deploymentEnvironment);
	}
}
