/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services;

import java.util.Comparator;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import org.marvelution.jji.data.access.api.BuildDAO;
import org.marvelution.jji.data.access.api.IssueReferenceProvider;
import org.marvelution.jji.data.access.api.IssueToBuildDAO;
import org.marvelution.jji.data.access.api.LinkStatisticsDAO;
import org.marvelution.jji.data.services.api.LinkStatistician;
import org.marvelution.jji.model.Build;
import org.marvelution.jji.model.IssueReference;
import org.marvelution.jji.model.Job;
import org.marvelution.jji.model.LinkStatistics;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toSet;

public class BaseLinkStatistician
        implements LinkStatistician
{

    protected final Logger logger = LoggerFactory.getLogger(BaseLinkStatistician.class);
    private final IssueToBuildDAO issueToBuildDAO;
    private final BuildDAO buildDAO;
    private final LinkStatisticsDAO linkStatisticsDAO;
    private final IssueReferenceProvider issueReferenceProvider;

    public BaseLinkStatistician(
            IssueToBuildDAO issueToBuildDAO,
            BuildDAO buildDAO,
            LinkStatisticsDAO linkStatisticsDAO,
            IssueReferenceProvider issueReferenceProvider)
    {
        this.issueToBuildDAO = issueToBuildDAO;
        this.buildDAO = buildDAO;
        this.linkStatisticsDAO = linkStatisticsDAO;
        this.issueReferenceProvider = issueReferenceProvider;
    }

    @Override
    public Response calculate(Request request)
    {
        long deadLine = System.currentTimeMillis() + request.getTimeLimit();

        Response response = new Response();

        Set<String> existingIssueKeys = issueReferenceProvider.getIssueReferences(request.getIssueKeys())
                .stream()
                .map(IssueReference::getIssueKey)
                .collect(toSet());

        String[] issueKeys = request.getIssueKeys()
                .stream()
                .filter(issueKey -> !existingIssueKeys.contains(issueKey))
                .toArray(String[]::new);
        logger.debug("Clearing link statistics for non-existing issue: {}", String.join(", ", issueKeys));
        linkStatisticsDAO.clearLinkStatistics(issueKeys);

        for (String issueKey : existingIssueKeys)
        {
            try
            {
                long elapsedTime = calculateStatistics(issueKey);
                response.processedIssue(issueKey, elapsedTime);
                if (System.currentTimeMillis() + response.averageProcessingTime() > deadLine)
                {
                    logger.warn("Breaking processing of the next issue as it would likely take longer than we have time for.");
                    break;
                }
            }
            catch (Exception e)
            {
                logger.error("Failed to update statistics for issue {}", issueKey, e);
                response.erroredIssues(issueKey, e);
            }
        }

        if (request.isReindexIssue())
        {
            triggerIssueReindexing(response.getProcessedIssues());
        }

        Set<String> unprocessedIssues = new HashSet<>(existingIssueKeys);
        unprocessedIssues.removeAll(response.getProcessedIssues());
        unprocessedIssues.removeAll(response.getErroredIssues());
        if (!unprocessedIssues.isEmpty())
        {
            response.setUnprocessedIssues(unprocessedIssues);
        }

        logger.info("Issue Stats: processed: {}, errored: {}, unprocessed: {}",
                response.getProcessedIssues()
                        .size(),
                response.getErroredIssues()
                        .size(),
                unprocessedIssues.size());

        return response;
    }

    protected void triggerIssueReindexing(Set<String> issueKeys)
    {}

    private long calculateStatistics(String issueKey)
    {
        long startTime = System.currentTimeMillis();

        LinkStatistics statistics = new LinkStatistics();
        int linkCount = issueToBuildDAO.getLinkCountForIssue(issueKey);
        if (linkCount > 0)
        {
            logger.debug("Indexing {} links for link statistics for {}", linkCount, issueKey);

            Set<Job> jobsForIssue = issueToBuildDAO.getBuildsForIssue(issueKey, linkCount, false)
                    .stream()
                    .collect(groupingBy(Build::getJob))
                    .entrySet()
                    .stream()
                    .map(entry -> entry.getKey()
                            .setBuilds(entry.getValue()))
                    .collect(toSet());

            logger.debug("Indexing {} jobs for link statistics for {}: {}", jobsForIssue.size(), issueKey, jobsForIssue);

            statistics.setTotal(jobsForIssue.size())
                    .setBuildTotal(linkCount);

            jobsForIssue.stream()
                    .flatMap(job -> job.getBuilds()
                            .stream())
                    .collect(groupingBy(Build::getResult, toSet()))
                    .forEach((result, builds) -> statistics.addCountByResult(result, builds.size()));

            Set<Build> buildsForIssue = jobsForIssue.stream()
                    .map(job -> job.getLastBuildObject()
                            .orElseGet(() -> buildDAO.get(job.getId(), job.getLastBuild())))
                    .filter(Objects::nonNull)
                    .collect(toSet());

            buildsForIssue.stream()
                    .max(Comparator.comparing(Build::getResult)
                            .thenComparingLong(Build::getTimestamp))
                    .map(LinkStatistics.BuildStats::forBuild)
                    .ifPresent(statistics::setWorstBuild);

            buildsForIssue.stream()
                    .max(Comparator.comparingLong(Build::getTimestamp))
                    .map(LinkStatistics.BuildStats::forBuild)
                    .ifPresent(statistics::setLatestBuild);

            Set<LinkStatistics.DeployStats> deploymentStats = jobsForIssue.stream()
                    .flatMap(job -> job.getBuilds()
                            .stream())
                    .flatMap(build -> build.getDeploymentEnvironments()
                            .stream()
                            .map(deploymentEnvironment -> LinkStatistics.DeployStats.forDeployment(build, deploymentEnvironment)))
                    .collect(toSet());

            statistics.setDeployTotal(deploymentStats.size());

            deploymentStats.stream()
                    .collect(groupingBy(LinkStatistics.DeployStats::getResult, toSet()))
                    .forEach((result, stats) -> statistics.addDeployCountByResult(result, stats.size()));

            deploymentStats.stream()
                    .max(Comparator.comparing(LinkStatistics.DeployStats::getResult)
                            .thenComparingLong(LinkStatistics.DeployStats::getTimestamp))
                    .ifPresent(statistics::setWorstDeploy);

            deploymentStats.stream()
                    .max(Comparator.comparingLong(LinkStatistics.DeployStats::getTimestamp))
                    .ifPresent(statistics::setLatestDeploy);
        }

        linkStatisticsDAO.storeLinkStatistics(issueKey, statistics);

        long elapsedTime = System.currentTimeMillis() - startTime;
        logger.info("Calculated link statistics for issue {} in {} ms: {}", issueKey, elapsedTime, statistics);
        return elapsedTime;
    }
}
