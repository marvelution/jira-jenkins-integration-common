/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import jakarta.annotation.Nullable;

import org.marvelution.jji.api.text.TextResolver;
import org.marvelution.jji.data.access.api.IssueReferenceProvider;
import org.marvelution.jji.data.services.api.*;
import org.marvelution.jji.model.IssueReference;
import org.marvelution.jji.model.Job;
import org.marvelution.jji.model.request.EvaluateParameterIssueFieldsRequest;
import org.marvelution.jji.model.request.TriggerBuildsRequest;
import org.marvelution.jji.model.response.TriggerBuildsResponse;
import org.marvelution.jji.utils.KeyExtractor;

import com.jayway.jsonpath.DocumentContext;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.util.stream.Collectors.toList;

public abstract class JobTriggerService
{

    private static final Logger LOGGER = LoggerFactory.getLogger(JobTriggerService.class);
    private final ConfigurationService configurationService;
    private final IssueReferenceProvider issueReferenceProvider;
    private final IssueLinkService issueLinkService;
    private final SiteClient siteClient;
    private final JobService jobService;
    private final TextResolver textResolver;
    private final Predicate<Job> triggerableJobFilter;

    protected JobTriggerService(
            ConfigurationService configurationService,
            IssueReferenceProvider issueReferenceProvider,
            IssueLinkService issueLinkService,
            SiteClient siteClient,
            JobService jobService,
            TextResolver textResolver,
            PermissionService<?> permissionService)
    {
        this.configurationService = configurationService;
        this.issueReferenceProvider = issueReferenceProvider;
        this.issueLinkService = issueLinkService;
        this.siteClient = siteClient;
        this.jobService = jobService;
        this.textResolver = textResolver;
        triggerableJobFilter = job -> {
            boolean triggerable = job.isEnabled() && !job.getSite()
                    .isInaccessibleSite() && job.getSite()
                                          .isJenkinsPluginInstalled();
            if (!triggerable)
            {
                return false;
            }
            else
            {
                return permissionService.canTriggerBuilds(job);
            }
        };
    }

    public Optional<Job> getTriggerableJob(String jobId)
    {
        return jobService.get(jobId)
                .filter(triggerableJobFilter);
    }

    public List<Job> findTriggerableJobs(String term)
    {
        return jobService.search(term)
                .stream()
                .filter(triggerableJobFilter)
                .collect(toList());
    }

    public TriggerBuildsResponse triggerBuilds(TriggerBuildsRequest request)
    {
        TriggerBuildsResponse response = new TriggerBuildsResponse();
        String scope = Optional.ofNullable(request.getIssueKey())
                .map(KeyExtractor::extractProjectKeyFromIssueKey)
                .orElse(null);
        boolean useUserId = configurationService.useUserId(scope);

        Optional<IssueReference> issueReference = Optional.ofNullable(request.getIssueKey())
                .flatMap(key -> getIssueReference(key,
                        useUserId,
                        scope,
                        request.getUseParameters() == TriggerBuildsRequest.UseParameters.reference));

        Set<Job> jobs = new HashSet<>();
        if (request.isSpecificJob())
        {
            LOGGER.debug("Triggering specific jobs from {}", request.getJobs());
            getJobsToTrigger(request, jobs, response);
        }
        else if (request.isLookupJob())
        {
            LOGGER.debug("Triggering all jobs by lookup, search term: {}", request.getLookupJob());
            jobs.addAll(jobService.search(request.getLookupJob(), request.isExactMatch()));
        }
        else if (issueReference.isPresent())
        {
            if (request.isLinked())
            {
                LOGGER.debug("Triggering all jobs linked to {}", request.getIssueKey());
                issueLinkService.getJobsForIssue(false, request.getIssueKey())
                        .stream()
                        .filter(triggerableJobFilter)
                        .forEach(jobs::add);
            }
            else if (request.isByConvention())
            {
                LOGGER.debug("Triggering all jobs by convention, search term: {}", request.getIssueKey());
                jobs.addAll(jobService.search(request.getIssueKey()));
            }
            else if (request.isByIssueField())
            {
                LOGGER.debug("Triggering all jobs by issue field, field: {}, mapping: {}",
                        request.getByIssueField(),
                        request.getByIssueFieldMapping());
                Stream.of(getIssueReference(request.getIssueKey(), request.getByIssueField(), request.getByIssueFieldMapping(), useUserId))
                        .filter(Optional::isPresent)
                        .map(Optional::get)
                        .map(IssueReference::getFields)
                        .flatMap(Set::stream)
                        .filter(field -> Objects.equals(request.getByIssueField(), field.getKey()))
                        .map(IssueReference.Field::getValue)
                        .filter(StringUtils::isNoneBlank)
                        .findFirst()
                        .ifPresent(s -> {
                            LOGGER.debug("Triggering all jobs by issue field, search term: {}", s);
                            jobs.addAll(jobService.search(s, request.isExactMatch()));
                        });
            }
        }
        else
        {
            response.addWarning(textResolver.getText("unknown.issue", request.getIssueKey()), null);
        }

        if (!jobs.isEmpty())
        {
            String subject = getSubject(useUserId);
            Map<String, String> parameters = switch (request.getUseParameters())
            {
                case reference -> issueReference.map(ref -> ref.getFields()
                                .stream()
                                .collect(Collectors.toMap(field -> field.getName()
                                        .replaceAll("[^A-Za-z0-9]", "_"), IssueReference.Field::getValue)))
                        .orElse(null);
                case specified -> request.getParameters()
                        .stream()
                        .collect(Collectors.toMap(TriggerBuildsRequest.Parameter::getName, TriggerBuildsRequest.Parameter::getValue));
                default -> null;
            };

            triggerBuilds(issueReference.orElse(null), jobs, subject, parameters, response);
        }
        else
        {
            LOGGER.debug("No jobs found that match request {}", request);
            response.addWarning(textResolver.getText("trigger.build.no.jobs.matched.request"), null);
        }
        return response;
    }

    private Optional<IssueReference> getIssueReference(
            String issueKey,
            boolean useUserId,
            String scope,
            boolean includeFields)
    {
        if (includeFields)
        {
            JsonFieldContext fieldContext = new JsonFieldContext(configurationService.getParameterIssueFields(scope),
                    useUserId,
                    configurationService.getParameterIssueFieldNames(scope),
                    configurationService.getParameterIssueFieldMappers(scope));
            return issueReferenceProvider.getIssueReference(issueKey, fieldContext);
        }
        else
        {
            return issueReferenceProvider.getIssueReference(issueKey);
        }
    }

    private Optional<IssueReference> getIssueReference(
            String issueKey,
            String issueField,
            @Nullable
            String issueFieldMapping,
            boolean useUserId)
    {
        Map<String, String> mapping = new HashMap<>();
        if (issueFieldMapping != null)
        {
            mapping.put(issueField, issueFieldMapping);
        }
        JsonFieldContext fieldContext = new JsonFieldContext(Collections.singleton(issueField), useUserId, null, mapping);
        return issueReferenceProvider.getIssueReference(issueKey, fieldContext);
    }

    private void getJobsToTrigger(
            TriggerBuildsRequest request,
            Set<Job> jobs,
            TriggerBuildsResponse response)
    {
        for (String jobId : request.getJobs())
        {
            Optional<Job> job = jobService.get(jobId);
            if (job.filter(triggerableJobFilter)
                    .isPresent())
            {
                jobs.add(job.get());
            }
            else if (job.isPresent())
            {
                response.addWarning(textResolver.getText("job.not.triggerable",
                                job.get()
                                        .getDisplayName()),
                        job.get()
                                .getDisplayUrl());
            }
            else
            {
                response.addFailure(textResolver.getText("unknown.job", jobId), null);
            }
        }
    }

    private void triggerBuilds(
            @Nullable
            IssueReference issueReference,
            Set<Job> jobs,
            String subject,
            @Nullable
            Map<String, String> parameters,
            TriggerBuildsResponse response)
    {
        for (Job job : jobs)
        {
            SiteClient.BuildJobResponse buildJobResponse = siteClient.buildJob(SiteClient.BuildJobRequest.create(job, subject)
                    .setIssue(issueReference)
                    .setParameters(parameters));
            String displayName = job.getDisplayName();
            switch (buildJobResponse)
            {
                case NOT_SUPPORTED:
                    response.addFailure(textResolver.getText("unsupported.site.for.build.triggers",
                            job.getSite()
                                    .getName()), null);
                    break;
                case JOB_NOT_BUILDABLE:
                    response.addWarning(textResolver.getText("job.not.buidable",
                            displayName,
                            job.getSite()
                                    .getName()), job.getDisplayUrl());
                    break;
                case FAILED:
                    response.addFailure(textResolver.getText("failed.to.trigger.build", displayName), job.getDisplayUrl());
                    break;
                case BUILD_SCHEDULED:
                    response.addSuccess(textResolver.getText("successfully.scheduled.build", displayName), job.getDisplayUrl());
                    break;
                case ACCEPTED:
                    response.addSuccess(textResolver.getText("successfully.send.build.request", displayName), job.getDisplayUrl());
                    break;
            }
        }
    }

    private String getSubject(
            boolean useUserId)
    {
        return useUserId ? getCurrentUserId() : getCurrentUserDisplayName();
    }

    protected abstract String getCurrentUserId();

    protected abstract String getCurrentUserDisplayName();

    public String evaluateParameterIssueFields(EvaluateParameterIssueFieldsRequest request)
    {
        Map<String, String> results = new HashMap<>();
        JsonFieldContext fieldContext = new JsonFieldContext(ConfigurationService.STRING_TO_SET.apply(request.getFields()),
                request.isUseUserId(),
                ConfigurationService.STRING_TO_MAP.apply(request.getNames()),
                ConfigurationService.STRING_TO_MAP.apply(request.getMappers()))
        {
            @Override
            protected void handleInvalidMapper(
                    String fieldKey,
                    String fieldJsonPath,
                    DocumentContext context,
                    Exception exception)
            {
                results.put(fieldKey,
                        String.format("Failed to map value of field %s using json-path '%s': %s; json: %s",
                                fieldKey,
                                fieldJsonPath,
                                exception.getMessage(),
                                context.jsonString()));
            }
        };
        Optional<IssueReference> reference = issueReferenceProvider.getIssueReference(request.getIssueKey(), fieldContext);
        if (reference.isPresent())
        {
            if (!request.isExcludeDefaults())
            {
                results.put("issueKey",
                        reference.get()
                                .getIssueKey());
                results.put("issueUrl",
                        reference.get()
                                .getIssueUrl()
                                .toASCIIString());
            }
            reference.map(IssueReference::getFields)
                    .ifPresent(fields -> fields.forEach(field -> results.put(field.getName(), field.getValue())));

            return results.entrySet()
                    .stream()
                    .map(entry -> entry.getKey() + "=" + entry.getValue())
                    .collect(Collectors.joining("\n"));
        }
        else
        {
            return "Unknown issue " + request.getIssueKey();
        }
    }
}
