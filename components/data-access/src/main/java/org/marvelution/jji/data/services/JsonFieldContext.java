/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services;

import javax.annotation.*;
import java.util.*;

import org.marvelution.jji.data.access.api.IssueReferenceProvider;

import com.jayway.jsonpath.*;
import org.slf4j.*;

import static org.marvelution.jji.data.services.api.model.IssueFields.*;

public class JsonFieldContext
        extends IssueReferenceProvider.FieldContext
{

    private static final Logger LOGGER = LoggerFactory.getLogger(JsonFieldContext.class);
    private final Set<String> additionalFields;
    private final boolean useUserId;
    private final Map<String, String> nameMappers;
    private final Map<String, String> valueMappers;

    public JsonFieldContext(
            Set<String> additionalFields,
            boolean useUserId,
            Map<String, String> nameMappers,
            Map<String, String> valueMappers)
    {
        this.additionalFields = additionalFields;
        this.useUserId = useUserId;
        this.nameMappers = Optional.ofNullable(nameMappers)
                .orElse(new HashMap<>());
        this.valueMappers = Optional.ofNullable(valueMappers)
                .orElse(new HashMap<>());
    }

    private static String mergeFieldValues(List<String> values)
    {
        return values.isEmpty() ? null : String.join(",", values);
    }

    @Override
    public Set<String> additionalFields()
    {
        return additionalFields;
    }

    @Override
    public String fieldName(
            String fieldKey,
            String fieldName)
    {
        return nameMappers.getOrDefault(fieldKey, fieldName);
    }

    @Nullable
    @Override
    public String fieldValue(
            String fieldKey,
            String fieldName,
            @Nullable
            Object value,
            @Nullable
            Object renderedValue)
    {
        if (value == null)
        {
            return null;
        }
        else if (value instanceof String)
        {
            return (String) value;
        }

        String fieldJsonPath = getFieldJsonPath(fieldKey);
        if (fieldJsonPath != null)
        {
            DocumentContext context = JsonPath.using(Configuration.defaultConfiguration()
                            .addOptions(Option.ALWAYS_RETURN_LIST))
                    .parse(value);
            try
            {
                List<String> values = context.read(fieldJsonPath);
                return mergeFieldValues(values);
            }
            catch (Exception e)
            {
                handleInvalidMapper(fieldKey, fieldJsonPath, context, e);
                return null;
            }
        }
        else if (CREATOR.equals(fieldKey) || ASSIGNEE.equals(fieldKey) || REPORTER.equals(fieldKey))
        {
            DocumentContext context = JsonPath.using(Configuration.defaultConfiguration()
                            .addOptions(Option.SUPPRESS_EXCEPTIONS))
                    .parse(value);
            if (useUserId)
            {
                return context.read("$.accountId");
            }
            else
            {
                return context.read("$.displayName");
            }
        }
        else if (renderedValue instanceof String)
        {
            return (String) renderedValue;
        }
        else
        {
            String valueJson = JsonPath.parse(value)
                    .jsonString();
            String renderedValueJson = Optional.ofNullable(renderedValue)
                    .map(JsonPath::parse)
                    .map(DocumentContext::jsonString)
                    .orElse(null);
            LOGGER.warn("Unable to extract string value from field {} with name: {}, value: {}, renderedValue: {}",
                    fieldKey,
                    fieldName,
                    valueJson,
                    renderedValueJson);
        }
        return null;
    }

    @Nullable
    private String getFieldJsonPath(String fieldKey)
    {
        if (valueMappers.containsKey(fieldKey))
        {
            return valueMappers.get(fieldKey);
        }
        else if (PROJECT.equals(fieldKey))
        {
            return "$.name";
        }
        else if (VERSIONS.equals(fieldKey) || FIX_VERSIONS.equals(fieldKey) || COMPONENTS.equals(fieldKey))
        {
            return "$.[*].name";
        }
        else if (LABELS.equals(fieldKey))
        {
            return "$..*";
        }
        else
        {
            return null;
        }
    }

    protected void handleInvalidMapper(
            String fieldKey,
            String fieldJsonPath,
            DocumentContext context,
            Exception exception)
    {
        LOGGER.error("Failed to map value of field {} using json-path '{}': value: {}",
                fieldKey,
                fieldJsonPath,
                context.jsonString(),
                exception);
    }
}
