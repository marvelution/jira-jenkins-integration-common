/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services;

import java.util.*;
import java.util.concurrent.*;

import org.marvelution.jji.data.services.api.*;
import org.marvelution.jji.data.synchronization.api.*;
import org.marvelution.jji.data.synchronization.api.trigger.*;
import org.marvelution.jji.model.*;
import org.marvelution.jji.model.request.*;

import jakarta.inject.*;
import org.slf4j.*;

import static org.marvelution.jji.model.request.BulkRequest.Operation.*;

@Named
@javax.inject.Named
public class BulkRequestService
{

    private static final Logger LOGGER = LoggerFactory.getLogger(BulkRequestService.class);
    private final SiteService siteService;
    private final JobService jobService;
    private final SynchronizationService synchronizationService;
    private final Executor asyncExecutor;

    @Inject
    @javax.inject.Inject
    public BulkRequestService(
            SiteService siteService,
            JobService jobService,
            SynchronizationService synchronizationService,
            Executor asyncExecutor)
    {
        this.siteService = siteService;
        this.jobService = jobService;
        this.synchronizationService = synchronizationService;
        this.asyncExecutor = asyncExecutor;
    }

    /**
     * @since 1.15.0
     */
    public void deleteSite(Site site)
    {
        asyncExecutor.execute(() -> siteService.delete(site));
    }

    /**
     * @since 1.15.0
     */
    public void deleteJobs(Site site)
    {
        asyncExecutor.execute(() -> siteService.deleteDeletedJobs(site));
    }

    /**
     * @since 1.15.0
     */
    public void deleteJob(Job job)
    {
        asyncExecutor.execute(() -> jobService.delete(job));
    }

    /**
     * @since 1.15.0
     */
    public void deleteBuilds(Job job)
    {
        asyncExecutor.execute(() -> jobService.deleteAllBuilds(job));
    }

    public void process(BulkRequest request)
    {
        if (!request.getOperations()
                .isEmpty() && !request.getJobIds()
                .isEmpty())
        {
            asyncExecutor.execute(new BulkRequestRunner(request));
        }
    }

    private class BulkRequestRunner
            implements Runnable
    {

        private final BulkRequest request;

        private BulkRequestRunner(BulkRequest request)
        {
            this.request = request;
        }

        @Override
        public void run()
        {
            request.getOperations()
                    .sort(null);
            if (request.getJobs()
                    .isEmpty())
            {
                request.getJobIds()
                        .stream()
                        .map(jobService::get)
                        .filter(Optional::isPresent)
                        .map(Optional::get)
                        .forEach(this::handleJob);
            }
            else
            {
                request.getJobs()
                        .forEach(this::handleJob);
            }
        }

        private void handleJob(Job job)
        {
            for (BulkRequest.Operation operation : request.getOperations())
            {
                if (operation == ENABLE_JOB || operation == DISABLE_JOB)
                {
                    boolean enabled = operation == ENABLE_JOB;
                    LOGGER.debug("{} job {} for synchronization", (enabled ? "Enabling" : "Disabled"), job);
                    jobService.enable(job, enabled);
                }
                else if (operation == CLEAR_CACHE)
                {
                    LOGGER.debug("Clearing build cache of {}", job);
                    jobService.deleteAllBuilds(job);
                }
                else if (operation == REBUILD_CACHE)
                {
                    LOGGER.debug("Resetting build cache for {}", job);
                    jobService.resetBuildCache(job);
                }
                if (operation == SYNCHRONIZE || (operation == REBUILD_CACHE && !request.getOperations()
                        .contains(SYNCHRONIZE)))
                {
                    LOGGER.debug("Scheduling synchronization of {}", job);
                    synchronizationService.synchronize(job, new BulkRequestTrigger());
                }
            }
        }
    }
}
