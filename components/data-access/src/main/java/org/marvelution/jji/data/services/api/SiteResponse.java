/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services.api;

import java.io.*;
import java.util.*;
import java.util.function.*;

import org.marvelution.jji.model.*;
import org.marvelution.jji.model.parsers.*;

import jakarta.json.*;

/**
 * Response holder for {@link Site}.
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public abstract class SiteResponse
{

	protected final List<String> errors;
	protected JsonStructure json;

	protected SiteResponse()
	{
		this(null);
	}

	protected SiteResponse(JsonStructure json)
	{
		errors = new ArrayList<>();
		this.json = json;
	}

	/**
	 * Returns whether the response was successful.
	 */
	public boolean isSuccessful()
	{
		return getStatusCode() / 100 == 2 && !hasAuthErrors() && !hasErrors();
	}

	/**
	 * Performs the specified {@link Consumer action} is the response was successful.
	 */
	public void ifSuccessful(Consumer<SiteResponse> action)
	{
		if (isSuccessful())
		{
			action.accept(this);
		}
	}

	/**
	 * Returns whether the response has any auth related errors.
	 */
	public boolean hasAuthErrors()
	{
		int statusCode = getStatusCode();
		return statusCode == 401 || statusCode == 403;
	}

	/**
	 * Returns the HTTP Status code.
	 */
	public abstract int getStatusCode();

	/**
	 * Returns the HTTP Status message.
	 */
	public abstract String getStatusMessage();

	/**
	 * Returns the HTTP header form the response, if there are multiple values set, this will only return the first.
	 */
	public abstract Optional<String> getHeader(String header);

	public Optional<Version> getPluginVersion()
	{
		Optional<String> versionheader = getHeader(SiteClient.X_JIRA_INTEGRATION);
		if (!versionheader.isPresent()) {
			versionheader = getHeader(SiteClient.X_JJI_VERSION);
		}
		return versionheader.map(Version::of);
	}

	/**
	 * Parse the {@link SiteResponse#getJson()} json response} using the specified {@link Parser}.
	 */
	public <T> Optional<T> parseJson(Parser<T> parser)
	{
		return getJson().map(parser::parse);
	}

	/**
	 * Returns whether the response has any errors.
	 */
	public boolean hasErrors()
	{
		return !getErrors().isEmpty();
	}

	/**
	 * Parse the {@link SiteResponse#getJson()} json response} using the specified {@link MergingParser} and merge the results with the
	 * specified target.
	 */
	public <T> void parseJson(
			MergingParser<T> parser,
			T target)
	{
		getJson().ifPresent(j -> parser.parse(j, target));
	}

	/**
	 * Returns a list of errors, if any.
	 */
	public List<String> getErrors()
	{
		return errors;
	}

	/**
	 * Returns the response body as {@link JsonStructure}.
	 */
	protected Optional<JsonStructure> getJson()
	{
		if (json == null)
		{
			try (JsonReader reader = Json.createReader(getResponseBodyReader()))
			{
				json = reader.read();
			}
			catch (Exception e)
			{
				errors.add("Failed to parse respond body " + e.getMessage());
			}
		}
		return Optional.ofNullable(json);
	}

	/**
	 * Returns the {@link Reader} to the response body.
	 */
	protected abstract Reader getResponseBodyReader()
			throws IOException;
}
