/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services.api;

import javax.annotation.Nullable;
import jakarta.json.Json;
import jakarta.json.JsonObject;
import jakarta.json.JsonObjectBuilder;

import org.marvelution.jji.model.Configuration;
import org.marvelution.jji.model.ConfigurationSetting;
import org.marvelution.jji.model.Site;

import java.net.URI;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.commons.lang3.StringUtils;

import static org.apache.commons.lang3.StringUtils.isNotBlank;

public interface ConfigurationService
{

    String MAX_BUILDS_PER_PAGE = "max_builds_per_page";
    String USE_USER_ID = "use_user_id";
    String PARAMETER_ISSUE_FIELDS = "parameter_issue_fields";
    String PARAMETER_ISSUE_FIELD_NAMES = "parameter_issue_field_names";
    String PARAMETER_ISSUE_FIELD_MAPPERS = "parameter_issue_field_mappers";
    String DELETED_DATA_RETENTION_PERIOD = "deleted_data_retention_period";
    String AUDIT_DATA_RETENTION_PERIOD = "audit_data_retention_period";
    String ENABLED_PROJECT_KEYS = "enabled_project_keys";

    Set<String> GLOBAL_ONLY_SETTING_KEYS = Stream.of(DELETED_DATA_RETENTION_PERIOD, AUDIT_DATA_RETENTION_PERIOD, ENABLED_PROJECT_KEYS)
            .collect(Collectors.toSet());

    String GLOBAL_SCOPE = ConfigurationSetting.GLOBAL_SCOPE;

    int DEFAULT_MAX_BUILDS_PER_PAGE = 100;
    boolean DEFAULT_USE_USER_ID = false;
    Set<String> DEFAULT_PARAMETER_ISSUE_FIELDS = Collections.emptySet();
    Map<String, String> DEFAULT_PARAMETER_ISSUE_FIELD_NAMES = Collections.emptyMap();
    Map<String, String> DEFAULT_PARAMETER_ISSUE_FIELD_MAPPERS = Collections.emptyMap();
    int DEFAULT_DELETED_DATA_RETENTION_PERIOD = 12;
    int DEFAULT_AUDIT_DATA_RETENTION_PERIOD = 1;

    Function<String, Set<String>> STRING_TO_SET = ids -> Stream.of(ids.split(","))
            .collect(Collectors.toSet());
    Function<String, Map<String, String>> STRING_TO_MAP = mappers -> Stream.of(mappers.split("\\R"))
            .filter(StringUtils::isNotBlank)
            .map(mapper -> mapper.split("=", 2))
            .filter(mapper -> mapper.length == 2 && isNotBlank(mapper[0]) && isNotBlank(mapper[1]))
            .collect(Collectors.toMap(mapper -> mapper[0], mapper -> mapper[1]));

    String getInstanceName();

    URI getBaseUrl();

    URI getBaseAPIUrl();

    default int getMaximumBuildsPerPage(String scope)
    {
        return getConfigurationSetting(scope, MAX_BUILDS_PER_PAGE).valueAsInteger()
                .orElse(DEFAULT_MAX_BUILDS_PER_PAGE);
    }

    default boolean useUserId(String scope)
    {
        return getConfigurationSetting(scope, USE_USER_ID).valueAsBoolean()
                .orElse(DEFAULT_USE_USER_ID);
    }

    default Set<String> getParameterIssueFields(String scope)
    {
        return getConfigurationSetting(scope, PARAMETER_ISSUE_FIELDS).valueAs(STRING_TO_SET)
                .orElse(DEFAULT_PARAMETER_ISSUE_FIELDS);
    }

    default Map<String, String> getParameterIssueFieldNames(String scope)
    {
        return getConfigurationSetting(scope, PARAMETER_ISSUE_FIELD_NAMES).valueAs(STRING_TO_MAP)
                .orElse(DEFAULT_PARAMETER_ISSUE_FIELD_NAMES);
    }

    default Map<String, String> getParameterIssueFieldMappers(String scope)
    {
        return getConfigurationSetting(scope, PARAMETER_ISSUE_FIELD_MAPPERS).valueAs(STRING_TO_MAP)
                .orElse(DEFAULT_PARAMETER_ISSUE_FIELD_MAPPERS);
    }

    default int getDeletedDataRetentionPeriod()
    {
        return getConfigurationSetting(DELETED_DATA_RETENTION_PERIOD).valueAsInteger()
                .orElse(DEFAULT_DELETED_DATA_RETENTION_PERIOD);
    }

    default Instant getDeletedDataRetentionTimestamp()
    {
        int deletedDataRetentionPeriod = getDeletedDataRetentionPeriod();
        if (deletedDataRetentionPeriod > 0)
        {
            return Instant.now()
                    .minusMillis(ChronoUnit.MONTHS.getDuration()
                                         .toMillis() * deletedDataRetentionPeriod);
        }
        else
        {
            return Instant.now()
                    .minusMillis(ChronoUnit.DAYS.getDuration()
                            .toMillis());
        }
    }

    default int getAuditDataRetentionPeriod()
    {
        return getConfigurationSetting(AUDIT_DATA_RETENTION_PERIOD).valueAsInteger()
                .orElse(DEFAULT_AUDIT_DATA_RETENTION_PERIOD);
    }

    default Instant getAuditDataRetentionTimestamp()
    {
        int auditDataRetentionPeriod = getAuditDataRetentionPeriod();
        if (auditDataRetentionPeriod > 0)
        {
            return Instant.now()
                    .minusMillis(ChronoUnit.MONTHS.getDuration()
                                         .toMillis() * auditDataRetentionPeriod);
        }
        else
        {
            return Instant.now()
                    .minusMillis(ChronoUnit.DAYS.getDuration()
                            .toMillis());
        }
    }

    @Nullable
    default Set<String> getEnabledProjectKeys()
    {
        return getConfigurationSetting(ENABLED_PROJECT_KEYS).valueAs(STRING_TO_SET)
                .orElse(null);
    }

    default boolean isProjectEnabled(String projectKey)
    {
        Set<String> enabledProjects = getEnabledProjectKeys();
        return enabledProjects == null || enabledProjects.contains(projectKey);
    }

    default Configuration getConfiguration()
    {
        return getConfiguration(GLOBAL_SCOPE);
    }

    Configuration getConfiguration(
            @Nullable
            String scope);

    void saveConfiguration(Configuration configuration);

    default ConfigurationSetting getConfigurationSetting(String key)
    {
        return getConfigurationSetting(GLOBAL_SCOPE, key);
    }

    ConfigurationSetting getConfigurationSetting(
            @Nullable
            String scope,
            String key);

    List<ConfigurationSetting> getAllConfigurationSettings();

    default void saveConfigurationSetting(
            ConfigurationSetting setting)
    {
        saveConfigurationSetting(setting.getScope(), setting);
    }

    default void saveConfigurationSetting(
            @Nullable
            String scope,
            ConfigurationSetting setting)
    {
        String value;
        if (Objects.equals(MAX_BUILDS_PER_PAGE, setting.getKey()))
        {
            int count = setting.valueAsInteger()
                    .orElse(DEFAULT_MAX_BUILDS_PER_PAGE);
            if (count < 1)
            {
                value = Integer.toString(DEFAULT_MAX_BUILDS_PER_PAGE);
            }
            else
            {
                value = Integer.toString(count);
            }
        }
        else if (Objects.equals(USE_USER_ID, setting.getKey()))
        {
            value = Boolean.toString(setting.valueAsBoolean()
                    .orElse(false));
        }
        else if (Objects.equals(DELETED_DATA_RETENTION_PERIOD, setting.getKey()))
        {
            int period = setting.valueAsInteger()
                    .orElse(DEFAULT_DELETED_DATA_RETENTION_PERIOD);
            if (period < 1)
            {
                value = Integer.toString(DEFAULT_DELETED_DATA_RETENTION_PERIOD);
            }
            else
            {
                value = Integer.toString(period);
            }
        }
        else if (Objects.equals(AUDIT_DATA_RETENTION_PERIOD, setting.getKey()))
        {
            int period = setting.valueAsInteger()
                    .orElse(DEFAULT_AUDIT_DATA_RETENTION_PERIOD);
            if (period < 0)
            {
                value = Integer.toString(DEFAULT_AUDIT_DATA_RETENTION_PERIOD);
            }
            else
            {
                value = Integer.toString(period);
            }
        }
        else
        {
            value = getStorableConfigurationSettingValue(setting);
        }

        storeConfigurationSetting(setting, getScopeForKey(setting.getKey(), scope), value);
    }

    @Nullable
    default String getStorableConfigurationSettingValue(ConfigurationSetting setting)
    {
        return setting.getValue();
    }

    void storeConfigurationSetting(
            ConfigurationSetting setting,
            String scopeForKey,
            @Nullable
            String value);

    void resetToDefaults();

    void resetToDefaults(
            @Nullable
            String scope);

    default JsonObject getSiteRegistrationDetails(Site site)
    {
        JsonObjectBuilder json = Json.createObjectBuilder();
        json.add("url", getBaseAPIUrl().toASCIIString());
        json.add("name", getInstanceName());
        json.add("identifier", site.getId());
        if (!site.isRegistrationComplete())
        {
            json.add("sharedSecret", site.getSharedSecret());
        }
        json.add("firewalled", site.isInaccessibleSite());
        json.add("tunneled", site.isTunneledSite());
        Map<String, Object> context = getSiteRegistrationContext(site);
        if (context != null && !context.isEmpty())
        {
            json.add("context", Json.createObjectBuilder(context));
        }
        return json.build();
    }

    default Map<String, Object> getSiteRegistrationContext(Site site)
    {
        return null;
    }

    static String getScopeForKey(
            String key,
            String scope)
    {
        if (GLOBAL_ONLY_SETTING_KEYS.contains(key) || StringUtils.isBlank(scope))
        {
            return GLOBAL_SCOPE;
        }
        else
        {
            return scope;
        }
    }

    static String requireScopeElse(String scope)
    {
        return (StringUtils.isNotBlank(scope)) ? scope : GLOBAL_SCOPE;
    }
}
