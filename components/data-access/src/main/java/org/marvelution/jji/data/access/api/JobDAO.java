/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access.api;

import javax.annotation.Nullable;

import org.marvelution.jji.model.Job;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

public interface JobDAO
        extends BaseDAO<Job>
{

    long countBySite(String siteId);

    boolean hasDeletedJobs(String siteId);

    List<Job> getDeletedJobs(String siteId);

    List<Job> getBySiteIdAtLevel(
            String siteId,
            String level,
            boolean includeDeleted);

    Optional<Job> find(
            String siteId,
            String name,
            @Nullable
            String parent);

    List<Job> find(String name);

    List<Job> search(String term);

    List<Job> search(
            String siteId,
            String term);

    default List<Job> findByHash(String hash)
    {
        return findByHash(null, hash);
    }

    List<Job> findByHash(
            @Nullable
            String siteId,
            String hash);

    List<Job> findBySiteId(
            String siteId,
            int page,
            int size);

    void cleanupDeletedJobsWithoutBuilds();

    void cleanupOldDeletedJobs(Instant timestamp);

    List<String> findOldDeletedJobIds(Instant timestamp);

    List<Job> getLatestActiveJobs(int limit);

    default List<Job> getLatestActiveJobsWithCompassLink()
    {
        return List.of();
    }
}
