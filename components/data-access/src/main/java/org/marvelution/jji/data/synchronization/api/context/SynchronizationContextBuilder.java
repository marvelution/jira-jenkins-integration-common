/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.synchronization.api.context;

import org.marvelution.jji.api.events.EventPublisher;
import org.marvelution.jji.data.services.api.SiteClient;
import org.marvelution.jji.data.services.api.SiteDataClient;
import org.marvelution.jji.data.synchronization.api.SynchronizationService;
import org.marvelution.jji.data.synchronization.api.result.ImmutableSynchronizationResult;
import org.marvelution.jji.data.synchronization.api.result.SynchronizationResult;
import org.marvelution.jji.model.Build;
import org.marvelution.jji.model.Job;
import org.marvelution.jji.model.Site;
import org.marvelution.jji.model.Syncable;

public class SynchronizationContextBuilder<S extends Syncable<S>, R extends SynchronizationResult>
{

    private final SynchronizationService synchronizationService;
    private final EventPublisher eventPublisher;
    private final SiteClient siteClient;
    private final S syncable;
    private final R result;

    public SynchronizationContextBuilder(
            SynchronizationService synchronizationService,
            EventPublisher eventPublisher,
            SiteClient siteClient)
    {
        this(synchronizationService, eventPublisher, siteClient, null, null);
    }

    private SynchronizationContextBuilder(
            SynchronizationService synchronizationService,
            EventPublisher eventPublisher,
            SiteClient siteClient,
            S syncable,
            R result)
    {
        this.synchronizationService = synchronizationService;
        this.eventPublisher = eventPublisher;
        this.siteClient = siteClient;
        this.syncable = syncable;
        this.result = result;
    }

    /**
     * Returns a new {@link SynchronizationContextBuilder} with the specified {@link SynchronizationService}.
     */
    public SynchronizationContextBuilder<S, R> synchronizationService(SynchronizationService synchronizationService)
    {
        return new SynchronizationContextBuilder<>(synchronizationService, eventPublisher, siteClient, syncable, result);
    }

    /**
     * Returns a new {@link SynchronizationContextBuilder} with the specified {@link EventPublisher}.
     */
    public SynchronizationContextBuilder<S, R> eventPublisher(EventPublisher eventPublisher)
    {
        return new SynchronizationContextBuilder<>(synchronizationService, eventPublisher, siteClient, syncable, result);
    }

    /**
     * Returns a new {@link SynchronizationContextBuilder} for the specified {@link Syncable}.
     */
    public <S1 extends Syncable<S1>> SynchronizationContextBuilder<S1, R> syncable(S1 syncable)
    {
        return new SynchronizationContextBuilder<>(synchronizationService, eventPublisher, siteClient, syncable, result);
    }

    /**
     * Returns a new {@link SynchronizationContextBuilder} for the specified {@link Site}.
     */
    public SynchronizationContextBuilder<Site, R> site(Site site)
    {
        return new SynchronizationContextBuilder<>(synchronizationService, eventPublisher, siteClient, site, result);
    }

    /**
     * Returns a new {@link SynchronizationContextBuilder} for the specified {@link Job}.
     */
    public SynchronizationContextBuilder<Job, R> job(Job job)
    {
        return new SynchronizationContextBuilder<>(synchronizationService, eventPublisher, siteClient, job, result);
    }

    /**
     * Returns a new {@link SynchronizationContextBuilder} for the specified {@link Build}.
     */
    public SynchronizationContextBuilder<Build, R> build(Build build)
    {
        return new SynchronizationContextBuilder<>(synchronizationService, eventPublisher, siteClient, build, result);
    }

    /**
     * Returns a new {@link SynchronizationContextBuilder} with the specified {@link SynchronizationResult}.
     */
    public <R1 extends SynchronizationResult> SynchronizationContextBuilder<S, R1> result(R1 result)
    {
        return new SynchronizationContextBuilder<>(synchronizationService, eventPublisher, siteClient, syncable, result);
    }

    /**
     * Builds the {@link SynchronizationContext}.
     */
    public SynchronizationContext<S> build()
    {
        SiteDataClient siteDataClient;
        if (result.triggerReason()
                .hasData())
        {
            siteDataClient = new TriggerReasonSiteDataClient<>(syncable, result.triggerReason());
        }
        else
        {
            siteDataClient = siteClient;
        }
        return new SimpleSynchronizationContext<>(synchronizationService, eventPublisher, siteDataClient, syncable, result);
    }
}
