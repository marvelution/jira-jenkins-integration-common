/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.synchronization.api.exceptions;

/**
 * {@link RuntimeException} thrown in case of exceptions triggering synchronization operations.
 *
 * @author Mark Rekveld
 * @since 1.9.0
 */
public class SynchronizationTriggerException
        extends RuntimeException
{

    public SynchronizationTriggerException(String message)
    {
        super(message);
    }

    public SynchronizationTriggerException(
            String message,
            Throwable cause)
    {
        super(message, cause);
    }
}
