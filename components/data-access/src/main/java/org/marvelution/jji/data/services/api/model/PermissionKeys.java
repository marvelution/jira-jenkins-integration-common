/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services.api.model;

public class PermissionKeys
{

    public static final String ADMINISTER = "ADMINISTER";
    public static final String ADMINISTER_PROJECTS = "ADMINISTER_PROJECTS";
    public static final String VIEW_DEV_TOOLS = "VIEW_DEV_TOOLS";

    public static final String[] JIRA_PERMISSIONS = {ADMINISTER,
            ADMINISTER_PROJECTS,
            VIEW_DEV_TOOLS};

    public static final String APP_ADMINISTER = "administer";
    public static final String TRIGGER_JENKINS_BUILD = "trigger-jenkins-build";
    public static final String VIEW_JENKINS = "view-jenkins";

    public static final String[] APP_PERMISSIONS = {APP_ADMINISTER,
            TRIGGER_JENKINS_BUILD,
            VIEW_JENKINS};
}
