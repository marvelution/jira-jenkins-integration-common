/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.stream.Stream;
import jakarta.json.*;

import org.marvelution.jji.data.services.api.*;
import org.marvelution.jji.model.*;
import org.marvelution.jji.model.parsers.MergingParser;
import org.marvelution.jji.model.parsers.ParserProvider;
import org.marvelution.jji.model.response.SiteTunnelDetails;
import org.marvelution.jji.synctoken.CanonicalHttpServletRequest;
import org.marvelution.jji.synctoken.SyncTokenBuilder;
import org.marvelution.jji.utils.SiteUrl;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.util.Collections.singletonList;
import static java.util.Objects.requireNonNull;
import static java.util.Optional.empty;
import static java.util.stream.Collectors.toSet;

public abstract class BaseSiteClient
        implements SiteClient
{

    private static final int NOT_FOUND = 404;
    private static final int NOT_ACCEPTABLE = 406;
    private static final int CREATED = 201;
    private static final String REGISTER_PATH = "jji/register/";
    private static final String UNREGISTER_PATH = "jji/unregister/";
    private static final String TRIGGER_BUILD_PATH = "jji/build/";
    private static final Set<String> CRUMB_EXCLUSIONS = Stream.of(REGISTER_PATH, UNREGISTER_PATH, TRIGGER_BUILD_PATH)
            .collect(toSet());
    protected final Logger logger = LoggerFactory.getLogger(getClass());
    protected final ConfigurationService configurationService;
    protected final TunnelManager tunnelManager;
    private final InheritableThreadLocal<List<SiteResponseListener>> listeners = new InheritableThreadLocal<List<SiteResponseListener>>()
    {
        @Override
        protected List<SiteResponseListener> initialValue()
        {
            return new ArrayList<>();
        }
    };

    protected BaseSiteClient(
            ConfigurationService configurationService,
            TunnelManager tunnelManager)
    {
        this.configurationService = requireNonNull(configurationService);
        this.tunnelManager = requireNonNull(tunnelManager);
    }

    protected List<SiteResponseListener> getSiteResponseListeners()
    {
        return listeners.get();
    }

    @Override
    public Status getRemoteStatus(Site site)
    {
        if (site.isInaccessibleSite())
        {
            return Status.FIREWALLED;
        }
        else
        {
            SiteResponse response = get(syncUrl(site).api());
            logger.debug("Remote status check for {} returned {} [{}]",
                    site.getName(),
                    response.getStatusCode(),
                    response.getStatusMessage());
            if (response.getStatusCode() != -1 && (response.hasAuthErrors() || response.hasErrors()))
            {
                return Status.NOT_ACCESSIBLE;
            }
            else if (response.getStatusCode() == -1 && response.getErrors()
                    .stream()
                    .anyMatch(error -> error.contains("UnknownHostException")))
            {
                return site.isTunneledSite() ? Status.GETTING_READY : Status.UNREACHABLE;
            }
            else if (response.getStatusCode() == -1 && response.getErrors()
                    .stream()
                    .anyMatch(error -> error.contains("SSLHandshakeException")))
            {
                return Status.UNREACHABLE;
            }
            else if (response.isSuccessful())
            {
                return Status.ONLINE;
            }
            else
            {
                return Status.OFFLINE;
            }
        }
    }

    @Override
    public boolean isJenkinsPluginInstalled(Site site)
    {
        if (site.isInaccessibleSite())
        {
            return site.isJenkinsPluginInstalled();
        }
        else
        {
            return getJenkinsPluginVersion(site).isPresent();
        }
    }

    @Override
    public Optional<Version> getJenkinsPluginVersion(Site site)
    {
        if (site.isInaccessibleSite())
        {
            return empty();
        }
        else
        {
            // Try the new plugin first
            SiteResponse response = get(syncUrl(site).path("plugin/jira-integration/ping.html"));
            Optional<Version> version = response.getPluginVersion();
            if (version.isPresent())
            {
                logger.debug("The 'Jira Integration' plugin version {} is installed on {} [{}]",
                        version.get(),
                        site.getName(),
                        response.getHeader(X_JENKINS)
                                .orElse("Unknown"));
                return version;
            }
            else
            {
                // Try the old plugin
                response = get(syncUrl(site).path("plugin/jenkins-jira-plugin/ping.html"));
                version = response.getPluginVersion();
                if (version.isPresent())
                {
                    logger.debug("The 'Jira Integration for Jenkins' plugin version {} is installed on {}", version.get(), site.getName());
                    return version;
                }
                else if (response.isSuccessful())
                {
                    logger.debug("The 'Jira Integration for Jenkins' plugin is installed on {}, but didnt include the version header",
                            site.getName());
                    return Optional.of(Version.of("3.3.0"));
                }
                else
                {
                    return empty();
                }
            }
        }
    }

    @Override
    public boolean isCrumbSecurityEnabled(Site site)
    {
        if (site.isInaccessibleSite())
        {
            return false;
        }
        else
        {
            SiteUrl uri = syncUrl(site).api();
            addFieldFilterIfNeeded(uri, singletonList("useCrumbs"));
            return get(uri).parseJson(json -> {
                        if (json.getValueType() == JsonValue.ValueType.OBJECT)
                        {
                            return json.asJsonObject()
                                    .getBoolean("useCrumbs", true);
                        }
                        return true;
                    })
                    .orElse(true);
        }
    }

    @Override
    public boolean registerWithSite(
            Site site,
            SiteAuthentication authentication,
            boolean failureOnError)
    {
        if (site.isAccessibleSite() && (site.isJenkinsPluginInstalled() || isJenkinsPluginInstalled(site)))
        {
            logger.debug("About to register this Jira instance with {}", site.getName());
            JsonObject json = configurationService.getSiteRegistrationDetails(site);
            SiteResponse response = post(syncUrl(site).path(REGISTER_PATH)
                    .auth(authentication), json);
            if (!response.isSuccessful())
            {
                logger.warn("Unable to register with {}, {} [{}]", site.getName(), response.getStatusMessage(), response.getStatusCode());
                if (failureOnError)
                {
                    throw new IllegalStateException(
                            "Failed to register with site " + site.getName() + "; Site returned " + response.getStatusMessage() + " [" +
                            response.getStatusCode() + "]");
                }
            }
            return response.isSuccessful();
        }
        else
        {
            logger.debug("Site {} doesn't support automated registration", site.getName());
            return false;
        }
    }

    @Override
    public boolean unregisterWithSite(Site site)
    {
        if (!site.isInaccessibleSite())
        {
            logger.debug("About to unregister this Jira instance with {}", site.getName());
            JsonObjectBuilder json = Json.createObjectBuilder();
            json.add("url",
                    configurationService.getBaseAPIUrl()
                            .toASCIIString());
            SiteResponse response = post(syncUrl(site).path(UNREGISTER_PATH), json.build());
            if (!response.isSuccessful())
            {
                logger.warn("Unable to unregister with {}, {} [{}]", site.getName(), response.getStatusMessage(), response.getStatusCode());
            }
            return response.isSuccessful();
        }
        else
        {
            logger.debug("Site {} doesn't support automated un-registration", site.getName());
            return false;
        }
    }

    @Override
    public List<Job> getJobs(Site site)
    {
        MergingParser<JobsContainer> parser = ParserProvider.siteParser();

        SiteUrl uri = syncUrl(site).api();
        addFieldFilterIfNeeded(uri, parser.fields());
        Site copy = site.copy();
        get(uri).parseJson(parser, copy);
        return copy.getJobs();
    }

    @Override
    public BuildJobResponse buildJob(BuildJobRequest request)
    {
        Job job = request.getJob();
        Site site = job.getSite();
        if (!site.isInaccessibleSite() && site.isJenkinsPluginInstalled())
        {
            logger.debug("About to trigger a new build for {}", job);
            JsonObjectBuilder jsonBuilder = Json.createObjectBuilder();
            jsonBuilder.add("by", request.getSubject());
            IssueReference issue = request.getIssue();
            if (issue != null)
            {
                jsonBuilder.add("issueUrl",
                        issue.getIssueUrl()
                                .toASCIIString());
                jsonBuilder.add("issueKey", issue.getIssueKey());
            }
            if (!request.getParameters()
                    .isEmpty())
            {
                JsonArrayBuilder parameters = Json.createArrayBuilder();
                request.getParameters()
                        .entrySet()
                        .stream()
                        .map(field -> {
                            JsonObjectBuilder parameter = Json.createObjectBuilder();
                            parameter.add("name", field.getKey());
                            parameter.add("value", field.getValue());
                            return parameter;
                        })
                        .forEach(parameters::add);
                jsonBuilder.add("parameters", parameters);
            }
            JsonObject json = jsonBuilder.build();
            logger.debug("Triggering build of job {} with data {}", job.getName(), json);
            SiteResponse response = post(syncUrl(site).job(job)
                    .path(TRIGGER_BUILD_PATH), json);
            logger.debug("Build trigger of {} responded with: {} [{}]", job, response.getStatusMessage(), response.getStatusCode());
            switch (response.getStatusCode())
            {
                case NOT_ACCEPTABLE:
                case NOT_FOUND:
                    return BuildJobResponse.JOB_NOT_BUILDABLE;
                case CREATED:
                    return BuildJobResponse.BUILD_SCHEDULED;
                default:
                    if (response.isSuccessful())
                    {
                        return BuildJobResponse.ACCEPTED;
                    }
                    else
                    {
                        return BuildJobResponse.FAILED;
                    }
            }
        }
        else
        {
            logger.debug("Site {} doesn't support build triggering", site.getName());
            return BuildJobResponse.NOT_SUPPORTED;
        }
    }

    @Override
    public Status getRemoteStatus(Syncable<?> syncable)
    {
        return getRemoteStatus(syncable.getSite());
    }

    @Override
    public boolean populateJobDetails(Job job)
    {
        MergingParser<Job> parser = ParserProvider.jobParser();

        SiteUrl uri = syncUrl(job).job(job)
                .api();
        addFieldFilterIfNeeded(uri, parser.fields());
        SiteResponse response = get(uri);
        if (response.getStatusCode() == NOT_FOUND)
        {
            job.setDeleted(true);
        }
        else
        {
            response.parseJson(parser, job.setDeleted(false));
        }
        return true;
    }

    @Override
    public boolean populateBuildDetails(Build build)
    {
        MergingParser<Build> parser = ParserProvider.buildParser();

        SiteUrl uri = syncUrl(build).build(build)
                .api();
        addFieldFilterIfNeeded(uri, parser.fields());
        SiteResponse response = get(uri);
        if (response.getStatusCode() == NOT_FOUND)
        {
            build.setDeleted(true);
        }
        else
        {
            response.parseJson(parser, build.setDeleted(false));
        }
        return true;
    }

    @Override
    public void registerListener(SiteResponseListener listener)
    {
        listeners.get()
                .add(listener);
    }

    @Override
    public void unregisterListener(SiteResponseListener listener)
    {
        if (listeners.get()
                    .remove(listener) && listeners.get()
                    .isEmpty())
        {
            listeners.remove();
        }
    }

    protected void setHeaders(
            Site site,
            String requestMethod,
            URI requestUri,
            BiConsumer<String, String> header)
    {
        if (site.isTunneledSite())
        {
            Optional.ofNullable(tunnelManager.getTunnelDetails(site))
                    .map(SiteTunnelDetails::getToken)
                    .ifPresent(token -> header.accept("X-Tunnel-Token", token));
        }
        if (site.getSharedSecret() == null || Obfuscate.isObfuscated(site.getSharedSecret()))
        {
            return;
        }
        Optional<String> contextPath = Optional.ofNullable(site.getRpcUrl()
                        .getPath())
                .filter(StringUtils::isNotBlank);
        new SyncTokenBuilder().identifier(site.getId())
                .sharedSecret(site.getSharedSecret())
                .request(new CanonicalHttpServletRequest(requestMethod, requestUri, contextPath))
                .generateTokenAndAddHeaders(header);
    }

    protected void handleCrumbIfRequired(
            SiteUrl siteUrl,
            BiConsumer<String, String> action)
    {
        if (siteUrl.path()
                .filter(CRUMB_EXCLUSIONS::contains)
                .isPresent())
        {
            return;
        }
        Site site = siteUrl.site();
        if (site.isUseCrumbs())
        {
            get(syncUrl(site).path("crumbIssuer")
                    .api()).ifSuccessful(response -> response.parseJson(json -> {
                boolean added = false;
                if (json.getValueType() == JsonValue.ValueType.OBJECT)
                {
                    JsonObject object = json.asJsonObject();
                    if (object.containsKey("crumb") && object.containsKey("crumbRequestField"))
                    {
                        action.accept(object.getString("crumbRequestField"), object.getString("crumb"));
                        added = true;
                    }
                }
                if (!added)
                {
                    logger.error("Invalid crumb response received from remote {}", site.getName());
                }
                return null;
            }));
        }
    }

    private void addFieldFilterIfNeeded(
            SiteUrl siteUrl,
            List<String> fields)
    {
        if (!fields.isEmpty())
        {
            siteUrl.query("tree", String.join(",", fields));
        }
    }

    private <S extends Syncable<?>> SiteUrl syncUrl(S syncable)
    {
        Site site = syncable.getSite();
        if (site.isTunneledSite())
        {
            return new SiteUrl(tunnelManager::getTunnelDomainUrl).site(site);
        }
        else
        {
            return SiteUrl.syncUrl()
                    .site(site);
        }
    }

    private SiteResponse get(SiteUrl siteUrl)
    {
        SiteResponse response;
        if (!siteUrl.site()
                .isInaccessibleSite())
        {
            response = doGet(siteUrl);
        }
        else
        {
            response = new FirewalledSiteResponse(siteUrl.site());
        }
        getSiteResponseListeners().forEach(listener -> listener.get(siteUrl, response));
        return response;
    }

    protected abstract SiteResponse doGet(SiteUrl siteUrl);

    private SiteResponse post(
            SiteUrl siteUrl,
            JsonObject json)
    {
        SiteResponse response;
        if (!siteUrl.site()
                .isInaccessibleSite())
        {
            response = doPost(siteUrl, json);
        }
        else
        {
            response = new FirewalledSiteResponse(siteUrl.site());
        }
        getSiteResponseListeners().forEach(listener -> listener.post(siteUrl, json, response));
        return response;
    }

    protected abstract SiteResponse doPost(
            SiteUrl siteUrl,
            JsonObject json);
}
