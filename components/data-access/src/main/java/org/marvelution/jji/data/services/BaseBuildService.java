/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services;

import java.util.*;

import org.marvelution.jji.api.events.*;
import org.marvelution.jji.data.access.api.*;
import org.marvelution.jji.data.services.api.*;
import org.marvelution.jji.events.*;
import org.marvelution.jji.model.*;

import static java.util.Objects.*;
import static java.util.Optional.*;

/**
 * Base {@link BuildService} implementation.
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class BaseBuildService
		implements BuildService
{

	protected final BuildDAO buildDAO;
	protected final EventPublisher eventPublisher;

	protected BaseBuildService(
			BuildDAO buildDAO,
			EventPublisher eventPublisher)
	{
		this.buildDAO = requireNonNull(buildDAO);
		this.eventPublisher = eventPublisher;
	}

	@Override
	public Optional<Build> get(String buildId)
	{
		return ofNullable(buildDAO.get(buildId));
	}

	@Override
	public Optional<Build> get(
			Job job,
			int buildNumber)
	{
		return ofNullable(buildDAO.get(job.getId(), buildNumber));
	}

	@Override
	public Set<Build> getAllInRange(
			Job job,
			int start,
			int end)
	{
		return buildDAO.getAllInRange(job.getId(), start, end);
	}

	@Override
	public Set<Build> getByJob(
			Job job,
			int limit)
	{
		return buildDAO.getAllByJob(job.getId(), limit);
	}

	@Override
	public Build save(Build build)
	{
		return buildDAO.save(build);
	}

	@Override
	public void delete(Build build)
	{
		buildDAO.delete(build.getId());
		eventPublisher.publish(new BuildDeletedEvent(build));
	}

	@Override
	public void deleteAllInJob(Job job)
	{
		buildDAO.deleteAllByJob(job.getId());
		eventPublisher.publish(new BuildsDeletedEvent(job));
	}

	@Override
	public void markAsDeleted(Build build)
	{
		buildDAO.markAsDeleted(build.getId());
	}

	@Override
	public void markAllInJobAsDeleted(Job job)
	{
		buildDAO.markAllInJobAsDeleted(job.getId());
	}

	@Override
	public void markAllInJobAsDeleted(
			Job job,
			int buildNumber)
	{
		if (buildNumber > 0)
		{
			buildDAO.markAllInJobAsDeleted(job.getId(), buildNumber);
		}
	}
}
