/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services.api;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public interface LinkStatistician
{

    Response calculate(Request request);

    class Request
    {
        private Set<String> issueKeys;
        private boolean reindexIssue;
        private long timeLimit = Integer.MAX_VALUE;

        public Set<String> getIssueKeys()
        {
            return issueKeys;
        }

        public Request setIssueKeys(Set<String> issueKeys)
        {
            this.issueKeys = issueKeys;
            return this;
        }

        public boolean isReindexIssue()
        {
            return reindexIssue;
        }

        public Request setReindexIssue(boolean reindexIssue)
        {
            this.reindexIssue = reindexIssue;
            return this;
        }

        public long getTimeLimit()
        {
            return timeLimit;
        }

        public Request setTimeLimit(long timeLimit)
        {
            this.timeLimit = timeLimit;
            return this;
        }
    }

    class Response
    {
        private Map<String, Long> processedIssues = new HashMap<>();
        private Map<String, Exception> erroredIssues = new HashMap<>();
        private Set<String> unprocessedIssues = new HashSet<>();

        public Set<String> getProcessedIssues()
        {
            return processedIssues.keySet();
        }

        public void processedIssue(
                String issueKey,
                long elapsedTime)
        {
            processedIssues.put(issueKey, elapsedTime);
        }

        public long averageProcessingTime()
        {
            return processedIssues.values()
                           .stream()
                           .mapToLong(Long::longValue)
                           .sum() / processedIssues.size();
        }

        public Set<String> getErroredIssues()
        {
            return erroredIssues.keySet();
        }

        public Response erroredIssues(
                String issueKey,
                Exception error)
        {
            this.erroredIssues.put(issueKey, error);
            return this;
        }

        public Set<String> getUnprocessedIssues()
        {
            return unprocessedIssues;
        }

        public Response setUnprocessedIssues(Set<String> unprocessedIssues)
        {
            this.unprocessedIssues = new HashSet<>(unprocessedIssues);
            return this;
        }

        public boolean hasUnprocessedIssues()
        {
            return !unprocessedIssues.isEmpty();
        }
    }
}
