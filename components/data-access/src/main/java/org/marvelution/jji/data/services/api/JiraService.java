/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services.api;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.marvelution.jji.data.services.api.model.IssueField;
import org.marvelution.jji.data.services.api.model.Project;

public interface JiraService
{

    List<Project> getProjects(Set<String> keys);

    List<Project> getProjects(String query);

    default Optional<Project> getProject(String key)
    {
        return getProjects(Set.of(key)).stream()
                .findFirst();
    }

    List<IssueField> findIssueFields(String query);

    List<IssueField> getIssueFields(Set<String> ids);
}
