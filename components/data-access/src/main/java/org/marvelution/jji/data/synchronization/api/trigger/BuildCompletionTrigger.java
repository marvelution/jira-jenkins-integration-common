/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.synchronization.api.trigger;

import java.util.*;

import jakarta.json.bind.annotation.*;

/**
 * {@link TriggerReason} used when a build completion notification is received.
 *
 * @author Mark Rekveld
 * @since 1.8.0
 */
public class BuildCompletionTrigger
		extends TriggerReason
{

	private final String jobHash;
	private final Integer buildNumber;

	@JsonbCreator
	public BuildCompletionTrigger(
			@JsonbProperty String jobHash,
			@JsonbProperty Integer buildNumber)
	{
		this.jobHash = jobHash;
		this.buildNumber = buildNumber;
	}

	public String getJobHash()
	{
		return jobHash;
	}

	public Integer getBuildNumber()
	{
		return buildNumber;
	}

	@Override
	public String describe()
	{
		return "build completion notification of build " + buildNumber + " of " + jobHash;
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(jobHash, buildNumber, getData());
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}
		BuildCompletionTrigger that = (BuildCompletionTrigger) o;
		return jobHash.equals(that.jobHash) && buildNumber.equals(that.buildNumber) && Objects.equals(getData(), that.getData());
	}
}
