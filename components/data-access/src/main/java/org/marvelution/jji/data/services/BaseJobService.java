/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

import org.marvelution.jji.api.events.EventPublisher;
import org.marvelution.jji.api.text.TextResolver;
import org.marvelution.jji.data.access.api.JobDAO;
import org.marvelution.jji.data.services.api.BuildService;
import org.marvelution.jji.data.services.api.JobService;
import org.marvelution.jji.data.services.api.exceptions.CannotDeleteJobException;
import org.marvelution.jji.events.JobDeletedEvent;
import org.marvelution.jji.model.Job;
import org.marvelution.jji.model.Site;
import org.marvelution.jji.validation.api.ErrorMessages;
import org.marvelution.jji.validation.api.ValidationException;

import static java.util.Collections.emptyList;
import static java.util.Objects.requireNonNull;
import static java.util.Optional.empty;
import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

/**
 * Base {@link JobService} implementation.
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class BaseJobService
        implements JobService
{

    protected final JobDAO jobDAO;
    protected final BuildService buildService;
    protected final TextResolver textResolver;
    protected final EventPublisher eventPublisher;

    protected BaseJobService(
            JobDAO jobDAO,
            BuildService buildService,
            TextResolver textResolver,
            EventPublisher eventPublisher)
    {
        this.jobDAO = requireNonNull(jobDAO);
        this.buildService = requireNonNull(buildService);
        this.textResolver = requireNonNull(textResolver);
        this.eventPublisher = eventPublisher;
    }

    @Override
    public long countBySite(Site site)
    {
        return jobDAO.countBySite(site.getId());
    }

    @Override
    public boolean hasDeletedJobs(Site site)
    {
        return jobDAO.hasDeletedJobs(site.getId());
    }

    @Override
    public List<Job> getDeletedJobs(Site site)
    {
        return jobDAO.getDeletedJobs(site.getId());
    }

    @Override
    public List<Job> getRootJobsOnSite(Site site)
    {
        return getJobsAtLevel(site, null);
    }

    @Override
    public Optional<Job> getParent(Job job)
    {
        String parentName = job.getParentName();
        if (isBlank(parentName))
        {
            return empty();
        }
        int index = parentName.lastIndexOf("/");
        if (index > 0)
        {
            return jobDAO.find(job.getSite()
                    .getId(), parentName.substring(index + 1), parentName.substring(0, index));
        }
        else
        {
            return jobDAO.find(job.getSite()
                    .getId(), parentName, null);
        }
    }

    @Override
    public List<Job> getParents(Job job)
    {
        if (job.getParentName() != null)
        {
            List<Job> parents = new ArrayList<>();
            Optional<Job> parent = getParent(job);
            while (parent.isPresent())
            {
                parents.add(0, parent.get());
                parent = getParent(parent.get());
            }
            return parents;
        }
        else
        {
            return emptyList();
        }
    }

    @Override
    public List<Job> getSubJobs(Job job)
    {
        return getJobsAtLevel(job.getSite(), job.getFullName());
    }

    @Override
    public Optional<Job> get(
            String jobId,
            boolean includeSubJobs)
    {
        Job job = jobDAO.get(jobId);
        if (job != null && includeSubJobs)
        {
            job.setJobs(getJobsAtLevel(job.getSite(), job.getFullName()));
        }
        return ofNullable(job);
    }

    @Override
    public Optional<Job> findByHash(
            Site site,
            String hash)
    {
        return jobDAO.findByHash(site.getId(), hash)
                .stream()
                .findFirst();
    }

    @Override
    public List<Job> find(String name)
    {
        return jobDAO.find(name);
    }

    @Override
    public List<Job> search(
            String term,
            boolean exactMatch)
    {
        if (exactMatch)
        {
            return jobDAO.find(term);
        }
        else
        {
            return jobDAO.search(term);
        }
    }

    @Override
    public List<Job> search(
            Site site,
            String term)
    {
        return jobDAO.search(site.getId(), term);
    }

    @Override
    public void enable(
            Job job,
            boolean enabled)
    {
        job.setLinked(enabled);
        save(job);
    }

    @Override
    public Job save(Job job)
    {
        assertValidJob(job);
        return jobDAO.save(job);
    }

    @Override
    public void delete(Job job)
    {
        if (isNotBlank(job.getId()) && job.isDeleted())
        {
            jobDAO.delete(job.getId());
            eventPublisher.publish(new JobDeletedEvent(job));
        }
        else
        {
            throw new CannotDeleteJobException(job);
        }
    }

    @Override
    public void deleteAllBuilds(Job job)
    {
        resetJobBuilds(job, buildService::deleteAllInJob);
    }

    @Override
    public void resetBuildCache(Job job)
    {
        resetJobBuilds(job, buildService::markAllInJobAsDeleted);
    }

    @Override
    public List<Job> getLatestActiveJobs(int limit)
    {
        return jobDAO.getLatestActiveJobs(limit);
    }

    private List<Job> getJobsAtLevel(
            Site site,
            String level)
    {
        return jobDAO.getBySiteIdAtLevel(site.getId(), level, true)
                .stream()
                .sorted(Comparator.comparing(Job::getDisplayName))
                .collect(toList());
    }

    protected void assertValidJob(Job job)
    {
        ErrorMessages errorMessages = validateJob(job);
        if (errorMessages.hasErrors())
        {
            throw new ValidationException(errorMessages);
        }
    }

    protected ErrorMessages validateJob(Job job)
    {
        ErrorMessages errorMessages = new ErrorMessages();
        if (isBlank(job.getName()))
        {
            errorMessages.addError("name", textResolver.getText("name.required"));
        }
        if (job.getSite() == null || isBlank(job.getSite()
                .getId()))
        {
            errorMessages.addError("site", textResolver.getText("job.site.required"));
        }
        return errorMessages;
    }

    private void resetJobBuilds(
            Job job,
            Consumer<Job> callback)
    {
        job.setOldestBuild(0)
                .setLastBuild(0);
        callback.accept(save(job));
    }
}
