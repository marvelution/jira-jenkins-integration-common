/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.helper;

import java.net.*;
import java.util.*;
import javax.annotation.*;

import org.marvelution.jji.model.*;

import static java.util.stream.Collectors.*;

/**
 * {@link Job} model specific for use on panels.
 *
 * @author Mark Rekveld
 * @since 1.5.0
 */
public class PanelJob
{

	private final Job job;
	private final List<PanelBuild> builds;
	private PanelBuild lastBuild;

	public PanelJob(Job job)
	{
		this(job, job.getBuilds().stream().map(PanelBuild::new).collect(toList()));
	}

	public PanelJob(
			Job job,
			List<PanelBuild> builds)
	{
		this.job = job;
		this.builds = builds;
	}

	public String getId()
	{
		return job.getId();
	}

	public Job getJob()
	{
		return job;
	}

	public String getDisplayName()
	{
		return job.getDisplayName();
	}

	public URI getDisplayUrl()
	{
		return job.getDisplayUrl();
	}

	public boolean isDeleted()
	{
		return job.isDeleted();
	}

	public List<PanelBuild> getBuilds()
	{
		return builds;
	}

	public boolean hasLastBuild()
	{
		return lastBuild != null && builds.stream().anyMatch(b -> b.getNumber() == job.getLastBuild());
	}

	public PanelBuild getLastBuild()
	{
		return Optional.ofNullable(lastBuild).orElseGet(this::locateLastBuild);
	}

	public void setLastBuild(PanelBuild lastBuild)
	{
		this.lastBuild = lastBuild;
	}

	@Nullable
	private PanelBuild locateLastBuild()
	{
		Optional<PanelBuild> build = builds.stream().filter(b -> b.getNumber() == job.getLastBuild()).findFirst();
		return build.orElseGet(() -> builds.stream().max(Comparator.comparingInt(PanelBuild::getNumber)).orElse(null));
	}
}
