/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.synchronization.api.result;

import java.time.*;
import java.util.*;
import javax.annotation.*;

import org.marvelution.jji.data.synchronization.api.trigger.*;
import org.marvelution.jji.model.*;

public interface ImmutableSynchronizationResult
{

	String getId();

	OperationId getOperationId();

	TriggerReason triggerReason();

	default <T extends TriggerReason> Optional<T> triggerReason(Class<T> type)
	{
		return Optional.of(triggerReason()).filter(type::isInstance).map(type::cast);
	}

	boolean isQueued();

	boolean hasStarted();

	boolean isFinished();

	boolean stopRequested();

	Duration queuedDuration();

	Duration executionDuration();

	Duration completedDuration();

	SyncProgress asSyncProgress();

	default boolean hasErrors()
	{
		return !getErrors().isEmpty();
	}

	List<Error> getErrors();

	interface Error
	{

		String getMessage();

		@Nullable
		String getDetails();
	}
}
