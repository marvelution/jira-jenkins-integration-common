/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.synchronization.api.context;

import java.net.URI;
import java.time.Duration;
import java.util.*;
import java.util.stream.Collectors;
import jakarta.json.JsonObject;

import org.marvelution.jji.api.events.EventPublisher;
import org.marvelution.jji.data.services.api.SiteDataClient;
import org.marvelution.jji.data.services.api.SiteResponse;
import org.marvelution.jji.data.services.api.SiteResponseListener;
import org.marvelution.jji.data.synchronization.api.SynchronizationService;
import org.marvelution.jji.data.synchronization.api.result.SynchronizationResult;
import org.marvelution.jji.data.synchronization.api.trigger.TriggerReason;
import org.marvelution.jji.data.synchronization.api.trigger.UpstreamTrigger;
import org.marvelution.jji.events.BuildSynchronizedEvent;
import org.marvelution.jji.events.JobSynchronizedEvent;
import org.marvelution.jji.events.SiteSynchronizedEvent;
import org.marvelution.jji.model.*;
import org.marvelution.jji.utils.SiteUrl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.joining;

class SimpleSynchronizationContext<S extends Syncable<S>, R extends SynchronizationResult>
        implements SynchronizationContext<S>
{

    private static final Logger LOGGER = LoggerFactory.getLogger(SimpleSynchronizationContext.class);
    private final SynchronizationService synchronizationService;
    private final EventPublisher eventPublisher;
    private final SiteDataClient siteDataClient;
    private final S syncable;
    private final R result;
    private final Set<Syncable<?>> synced = new HashSet<>();
    private final SiteResponseListener siteResponseListener = new SiteResponseListener()
    {
        @Override
        public void get(
                SiteUrl siteUrl,
                SiteResponse response)
        {
            if (shouldRecordError(response))
            {
                String message = "";
                if (siteUrl.build()
                        .isPresent())
                {
                    message = " for build " + siteUrl.build()
                            .get()
                            .getDisplayName();
                }
                else if (siteUrl.job()
                        .isPresent())
                {
                    message = " for job " + siteUrl.job()
                            .get()
                            .getDisplayName();
                }
                recordError("Failed to load data" + message + " from " + siteUrl.site()
                        .getName(), siteUrl, response);
            }
        }

        @Override
        public void post(
                SiteUrl siteUrl,
                JsonObject json,
                SiteResponse response)
        {
            if (shouldRecordError(response))
            {
                String message = "";
                if (siteUrl.build()
                        .isPresent())
                {
                    message = " for build " + siteUrl.build()
                            .get()
                            .getDisplayName();
                }
                else if (siteUrl.job()
                        .isPresent())
                {
                    message = " for job " + siteUrl.job()
                            .get()
                            .getDisplayName();
                }
                recordError("Failed to send data" + message + " to " + siteUrl.site()
                        .getName(), siteUrl, response);
            }
        }

        private void recordError(
                String message,
                SiteUrl siteUrl,
                SiteResponse response)
        {
            List<String> errors = new ArrayList<>();
            errors.add("URL: " + Optional.ofNullable(siteUrl.url())
                    .map(URI::toASCIIString)
                    .orElseGet(siteUrl::toString));
            errors.add("Status Code: " + response.getStatusCode());
            errors.add("Status Message: " + response.getStatusMessage());
            errors.addAll(response.getErrors());
            addError(message,
                    errors.stream()
                            .collect(joining("\n - ", " - ", "")));
        }

        private boolean shouldRecordError(SiteResponse response)
        {
            return !response.isSuccessful() || response.hasErrors();
        }
    };

    SimpleSynchronizationContext(
            SynchronizationService synchronizationService,
            EventPublisher eventPublisher,
            SiteDataClient siteDataClient,
            S syncable,
            R result)
    {
        this.synchronizationService = requireNonNull(synchronizationService);
        this.eventPublisher = requireNonNull(eventPublisher);
        this.siteDataClient = requireNonNull(siteDataClient);
        this.syncable = requireNonNull(syncable);
        this.result = requireNonNull(result);
    }

    @Override
    public S getSyncable()
    {
        return syncable;
    }

    @Override
    public <S1 extends Syncable<S1>> void synchronize(S1 syncable)
    {
        synchronizationService.synchronize(syncable, new UpstreamTrigger(this));
    }

    @Override
    public Status getSyncableStatus()
    {
        return siteDataClient.getRemoteStatus(syncable);
    }

    @Override
    public boolean populateJobDetails(Job job)
    {
        return siteDataClient.populateJobDetails(job);
    }

    @Override
    public boolean populateBuildDetails(Build build)
    {
        return siteDataClient.populateBuildDetails(build);
    }

    @Override
    public String getId()
    {
        return result.getId();
    }

    @Override
    public TriggerReason triggerReason()
    {
        return result.triggerReason();
    }

    @Override
    public <T extends TriggerReason> Optional<T> triggerReason(Class<T> type)
    {
        return result.triggerReason(type);
    }

    @Override
    public boolean isQueued()
    {
        return result.isQueued();
    }

    @Override
    public boolean hasStarted()
    {
        return result.hasStarted();
    }

    @Override
    public boolean isFinished()
    {
        return result.isFinished();
    }

    @Override
    public boolean stopRequested()
    {
        return result.stopRequested();
    }

    @Override
    public Duration queuedDuration()
    {
        return result.queuedDuration();
    }

    @Override
    public Duration executionDuration()
    {
        return result.executionDuration();
    }

    @Override
    public Duration completedDuration()
    {
        return result.completedDuration();
    }

    @Override
    public SyncProgress asSyncProgress()
    {
        return result.asSyncProgress();
    }

    @Override
    public List<Error> getErrors()
    {
        return result.getErrors();
    }

    @Override
    public void queued()
    {
        result.queued();
    }

    @Override
    public void started()
    {
        LOGGER.debug("Started synchronization of {}", syncable);
        result.started();
        siteDataClient.registerListener(siteResponseListener);
    }

    @Override
    public void finished()
    {
        LOGGER.debug("Finished synchronization of {}", syncable);
        siteDataClient.unregisterListener(siteResponseListener);
        result.finished();
        if (result.hasErrors())
        {
            LOGGER.warn("Synchronization of {} has errors: \n{}",
                    syncable.getOperationId(),
                    result.getErrors()
                            .stream()
                            .map(error -> "* " + error.getMessage() + "\n" + error.getDetails())
                            .collect(joining("\n")));
        }
        if (syncable instanceof Site)
        {
            LOGGER.debug("Publishing new site synchronized event");
            eventPublisher.publish(new SiteSynchronizedEvent((Site) syncable));
            getSyncedOfType(Job.class).stream()
                    .map(JobSynchronizedEvent::new)
                    .forEach(eventPublisher::publish);
        }
        else if (syncable instanceof Job)
        {
            LOGGER.debug("Publishing new job synchronized event");
            eventPublisher.publish(new JobSynchronizedEvent((Job) syncable));
            getSyncedOfType(Job.class).stream()
                    .map(JobSynchronizedEvent::new)
                    .forEach(eventPublisher::publish);
            getSyncedOfType(Build.class).stream()
                    .map(BuildSynchronizedEvent::new)
                    .forEach(eventPublisher::publish);
        }
        else if (syncable instanceof Build)
        {
            LOGGER.debug("Publishing new build synchronized event");
            Build build = getSyncedOfType(Build.class).stream()
                    .findFirst()
                    .orElse((Build) syncable);
            eventPublisher.publish(new BuildSynchronizedEvent(build));
        }
        else
        {
            LOGGER.warn("No event published for syncables of type {}", syncable.getClass());
        }
    }

    @Override
    public void shouldStop()
    {
        result.shouldStop();
    }

    @Override
    public void synchronizedJob(Job job)
    {
        result.synchronizedJob(job);
        synced.add(job);
    }

    @Override
    public void synchronizedBuild(Build build)
    {
        result.synchronizedBuild(build);
        synced.add(build);
    }

    @Override
    public void linkedIssues(Set<IssueReference> references)
    {
        result.linkedIssues(references);
    }

    @Override
    public void addError(Throwable error)
    {
        result.addError(error);
    }

    @Override
    public void addError(String message)
    {
        result.addError(message);
    }

    @Override
    public void addError(
            String message,
            String details)
    {
        result.addError(message, details);
    }

    private <T extends Syncable<T>> List<T> getSyncedOfType(Class<T> type)
    {
        return synced.stream()
                .filter(type::isInstance)
                .map(type::cast)
                .collect(Collectors.toList());
    }
}
