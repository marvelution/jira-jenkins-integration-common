/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services.api;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.marvelution.jji.api.features.FeatureFlags;
import org.marvelution.jji.api.features.Features;
import org.marvelution.jji.model.Job;
import org.marvelution.jji.model.Site;
import org.marvelution.jji.utils.KeyExtractor;
import org.marvelution.jji.data.services.api.model.PermissionKeys;

import static org.apache.commons.lang3.StringUtils.isBlank;

public abstract class PermissionService<U>
{

    protected final Features features;

    protected PermissionService(Features features)
    {
        this.features = features;
    }

    protected abstract U getCurrentUser();

    public boolean isSiteAdministrator(U user)
    {
        return hasPermission(user, PermissionKeys.ADMINISTER);
    }

    public boolean isScopeAdministrator(
            U user,
            String scope)
    {
        if (ConfigurationService.GLOBAL_SCOPE.equals(scope))
        {
            return isSiteAdministrator(user);
        }
        else if (isSiteAdministrator(user))
        {
            return true;
        }
        else if (features.isFeatureEnabled(FeatureFlags.PROJECT_CONFIGURATION))
        {
            return getPermittedProjects(user, PermissionKeys.ADMINISTER_PROJECTS).contains(scope);
        }
        else
        {
            return false;
        }
    }

    public boolean canTriggerBuilds(
            U user,
            String issueKey)
    {
        if (isBlank(issueKey))
        {
            return hasPermission(user, null, PermissionKeys.TRIGGER_JENKINS_BUILD);
        }
        else
        {
            String projectKey;
            try
            {
                projectKey = KeyExtractor.extractProjectKeyFromIssueKey(issueKey);
            }
            catch (IllegalArgumentException ignored)
            {
                projectKey = issueKey;
            }
            return hasPermission(user, projectKey, PermissionKeys.TRIGGER_JENKINS_BUILD);
        }
    }

    public boolean canTriggerBuilds(Job job)
    {
        return canTriggerBuilds(getCurrentUser(),
                job.getSite()
                        .getScope());
    }

    public abstract boolean hasPermission(
            U user,
            String permissionKey);

    public abstract boolean hasPermission(
            U user,
            String projectKey,
            String permissionKey);

    public abstract Set<String> getPermittedProjects(
            U user,
            String permission);

    public boolean isAdministrator(
            U user,
            Site site)
    {
        return isScopeAdministrator(user, site.getScope());
    }

    public boolean isAdministrator(
            U user,
            Job job)
    {
        return isAdministrator(user, job.getSite());
    }

    public List<Site> filterPermittedSites(
            U user,
            List<Site> sites)
    {
        if (isSiteAdministrator(user))
        {
            return sites;
        }
        else if (features.isFeatureEnabled(FeatureFlags.PROJECT_CONFIGURATION))
        {
            Set<String> scopes = getPermittedProjects(user, PermissionKeys.ADMINISTER_PROJECTS);
            return sites.stream()
                    .filter(site -> ConfigurationService.GLOBAL_SCOPE.equals(site.getScope()) || scopes.contains(site.getScope()))
                    .collect(Collectors.toList());
        }
        else
        {
            return Collections.emptyList();
        }
    }

    public List<Job> filterPermittedJobs(
            U user,
            List<Job> jobs)
    {
        if (isSiteAdministrator(user))
        {
            return jobs;
        }
        else if (features.isFeatureEnabled(FeatureFlags.PROJECT_CONFIGURATION))
        {
            Set<String> scopes = getPermittedProjects(user, PermissionKeys.ADMINISTER_PROJECTS);
            return jobs.stream()
                    .filter(job -> ConfigurationService.GLOBAL_SCOPE.equals(job.getSite()
                            .getScope()) || scopes.contains(job.getSite()
                            .getScope()))
                    .collect(Collectors.toList());
        }
        else
        {
            return Collections.emptyList();
        }
    }
}
