/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access.api;

import java.util.*;

import org.marvelution.jji.data.services.api.IssueLinkService.*;
import org.marvelution.jji.model.*;

public interface IssueToBuildDAO
{

    Set<String> getAllIssueKeysWithLinks(
            int limit,
            int page);

    Set<String> getProjectKeysWithLinks();

    int getLinkCountForIssue(String issueKey);

    int getLinkCountForProject(String projectKey);

    Set<Build> getBuildsForIssue(
            String issueKey,
            int limit,
            boolean includeIssueReferences);

    Set<Build> getBuildsForLinkRequest(
            LinkRequest request,
            int limit,
            boolean includeIssueReferences);

    Set<Job> getJobsForIssue(
            boolean includeIssueReferences,
            String... issueKey);

	Set<String> getIssueKeys(Build build);

	Set<String> getIssueKeys(Job job);

    Set<IssueReference> getIssueLinks(Build build);

    Set<IssueReference> getIssueLinks(Job job);

    void link(
            Build build,
            IssueReference issue);

    void relink(
            Build build,
            Set<IssueReference> references);

    void unlinkForIssue(String... issueKey);

    void unlinkForProject(String projectKey);
}
