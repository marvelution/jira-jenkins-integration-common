/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.concurrent.Executor;
import jakarta.json.Json;
import jakarta.json.JsonException;
import jakarta.json.JsonReader;
import jakarta.json.JsonStructure;

import org.marvelution.jji.api.AppState;
import org.marvelution.jji.data.services.api.SiteClient;
import org.marvelution.jji.data.synchronization.api.SynchronizationService;
import org.marvelution.jji.data.synchronization.api.exceptions.SynchronizationTriggerException;
import org.marvelution.jji.data.synchronization.api.result.ImmutableSynchronizationResult;
import org.marvelution.jji.data.synchronization.api.result.SynchronizationResult;
import org.marvelution.jji.data.synchronization.api.trigger.AbstractSiteTrigger;
import org.marvelution.jji.data.synchronization.api.trigger.TriggerReason;
import org.marvelution.jji.model.OperationId;
import org.marvelution.jji.model.Site;
import org.marvelution.jji.model.Status;
import org.marvelution.jji.model.Syncable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.util.Optional.empty;
import static java.util.Optional.of;

public abstract class BaseSynchronizationService<R extends SynchronizationResult>
        implements SynchronizationService
{

    protected final Logger logger = LoggerFactory.getLogger(getClass());
    protected final AppState appState;
    private final SiteClient siteClient;
    private final Executor asyncExecutor;

    protected BaseSynchronizationService(
            AppState appState,
            SiteClient siteClient,
            Executor asyncExecutor)
    {
        this.appState = appState;
        this.siteClient = siteClient;
        this.asyncExecutor = asyncExecutor;
    }

    @Override
    public Optional<String> synchronize(
            Syncable<?> syncable,
            TriggerReason triggerReason)
    {
        return createResultSafely(syncable, triggerReason).map(result -> {
            logger.debug("Scheduling synchronization for {}", syncable.getOperationId());
            if (syncable instanceof Site && triggerReason instanceof AbstractSiteTrigger)
            {
                enqueueSiteOperation((Site) syncable, result);
            }
            else
            {
                enqueueResult(syncable, result);
            }
            return result.getId();
        });
    }

    @Override
    public Optional<String> synchronize(
            Syncable<?> syncable,
            TriggerReason triggerReason,
            JsonStructure json)
    {
        triggerReason.setData(json.toString());
        return synchronize(syncable, triggerReason);
    }

    @Override
    public Optional<String> synchronize(
            Syncable<?> syncable,
            TriggerReason triggerReason,
            InputStream inputStream,
            Charset charset)
    {
        try (Reader reader = new InputStreamReader(inputStream, charset);
             JsonReader jsonReader = Json.createReader(reader))
        {
            return synchronize(syncable, triggerReason, jsonReader.read());
        }
        catch (IOException | JsonException e)
        {
            throw new SynchronizationTriggerException("Failed to read json data to be used by the synchronization operation.", e);
        }
        catch (Exception e)
        {
            throw new SynchronizationTriggerException("Failed to trigger synchronization operation.", e);
        }
    }

    @Override
    public void stopSynchronization(Syncable<?> syncable)
    {
        findLatestResult(syncable.getOperationId()).filter(result -> !result.isFinished())
                .ifPresent(result -> {
                    logger.debug("Requesting synchronization stop for {}", syncable.getOperationId());
                    stopSynchronization(result);
                });
    }

    @Override
    public Optional<ImmutableSynchronizationResult> getSynchronizationResult(Syncable<?> syncable)
    {
        return findLatestResult(syncable.getOperationId()).map(ImmutableSynchronizationResult.class::cast);
    }

    @SuppressWarnings("BusyWait")
    private void enqueueSiteOperation(
            Site site,
            R result)
    {
        asyncExecutor.execute(() -> {
            LocalDateTime until = LocalDateTime.now()
                    .plusMinutes(10);
            Status status = Status.OFFLINE;
            while (LocalDateTime.now()
                    .isBefore(until))
            {
                status = siteClient.getRemoteStatus(site);
                if (status == Status.GETTING_READY)
                {
                    logger.debug("{} is {}, sleeping for 30 seconds before checking again.", site, status);
                    try
                    {
                        Thread.sleep(Duration.ofSeconds(30)
                                .toMillis());
                    }
                    catch (InterruptedException e)
                    {
                        throw new SynchronizationTriggerException("Got interrupted checking the status of " + site.getName(), e);
                    }
                }
                else
                {
                    break;
                }
            }
            if (status.isAccessible() || result.triggerReason()
                    .hasData())
            {
                logger.debug("Enqueueing operation; {} is {}", site, status);
                enqueueResult(site, result);
            }
            else
            {
                logger.debug("No point in syncing site if not accessible; {} is {}", site, status);
                handleEnqueueingError(result, new SynchronizationTriggerException(site.getName() + " was not accessible."));
            }
        });
    }

    private void enqueueResult(
            Syncable<?> syncable,
            R result)
    {
        markQueued(result);
        try
        {
            enqueueSyncable(syncable, result);
        }
        catch (Exception e)
        {
            logger.error("Unable to enqueue {} for synchronization; {}", syncable.getOperationId(), e.getMessage(), e);
            handleEnqueueingError(result, e);
        }
    }

    protected void markQueued(R result)
    {
        result.queued();
    }

    protected void handleEnqueueingError(
            R result,
            Exception e)
    {
        result.addError(e);
        result.finished();
    }

    protected void stopSynchronization(R result)
    {
        result.shouldStop();
    }

    protected abstract void enqueueSyncable(
            Syncable<?> syncable,
            R result);

    protected abstract Optional<R> findLatestResult(OperationId operationId);

    private boolean skipSynchronization(
            OperationId operationId)
    {
        Optional<R> result = findLatestResult(operationId);
        if (result.isEmpty() || result.get()
                .isFinished())
        {
            logger.debug("Don't skip, either this is the first run or the previous has finished");
            return false;
        }
        else
        {
            logger.debug("Skip the execution, there is a non-finished previous execution.");
            return true;
        }
    }

    protected abstract R createResult(
            OperationId operationId,
            TriggerReason triggerReason);

    private Optional<R> createResultSafely(
            Syncable<?> syncable,
            TriggerReason triggerReason)
    {
        if (!appState.isEnabled())
        {
            logger.debug("Synchronization is not enabled, skipping synchronization for {}", syncable.getOperationId());
        }
        else if (!syncable.isEnabled())
        {
            logger.debug("Syncable {} not enabled, skipping synchronization for {}", syncable, syncable.getOperationId());
        }
        else if (skipSynchronization(syncable.getOperationId()))
        {
            logger.debug("Skipping synchronization for {} based on last results", syncable.getOperationId());
        }
        else if (syncable.getSite()
                         .isInaccessibleSite() && !triggerReason.hasData())
        {
            logger.debug("Skipping synchronization of {}, site is private and the trigger is missing data", syncable.getOperationId());
        }
        else
        {
            return of(createResult(syncable.getOperationId(), triggerReason));
        }
        return empty();
    }
}
