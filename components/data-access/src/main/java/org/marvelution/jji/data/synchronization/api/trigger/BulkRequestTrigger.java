/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.synchronization.api.trigger;

import java.util.*;

import org.marvelution.jji.model.request.*;

/**
 * {@link TriggerReason} used when synchronization is triggered via a {@link BulkRequest}.
 *
 * @author Mark Rekveld
 * @since 1.8.0
 */
public class BulkRequestTrigger
		extends TriggerReason
{

	@Override
	public String describe()
	{
		return "bulk request";
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(getData());
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}
		BulkRequestTrigger that = (BulkRequestTrigger) o;
		return Objects.equals(getData(), that.getData());
	}
}
