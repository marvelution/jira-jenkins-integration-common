/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.api.text;

import java.util.Locale;
import java.util.Properties;

/**
 * API to resolve Text.
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public interface TextResolver {

	/**
	 * Resolves the specified {@code key} to text using the {@code arguments} provided.
	 */
	String getText(String key, Object... arguments);

	/**
	 * Resolves the specified {@code key} to text using the {@code arguments} provided in the specified {@link Locale}.
	 */
	String getText(Locale locale, String key, Object... arguments);

	/**
	 * Resolves all text keys for the specified {@link Locale}.
	 */
	default Properties getTexts(Locale locale) {
		throw new UnsupportedOperationException();
	}
}
