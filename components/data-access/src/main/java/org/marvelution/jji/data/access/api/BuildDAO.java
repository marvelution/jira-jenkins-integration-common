/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access.api;

import javax.annotation.Nullable;

import org.marvelution.jji.model.Build;
import org.marvelution.jji.model.DeploymentEnvironment;

import java.time.Instant;
import java.util.Set;

public interface BuildDAO
        extends BaseDAO<Build>
{

    @Nullable
    Build get(
            String jobId,
            int buildNumber);

    Set<Build> getAllInRange(
            String jobId,
            int start,
            int end);

    Set<Build> getAll(
            String jobId,
            int page,
            int size);

    default Set<Build> getAllByJob(String jobId)
    {
        return getAllByJob(jobId, -1);
    }

    Set<Build> getAllByJob(
            String jobId,
            int limit);

    void deleteAllByJob(String jobId);

    void markAsDeleted(String buildId);

    default void markAllInJobAsDeleted(String jobId)
    {
        markAllInJobAsDeleted(jobId, -1);
    }

    void markAllInJobAsDeleted(
            String jobId,
            int buildNumber);

    Set<DeploymentEnvironment> getDeploymentEnvironments();

    void cleanupOldDeletedBuilds(Instant timestamp);
}
