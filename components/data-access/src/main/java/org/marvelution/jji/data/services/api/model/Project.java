/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services.api.model;

import java.util.*;

import com.fasterxml.jackson.annotation.*;

public class Project
		extends ProjectIdentifier
{
	private String name;
	private String projectTypeKey;
	private AvatarUrls avatarUrls;
	private List<IssueType> issueTypes;
	@JsonProperty
	private Map<String, Object> fields = new HashMap<>();

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getProjectTypeKey()
	{
		return projectTypeKey;
	}

	public void setProjectTypeKey(String projectTypeKey)
	{
		this.projectTypeKey = projectTypeKey;
	}

	public AvatarUrls getAvatarUrls()
	{
		return avatarUrls;
	}

	public void setAvatarUrls(AvatarUrls avatarUrls)
	{
		this.avatarUrls = avatarUrls;
	}

	public List<IssueType> getIssueTypes()
	{
		return issueTypes;
	}

	public void setIssueTypes(List<IssueType> issueTypes)
	{
		this.issueTypes = issueTypes;
	}

	@JsonAnyGetter
	public Map<String, Object> get()
	{
		return fields;
	}

	@JsonAnySetter
	public void set(
			String key,
			Object value)
	{
		fields.put(key, value);
	}
}
