/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.synchronization.api;

import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Optional;
import jakarta.json.JsonStructure;

import org.marvelution.jji.data.synchronization.api.result.ImmutableSynchronizationResult;
import org.marvelution.jji.data.synchronization.api.result.SynchronizationResult;
import org.marvelution.jji.data.synchronization.api.trigger.TriggerReason;
import org.marvelution.jji.model.Syncable;

/**
 * Synchronizer interface for synchronizing builds of jobs.
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public interface SynchronizationService
{

    /**
     * Schedule the specified {@link Syncable} for execution.
     * Returns the reference to the {@link SynchronizationResult} if the {@link Syncable} was scheduled.
     */
    Optional<String> synchronize(
            Syncable<?> syncable,
            TriggerReason triggerReason);

    /**
     * Schedule the specified {@link Syncable} for execution, storing the json data provided by the specified {@link JsonStructure} to be
     * used by the synchronization process. Returns the reference to the {@link SynchronizationResult} if the {@link Syncable} was
     * scheduled.
     *
     * @since 1.9.0
     */
    Optional<String> synchronize(
            Syncable<?> syncable,
            TriggerReason triggerReason,
            JsonStructure json);

    /**
     * Schedule the specified {@link Syncable} for execution, storing the json data provided by the specified {@link InputStream} to be
     * used by the synchronization process. Returns the reference to the {@link SynchronizationResult} if the {@link Syncable} was
     * scheduled.
     *
     * @since 1.9.0
     */
    Optional<String> synchronize(
            Syncable<?> syncable,
            TriggerReason triggerReason,
            InputStream inputStream,
            Charset charset);

    /**
     * Request the operation with the specified {@link Syncable} to be stopped.
     */
    void stopSynchronization(Syncable<?> syncable);

    /**
     * Returns the {@link ImmutableSynchronizationResult} of the {@link Syncable} with the specified {@link Syncable}.
     */
    Optional<ImmutableSynchronizationResult> getSynchronizationResult(Syncable<?> syncable);

    /**
     * Returns the {@link ImmutableSynchronizationResult} with the specified {@code reference}.
     *
     * @since 1.18
     */
    Optional<ImmutableSynchronizationResult> getSynchronizationResult(String reference);

    List<ImmutableSynchronizationResult> findLastResults(int limit);
}
