/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.synchronization.api.trigger;

import java.lang.reflect.*;
import java.util.*;

import jakarta.json.*;
import jakarta.json.bind.*;
import jakarta.json.bind.serializer.*;
import jakarta.json.stream.*;
import org.eclipse.yasson.*;

/**
 * Helper for {@link TriggerReason}s.
 *
 * @author Mark Rekveld
 * @since 1.8.0
 */
public final class TriggerReasons
{

	public static final TriggerReasonAdapter TRIGGER_REASON_ADAPTER = new TriggerReasonAdapter();
	private static final Jsonb JSONB = JsonbBuilder.newBuilder()
			.withConfig(new JsonbConfig().withSerializers(TRIGGER_REASON_ADAPTER).withDeserializers(TRIGGER_REASON_ADAPTER))
			.build();

	/**
	 * Returns the JSON representation of the specified {@link TriggerReason}.
	 */
	public static String asJson(TriggerReason triggerReason)
	{
		return JSONB.toJson(triggerReason, TriggerReason.class);
	}

	/**
	 * Returns the {@link TriggerReason} representation of the specified JSON.
	 */
	public static TriggerReason fromJson(String json)
	{
		return JSONB.fromJson(json, TriggerReason.class);
	}

	private static class TriggerReasonAdapter
			implements JsonbSerializer<TriggerReason>, JsonbDeserializer<TriggerReason>
	{

		private static final YassonJsonb YASSONB = (YassonJsonb) JsonbBuilder.create();
		private static final String TYPE_FIELD = "__type__";

		@Override
		public void serialize(
				TriggerReason triggerReason,
				JsonGenerator generator,
				SerializationContext ctx)
		{
			generator.writeStartObject();
			JsonObject object = YASSONB.toJsonStructure(triggerReason).asJsonObject();
			for(Map.Entry<String, JsonValue> member: object.entrySet()) {
				generator.write(member.getKey(), member.getValue());
			}
			generator.write(TYPE_FIELD, triggerReason.getClass().getName());
			generator.writeEnd();
		}

		@Override
		@SuppressWarnings("unchecked")
		public TriggerReason deserialize(
				JsonParser parser,
				DeserializationContext ctx,
				Type rtType)
		{
			JsonObject object = parser.getObject();
			String typeName;
			try
			{
				typeName = object.getString(TYPE_FIELD);
			}
			catch (Exception e)
			{
				throw new JsonbException("cannot deserialize trigger reason to specific type specified", e);
			}

			Class<? extends TriggerReason> type;
			try
			{
				type = (Class<? extends TriggerReason>) getClass().getClassLoader().loadClass(typeName);
			}
			catch (ClassNotFoundException e)
			{
				throw new JsonbException("cannot load trigger reason type " + typeName, e);
			}
			catch (Throwable e)
			{
				throw new JsonbException("cannot deserialize type " + typeName, e);
			}
			return YASSONB.fromJsonStructure(Json.createObjectBuilder(object).remove(TYPE_FIELD).build(), type);
		}
	}
}
