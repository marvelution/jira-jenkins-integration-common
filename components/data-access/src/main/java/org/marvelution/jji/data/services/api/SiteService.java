/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services.api;

import javax.annotation.*;
import java.util.*;

import org.marvelution.jji.data.services.api.exceptions.*;
import org.marvelution.jji.model.*;
import org.marvelution.jji.model.response.*;
import org.marvelution.jji.validation.api.*;

/**
 * {@link Site} services.
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public interface SiteService
{

    default Site getNewSiteStartingPoint(
            @Nullable
            String scope)
    {
        return new Site().setType(SiteType.JENKINS)
                         .setScope(scope)
                         .setAutoLinkNewJobs(true)
                         .setUseCrumbs(true);
    }

    /**
     * Returns the {@link Site} with the specified {@code siteId}.
     */
    default Optional<Site> get(String siteId)
    {
        return get(siteId, false, null);
    }

    /**
     * Returns the {@link Site} with the specified {@code siteId}, optionally including all its {@link Job jobs}.
     */
    default Optional<Site> get(
            String siteId,
            boolean includeJobs)
    {
        return get(siteId, includeJobs, null);
    }

    /**
     * Returns the {@link Site} with the specified {@code siteId}, optionally including all its {@link Job jobs} that match the {@literal
     * jobSearchTerm}.
     *
     * @since 1.12.0
     */
    Optional<Site> get(
            String siteId,
            boolean includeJobs,
            String jobSearchTerm);

    /**
     * Returns the {@link Site} with the specified {@code siteId}.
     */
    default Site getExisting(String siteId)
    {
        return getExisting(siteId, false, null);
    }

    /**
     * Returns the {@link Site} with the specified {@code siteId}, optionally including all its {@link Job jobs}.
     */
    default Site getExisting(
            String siteId,
            boolean includeJobs)
    {
        return getExisting(siteId, includeJobs, null);
    }

    /**
     * Returns the {@link Site} with the specified {@code siteId}, optionally including all its {@link Job jobs} that match the {@literal
     * jobSearchTerm}.
     *
     * @since 1.12.0
     */
    default Site getExisting(
            String siteId,
            boolean includeJobs,
            String jobSearchTerm)
    {
        return get(siteId, includeJobs, jobSearchTerm).orElseThrow(() -> new UnknownSiteException(siteId));
    }

    /**
     * Returns all the {@link Site sites}.
     */
    default List<Site> getAll()
    {
        return getAll(false);
    }

    /**
     * Returns all the {@link Site sites}, optionally including all there {@link Job jobs}.
     */
    List<Site> getAll(boolean includeJobs);

    /**
     * Adds a new {@link Site}.
     */
    Site add(Site site)
            throws ValidationException;

    /**
     * Updates the {@link Site} with the specified {@code siteId} using the specified {@link Site}.
     */
    Site update(
            String siteId,
            Site site)
            throws ValidationException;

    /**
     * Validate the {@link Site} throwing a {@link ValidationException} in case a validation error is found for one of the specified fields.
     *
     * @since 1.46
     */
    void validateSite(
            Site site,
            String[] fields)
            throws ValidationException;

    /**
     * Updates the {@link Site} with the specified {@code siteId} using the specified {@link Site}.
     */
    default Site update(Site site)
            throws ValidationException
    {
        return update(site.getId(), site);
    }

    /**
     * Save the specified {@link Site}, and return its new state.
     */
    Site save(Site site)
            throws ValidationException;

    /**
     * Enable/Disable a site for synchronization.
     *
     * @since 1.38
     */
    void enableSite(
            String siteId,
            boolean enabled);

    /**
     * Enable/Disable the default enabled state for new {@link Job jobs} of the specified {@link Site} with the specified {@code siteId}.
     */
    void automaticallyEnableNewJobs(
            String siteId,
            boolean enabled);

    /**
     * Delete the specified {@link Site}.
     */
    void delete(Site site);

    /**
     * Delete the specified {@link Site} with the specified {@code siteId}.
     */
    default void delete(String siteId)
    {
        delete(getExisting(siteId));
    }

    /**
     * Delete all the {@link Job jobs} of the specified {@link Site}.
     */
    void deleteDeletedJobs(Site site);

    /**
     * Delete all the {@link Job jobs} of the {@link Site} with the specified {@code siteId}.
     */
    default void deleteDeletedJobs(String siteId)
    {
        deleteDeletedJobs(getExisting(siteId));
    }

    /**
     * Returns the {@link SiteStatus} for the specified {@link Site}.
     */
    SiteStatus getSiteStatus(Site site);

    /**
     * Returns the {@link SiteStatus} for the {@link Site} with the specified {@code siteId}.
     */
    default SiteStatus getSiteStatus(String siteId)
    {
        return getSiteStatus(getExisting(siteId));
    }

    /**
     * Returns the {@link SiteTunnelDetails} for the specified {@link Site}.
     */
    @Nullable
    SiteTunnelDetails getTunnelDetails(Site site);

    /**
     * Returns the {@link SiteTunnelDetails} for the {@link Site} with the specified {@code siteId}.
     */
    @Nullable
    default SiteTunnelDetails getTunnelDetails(String siteId)
    {
        return getTunnelDetails(getExisting(siteId));
    }
}
