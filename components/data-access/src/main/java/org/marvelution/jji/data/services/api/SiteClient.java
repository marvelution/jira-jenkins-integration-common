/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services.api;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.marvelution.jji.model.*;

public interface SiteClient
        extends SiteDataClient
{
    String X_JIRA_INTEGRATION = "X-JiraIntegration";
    String X_JJI_VERSION = "X-JJI-Version";
    String X_JENKINS = "X-Jenkins";

    Status getRemoteStatus(Site site);

    boolean isJenkinsPluginInstalled(Site site);

    Optional<Version> getJenkinsPluginVersion(Site site);

    boolean isCrumbSecurityEnabled(Site site);

    default boolean registerWithSite(Site site)
    {
        return registerWithSite(site, false);
    }

    default boolean registerWithSite(
            Site site,
            boolean failureOnError)
    {
        return registerWithSite(site, site.getAuthentication(), failureOnError);
    }

    default boolean registerWithSite(
            Site site,
            SiteAuthentication authentication)
    {
        return registerWithSite(site, authentication, false);
    }

    boolean registerWithSite(
            Site site,
            SiteAuthentication authentication,
            boolean failureOnError);

    boolean unregisterWithSite(Site site);

    List<Job> getJobs(Site site);

    BuildJobResponse buildJob(BuildJobRequest request);

    enum BuildJobResponse
    {
        ACCEPTED,
        BUILD_SCHEDULED,
        JOB_NOT_BUILDABLE,
        FAILED,
        NOT_SUPPORTED
    }

    class BuildJobRequest
    {
        private Job job;
        private IssueReference issue;
        private String subject;
        private Map<String, String> parameters;

        public Job getJob()
        {
            return job;
        }

        public BuildJobRequest setJob(Job job)
        {
            this.job = job;
            return this;
        }

        public IssueReference getIssue()
        {
            return issue;
        }

        public BuildJobRequest setIssue(IssueReference issue)
        {
            this.issue = issue;
            return this;
        }

        public String getSubject()
        {
            return subject;
        }

        public BuildJobRequest setSubject(String subject)
        {
            this.subject = subject;
            return this;
        }

        public Map<String, String> getParameters()
        {
            if (parameters == null)
            {
                parameters = new HashMap<>();
            }
            return parameters;
        }

        public BuildJobRequest setParameters(Map<String, String> parameters)
        {
            this.parameters = parameters;
            return this;
        }

        public BuildJobRequest addParameter(
                String name,
                String value)
        {
            getParameters().put(name, value);
            return this;
        }

        public static BuildJobRequest create(
                Job job,
                String subject)
        {
            return new BuildJobRequest().setJob(job)
                    .setSubject(subject);
        }

        public static BuildJobRequest create(
                Job job,
                IssueReference issue,
                String subject)
        {
            return create(job, subject).setIssue(issue);
        }
    }
}
