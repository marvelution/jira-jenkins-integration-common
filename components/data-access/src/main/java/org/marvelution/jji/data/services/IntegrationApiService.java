/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.util.*;
import javax.annotation.Nullable;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import jakarta.json.Json;
import jakarta.json.JsonObject;
import jakarta.json.JsonReader;
import jakarta.json.JsonStructure;

import org.marvelution.jji.Headers;
import org.marvelution.jji.data.services.api.*;
import org.marvelution.jji.data.synchronization.api.SynchronizationService;
import org.marvelution.jji.data.synchronization.api.exceptions.SynchronizationTriggerException;
import org.marvelution.jji.data.synchronization.api.trigger.*;
import org.marvelution.jji.events.JobNotificationType;
import org.marvelution.jji.model.*;
import org.marvelution.jji.model.parsers.ParserProvider;
import org.marvelution.jji.model.response.SiteTunnelDetails;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.util.stream.Collectors.toMap;

@Named
@javax.inject.Named
public class IntegrationApiService
{

    public static final String SYNC_RESULT_HEADER = Headers.SYNC_RESULT_HEADER;
    private static final Logger LOGGER = LoggerFactory.getLogger(IntegrationApiService.class);
    private final ConfigurationService configurationService;
    private final SiteService siteService;
    private final JobService jobService;
    private final BuildService buildService;
    private final IssueLinkService issueLinkService;
    private final SynchronizationService synchronizationService;

    @Inject
    @javax.inject.Inject
    public IntegrationApiService(
            ConfigurationService configurationService,
            SiteService siteService,
            JobService jobService,
            BuildService buildService,
            IssueLinkService issueLinkService,
            SynchronizationService synchronizationService)
    {
        this.configurationService = configurationService;
        this.siteService = siteService;
        this.jobService = jobService;
        this.buildService = buildService;
        this.issueLinkService = issueLinkService;
        this.synchronizationService = synchronizationService;
    }

    public Optional<SiteTunnelDetails> getTunnelDetails(Site site)
    {
        if (site.isTunneledSite())
        {
            return Optional.ofNullable(siteService.getTunnelDetails(site));
        }
        else
        {
            return Optional.empty();
        }
    }

    public JsonObject getRegistrationDetails(Site site)
    {
        return configurationService.getSiteRegistrationDetails(site);
    }

    public void completeRegistration(Site site)
    {
        LOGGER.debug("Marking registration complete for site {} [{}]", site.getName(), site.getId());
        siteService.save(site.setJenkinsPluginInstalled(true)
                .setRegistrationComplete(true));
    }

    public void incompleteRegistration(Site site)
    {
        LOGGER.debug("Marking registration incomplete for site {} [{}]", site.getName(), site.getId());
        siteService.save(site.setJenkinsPluginInstalled(true)
                .setRegistrationComplete(false));
    }

    public Optional<String> synchronizeJob(
            Site site,
            String jobHash,
            @Nullable
            JobNotificationType notificationType)
    {
        return synchronizeJob(site, jobHash, notificationType, null, Optional.empty());
    }

    public Optional<String> synchronizeJob(
            Site site,
            String jobHash,
            @Nullable
            JobNotificationType notificationType,
            @Nullable
            InputStream inputStream,
            Optional<Charset> charset)
    {
        TriggerReason triggerReason;
        switch (Optional.ofNullable(notificationType)
                .orElse(JobNotificationType.JOB_SYNC))
        {
            case JOB_MOVED:
                triggerReason = new JobMovedTrigger(jobHash);
                break;
            case JOB_CREATED:
                triggerReason = new JobCreatedTrigger(jobHash);
                break;
            case JOB_MODIFIED:
                triggerReason = new JobModifiedTrigger(jobHash);
                break;
            case JOB_SYNC:
            default:
                triggerReason = new JobSyncTrigger(jobHash);
                break;
        }

        Optional<Job> job = jobService.findByHash(site, jobHash);
        if (job.isPresent())
        {
            if (inputStream != null)
            {
                return synchronize(job.get(), triggerReason, inputStream, charset.orElseGet(Charset::defaultCharset));
            }
            else
            {
                return synchronize(job.get(), triggerReason);
            }
        }
        else if (inputStream != null)
        {
            try (Reader reader = new InputStreamReader(inputStream, charset.orElseGet(Charset::defaultCharset));
                 JsonReader jsonReader = Json.createReader(reader))
            {
                Job newJob = new Job().setSite(site);
                ParserProvider.jobParser()
                        .parse(jsonReader.read(), newJob);
                jobService.findByHash(site, newJob.getHash())
                        .map(Job::getId)
                        .ifPresent(newJob::setId);
                jobService.save(newJob);
                return Optional.empty();
            }
            catch (Exception e)
            {
                LOGGER.error("Failed to parse request body: ", e);
                throw new SynchronizationTriggerException("Failed to parse request body: " + e.getMessage(), e);
            }
        }
        else if (site.isAutoLinkNewJobs())
        {
            return synchronize(site, triggerReason);
        }
        else
        {
            LOGGER.debug("Ignoring job synchronization request, job is unknown and auto enabled new jobs is disabled on {}",
                    site.getName());
            return Optional.empty();
        }
    }

    public Optional<String> synchronizeBuild(
            Site site,
            String jobHash,
            int buildNumber,
            @Nullable
            List<String> parentHashes)
    {
        verifyBuildNumber(buildNumber);

        BuildCompletionTrigger triggerReason = new BuildCompletionTrigger(jobHash, buildNumber);

        Optional<Build> build = jobService.findByHash(site, jobHash)
                .map(job -> getBuild(job, buildNumber));
        if (build.isPresent())
        {
            return synchronize(build.get(), triggerReason);
        }
        else
        {
            if (parentHashes != null)
            {
                for (String parentHash : parentHashes)
                {
                    Optional<Job> parent = jobService.findByHash(site, parentHash);
                    if (parent.isPresent())
                    {
                        LOGGER.debug("Found closest parent {} [{}] to synchronize",
                                parent.get()
                                        .getName(),
                                parent.get()
                                        .getId());
                        return synchronize(parent.get(), triggerReason);
                    }
                }
            }
            if (site.isAutoLinkNewJobs())
            {
                return synchronize(site, triggerReason);
            }
            else
            {
                return Optional.empty();
            }
        }
    }

    public Optional<String> synchronizeBuild(
            Site site,
            String jobHash,
            int buildNumber,
            InputStream inputStream,
            Optional<Charset> charset)
    {
        verifyBuildNumber(buildNumber);

        BuildCompletionTrigger triggerReason = new BuildCompletionTrigger(jobHash, buildNumber);

        Optional<Build> build = jobService.findByHash(site, jobHash)
                .map(job -> getBuild(job, buildNumber));
        if (build.isPresent())
        {
            LOGGER.debug("Found build {} in build data",
                    build.map(Build::getDisplayName)
                            .orElse(""));
            return synchronize(build.get(), triggerReason, inputStream, charset.orElseGet(Charset::defaultCharset));
        }
        else
        {
            try (Reader reader = new InputStreamReader(inputStream, charset.orElseGet(Charset::defaultCharset));
                 JsonReader jsonReader = Json.createReader(reader))
            {
                JsonStructure json = jsonReader.read();
                Build newBuild = new Build();
                ParserProvider.buildParentParser()
                        .parse(json, newBuild);
                if (newBuild.getJob() != null)
                {
                    Job job = jobService.findByHash(site,
                                    newBuild.getJob()
                                            .getHash())
                            .orElseGet(() -> jobService.save(newBuild.getJob()
                                    .setSite(site)
                                    .setLinked(site.isAutoLinkNewJobs())));
                    LOGGER.debug("Found job {} [{}] in build data", job.getName(), job.getId());
                    return synchronize(getBuild(job, buildNumber), triggerReason, json);
                }
                else
                {
                    LOGGER.debug("No job found in build synchronization request, ignoring request");
                    return Optional.empty();
                }
            }
            catch (Exception e)
            {
                LOGGER.error("Failed to parse request body: ", e);
                throw new SynchronizationTriggerException("Failed to parse request body: " + e.getMessage(), e);
            }
        }
    }

    private Optional<String> synchronize(
            Syncable<?> syncable,
            TriggerReason triggerReason)
    {
        completeRegistrationIfNeeded(syncable);
        LOGGER.debug("Synchronizing push data for {} trigger reason: {}", syncable, triggerReason.describe());
        return synchronizationService.synchronize(syncable, triggerReason);
    }

    private Optional<String> synchronize(
            Syncable<?> syncable,
            TriggerReason triggerReason,
            InputStream inputStream,
            Charset charset)
    {
        completeRegistrationIfNeeded(syncable);
        LOGGER.debug("Synchronizing push data for {} trigger reason: {}", syncable, triggerReason.describe());
        return synchronizationService.synchronize(syncable, triggerReason, inputStream, charset);
    }

    private Optional<String> synchronize(
            Syncable<?> syncable,
            BuildCompletionTrigger triggerReason,
            JsonStructure json)
    {
        completeRegistrationIfNeeded(syncable);
        LOGGER.debug("Synchronizing pull data for {} trigger reason: {}", syncable, triggerReason.describe());
        return synchronizationService.synchronize(syncable, triggerReason, json);
    }

    private void completeRegistrationIfNeeded(Syncable<?> syncable)
    {
        if (!syncable.getSite()
                .isRegistrationComplete())
        {
            LOGGER.debug("Marking site registration complete for {} [{}]",
                    syncable.getSite()
                            .getName(),
                    syncable.getSite()
                            .getId());
            siteService.save(syncable.getSite()
                    .setRegistrationComplete(true));
        }
    }

    private Build getBuild(
            Job job,
            int buildNumber)
    {
        return buildService.get(job, buildNumber)
                .orElseGet(() -> new Build().setJob(job)
                        .setNumber(buildNumber));
    }

    public Map<String, String> getBuildLinks(
            Site site,
            String jobHash,
            int buildNumber)
    {
        verifyBuildNumber(buildNumber);
        return getBuild(site, jobHash, buildNumber).map(issueLinkService::getIssueLinks)
                .map(Collection::stream)
                .map(stream -> stream.collect(toMap(IssueReference::getIssueKey,
                        reference -> reference.getIssueUrl()
                                .toASCIIString(),
                        (a, b) -> a)))
                .orElseGet(HashMap::new);
    }

    public void markJobAsDeleted(
            Site site,
            String jobHash)
    {
        LOGGER.debug("Marking job with hash {} on {} as deleted", jobHash, site.getName());
        jobService.findByHash(site, jobHash)
                .map(job -> job.setDeleted(true))
                .ifPresent(jobService::save);
    }

    public void markBuildAsDeleted(
            Site site,
            String jobHash,
            int buildNumber)
    {
        verifyBuildNumber(buildNumber);
        LOGGER.debug("Marking build with {} #{} on {} as deleted", jobHash, buildNumber, site.getName());
        getBuild(site, jobHash, buildNumber).map(build -> build.setDeleted(true))
                .ifPresent(buildService::save);
    }

    private Optional<Build> getBuild(
            Site site,
            String jobHash,
            int buildNumber)
    {
        return jobService.findByHash(site, jobHash)
                .flatMap(job -> buildService.get(job, buildNumber));
    }

    private void verifyBuildNumber(int buildNumber)
    {
        if (buildNumber <= 0)
        {
            throw new IllegalArgumentException("buildNumber must be a positive number");
        }
    }
}
