/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services.api.model;

import javax.annotation.*;
import java.util.*;

import com.fasterxml.jackson.annotation.*;

public class AvatarUrls
{

    @JsonProperty
    private Map<String, String> urls = new HashMap<>();

    @Nullable
    public String avatar(String size)
    {
        return urls.get(size);
    }

    @JsonAnyGetter
    public Map<String, String> get()
    {
        return urls;
    }

    @JsonAnySetter
    public void set(
            String key,
            String value)
    {
        urls.put(key, value);
    }
}
