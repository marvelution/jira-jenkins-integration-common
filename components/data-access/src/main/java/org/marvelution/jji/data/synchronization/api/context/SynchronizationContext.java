/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.synchronization.api.context;

import org.marvelution.jji.data.synchronization.api.result.*;
import org.marvelution.jji.model.*;

public interface SynchronizationContext<S extends Syncable<S>>
		extends SynchronizationResult
{

	S getSyncable();

	default OperationId getOperationId()
	{
		return getSyncable().getOperationId();
	}

	<S1 extends Syncable<S1>> void synchronize(S1 syncable);

	Status getSyncableStatus();

	boolean populateJobDetails(Job job);

	boolean populateBuildDetails(Build build);
}
