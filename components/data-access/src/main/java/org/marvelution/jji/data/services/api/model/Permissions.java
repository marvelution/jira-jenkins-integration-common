/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services.api.model;

import javax.annotation.*;
import java.io.*;
import java.util.*;

import com.fasterxml.jackson.annotation.*;

public class Permissions
        implements Serializable
{

    private static final long serialVersionUID = -1988412539139773112L;
    @JsonProperty
    private Map<String, Permission> permissions = new HashMap<>();

    @JsonAnyGetter
    public Map<String, Permission> get()
    {
        return permissions;
    }

    @JsonAnySetter
    public void set(
            String key,
            Permission permission)
    {
        permissions.put(key, permission);
    }

    @JsonIgnore
    public boolean hasPermissions()
    {
        return !permissions.isEmpty();
    }

    @Nullable
    @JsonIgnore
    public Permission getPermission(String key)
    {
        return permissions.get(key);
    }
}
