/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services;

import java.net.URI;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;
import javax.annotation.Nonnull;
import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.marvelution.jji.api.text.TextResolver;
import org.marvelution.jji.data.access.api.SiteDAO;
import org.marvelution.jji.data.services.api.SiteClient;
import org.marvelution.jji.data.services.api.SiteValidator;
import org.marvelution.jji.model.Site;
import org.marvelution.jji.validation.api.ErrorMessages;

import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

@Named
@javax.inject.Named
public class DefaultSiteValidator
        implements SiteValidator
{

    public static final List<String> SUPPORTED_SCHEMES = Arrays.asList("http", "https");
    protected final TextResolver textResolver;
    protected final SiteDAO siteDAO;
    private final SiteClient siteClient;

    @Inject
    @javax.inject.Inject
    public DefaultSiteValidator(
            TextResolver textResolver,
            SiteDAO siteDAO,
            SiteClient siteClient)
    {
        this.textResolver = textResolver;
        this.siteDAO = siteDAO;
        this.siteClient = siteClient;
    }

    @Override
    public ErrorMessages validate(Site site)
    {
        ErrorMessages errorMessages = new ErrorMessages();
        if (isBlank(site.getName()))
        {
            errorMessages.addError("name", textResolver.getText("name.required"));
        }
        else if (!site.getName()
                .matches("[\\w\\s\\-_]*"))
        {
            errorMessages.addError("name", textResolver.getText("name.invalid.tokens"));
        }
        else if (siteDAO.findByName(site.getName())
                .filter(existing -> !Objects.equals(existing, site))
                .isPresent())
        {
            errorMessages.addError("name", textResolver.getText("name.duplicate", site.getName()));
        }
        if (site.getType() == null)
        {
            errorMessages.addError("type", textResolver.getText("site.type.required"));
        }
        validateUrl(UrlValidationMode.SYNC, site, errorMessages);
        validateUrl(UrlValidationMode.DISPLAY, site, errorMessages);

        if (site.isAccessibleSite())
        {
            if (isNotBlank(site.getUser()) && isBlank(site.getToken()))
            {
                errorMessages.addError("token", textResolver.getText("site.token.required"));
            }
            else if (isNotBlank(site.getToken()) && isBlank(site.getUser()))
            {
                errorMessages.addError("user", textResolver.getText("site.user.required"));
            }
        }
        return errorMessages;
    }

    protected void validateUrl(
            UrlValidationMode mode,
            Site site,
            ErrorMessages errorMessages)
    {
        URI url = mode.siteUrl(site);

        if (url != null)
        {
            String scheme = url.getScheme();
            String host = url.getHost();
            if (!SUPPORTED_SCHEMES.contains(scheme) || isBlank(host) || !isHostNameSupported(host) || isNotBlank(url.getRawQuery()) ||
                isNotBlank(url.getRawFragment()))
            {
                errorMessages.addError(mode.fieldName(), textResolver.getText("site.url.invalid"));
            }
            else if (mode.unique() && mode.findDuplicate(siteDAO, site)
                    .isPresent())
            {
                errorMessages.addError(mode.fieldName(), textResolver.getText("site.url.duplicate", url.toASCIIString()));
            }
        }
        else if (mode.required())
        {
            errorMessages.addError(mode.fieldName(), textResolver.getText("site.url.required"));
        }
    }

    protected boolean isHostNameSupported(
            @Nonnull
            String host)
    {
        return true;
    }

    /**
     * @since 1.3.0
     */
    protected enum UrlValidationMode
    {
        SYNC("rpcUrl", true, true, Site::getRpcUrl, SiteDAO::findByRpcUrl),
        DISPLAY("displayUrl", false, false, Site::getDisplayUrlOrNull, SiteDAO::findByDisplayUrl);

        private final String fieldName;
        private final boolean required;
        private final boolean unique;
        private final Function<Site, URI> toUrl;
        private final BiFunction<SiteDAO, URI, Optional<Site>> findByUrl;

        UrlValidationMode(
                String fieldName,
                boolean required,
                boolean unique,
                Function<Site, URI> toUrl,
                BiFunction<SiteDAO, URI, Optional<Site>> findByUrl)
        {
            this.fieldName = fieldName;
            this.required = required;
            this.unique = unique;
            this.toUrl = toUrl;
            this.findByUrl = findByUrl;
        }

        public String fieldName()
        {
            return fieldName;
        }

        public boolean required()
        {
            return required;
        }

        public boolean unique()
        {
            return unique;
        }

        public URI siteUrl(Site site)
        {
            return Optional.of(site)
                    .map(toUrl)
                    .orElse(null);
        }

        public Optional<Site> findDuplicate(
                SiteDAO siteDAO,
                Site site)
        {
            return findByUrl.apply(siteDAO, siteUrl(site))
                    .filter(existing -> !Objects.equals(existing, site));
        }
    }
}
