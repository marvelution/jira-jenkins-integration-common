/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access.api;

import javax.annotation.*;
import java.util.*;

import org.marvelution.jji.model.*;

public interface IssueReferenceProvider
{

    void setIssueUrl(IssueReference reference);

    default Optional<IssueReference> getIssueReference(String key)
    {
        return getIssueReference(key, new FieldContext());
    }

    Optional<IssueReference> getIssueReference(
            String key,
            FieldContext fieldContext);

    default Set<IssueReference> getIssueReferences(Set<String> keys)
    {
        return getIssueReferences(keys, new FieldContext());
    }

    Set<IssueReference> getIssueReferences(
            Set<String> keys,
            FieldContext fieldContext);

    class FieldContext
    {

        public boolean hasAdditionalFields()
        {
            return !additionalFields().isEmpty();
        }

        public Set<String> additionalFields()
        {
            return Collections.emptySet();
        }

        public String fieldName(
                String fieldKey,
                String fieldName)
        {
            return fieldName;
        }

        @Nullable
        public String fieldValue(
                String fieldKey,
                String fieldName,
                @Nullable
                Object value,
                @Nullable
                Object renderedValue)
        {
            return null;
        }
    }
}
