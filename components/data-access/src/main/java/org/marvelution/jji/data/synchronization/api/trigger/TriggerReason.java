/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.synchronization.api.trigger;

import org.marvelution.jji.data.synchronization.api.*;

import static org.apache.commons.lang3.StringUtils.*;

/**
 * API to specify the reason for a {@link SynchronizationOperation} to be scheduled.
 *
 * @author Mark Rekveld
 * @since 1.8.0
 */
public abstract class TriggerReason
{

	private String data;

	/**
	 * @since 1.42
	 */
	public String getData()
	{
		return data;
	}

	/**
	 * @since 1.42
	 */
	public void setData(String data)
	{
		this.data = data;
	}

	/**
	 * @since 1.42
	 */
	public TriggerReason withData(String data)
	{
		this.data = data;
		return this;
	}

	/**
	 * @since 1.9.0
	 */
	public boolean hasData()
	{
		return isNotBlank(data);
	}

	/**
	 * Describes this trigger reason.
	 */
	public abstract String describe();
}
