/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access.api;

import java.util.function.BiConsumer;

import org.marvelution.jji.model.LinkStatistics;

/**
 * @author Mark Rekveld
 * @since 1.19
 */
public interface LinkStatisticsDAO
{

    LinkStatistics getLinkStatistics(String issueKey);

    LinkStatisticianState getLinkStatisticianState(String issueKey);

    int getLinkStatisticianStates(
            int page,
            int size,
            BiConsumer<String, String> consumer);

    default void storeLinkStatistics(
            String issueKey,
            LinkStatistics statistics)
    {
        storeLinkStatistics(issueKey, false, statistics);
    }

    void storeLinkStatistics(
            String issueKey,
            boolean queued,
            LinkStatistics statistics);

    void importLinkStatistics(
            String issueKey,
            String statisticsJson);

    default void finishLinkStatisticsImports() {}

    void clearLinkStatistics(String... issueKey);

    void clearLinkStatisticsForProject(String projectKey);

    interface LinkStatisticianState
    {

        String issueKey();

        boolean queued();

        LinkStatistics current();
    }
}
