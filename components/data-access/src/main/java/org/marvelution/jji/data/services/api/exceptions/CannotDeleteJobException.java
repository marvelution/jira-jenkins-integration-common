/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services.api.exceptions;

import org.marvelution.jji.model.Job;

/**
 * Exception thrown in case a {@link Job} cannot be deleted.
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class CannotDeleteJobException extends RuntimeException {

	public static final String MESSAGE_PREFIX = "Cannot delete job with id: ";
	private static final long serialVersionUID = 1912208274645753294L;
	private final Job job;

	public CannotDeleteJobException(String jobId) {
		super(MESSAGE_PREFIX + jobId);
		job = null;
	}
	
	public CannotDeleteJobException(Job job) {
		super(MESSAGE_PREFIX + job.getId());
		this.job = job;
	}

	public Job getJob() {
		return job;
	}
}
