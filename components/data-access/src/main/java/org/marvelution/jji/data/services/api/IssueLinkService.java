/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services.api;

import java.util.*;

import org.marvelution.jji.model.*;

import static java.util.stream.Collectors.*;
import static java.util.stream.Stream.*;

/**
 * Service for obtaining build-to-issue link related data.
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public interface IssueLinkService
{

    /**
     * Returns the number of build links for the specified {@code issueKey}.
     *
     * @since 1.14.0
     */
    int getBuildCountForIssue(String issueKey);

    /**
     * Returns the number of build links for the specified {@code projectKey}.
     *
     * @since 1.14.0
     */
    int getBuildCountForProject(String projectKey);

    /**
     * Returns the {@link Build}s related to the specified {@code issueKey}.
     *
     * @since 1.19
     */
    Set<Build> getBuildsForIssue(
            String issueKey,
            int limit,
            boolean includeIssueReferences);

    /**
     * Returns the {@link Build}s related to the specified {@code issueKey}.
     *
     * @since 1.19
     */
    Set<Build> getBuildsForLinkRequest(
            LinkRequest request,
            int limit,
            boolean includeIssueReferences);

    /**
     * Returns the {@link Job}s related to the specified {@code issueKey}.
     *
     * @since 1.19
     */
    Set<Job> getJobsForIssue(
            boolean includeIssueReferences,
            String... issueKey);

    /**
     * Returns all related issue keys for the specified {@link Build}.
     */
    Set<String> getRelatedIssueKeys(Build build);

    /**
     * Returns all related issue keys for the specified {@link Job}.
     */
    Set<String> getRelatedIssueKeys(Job job);

    /**
     * Returns a {@link Set} of all {@link IssueReference issues} linked to the specified {@link Build}.
     */
    Set<IssueReference> getIssueLinks(Build build);

    /**
     * Returns a {@link Set} of all {@link IssueReference issues} linked to the specified {@link Job}.
     *
     * @since 1.19
     */
    Set<IssueReference> getIssueLinks(Job job);

    /**
     * Returns the {@link LinkStatistics} for the specified {@code issueKey}.
     *
     * @since 1.19
     */
    LinkStatistics getLinkStatistics(String issueKey);

    /**
     * Link the specified {@link Build} and {@link String issueKey}.
     */
    Optional<IssueReference> link(
            Build build,
            String issueKey);

    /**
     * Relink between the specified {@link Build build} and {@link String issueKeys}, this will clear all existing links.
     */
    Set<IssueReference> relink(
            Build build,
            Set<String> issueKeys);

    /**
     * Unlink all links related to the specified {@code issueKey}.
     */
    void unlinkForIssue(String issueKey);

    /**
     * Unlink all links related to the specified {@code projectKey}.
     */
    void unlinkForProject(String projectKey);

    /**
     * Rebuild all the link statistics for all issues that have links.
     */
    void rebuildLinkStatistics();

    class LinkRequest
    {

        private Set<String> inProjectKeys;
        private Set<String> notInProjectKeys;
        private Set<String> inIssueKeys;
        private Set<String> notInIssueKeys;

        public Set<String> getInProjectKeys()
        {
            if (inProjectKeys == null)
            {
                inProjectKeys = new HashSet<>();
            }
            return inProjectKeys;
        }

        public LinkRequest setInProjectKeys(Set<String> inProjectKeys)
        {
            this.inProjectKeys = inProjectKeys;
            return this;
        }

        public LinkRequest setInProjectKeys(String... inProjectKeys)
        {
            return setInProjectKeys(of(inProjectKeys).collect(toSet()));
        }

        public Set<String> getNotInProjectKeys()
        {
            if (notInProjectKeys == null)
            {
                notInProjectKeys = new HashSet<>();
            }
            return notInProjectKeys;
        }

        public LinkRequest setNotInProjectKeys(Set<String> notInProjectKeys)
        {
            this.notInProjectKeys = notInProjectKeys;
            return this;
        }

        public LinkRequest setNotInProjectKeys(String... notInProjectKeys)
        {
            return setNotInProjectKeys(of(notInProjectKeys).collect(toSet()));
        }

        public Set<String> getInIssueKeys()
        {
            if (inIssueKeys == null)
            {
                inIssueKeys = new HashSet<>();
            }
            return inIssueKeys;
        }

        public LinkRequest setInIssueKeys(Set<String> inIssueKeys)
        {
            this.inIssueKeys = inIssueKeys;
            return this;
        }

        public LinkRequest setInIssueKeys(String... inIssueKeys)
        {
            return setInIssueKeys(of(inIssueKeys).collect(toSet()));
        }

        public Set<String> getNotInIssueKeys()
        {
            if (notInIssueKeys == null)
            {
                notInIssueKeys = new HashSet<>();
            }
            return notInIssueKeys;
        }

        public LinkRequest setNotInIssueKeys(Set<String> notInIssueKeys)
        {
            this.notInIssueKeys = notInIssueKeys;
            return this;
        }

        public LinkRequest setNotInIssueKeys(String... notInIssueKeys)
        {
            return setNotInIssueKeys(of(notInIssueKeys).collect(toSet()));
        }

        @Override
        public String toString()
        {
            return new StringJoiner(", ", LinkRequest.class.getSimpleName() + "[", "]").add("inProjectKeys=" + inProjectKeys)
                    .add("notInProjectKeys=" + notInProjectKeys)
                    .add("inIssueKeys=" + inIssueKeys)
                    .add("notInIssueKeys=" + notInIssueKeys)
                    .toString();
        }
    }
}
