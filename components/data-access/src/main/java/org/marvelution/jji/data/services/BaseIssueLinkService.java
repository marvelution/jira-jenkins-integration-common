/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services;

import java.util.Optional;
import java.util.Set;

import org.marvelution.jji.api.events.EventPublisher;
import org.marvelution.jji.data.access.api.IssueReferenceProvider;
import org.marvelution.jji.data.access.api.IssueToBuildDAO;
import org.marvelution.jji.data.access.api.LinkStatisticsDAO;
import org.marvelution.jji.data.services.api.ConfigurationService;
import org.marvelution.jji.data.services.api.IssueLinkService;
import org.marvelution.jji.events.ConfigurationSettingUpdatedEvent;
import org.marvelution.jji.events.IssueLinksDeletedEvent;
import org.marvelution.jji.events.IssueLinksUpdatedEvent;
import org.marvelution.jji.model.Build;
import org.marvelution.jji.model.IssueReference;
import org.marvelution.jji.model.Job;
import org.marvelution.jji.model.LinkStatistics;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toSet;

public abstract class BaseIssueLinkService
        implements IssueLinkService
{

    protected final Logger logger = LoggerFactory.getLogger(getClass());
    protected final ConfigurationService configurationService;
    private final IssueToBuildDAO issueToBuildDAO;
    private final IssueReferenceProvider issueReferenceProvider;
    private final EventPublisher eventPublisher;
    private final LinkStatisticsDAO linkStatisticsDAO;

    protected BaseIssueLinkService(
            IssueToBuildDAO issueToBuildDAO,
            LinkStatisticsDAO linkStatisticsDAO,
            IssueReferenceProvider issueReferenceProvider,
            ConfigurationService configurationService,
            EventPublisher eventPublisher)
    {
        this.issueToBuildDAO = requireNonNull(issueToBuildDAO);
        this.linkStatisticsDAO = requireNonNull(linkStatisticsDAO);
        this.issueReferenceProvider = requireNonNull(issueReferenceProvider);
        this.configurationService = configurationService;
        this.eventPublisher = requireNonNull(eventPublisher);
    }

    @Override
    public int getBuildCountForIssue(String issueKey)
    {
        return issueToBuildDAO.getLinkCountForIssue(issueKey);
    }

    @Override
    public int getBuildCountForProject(String projectKey)
    {
        return issueToBuildDAO.getLinkCountForProject(projectKey);
    }

    @Override
    public Set<Build> getBuildsForIssue(
            String issueKey,
            int limit,
            boolean includeIssueReferences)
    {
        return issueToBuildDAO.getBuildsForIssue(issueKey, limit, includeIssueReferences);
    }

    @Override
    public Set<Build> getBuildsForLinkRequest(
            LinkRequest request,
            int limit,
            boolean includeIssueReferences)
    {
        return issueToBuildDAO.getBuildsForLinkRequest(request, limit, includeIssueReferences);
    }

    @Override
    public Set<Job> getJobsForIssue(
            boolean includeIssueReferences,
            String... issueKey)
    {
        return issueToBuildDAO.getJobsForIssue(includeIssueReferences, issueKey);
    }

    @Override
    public Set<String> getRelatedIssueKeys(Build build)
    {
        return issueToBuildDAO.getIssueKeys(build);
    }

    @Override
    public Set<String> getRelatedIssueKeys(Job job)
    {
        return issueToBuildDAO.getIssueKeys(job);
    }

    @Override
    public Set<IssueReference> getIssueLinks(Build build)
    {
        return setIssueUrls(issueToBuildDAO.getIssueLinks(build));
    }

    @Override
    public Set<IssueReference> getIssueLinks(Job job)
    {
        return setIssueUrls(issueToBuildDAO.getIssueLinks(job));
    }

    private Set<IssueReference> setIssueUrls(Set<IssueReference> issueReferences)
    {
        return issueReferences.stream()
                .peek(issueReferenceProvider::setIssueUrl)
                .collect(toSet());
    }

    @Override
    public LinkStatistics getLinkStatistics(String issueKey)
    {
        return linkStatisticsDAO.getLinkStatistics(issueKey);
    }

    @Override
    public Optional<IssueReference> link(
            Build build,
            String issueKey)
    {
        Optional<IssueReference> reference = issueReferenceProvider.getIssueReference(issueKey)
                .filter(ref -> configurationService.isProjectEnabled(ref.getProjectKey()));
        reference.ifPresent(ref -> {
            issueToBuildDAO.link(build, ref);
            updatedIssueLinks(build, ref.getIssueKey());
        });
        return reference;
    }

    @Override
    public Set<IssueReference> relink(
            Build build,
            Set<String> issueKeys)
    {
        Set<IssueReference> oldReferences = getIssueLinks(build);
        Set<IssueReference> references = issueReferenceProvider.getIssueReferences(issueKeys)
                .stream()
                .filter(ref -> configurationService.isProjectEnabled(ref.getProjectKey()))
                .collect(toSet());
        issueToBuildDAO.relink(build, references);
        updatedIssueLinks(build,
                references.stream()
                        .map(IssueReference::getIssueKey)
                        .toArray(String[]::new));
        oldReferences.removeAll(references);
        deletedIssueLinks(oldReferences.stream()
                .map(IssueReference::getIssueKey)
                .toArray(String[]::new));
        return references;
    }

    @Override
    public void unlinkForIssue(String issueKey)
    {
        deletedIssueLinks(issueKey);
    }

    @Override
    public void unlinkForProject(String projectKey)
    {
        eventPublisher.publish(new IssueLinksDeletedEvent(projectKey));
        issueToBuildDAO.unlinkForProject(projectKey);
        linkStatisticsDAO.clearLinkStatisticsForProject(projectKey);
    }

    @Override
    public void rebuildLinkStatistics()
    {
        int limit = 100;
        int page = 0;
        Set<String> issueKeys;
        do
        {
            issueKeys = issueToBuildDAO.getAllIssueKeysWithLinks(limit, page++);

            if (!issueKeys.isEmpty())
            {
                scheduleBulkStatistician(issueKeys);
            }
        }
        while (issueKeys.size() == limit);
    }

    private void updatedIssueLinks(
            Build build,
            String... issueKeys)
    {
        eventPublisher.publish(new IssueLinksUpdatedEvent(build.getId(), issueKeys));
        if (issueKeys != null)
        {
            for (String issueKey : issueKeys)
            {
                int links = getBuildCountForIssue(issueKey);
                LinkStatisticsDAO.LinkStatisticianState state = linkStatisticsDAO.getLinkStatisticianState(issueKey);
                if ((state.current() == null || state.current()
                                                        .getBuildTotal() != links) && !state.queued())
                {
                    linkStatisticsDAO.storeLinkStatistics(issueKey, true, state.current());
                    scheduleStatistician(issueKey);
                }
            }
        }
    }

    protected void onConfigurationSettingChangedEvent(ConfigurationSettingUpdatedEvent event)
    {
        if (ConfigurationService.ENABLED_PROJECT_KEYS.equals(event.getSetting()
                .getKey()))
        {
            Set<String> enabledProjects = configurationService.getEnabledProjectKeys();
            if (enabledProjects != null)
            {
                issueToBuildDAO.getProjectKeysWithLinks()
                        .stream()
                        .filter(projectKey -> !enabledProjects.contains(projectKey))
                        .forEach(this::unlinkForProject);
            }
        }
    }

    protected abstract void scheduleStatistician(String issueKey);

    protected abstract void scheduleBulkStatistician(Set<String> issueKeys);

    private void deletedIssueLinks(String... issueKeys)
    {
        if (issueKeys != null)
        {
            eventPublisher.publish(new IssueLinksDeletedEvent(null, issueKeys));
            issueToBuildDAO.unlinkForIssue(issueKeys);
            linkStatisticsDAO.clearLinkStatistics(issueKeys);
        }
    }
}
