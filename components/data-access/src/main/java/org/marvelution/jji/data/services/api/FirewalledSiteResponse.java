/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services.api;

import java.io.*;
import java.util.*;

import org.marvelution.jji.model.*;

import static java.util.Optional.empty;

/**
 * {@link SiteResponse} for firewalled {@link Site}s.
 *
 * @since 1.9.0
 */
public class FirewalledSiteResponse
		extends SiteResponse
{

	private final Site site;

	public FirewalledSiteResponse(Site site)
	{
		this.site = site;
	}

	@Override
	public int getStatusCode()
	{
		return 418;
	}

	@Override
	public String getStatusMessage()
	{
		return "Site " + site.getName() + " is Firewalled";
	}

	@Override
	protected Reader getResponseBodyReader()
	{
		return new StringReader("");
	}

	@Override
	public Optional<String> getHeader(String header)
	{
		return empty();
	}
}
