/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services.api.model;

import java.util.*;

import com.fasterxml.jackson.annotation.*;

public class IssueFields
{

	public static final String PROJECT = "project";
	public static final String ISSUETYPE = "issuetype";
	public static final String SUMMARY = "summary";
	public static final String DESCRIPTION = "description";
	public static final String ENVIRONMENT = "environment";
	public static final String COMMENT = "comment";
	public static final String VERSIONS = "versions";
	public static final String FIX_VERSIONS = "fixVersions";
	public static final String LABELS = "labels";
	public static final String COMPONENTS = "components";
	public static final String ASSIGNEE = "assignee";
	public static final String CREATOR = "creator";
	public static final String REPORTER = "reporter";
	@JsonProperty
	private Map<String, Object> fields = new HashMap<>();

	public Object field(String key)
	{
		return fields.get(key);
	}

	@JsonAnyGetter
	public Map<String, Object> get()
	{
		return fields;
	}

	@JsonAnySetter
	public void set(
			String key,
			Object value)
	{
		fields.put(key, value);
	}
}
