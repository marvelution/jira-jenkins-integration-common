/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services.api;

import java.util.*;

import org.marvelution.jji.model.*;

/**
 * {@link Build} services.
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public interface BuildService
{

	/**
	 * Returns the {@link Build} with the specified {@code buildId}
	 */
	Optional<Build> get(String buildId);

	/**
	 * Returns the {@link Build} with its {@link Job} and build number
	 */
	Optional<Build> get(
			Job job,
			int buildNumber);

	/**
	 * Returns the last {@link Build} of the specified {@link Job}
	 *
	 * @since 1.19
	 */
	default Optional<Build> getLast(Job job)
	{
		if (job.getLastBuild() > 0)
		{
			return get(job, job.getLastBuild());
		}
		else
		{
			return Optional.empty();
		}
	}

	/**
	 * Returns all the builds of the specified {@link Job job} within the range defined by {@code start} and {@code end}, both inclusive.
	 */
	Set<Build> getAllInRange(
			Job job,
			int start,
			int end);

	/**
	 * Returns all the {@link Build builds} of the specified {@link Job}.
	 */
	default Set<Build> getByJob(Job job)
	{
		return getByJob(job, -1);
	}

	/**
	 * Returns the last X {@link Build builds} of the specified {@link Job}.
	 */
	Set<Build> getByJob(
			Job job,
			int limit);

	/**
	 * Save the specified {@link Build}.
	 */
	Build save(Build build);

	/**
	 * Delete the {@link Build} specified.
	 */
	void delete(Build build);

	/**
	 * Delete all {@link Build builds} related to the specified {@link Job}.
	 */
	void deleteAllInJob(Job job);

	/**
	 * Mark the specified {@link Build} as deleted.
	 */
	void markAsDeleted(Build build);

	/**
	 * Mark all the {@link Build builds} of the specified {@link Job} as deleted.
	 */
	void markAllInJobAsDeleted(Job job);

	/**
	 * Mark all the {@link Build builds} up to the specified {@code buildNumber} of the specified {@link Job} as deleted.
	 */
	void markAllInJobAsDeleted(
			Job job,
			int buildNumber);
}
