/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services.api.model;

import java.io.Serial;
import java.io.Serializable;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Permission
        implements Serializable
{

    @Serial
    private static final long serialVersionUID = 7372626078250965443L;
    private String id;
    private String key;
    private Type type;
    private String name;
    private String description;
    private boolean havePermission;
    private boolean deprecatedKey;
    @JsonIgnore
    private PermittedProjects projects;

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getKey()
    {
        return key;
    }

    public void setKey(String key)
    {
        this.key = key;
    }

    public Type getType()
    {
        return type;
    }

    public void setType(Type type)
    {
        this.type = type;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public boolean isHavePermission()
    {
        return havePermission;
    }

    public void setHavePermission(boolean havePermission)
    {
        this.havePermission = havePermission;
    }

    public boolean isDeprecatedKey()
    {
        return deprecatedKey;
    }

    public void setDeprecatedKey(boolean deprecatedKey)
    {
        this.deprecatedKey = deprecatedKey;
    }

    @JsonIgnore
    public Set<PermittedProject> getPermittedProjects()
    {
        return projects.getProjects();
    }

    @JsonIgnore
    public void setPermittedProjects(PermittedProjects projects)
    {
        this.projects = projects;
    }

    public static long getSerialVersionUID()
    {
        return serialVersionUID;
    }

    public enum Type
    {
        GLOBAL,
        PROJECT
    }
}
