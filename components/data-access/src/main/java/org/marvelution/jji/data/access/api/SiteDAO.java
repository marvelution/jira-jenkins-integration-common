/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access.api;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.marvelution.jji.model.*;

/**
 * {@link Site} Data Access Service
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public interface SiteDAO
		extends BaseDAO<Site>
{

	/**
	 * Get all the {@link Site}s
	 */
	List<Site> getAll();

	/**
	 * Returns the {@link Site} with the specified {@link String name} if it exists.
	 */
	Optional<Site> findByName(String name);

	/**
	 * Returns the {@link Site} with the specified {@link URI rpcUrl} if it exists.
	 */
	Optional<Site> findByRpcUrl(URI rpcUri);

	/**
	 * Returns the {@link Site} with the specified {@link URI displayUrl} if it exists.
	 *
	 * @since 1.3.0
	 */
	Optional<Site> findByDisplayUrl(URI displayUri);

	/**
	 * Deleted all the {@link Site}s.
	 *
	 * @since 1.33
	 */
	void deleteAll();

	/**
	 * Deleted all the {@link Site}s of a specific scope.
	 *
	 * @since 1.46
	 */
	void deleteAll(String scope);

	/**
	 * Enable/Disable a site for synchronization.
	 *
	 * @since 1.38
	 */
	void enableSite(
			String siteId,
			boolean enabled);

	/**
	 * Enable/Disable the default enabled state for new {@link Job jobs} of the specified {@link Site} with the specified {@code siteId}.
	 *
	 * @since 1.38
	 */
	void enableNewJobs(
			String siteId,
			boolean enabled);
}
