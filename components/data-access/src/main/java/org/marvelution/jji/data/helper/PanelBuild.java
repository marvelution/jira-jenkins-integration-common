/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.helper;

import java.net.URI;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;
import javax.annotation.Nullable;

import org.marvelution.jji.model.*;

public class PanelBuild
{

    private final Build build;

    public PanelBuild(Build build)
    {
        this.build = build;
    }

    public String getId()
    {
        return build.getId();
    }

    public Job getJob()
    {
        return build.getJob();
    }

    public int getNumber()
    {
        return build.getNumber();
    }

    public String getDisplayName()
    {
        return build.getDisplayName();
    }

    public String getShortDisplayName()
    {
        return build.getShortDisplayName();
    }

    public URI getDisplayUrl()
    {
        return build.getDisplayUrl();
    }

    public boolean isDeleted()
    {
        return build.isDeleted();
    }

    @Nullable
    public String getCause()
    {
        return build.getCause();
    }

    public String getBuildColor()
    {
        return getBuildColor(build.getResult());
    }

    public String getBuildIcon()
    {
        return getBuildIcon(build.getResult());
    }

    public String getFormattedDuration()
    {
        return build.getFormattedDuration();
    }

    public Instant getDate()
    {
        return Instant.ofEpochMilli(build.getTimestamp());
    }

    @Nullable
    public TestResults getTestResults()
    {
        return build.getTestResults();
    }

    public Set<IssueReference> getIssueReferences()
    {
        return build.getIssueReferences()
                .stream()
                .sorted(Comparator.comparing(IssueReference::getIssueKey))
                .collect(Collectors.toCollection(LinkedHashSet::new));
    }

    public Result getResult()
    {
        return build.getResult();
    }

    public List<DeploymentEnvironment> getDeploymentEnvironments()
    {
        return build.getDeploymentEnvironments();
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(build);
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }
        PanelBuild that = (PanelBuild) o;
        return Objects.equals(build, that.build);
    }

    public static String getBuildColor(Result result)
    {
        return switch (result)
        {
            case SUCCESS -> "jenkins-green";
            case FAILURE -> "jenkins-red";
            case UNSTABLE -> "jenkins-yellow";
            default -> "jenkins-gray";
        };
    }

    public static String getBuildIcon(Result result)
    {
        return switch (result)
        {
            case SUCCESS -> "aui-iconfont-approve";
            case FAILURE, UNSTABLE -> "aui-iconfont-error";
            default -> "aui-iconfont-devtools-task-cancelled";
        };
    }
}
