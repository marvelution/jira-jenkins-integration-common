/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.synchronization.api.trigger;

import java.util.*;

import org.marvelution.jji.data.synchronization.api.*;
import org.marvelution.jji.data.synchronization.api.result.*;
import org.marvelution.jji.model.*;

import jakarta.json.bind.annotation.*;

/**
 * {@link TriggerReason} used when triggering a {@link SynchronizationOperation} from a {@link SynchronizationOperation}.
 *
 * @author Mark Rekveld
 * @since 1.8.0
 */
public class UpstreamTrigger
		extends TriggerReason
{

	private final String resultId;
	private final OperationId operationId;

	public UpstreamTrigger(ImmutableSynchronizationResult result)
	{
		this(result.getId(), result.getOperationId());
	}

	@JsonbCreator
	public UpstreamTrigger(
			@JsonbProperty String resultId,
			@JsonbProperty OperationId operationId)
	{
		this.resultId = resultId;
		this.operationId = operationId;
	}

	public String getResultId()
	{
		return resultId;
	}

	public OperationId getOperationId()
	{
		return operationId;
	}

	@Override
	public String describe()
	{
		return "upstream operation " + operationId;
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(resultId, operationId, getData());
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}
		UpstreamTrigger that = (UpstreamTrigger) o;
		return resultId.equals(that.resultId) && operationId.equals(that.operationId) && Objects.equals(getData(), that.getData());
	}
}
