/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.synchronization.api.context;

import java.io.*;
import java.util.*;

import org.marvelution.jji.data.services.api.*;
import org.marvelution.jji.data.synchronization.api.trigger.*;
import org.marvelution.jji.model.*;
import org.marvelution.jji.model.parsers.*;

import jakarta.json.*;

class TriggerReasonSiteDataClient<S extends Syncable<S>>
		implements SiteDataClient
{

	private final S syncable;
	private final TriggerReason triggerReason;
	private transient JsonValue data;

	TriggerReasonSiteDataClient(
			S syncable,
			TriggerReason triggerReason)
	{
		this.syncable = syncable;
		this.triggerReason = triggerReason;
	}

	@Override
	public Status getRemoteStatus(Syncable<?> syncable)
	{
		if (Objects.equals(this.syncable, syncable))
		{
			return Status.FIREWALLED;
		}
		else
		{
			return Status.OFFLINE;
		}
	}

	@Override
	public boolean populateJobDetails(Job job)
	{
		return populateDetails(job, ParserProvider.jobParser());
	}

	@Override
	public boolean populateBuildDetails(Build build)
	{
		return populateDetails(build, ParserProvider.buildParser());
	}

	@Override
	public void registerListener(SiteResponseListener listener)
	{}

	@Override
	public void unregisterListener(SiteResponseListener listener)
	{}

	private <T> boolean populateDetails(
			T target,
			MergingParser<T> parser)
	{
		if (Objects.equals(syncable, target))
		{
			if (data == null)
			{
				synchronized (this)
				{
					if (data == null)
					{
						try (JsonReader reader = Json.createReader(new StringReader(triggerReason.getData())))
						{
							data = reader.read();
						}
						catch (Exception e)
						{
							throw new IllegalArgumentException("Failed to parse synchronization data for " + syncable.getOperationId(), e);
						}
					}
				}
			}
			parser.parse(data, target);
			return true;
		}
		else
		{
			return false;
		}
	}
}
