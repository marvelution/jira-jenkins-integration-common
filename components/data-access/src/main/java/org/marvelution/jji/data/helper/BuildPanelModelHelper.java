/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.helper;

import java.time.*;
import java.util.*;
import java.util.stream.*;

import org.marvelution.jji.data.services.api.*;
import org.marvelution.jji.model.*;
import org.marvelution.jji.utils.*;

import jakarta.inject.*;

import static java.util.Comparator.*;
import static java.util.stream.Collectors.*;

@Named
@javax.inject.Named
public class BuildPanelModelHelper
{

    private final ConfigurationService configurationService;
    private final SiteService siteService;
    private final BuildService buildService;
    private final IssueLinkService issueLinkService;

    @Inject
    @javax.inject.Inject
    public BuildPanelModelHelper(
            ConfigurationService configurationService,
            SiteService siteService,
            BuildService buildService,
            IssueLinkService issueLinkService)
    {
        this.configurationService = configurationService;
        this.siteService = siteService;
        this.buildService = buildService;
        this.issueLinkService = issueLinkService;
    }

    public Map<String, Object> modelForDevStatusPanel(String issueKey)
    {
        Map<String, Object> model = newModel(issueKey);
        LinkStatistics stats = issueLinkService.getLinkStatistics(issueKey);
        model.put("buildCount", stats.getBuildTotal());
        LinkStatistics.BuildStats latestBuild = stats.getLatestBuild();
        model.put("buildDate",
                Optional.ofNullable(latestBuild)
                        .map(LinkStatistics.BuildStats::getTimestamp)
                        .map(Instant::ofEpochMilli)
                        .orElse(null));
        model.put("buildClasses",
                Optional.of(stats.getWorstResult())
                        .map(result -> PanelBuild.getBuildIcon(result) + " " + PanelBuild.getBuildColor(result))
                        .orElse(null));
        model.put("deployCount", stats.getDeployTotal());
        LinkStatistics.DeployStats latestDeploy = stats.getLatestDeploy();
        model.put("deployDate",
                Optional.ofNullable(latestDeploy)
                        .map(LinkStatistics.DeployStats::getTimestamp)
                        .map(Instant::ofEpochMilli)
                        .orElse(null));
        model.put("deployClasses",
                Optional.of(stats.getWorstDeployResult())
                        .map(result -> PanelBuild.getBuildIcon(result) + " " + PanelBuild.getBuildColor(result))
                        .orElse(null));
        return model;
    }

    public Map<String, Object> modelForBuildsDialog(
            String issueKey,
            String tab)
    {
        Map<String, Object> model = newModel(issueKey);
        List<PanelJob> jobs = relatedJobsWithBuilds(issueKey);
        model.put("jobs", jobs);
        Set<PanelDeployment> deployments = jobs.stream()
                .flatMap(job -> job.getBuilds()
                        .stream())
                .collect(Collector.of(() -> new HashMap<DeploymentEnvironment, Set<PanelBuild>>(), (result, build) -> {
                    for (DeploymentEnvironment deploymentEnvironment : build.getDeploymentEnvironments())
                    {
                        result.computeIfAbsent(deploymentEnvironment, d -> new HashSet<>())
                                .add(build);
                    }
                }, (left, right) -> {
                    right.forEach((env, builds) -> left.computeIfAbsent(env, e -> new HashSet<>())
                            .addAll(builds));
                    return left;
                }))
                .entrySet()
                .stream()
                .map(entry -> new PanelDeployment(entry.getKey(),
                        entry.getValue()
                                .stream()
                                .sorted(Comparator.comparing(PanelBuild::getDate)
                                        .reversed())
                                .collect(toList())))
                .collect(toSet());
        model.put("deployments", deployments);
        if ("deployments".equals(tab) || "builds".equals(tab))
        {
            model.put("tab", tab);
        }
        else
        {
            model.put("tab", "builds");
        }
        return model;
    }

    public Map<String, Object> modelForBuildTriggerDialog(String issueKey)
    {
        Map<String, Object> model = newModel(issueKey);
        List<PanelJob> jobs = relatedJobsWithLatestBuild(issueKey);
        if (jobs.stream()
                .anyMatch(job -> job.getJob()
                        .getSite()
                        .isInaccessibleSite()))
        {
            model.put("hasFirewalledSites", true);
            model.put("jobs",
                    jobs.stream()
                            .filter(job -> !job.getJob()
                                    .getSite()
                                    .isInaccessibleSite())
                            .collect(toList()));
        }
        else
        {
            model.put("hasFirewalledSites",
                    siteService.getAll()
                            .stream()
                            .anyMatch(Site::isInaccessibleSite));
            model.put("jobs", jobs);
        }
        return model;
    }

    public List<PanelBuild> relatedBuilds(String issueKey)
    {
        return getPanelBuildStream(issueKey).collect(toList());
    }

    public List<PanelJob> relatedJobsWithLatestBuild(String issueKey)
    {
        return issueLinkService.getJobsForIssue(false, issueKey)
                .stream()
                .map(job -> {
                    buildService.getLast(job)
                            .ifPresent(job::addBuild);
                    return new PanelJob(job);
                })
                .collect(toList());
    }

    public List<PanelJob> relatedJobsWithBuilds(String issueKey)
    {
        return getPanelBuildStream(issueKey).collect(groupingBy(PanelBuild::getJob))
                .entrySet()
                .stream()
                .map(entry -> {
                    PanelJob job = new PanelJob(entry.getKey(), entry.getValue());
                    if (!job.hasLastBuild())
                    {
                        buildService.getLast(job.getJob())
                                .map(PanelBuild::new)
                                .ifPresent(job::setLastBuild);
                    }
                    return job;
                })
                .collect(toList());
    }

    private Stream<PanelBuild> getPanelBuildStream(String issueKey)
    {
        return issueLinkService.getBuildsForIssue(issueKey,
                        configurationService.getMaximumBuildsPerPage(KeyExtractor.extractProjectKeyFromIssueKey(issueKey)),
                        true)
                .stream()
                .map(PanelBuild::new)
                .sorted(comparing(PanelBuild::getDate).reversed());
    }

    private Map<String, Object> newModel(String issueKey)
    {
        Map<String, Object> model = new HashMap<>();
        model.put("issueKey", issueKey);
        return model;
    }
}
