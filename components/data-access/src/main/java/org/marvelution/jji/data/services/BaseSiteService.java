/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.marvelution.jji.api.events.EventPublisher;
import org.marvelution.jji.api.text.TextResolver;
import org.marvelution.jji.data.access.api.SiteDAO;
import org.marvelution.jji.data.services.api.JobService;
import org.marvelution.jji.data.services.api.SiteClient;
import org.marvelution.jji.data.services.api.SiteService;
import org.marvelution.jji.data.services.api.SiteValidator;
import org.marvelution.jji.events.SiteDeletedEvent;
import org.marvelution.jji.model.*;
import org.marvelution.jji.model.response.SiteTunnelDetails;
import org.marvelution.jji.synctoken.SyncTokenAuthenticator;
import org.marvelution.jji.synctoken.utils.SharedSecretGenerator;
import org.marvelution.jji.validation.api.ErrorMessages;
import org.marvelution.jji.validation.api.ValidationException;

import static java.util.Objects.requireNonNull;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.marvelution.jji.data.services.api.ConfigurationService.GLOBAL_SCOPE;

/**
 * Base {@link SiteService} implementation.
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class BaseSiteService
        implements SiteService, SyncTokenAuthenticator.SharedSecretProvider
{

    protected final SiteDAO siteDAO;
    protected final SiteClient siteClient;
    protected final JobService jobService;
    protected final SiteValidator siteValidator;
    protected final EventPublisher eventPublisher;
    protected final TextResolver textResolver;
    protected final TunnelManager tunnelManager;

    protected BaseSiteService(
            SiteDAO siteDAO,
            SiteClient siteClient,
            JobService jobService,
            SiteValidator siteValidator,
            EventPublisher eventPublisher,
            TextResolver textResolver,
            TunnelManager tunnelManager)
    {
        this.siteDAO = requireNonNull(siteDAO);
        this.siteClient = requireNonNull(siteClient);
        this.jobService = requireNonNull(jobService);
        this.siteValidator = requireNonNull(siteValidator);
        this.eventPublisher = eventPublisher;
        this.textResolver = textResolver;
        this.tunnelManager = tunnelManager;
    }

    @Override
    public Optional<Site> get(
            String siteId,
            boolean includeJobs,
            String jobSearchTerm)
    {
        return Optional.ofNullable(siteDAO.get(siteId))
                .map(site -> augmentSite(site, includeJobs, jobSearchTerm));
    }

    @Override
    public List<Site> getAll(boolean includeJobs)
    {
        return siteDAO.getAll()
                .stream()
                .map(site -> augmentSite(site, includeJobs, null))
                .collect(Collectors.toList());
    }

    @Override
    public Site add(Site site)
    {
        if (site.getId() != null)
        {
            throw new IllegalArgumentException(textResolver.getText("site.id.present.on.add"));
        }
        else
        {
            return save(site);
        }
    }

    @Override
    public Site update(
            String siteId,
            Site site)
    {
        requireNonNull(siteId, textResolver.getText("site.id.required"));
        if (!Objects.equals(siteId, site.getId()))
        {
            throw new IllegalArgumentException(textResolver.getText("site.id.mismatch"));
        }
        else
        {
            Site existing = getExisting(siteId);
            existing.setType(site.getType());
            existing.setScope(site.getScope());
            existing.setName(site.getName());
            existing.setRpcUrl(site.getRpcUrl());
            existing.setDisplayUrl(site.getDisplayUrl());
            existing.setAutoLinkNewJobs(site.isAutoLinkNewJobs());
            existing.setUseCrumbs(site.isUseCrumbs());
            existing.setConnectionType(site.getConnectionType());
            if (isNotBlank(site.getUser()))
            {
                String token = Optional.ofNullable(site.getToken())
                        .filter(t -> !Obfuscate.isObfuscated(t))
                        .orElse(null);
                if (existing.usesBasicAuthentication())
                {
                    if (token == null)
                    {
                        throw new ValidationException("token", textResolver.getText("site.update.missing.token"));
                    }
                    else if (!token.equals(existing.getToken()))
                    {
                        throw new ValidationException("token", textResolver.getText("site.update.token.mismatch"));
                    }
                    if (isNotBlank(site.getNewToken()))
                    {
                        existing.setUser(site.getUser());
                        existing.setToken(site.getNewToken());
                    }
                }
                else
                {
                    existing.setUser(site.getUser());
                    existing.setToken(site.getToken());
                }
                existing.setAuthenticationType(SiteAuthenticationType.BASIC);
            }
            else
            {
                existing.setAuthenticationType(SiteAuthenticationType.TOKEN);
                existing.setUser(null);
                existing.setToken(null);
            }
            return save(existing);
        }
    }

    @Override
    public void validateSite(
            Site site,
            String[] fields)
            throws ValidationException
    {
        List<String> fieldList = Stream.of(fields)
                .flatMap(field -> Stream.of(field.split(",")))
                .collect(Collectors.toList());
        ErrorMessages errorMessages = siteValidator.validate(site)
                .getErrors()
                .stream()
                .filter(error -> fieldList.contains(error.getField()))
                .collect(Collectors.collectingAndThen(Collectors.toList(), errors -> new ErrorMessages().setErrors(errors)));
        if (errorMessages.hasErrors())
        {
            throw new ValidationException(errorMessages);
        }
    }

    @Override
    public Site save(Site site)
    {
        if (site.getSharedSecret() == null)
        {
            site.setSharedSecret(SharedSecretGenerator.generate());
        }
        siteValidator.assertValid(site);
        if (site.getId() == null)
        {
            site.setId(HasId.newId());
        }
        if (site.getScope() == null)
        {
            site.setScope(GLOBAL_SCOPE);
        }
        if (siteClient.isJenkinsPluginInstalled(site))
        {
            site.setJenkinsPluginInstalled(true);
        }
        if (site.usesTokenAuthentication())
        {
            site.setUser(null)
                    .setToken(null);
        }
        Site saved = siteDAO.save(site);
        tunnelManager.setupTunnelingIfNeeded(saved);
        return saved;
    }

    @Override
    public void enableSite(
            String siteId,
            boolean enabled)
    {
        siteDAO.enableSite(siteId, enabled);
    }

    @Override
    public void automaticallyEnableNewJobs(
            String siteId,
            boolean enabled)
    {
        siteDAO.enableNewJobs(siteId, enabled);
    }

    @Override
    public void delete(Site site)
    {
        asyncUnregisterFromSite(site);
        tunnelManager.cleanupTunnelIfNeeded(site);
        siteDAO.delete(site.getId());
        eventPublisher.publish(new SiteDeletedEvent(site));
    }

    @Override
    public void deleteDeletedJobs(Site site)
    {
        jobService.getDeletedJobs(site)
                .forEach(jobService::delete);
    }

    @Override
    public SiteStatus getSiteStatus(Site site)
    {
        SiteStatus status = new SiteStatus().setStatus(siteClient.getRemoteStatus(site));
        if (status.getStatus()
                .isAccessible())
        {
            siteClient.getJenkinsPluginVersion(site)
                    .ifPresent(status::setPluginVersion);
        }
        return status;
    }

    @Override
    public SiteTunnelDetails getTunnelDetails(Site site)
    {
        return tunnelManager.getTunnelDetails(site);
    }

    private Site augmentSite(
            Site site,
            boolean includeJobs,
            String jobSearchTerm)
    {
        if (includeJobs)
        {
            if (isNotBlank(jobSearchTerm))
            {
                site.setJobs(jobService.search(site, jobSearchTerm));
            }
            else
            {
                site.setJobs(jobService.getRootJobsOnSite(site));
            }
        }
        else
        {
            site.setHasJobs(jobService.countBySite(site) > 0);
            site.setHasDeletedJobs(jobService.hasDeletedJobs(site));
        }
        return site;
    }

    protected void asyncUnregisterFromSite(Site site)
    {
        CompletableFuture.runAsync(() -> unregisterFromSite(site));
    }

    protected void unregisterFromSite(Site site)
    {
        siteClient.unregisterWithSite(site);
    }

    @Override
    public Optional<String> getSharedSecretForIssuer(String issuer)
    {
        return get(issuer).map(Site::getSharedSecret);
    }
}
