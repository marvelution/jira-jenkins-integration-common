/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services.api;

import java.util.List;
import java.util.Optional;

import org.marvelution.jji.data.services.api.exceptions.CannotDeleteJobException;
import org.marvelution.jji.data.services.api.exceptions.UnknownJobException;
import org.marvelution.jji.model.Job;
import org.marvelution.jji.model.Site;

/**
 * {@link Job} services.
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public interface JobService
{

    /**
     * Returns the number of Jobs that are related to the specified {@link Site}.
     *
     * @since 1.3.0
     */
    long countBySite(Site site);

    /**
     * Returns whether the specified {@link Site} has any jobs marked as deleted.
     *
     * @since 1.8.0
     */
    boolean hasDeletedJobs(Site site);

    /**
     * Returns the {@link Job}s that are marked as deleted on the specified {@link Site}.
     *
     * @since 1.14.0
     */
    List<Job> getDeletedJobs(Site site);

    /**
     * Return the {@link Job jobs} related to the specified {@link Site}.
     *
     * @since 1.14.0
     */
    List<Job> getRootJobsOnSite(Site site);

    /**
     * Returns the parent {@link Job} of the specified {@link Job} if any.
     *
     * @since 1.14.0
     */
    Optional<Job> getParent(Job job);

    /**
     * Returns all the parent {@link Job}s of the specified {@link Job} in order.
     *
     * @since 1.14.0
     */
    List<Job> getParents(Job job);

    /**
     * Return the {@link Job}s that are child/sub jobs of the specified {@link Job}.
     *
     * @since 1.14.0
     */
    List<Job> getSubJobs(Job job);

    /**
     * Returns a {@link Job} with the specified {@code jobId}.
     */
    default Optional<Job> get(String jobId)
    {
        return get(jobId, false);
    }

    /**
     * Returns a {@link Job} with the specified {@code jobId}, optionally including first level sub jobs.
     */
    Optional<Job> get(
            String jobId,
            boolean includeSubJobs);

    /**
     * Returns the {@link Job} with the specified {@code jobId}, throwing {@link UnknownJobException} if there is no job with the specified
     * {@code jobId}.
     */
    default Job getExisting(String jobId)
    {
        return getExisting(jobId, false);
    }

    /**
     * Returns the {@link Job} with the specified {@code jobId}, optionally including first level sub jobs, throwing {@link
     * UnknownJobException} if there is no job with the specified {@code jobId}.
     */
    default Job getExisting(
            String jobId,
            boolean includeSubJobs)
    {
        return get(jobId, includeSubJobs).orElseThrow(() -> new UnknownJobException(jobId));
    }

    /**
     * Returns the {@link Job job} that match the specified {@code hash}.
     */
    Optional<Job> findByHash(
            Site site,
            String hash);

    /**
     * Returns {@link Job jobs} that have the specified {@code name}.
     */
    List<Job> find(String name);

    /**
     * Search for {@link Job jobs} that have the specified {@code term} in there name or display name.
     *
     * @since 1.5.0
     */
    default List<Job> search(String term)
    {
        return search(term, false);
    }

    List<Job> search(
            String term,
            boolean exactMatch);

    /**
     * Search for {@link Job jobs} that have the specified {@code term} in there name or display name limiting results to jobs from the
     * specified {@link Site}.
     *
     * @since 1.12.0
     */
    List<Job> search(
            Site site,
            String term);

    /**
     * Enable/Disable the synchronization of the specified {@link Job}.
     */
    void enable(
            Job job,
            boolean enabled);

    /**
     * Enable/Disable the synchronization of the {@link Job} with the specified {@code jobId}.
     */
    default void enable(
            String jobId,
            boolean enabled)
    {
        enable(getExisting(jobId), enabled);
    }

    /**
     * Save the specified {@link Job} and return its new state.
     */
    Job save(Job job);

    /**
     * Delete the specified {@link Job}.
     */
    void delete(Job job)
            throws CannotDeleteJobException;

    /**
     * Delete the {@link Job} with the specified {@code jobId}.
     */
    default void delete(String jobId)
            throws CannotDeleteJobException
    {
        delete(getExisting(jobId));
    }

    /**
     * Delete all builds of the specified {@link Job}.
     */
    void deleteAllBuilds(Job job);

    /**
     * Delete all builds of the {@link Job} with the specified {@code jobId}.
     */
    default void deleteAllBuilds(String jobId)
    {
        deleteAllBuilds(getExisting(jobId));
    }

    /**
     * Rebuild the build cache of the specified {@link Job}.
     */
    void resetBuildCache(Job job);

    List<Job> getLatestActiveJobs(int limit);
}
