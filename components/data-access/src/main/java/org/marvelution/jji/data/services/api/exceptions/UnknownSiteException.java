/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services.api.exceptions;

import org.marvelution.jji.model.Site;

/**
 * Exception thrown in case a requested {@link Site} doesn't exist.
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class UnknownSiteException extends RuntimeException {

	public static final String MESSAGE_PREFIX = "Unknown site with id: ";
	private static final long serialVersionUID = 4219808032929577409L;
	private final String siteId;

	public UnknownSiteException(String siteId) {
		super(MESSAGE_PREFIX + siteId);
		this.siteId = siteId;
	}

	public String getSiteId() {
		return siteId;
	}
}
