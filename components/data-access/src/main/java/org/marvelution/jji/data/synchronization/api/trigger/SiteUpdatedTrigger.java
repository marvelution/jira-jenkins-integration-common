/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.synchronization.api.trigger;

import java.util.*;

import org.marvelution.jji.model.*;

import jakarta.json.bind.annotation.*;

/**
 * {@link TriggerReason} used when a {@link Site} is updated.
 *
 * @author Mark Rekveld
 * @since 1.8.0
 */
public class SiteUpdatedTrigger
		extends AbstractSiteTrigger
{

	@JsonbCreator
	public SiteUpdatedTrigger(
			@JsonbProperty String siteId,
			@JsonbProperty String userId)
	{
		super(siteId, userId);
	}

	@Override
	public String describe()
	{
		return "site updated by " + userId;
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(siteId, userId, getData());
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}
		SiteUpdatedTrigger that = (SiteUpdatedTrigger) o;
		return siteId.equals(that.siteId) && userId.equals(that.userId) && Objects.equals(getData(), that.getData());
	}
}
