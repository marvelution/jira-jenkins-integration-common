//
// Copyright (c) 2012-present Marvelution Holding B.V.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

syntax = "proto3";

package migration;
option java_package = "org.marvelution.jji.data.migration";
option java_multiple_files = true;

message MigrationPartsList {
  repeated string label = 1;
}

message MigrationData {
  ConfigurationData configuration = 1;
  repeated SiteData sites = 2;
  repeated LinkStatsData linkStatsData = 5;
  repeated JobData jobs = 6;
  repeated BuildData builds = 7;
}

message ConfigurationData {
  repeated ConfigurationSettingData settings = 1;
}

message ConfigurationSettingData {
  string key = 1;
  string value = 2;
  string scope = 3;
}

message SiteData {
  string id = 1;
  string name = 2;
  string sharedSecret = 3;

  enum Type {
    JENKINS = 0;
    HUDSON = 1;
  }
  Type type = 4;

  string rpcUrl = 5;
  string displayUrl = 6;
  string user = 7;
  string token = 8;
  bool useCrumbs = 9;
  bool jenkinsPluginInstalled = 10;
  bool registrationComplete = 11;
  bool autoLinkNewJobs = 12;
  bool firewalled = 13;

  repeated JobData jobs = 14;
  string scope = 15;

  enum ConnectionType {
    ACCESSIBLE = 0;
    INACCESSIBLE = 1;
    TUNNELED = 3;
  }
  ConnectionType connectionType = 16;
}

message JobData {
  string id = 1;
  string siteId = 2;
  string name = 3;
  string parentName = 4;
  string fullName = 5;
  string urlName = 6;
  string displayName = 7;
  string description = 8;
  int32 lastBuild = 9;
  int32 oldestBuild = 10;
  bool linked = 11;
  bool deleted = 12;

  repeated BuildData builds = 13;
}

message BuildData {
  string id = 1;
  string jobId = 2;
  int32 number = 3;
  string displayName = 4;
  string description = 5;
  bool deleted = 6;
  string cause = 7;

  enum Result {
    SUCCESS = 0;
    UNSTABLE = 1;
    FAILURE = 2;
    NOT_BUILT = 3;
    ABORTED = 4;
    UNKNOWN = 5;
  }
  Result result = 8;

  string builtOn = 9;
  int64 duration = 10;
  int64 timestamp = 11;

  repeated string branches = 12;

  message ChangeSet {
    string commitId = 1;
    string message = 2;
  }
  repeated ChangeSet changeSet = 13;

  message TestResults {
    int32 total = 1;
    int32 failed = 2;
    int32 skipped = 3;
  }
  TestResults testResults = 14;

  message DeploymentEnvironment {
    string id = 1;
    string name = 2;

    enum DeploymentEnvironmentType {
      UNMAPPED = 0;
      DEVELOPMENT = 1;
      TESTING = 2;
      STAGING = 3;
      PRODUCTION = 4;
    }
    DeploymentEnvironmentType type = 3;
  }
  repeated DeploymentEnvironment deploymentEnvironments = 15;

  repeated string issueReferences = 16;
}

message LinkStatsData {
  string issueKey = 1;
  string statsJson = 2;
}
