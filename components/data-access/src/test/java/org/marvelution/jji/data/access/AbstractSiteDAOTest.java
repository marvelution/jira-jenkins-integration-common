/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access;

import java.net.*;
import java.util.*;

import org.marvelution.jji.model.*;

import org.junit.jupiter.api.*;

import static org.marvelution.jji.model.Matchers.*;

import static org.assertj.core.api.Assertions.*;

/**
 * @author Mark Rekveld
 * @since 1.0.0
 */
public abstract class AbstractSiteDAOTest
		extends AbstractDAOTest
{

	@Test
	void testCRUD()
	{
		// Create
		Site site = new Site().setName(getTestMethodName())
				.setSharedSecret("my-super-secret")
				.setType(SiteType.JENKINS)
				.setRpcUrl(URI.create("http://localhost:8080/"))
				.setDisplayUrl(URI.create("http://localhost/"))
				.setUser("admin")
				.setToken("admin")
				.setAutoLinkNewJobs(true)
				.setJenkinsPluginInstalled(true)
				.setUseCrumbs(true);
		Site inserted = siteDAO.save(site);
		assertThat(inserted).is(equalTo(site, true));
		assertThat(inserted.getSharedSecret()).isNotBlank();

		// Read
		assertThat(siteDAO.get(inserted.getId())).is(equalTo(inserted));
		assertThat(siteDAO.findByRpcUrl(inserted.getRpcUrl()).orElse(null)).is(equalTo(inserted));

		// Update
		String sharedSecret = inserted.getSharedSecret();
		inserted.setName("Updated").setSharedSecret("updated-secret").setDisplayUrl(null);
		Site updated = siteDAO.save(inserted);
		assertThat(updated).is(equalTo(inserted));
		// SharedSecret should not be updated in the database
		assertThat(siteDAO.get(updated.getId())).extracting(Site::getSharedSecret).isEqualTo(sharedSecret);

		// Delete
		siteDAO.delete(updated.getId());
		assertThat(siteDAO.get(updated.getId())).isNull();
	}

	@Test
	void testGetAll()
	{
		Site[] sites = new Site[] { siteDAO.save(new Site().setName(getTestMethodName() + " - 1")
				                                         .setSharedSecret("my-super-secret")
				                                         .setType(SiteType.JENKINS)
				                                         .setRpcUrl(URI.create("http://localhost:8080/"))),
				siteDAO.save(new Site().setName(getTestMethodName() + " - 2")
						             .setSharedSecret("my-super-secret")
						             .setType(SiteType.HUDSON)
						             .setRpcUrl(URI.create("http://localhost:8081/"))
						             .setDisplayUrl(URI.create("http://localhost/"))) };
		List<Site> stored = siteDAO.getAll();
		assertThat(stored).hasSize(2).contains(sites);
	}

	@Test
	void testSave_TrailingSlashAdded()
	{
		Site site = siteDAO.save(new Site().setName(getTestMethodName())
				                         .setSharedSecret("my-super-secret")
				                         .setType(SiteType.JENKINS)
				                         .setRpcUrl(URI.create("http://localhost:8080"))
				                         .setDisplayUrl(URI.create("http://localhost")));
		assertThat(site.getRpcUrl()).isEqualTo(URI.create("http://localhost:8080/"));
		assertThat(site.getDisplayUrl()).isEqualTo(URI.create("http://localhost/"));
	}

	@Test
	void testDeleteAllScoped()
	{
		Site globalSite = siteDAO.save(new Site().setName(getTestMethodName() + " - 1")
				                               .setSharedSecret("my-super-secret")
				                               .setType(SiteType.JENKINS)
				                               .setRpcUrl(URI.create("http://localhost:8080/")));
		Site scopedSite = siteDAO.save(new Site().setName(getTestMethodName() + " - 2")
				                               .setScope("TEST")
				                               .setSharedSecret("my-super-secret")
				                               .setType(SiteType.HUDSON)
				                               .setRpcUrl(URI.create("http://localhost:8081/"))
				                               .setDisplayUrl(URI.create("http://localhost/")));
		assertThat(siteDAO.getAll()).hasSize(2).contains(globalSite, scopedSite);
		siteDAO.deleteAll("TEST");
		assertThat(siteDAO.getAll()).hasSize(1).contains(globalSite);
	}

	@Test
	void testDeleteAll()
	{
		Site globalSite = siteDAO.save(new Site().setName(getTestMethodName() + " - 1")
				                               .setSharedSecret("my-super-secret")
				                               .setType(SiteType.JENKINS)
				                               .setRpcUrl(URI.create("http://localhost:8080/")));
		Site scopedSite = siteDAO.save(new Site().setName(getTestMethodName() + " - 2")
				                               .setScope("TEST")
				                               .setSharedSecret("my-super-secret")
				                               .setType(SiteType.HUDSON)
				                               .setRpcUrl(URI.create("http://localhost:8081/"))
				                               .setDisplayUrl(URI.create("http://localhost/")));
		assertThat(siteDAO.getAll()).hasSize(2).contains(globalSite, scopedSite);
		siteDAO.deleteAll();
		assertThat(siteDAO.getAll()).isEmpty();
	}
}
