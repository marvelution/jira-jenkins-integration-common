/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services;

import java.util.*;

import org.marvelution.jji.events.*;
import org.marvelution.jji.model.*;

import jakarta.inject.*;
import org.junit.jupiter.api.*;

import static java.util.stream.Collectors.*;
import static java.util.stream.Stream.*;
import static org.assertj.core.api.Assertions.*;
import static org.marvelution.jji.model.Matchers.*;

public abstract class AbstractBaseBuildServiceTest<S extends BaseBuildService>
        extends AbstractBaseServiceTest
{

    @Inject
    @javax.inject.Inject
    protected S buildService;
    protected Job job;

    @BeforeEach
    void createDefaultJob()
    {
        job = setupJob(createJob(createSite()));
    }

    protected abstract Job setupJob(Job job);

    protected abstract Build setupBuild(
            Build build,
            IssueReference... issueReferences);

    private List<Build> setupBuilds()
    {
        return of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10).map(number -> new Build().setJob(job)
                        .setNumber(number)
                        .setResult(Result.SUCCESS))
                .map(this::setupBuild)
                .collect(toList());
    }

    @Test
    void testCRUD()
    {
        // Create
        Build build = new Build().setJob(job)
                .setNumber(1)
                .setResult(Result.SUCCESS);
        Build inserted = buildService.save(build);
        assertThat(inserted).is(equalTo(build, true));

        // Read
        assertThat(buildService.get(inserted.getId())).isPresent()
                .get()
                .is(equalTo(inserted));
        assertThat(buildService.get(job, inserted.getNumber())).isPresent()
                .get()
                .is(equalTo(inserted));

        // Update
        inserted.setTestResults(new TestResults().setFailed(1)
                        .setSkipped(1)
                        .setTotal(4))
                .setResult(Result.FAILURE);
        Build updated = buildService.save(inserted);
        assertThat(updated).is(equalTo(inserted));

        // Delete
        buildService.delete(inserted);
        assertThat(buildService.get(inserted.getId())).isEmpty();
        assertThat(buildService.get(job, inserted.getNumber())).isEmpty();
        assertEventPublished(new BuildDeletedEvent(inserted));
    }

    @Test
    void testGetAllInRange()
    {
        List<Build> builds = setupBuilds();

        Set<Build> allByJob = buildService.getByJob(job);
        assertThat(allByJob).hasSize(10)
                .containsAll(builds);

        Set<Build> allInRange = buildService.getAllInRange(job, 1, -1);
        assertThat(allInRange).hasSize(10)
                .containsAll(builds);

        allInRange = buildService.getAllInRange(job, 4, 8);
        assertThat(allInRange).hasSize(5)
                .containsAll(builds.stream()
                        .filter(build -> build.getNumber() >= 4)
                        .filter(build -> build.getNumber() <= 8)
                        .collect(toList()));
    }

    @Test
    void testDeleteAllByJob()
    {
        List<Build> builds = setupBuilds();

        Set<Build> allByJob = buildService.getByJob(job);
        assertThat(allByJob).hasSize(10)
                .containsAll(builds);

        buildService.deleteAllInJob(job);
        allByJob = buildService.getByJob(job);
        assertThat(allByJob).hasSize(0);
        assertEventPublished(new BuildsDeletedEvent(job));
    }

    @Test
    void testMarkAsDeleted()
    {
        Build build = setupBuild(new Build().setJob(job)
                .setNumber(1)
                .setResult(Result.SUCCESS));
        assertThat(buildService.get(build.getId())).isPresent()
                .get()
                .is(equalTo(build));
        buildService.markAsDeleted(build);
        Build marked = buildService.get(build.getId())
                .orElse(null);
        assertThat(marked).is(equalTo(build.setDeleted(true)));
    }

    @Test
    void testMarkAllInJobAsDelete()
    {
        setupBuilds();

        buildService.markAllInJobAsDeleted(job, 5);
        for (Build build : buildService.getByJob(job))
        {
            assertThat(build.isDeleted()).isEqualTo(build.getNumber() < 5);
        }
        buildService.markAllInJobAsDeleted(job);
        for (Build build : buildService.getByJob(job))
        {
            assertThat(build.isDeleted()).isTrue();
        }
    }
}
