/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services;

import java.net.URI;
import java.util.*;
import java.util.stream.Stream;
import javax.annotation.Nullable;

import org.marvelution.jji.api.text.TextResolver;
import org.marvelution.jji.data.access.api.IssueReferenceProvider;
import org.marvelution.jji.data.services.api.*;
import org.marvelution.jji.model.IssueReference;
import org.marvelution.jji.model.Job;
import org.marvelution.jji.model.Site;
import org.marvelution.jji.model.request.TriggerBuildsRequest;
import org.marvelution.jji.model.response.TriggerBuildsResponse;
import org.marvelution.jji.model.response.TriggerBuildsResponse.TriggerResultType;
import org.marvelution.testing.TestSupport;

import org.assertj.core.api.Condition;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.quality.Strictness;

import static java.util.Arrays.asList;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static java.util.stream.Collectors.toSet;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.entry;
import static org.marvelution.jji.data.services.api.SiteClient.BuildJobResponse.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

class JobTriggerServiceTest
        extends TestSupport
{

    @Mock
    private ConfigurationService configurationService;
    @Mock
    private IssueReferenceProvider issueReferenceProvider;
    @Mock
    private IssueLinkService issueLinkService;
    @Mock
    private SiteClient siteClient;
    @Mock
    private JobService jobService;
    @Mock(strictness = Mock.Strictness.LENIENT)
    private PermissionService<Object> permissionService;
    private JobTriggerService helper;
    @Captor
    private ArgumentCaptor<SiteClient.BuildJobRequest> requestCaptor;

    @BeforeEach
    void setUp()
    {
        TextResolver textResolver = mock(TextResolver.class,
                withSettings().strictness(Strictness.LENIENT)
                        .defaultAnswer(invocation -> invocation.<String>getArgument(0)));
        helper = new JobTriggerService(configurationService,
                issueReferenceProvider,
                issueLinkService,
                siteClient,
                jobService,
                textResolver,
                permissionService)
        {
            @Override
            protected String getCurrentUserId()
            {
                return "1234";
            }

            @Override
            protected String getCurrentUserDisplayName()
            {
                return "Admin";
            }
        };
        when(permissionService.canTriggerBuilds(any(Job.class))).thenReturn(true);
    }

    @Test
    void testFindTriggerableJobs()
    {
        Site site1 = new Site().setJenkinsPluginInstalled(false);
        Site site2 = new Site().setJenkinsPluginInstalled(true);
        Site site3 = new Site().setInaccessibleSite()
                .setJenkinsPluginInstalled(true);

        Job job1 = new Job().setName("Job 1")
                .setSite(site1)
                .setLinked(true);
        Job job2 = new Job().setName("Job 2")
                .setSite(site2)
                .setLinked(true);
        Job job3 = new Job().setName("Job 3")
                .setSite(site2)
                .setDeleted(true);
        Job job4 = new Job().setName("Job 4")
                .setSite(site2);
        Job job5 = new Job().setName("Job 5")
                .setSite(site3)
                .setLinked(true);

        when(jobService.search("job")).thenReturn(asList(job1, job2, job3, job4, job5));

        List<Job> jobs = helper.findTriggerableJobs("job");

        assertThat(jobs).hasSize(1)
                .containsOnly(job2);
    }

    @ParameterizedTest
    @MethodSource("triggerableJob")
    void testGetTriggerableJob(
            String jobId,
            @Nullable
            Job job,
            boolean triggerable)
    {
        when(jobService.get(jobId)).thenReturn(Optional.ofNullable(job));

        assertThat(helper.getTriggerableJob(jobId)).is(new Condition<>(found -> found.isPresent() == triggerable,
                "job %s to be %s triggerable",
                job,
                triggerable));
    }

    @Test
    void testTriggerBuilds()
    {
        when(configurationService.useUserId("TEST")).thenReturn(false);

        Site site = new Site().setName("site")
                .setRpcUrl(URI.create("https://jenkins.example.com"))
                .setJenkinsPluginInstalled(true);
        Job job1 = new Job().setId("job-1")
                .setName("job-1")
                .setSite(site)
                .setLinked(true);
        Job job2 = new Job().setId("job-2")
                .setName("job-2")
                .setSite(site)
                .setLinked(true);
        Job job3 = new Job().setId("job-3")
                .setName("job-3")
                .setSite(site)
                .setLinked(true);
        Job job4 = new Job().setId("job-4")
                .setName("job-4")
                .setSite(site)
                .setLinked(true);
        Job job5 = new Job().setId("job-5")
                .setName("job-5")
                .setSite(site)
                .setLinked(true);
        IssueReference issueReference = new IssueReference().setIssueKey("TEST-1");
        when(issueReferenceProvider.getIssueReference(eq("TEST-1"), any(IssueReferenceProvider.FieldContext.class))).thenReturn(of(
                issueReference));
        when(jobService.get(anyString())).then(invocation -> {
            switch (invocation.<String>getArgument(0))
            {
                case "job-1":
                    return Optional.of(job1);
                case "job-2":
                    return Optional.of(job2);
                case "job-3":
                    return Optional.of(job3);
                case "job-4":
                    return Optional.of(job4);
                case "job-5":
                    return Optional.of(job5);
                default:
                    return empty();
            }
        });
        when(siteClient.buildJob(any(SiteClient.BuildJobRequest.class))).then(invocation -> {
            SiteClient.BuildJobRequest request = invocation.getArgument(0);
            if (Objects.equals(job1, request.getJob()))
            {
                return BUILD_SCHEDULED;
            }
            else if (Objects.equals(job2, request.getJob()))
            {
                return NOT_SUPPORTED;
            }
            else if (Objects.equals(job3, request.getJob()))
            {
                return ACCEPTED;
            }
            else if (Objects.equals(job4, request.getJob()))
            {
                return FAILED;
            }
            else if (Objects.equals(job5, request.getJob()))
            {
                return JOB_NOT_BUILDABLE;
            }
            else
            {
                throw new IllegalArgumentException();
            }
        });

        TriggerBuildsResponse response = helper.triggerBuilds(new TriggerBuildsRequest().setJobs(asSet("job-1",
                        "job-2",
                        "job-3",
                        "job-4",
                        "job-5",
                        "job-6"))
                .setIssueKey("TEST-1"));

        assertThat(response.getFailures()).hasSize(3)
                .containsOnly(new TriggerBuildsResponse.TriggerResult().setType(TriggerResultType.ERROR)
                                .setMessage("failed.to.trigger.build")
                                .setLink(job4.getDisplayUrl()),
                        new TriggerBuildsResponse.TriggerResult().setType(TriggerResultType.ERROR)
                                .setMessage("unsupported.site.for.build.triggers"),
                        new TriggerBuildsResponse.TriggerResult().setType(TriggerResultType.ERROR)
                                .setMessage("unknown.job"));
        assertThat(response.getWarnings()).hasSize(1)
                .containsOnly(new TriggerBuildsResponse.TriggerResult().setType(TriggerResultType.WARNING)
                        .setMessage("job.not.buidable")
                        .setLink(job5.getDisplayUrl()));
        assertThat(response.getSuccesses()).hasSize(2)
                .containsOnly(new TriggerBuildsResponse.TriggerResult().setType(TriggerResultType.SUCCESS)
                                .setMessage("successfully.scheduled.build")
                                .setLink(job1.getDisplayUrl()),
                        new TriggerBuildsResponse.TriggerResult().setType(TriggerResultType.SUCCESS)
                                .setMessage("successfully.send.build.request")
                                .setLink(job3.getDisplayUrl()));

        verify(siteClient, times(5)).buildJob(any(SiteClient.BuildJobRequest.class));
        verify(configurationService).useUserId("TEST");
        verify(configurationService).getParameterIssueFields("TEST");
        verify(configurationService).getParameterIssueFieldNames("TEST");
        verify(configurationService).getParameterIssueFieldMappers("TEST");
        verifyNoMoreInteractions(configurationService);
    }

    @Test
    void testTriggerBuilds_LinkedToIssue()
    {
        when(configurationService.useUserId("TEST")).thenReturn(false);

        Site site = new Site().setName("site")
                .setRpcUrl(URI.create("https://jenkins.example.com"))
                .setJenkinsPluginInstalled(true);
        Job job1 = new Job().setId("job-1")
                .setName("job-1")
                .setSite(site)
                .setLinked(true);
        IssueReference issueReference = new IssueReference().setIssueKey("TEST-1");
        when(issueReferenceProvider.getIssueReference(eq("TEST-1"), any(IssueReferenceProvider.FieldContext.class))).thenReturn(of(
                issueReference));
        when(issueLinkService.getJobsForIssue(eq(false), eq("TEST-1"))).thenReturn(Collections.singleton(job1));
        when(siteClient.buildJob(any(SiteClient.BuildJobRequest.class))).thenReturn(ACCEPTED);

        TriggerBuildsResponse response = helper.triggerBuilds(new TriggerBuildsRequest().setIssueKey("TEST-1")
                .setLinked());

        assertThat(response).extracting(TriggerBuildsResponse::hasSuccesses,
                        TriggerBuildsResponse::hasWarnings,
                        TriggerBuildsResponse::hasFailures)
                .containsExactly(true, false, false);

        verify(siteClient).buildJob(any(SiteClient.BuildJobRequest.class));
        verify(issueLinkService).getJobsForIssue(eq(false), eq("TEST-1"));
        verify(configurationService).useUserId("TEST");
        verify(configurationService).getParameterIssueFields("TEST");
        verify(configurationService).getParameterIssueFieldNames("TEST");
        verify(configurationService).getParameterIssueFieldMappers("TEST");
        verifyNoMoreInteractions(configurationService);
    }

    @Test
    void testTriggerBuilds_ByConvention()
    {
        when(configurationService.useUserId("TEST")).thenReturn(false);

        Site site = new Site().setName("site")
                .setRpcUrl(URI.create("https://jenkins.example.com"))
                .setJenkinsPluginInstalled(true);
        Job job1 = new Job().setId("job-1")
                .setName("feature/TEST-1")
                .setSite(site)
                .setLinked(true);
        IssueReference issueReference = new IssueReference().setIssueKey("TEST-1");
        when(issueReferenceProvider.getIssueReference(eq("TEST-1"), any(IssueReferenceProvider.FieldContext.class))).thenReturn(of(
                issueReference));
        when(jobService.search(eq("TEST-1"))).thenReturn(Collections.singletonList(job1));
        when(siteClient.buildJob(any(SiteClient.BuildJobRequest.class))).thenReturn(ACCEPTED);

        TriggerBuildsResponse response = helper.triggerBuilds(new TriggerBuildsRequest().setIssueKey("TEST-1")
                .setByConvention());

        assertThat(response).extracting(TriggerBuildsResponse::hasSuccesses,
                        TriggerBuildsResponse::hasWarnings,
                        TriggerBuildsResponse::hasFailures)
                .containsExactly(true, false, false);

        verify(siteClient).buildJob(any(SiteClient.BuildJobRequest.class));
        verify(jobService).search(eq("TEST-1"));
        verify(configurationService).useUserId("TEST");
        verify(configurationService).getParameterIssueFields("TEST");
        verify(configurationService).getParameterIssueFieldNames("TEST");
        verify(configurationService).getParameterIssueFieldMappers("TEST");
        verifyNoMoreInteractions(configurationService);
    }

    @Test
    void testTriggerBuilds_ByLookup()
    {
        when(configurationService.useUserId(null)).thenReturn(false);

        Site site = new Site().setName("site")
                .setRpcUrl(URI.create("https://jenkins.example.com"))
                .setJenkinsPluginInstalled(true);
        Job job1 = new Job().setId("job-1")
                .setName("feature/lookup")
                .setSite(site)
                .setLinked(true);
        when(jobService.search(eq("look"), eq(false))).thenReturn(Collections.singletonList(job1));
        when(siteClient.buildJob(any(SiteClient.BuildJobRequest.class))).thenReturn(ACCEPTED);

        TriggerBuildsResponse response = helper.triggerBuilds(new TriggerBuildsRequest().setLookupJob("look"));

        assertThat(response).extracting(TriggerBuildsResponse::hasSuccesses,
                        TriggerBuildsResponse::hasWarnings,
                        TriggerBuildsResponse::hasFailures)
                .containsExactly(true, false, false);

        verify(siteClient).buildJob(any(SiteClient.BuildJobRequest.class));
        verify(jobService).search(eq("look"), eq(false));
        verify(configurationService).useUserId(null);
        verifyNoMoreInteractions(configurationService);
    }

    @Test
    void testTriggerBuilds_ByLookupExactMatch()
    {
        when(configurationService.useUserId(null)).thenReturn(false);

        Site site = new Site().setName("site")
                .setRpcUrl(URI.create("https://jenkins.example.com"))
                .setJenkinsPluginInstalled(true);
        Job job1 = new Job().setId("job-1")
                .setName("feature/lookup")
                .setSite(site)
                .setLinked(true);
        when(jobService.search(eq("look"), eq(true))).thenReturn(Collections.singletonList(job1));
        when(siteClient.buildJob(any(SiteClient.BuildJobRequest.class))).thenReturn(ACCEPTED);

        TriggerBuildsResponse response = helper.triggerBuilds(new TriggerBuildsRequest().setLookupJob("look")
                .setExactMatch(true));

        assertThat(response).extracting(TriggerBuildsResponse::hasSuccesses,
                        TriggerBuildsResponse::hasWarnings,
                        TriggerBuildsResponse::hasFailures)
                .containsExactly(true, false, false);

        verify(siteClient).buildJob(any(SiteClient.BuildJobRequest.class));
        verify(jobService).search(eq("look"), eq(true));
        verify(configurationService).useUserId(null);
        verifyNoMoreInteractions(configurationService);
    }

    @Test
    void testTriggerBuilds_ByIssueField()
    {
        when(configurationService.useUserId("TEST")).thenReturn(false);

        Site site = new Site().setName("site")
                .setRpcUrl(URI.create("https://jenkins.example.com"))
                .setJenkinsPluginInstalled(true);
        Job job1 = new Job().setId("job-1")
                .setName("backend")
                .setSite(site)
                .setLinked(true);
        IssueReference issueReference = new IssueReference().setIssueKey("TEST-1")
                .addField("components", "Components", "backend");
        when(issueReferenceProvider.getIssueReference(eq("TEST-1"), any(IssueReferenceProvider.FieldContext.class))).thenReturn(of(
                issueReference));
        when(jobService.search(eq("backend"), eq(false))).thenReturn(Collections.singletonList(job1));
        when(siteClient.buildJob(any(SiteClient.BuildJobRequest.class))).thenReturn(ACCEPTED);

        TriggerBuildsResponse response = helper.triggerBuilds(new TriggerBuildsRequest().setIssueKey("TEST-1")
                .setByIssueField("components"));

        assertThat(response).extracting(TriggerBuildsResponse::hasSuccesses,
                        TriggerBuildsResponse::hasWarnings,
                        TriggerBuildsResponse::hasFailures)
                .containsExactly(true, false, false);

        verify(siteClient).buildJob(any(SiteClient.BuildJobRequest.class));
        verify(jobService).search(eq("backend"), eq(false));
        verify(configurationService).useUserId("TEST");
        verify(configurationService).getParameterIssueFields("TEST");
        verify(configurationService).getParameterIssueFieldNames("TEST");
        verify(configurationService).getParameterIssueFieldMappers("TEST");
        verifyNoMoreInteractions(configurationService);
    }

    @Test
    void testTriggerBuilds_ByIssueFieldExactMatch()
    {
        when(configurationService.useUserId("TEST")).thenReturn(false);

        Site site = new Site().setName("site")
                .setRpcUrl(URI.create("https://jenkins.example.com"))
                .setJenkinsPluginInstalled(true);
        Job job1 = new Job().setId("job-1")
                .setName("backend")
                .setSite(site)
                .setLinked(true);
        IssueReference issueReference = new IssueReference().setIssueKey("TEST-1")
                .addField("components", "Components", "backend");
        when(issueReferenceProvider.getIssueReference(eq("TEST-1"), any(IssueReferenceProvider.FieldContext.class))).thenReturn(of(
                issueReference));
        when(jobService.search(eq("backend"), eq(true))).thenReturn(Collections.singletonList(job1));
        when(siteClient.buildJob(any(SiteClient.BuildJobRequest.class))).thenReturn(ACCEPTED);

        TriggerBuildsResponse response = helper.triggerBuilds(new TriggerBuildsRequest().setIssueKey("TEST-1")
                .setByIssueField("components")
                .setExactMatch(true));

        assertThat(response).extracting(TriggerBuildsResponse::hasSuccesses,
                        TriggerBuildsResponse::hasWarnings,
                        TriggerBuildsResponse::hasFailures)
                .containsExactly(true, false, false);

        verify(siteClient).buildJob(any(SiteClient.BuildJobRequest.class));
        verify(jobService).search(eq("backend"), eq(true));
        verify(configurationService).useUserId("TEST");
        verify(configurationService).getParameterIssueFields("TEST");
        verify(configurationService).getParameterIssueFieldNames("TEST");
        verify(configurationService).getParameterIssueFieldMappers("TEST");
        verifyNoMoreInteractions(configurationService);
    }

    @Test
    void testTriggerBuilds_BlankTargets()
    {
        IssueReference issueReference = new IssueReference().setIssueKey("TEST-1");
        when(issueReferenceProvider.getIssueReference(eq("TEST-1"), any(IssueReferenceProvider.FieldContext.class))).thenReturn(of(
                issueReference));

        TriggerBuildsResponse response = helper.triggerBuilds(new TriggerBuildsRequest().setIssueKey("TEST-1"));

        assertThat(response).extracting(TriggerBuildsResponse::hasSuccesses,
                        TriggerBuildsResponse::hasWarnings,
                        TriggerBuildsResponse::hasFailures)
                .containsExactly(false, true, false);
        assertThat(response.getWarnings()).hasSize(1)
                .containsOnly(new TriggerBuildsResponse.TriggerResult().setType(TriggerResultType.WARNING)
                        .setMessage("trigger.build.no.jobs.matched.request"));

        verify(siteClient, never()).buildJob(any());
        verify(configurationService).useUserId("TEST");
        verify(configurationService).getParameterIssueFields("TEST");
        verify(configurationService).getParameterIssueFieldNames("TEST");
        verify(configurationService).getParameterIssueFieldMappers("TEST");
        verifyNoMoreInteractions(configurationService);
    }

    @Test
    void testTriggerBuilds_UserUsingId()
    {
        when(configurationService.useUserId("TEST")).thenReturn(true);

        Site site = new Site().setRpcUrl(URI.create("https://jenkins.example.com"))
                .setJenkinsPluginInstalled(true);
        IssueReference issueReference = new IssueReference().setIssueKey("TEST-1");
        when(issueReferenceProvider.getIssueReference(eq("TEST-1"), any(IssueReferenceProvider.FieldContext.class))).thenReturn(of(
                issueReference));
        when(jobService.get(anyString())).thenAnswer(invocation -> of(new Job().setName(invocation.getArgument(0))
                .setSite(site)
                .setLinked(true)));
        when(siteClient.buildJob(any(SiteClient.BuildJobRequest.class))).then(invocation -> {
            SiteClient.BuildJobRequest request = invocation.getArgument(0);
            return Objects.equals(issueReference, request.getIssue()) && "1234".equals(request.getSubject()) ? BUILD_SCHEDULED : FAILED;
        });

        TriggerBuildsResponse response = helper.triggerBuilds(new TriggerBuildsRequest().setJobs(asSet("job-1", "job-2"))
                .setIssueKey("TEST-1"));

        assertThat(response.getSuccesses()).hasSize(2)
                .containsOnly(new TriggerBuildsResponse.TriggerResult().setType(TriggerResultType.SUCCESS)
                        .setMessage("successfully.scheduled.build")
                        .setLink(URI.create("https://jenkins.example.com/job/job-1/")), new TriggerBuildsResponse.TriggerResult().setType(
                                TriggerResultType.SUCCESS)
                        .setMessage("successfully.scheduled.build")
                        .setLink(URI.create("https://jenkins.example.com/job/job-2/")));

        verify(siteClient, times(2)).buildJob(any(SiteClient.BuildJobRequest.class));
        verify(configurationService).useUserId("TEST");
        verify(configurationService).getParameterIssueFields("TEST");
        verify(configurationService).getParameterIssueFieldNames("TEST");
        verify(configurationService).getParameterIssueFieldMappers("TEST");
        verifyNoMoreInteractions(configurationService);
    }

    @Test
    void testTriggerBuilds_UseReferenceParameters()
    {
        when(configurationService.useUserId("TEST")).thenReturn(true);

        Site site = new Site().setRpcUrl(URI.create("https://jenkins.example.com"))
                .setJenkinsPluginInstalled(true);
        Job job = new Job().setName("job-1")
                .setSite(site)
                .setLinked(true);
        IssueReference issueReference = new IssueReference().setIssueKey("TEST-1")
                .addField("fixVersions", "Fix Versions", "1.0")
                .addField("components", "Components", "backend");
        when(issueReferenceProvider.getIssueReference(eq("TEST-1"), any(IssueReferenceProvider.FieldContext.class))).thenReturn(of(
                issueReference));
        when(jobService.get(anyString())).thenReturn(Optional.of(job));
        when(siteClient.buildJob(any(SiteClient.BuildJobRequest.class))).then(invocation -> {
            SiteClient.BuildJobRequest request = invocation.getArgument(0);
            return Objects.equals(issueReference, request.getIssue()) && "1234".equals(request.getSubject()) ? BUILD_SCHEDULED : FAILED;
        });

        TriggerBuildsResponse response = helper.triggerBuilds(new TriggerBuildsRequest().setJobs(asSet("job-1"))
                .setIssueKey("TEST-1"));

        assertThat(response).extracting(TriggerBuildsResponse::hasSuccesses,
                        TriggerBuildsResponse::hasWarnings,
                        TriggerBuildsResponse::hasFailures)
                .containsExactly(true, false, false);

        verify(siteClient).buildJob(requestCaptor.capture());
        assertThat(requestCaptor.getValue()
                .getParameters()).hasSize(2)
                .containsOnly(entry("Fix_Versions", "1.0"), entry("Components", "backend"));

        verify(configurationService).useUserId("TEST");
        verify(configurationService).getParameterIssueFields("TEST");
        verify(configurationService).getParameterIssueFieldNames("TEST");
        verify(configurationService).getParameterIssueFieldMappers("TEST");
        verifyNoMoreInteractions(configurationService);
    }

    @Test
    void testTriggerBuilds_UseSpecificParameters()
    {
        when(configurationService.useUserId(null)).thenReturn(false);

        Site site = new Site().setName("site")
                .setRpcUrl(URI.create("https://jenkins.example.com"))
                .setJenkinsPluginInstalled(true);
        Job job1 = new Job().setId("job-1")
                .setName("feature/lookup")
                .setSite(site)
                .setLinked(true);
        when(jobService.search(eq("look"), eq(false))).thenReturn(Collections.singletonList(job1));
        when(siteClient.buildJob(any(SiteClient.BuildJobRequest.class))).thenReturn(ACCEPTED);

        TriggerBuildsResponse response = helper.triggerBuilds(new TriggerBuildsRequest().setLookupJob("look")
                .setUseParameters(TriggerBuildsRequest.UseParameters.specified)
                .addParameter("param-1", "value-1")
                .addParameter("param-2", "value-2"));

        assertThat(response).extracting(TriggerBuildsResponse::hasSuccesses,
                        TriggerBuildsResponse::hasWarnings,
                        TriggerBuildsResponse::hasFailures)
                .containsExactly(true, false, false);

        verify(siteClient).buildJob(requestCaptor.capture());
        assertThat(requestCaptor.getValue()
                .getParameters()).hasSize(2)
                .containsOnly(entry("param-1", "value-1"), entry("param-2", "value-2"));

        verify(jobService).search(eq("look"), eq(false));
        verify(configurationService).useUserId(null);
        verifyNoMoreInteractions(configurationService);
    }

    @Test
    void testTriggerBuilds_UseNoParameters()
    {
        when(configurationService.useUserId("TEST")).thenReturn(true);

        Site site = new Site().setRpcUrl(URI.create("https://jenkins.example.com"))
                .setJenkinsPluginInstalled(true);
        Job job = new Job().setName("job-1")
                .setSite(site)
                .setLinked(true);
        IssueReference issueReference = new IssueReference().setIssueKey("TEST-1")
                .addField("fixVersions", "Fix Versions", "1.0")
                .addField("components", "Components", "backend");
        when(issueReferenceProvider.getIssueReference(eq("TEST-1"))).thenReturn(of(issueReference));
        when(jobService.get(anyString())).thenReturn(Optional.of(job));
        when(siteClient.buildJob(any(SiteClient.BuildJobRequest.class))).thenReturn(ACCEPTED);

        TriggerBuildsResponse response = helper.triggerBuilds(new TriggerBuildsRequest().setJobs(asSet("job-1"))
                .setIssueKey("TEST-1")
                .setUseParameters(TriggerBuildsRequest.UseParameters.none));

        assertThat(response).extracting(TriggerBuildsResponse::hasSuccesses,
                        TriggerBuildsResponse::hasWarnings,
                        TriggerBuildsResponse::hasFailures)
                .containsExactly(true, false, false);

        verify(siteClient).buildJob(requestCaptor.capture());
        assertThat(requestCaptor.getValue()
                .getParameters()).isNullOrEmpty();

        verify(configurationService).useUserId("TEST");
        verifyNoMoreInteractions(configurationService);
    }

    @Test
    void testTriggerBuilds_UnknownIssueKey()
    {
        when(issueReferenceProvider.getIssueReference(eq("TEST-1"), any(IssueReferenceProvider.FieldContext.class))).thenReturn(empty());

        TriggerBuildsResponse response = helper.triggerBuilds(new TriggerBuildsRequest().setLinked()
                .setIssueKey("TEST-1"));

        assertThat(response.getWarnings()).hasSize(2)
                .containsOnly(new TriggerBuildsResponse.TriggerResult().setType(TriggerResultType.WARNING)
                                .setMessage("unknown.issue"),
                        new TriggerBuildsResponse.TriggerResult().setType(TriggerResultType.WARNING)
                                .setMessage("trigger.build.no.jobs.matched.request"));

        verify(configurationService).useUserId("TEST");
        verify(configurationService).getParameterIssueFields("TEST");
        verify(configurationService).getParameterIssueFieldNames("TEST");
        verify(configurationService).getParameterIssueFieldMappers("TEST");
        verifyNoMoreInteractions(jobService, siteClient, configurationService);
    }

    @Test
    void testTriggerBuilds_UnknownJob()
    {
        when(configurationService.useUserId("TEST")).thenReturn(false);

        IssueReference issueReference = new IssueReference().setIssueKey("TEST-1");
        when(issueReferenceProvider.getIssueReference(eq("TEST-1"), any(IssueReferenceProvider.FieldContext.class))).thenReturn(of(
                issueReference));
        when(jobService.get(anyString())).thenReturn(empty());

        TriggerBuildsResponse response = helper.triggerBuilds(new TriggerBuildsRequest().setJobs(asSet("job-1"))
                .setIssueKey("TEST-1"));

        assertThat(response.getFailures()).hasSize(1)
                .containsOnly(new TriggerBuildsResponse.TriggerResult().setType(TriggerResultType.ERROR)
                        .setMessage("unknown.job"));

        verifyNoInteractions(siteClient);
        verify(configurationService).useUserId("TEST");
        verify(configurationService).getParameterIssueFields("TEST");
        verify(configurationService).getParameterIssueFieldNames("TEST");
        verify(configurationService).getParameterIssueFieldMappers("TEST");
        verifyNoMoreInteractions(configurationService);
    }

    @Test
    void testTriggerBuilds_DeletedJob()
    {
        when(configurationService.useUserId("TEST")).thenReturn(false);

        Job job = new Job().setLinked(true)
                .setDeleted(true);

        IssueReference issueReference = new IssueReference().setIssueKey("TEST-1");
        when(issueReferenceProvider.getIssueReference(eq("TEST-1"), any(IssueReferenceProvider.FieldContext.class))).thenReturn(of(
                issueReference));
        when(jobService.get(anyString())).thenReturn(of(job));

        TriggerBuildsResponse response = helper.triggerBuilds(new TriggerBuildsRequest().setJobs(asSet("job-1"))
                .setIssueKey("TEST-1"));

        assertThat(response.getWarnings()).hasSize(2)
                .containsOnly(new TriggerBuildsResponse.TriggerResult().setType(TriggerResultType.WARNING)
                                .setMessage("job.not.triggerable")
                                .setLink(job.getDisplayUrl()),
                        new TriggerBuildsResponse.TriggerResult().setType(TriggerResultType.WARNING)
                                .setMessage("trigger.build.no.jobs.matched.request"));

        verifyNoInteractions(siteClient);
        verify(configurationService).useUserId("TEST");
        verify(configurationService).getParameterIssueFields("TEST");
        verify(configurationService).getParameterIssueFieldNames("TEST");
        verify(configurationService).getParameterIssueFieldMappers("TEST");
        verifyNoMoreInteractions(configurationService);
    }

    @Test
    void testTriggerBuilds_UnlinkedJob()
    {
        when(configurationService.useUserId("TEST")).thenReturn(false);

        Job job = new Job();

        IssueReference issueReference = new IssueReference().setIssueKey("TEST-1");
        when(issueReferenceProvider.getIssueReference(eq("TEST-1"), any(IssueReferenceProvider.FieldContext.class))).thenReturn(of(
                issueReference));
        when(jobService.get(anyString())).thenReturn(of(job));

        TriggerBuildsResponse response = helper.triggerBuilds(new TriggerBuildsRequest().setJobs(asSet("job-1"))
                .setIssueKey("TEST-1"));

        assertThat(response.getWarnings()).hasSize(2)
                .containsOnly(new TriggerBuildsResponse.TriggerResult().setType(TriggerResultType.WARNING)
                                .setMessage("job.not.triggerable")
                                .setLink(job.getDisplayUrl()),
                        new TriggerBuildsResponse.TriggerResult().setType(TriggerResultType.WARNING)
                                .setMessage("trigger.build.no.jobs.matched.request"));

        verifyNoInteractions(siteClient);
        verify(configurationService).useUserId("TEST");
        verify(configurationService).getParameterIssueFields("TEST");
        verify(configurationService).getParameterIssueFieldNames("TEST");
        verify(configurationService).getParameterIssueFieldMappers("TEST");
        verifyNoMoreInteractions(configurationService);
    }

    private Set<String> asSet(String... string)
    {
        return Stream.of(string)
                .collect(toSet());
    }

    private static Stream<Arguments> triggerableJob()
    {
        Site site1 = new Site().setJenkinsPluginInstalled(false);
        Site site2 = new Site().setJenkinsPluginInstalled(true);
        Site site3 = new Site().setInaccessibleSite()
                .setJenkinsPluginInstalled(true);

        return Stream.of(Arguments.of("job-1",
                        new Job().setId("job-1")
                                .setName("Job 1")
                                .setSite(site1)
                                .setLinked(true),
                        false),
                Arguments.of("job-2",
                        new Job().setId("job-2")
                                .setName("Job 2")
                                .setSite(site2)
                                .setLinked(true),
                        true),
                Arguments.of("job-3",
                        new Job().setId("job-3")
                                .setName("Job 3")
                                .setSite(site2)
                                .setDeleted(true),
                        false),
                Arguments.of("job-4",
                        new Job().setId("job-4")
                                .setName("Job 4")
                                .setSite(site2),
                        false),
                Arguments.of("job-5",
                        new Job().setId("job-5")
                                .setName("Job 5")
                                .setSite(site3)
                                .setLinked(true),
                        false),
                Arguments.of("job-6", null, false));
    }
}
