/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services;

import java.time.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.*;

import org.marvelution.jji.data.services.api.*;
import org.marvelution.jji.events.*;
import org.marvelution.jji.model.*;

import jakarta.inject.*;
import org.assertj.core.api.*;
import org.junit.jupiter.api.*;

import static java.util.stream.Collectors.*;
import static org.assertj.core.api.Assertions.*;
import static org.awaitility.Awaitility.*;
import static org.marvelution.jji.model.Result.*;

public abstract class AbstractBaseIssueLinkServiceTest<S extends BaseIssueLinkService>
        extends AbstractBaseServiceTest
{

    @Inject
    @javax.inject.Inject
    protected S issueLinkService;
    protected Job job;

    @BeforeEach
    void createDefaultJob()
    {
        job = setupJob(createJob(createSite()));
    }

    protected abstract void setupIssueReference(String... issueKeys);

    protected abstract Job setupJob(Job job);

    protected abstract Build setupBuild(
            Build build,
            IssueReference... issueLinks);

    @Test
    void testGetBuildCountForIssue()
    {
        setupBuild(createBuild(job),
                new IssueReference().setIssueKey("PR-1")
                        .setProjectKey("PR"),
                new IssueReference().setIssueKey("PR-2")
                        .setProjectKey("PR"),
                new IssueReference().setIssueKey("OP-1")
                        .setProjectKey("OP"));

        assertThat(issueLinkService.getBuildCountForIssue("PR-1")).isEqualTo(1);
    }

    @Test
    void testGetBuildCountForProject()
    {
        setupBuild(createBuild(job),
                new IssueReference().setIssueKey("PR-1")
                        .setProjectKey("PR"),
                new IssueReference().setIssueKey("PR-2")
                        .setProjectKey("PR"),
                new IssueReference().setIssueKey("OP-1")
                        .setProjectKey("OP"));

        assertThat(issueLinkService.getBuildCountForProject("PR")).isEqualTo(2);
    }

    @Test
    void testGetBuildsForIssue()
    {
        Build build1 = setupBuild(createBuild(job).setTimestamp(System.currentTimeMillis() - 86400),
                new IssueReference().setIssueKey("PR-1")
                        .setProjectKey("PR"),
                new IssueReference().setIssueKey("PR-2")
                        .setProjectKey("PR"),
                new IssueReference().setIssueKey("OP-1")
                        .setProjectKey("OP"));
        Build build2 = setupBuild(createBuild(job, 2).setTimestamp(System.currentTimeMillis()),
                new IssueReference().setIssueKey("OP-1")
                        .setProjectKey("OP"));

        assertThat(issueLinkService.getBuildsForIssue("PR-1", 1, true)).containsOnly(build1)
                .extracting(Build::getIssueReferences)
                .first()
                .asInstanceOf(InstanceOfAssertFactories.iterable(IssueReference.class))
                .hasSize(3)
                .containsOnly(new IssueReference().setIssueKey("PR-1")
                                .setProjectKey("PR"),
                        new IssueReference().setIssueKey("PR-2")
                                .setProjectKey("PR"),
                        new IssueReference().setIssueKey("OP-1")
                                .setProjectKey("OP"));
        assertThat(issueLinkService.getBuildsForIssue("OP-1", 2, true)).containsSequence(build2, build1);
        assertThat(issueLinkService.getBuildsForIssue("OP-1", 1, true)).containsOnly(build2)
                .extracting(Build::getIssueReferences)
                .first()
                .asInstanceOf(InstanceOfAssertFactories.iterable(IssueReference.class))
                .hasSize(1)
                .containsOnly(new IssueReference().setIssueKey("OP-1")
                        .setProjectKey("OP"));
    }

    @Test
    void testGetBuildsForLinkRequest()
    {
        Build build1 = setupBuild(createBuild(job).setTimestamp(System.currentTimeMillis() - 86400),
                new IssueReference().setIssueKey("PR-1")
                        .setProjectKey("PR"),
                new IssueReference().setIssueKey("PR-2")
                        .setProjectKey("PR"),
                new IssueReference().setIssueKey("OP-2")
                        .setProjectKey("OP"));
        Build build2 = setupBuild(createBuild(job, 2).setTimestamp(System.currentTimeMillis() - 3600),
                new IssueReference().setIssueKey("OP-1")
                        .setProjectKey("OP"));
        Build build3 = setupBuild(createBuild(job, 3).setTimestamp(System.currentTimeMillis()),
                new IssueReference().setIssueKey("TE-1")
                        .setProjectKey("TE"));

        IssueLinkService.LinkRequest linkRequest = new IssueLinkService.LinkRequest();

        assertThat(issueLinkService.getBuildsForLinkRequest(linkRequest, 10, true)).containsSequence(build3, build2, build1);

        linkRequest.setInProjectKeys("OP");

        assertThat(issueLinkService.getBuildsForLinkRequest(linkRequest, 10, true)).containsSequence(build2, build1);

        linkRequest.setNotInIssueKeys("OP-1");

        assertThat(issueLinkService.getBuildsForLinkRequest(linkRequest, 10, true)).containsOnly(build1);
    }

    @Test
    void testGetJobsForIssue()
    {
        setupBuild(createBuild(job, 1).setTimestamp(System.currentTimeMillis() - 86400),
                new IssueReference().setIssueKey("PR-1")
                        .setProjectKey("PR"),
                new IssueReference().setIssueKey("PR-2")
                        .setProjectKey("PR"),
                new IssueReference().setIssueKey("OP-1")
                        .setProjectKey("OP"));
        setupBuild(createBuild(job, 2).setTimestamp(System.currentTimeMillis() - 80000),
                new IssueReference().setIssueKey("PR-1")
                        .setProjectKey("PR"),
                new IssueReference().setIssueKey("PR-2")
                        .setProjectKey("PR"),
                new IssueReference().setIssueKey("OP-1")
                        .setProjectKey("OP"));
        Job job1 = setupJob(createJob(this.job.getSite()));
        setupBuild(createBuild(job1, 1).setTimestamp(System.currentTimeMillis()),
                new IssueReference().setIssueKey("OP-1")
                        .setProjectKey("OP"),
                new IssueReference().setIssueKey("OP-2")
                        .setProjectKey("OP"));
        setupBuild(createBuild(job1, 2).setTimestamp(System.currentTimeMillis() - 6000),
                new IssueReference().setIssueKey("OP-1")
                        .setProjectKey("OP"),
                new IssueReference().setIssueKey("OP-2")
                        .setProjectKey("OP"));

        assertThat(issueLinkService.getJobsForIssue(true, "PR-1")).containsOnly(job)
                .extracting(Job::getIssueReferences)
                .first()
                .asInstanceOf(InstanceOfAssertFactories.iterable(IssueReference.class))
                .hasSize(3)
                .containsOnly(new IssueReference().setIssueKey("PR-1")
                                .setProjectKey("PR"),
                        new IssueReference().setIssueKey("PR-2")
                                .setProjectKey("PR"),
                        new IssueReference().setIssueKey("OP-1")
                                .setProjectKey("OP"));
        assertThat(issueLinkService.getJobsForIssue(true, "OP-1")).containsOnly(job1, job);
    }

    @Test
    void testGetRelatedIssueKeys()
    {
        Build build = setupBuild(createBuild(job),
                new IssueReference().setIssueKey("PR-1")
                        .setProjectKey("PR"),
                new IssueReference().setIssueKey("PR-2")
                        .setProjectKey("PR"),
                new IssueReference().setIssueKey("OP-1")
                        .setProjectKey("OP"));

        Set<String> issueKeys = issueLinkService.getRelatedIssueKeys(build);

        assertThat(issueKeys).hasSize(3)
                .containsOnly("PR-1", "PR-2", "OP-1");
    }

    @Test
    void testGetIssueLinks()
    {
        Build build = setupBuild(createBuild(job),
                new IssueReference().setIssueKey("PR-1")
                        .setProjectKey("PR"),
                new IssueReference().setIssueKey("PR-2")
                        .setProjectKey("PR"),
                new IssueReference().setIssueKey("OP-1")
                        .setProjectKey("OP"));

        Set<IssueReference> links = issueLinkService.getIssueLinks(build);

        assertThat(links).hasSize(3)
                .containsOnly(new IssueReference().setIssueKey("PR-1")
                                .setProjectKey("PR"),
                        new IssueReference().setIssueKey("PR-2")
                                .setProjectKey("PR"),
                        new IssueReference().setIssueKey("OP-1")
                                .setProjectKey("OP"));
    }

    @Test
    void testGetLinkStatistics()
    {
        Duration timeout = Duration.ofSeconds(10);
        Build build1 = setupBuild(createBuild(job).setResult(SUCCESS));
        setupIssueReference("PR-1");

        LinkStatistics statistics = issueLinkService.getLinkStatistics("PR-1");
        assertThat(statistics).extracting(LinkStatistics::getTotal,
                        LinkStatistics::getBuildTotal,
                        LinkStatistics::getWorstResult,
                        LinkStatistics::getLatestBuild,
                        LinkStatistics::getWorstBuild)
                .containsOnly(0L, SUCCESS, null, null);

        issueLinkService.link(build1, "PR-1");

        await().pollInSameThread()
                .atMost(timeout)
                .untilAsserted(() -> assertThat(issueLinkService.getLinkStatistics("PR-1")).extracting(LinkStatistics::getTotal,
                                LinkStatistics::getWorstResult)
                        .containsOnly(1L, SUCCESS));
        statistics = issueLinkService.getLinkStatistics("PR-1");
        assertThat(statistics).extracting(LinkStatistics::getTotal,
                        LinkStatistics::getBuildTotal,
                        LinkStatistics::getWorstResult,
                        LinkStatistics::getLatestBuild,
                        LinkStatistics::getWorstBuild)
                .containsOnly(1L, 1L, SUCCESS, LinkStatistics.BuildStats.forBuild(build1), LinkStatistics.BuildStats.forBuild(build1));
        assertThat(statistics.getCountByResult()).containsOnly(entry(SUCCESS, 1L),
                entry(UNSTABLE, 0L),
                entry(FAILURE, 0L),
                entry(NOT_BUILT, 0L),
                entry(ABORTED, 0L),
                entry(UNKNOWN, 0L));
    }

    @Test
    void testLink()
    {
        Build build = setupBuild(createBuild(job));
        setupIssueReference("PR-1", "PR-2");

        issueLinkService.link(build, "OP-1");

        assertThat(issueLinkService.getIssueLinks(build)).isEmpty();

        issueLinkService.link(build, "PR-1");

        assertEventPublished(new IssueLinksUpdatedEvent(build.getId(), "PR-1"));
        assertThat(issueLinkService.getIssueLinks(build)).hasSize(1);
        Set<String> issueKeysByBuild = issueLinkService.getRelatedIssueKeys(build);
        assertThat(issueKeysByBuild).containsOnly("PR-1");
        assertThat(issueLinkService.getIssueLinks(build.getJob())).hasSize(1);
        Set<String> issueKeysByJob = issueLinkService.getRelatedIssueKeys(build.getJob());
        assertThat(issueKeysByJob).containsOnly("PR-1");

        // Adding the same link again overrides the existing link
        issueLinkService.link(build, "PR-1");

        assertEventPublished(new IssueLinksUpdatedEvent(build.getId(), "PR-1"));
        assertThat(issueLinkService.getIssueLinks(build)).hasSize(1);
        issueKeysByBuild = issueLinkService.getRelatedIssueKeys(build);
        assertThat(issueKeysByBuild).containsOnly("PR-1");
        assertThat(issueLinkService.getIssueLinks(build.getJob())).hasSize(1);
        issueKeysByJob = issueLinkService.getRelatedIssueKeys(build.getJob());
        assertThat(issueKeysByJob).containsOnly("PR-1");

        // Adding a new link adds it
        issueLinkService.link(build, "PR-2");

        assertEventPublished(new IssueLinksUpdatedEvent(build.getId(), "PR-2"));
        assertThat(issueLinkService.getIssueLinks(build)).hasSize(2);
        issueKeysByBuild = issueLinkService.getRelatedIssueKeys(build);
        assertThat(issueKeysByBuild).containsOnly("PR-1", "PR-2");
        assertThat(issueLinkService.getIssueLinks(build.getJob())).hasSize(2);
        issueKeysByJob = issueLinkService.getRelatedIssueKeys(build.getJob());
        assertThat(issueKeysByJob).containsOnly("PR-1", "PR-2");
    }

    @Test
    void testRelink()
    {
        Build build = setupBuild(createBuild(job));
        setupIssueReference("PR-1", "PR-2", "OP-1");

        issueLinkService.relink(build,
                Stream.of("PR-1", "PR-2")
                        .collect(toSet()));

        assertEventPublished(new IssueLinksUpdatedEvent(build.getId(), "PR-1", "PR-2"));
        assertThat(issueLinkService.getIssueLinks(build)).hasSize(2);
        assertThat(issueLinkService.getRelatedIssueKeys(build)).containsOnly("PR-1", "PR-2");
        assertThat(issueLinkService.getIssueLinks(build.getJob())).hasSize(2);
        assertThat(issueLinkService.getRelatedIssueKeys(build.getJob())).containsOnly("PR-1", "PR-2");

        issueLinkService.relink(build,
                Stream.of("OP-1")
                        .collect(toSet()));

        assertEventPublished(new IssueLinksUpdatedEvent(build.getId(), "OP-1"));
        assertEventPublished(new IssueLinksDeletedEvent(null, "PR-1", "PR-2"));
        await().atMost(1, TimeUnit.MINUTES)
                .pollInSameThread()
                .untilAsserted(() -> assertThat(issueLinkService.getIssueLinks(build)).hasSize(1));
        assertThat(issueLinkService.getRelatedIssueKeys(build)).containsOnly("OP-1");
        assertThat(issueLinkService.getIssueLinks(build.getJob())).hasSize(1);
        assertThat(issueLinkService.getRelatedIssueKeys(build.getJob())).containsOnly("OP-1");

        issueLinkService.relink(build, new HashSet<>());
        assertEventPublished(new IssueLinksDeletedEvent(null, "OP-1"));

        assertThat(issueLinkService.getIssueLinks(build)).isEmpty();
    }

    @Test
    void testUnlinkForIssue()
    {
        Build build = setupBuild(createBuild(job),
                new IssueReference().setIssueKey("PR-1")
                        .setProjectKey("PR"),
                new IssueReference().setIssueKey("PR-2")
                        .setProjectKey("PR"),
                new IssueReference().setIssueKey("PR-3")
                        .setProjectKey("PR"));

        assertThat(issueLinkService.getRelatedIssueKeys(build)).hasSize(3)
                .containsOnly("PR-1", "PR-2", "PR-3");

        issueLinkService.unlinkForIssue("PR-1");

        assertEventPublished(new IssueLinksDeletedEvent(null, "PR-1"));
        await().atMost(1, TimeUnit.MINUTES)
                .pollInSameThread()
                .untilAsserted(() -> assertThat(issueLinkService.getRelatedIssueKeys(build)).hasSize(2)
                        .containsOnly("PR-2", "PR-3"));
    }

    @Test
    void testUnlinkForProject()
    {
        Build build = setupBuild(createBuild(job),
                new IssueReference().setIssueKey("PR-1")
                        .setProjectKey("PR"),
                new IssueReference().setIssueKey("PR-2")
                        .setProjectKey("PR"),
                new IssueReference().setIssueKey("OP-1")
                        .setProjectKey("OP"));

        assertThat(issueLinkService.getRelatedIssueKeys(build)).containsOnly("PR-1", "PR-2", "OP-1");

        issueLinkService.unlinkForProject("PR");

        assertEventPublished(new IssueLinksDeletedEvent("PR"));
        await().atMost(1, TimeUnit.MINUTES)
                .pollInSameThread()
                .untilAsserted(() -> assertThat(issueLinkService.getRelatedIssueKeys(build)).containsOnly("OP-1"));
    }
}
