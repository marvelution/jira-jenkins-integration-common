/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services;

import java.net.*;
import java.util.*;

import org.marvelution.jji.data.services.api.*;
import org.marvelution.jji.data.services.api.exceptions.*;
import org.marvelution.jji.events.*;
import org.marvelution.jji.model.*;
import org.marvelution.jji.validation.api.*;

import jakarta.inject.*;
import org.assertj.core.api.*;
import org.junit.jupiter.api.*;

import static java.util.Optional.*;
import static org.assertj.core.api.Assertions.*;
import static org.marvelution.jji.model.Matchers.*;
import static org.mockito.Mockito.*;

/**
 * Common tests for {@link BaseSiteService} implementations.
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public abstract class AbstractBaseSiteServiceTest<S extends BaseSiteService>
        extends AbstractBaseServiceTest
{

    protected S siteService;

    @Inject
    @javax.inject.Inject
    void setSiteService(S siteService)
    {
        this.siteService = spy(siteService);
    }

    protected abstract Site setupSite(
            Site site,
            Job... jobs)
            throws Exception;

    protected abstract void setupSiteClient(
            Site site,
            Status status,
            boolean pluginInstalled)
            throws Exception;

    @Test
    void testGet()
            throws Exception
    {
        Site site = setupSite(createSite());

        assertThat(siteService.get(site.getId())).isPresent()
                .get()
                .isEqualTo(site)
                .extracting(Site::getJobs)
                .asInstanceOf(InstanceOfAssertFactories.LIST)
                .isEmpty();
    }

    @Test
    void testGet_IncludeJobs()
            throws Exception
    {
        Site site = setupSite(createSite(),
                new Job().setName("Job 1"),
                new Job().setName("Job 2"),
                new Job().setName("Job 3")
                        .setDeleted(true));

        Optional<Site> actual = siteService.get(site.getId(), true);

        assertThat(actual).isPresent()
                .get()
                .isEqualTo(site)
                .extracting(Site::getJobs)
                .asInstanceOf(InstanceOfAssertFactories.list(Job.class))
                .hasSize(3)
                .extracting(Job::getName, Job::isDeleted)
                .containsOnly(tuple("Job 1", false), tuple("Job 2", false), tuple("Job 3", true));
    }

    @Test
    void testGet_IncludeJobs_JobSearchTerm()
            throws Exception
    {
        Site site = setupSite(createSite(),
                new Job().setName("My Job 1"),
                new Job().setName("Job 2"),
                new Job().setName("Job 3")
                        .setDeleted(true));

        Optional<Site> actual = siteService.get(site.getId(), true, "My");

        assertThat(actual).isPresent()
                .get()
                .isEqualTo(site)
                .extracting(Site::getJobs)
                .asInstanceOf(InstanceOfAssertFactories.list(Job.class))
                .hasSize(1)
                .extracting(Job::getName)
                .containsOnly("My Job 1");
    }

    @Test
    void testGet_UnknownSite()
    {
        Optional<Site> site = siteService.get("unknown-id");

        assertThat(site).isEmpty();
    }

    @Test
    void testGetExisting_UnknownSite()
    {
        assertThatThrownBy(() -> siteService.getExisting("unknown-id")).isInstanceOf(UnknownSiteException.class)
                .asInstanceOf(InstanceOfAssertFactories.type(UnknownSiteException.class))
                .extracting(UnknownSiteException::getSiteId)
                .isEqualTo("unknown-id");
    }

    @Test
    void testGetAll()
            throws Exception
    {
        Site site = setupSite(createSite());

        List<Site> actual = siteService.getAll();
        assertThat(actual).hasSize(1)
                .have(equalTo(site));
        assertThat(actual.get(0)
                .getJobs()).isEmpty();
    }

    @Test
    void testGetAll_IncludeJobs()
            throws Exception
    {
        Site site = setupSite(createSite(), new Job().setName("Job 1"), new Job().setName("Job 2"));

        List<Site> actual = siteService.getAll(true);
        assertThat(actual).hasSize(1)
                .have(equalTo(site));
        assertThat(actual.get(0)
                .getJobs()).hasSize(2)
                .extracting(Job::getName)
                .containsOnly("Job 1", "Job 2");
    }

    @Test
    void testAdd_WithIdSet()
    {
        assertThatThrownBy(() -> siteService.add(new Site().setId("site-id"))).isInstanceOf(IllegalArgumentException.class)
                .hasMessage("site.id.present.on.add");
    }

    @Test
    void testUpdate_IdNull()
    {
        assertThatThrownBy(() -> siteService.update(null, new Site().setId("site-id"))).isInstanceOf(NullPointerException.class)
                .hasMessage("site.id.required");
    }

    @Test
    void testUpdate_IdMismatch()
    {
        assertThatThrownBy(() -> siteService.update("id-site", new Site().setId("site-id"))).isInstanceOf(IllegalArgumentException.class)
                .hasMessage("site.id.mismatch");
    }

    @Test
    void testUpdate_NullUserResetsToken()
            throws Exception
    {
        Site site = setupSite(createSite().setUser("admin")
                .setToken("admin"));
        assertThat(site).extracting(Site::getUser, Site::getToken)
                .containsExactly("admin", "admin");

        setupSiteClient(site, Status.ONLINE, true);

        Site updated = siteService.update(site.setUser(null));
        assertThat(updated).extracting(Site::getUser, Site::getToken)
                .containsExactly(null, null);
    }

    @Test
    void testUpdate_TokenToBasicAuth()
            throws Exception
    {
        Site site = setupSite(createSite());
        assertThat(site).extracting(Site::getUser, Site::getToken)
                .containsExactly(null, null);

        setupSiteClient(site, Status.ONLINE, true);

        Site updated = siteService.update(site.setUser("admin")
                .setToken("admin"));
        assertThat(updated).extracting(Site::getUser, Site::getToken)
                .containsExactly("admin", "admin");
    }

    @Test
    void testUpdate_BasicToTokenAuth()
            throws Exception
    {
        Site site = setupSite(createSite().setUser("admin")
                .setToken("admin"));
        assertThat(site).extracting(Site::getUser, Site::getToken)
                .containsExactly("admin", "admin");

        setupSiteClient(site, Status.ONLINE, true);

        Site updated = siteService.update(site.setUser(null)
                .setToken(null));
        assertThat(updated).extracting(Site::getUser, Site::getToken)
                .containsExactly(null, null);
    }

    @Test
    void testUpdate_BasicAuth_KeepToken()
            throws Exception
    {
        Site site = setupSite(createSite().setUser("admin")
                .setToken("admin"));
        assertThat(site).extracting(Site::getUser, Site::getToken)
                .containsExactly("admin", "admin");

        setupSiteClient(site, Status.ONLINE, true);

        Site updated = siteService.update(site.setToken("admin"));
        assertThat(updated).extracting(Site::getUser, Site::getToken)
                .containsExactly("admin", "admin");
    }

    @Test
    void testUpdate_BasicAuth_UpdateToken()
            throws Exception
    {
        Site site = setupSite(createSite().setUser("admin")
                .setToken("admin"));
        assertThat(site).extracting(Site::getUser, Site::getToken)
                .containsExactly("admin", "admin");

        setupSiteClient(site, Status.ONLINE, true);

        Site updated = siteService.update(site.setToken("admin")
                .setNewToken("admin123"));
        assertThat(updated).extracting(Site::getUser, Site::getToken)
                .containsExactly("admin", "admin123");
    }

    @Test
    void testUpdate_TokenMissing()
            throws Exception
    {
        Site site = setupSite(createSite().setUser("admin")
                .setToken("admin"));
        assertThat(site).extracting(Site::getUser, Site::getToken)
                .containsExactly("admin", "admin");

        setupSiteClient(site, Status.ONLINE, true);

        assertThatThrownBy(() -> siteService.update(site.setToken(null))).isInstanceOf(ValidationException.class)
                .hasMessage("Validation failed with error: 'token' site.update.missing.token");
    }

    @Test
    void testUpdate_TokenMismatch()
            throws Exception
    {
        Site site = setupSite(createSite().setUser("admin")
                .setToken("admin"));
        assertThat(site).extracting(Site::getUser, Site::getToken)
                .containsExactly("admin", "admin");

        setupSiteClient(site, Status.ONLINE, true);

        assertThatThrownBy(() -> siteService.update(site.setNewToken("admin123")
                .setToken("admin123"))).isInstanceOf(ValidationException.class)
                .hasMessage("Validation failed with error: 'token' site.update.token.mismatch");
    }

    @Test
    void testUpdate_NewScope()
            throws Exception
    {
        Site site = setupSite(createSite());
        assertThat(site).extracting(Site::getScope)
                .isEqualTo(ConfigurationService.GLOBAL_SCOPE);

        setupSiteClient(site, Status.ONLINE, true);

        assertThat(siteService.update(site.setScope("TEST"))).extracting(Site::getScope)
                .isEqualTo("TEST");

        assertThat(siteService.update(site.setScope(null))).extracting(Site::getScope)
                .isEqualTo(ConfigurationService.GLOBAL_SCOPE);
    }

    @Test
    void testAdd_BlankSite()
    {
        assertThatThrownBy(() -> siteService.add(new Site())).isInstanceOf(ValidationException.class)
                .asInstanceOf(InstanceOfAssertFactories.type(ValidationException.class))
                .extracting(ValidationException::getMessages)
                .extracting(ErrorMessages::getErrors)
                .asInstanceOf(InstanceOfAssertFactories.list(ErrorMessages.ErrorMessage.class))
                .hasSize(3)
                .extracting(ErrorMessages.ErrorMessage::getField)
                .containsOnly("name", "type", "rpcUrl");
    }

    @Test
    void testAdd_UrlsInvalid()
    {
        assertThatThrownBy(() -> siteService.add(new Site().setType(SiteType.JENKINS)
                .setName("Added CI")
                .setRpcUrl(URI.create("localhost"))
                .setDisplayUrl(URI.create("jenkins.example.com"))
                .setUser("bob")
                .setToken("bob"))).isInstanceOf(ValidationException.class)
                .asInstanceOf(InstanceOfAssertFactories.type(ValidationException.class))
                .extracting(ValidationException::getMessages)
                .extracting(ErrorMessages::getErrors)
                .asInstanceOf(InstanceOfAssertFactories.list(ErrorMessages.ErrorMessage.class))
                .hasSize(2)
                .extracting(ErrorMessages.ErrorMessage::getField)
                .containsOnly("rpcUrl", "displayUrl");
    }

    @Test
    void testCRUD()
            throws Exception
    {
        Site site = new Site().setType(SiteType.JENKINS)
                .setName("Added CI")
                .setRpcUrl(createRpcUrl(empty()))
                .setDisplayUrl(URI.create("http://ci.example.com"));
        setupSiteClient(site, Status.ONLINE, true);

        // Create
        Site inserted = siteService.add(site);
        assertThat(inserted).is(equalTo(site, true))
                .extracting(Site::isJenkinsPluginInstalled, Site::isRegistrationComplete)
                .containsExactly(true, false);
        assertThat(inserted.getSharedSecret()).isNotNull();
        // Read
        assertThat(siteService.get(inserted.getId())).isPresent()
                .get()
                .is(equalTo(inserted));
        // Update
        site = inserted.copy()
                .setName("Updated CI")
                .setSharedSecret("Some Different Secret");
        Site updated = siteService.update(site.getId(), site);
        assertThat(siteService.get(site.getId())).isPresent()
                .get()
                .is(equalTo(updated));
        // Verify that the sharedSecret is not updated
        assertThat(updated.getSharedSecret()).isEqualTo(inserted.getSharedSecret());
        // Delete
        siteService.delete(updated.getId());
        assertThat(siteService.get(updated.getId())).isEmpty();
        assertEventPublished(new SiteDeletedEvent(updated));
    }

    @Test
    void testSave_DuplicateName()
            throws Exception
    {
        setupSite(createSite().setName("CI"));
        assertThatThrownBy(() -> siteService.save(createSite().setName("CI")
                .setRpcUrl(URI.create("http://localhost")))).isInstanceOf(ValidationException.class)
                .asInstanceOf(InstanceOfAssertFactories.type(ValidationException.class))
                .extracting(ValidationException::getMessages)
                .asInstanceOf(InstanceOfAssertFactories.type(ErrorMessages.class))
                .extracting(ErrorMessages::getErrors)
                .asInstanceOf(InstanceOfAssertFactories.list(ErrorMessages.ErrorMessage.class))
                .extracting(ErrorMessages.ErrorMessage::getField)
                .containsOnly("name");
    }

    @Test
    void testSave_DuplicateRpcUrl()
            throws Exception
    {
        setupSite(createSite().setRpcUrl(URI.create("http://jenkins.example.com")));
        assertThatThrownBy(() -> siteService.save(createSite().setType(SiteType.HUDSON)
                .setName("Hudson")
                .setRpcUrl(URI.create("http://jenkins.example.com")))).isInstanceOf(ValidationException.class)
                .asInstanceOf(InstanceOfAssertFactories.type(ValidationException.class))
                .extracting(ValidationException::getMessages)
                .asInstanceOf(InstanceOfAssertFactories.type(ErrorMessages.class))
                .extracting(ErrorMessages::getErrors)
                .asInstanceOf(InstanceOfAssertFactories.list(ErrorMessages.ErrorMessage.class))
                .extracting(ErrorMessages.ErrorMessage::getField)
                .containsOnly("rpcUrl");
    }

    @Test
    void testSave_MissingUser()
    {
        assertThatThrownBy(() -> siteService.save(createSite().setToken("admin"))).isInstanceOf(ValidationException.class)
                .asInstanceOf(InstanceOfAssertFactories.type(ValidationException.class))
                .extracting(ValidationException::getMessages)
                .asInstanceOf(InstanceOfAssertFactories.type(ErrorMessages.class))
                .extracting(ErrorMessages::getErrors)
                .asInstanceOf(InstanceOfAssertFactories.list(ErrorMessages.ErrorMessage.class))
                .extracting(ErrorMessages.ErrorMessage::getField)
                .containsOnly("user");
    }

    @Test
    void testSave_MissingToken()
    {
        assertThatThrownBy(() -> siteService.save(createSite().setUser("admin"))).isInstanceOf(ValidationException.class)
                .asInstanceOf(InstanceOfAssertFactories.type(ValidationException.class))
                .extracting(ValidationException::getMessages)
                .asInstanceOf(InstanceOfAssertFactories.type(ErrorMessages.class))
                .extracting(ErrorMessages::getErrors)
                .asInstanceOf(InstanceOfAssertFactories.list(ErrorMessages.ErrorMessage.class))
                .extracting(ErrorMessages.ErrorMessage::getField)
                .containsOnly("token");
    }

    @Test
    void testEnableSite()
            throws Exception
    {
        Site site = setupSite(createSite());

        assertThat(siteService.getExisting(site.getId())
                .isEnabled()).isTrue();

        siteService.enableSite(site.getId(), false);

        assertThat(siteService.getExisting(site.getId())
                .isEnabled()).isFalse();

        siteService.enableSite(site.getId(), true);

        assertThat(siteService.getExisting(site.getId())
                .isEnabled()).isTrue();
    }

    @Test
    void testAutomaticallyEnableNewJobs()
            throws Exception
    {
        Site site = setupSite(createSite());

        assertThat(siteService.getExisting(site.getId())
                .isAutoLinkNewJobs()).isFalse();

        siteService.automaticallyEnableNewJobs(site.getId(), true);

        assertThat(siteService.getExisting(site.getId())
                .isAutoLinkNewJobs()).isTrue();

        siteService.automaticallyEnableNewJobs(site.getId(), false);

        assertThat(siteService.getExisting(site.getId())
                .isAutoLinkNewJobs()).isFalse();

        siteService.automaticallyEnableNewJobs(site.getId(), true);

        assertThat(siteService.getExisting(site.getId())
                .isAutoLinkNewJobs()).isTrue();

        siteService.automaticallyEnableNewJobs(site.getId(), false);

        assertThat(siteService.getExisting(site.getId())
                .isAutoLinkNewJobs()).isFalse();
    }

    @Test
    void testDeleteDeletedJobs()
            throws Exception
    {
        Job jobA3 = new Job().setName("Job A3");
        Site siteA = setupSite(createSite(of("A")),
                new Job().setName("Job A1")
                        .setDeleted(true),
                new Job().setName("Job A2")
                        .setDeleted(true),
                jobA3);
        Job jobB3 = new Job().setName("Job B3");
        Site siteB = setupSite(createSite(of("B")),
                new Job().setName("Job B1")
                        .setDeleted(true),
                new Job().setName("Job B2")
                        .setDeleted(true),
                jobB3);

        siteService.deleteDeletedJobs(siteA);
        Site actual = siteService.getExisting(siteA.getId(), true);
        assertThat(actual).is(equalTo(siteA));
        assertThat(actual.getJobs()).hasSize(1)
                .extracting(Job::getName)
                .containsOnly("Job A3");

        actual = siteService.getExisting(siteB.getId(), true);
        assertThat(actual).is(equalTo(siteB));
        assertThat(actual.getJobs()).hasSize(3)
                .extracting(Job::getName)
                .containsOnly("Job B1", "Job B2", "Job B3");

        siteService.deleteDeletedJobs(siteB.getId());
        actual = siteService.getExisting(siteB.getId(), true);
        assertThat(actual).is(equalTo(siteB));
        assertThat(actual.getJobs()).hasSize(1)
                .extracting(Job::getName)
                .containsOnly("Job B3");

        siteA.getJobs()
                .stream()
                .filter(Job::isDeleted)
                .forEach(job -> assertEventPublished(new JobDeletedEvent(job)));
        siteB.getJobs()
                .stream()
                .filter(Job::isDeleted)
                .forEach(job -> assertEventPublished(new JobDeletedEvent(job)));
    }

    @Test
    void testGetSiteStatus()
            throws Exception
    {
        Site site = setupSite(createSite());

        setupSiteClient(site, Status.OFFLINE, true);
        SiteStatus status = siteService.getSiteStatus(site);
        assertThat(status).extracting(SiteStatus::getStatus, SiteStatus::getPluginVersion)
                .containsExactly(Status.OFFLINE, null);

        setupSiteClient(site, Status.NOT_ACCESSIBLE, true);
        status = siteService.getSiteStatus(site);
        assertThat(status).extracting(SiteStatus::getStatus, SiteStatus::getPluginVersion)
                .containsExactly(Status.NOT_ACCESSIBLE, null);

        setupSiteClient(site, Status.ONLINE, false);
        status = siteService.getSiteStatus(site.getId());
        assertThat(status).extracting(SiteStatus::getStatus, SiteStatus::getPluginVersion)
                .containsExactly(Status.ONLINE, null);

        setupSiteClient(site, Status.ONLINE, true);
        status = siteService.getSiteStatus(site.getId());
        assertThat(status).extracting(SiteStatus::getStatus, SiteStatus::getPluginVersion)
                .containsExactly(Status.ONLINE, Version.of("5.2.0"));
    }
}
