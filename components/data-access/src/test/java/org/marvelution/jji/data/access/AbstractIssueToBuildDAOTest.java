/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access;

import java.net.*;
import java.util.*;
import java.util.concurrent.atomic.*;

import org.marvelution.jji.model.*;

import com.google.common.collect.*;
import org.junit.jupiter.api.*;

import static org.assertj.core.api.Assertions.*;

/**
 * @author Mark Rekveld
 * @since 1.0.0
 */
public abstract class AbstractIssueToBuildDAOTest
		extends AbstractDAOTest
{

	private final AtomicInteger number = new AtomicInteger(1);
	protected Job job;

	@BeforeEach
	void setUp()
	{
		Site site = siteDAO.save(new Site().setName(getTestMethodName())
				                         .setSharedSecret("my-super-secret")
				                         .setType(SiteType.JENKINS)
				                         .setRpcUrl(URI.create("http://localhost:8080")));
		job = jobDAO.save(new Job().setSite(site).setName(getTestMethodName()).setLastBuild(1));
	}

	@Test
	void testGetAllIssueKeysWithLinks()
	{
		issueToBuildDAO.relink(getBuild(), ImmutableSet.of(new IssueReference().setIssueKey("PR-1").setProjectKey("PR"),
		                                                   new IssueReference().setIssueKey("PR-2").setProjectKey("PR"),
		                                                   new IssueReference().setIssueKey("OP-1")));
		issueToBuildDAO.relink(getBuild(), ImmutableSet.of(new IssueReference().setIssueKey("PR-1").setProjectKey("PR"),
		                                                   new IssueReference().setIssueKey("PR-3").setProjectKey("PR"),
		                                                   new IssueReference().setIssueKey("OP-2")));

		assertThat(issueToBuildDAO.getAllIssueKeysWithLinks(5, 0)).containsOnly("PR-1", "PR-2", "PR-3", "OP-1", "OP-2");
		assertThat(issueToBuildDAO.getAllIssueKeysWithLinks(3, 0)).containsOnly("OP-1", "OP-2", "PR-1");
		assertThat(issueToBuildDAO.getAllIssueKeysWithLinks(3, 1)).containsOnly("PR-2", "PR-3");
	}

	@Test
	void testGetIssueLinks()
	{
		Build build = getBuild();

		issueToBuildDAO.relink(build, ImmutableSet.of(new IssueReference().setIssueKey("PR-1").setProjectKey("PR"),
		                                              new IssueReference().setIssueKey("PR-2").setProjectKey("PR"),
		                                              new IssueReference().setIssueKey("OP-1")));

		assertThat(issueToBuildDAO.getIssueLinks(build)).hasSize(3)
				.extracting(IssueReference::getIssueKey, IssueReference::getProjectKey)
				.containsOnly(tuple("PR-1", "PR"), tuple("PR-2", "PR"), tuple("OP-1", "OP"));

		assertThat(issueToBuildDAO.getIssueLinks(job)).hasSize(3)
				.extracting(IssueReference::getIssueKey, IssueReference::getProjectKey)
				.containsOnly(tuple("PR-1", "PR"), tuple("PR-2", "PR"), tuple("OP-1", "OP"));
	}

	@Test
	void testGetIssueLinks_Job()
	{
		Build build1 = getBuild();

		issueToBuildDAO.relink(build1, ImmutableSet.of(new IssueReference().setIssueKey("PR-1").setProjectKey("PR"),
		                                               new IssueReference().setIssueKey("PR-2").setProjectKey("PR"),
		                                               new IssueReference().setIssueKey("OP-1")));

		Build build2 = getBuild();

		issueToBuildDAO.relink(build2, ImmutableSet.of(new IssueReference().setIssueKey("PR-3").setProjectKey("PR"),
		                                               new IssueReference().setIssueKey("PR-4").setProjectKey("PR"),
		                                               new IssueReference().setIssueKey("OP-2")));

		assertThat(issueToBuildDAO.getIssueLinks(build1)).hasSize(3)
				.extracting(IssueReference::getIssueKey, IssueReference::getProjectKey)
				.containsOnly(tuple("PR-1", "PR"), tuple("PR-2", "PR"), tuple("OP-1", "OP"));
		assertThat(issueToBuildDAO.getIssueLinks(build2)).hasSize(3)
				.extracting(IssueReference::getIssueKey, IssueReference::getProjectKey)
				.containsOnly(tuple("PR-3", "PR"), tuple("PR-4", "PR"), tuple("OP-2", "OP"));

		Set<IssueReference> issuesByJob = issueToBuildDAO.getIssueLinks(job);
		assertThat(issuesByJob).hasSize(6)
				.extracting(IssueReference::getIssueKey, IssueReference::getProjectKey)
				.containsOnly(tuple("PR-1", "PR"), tuple("PR-2", "PR"), tuple("OP-1", "OP"), tuple("PR-3", "PR"), tuple("PR-4", "PR"),
				              tuple("OP-2", "OP"));
	}

	@Test
	void testLink()
	{
		Build build = getBuild();

		issueToBuildDAO.link(build, new IssueReference().setIssueKey("PR-1").setProjectKey("PR"));

		assertThat(issueToBuildDAO.getIssueLinks(build)).hasSize(1)
				.extracting(IssueReference::getIssueKey, IssueReference::getProjectKey)
				.containsOnly(tuple("PR-1", "PR"));

		// Adding the same link again overrides the existing link
		issueToBuildDAO.link(build, new IssueReference().setIssueKey("PR-1").setProjectKey("PR"));

		assertThat(issueToBuildDAO.getIssueLinks(build)).hasSize(1)
				.extracting(IssueReference::getIssueKey, IssueReference::getProjectKey)
				.containsOnly(tuple("PR-1", "PR"));

		// Adding a new link adds it
		issueToBuildDAO.link(build, new IssueReference().setIssueKey("PR-2").setProjectKey("PR"));

		assertThat(issueToBuildDAO.getIssueLinks(build)).hasSize(2)
				.extracting(IssueReference::getIssueKey, IssueReference::getProjectKey)
				.containsOnly(tuple("PR-1", "PR"), tuple("PR-2", "PR"));
	}

	@Test
	void testRelink()
	{
		Build build = getBuild();

		issueToBuildDAO.relink(build, ImmutableSet.of(new IssueReference().setIssueKey("PR-1").setProjectKey("PR")));

		assertThat(issueToBuildDAO.getIssueLinks(build)).hasSize(1)
				.extracting(IssueReference::getIssueKey, IssueReference::getProjectKey)
				.containsOnly(tuple("PR-1", "PR"));

		issueToBuildDAO.relink(build, ImmutableSet.of(new IssueReference().setIssueKey("OP-1").setProjectKey("OP")));

		assertThat(issueToBuildDAO.getIssueLinks(build)).hasSize(1)
				.extracting(IssueReference::getIssueKey, IssueReference::getProjectKey)
				.containsOnly(tuple("OP-1", "OP"));

		issueToBuildDAO.relink(build, new HashSet<>());

		assertThat(issueToBuildDAO.getIssueLinks(build)).isEmpty();
	}

	@Test
	void testUnlinkForIssueKey()
	{
		Build build = getBuild();

		issueToBuildDAO.relink(build, ImmutableSet.of(new IssueReference().setIssueKey("PR-1").setProjectKey("PR"),
		                                              new IssueReference().setIssueKey("PR-2").setProjectKey("PR"),
		                                              new IssueReference().setIssueKey("PR-3").setProjectKey("PR")));

		assertThat(issueToBuildDAO.getIssueLinks(build)).hasSize(3)
				.extracting(IssueReference::getIssueKey)
				.containsOnly("PR-1", "PR-2", "PR-3");

		issueToBuildDAO.unlinkForIssue("PR-1");

		assertThat(issueToBuildDAO.getIssueLinks(build)).hasSize(2).extracting(IssueReference::getIssueKey).containsOnly("PR-2", "PR-3");
	}

	@Test
	void testUnlinkForProjectKey()
	{
		Build build = getBuild();

		issueToBuildDAO.relink(build, ImmutableSet.of(new IssueReference().setIssueKey("PR-1").setProjectKey("PR"),
		                                              new IssueReference().setIssueKey("PR-2").setProjectKey("PR"),
		                                              new IssueReference().setIssueKey("OP-1").setProjectKey("OP")));

		assertThat(issueToBuildDAO.getIssueLinks(build)).hasSize(3)
				.extracting(IssueReference::getIssueKey)
				.containsOnly("PR-1", "PR-2", "OP-1");

		issueToBuildDAO.unlinkForProject("PR");

		assertThat(issueToBuildDAO.getIssueLinks(build)).hasSize(1).extracting(IssueReference::getIssueKey).containsOnly("OP-1");
	}

	protected Build getBuild()
	{
		Build build = buildDAO.save(new Build().setJob(job)
				                            .setNumber(number.getAndIncrement())
				                            .setResult(Result.SUCCESS)
				                            .setTimestamp(System.currentTimeMillis()));

		assertThat(issueToBuildDAO.getIssueLinks(build)).isEmpty();
		return build;
	}
}
