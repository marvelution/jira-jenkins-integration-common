/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services.api;

import java.util.Map;

import org.marvelution.testing.TestSupport;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.assertj.core.api.Assertions.assertThat;

public class ConfigurationServiceTest
        extends TestSupport
{

    @Test
    void testStringToSet()
    {
        assertThat(ConfigurationService.STRING_TO_SET.apply("foo,bar,baz")).contains("foo", "bar", "baz");
    }

    @ParameterizedTest
    @ValueSource(strings = {"customfield_12345=$.value\r\ncustomfield_54321=$.key\r\ncomponents=$.id\r\n",
            "customfield_12345=$.value\ncustomfield_54321=$.key\ncomponents=$.id\n"})
    void testStringToMap(String string)
    {
        Map<String, String> map = ConfigurationService.STRING_TO_MAP.apply(string);
        assertThat(map).containsOnlyKeys("customfield_12345", "customfield_54321", "components")
                .containsEntry("customfield_12345", "$.value")
                .containsEntry("customfield_54321", "$.key")
                .containsEntry("components", "$.id");
    }
}
