/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services;

import java.io.ByteArrayInputStream;
import java.net.URI;
import java.util.*;
import java.util.stream.Stream;
import jakarta.inject.Inject;
import jakarta.json.Json;
import jakarta.json.JsonObject;
import jakarta.json.JsonValue;

import org.marvelution.jji.Headers;
import org.marvelution.jji.data.services.api.ConfigurationService;
import org.marvelution.jji.model.*;
import org.marvelution.jji.synctoken.CanonicalHttpServletRequest;
import org.marvelution.jji.synctoken.SyncTokenAuthenticator;
import org.marvelution.jji.synctoken.utils.SharedSecretGenerator;
import org.marvelution.testing.TestSupport;
import org.marvelution.testing.inject.InjectorExtension;
import org.marvelution.testing.wiremock.FileSource;
import org.marvelution.testing.wiremock.FileSourceType;
import org.marvelution.testing.wiremock.WireMockOptions;
import org.marvelution.testing.wiremock.WireMockServer;

import com.github.tomakehurst.wiremock.client.ResponseDefinitionBuilder;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.http.HttpHeader;
import com.github.tomakehurst.wiremock.http.HttpHeaders;
import com.github.tomakehurst.wiremock.matching.RequestPatternBuilder;
import com.github.tomakehurst.wiremock.matching.UrlPattern;
import com.github.tomakehurst.wiremock.verification.LoggedRequest;
import com.google.inject.Module;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.http.RequestMethod.GET;
import static com.github.tomakehurst.wiremock.http.RequestMethod.POST;
import static org.assertj.core.api.Assertions.*;
import static org.marvelution.jji.data.services.BaseSiteClient.X_JIRA_INTEGRATION;
import static org.marvelution.jji.data.services.BaseSiteClient.X_JJI_VERSION;
import static org.marvelution.jji.model.Matchers.equalTo;
import static org.marvelution.jji.model.Result.FAILURE;
import static org.marvelution.jji.model.Status.*;

/**
 * Common integration tests for {@link BaseSiteClient} implementations.
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
@ExtendWith(InjectorExtension.class)
public abstract class AbstractBaseSiteClientIT<S extends BaseSiteClient>
        extends TestSupport
        implements Module
{

    private static final UrlPattern ROOT_API_JSON_PATH_MATCHER = urlPathEqualTo("/api/json/");
    private static final UrlPattern CRUMB_PATH_MATCHER = urlPathEqualTo("/crumbIssuer/api/json/");
    private static final UrlPattern PING_PATH_MATCHER = urlPathEqualTo("/plugin/jira-integration/ping.html");
    private static final UrlPattern REGISTER_PATH_MATCHER = urlPathEqualTo("/jji/register/");
    private static final UrlPattern UNREGISTER_PATH_MATCHER = urlPathEqualTo("/jji/unregister/");
    private static final String JOBS_FIELD_FILTER = "jobs[name,fullName,url]";
    private static final String JOB_FIELD_FILTER =
            "name,fullName,url,displayName,description,firstBuild[number],lastBuild[number],builds[number,result]," + JOBS_FIELD_FILTER;
    private static final String BUILD_FIELD_FILTER =
            "number,displayName,fullDisplayName,description,builtOn,result,building,timestamp,duration,changeSet[*,items[*]],changeSets[*,items[*]],actions[*,causes[shortDescription],parent[*],environment[*],revision[*,head[*]]]";
    protected WireMockServer jenkins;
    @Inject
    @javax.inject.Inject
    private S siteClient;
    @Inject
    @javax.inject.Inject
    private ConfigurationService configurationService;
    private Site site;

    protected abstract String baseRpcUrl();

    @BeforeEach
    void setUpSite(
            @WireMockOptions(fileSource = @FileSource(type = FileSourceType.UNDER_CLASSPATH,
                                                      path = "BaseSiteClientIT"))
            WireMockServer jenkins)
    {
        this.jenkins = jenkins;
        site = new Site().setName(getTestMethodName())
                .setRpcUrl(jenkins.serverUri())
                .setSharedSecret(SharedSecretGenerator.generate());
    }

    @AfterEach
    void tearDown()
    {
        jenkins.findAll(RequestPatternBuilder.allRequests())
                .forEach(request -> {
                    assertThat(request.getHeaders()
                            .keys()).contains(Headers.SYNC_TOKEN);
                    String token = request.getHeader(Headers.SYNC_TOKEN);
                    CanonicalHttpServletRequest canonicalHttpRequest = new CanonicalHttpServletRequest(request.getMethod()
                            .getName(),
                            URI.create(request.getAbsoluteUrl()),
                            Optional.of(site.getRpcUrl()
                                    .getPath()));
                    new SyncTokenAuthenticator(issuer -> Optional.of(site)
                            .filter(site -> Objects.equals(site.getId(), issuer))
                            .map(Site::getSharedSecret)).authenticate(token, canonicalHttpRequest);
                });
    }

    @Test
    void testGetRemoteStatus()
    {
        jenkins.expect(GET, ROOT_API_JSON_PATH_MATCHER, aResponse().withStatus(200));

        assertThat(siteClient.getRemoteStatus(site)).isEqualTo(ONLINE);

        jenkins.verify(GET, ROOT_API_JSON_PATH_MATCHER);
    }

    @Test
    void testGetRemoteStatus_NotAccessible()
    {
        jenkins.expect(GET, ROOT_API_JSON_PATH_MATCHER, aResponse().withStatus(401));

        assertThat(siteClient.getRemoteStatus(site)).isEqualTo(NOT_ACCESSIBLE);

        jenkins.verify(GET, ROOT_API_JSON_PATH_MATCHER);
    }

    @Test
    void testGetRemoteStatus_Offline()
    {
        jenkins.shutdown();

        assertThat(siteClient.getRemoteStatus(site)).isEqualTo(OFFLINE);
    }

    @Test
    void testGetRemoteStatus_DnsIssue()
    {
        assertThat(siteClient.getRemoteStatus(new Site().setRpcUrl(URI.create("https://1234567890.sites.api.marvelution.io")))).isEqualTo(
                UNREACHABLE);
    }

    @Test
    void testGetRemoteStatus_Tunnels_DnsIssue()
    {
        assertThat(siteClient.getRemoteStatus(new Site().setTunneledSite()
                .setRpcUrl(URI.create("https://1234567890.sites.api.marvelution.io")))).isEqualTo(GETTING_READY);
    }

    @ParameterizedTest
    @MethodSource("jenkinsPluginArgs")
    void testIsJenkinsPluginInstalled(
            String header,
            String version)
    {
        jenkins.expect(GET,
                PING_PATH_MATCHER,
                aResponse().withHeaders(new HttpHeaders(HttpHeader.httpHeader(header, version)))
                        .withStatus(200));

        assertThat(siteClient.isJenkinsPluginInstalled(site)).isTrue();

        jenkins.verify(GET, PING_PATH_MATCHER);
    }

    @Test
    void testIsJenkinsPluginInstalled_NotInstalled()
    {
        jenkins.expect(GET, PING_PATH_MATCHER, aResponse().withStatus(404));

        assertThat(siteClient.isJenkinsPluginInstalled(site)).isFalse();

        jenkins.verify(GET, PING_PATH_MATCHER);
    }

    @ParameterizedTest
    @MethodSource("jenkinsPluginArgs")
    void testGetJenkinsPluginVersion(
            String header,
            String version)
    {
        jenkins.expect(GET,
                PING_PATH_MATCHER,
                aResponse().withHeaders(new HttpHeaders(HttpHeader.httpHeader(header, version)))
                        .withStatus(200));

        assertThat(siteClient.getJenkinsPluginVersion(site)).isPresent()
                .contains(Version.of(version));

        jenkins.verify(GET, PING_PATH_MATCHER);
    }

    @Test
    void testGetJenkinsPluginVersion_NotInstalled()
    {
        jenkins.expect(GET, PING_PATH_MATCHER, aResponse().withStatus(404));

        assertThat(siteClient.getJenkinsPluginVersion(site)).isEmpty();

        jenkins.verify(GET, PING_PATH_MATCHER);
    }

    @Test
    void testGetJenkinsPluginVersion_OldPlugin()
    {
        jenkins.expect(GET, PING_PATH_MATCHER, aResponse().withStatus(404));
        jenkins.expect(GET,
                urlPathEqualTo("/plugin/jenkins-jira-plugin/ping.html"),
                aResponse().withStatus(200)
                        .withHeader(X_JJI_VERSION, "5.1.0"));

        assertThat(siteClient.getJenkinsPluginVersion(site)).isPresent()
                .contains(Version.of("5.1.0"));

        jenkins.verify(GET, PING_PATH_MATCHER);
        jenkins.verify(GET, urlPathEqualTo("/plugin/jenkins-jira-plugin/ping.html"));
    }

    @Test
    void testIsCrumbSecurityEnabled()
    {
        jenkins.expect(GET,
                ROOT_API_JSON_PATH_MATCHER,
                aResponse().withStatus(200)
                        .withBodyFile("crumb_security_enabled.json"));

        assertThat(siteClient.isCrumbSecurityEnabled(site)).isTrue();

        jenkins.verify(GET, ROOT_API_JSON_PATH_MATCHER);
    }

    @Test
    void testIsCrumbSecurityEnabled_Disabled()
    {
        jenkins.expect(GET,
                ROOT_API_JSON_PATH_MATCHER,
                aResponse().withStatus(200)
                        .withBodyFile("crumb_security_disabled.json"));

        assertThat(siteClient.isCrumbSecurityEnabled(site)).isFalse();

        jenkins.verify(GET, ROOT_API_JSON_PATH_MATCHER);
    }

    @Test
    void testIsCrumbSecurityEnabled_NotDefined()
    {
        jenkins.expect(GET,
                ROOT_API_JSON_PATH_MATCHER,
                aResponse().withStatus(200)
                        .withBodyFile("crumb_security_not_defined.json"));

        assertThat(siteClient.isCrumbSecurityEnabled(site)).isTrue();

        jenkins.verify(GET, ROOT_API_JSON_PATH_MATCHER);
    }

    @Test
    void testRegisterWithSite()
            throws Exception
    {
        testRegisterWithSite(true, true);
    }

    @Test
    void testRegisterWithSite_WithCrumbDisabled()
            throws Exception
    {
        testRegisterWithSite(false, true);
    }

    @Test
    void testRegisterWithSite_Unsuccessful()
            throws Exception
    {
        testRegisterWithSite(true, false);
    }

    private void testRegisterWithSite(
            boolean useCrumbs,
            boolean successful)
            throws Exception
    {
        site.setId("my-special-id")
                .setSharedSecret(SharedSecretGenerator.generate())
                .setJenkinsPluginInstalled(true)
                .setUseCrumbs(useCrumbs);
        jenkins.expect(POST, REGISTER_PATH_MATCHER, aResponse().withStatus(successful ? 200 : 500));

        assertThat(siteClient.registerWithSite(site)).isEqualTo(successful);

        jenkins.verify(0, getRequestedFor(CRUMB_PATH_MATCHER));
        jenkins.verify(POST, REGISTER_PATH_MATCHER);
        List<LoggedRequest> requests = jenkins.findAll(postRequestedFor(REGISTER_PATH_MATCHER));
        assertThat(requests).hasSize(1);
        JsonObject json = Json.createReader(new ByteArrayInputStream(requests.get(0)
                        .getBody()))
                .readObject();
        assertThat(json.keySet()).contains("url", "name", "identifier", "sharedSecret", "firewalled", "tunneled");
        assertThat(json.get("url")).isEqualTo(Json.createValue(baseRpcUrl()));
        assertThat(json).contains(entry("name", Json.createValue(configurationService.getInstanceName())),
                entry("identifier", Json.createValue(site.getId())),
                entry("sharedSecret", Json.createValue(site.getSharedSecret())),
                entry("firewalled", site.isInaccessibleSite() ? JsonValue.TRUE : JsonValue.FALSE),
                entry("tunneled", site.isTunneledSite() ? JsonValue.TRUE : JsonValue.FALSE));
        Map<String, Object> context = configurationService.getSiteRegistrationContext(site);
        if (context != null)
        {
            assertThat(json.get("context")).isEqualTo(Json.createObjectBuilder(context)
                    .build());
        }
    }

    @Test
    void testRegisterWithSiteFailureOnError()
    {
        site.setId("my-special-id")
                .setSharedSecret(SharedSecretGenerator.generate())
                .setJenkinsPluginInstalled(true)
                .setUseCrumbs(true);
        jenkins.expect(POST,
                REGISTER_PATH_MATCHER,
                aResponse().withStatus(500)
                        .withStatusMessage("OOPS"));

        assertThatThrownBy(() -> siteClient.registerWithSite(site, true)).isInstanceOf(IllegalStateException.class)
                .hasMessage("Failed to register with site " + site.getName() + "; Site returned OOPS [500]");
    }

    @Test
    void testUnregisterWithSite()
            throws Exception
    {
        testUnregisterWithSite(true, true);
    }

    @Test
    void testUnregisterWithSite_WithCrumbDisabled()
            throws Exception
    {
        testUnregisterWithSite(false, true);
    }

    @Test
    void testUnregisterWithSite_Unsuccessful()
            throws Exception
    {
        testUnregisterWithSite(true, false);
    }

    private void testUnregisterWithSite(
            boolean useCrumbs,
            boolean successful)
            throws Exception
    {
        site.setJenkinsPluginInstalled(true)
                .setUseCrumbs(useCrumbs);
        jenkins.expect(POST, UNREGISTER_PATH_MATCHER, aResponse().withStatus(successful ? 200 : 500));

        assertThat(siteClient.unregisterWithSite(site)).isEqualTo(successful);

        jenkins.verify(0, getRequestedFor(CRUMB_PATH_MATCHER));
        jenkins.verify(POST, UNREGISTER_PATH_MATCHER);
        List<LoggedRequest> requests = jenkins.findAll(postRequestedFor(UNREGISTER_PATH_MATCHER));
        assertThat(requests).hasSize(1);
        JsonObject json = Json.createReader(new ByteArrayInputStream(requests.get(0)
                        .getBody()))
                .readObject();
        assertThat(json.keySet()).hasSize(1)
                .containsOnly("url");
        assertThat(json.get("url")).isEqualTo(Json.createValue(baseRpcUrl()));
    }

    @Test
    void testGetJobs()
    {
        expectApiCall(ROOT_API_JSON_PATH_MATCHER,
                JOBS_FIELD_FILTER,
                aResponse().withStatus(200)
                        .withBodyFile("job-list.json"));

        List<Job> jobs = siteClient.getJobs(site);

        assertThat(jobs).hasSize(3)
                .extracting(Job::getSite, Job::getName)
                .containsOnly(tuple(site, "jira-jenkins-integration"),
                        tuple(site, "jira-jenkins-integration-common"),
                        tuple(site, "jenkins-jira-integration"));
        verifyApiCall(ROOT_API_JSON_PATH_MATCHER, JOBS_FIELD_FILTER);
    }

    @Test
    void testGetJobs_NoJobs()
    {
        expectApiCall(ROOT_API_JSON_PATH_MATCHER, JOBS_FIELD_FILTER, aResponse().withStatus(200));

        List<Job> jobs = siteClient.getJobs(site);

        assertThat(jobs).isEmpty();
        verifyApiCall(ROOT_API_JSON_PATH_MATCHER, JOBS_FIELD_FILTER);
    }

    @Test
    void testPopulateJobDetails()
    {
        UrlPattern urlPattern = urlPathEqualTo("/job/jira-jenkins-integration/api/json/");
        expectApiCall(urlPattern,
                JOB_FIELD_FILTER,
                aResponse().withStatus(200)
                        .withBodyFile("job.json"));
        Job job = new Job().setSite(site)
                .setName("jira-jenkins-integration");

        siteClient.populateJobDetails(job);

        assertThat(job).is(equalTo(new Job().setSite(site)
                .setLinked(false)
                .setName("jira-jenkins-integration")
                .setLastBuild(2144)
                .setOldestBuild(1623)
                .setDeleted(false)));
        assertThat(job.getBuilds()).hasSize(18);
        assertThat(job.getJobs()).isEmpty();
        verifyApiCall(urlPattern, JOB_FIELD_FILTER);
    }

    @Test
    void testPopulateJobDetails_JobNotFound()
    {
        UrlPattern urlPattern = urlPathEqualTo("/job/jira-jenkins-integration/api/json/");
        expectApiCall(urlPattern, JOB_FIELD_FILTER, aResponse().withStatus(404));
        Job job = new Job().setSite(site)
                .setName("jira-jenkins-integration");

        siteClient.populateJobDetails(job);

        assertThat(job).is(equalTo(new Job().setSite(site)
                .setName("jira-jenkins-integration")
                .setDeleted(true)));
        assertThat(job.getBuilds()).isEmpty();
        assertThat(job.getJobs()).isEmpty();
        verifyApiCall(urlPattern, JOB_FIELD_FILTER);
    }

    @Test
    void testPopulateBuildDetails()
    {
        UrlPattern urlPattern = urlPathEqualTo("/job/jira-jenkins-integration/2144/api/json/");
        expectApiCall(urlPattern,
                BUILD_FIELD_FILTER,
                aResponse().withStatus(200)
                        .withBodyFile("build.json"));
        Job job = new Job().setSite(site)
                .setName("jira-jenkins-integration");
        Build build = new Build().setJob(job)
                .setNumber(2144);

        siteClient.populateBuildDetails(build);

        List<ChangeSet> changeSet = new ArrayList<>();
        changeSet.add(new ChangeSet().setCommitId("9a0d506fa8982e41ed3340caeed4a605587b6aa2")
                .setMessage("Dummy commit message for testing"));
        changeSet.add(new ChangeSet().setCommitId("9a0d506fa8982e41ed3340caeed4a605587b6aa3")
                .setMessage("Commit message for testing"));
        assertThat(build).is(equalTo(new Build().setJob(job)
                .setNumber(2144)
                .setCause("Started by an SCM change")
                .setDescription("My build description")
                .setDuration(135329L)
                .setBuiltOn("remote-slave-6")
                .setTimestamp(1355405446078L)
                .setResult(FAILURE)
                .setTestResults(new TestResults().setTotal(3121)
                        .setFailed(0)
                        .setSkipped(0))
                .setChangeSet(changeSet)));
        verifyApiCall(urlPattern, BUILD_FIELD_FILTER);
    }

    @Test
    void testPopulateBuildDetails_BuildNotFound()
    {
        UrlPattern urlPattern = urlPathEqualTo("/job/jira-jenkins-integration/2144/api/json/");
        expectApiCall(urlPattern, BUILD_FIELD_FILTER, aResponse().withStatus(404));
        Job job = new Job().setSite(site)
                .setName("jira-jenkins-integration");
        Build build = new Build().setJob(job)
                .setNumber(2144);

        siteClient.populateBuildDetails(build);

        assertThat(build).is(equalTo(new Build().setJob(job)
                .setNumber(2144)
                .setDeleted(true)));
        verifyApiCall(urlPattern, BUILD_FIELD_FILTER);
    }

    private void expectApiCall(
            UrlPattern urlPattern,
            String fieldFilter,
            ResponseDefinitionBuilder response)
    {
        jenkins.expect(request(GET.getName(), urlPattern).withQueryParam("tree", WireMock.equalTo(fieldFilter))
                .willReturn(response));
    }

    private void verifyApiCall(
            UrlPattern urlPattern,
            String fieldFilter)
    {
        jenkins.verify(new RequestPatternBuilder(GET, urlPattern).withQueryParam("tree", WireMock.equalTo(fieldFilter)));
    }

    public static Stream<Arguments> jenkinsPluginArgs()
    {
        return Stream.of(Arguments.of(X_JIRA_INTEGRATION, "5.2.0"), Arguments.of(X_JJI_VERSION, "5.1.0"));
    }
}
