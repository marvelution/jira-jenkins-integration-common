/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services;

import java.net.URI;
import java.util.Optional;
import java.util.UUID;

import org.marvelution.jji.api.text.TextResolver;
import org.marvelution.jji.data.access.api.SiteDAO;
import org.marvelution.jji.data.services.api.SiteClient;
import org.marvelution.jji.model.Site;
import org.marvelution.jji.model.SiteType;
import org.marvelution.jji.model.Status;
import org.marvelution.jji.model.Version;
import org.marvelution.jji.validation.api.ErrorMessages;
import org.marvelution.jji.validation.api.ErrorMessages.ErrorMessage;
import org.marvelution.jji.validation.api.ValidationException;
import org.marvelution.testing.TestSupport;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mock;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class DefaultSiteValidatorTest
        extends TestSupport
{

    protected TextResolver textResolver;
    @Mock
    protected SiteDAO siteDAO;
    @Mock
    protected SiteClient siteClient;
    protected DefaultSiteValidator validator;

    @BeforeEach
    public void setUp()
    {
        textResolver = mock(TextResolver.class, invocation -> invocation.getArgument(0));
        validator = new DefaultSiteValidator(textResolver, siteDAO, siteClient);
    }

    @Test
    void testAssertValidSite()
    {
        Site site = newBareValidSite();
        validator.assertValid(site);
    }

    @Test
    void testAssertValidSite_InvalidSite()
    {
        assertThatThrownBy(() -> validator.assertValid(new Site())).isInstanceOf(ValidationException.class);
    }

    @Test
    void testValidateSite()
    {
        Site site = newBareValidSite();

        when(siteDAO.findByName(site.getName())).thenReturn(Optional.of(site));
        when(siteDAO.findByRpcUrl(site.getRpcUrl())).thenReturn(Optional.of(site));

        ErrorMessages errorMessages = validator.validate(site);

        assertThat(errorMessages.hasErrors()).isFalse();
    }

    @ParameterizedTest
    @ValueSource(strings = {"http://jenkins.example.com",
            "https://jenkins.example.com",
            "http://jenkins.example.com:80",
            "https://jenkins.example.com:443"})
    void testValidateSite_Ports(String rpcUrl)
    {
        Site site = newBareValidSite().setRpcUrl(URI.create(rpcUrl));

        ErrorMessages errorMessages = validator.validate(site);

        assertThat(errorMessages.hasErrors()).isFalse();
    }

    @Test
    void testValidateSite_EmptySite()
    {
        ErrorMessages errorMessages = validator.validate(new Site());

        assertThat(errorMessages.hasErrors()).isTrue();
        assertThat(errorMessages.getErrors()).hasSize(3)
                .contains(new ErrorMessage().setField("name")
                                .setMessage("name.required"),
                        new ErrorMessage().setField("type")
                                .setMessage("site.type.required"),
                        new ErrorMessage().setField("rpcUrl")
                                .setMessage("site.url.required"));
    }

    @Test
    void testValidateSite_InvalidName()
    {
        Site site = newBareValidSite().setName("\"><img src=x onerror=prompt(1);>");
        ErrorMessages errorMessages = validator.validate(site);

        assertThat(errorMessages.hasErrors()).isTrue();
        assertThat(errorMessages.getErrors()).hasSize(1)
                .contains(new ErrorMessage().setField("name")
                        .setMessage("name.invalid.tokens"));
    }

    @ParameterizedTest
    @ValueSource(strings = {"test",
            "test123",
            "test 123",
            "test - test",
            "test_test",
            "test - test_1",
            "123",
            "t"})
    void testValidateSite_ValidNames(String name)
    {
        Site site = newBareValidSite().setName(name);

        when(siteDAO.findByName(site.getName())).thenReturn(Optional.of(site));
        when(siteDAO.findByRpcUrl(site.getRpcUrl())).thenReturn(Optional.of(site));

        ErrorMessages errorMessages = validator.validate(site);

        assertThat(errorMessages.hasErrors()).isFalse();
        assertThat(errorMessages.getErrors()).isEmpty();
    }

    @Test
    void testValidateSite_InvalidUrls()
    {
        ErrorMessages errorMessages = validator.validate(newBareValidSite().setRpcUrl(URI.create("localhost"))
                .setDisplayUrl(URI.create("ci.local")));

        assertThat(errorMessages.hasErrors()).isTrue();
        assertThat(errorMessages.getErrors()).hasSize(2)
                .contains(new ErrorMessage().setField("rpcUrl")
                                .setMessage("site.url.invalid"),
                        new ErrorMessage().setField("displayUrl")
                                .setMessage("site.url.invalid"));
    }

    @Test
    void testValidateSite_InvalidUrlBlankHostname()
    {
        ErrorMessages errorMessages = validator.validate(newBareValidSite().setRpcUrl(URI.create("http:///jenkins"))
                .setDisplayUrl(URI.create("https:///jenkins")));

        assertThat(errorMessages.hasErrors()).isTrue();
        assertThat(errorMessages.getErrors()).hasSize(2)
                .contains(new ErrorMessage().setField("rpcUrl")
                                .setMessage("site.url.invalid"),
                        new ErrorMessage().setField("displayUrl")
                                .setMessage("site.url.invalid"));
    }

    @Test
    void testValidateSite_UnsupportedSchemes()
    {
        ErrorMessages errorMessages = validator.validate(newBareValidSite().setRpcUrl(URI.create("ftp://localhost"))
                .setDisplayUrl(URI.create("file://localhost")));

        assertThat(errorMessages.hasErrors()).isTrue();
        assertThat(errorMessages.getErrors()).hasSize(2)
                .contains(new ErrorMessage().setField("rpcUrl")
                                .setMessage("site.url.invalid"),
                        new ErrorMessage().setField("displayUrl")
                                .setMessage("site.url.invalid"));
    }

    @Test
    void testValidateSite_QueryString()
    {
        ErrorMessages errorMessages = validator.validate(newBareValidSite().setRpcUrl(URI.create("http://localhost?jenkins"))
                .setDisplayUrl(URI.create("https://localhost?other")));

        assertThat(errorMessages.hasErrors()).isTrue();
        assertThat(errorMessages.getErrors()).hasSize(2)
                .contains(new ErrorMessage().setField("rpcUrl")
                                .setMessage("site.url.invalid"),
                        new ErrorMessage().setField("displayUrl")
                                .setMessage("site.url.invalid"));
    }

    @Test
    void testValidateSite_Fragment()
    {
        ErrorMessages errorMessages = validator.validate(newBareValidSite().setRpcUrl(URI.create("http://localhost#jenkins"))
                .setDisplayUrl(URI.create("https://localhost#other")));

        assertThat(errorMessages.hasErrors()).isTrue();
        assertThat(errorMessages.getErrors()).hasSize(2)
                .contains(new ErrorMessage().setField("rpcUrl")
                                .setMessage("site.url.invalid"),
                        new ErrorMessage().setField("displayUrl")
                                .setMessage("site.url.invalid"));
    }

    @Test
    void testValidateSite_DuplicateName()
    {
        Site site = newBareValidSite();
        when(siteDAO.findByName(site.getName())).thenReturn(Optional.of(newBareValidSite()));

        ErrorMessages errorMessages = validator.validate(site);

        assertThat(errorMessages.hasErrors()).isTrue();
        assertThat(errorMessages.getErrors()).hasSize(1)
                .contains(new ErrorMessage().setField("name")
                        .setMessage("name.duplicate"));
    }

    @Test
    void testValidateSite_DuplicateRpcUrl()
    {
        Site site = newBareValidSite();
        when(siteDAO.findByRpcUrl(site.getRpcUrl())).thenReturn(Optional.of(newBareValidSite()));

        ErrorMessages errorMessages = validator.validate(site);

        assertThat(errorMessages.hasErrors()).isTrue();
        assertThat(errorMessages.getErrors()).hasSize(1)
                .contains(new ErrorMessage().setField("rpcUrl")
                        .setMessage("site.url.duplicate"));
    }

    @Test
    void testValidateSite_MissingUser()
    {
        Site site = newBareValidSite().setToken("my-secret");

        ErrorMessages errorMessages = validator.validate(site);

        assertThat(errorMessages.hasErrors()).isTrue();
        assertThat(errorMessages.getErrors()).hasSize(1)
                .contains(new ErrorMessage().setField("user")
                        .setMessage("site.user.required"));
    }

    @Test
    void testValidateSite_MissingToken()
    {
        Site site = newBareValidSite().setUser("admin");

        ErrorMessages errorMessages = validator.validate(site);

        assertThat(errorMessages.hasErrors()).isTrue();
        assertThat(errorMessages.getErrors()).hasSize(1)
                .contains(new ErrorMessage().setField("token")
                        .setMessage("site.token.required"));
    }

    protected Site newBareValidSite()
    {
        return new Site().setId(UUID.randomUUID()
                        .toString())
                .setType(SiteType.JENKINS)
                .setName(getTestMethodName())
                .setRpcUrl(URI.create("http://jenkins.example.com"));
    }
}
