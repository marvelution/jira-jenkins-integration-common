/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services;

import java.util.stream.*;

import org.marvelution.jji.data.services.api.*;
import org.marvelution.jji.model.*;

import jakarta.inject.*;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.*;
import org.junit.jupiter.params.provider.*;

import static org.assertj.core.api.Assertions.*;
import static org.marvelution.jji.data.services.api.ConfigurationService.GLOBAL_SCOPE;
import static org.marvelution.jji.data.services.api.ConfigurationService.*;
import static org.marvelution.jji.model.ConfigurationSetting.*;

public abstract class AbstractBaseConfigurationServiceTest
        extends AbstractBaseServiceTest
{
    @Inject
    @javax.inject.Inject
    protected ConfigurationService configurationService;

    @Test
    void testGetMaximumBuildsPerPage()
    {
        configurationService.saveConfigurationSetting(ConfigurationSetting.forKey(MAX_BUILDS_PER_PAGE)
                .setValue(1));

        assertThat(configurationService.getMaximumBuildsPerPage("TEST")).isOne();
    }

    @Test
    void testGetMaximumBuildsPerPage_Default()
    {
        assertThat(configurationService.getMaximumBuildsPerPage("TEST")).isEqualTo(DEFAULT_MAX_BUILDS_PER_PAGE);
    }

    @Test
    void testGetMaximumBuildsPerPage_InvalidNumber()
    {
        configurationService.saveConfigurationSetting(ConfigurationSetting.forKey(MAX_BUILDS_PER_PAGE)
                .setScope("TEST")
                .setValue("a1"));

        assertThat(configurationService.getMaximumBuildsPerPage("TEST")).isEqualTo(DEFAULT_MAX_BUILDS_PER_PAGE);
    }

    @Test
    void testSetMaximumBuildsPerPage_NegativeLimit()
    {
        configurationService.saveConfigurationSetting(ConfigurationSetting.forKey(MAX_BUILDS_PER_PAGE)
                .setValue(-12));

        assertThat(configurationService.getMaximumBuildsPerPage("TEST")).isEqualTo(DEFAULT_MAX_BUILDS_PER_PAGE);
    }


    @Test
    void testSaveConfiguration()
    {
        configurationService.saveConfiguration(new Configuration().addConfiguration(forKey(MAX_BUILDS_PER_PAGE).setValue(1)));
    }

    @Test
    void testSaveConfiguration_OnlySaveValidConfiguration()
    {
        configurationService.saveConfiguration(new Configuration().addConfiguration(forKey(MAX_BUILDS_PER_PAGE).setValue("I'm not a number")));
    }

    @Test
    void testSaveConfiguration_BlankConfiguration()
    {
        configurationService.saveConfiguration(new Configuration());
    }

    @Test
    void testGetConfigurationSetting()
    {
        ConfigurationSetting setting = configurationService.getConfigurationSetting("key");
        assertThat(setting).extracting(ConfigurationSetting::getKey,
                        ConfigurationSetting::getScope,
                        ConfigurationSetting::getValue,
                        ConfigurationSetting::isOverridable)
                .containsExactly("key", GLOBAL_SCOPE, null, true);
    }

    @Test
    void testGetConfigurationSetting_NonExistingKey()
    {
        ConfigurationSetting setting = configurationService.getConfigurationSetting("key");
        assertThat(setting).extracting(ConfigurationSetting::getKey, ConfigurationSetting::getValue, ConfigurationSetting::isOverridable)
                .containsExactly("key", null, true);
    }

    @Test
    void testSaveConfigurationSetting()
    {
        configurationService.saveConfigurationSetting(forKey(MAX_BUILDS_PER_PAGE).setValue(1));
    }

    @Test
    void testSaveConfigurationSetting_NoneDefaultKey()
    {
        configurationService.saveConfigurationSetting(forKey("key").setValue("value"));
    }

    @Test
    void testResetToDefaults()
    {
        configurationService.resetToDefaults();
    }

    @ParameterizedTest
    @ValueSource(ints = {10,
            20,
            30,
            40,
            50,
            60,
            70,
            80,
            90,
            100})
    void testStoreParameterIssueFields(int count)
    {
        String fields = IntStream.range(1, count + 1)
                .mapToObj(n -> "customfield_" + n)
                .collect(Collectors.joining(","));

        configurationService.saveConfigurationSetting(ConfigurationSetting.forKey(PARAMETER_ISSUE_FIELDS)
                .setValue(fields));
    }
}
