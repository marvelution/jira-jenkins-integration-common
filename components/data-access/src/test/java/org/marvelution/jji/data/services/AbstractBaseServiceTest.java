/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services;

import java.net.URI;
import java.util.Optional;
import java.util.UUID;

import org.marvelution.jji.api.events.EventPublisher;
import org.marvelution.jji.model.*;
import org.marvelution.jji.synctoken.utils.SharedSecretGenerator;
import org.marvelution.testing.TestSupport;
import org.marvelution.testing.inject.InjectorExtension;

import com.google.inject.Inject;
import com.google.inject.Module;
import org.junit.jupiter.api.extension.ExtendWith;

import static java.util.Optional.empty;
import static org.mockito.Mockito.*;

/**
 * TestSupport specific for Base...Service tests.
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
@ExtendWith(InjectorExtension.class)
public abstract class AbstractBaseServiceTest
        extends TestSupport
        implements Module
{

    @Inject
    @javax.inject.Inject
    protected EventPublisher eventPublisher;

    protected final <T> void assertEventPublished(T event)
    {
        if (mockingDetails(eventPublisher).isMock())
        {
            verify(eventPublisher, atLeastOnce()).publish(eq(event));
        }
        else
        {
            assertEventPublished(eventPublisher, event);
        }
    }

    protected <T> void assertEventPublished(
            EventPublisher eventPublisher,
            T event)
    {
        throw new AssertionError("EventPublisher is not a Mock and test " + getClass().getName() +
                                 " doesn't override assertEventPublished(EventPublisher eventPublisher, T event)");
    }

    Site createSite()
    {
        return createSite(empty());
    }

    Site createSite(Optional<String> uniquifier)
    {
        return new Site().setName(getTestMethodName() + uniquifier.map(suffix -> " " + suffix)
                        .orElse(""))
                .setSharedSecret(SharedSecretGenerator.generate())
                .setType(SiteType.JENKINS)
                .setRpcUrl(createRpcUrl(uniquifier));
    }

    protected URI createRpcUrl(Optional<String> uniquifier)
    {
        return URI.create("http://jenkins.example.com" + uniquifier.map(suffix -> "/" + suffix)
                .orElse(""));
    }

    Job createJob(Site site)
    {
        return createJob(site, empty());
    }

    Job createJob(
            Site site,
            Optional<String> uniquifier)
    {
        return new Job().setSite(site)
                .setName(getTestMethodName() + uniquifier.map(suffix -> " " + suffix)
                        .orElse(""));
    }

    protected Build createBuild(Job job)
    {
        return createBuild(job, 1);
    }

    Build createBuild(
            Job job,
            int number)
    {
        return new Build().setJob(job)
                .setNumber(number)
                .setResult(Result.SUCCESS)
                .setTimestamp(System.currentTimeMillis());
    }

    protected <I extends HasId> I setId(I item)
    {
        if (item.getId() == null)
        {
            item.setId(UUID.randomUUID()
                    .toString());
        }
        return item;
    }
}
