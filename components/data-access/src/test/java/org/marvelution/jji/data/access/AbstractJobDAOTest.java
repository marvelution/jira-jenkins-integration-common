/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access;

import org.marvelution.jji.model.*;
import org.marvelution.jji.utils.JobHash;

import java.net.URI;
import java.time.Instant;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static java.time.temporal.ChronoUnit.DAYS;
import static org.assertj.core.api.Assertions.assertThat;
import static org.marvelution.jji.model.Matchers.equalTo;
import static org.marvelution.jji.model.Result.SUCCESS;

public abstract class AbstractJobDAOTest
        extends AbstractDAOTest
{

    protected Site site;

    @BeforeEach
    void setUp()
    {
        site = siteDAO.save(new Site().setName("Site for " + getTestMethodName())
                .setSharedSecret("my-super-secret")
                .setType(SiteType.JENKINS)
                .setRpcUrl(URI.create("http://localhost:8080/")));
    }

    @Test
    void testCRUD()
    {
        // Create
        Job job = new Job().setSite(site)
                .setName("SomeJob")
                .setDescription("Some job running builds")
                .setLastBuild(1)
                .setOldestBuild(1)
                .setDisplayName("Some Job")
                .setDeleted(false)
                .setLinked(true);
        Job inserted = jobDAO.save(job);
        assertThat(inserted).is(equalTo(job, true));

        // Read
        assertThat(jobDAO.get(inserted.getId())).is(equalTo(inserted));

        // Update
        inserted.setUrlName("ParentJob/job/SomeJob")
                .setDescription(null)
                .setOldestBuild(1)
                .setLastBuild(10);
        Job updated = jobDAO.save(inserted);
        assertThat(updated).is(equalTo(inserted));

        // Delete
        jobDAO.delete(updated.getId());
        assertThat(jobDAO.get(updated.getId())).isNull();
    }

    @Test
    void testCountBySite()
    {
        Site other = siteDAO.save(new Site().setName("Other site for " + getTestMethodName())
                .setSharedSecret("my-super-other-secret")
                .setType(SiteType.JENKINS)
                .setRpcUrl(URI.create("http://localhost:8181/")));
        Job job = jobDAO.save(new Job().setSite(other)
                .setName("Job 1"));
        jobDAO.save(new Job().setSite(other)
                .setName("Other Job")
                .setParentName(job.getFullName()));
        jobDAO.save(new Job().setSite(site)
                .setName("Job 3")
                .setDeleted(true));
        jobDAO.save(new Job().setSite(site)
                .setName("project 1")
                .setDisplayName("Another Job 4"));

        assertThat(jobDAO.countBySite(site.getId())).isEqualTo(2);
        assertThat(jobDAO.countBySite(other.getId())).isEqualTo(2);
    }

    @Test
    void testHasGetDeletedJobs()
    {
        Job job1 = jobDAO.save(new Job().setSite(site)
                .setName("Job 1"));
        Job job2 = jobDAO.save(new Job().setSite(site)
                .setName("Other Job"));
        Job job3 = jobDAO.save(new Job().setSite(site)
                .setName("Job 3")
                .setDeleted(true));

        assertThat(jobDAO.hasDeletedJobs(site.getId())).isTrue();
        List<Job> jobs = jobDAO.getDeletedJobs(site.getId());
        assertThat(jobs).hasSize(1)
                .areExactly(1, equalTo(job3));
    }

    @Test
    void testGetBySiteIdAtLevel()
    {
        Job root = jobDAO.save(new Job().setSite(site)
                .setName("Root"));
        Job child1 = jobDAO.save(new Job().setSite(site)
                .setName("Child 1")
                .setParentName(root.getFullName()));
        Job child2 = jobDAO.save(new Job().setSite(site)
                .setName("Child 2")
                .setParentName(root.getFullName())
                .setDeleted(true));
        Job subChild = jobDAO.save(new Job().setSite(site)
                .setName("Sub Child")
                .setParentName(child1.getFullName()));

        List<Job> jobs = jobDAO.getBySiteIdAtLevel(site.getId(), null, true);
        assertThat(jobs).hasSize(1)
                .areExactly(1, equalTo(root));

        jobs = jobDAO.getBySiteIdAtLevel(site.getId(), root.getFullName(), false);
        assertThat(jobs).hasSize(1)
                .areExactly(1, equalTo(child1));

        jobs = jobDAO.getBySiteIdAtLevel(site.getId(), root.getFullName(), true);
        assertThat(jobs).hasSize(2)
                .areExactly(1, equalTo(child1))
                .areExactly(1, equalTo(child2));

        jobs = jobDAO.getBySiteIdAtLevel(site.getId(), child1.getFullName(), true);
        assertThat(jobs).hasSize(1)
                .areExactly(1, equalTo(subChild));

        assertThat(jobDAO.getBySiteIdAtLevel(site.getId(), child2.getFullName(), true)).isEmpty();
    }

    @Test
    void testFind_SiteId_Name_Parent()
    {
        Job root = jobDAO.save(new Job().setSite(site)
                .setName("Root"));
        Job child = jobDAO.save(new Job().setSite(site)
                .setName("Child")
                .setParentName(root.getFullName()));

        assertThat(jobDAO.find(site.getId(), child.getName(), root.getFullName())).isPresent()
                .get()
                .is(equalTo(child));
        assertThat(jobDAO.find(site.getId(), child.getName(), child.getParentName())).isPresent()
                .get()
                .is(equalTo(child));
    }

    @Test
    void testFind()
    {
        Job job = jobDAO.save(new Job().setSite(site)
                .setName("Find Me"));
        List<Job> jobs = jobDAO.find("FindMe");
        assertThat(jobs).isEmpty();
        jobs = jobDAO.find("Find Me");
        assertThat(jobs).hasSize(1)
                .areExactly(1, equalTo(job));
        Job job2 = jobDAO.save(new Job().setSite(site)
                .setName("Find Me"));
        jobs = jobDAO.find("Find Me");
        assertThat(jobs).hasSize(2)
                .areExactly(1, equalTo(job))
                .areExactly(1, equalTo(job2));

        Job job3 = jobDAO.save(new Job().setSite(site)
                .setName("Or Me")
                .setDisplayName("Find Me"));
        jobs = jobDAO.find("Find Me");
        assertThat(jobs).hasSize(3)
                .areExactly(1, equalTo(job))
                .areExactly(1, equalTo(job2))
                .areExactly(1, equalTo(job3));
    }

    @Test
    void testSearch()
    {
        Job job1 = jobDAO.save(new Job().setSite(site)
                .setName("Job 1"));
        Job job2 = jobDAO.save(new Job().setSite(site)
                .setName("Other Job"));
        Job job3 = jobDAO.save(new Job().setSite(site)
                .setName("Job 3"));
        Job job4 = jobDAO.save(new Job().setSite(site)
                .setName("project 1")
                .setDisplayName("Another Job 4"));

        List<Job> jobs = jobDAO.search("Job");
        assertThat(jobs).hasSize(4)
                .areExactly(1, equalTo(job1))
                .areExactly(1, equalTo(job2))
                .areExactly(1, equalTo(job3))
                .areExactly(1, equalTo(job4));

        jobs = jobDAO.search("JOB");
        assertThat(jobs).hasSize(4)
                .areExactly(1, equalTo(job1))
                .areExactly(1, equalTo(job2))
                .areExactly(1, equalTo(job3))
                .areExactly(1, equalTo(job4));

        jobs = jobDAO.search("job");
        assertThat(jobs).hasSize(4)
                .areExactly(1, equalTo(job1))
                .areExactly(1, equalTo(job2))
                .areExactly(1, equalTo(job3))
                .areExactly(1, equalTo(job4));
    }

    @Test
    void testSearch_Site()
    {
        Site other = siteDAO.save(new Site().setName("Other site for " + getTestMethodName())
                .setSharedSecret("my-super-other-secret")
                .setType(SiteType.JENKINS)
                .setRpcUrl(URI.create("http://localhost:8181/")));
        Job job1 = jobDAO.save(new Job().setSite(other)
                .setName("Job 1"));
        Job job2 = jobDAO.save(new Job().setSite(other)
                .setName("Other Job"));
        Job job3 = jobDAO.save(new Job().setSite(site)
                .setName("Job 3"));
        Job job4 = jobDAO.save(new Job().setSite(site)
                .setName("project 1")
                .setDisplayName("Another Job 4"));

        List<Job> jobs = jobDAO.search(site.getId(), "Job");
        assertThat(jobs).hasSize(2)
                .areExactly(1, equalTo(job3))
                .areExactly(1, equalTo(job4));

        jobs = jobDAO.search(site.getId(), "JOB");
        assertThat(jobs).hasSize(2)
                .areExactly(1, equalTo(job3))
                .areExactly(1, equalTo(job4));

        jobs = jobDAO.search(site.getId(), "job");
        assertThat(jobs).hasSize(2)
                .areExactly(1, equalTo(job3))
                .areExactly(1, equalTo(job4));
    }

    @Test
    void testFindByHash()
    {
        Site additionalSite = siteDAO.save(new Site().setName("Additional Site for " + getTestMethodName())
                .setSharedSecret("my-super-secret")
                .setType(SiteType.JENKINS)
                .setRpcUrl(URI.create("http://localhost:8081/")));
        Job job1 = jobDAO.save(new Job().setSite(site)
                .setName("Job 1"));
        Job job2 = jobDAO.save(new Job().setSite(site)
                .setName("Other Job")
                .setUrlName("Job 1"));
        Job job3 = jobDAO.save(new Job().setSite(additionalSite)
                .setName("Job 1"));
        Job job4 = jobDAO.save(new Job().setSite(additionalSite)
                .setName("Another Job 4"));

        List<Job> jobs = jobDAO.findByHash(JobHash.hash("Job 1", null));
        assertThat(jobs).hasSize(3)
                .areExactly(1, equalTo(job1))
                .areExactly(1, equalTo(job2))
                .areExactly(1, equalTo(job3));

        jobs = jobDAO.findByHash(site.getId(), JobHash.hash("Job 1", null));
        assertThat(jobs).hasSize(2)
                .areExactly(1, equalTo(job1))
                .areExactly(1, equalTo(job2));
    }

    @Test
    void testCleanupDeletedJobsWithoutBuilds()
    {
        Job buildLessDeletedJob = jobDAO.save(new Job().setSite(site)
                .setName("Buildless Deleted Job")
                .setLastBuild(10)
                .setDeleted(true));
        Job deletedJob = jobDAO.save(new Job().setSite(site)
                .setName("Deleted Job")
                .setLastBuild(5)
                .setDeleted(true));
        buildDAO.save(new Build().setJob(deletedJob)
                .setNumber(5)
                .setTimestamp(System.currentTimeMillis())
                .setResult(SUCCESS));
        Job job = jobDAO.save(new Job().setSite(site)
                .setName("Job 3")
                .setLastBuild(5));

        jobDAO.cleanupDeletedJobsWithoutBuilds();

        assertThat(jobDAO.get(buildLessDeletedJob.getId())).isNull();
        assertThat(jobDAO.get(deletedJob.getId())).isNotNull();
        assertThat(jobDAO.get(job.getId())).isNotNull();
    }

    @Test
    void testCleanupOldDeletedJobs()
    {
        Instant timestamp = Instant.now()
                .minus(10, DAYS);

        Job job1 = jobDAO.save(new Job().setSite(site)
                .setName("Job 1")
                .setLastBuild(10)
                .setDeleted(true));
        buildDAO.save(new Build().setJob(job1)
                .setNumber(10)
                .setTimestamp(timestamp.toEpochMilli())
                .setResult(SUCCESS)
                .setTestResults(new TestResults().setFailed(1)
                        .setSkipped(1)
                        .setTotal(10)));
        Job job2 = jobDAO.save(new Job().setSite(site)
                .setName("Job 2")
                .setLastBuild(5)
                .setDeleted(true));
        buildDAO.save(new Build().setJob(job2)
                .setNumber(5)
                .setTimestamp(timestamp.plus(5, DAYS)
                        .toEpochMilli())
                .setResult(SUCCESS));
        Job job3 = jobDAO.save(new Job().setSite(site)
                .setName("Job 3")
                .setLastBuild(5));
        buildDAO.save(new Build().setJob(job3)
                .setNumber(5)
                .setDeleted(true)
                .setTimestamp(timestamp.toEpochMilli())
                .setResult(SUCCESS));

        jobDAO.cleanupOldDeletedJobs(timestamp.plus(5, DAYS));

        assertThat(jobDAO.getBySiteIdAtLevel(site.getId(), null, true)).hasSize(2)
                .extracting(Job::getId)
                .containsOnly(job2.getId(), job3.getId());
        assertThat(jobDAO.get(job1.getId())).isNull();
        assertThat(buildDAO.getAllByJob(job1.getId())).isEmpty();
    }
}
