/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.synchronization.api.trigger;

import java.util.stream.Stream;
import jakarta.json.bind.JsonbException;

import org.marvelution.jji.model.OperationId;
import org.marvelution.testing.TestSupport;

import org.assertj.core.api.InstanceOfAssertFactories;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.marvelution.jji.data.synchronization.api.trigger.TriggerReasons.asJson;
import static org.marvelution.jji.data.synchronization.api.trigger.TriggerReasons.fromJson;

class TriggerReasonsTest
        extends TestSupport
{

    @ParameterizedTest()
    @MethodSource("toAndFromJson")
    void testToAndFromJson(
            TriggerReason reason,
            String json)
    {
        assertThat(asJson(reason)).isEqualTo(json);
        assertThat(fromJson(json)).isEqualTo(reason);
    }

    @Test
    void testFromJson_MissingTypeField()
    {
        assertThatThrownBy(() -> fromJson("{\"userId\":\"admin\"}")).isInstanceOf(JsonbException.class)
                .hasMessage("cannot deserialize trigger reason to specific type specified");
    }

    @Test
    void testFromJson_ClassNotFoundException()
    {
        assertThatThrownBy(() -> fromJson(
                "{\"userId\":\"admin\",\"__type__\":\"org.marvelution.jji.data.synchronization.api.trigger.UnknownTrigger\"}")).isInstanceOf(
                        JsonbException.class)
                .hasMessage("cannot load trigger reason type org.marvelution.jji.data.synchronization.api.trigger.UnknownTrigger");
    }

    @Test
    void testFromJson_ClassCastException()
    {
        assertThatThrownBy(() -> fromJson("{\"userId\":\"admin\",\"__type__\":\"java.lang.String\"}")).isInstanceOf(JsonbException.class);
    }

    @Test
    void testFromJson_WithData()
    {
        TriggerReason reason = new ManualSynchronizationTrigger("test");
        reason.setData("{\"name\":\"Name\",\"url\":\"http://localhost:8080/job/Test\"}");

        String json = asJson(reason);
        assertThat(json).isEqualTo(
                "{\"data\":\"{\\\"name\\\":\\\"Name\\\",\\\"url\\\":\\\"http://localhost:8080/job/Test\\\"}\",\"userId\":\"test\"," +
                "\"__type__\":\"org.marvelution.jji.data.synchronization.api.trigger.ManualSynchronizationTrigger\"}");

        TriggerReason actual = fromJson(json);
        assertThat(actual).isInstanceOf(ManualSynchronizationTrigger.class)
                .asInstanceOf(InstanceOfAssertFactories.type(ManualSynchronizationTrigger.class))
                .extracting(ManualSynchronizationTrigger::getUserId, ManualSynchronizationTrigger::getData)
                .containsOnly("test", "{\"name\":\"Name\",\"url\":\"http://localhost:8080/job/Test\"}");
    }

    public static Stream<Arguments> toAndFromJson()
    {
        return Stream.of(
                //
                Arguments.of(new BuildCompletionTrigger("jobhash", 1).withData("{\"name\":\"Name\"}"),
                        "{\"data\":\"{\\\"name\\\":\\\"Name\\\"}\",\"buildNumber\":1,\"jobHash\":\"jobhash\"," +
                        "\"__type__\":\"org.marvelution.jji.data.synchronization.api.trigger.BuildCompletionTrigger\"}"),
                //
                Arguments.of(new BulkRequestTrigger().withData("{\"name\":\"Name\"}"),
                        "{\"data\":\"{\\\"name\\\":\\\"Name\\\"}\"," +
                        "\"__type__\":\"org.marvelution.jji.data.synchronization.api.trigger.BulkRequestTrigger\"}"),
                //
                Arguments.of(new JobSyncTrigger("jobhash").withData("{\"name\":\"Name\"}"),
                        "{\"data\":\"{\\\"name\\\":\\\"Name\\\"}\",\"jobHash\":\"jobhash\"," +
                        "\"__type__\":\"org.marvelution.jji.data.synchronization.api.trigger.JobSyncTrigger\"}"),
                //
                Arguments.of(new ManualSynchronizationTrigger("admin").withData("{\"name\":\"Name\"}"),
                        "{\"data\":\"{\\\"name\\\":\\\"Name\\\"}\",\"userId\":\"admin\"," +
                        "\"__type__\":\"org.marvelution.jji.data.synchronization.api.trigger.ManualSynchronizationTrigger\"}"),
                //
                Arguments.of(new ResetBuildCacheTrigger("admin").withData("{\"name\":\"Name\"}"),
                        "{\"data\":\"{\\\"name\\\":\\\"Name\\\"}\",\"userId\":\"admin\"," +
                        "\"__type__\":\"org.marvelution.jji.data.synchronization.api.trigger.ResetBuildCacheTrigger\"}"),
                //
                Arguments.of(new SiteConnectedTrigger("site-id", "admin").withData("{\"name\":\"Name\"}"),
                        "{\"data\":\"{\\\"name\\\":\\\"Name\\\"}\",\"userId\":\"admin\",\"siteId\":\"site-id\"," +
                        "\"__type__\":\"org.marvelution.jji.data.synchronization.api.trigger.SiteConnectedTrigger\"}"),
                //
                Arguments.of(new SiteUpdatedTrigger("site-id", "admin").withData("{\"name\":\"Name\"}"),
                        "{\"data\":\"{\\\"name\\\":\\\"Name\\\"}\",\"userId\":\"admin\",\"siteId\":\"site-id\"," +
                        "\"__type__\":\"org.marvelution.jji.data.synchronization.api.trigger.SiteUpdatedTrigger\"}"),
                //
                Arguments.of(new UpstreamTrigger("test-id", OperationId.of("test-id-111")).withData("{\"name\":\"Name\"}"),
                        "{\"data\":\"{\\\"name\\\":\\\"Name\\\"}\",\"operationId\":{\"id\":\"test-id-111\",\"prefix\":\"test-id\"}," +
                        "\"resultId\":\"test-id\"," +
                        "\"__type__\":\"org.marvelution.jji.data.synchronization.api.trigger.UpstreamTrigger\"}"));
    }
}
