/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services;

import java.util.Optional;

import org.marvelution.jji.data.services.api.JobService;
import org.marvelution.jji.data.services.api.SiteService;
import org.marvelution.jji.data.synchronization.api.SynchronizationService;
import org.marvelution.jji.data.synchronization.api.trigger.BulkRequestTrigger;
import org.marvelution.jji.model.Job;
import org.marvelution.jji.model.request.BulkRequest;
import org.marvelution.testing.TestSupport;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InOrder;
import org.mockito.Mock;

import static java.util.Arrays.asList;
import static org.marvelution.jji.model.request.BulkRequest.Operation.*;
import static org.mockito.Mockito.*;

class BulkRequestServiceTest
        extends TestSupport
{

    @Mock
    private SiteService siteService;
    @Mock
    private JobService jobService;
    @Mock
    private SynchronizationService synchronizationService;
    private BulkRequestService helper;

    @BeforeEach
    void setUp()
    {
        helper = new BulkRequestService(siteService, jobService, synchronizationService, Runnable::run);
    }

    @Test
    void testOrderedProcessingOfOperations()
    {
        Job job1 = new Job().setId("job-1")
                .setName("Job 1");
        Job job2 = new Job().setId("job-2")
                .setName("Job 2");
        doReturn(Optional.of(job1)).when(jobService)
                .get(job1.getId());
        doReturn(Optional.of(job2)).when(jobService)
                .get(job2.getId());

        BulkRequest request = new BulkRequest().setJobIds(asList(job1.getId(), job2.getId()))
                .setOperations(asList(ENABLE_JOB, DISABLE_JOB, SYNCHRONIZE, REBUILD_CACHE, CLEAR_CACHE));

        helper.process(request);

        InOrder ordered = inOrder(jobService, synchronizationService);
        ordered.verify(jobService)
                .get(job1.getId());
        ordered.verify(jobService)
                .enable(job1, true);
        ordered.verify(jobService)
                .enable(job1, false);
        ordered.verify(jobService)
                .deleteAllBuilds(job1);
        ordered.verify(jobService)
                .resetBuildCache(job1);
        ordered.verify(synchronizationService)
                .synchronize(eq(job1), any(BulkRequestTrigger.class));
        ordered.verify(jobService)
                .get(job2.getId());
        ordered.verify(jobService)
                .enable(job2, true);
        ordered.verify(jobService)
                .enable(job2, false);
        ordered.verify(jobService)
                .deleteAllBuilds(job2);
        ordered.verify(jobService)
                .resetBuildCache(job2);
        ordered.verify(synchronizationService)
                .synchronize(eq(job2), any(BulkRequestTrigger.class));
    }
}
