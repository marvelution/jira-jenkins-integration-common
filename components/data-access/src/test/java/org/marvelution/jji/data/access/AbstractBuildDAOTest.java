/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access;

import org.marvelution.jji.model.*;

import java.net.URI;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Set;
import java.util.function.UnaryOperator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static java.util.Collections.*;
import static java.util.stream.Collectors.toList;
import static java.util.stream.IntStream.range;
import static org.assertj.core.api.Assertions.assertThat;
import static org.marvelution.jji.model.DeploymentEnvironmentType.TESTING;
import static org.marvelution.jji.model.Matchers.equalTo;

public abstract class AbstractBuildDAOTest
        extends AbstractDAOTest
{

    protected Job job;

    @BeforeEach
    void setUp()
    {
        Site site = siteDAO.save(new Site().setName(getTestMethodName())
                .setSharedSecret("my-super-secret")
                .setType(SiteType.JENKINS)
                .setRpcUrl(URI.create("http://localhost:8080")));
        job = jobDAO.save(new Job().setSite(site)
                .setName(getTestMethodName())
                .setLastBuild(1));
    }

    @Test
    void testCRUD()
    {
        // Create
        ChangeSet changeSet = new ChangeSet().setCommitId("commit-1")
                .setMessage("message");
        DeploymentEnvironment testingEnv = new DeploymentEnvironment().setId("test-1")
                .setName("Testing")
                .setType(TESTING);
        Build build = new Build().setJob(job)
                .setNumber(1)
                .setResult(Result.SUCCESS)
                .setBranches(singleton("master"))
                .setChangeSet(singletonList(changeSet))
                .setDeploymentEnvironments(singletonList(testingEnv))
                .setTestResults(new TestResults().setTotal(10)
                        .setSkipped(2)
                        .setFailed(3));
        Build inserted = buildDAO.save(build);
        assertThat(inserted).is(equalTo(build.copy()
                        .setBranches(emptySet())
                        .setChangeSet(emptyList()), true))
                .extracting(Build::getId)
                .isNotNull();

        // Read
        assertThat(buildDAO.get(inserted.getId())).is(equalTo(inserted));
        assertThat(buildDAO.get(job.getId(), inserted.getNumber())).is(equalTo(inserted));

        // Update
        inserted.setTestResults(new TestResults().setFailed(1)
                        .setSkipped(1)
                        .setTotal(4))
                .setResult(Result.FAILURE);
        inserted.getChangeSet()
                .add(new ChangeSet().setCommitId("commit-2")
                        .setMessage("second message"));
        inserted.getBranches()
                .add("origin/master");
        Build updated = buildDAO.save(inserted);
        assertThat(updated).is(equalTo(inserted.copy()
                .setChangeSet(emptyList())
                .setBranches(emptySet())));

        // Delete
        buildDAO.delete(inserted.getId());
        assertThat(buildDAO.get(inserted.getId())).isNull();
        assertThat(buildDAO.get(job.getId(), inserted.getNumber())).isNull();

        // Verify that the deployment environment is not deleted
        Set<DeploymentEnvironment> deploymentEnvironments = buildDAO.getDeploymentEnvironments();
        assertThat(deploymentEnvironments).hasSize(1)
                .containsOnly(testingEnv);
    }

    @Test
    void testGetAllInRange()
    {
        List<Build> builds = createBuilds();

        Set<Build> allByJob = buildDAO.getAllByJob(job.getId());
        assertThat(allByJob).hasSize(10)
                .contains(builds.toArray(new Build[0]));

        Set<Build> allInRange = buildDAO.getAllInRange(job.getId(), 1, -1);
        assertThat(allInRange).hasSize(10)
                .contains(builds.toArray(new Build[0]));

        allInRange = buildDAO.getAllInRange(job.getId(), 4, 8);
        assertThat(allInRange).hasSize(5)
                .contains(builds.stream()
                        .filter(build -> build.getNumber() >= 4)
                        .filter(build -> build.getNumber() <= 8)
                        .toArray(Build[]::new));
    }

    @Test
    void testGetDeleteAllByJob()
    {
        Build[] builds = createBuilds().toArray(new Build[0]);

        Set<Build> allByJob = buildDAO.getAllByJob(job.getId());
        assertThat(allByJob).hasSize(10)
                .contains(builds);

        buildDAO.deleteAllByJob(job.getId());
        allByJob = buildDAO.getAllByJob(job.getId());
        assertThat(allByJob).hasSize(0);
    }

    @Test
    void testMarkAsDeleted()
    {
        Build build = buildDAO.save(new Build().setJob(job)
                .setNumber(1)
                .setResult(Result.SUCCESS));
        assertThat(buildDAO.get(build.getId())).is(equalTo(build));
        buildDAO.markAsDeleted(build.getId());
        Build marked = buildDAO.get(build.getId());
        assertThat(marked).extracting(Build::isDeleted)
                .isEqualTo(true);
    }

    @Test
    void testMarkAllInJobAsDelete()
    {
        createBuilds();

        buildDAO.markAllInJobAsDeleted(job.getId(), 5);
        for (Build build : buildDAO.getAllByJob(job.getId()))
        {
            assertThat(build.isDeleted()).isEqualTo(build.getNumber() < 5);
        }
        buildDAO.markAllInJobAsDeleted(job.getId());
        for (Build build : buildDAO.getAllByJob(job.getId()))
        {
            assertThat(build.isDeleted()).isTrue();
        }
    }

    @Test
    void testCleanupOldDeletedBuilds()
    {
        Instant timestamp = Instant.now()
                .minus(15, ChronoUnit.DAYS);
        createBuilds(build -> build.setDeleted(true)
                .setTimestamp(timestamp.plus(build.getNumber(), ChronoUnit.DAYS)
                        .toEpochMilli()));

        buildDAO.cleanupOldDeletedBuilds(timestamp.plus(6, ChronoUnit.DAYS));

        assertThat(buildDAO.getAllByJob(job.getId())).hasSize(5)
                .extracting(Build::getNumber)
                .containsOnly(6, 7, 8, 9, 10);
    }

    private List<Build> createBuilds()
    {
        return createBuilds(UnaryOperator.identity());
    }

    private List<Build> createBuilds(UnaryOperator<Build> customizer)
    {
        return range(1, 11).mapToObj(number -> new Build().setJob(job)
                        .setNumber(number)
                        .setResult(Result.SUCCESS))
                .map(customizer)
                .map(buildDAO::save)
                .collect(toList());
    }
}
