/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services.api;

import java.util.stream.Stream;

import org.marvelution.jji.model.Version;
import org.marvelution.testing.TestSupport;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import static org.assertj.core.api.Assertions.assertThat;

class VersionTest
        extends TestSupport
{

    @Test
    void testEqual()
    {
        assertThat(Version.of("1.0.0")).isEqualTo(Version.of("1.0.0."));
    }

    @ParameterizedTest
    @MethodSource("compareToTests")
    void testCompareTo(
            Version left,
            Version right,
            int expectedResult)
    {
        assertThat(left.compareTo(right)).isEqualTo(expectedResult);
    }

    @ParameterizedTest
    @MethodSource("isNewerTests")
    void testIsNewer(
            Version older,
            Version newer,
            boolean expectedResult)
    {
        assertThat(newer.isNewer(older)).isEqualTo(expectedResult);
    }

    @ParameterizedTest
    @MethodSource("isOlderTests")
    void testIsOlder(
            Version older,
            Version newer,
            boolean expectedResult)
    {
        assertThat(older.isOlder(newer)).isEqualTo(expectedResult);
    }

    static Stream<Arguments> compareToTests()
    {
        return Stream.of(Arguments.of(Version.of("1.0.0"), Version.of("1.0.0"), 0),
                Arguments.of(Version.of("1.0.0"), Version.of("1.0.1"), -1),
                Arguments.of(Version.of("1.0.0"), Version.of("1.1.0"), -1),
                Arguments.of(Version.of("1.0.0"), Version.of("2.0.0"), -1),
                Arguments.of(Version.of("1.0.0"), Version.of("0.0.0"), 1),
                Arguments.of(Version.of("1.0.0"), Version.of("0.0.1"), 1),
                Arguments.of(Version.of("1.0.0"), Version.of("0.1.0"), 1));
    }

    static Stream<Arguments> isNewerTests()
    {
        return Stream.of(Arguments.of(Version.of("1.0.0"), Version.of("1.0.0"), false),
                Arguments.of(Version.of("1.0.0"), Version.of("1.0.1"), true),
                Arguments.of(Version.of("1.0.0"), Version.of("1.1.0"), true),
                Arguments.of(Version.of("1.0.0"), Version.of("2.0.0"), true),
                Arguments.of(Version.of("1.0.0"), Version.of("0.0.0"), false),
                Arguments.of(Version.of("1.0.0"), Version.of("0.0.1"), false),
                Arguments.of(Version.of("1.0.0"), Version.of("0.1.0"), false));
    }

    static Stream<Arguments> isOlderTests()
    {
        return Stream.of(Arguments.of(Version.of("1.0.0"), Version.of("1.0.0"), false),
                Arguments.of(Version.of("1.0.0"), Version.of("1.0.1"), true),
                Arguments.of(Version.of("1.0.0"), Version.of("1.1.0"), true),
                Arguments.of(Version.of("1.0.0"), Version.of("2.0.0"), true),
                Arguments.of(Version.of("1.0.0"), Version.of("0.0.0"), false),
                Arguments.of(Version.of("1.0.0"), Version.of("0.0.1"), false),
                Arguments.of(Version.of("1.0.0"), Version.of("0.1.0"), false));
    }
}
