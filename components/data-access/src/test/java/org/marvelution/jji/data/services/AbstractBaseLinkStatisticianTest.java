/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services;

import java.util.*;
import java.util.stream.*;

import org.marvelution.jji.data.access.api.*;
import org.marvelution.jji.data.services.api.*;
import org.marvelution.jji.model.*;

import jakarta.inject.*;
import org.junit.jupiter.api.*;

import static org.assertj.core.api.Assertions.*;
import static org.marvelution.jji.model.Result.*;

public abstract class AbstractBaseLinkStatisticianTest<L extends BaseLinkStatistician>
        extends AbstractBaseServiceTest
{

    @Inject
    @javax.inject.Inject
    protected LinkStatisticsDAO linkStatisticsDAO;
    @Inject
    @javax.inject.Inject
    protected L linkStatistician;
    protected Job job;

    @BeforeEach
    void createDefaultJob()
    {
        job = setupJob(createJob(createSite()));
    }

    protected abstract Job setupJob(Job job);

    protected abstract Build setupBuild(
            Build build,
            IssueReference... issueLinks);

    protected abstract IssueReference[] setupIssueReference(String... issueKeys);

    protected abstract void setupLink(
            Build build,
            IssueReference... issueLinks);

    protected void calculateStats(
            boolean reindexIssue,
            String... issueKeys)
    {
        linkStatistician.calculate(new LinkStatistician.Request().setIssueKeys(Stream.of(issueKeys)
                        .collect(Collectors.toSet()))
                .setReindexIssue(reindexIssue));
    }

    @Test
    void testGetLinkStatistics()
    {
        IssueReference[] issueReferences = setupIssueReference("PR-1");

        Build build1 = setupBuild(createBuild(job).setResult(SUCCESS));

        LinkStatistics statistics = linkStatisticsDAO.getLinkStatistics("PR-1");
        assertThat(statistics).extracting(LinkStatistics::getTotal,
                        LinkStatistics::getBuildTotal,
                        LinkStatistics::getWorstResult,
                        LinkStatistics::getLatestBuild,
                        LinkStatistics::getWorstBuild,
                        LinkStatistics::getDeployTotal,
                        LinkStatistics::getWorstDeployResult,
                        LinkStatistics::getLatestDeploy,
                        LinkStatistics::getWorstDeploy)
                .containsOnly(0L, SUCCESS, null, null, 0L, SUCCESS, null, null);

        setupLink(build1, issueReferences);

        calculateStats(true, "PR-1");

        statistics = linkStatisticsDAO.getLinkStatistics("PR-1");
        assertThat(statistics).extracting(LinkStatistics::getTotal,
                        LinkStatistics::getBuildTotal,
                        LinkStatistics::getWorstResult,
                        LinkStatistics::getLatestBuild,
                        LinkStatistics::getWorstBuild,
                        LinkStatistics::getDeployTotal,
                        LinkStatistics::getWorstDeployResult,
                        LinkStatistics::getLatestDeploy,
                        LinkStatistics::getWorstDeploy)
                .containsOnly(1L,
                        1L,
                        SUCCESS,
                        LinkStatistics.BuildStats.forBuild(build1),
                        LinkStatistics.BuildStats.forBuild(build1),
                        0L,
                        SUCCESS,
                        null,
                        null);
        assertThat(statistics.getCountByResult()).containsOnly(entry(SUCCESS, 1L),
                entry(UNSTABLE, 0L),
                entry(FAILURE, 0L),
                entry(NOT_BUILT, 0L),
                entry(ABORTED, 0L),
                entry(UNKNOWN, 0L));

        Build build2 = setupBuild(createBuild(job, 2).setResult(UNSTABLE));
        setupLink(build2, issueReferences);

        calculateStats(true, "PR-1");

        statistics = linkStatisticsDAO.getLinkStatistics("PR-1");
        assertThat(statistics).extracting(LinkStatistics::getTotal,
                        LinkStatistics::getBuildTotal,
                        LinkStatistics::getWorstResult,
                        LinkStatistics::getLatestBuild,
                        LinkStatistics::getWorstBuild,
                        LinkStatistics::getDeployTotal,
                        LinkStatistics::getWorstDeployResult,
                        LinkStatistics::getLatestDeploy,
                        LinkStatistics::getWorstDeploy)
                .containsOnly(1L,
                        2L,
                        UNSTABLE,
                        LinkStatistics.BuildStats.forBuild(build2),
                        LinkStatistics.BuildStats.forBuild(build2),
                        0L,
                        SUCCESS,
                        null,
                        null);
        assertThat(statistics.getCountByResult()).containsOnly(entry(UNSTABLE, 1L),
                entry(SUCCESS, 1L),
                entry(FAILURE, 0L),
                entry(NOT_BUILT, 0L),
                entry(ABORTED, 0L),
                entry(UNKNOWN, 0L));

        Build build3 = setupBuild(createBuild(job, 3).setResult(FAILURE));
        setupLink(build3, issueReferences);

        calculateStats(true, "PR-1");

        statistics = linkStatisticsDAO.getLinkStatistics("PR-1");
        assertThat(statistics).extracting(LinkStatistics::getTotal,
                        LinkStatistics::getBuildTotal,
                        LinkStatistics::getWorstResult,
                        LinkStatistics::getLatestBuild,
                        LinkStatistics::getWorstBuild,
                        LinkStatistics::getDeployTotal,
                        LinkStatistics::getWorstDeployResult,
                        LinkStatistics::getLatestDeploy,
                        LinkStatistics::getWorstDeploy)
                .containsOnly(1L,
                        3L,
                        FAILURE,
                        LinkStatistics.BuildStats.forBuild(build3),
                        LinkStatistics.BuildStats.forBuild(build3),
                        0L,
                        SUCCESS,
                        null,
                        null);
        assertThat(statistics.getCountByResult()).containsOnly(entry(FAILURE, 1L),
                entry(UNSTABLE, 1L),
                entry(SUCCESS, 1L),
                entry(NOT_BUILT, 0L),
                entry(ABORTED, 0L),
                entry(UNKNOWN, 0L));

        Build build4 = setupBuild(createBuild(job, 4).setResult(NOT_BUILT));
        setupLink(build4, issueReferences);
        Build build5 = setupBuild(createBuild(job, 5).setResult(ABORTED));
        setupLink(build5, issueReferences);
        Build build6 = setupBuild(createBuild(job, 6).setResult(UNKNOWN));
        setupLink(build6, issueReferences);

        calculateStats(true, "PR-1");

        statistics = linkStatisticsDAO.getLinkStatistics("PR-1");
        assertThat(statistics).extracting(LinkStatistics::getTotal,
                        LinkStatistics::getBuildTotal,
                        LinkStatistics::getWorstResult,
                        LinkStatistics::getLatestBuild,
                        LinkStatistics::getWorstBuild,
                        LinkStatistics::getDeployTotal,
                        LinkStatistics::getWorstDeployResult,
                        LinkStatistics::getLatestDeploy,
                        LinkStatistics::getWorstDeploy)
                .containsOnly(1L,
                        6L,
                        UNKNOWN,
                        LinkStatistics.BuildStats.forBuild(build6),
                        LinkStatistics.BuildStats.forBuild(build6),
                        0L,
                        SUCCESS,
                        null,
                        null);
        assertThat(statistics.getCountByResult()).containsOnly(entry(UNKNOWN, 1L),
                entry(UNSTABLE, 1L),
                entry(FAILURE, 1L),
                entry(NOT_BUILT, 1L),
                entry(ABORTED, 1L),
                entry(SUCCESS, 1L));

        Build build7 = setupBuild(createBuild(job, 7).setResult(SUCCESS));
        setupLink(build7, issueReferences);

        calculateStats(true, "PR-1");

        statistics = linkStatisticsDAO.getLinkStatistics("PR-1");
        assertThat(statistics).extracting(LinkStatistics::getTotal,
                        LinkStatistics::getBuildTotal,
                        LinkStatistics::getWorstResult,
                        LinkStatistics::getLatestBuild,
                        LinkStatistics::getWorstBuild,
                        LinkStatistics::getDeployTotal,
                        LinkStatistics::getWorstDeployResult,
                        LinkStatistics::getLatestDeploy,
                        LinkStatistics::getWorstDeploy)
                .containsOnly(1L,
                        7L,
                        SUCCESS,
                        LinkStatistics.BuildStats.forBuild(build7),
                        LinkStatistics.BuildStats.forBuild(build7),
                        0L,
                        SUCCESS,
                        null,
                        null);
        assertThat(statistics.getCountByResult()).containsOnly(entry(SUCCESS, 2L),
                entry(UNSTABLE, 1L),
                entry(FAILURE, 1L),
                entry(NOT_BUILT, 1L),
                entry(ABORTED, 1L),
                entry(UNKNOWN, 1L));
    }

    @Test
    void testGetLinkStatistics_MultipleJobs()
    {
        IssueReference[] issueReferences = setupIssueReference("PR-1");

        Build build1 = setupBuild(createBuild(job).setResult(FAILURE));
        setupLink(build1, issueReferences);

        calculateStats(true, "PR-1");

        LinkStatistics statistics = linkStatisticsDAO.getLinkStatistics("PR-1");
        assertThat(statistics).extracting(LinkStatistics::getTotal,
                        LinkStatistics::getBuildTotal,
                        LinkStatistics::getWorstResult,
                        LinkStatistics::getLatestBuild,
                        LinkStatistics::getWorstBuild)
                .containsOnly(1L, 1L, FAILURE, LinkStatistics.BuildStats.forBuild(build1), LinkStatistics.BuildStats.forBuild(build1));
        assertThat(statistics.getCountByResult()).containsOnly(entry(SUCCESS, 0L),
                entry(UNSTABLE, 0L),
                entry(FAILURE, 1L),
                entry(NOT_BUILT, 0L),
                entry(ABORTED, 0L),
                entry(UNKNOWN, 0L));

        Job job1 = setupJob(createJob(job.getSite(), Optional.of("1")));
        Build build2 = setupBuild(createBuild(job1).setResult(SUCCESS));
        setupLink(build2, issueReferences);

        calculateStats(true, "PR-1");

        statistics = linkStatisticsDAO.getLinkStatistics("PR-1");
        assertThat(statistics).extracting(LinkStatistics::getTotal,
                        LinkStatistics::getBuildTotal,
                        LinkStatistics::getWorstResult,
                        LinkStatistics::getLatestBuild,
                        LinkStatistics::getWorstBuild)
                .containsOnly(2L, 2L, FAILURE, LinkStatistics.BuildStats.forBuild(build2), LinkStatistics.BuildStats.forBuild(build1));
        assertThat(statistics.getCountByResult()).containsOnly(entry(SUCCESS, 1L),
                entry(UNSTABLE, 0L),
                entry(FAILURE, 1L),
                entry(NOT_BUILT, 0L),
                entry(ABORTED, 0L),
                entry(UNKNOWN, 0L));

        Job job2 = setupJob(createJob(job.getSite(), Optional.of("2")));
        Build build3 = setupBuild(createBuild(job2).setResult(UNSTABLE));
        setupLink(build3, issueReferences);

        calculateStats(true, "PR-1");

        statistics = linkStatisticsDAO.getLinkStatistics("PR-1");
        assertThat(statistics).extracting(LinkStatistics::getTotal,
                        LinkStatistics::getBuildTotal,
                        LinkStatistics::getWorstResult,
                        LinkStatistics::getLatestBuild,
                        LinkStatistics::getWorstBuild)
                .containsOnly(3L, 3L, FAILURE, LinkStatistics.BuildStats.forBuild(build3), LinkStatistics.BuildStats.forBuild(build1));
        assertThat(statistics.getCountByResult()).containsOnly(entry(SUCCESS, 1L),
                entry(UNSTABLE, 1L),
                entry(FAILURE, 1L),
                entry(NOT_BUILT, 0L),
                entry(ABORTED, 0L),
                entry(UNKNOWN, 0L));
    }

    @Test
    void testGetLinkStatistics_Deployments()
    {
        DeploymentEnvironment stagingEnvironment = new DeploymentEnvironment().setId("staging")
                .setName("Staging")
                .setType(DeploymentEnvironmentType.STAGING);
        DeploymentEnvironment productionEnvironment = new DeploymentEnvironment().setId("production")
                .setName("Production")
                .setType(DeploymentEnvironmentType.PRODUCTION);

        IssueReference[] issueReferences = setupIssueReference("PR-1");

        Build build1 = setupBuild(createBuild(job, 1).setResult(FAILURE)
                .setDeploymentEnvironments(Collections.singletonList(stagingEnvironment)));
        setupLink(build1, issueReferences);

        calculateStats(true, "PR-1");

        LinkStatistics statistics = linkStatisticsDAO.getLinkStatistics("PR-1");
        assertThat(statistics).extracting(LinkStatistics::getTotal,
                        LinkStatistics::getBuildTotal,
                        LinkStatistics::getWorstResult,
                        LinkStatistics::getLatestBuild,
                        LinkStatistics::getWorstBuild,
                        LinkStatistics::getDeployTotal,
                        LinkStatistics::getWorstDeployResult,
                        LinkStatistics::getLatestDeploy,
                        LinkStatistics::getWorstDeploy)
                .containsOnly(1L,
                        1L,
                        FAILURE,
                        LinkStatistics.BuildStats.forBuild(build1),
                        LinkStatistics.BuildStats.forBuild(build1),
                        1L,
                        FAILURE,
                        LinkStatistics.DeployStats.forDeployment(build1, stagingEnvironment),
                        LinkStatistics.DeployStats.forDeployment(build1, stagingEnvironment));
        assertThat(statistics.getDeployCountByResult()).containsOnly(entry(SUCCESS, 0L),
                entry(UNSTABLE, 0L),
                entry(FAILURE, 1L),
                entry(NOT_BUILT, 0L),
                entry(ABORTED, 0L),
                entry(UNKNOWN, 0L));

        Build build2 = setupBuild(createBuild(job, 2).setResult(SUCCESS)
                .setDeploymentEnvironments(Collections.singletonList(stagingEnvironment)));
        setupLink(build2, issueReferences);

        calculateStats(true, "PR-1");

        statistics = linkStatisticsDAO.getLinkStatistics("PR-1");
        assertThat(statistics).extracting(LinkStatistics::getTotal,
                        LinkStatistics::getBuildTotal,
                        LinkStatistics::getWorstResult,
                        LinkStatistics::getLatestBuild,
                        LinkStatistics::getWorstBuild,
                        LinkStatistics::getDeployTotal,
                        LinkStatistics::getWorstDeployResult,
                        LinkStatistics::getLatestDeploy,
                        LinkStatistics::getWorstDeploy)
                .containsOnly(1L,
                        2L,
                        SUCCESS,
                        LinkStatistics.BuildStats.forBuild(build2),
                        LinkStatistics.BuildStats.forBuild(build2),
                        2L,
                        FAILURE,
                        LinkStatistics.DeployStats.forDeployment(build2, stagingEnvironment),
                        LinkStatistics.DeployStats.forDeployment(build1, stagingEnvironment));
        assertThat(statistics.getDeployCountByResult()).containsOnly(entry(SUCCESS, 1L),
                entry(UNSTABLE, 0L),
                entry(FAILURE, 1L),
                entry(NOT_BUILT, 0L),
                entry(ABORTED, 0L),
                entry(UNKNOWN, 0L));

        Build build3 = setupBuild(createBuild(job, 3).setResult(SUCCESS)
                .setDeploymentEnvironments(Collections.singletonList(productionEnvironment)));
        setupLink(build3, issueReferences);

        calculateStats(true, "PR-1");

        statistics = linkStatisticsDAO.getLinkStatistics("PR-1");
        assertThat(statistics).extracting(LinkStatistics::getTotal,
                        LinkStatistics::getBuildTotal,
                        LinkStatistics::getWorstResult,
                        LinkStatistics::getLatestBuild,
                        LinkStatistics::getWorstBuild,
                        LinkStatistics::getDeployTotal,
                        LinkStatistics::getWorstDeployResult,
                        LinkStatistics::getLatestDeploy,
                        LinkStatistics::getWorstDeploy)
                .containsOnly(1L,
                        3L,
                        SUCCESS,
                        LinkStatistics.BuildStats.forBuild(build3),
                        LinkStatistics.BuildStats.forBuild(build3),
                        3L,
                        FAILURE,
                        LinkStatistics.DeployStats.forDeployment(build3, productionEnvironment),
                        LinkStatistics.DeployStats.forDeployment(build1, stagingEnvironment));
        assertThat(statistics.getDeployCountByResult()).containsOnly(entry(SUCCESS, 2L),
                entry(UNSTABLE, 0L),
                entry(FAILURE, 1L),
                entry(NOT_BUILT, 0L),
                entry(ABORTED, 0L),
                entry(UNKNOWN, 0L));
    }
}
