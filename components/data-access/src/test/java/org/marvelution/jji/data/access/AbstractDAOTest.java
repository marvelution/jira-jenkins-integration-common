/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access;

import org.marvelution.jji.data.access.api.*;
import org.marvelution.jji.model.*;
import org.marvelution.testing.*;

import jakarta.inject.*;
import org.junit.jupiter.api.*;

/**
 * @author Mark Rekveld
 * @since 1.0.0
 */
abstract class AbstractDAOTest
        extends TestSupport
{

    @Inject
    @javax.inject.Inject
    protected SiteDAO siteDAO;
    @Inject
    @javax.inject.Inject
    protected JobDAO jobDAO;
    @Inject
    @javax.inject.Inject
    protected BuildDAO buildDAO;
    @Inject
    @javax.inject.Inject
    protected IssueToBuildDAO issueToBuildDAO;

    @AfterEach
    public void deleteAllSites()
    {
        siteDAO.getAll()
                .stream()
                .map(Site::getId)
                .forEach(siteDAO::delete);
    }
}
