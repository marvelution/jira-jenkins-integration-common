/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;
import jakarta.json.JsonStructure;

import org.marvelution.jji.data.services.api.*;
import org.marvelution.jji.data.synchronization.api.SynchronizationService;
import org.marvelution.jji.data.synchronization.api.exceptions.SynchronizationTriggerException;
import org.marvelution.jji.data.synchronization.api.trigger.BuildCompletionTrigger;
import org.marvelution.jji.data.synchronization.api.trigger.JobSyncTrigger;
import org.marvelution.jji.model.Build;
import org.marvelution.jji.model.IssueReference;
import org.marvelution.jji.model.Job;
import org.marvelution.jji.model.Site;
import org.marvelution.jji.utils.JobHash;
import org.marvelution.testing.TestSupport;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.stream.Collectors.toSet;
import static org.apache.commons.codec.digest.DigestUtils.sha1Hex;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.*;

class IntegrationApiServiceTest
        extends TestSupport
{

    @Mock
    private ConfigurationService configurationService;
    @Mock
    private SiteService siteService;
    @Mock
    private JobService jobService;
    @Mock
    private BuildService buildService;
    @Mock
    private IssueLinkService issueLinkService;
    @Mock
    private SynchronizationService synchronizationService;
    private IntegrationApiService helper;

    @BeforeEach
    void setUp()
    {
        helper = new IntegrationApiService(configurationService,
                siteService,
                jobService,
                buildService,
                issueLinkService,
                synchronizationService);
    }

    @AfterEach
    void tearDown()
    {
        verifyNoMoreInteractions(siteService);
    }

    @Test
    void testCompleteRegistration()
    {
        Site site = new Site().setId("site-1");

        helper.completeRegistration(site);

        ArgumentCaptor<Site> captor = ArgumentCaptor.forClass(Site.class);
        verify(siteService).save(captor.capture());
        assertThat(captor.getValue()
                .isJenkinsPluginInstalled()).isTrue();
        assertThat(captor.getValue()
                .isRegistrationComplete()).isTrue();
    }

    @Test
    void testGetBuildLinks()
    {
        Site site = new Site().setId("site-1");
        Job job1 = new Job().setSite(site)
                .setName("test-job");
        Build build = new Build().setJob(job1)
                .setNumber(1);

        when(jobService.findByHash(site, sha1Hex(job1.getUrlName()))).thenReturn(Optional.of(job1));
        when(buildService.get(job1, 1)).thenReturn(Optional.of(build));
        when(issueLinkService.getIssueLinks(build)).thenReturn(Stream.of(createReference("PR-1"),
                        createReference("PR-2"),
                        createReference("OP-1"))
                .collect(toSet()));

        Map<String, String> links = helper.getBuildLinks(site, sha1Hex(job1.getUrlName()), 1);

        assertThat(links).hasSize(3)
                .containsEntry("PR-1", "https://jira.example.com/browse/PR-1")
                .containsEntry("PR-2", "https://jira.example.com/browse/PR-2")
                .containsEntry("OP-1", "https://jira.example.com/browse/OP-1");
    }

    private IssueReference createReference(String issueKey)
    {
        return new IssueReference().setIssueKey(issueKey)
                .setIssueUrl(URI.create("https://jira.example.com/browse/" + issueKey));
    }

    @Nested
    class SynchronizeJobByPut
    {

        private Site site;

        @BeforeEach
        void setUp()
        {
            site = new Site().setId("site-1");
        }

        @Test
        void testSynchronizeJob()
        {
            Job job = new Job().setName("Test")
                    .setSite(site);
            when(jobService.findByHash(site, job.getHash())).thenReturn(Optional.of(job));

            helper.synchronizeJob(site, job.getHash(), null);

            verify(synchronizationService).synchronize(same(job), ArgumentMatchers.any(JobSyncTrigger.class));

            ArgumentCaptor<Site> captor = ArgumentCaptor.forClass(Site.class);
            verify(siteService).save(captor.capture());
            assertThat(captor.getValue()
                    .isRegistrationComplete()).isTrue();
        }

        @Test
        void testSynchronizeJob_UnknownJob()
        {
            helper.synchronizeJob(site, "hash", null);

            verifyNoMoreInteractions(synchronizationService);
        }

        @Test
        void testSynchronizeJob_UnknownJob_AutoEnableNewJobs()
        {
            site.setAutoLinkNewJobs(true);
            helper.synchronizeJob(site, "hash", null);

            verify(synchronizationService).synchronize(same(site), ArgumentMatchers.any(JobSyncTrigger.class));

            ArgumentCaptor<Site> captor = ArgumentCaptor.forClass(Site.class);
            verify(siteService).save(captor.capture());
            assertThat(captor.getValue()
                    .isRegistrationComplete()).isTrue();
        }

        @Test
        void testSynchronizeBuild()
        {
            Job job = new Job().setName("Test")
                    .setSite(site);
            when(jobService.findByHash(site, job.getHash())).thenReturn(Optional.of(job));

            helper.synchronizeBuild(site, job.getHash(), 1, null);

            verify(synchronizationService).synchronize(eq(new Build().setJob(job)
                    .setNumber(1)), ArgumentMatchers.any(BuildCompletionTrigger.class));

            ArgumentCaptor<Site> captor = ArgumentCaptor.forClass(Site.class);
            verify(siteService).save(captor.capture());
            assertThat(captor.getValue()
                    .isRegistrationComplete()).isTrue();
        }

        @Test
        void testSynchronizeBuild_UnknownJob()
        {
            Job job = new Job().setName("Test")
                    .setSite(site);

            helper.synchronizeBuild(site, job.getHash(), 1, null);

            verifyNoMoreInteractions(synchronizationService);
        }

        @Test
        void testSynchronizeBuild_UnknownJobWithParents()
        {
            Job job = new Job().setName("Test")
                    .setSite(site);
            doReturn(Optional.empty()).when(jobService)
                    .findByHash(site, "unknown-job-hash");
            doReturn(Optional.of(job)).when(jobService)
                    .findByHash(site, job.getHash());
            List<String> parentHashes = new ArrayList<>();
            parentHashes.add(JobHash.hash(job));

            helper.synchronizeBuild(site, "unknown-job-hash", 1, parentHashes);

            verify(synchronizationService).synchronize(same(job), ArgumentMatchers.any(BuildCompletionTrigger.class));

            ArgumentCaptor<Site> captor = ArgumentCaptor.forClass(Site.class);
            verify(siteService).save(captor.capture());
            assertThat(captor.getValue()
                    .isRegistrationComplete()).isTrue();
        }

        @Test
        void testSynchronizeBuild_UnknownJobAndParentsAutoEnabledJob()
        {
            site.setAutoLinkNewJobs(true);
            doReturn(Optional.empty()).when(jobService)
                    .findByHash(site, "unknown-job-hash");
            doReturn(Optional.empty()).when(jobService)
                    .findByHash(site, "unknown-parent-hash");
            List<String> parentHashes = new ArrayList<>();
            parentHashes.add("unknown-parent-hash");

            helper.synchronizeBuild(site, "unknown-job-hash", 1, parentHashes);

            verify(synchronizationService).synchronize(same(site), ArgumentMatchers.any(BuildCompletionTrigger.class));

            ArgumentCaptor<Site> captor = ArgumentCaptor.forClass(Site.class);
            verify(siteService).save(captor.capture());
            assertThat(captor.getValue()
                    .isRegistrationComplete()).isTrue();
        }

        @Test
        void testSynchronizeBuild_UnknownJobAutoEnabledJob()
        {
            site.setAutoLinkNewJobs(true);
            Job job = new Job().setName("Test")
                    .setSite(site);

            helper.synchronizeBuild(site, job.getHash(), 1, null);

            verify(synchronizationService).synchronize(same(site), ArgumentMatchers.any(BuildCompletionTrigger.class));

            ArgumentCaptor<Site> captor = ArgumentCaptor.forClass(Site.class);
            verify(siteService).save(captor.capture());
            assertThat(captor.getValue()
                    .isRegistrationComplete()).isTrue();
        }
    }

    @Nested
    class SynchronizeJobByPost
    {

        private Site site;

        @BeforeEach
        void setUp()
        {
            site = new Site().setId("site-1")
                    .setRpcUrl(URI.create("http://localhost:8080/"));
        }

        @Test
        void testSynchronizeJob()
        {
            Job job = new Job().setName("jira-jenkins-integration")
                    .setSite(site);
            when(jobService.findByHash(site, job.getHash())).thenReturn(Optional.of(job));
            InputStream input = generateInputForJobSynchronization();

            helper.synchronizeJob(site, job.getHash(), null, input, Optional.of(UTF_8));

            verify(synchronizationService).synchronize(same(job), ArgumentMatchers.any(JobSyncTrigger.class), same(input), same(UTF_8));

            ArgumentCaptor<Site> captor = ArgumentCaptor.forClass(Site.class);
            verify(siteService).save(captor.capture());
            assertThat(captor.getValue()
                    .isRegistrationComplete()).isTrue();
        }

        @Test
        void testSynchronizeJob_UnknownJob()
        {
            when(jobService.findByHash(site, "UnknownJob")).thenReturn(Optional.empty());
            InputStream input = generateInputForJobSynchronization();

            helper.synchronizeJob(site, "UnknownJob", null, input, Optional.of(UTF_8));

            verify(jobService).save(eq(new Job().setSite(site)
                    .setName("jira-jenkins-integration")));
            verifyNoMoreInteractions(synchronizationService);
        }

        @Test
        void testSynchronizeJob_RenamedToExistingJob()
        {
            Job job = new Job().setName("jira-jenkins-integration")
                    .setSite(site);
            InputStream input = generateInputForJobSynchronization();
            doReturn(Optional.empty()).when(jobService)
                    .findByHash(site, "RenamedToExistingJob");
            doReturn(Optional.of(job)).when(jobService)
                    .findByHash(site, job.getHash());

            helper.synchronizeJob(site, "RenamedToExistingJob", null, input, Optional.of(UTF_8));

            verify(jobService).save(eq(job));
            verifyNoMoreInteractions(synchronizationService);
        }

        @Test
        void testSynchronizeJob_InvalidJson()
                throws IOException
        {
            InputStream input = generateInputForJobSynchronization();
            assertThat(input.skip(10)).isGreaterThanOrEqualTo(1L);
            when(jobService.findByHash(site, "InvalidJson")).thenReturn(Optional.empty());

            assertThatThrownBy(() -> helper.synchronizeJob(site, "InvalidJson", null, input, Optional.of(UTF_8))).isInstanceOf(
                            SynchronizationTriggerException.class)
                    .hasMessageStartingWith("Failed to parse request body:");
        }

        private InputStream generateInputForJobSynchronization()
        {
            return getResourceAsStream("jira-jenkins-integration.json");
        }

        @Test
        void testSynchronizeBuild()
        {
            Job job = new Job().setName("jira-jenkins-integration")
                    .setSite(site);
            when(jobService.findByHash(site, job.getHash())).thenReturn(Optional.of(job));
            InputStream input = generateInputForBuildSynchronization();

            helper.synchronizeBuild(site, job.getHash(), 1, input, Optional.of(UTF_8));

            verify(synchronizationService).synchronize(eq(new Build().setJob(job)
                    .setNumber(1)), ArgumentMatchers.any(BuildCompletionTrigger.class), same(input), same(UTF_8));

            ArgumentCaptor<Site> captor = ArgumentCaptor.forClass(Site.class);
            verify(siteService).save(captor.capture());
            assertThat(captor.getValue()
                    .isRegistrationComplete()).isTrue();
        }

        @Test
        void testSynchronizeBuild_UnknownJob()
        {
            Job job = new Job().setName("jira-jenkins-integration")
                    .setSite(site);
            doReturn(Optional.empty()).when(jobService)
                    .findByHash(site, "UnknownJob");
            doReturn(Optional.of(job)).when(jobService)
                    .findByHash(site, job.getHash());
            InputStream input = generateInputForBuildSynchronization();

            helper.synchronizeBuild(site, "UnknownJob", 1, input, Optional.of(UTF_8));

            verify(synchronizationService).synchronize(eq(new Build().setJob(job)
                    .setNumber(1)), ArgumentMatchers.any(BuildCompletionTrigger.class), ArgumentMatchers.any(JsonStructure.class));

            ArgumentCaptor<Site> captor = ArgumentCaptor.forClass(Site.class);
            verify(siteService).save(captor.capture());
            assertThat(captor.getValue()
                    .isRegistrationComplete()).isTrue();
        }

        @Test
        void testSynchronizeBuild_NoParentInBuildData()
        {
            when(jobService.findByHash(site, "NoParentInBuildData")).thenReturn(Optional.empty());
            InputStream input = getResourceAsStream("jira-jenkins-integration-build-no-parent.json");

            helper.synchronizeBuild(site, "NoParentInBuildData", 1, input, Optional.of(UTF_8));

            verifyNoMoreInteractions(synchronizationService);
        }

        @Test
        void testSynchronizeBuild_InvalidJson()
                throws IOException
        {
            InputStream input = generateInputForBuildSynchronization();
            assertThat(input.skip(10)).isGreaterThanOrEqualTo(1L);
            when(jobService.findByHash(site, "InvalidJson")).thenReturn(Optional.empty());

            assertThatThrownBy(() -> helper.synchronizeBuild(site, "InvalidJson", 1, input, Optional.of(UTF_8))).isInstanceOf(
                            SynchronizationTriggerException.class)
                    .hasMessageStartingWith("Failed to parse request body:");
        }

        private InputStream generateInputForBuildSynchronization()
        {
            return getResourceAsStream("jira-jenkins-integration-build.json");
        }

        private InputStream getResourceAsStream(String name)
        {
            return getClass().getClassLoader()
                    .getResourceAsStream(name);
        }
    }
}
