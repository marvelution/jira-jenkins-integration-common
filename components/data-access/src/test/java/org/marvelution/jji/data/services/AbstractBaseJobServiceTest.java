/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services;

import java.util.*;

import org.marvelution.jji.data.services.api.exceptions.*;
import org.marvelution.jji.events.*;
import org.marvelution.jji.model.*;
import org.marvelution.jji.utils.*;
import org.marvelution.jji.validation.api.*;

import jakarta.inject.*;
import org.junit.jupiter.api.*;

import static java.util.Optional.*;
import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsCollectionContaining.hasItem;
import static org.junit.jupiter.api.Assertions.*;
import static org.marvelution.jji.data.Matchers.*;
import static org.marvelution.jji.model.Matchers.equalTo;
import static org.mockito.Mockito.*;

/**
 * Common tests for {@link BaseJobService} implementations.
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public abstract class AbstractBaseJobServiceTest<S extends BaseJobService>
        extends AbstractBaseServiceTest
{

    protected S jobService;
    protected Site site;

    @Inject
    @javax.inject.Inject
    void setJobService(S jobService)
    {
        this.jobService = spy(jobService);
    }

    @BeforeEach
    void createDefaultSite()
    {
        site = setupSite(createSite());
    }

    protected abstract Site setupSite(Site site);

    protected abstract Job setupJob(
            Job job,
            Build... builds)
            throws Exception;

    @Test
    void testCountBySite()
            throws Exception
    {
        setupJob(createJob(site, of("1")));
        setupJob(createJob(site, of("2")));

        Site site = setupSite(createSite(of("ci")));
        setupJob(createJob(site, of("1")));
        setupJob(createJob(site, of("2")));

        assertThat(jobService.countBySite(this.site), is(2L));
        assertThat(jobService.countBySite(site), is(2L));
    }

    @Test
    void testHasGetDeletedJobs()
            throws Exception
    {
        Job job1 = setupJob(createJob(site, of("1")));
        Job job2 = setupJob(createJob(site, of("2")));
        Job job3 = setupJob(createJob(site, of("3")).setDeleted(true));

        assertThat(jobService.hasDeletedJobs(site), is(true));
        List<Job> jobs = jobService.getDeletedJobs(site);
        assertThat(jobs, hasSize(1));
        assertThat(jobs, hasItem(equalTo(job3)));
        assertThat(jobs, not(hasItem(equalTo(job1))));
        assertThat(jobs, not(hasItem(equalTo(job2))));
    }

    @Test
    void testFindByHash()
            throws Exception
    {
        Job job1 = setupJob(createJob(site, of("1")));
        Job job2 = setupJob(createJob(site, of("2")));

        Site site = setupSite(createSite(of("ci")));
        Job jobCI1 = setupJob(createJob(site, of("1")));
        Job jobCI2 = setupJob(createJob(site, of("2")));

        Optional<Job> jobs = jobService.findByHash(site, JobHash.hash(job1));
        assertThat(jobs.isPresent(), is(true));
        assertThat(jobs.get(), equalTo(jobCI1));
        assertThat(jobs.get(), not(equalTo(job1)));
        assertThat(jobs.get(), not(equalTo(job2)));
        assertThat(jobs.get(), not(equalTo(jobCI2)));
    }

    @Test
    void testGetRootJobsOnSite()
            throws Exception
    {
        Job job1 = setupJob(createJob(site, of("1")));
        Job job2 = setupJob(createJob(site, of("2")));
        Job job3 = setupJob(createJob(site, of("3")).setDeleted(true));

        Site site = setupSite(createSite(of("ci")));
        Job jobCI1 = setupJob(createJob(site, of("CI 1")));
        Job jobCI2 = setupJob(createJob(site, of("CI 2")));
        Job jobCI3 = setupJob(createJob(site, of("CI 3")).setParentName(jobCI1.getFullName())
                .setDeleted(true));

        List<Job> jobs = jobService.getRootJobsOnSite(site);
        assertThat(jobs, hasSize(2));
        assertThat(jobs, hasItem(equalTo(jobCI1)));
        assertThat(jobs, hasItem(equalTo(jobCI2)));
        assertThat(jobs, not(hasItem(equalTo(jobCI3))));
        assertThat(jobs, not(hasItem(equalTo(job1))));
        assertThat(jobs, not(hasItem(equalTo(job2))));
        assertThat(jobs, not(hasItem(equalTo(job3))));
    }

    @Test
    void testGetSubJobs()
            throws Exception
    {
        Job job1 = setupJob(createJob(site, of("1")));
        Job job2 = setupJob(createJob(site, of("2")).setParentName(job1.getFullName()));
        Job job3 = setupJob(createJob(site, of("3")).setParentName(job1.getFullName()));
        Job job4 = setupJob(createJob(site, of("4")).setParentName(job3.getFullName())
                .setDeleted(true));

        Site siteCI = setupSite(createSite(of("ci")));
        Job jobCI1 = setupJob(createJob(siteCI, of("CI 1")));
        Job jobCI2 = setupJob(createJob(siteCI, of("CI 2")).setParentName(jobCI1.getFullName()));
        Job jobCI3 = setupJob(createJob(siteCI, of("CI 3")).setParentName(jobCI1.getFullName())
                .setDeleted(true));

        List<Job> jobs = jobService.getSubJobs(job1);
        assertThat(jobs, hasSize(2));
        assertThat(jobs, hasItem(equalTo(job2)));
        assertThat(jobs, hasItem(equalTo(job3)));
        assertThat(jobs, not(hasItem(equalTo(job1))));
        assertThat(jobs, not(hasItem(equalTo(job4))));
        assertThat(jobs, not(hasItem(equalTo(jobCI1))));
        assertThat(jobs, not(hasItem(equalTo(jobCI2))));
        assertThat(jobs, not(hasItem(equalTo(jobCI3))));
    }

    @Test
    void testGetParent()
            throws Exception
    {
        Job job1 = setupJob(createJob(site, of("1")));
        Job job2 = setupJob(createJob(site, of("2")).setParentName(job1.getFullName()));
        Job job3 = setupJob(createJob(site, of("3")).setParentName(job2.getFullName()));

        assertThat(jobService.getParent(job1), is(Optional.empty()));
        assertThat(jobService.getParent(job2)
                .get(), equalTo(job1));
        assertThat(jobService.getParent(job3)
                .get(), equalTo(job2));
    }

    @Test
    void testGetIncludingSubJobs()
            throws Exception
    {
        Job job1 = setupJob(createJob(site, of("1")));
        Job job2 = setupJob(createJob(site, of("2")).setParentName(job1.getFullName()));
        Job job3 = setupJob(createJob(site, of("3")).setParentName(job1.getFullName()));
        Job job4 = setupJob(createJob(site, of("4")).setParentName(job3.getFullName())
                .setDeleted(true));

        Optional<Job> job = jobService.get(job1.getId(), true);
        assertThat(job.get(), equalTo(job1));
        assertThat(job.get()
                .getJobs(), hasItem(equalTo(job2)));
        assertThat(job.get()
                .getJobs(), hasItem(equalTo(job3)));
        assertThat(job.get()
                .getJobs(), not(hasItem(equalTo(job1))));
        assertThat(job.get()
                .getJobs(), not(hasItem(equalTo(job4))));
    }

    @Test
    void testCRUD()
    {
        // Create
        // try adding a site without any details
        Job job = new Job();
        try
        {
            jobService.save(job);
            fail("Expected ValidationException");
        }
        catch (ValidationException e)
        {
            ErrorMessages errors = e.getMessages();
            assertThat(errors.getErrors(), hasSize(2));
            assertThat(errors.getErrors(), hasItem(errorForField("name")));
            assertThat(errors.getErrors(), hasItem(errorForField("site")));
        }
        job.setSite(site)
                .setName(getTestMethodName());
        Job inserted = jobService.save(job);
        assertThat(inserted, equalTo(job, true));
        // Read
        assertThat(jobService.getExisting(inserted.getId()), equalTo(inserted));
        // Find
        List<Job> jobs = jobService.find(getTestMethodName());
        assertThat(jobs, hasSize(1));
        assertThat(jobs, hasItem(equalTo(inserted)));
        // Update
        job = inserted.copy()
                .setDescription("My great Job")
                .setDisplayName("My Job");
        Job updated = jobService.save(job);
        assertThat(updated, equalTo(job));
        // Delete
        try
        {
            jobService.delete(inserted);
            fail("Expected CannotDeleteJobException");
        }
        catch (CannotDeleteJobException e)
        {
            assertThat(e.getJob(), equalTo(inserted));
        }
        jobService.save(inserted.setDeleted(true));
        jobService.delete(inserted);
        assertThat(jobService.get(inserted.getId())
                .isPresent(), is(false));
        assertEventPublished(new JobDeletedEvent(inserted));
    }

    @Test
    void testEnable()
            throws Exception
    {
        Job job = setupJob(createJob(site));

        assertThat(jobService.getExisting(job.getId())
                .isLinked(), is(false));

        jobService.enable(job, true);

        assertThat(jobService.getExisting(job.getId())
                .isLinked(), is(true));

        jobService.enable(job.getId(), false);

        assertThat(jobService.getExisting(job.getId())
                .isLinked(), is(false));
    }

    @Test
    void testDeleteAllBuilds()
            throws Exception
    {
        Job job = setupJob(createJob(site).setLastBuild(10),
                new Build().setNumber(10)
                        .setResult(Result.SUCCESS),
                new Build().setNumber(9)
                        .setResult(Result.SUCCESS),
                new Build().setNumber(8)
                        .setResult(Result.SUCCESS));

        jobService.deleteAllBuilds(job.getId());

        assertThat(jobService.buildService.getByJob(job), hasSize(0));
        assertThat(jobService.getExisting(job.getId())
                .getLastBuild(), is(0));
        assertEventPublished(new BuildsDeletedEvent(job));
    }

    @Test
    void testResetBuildCache()
            throws Exception
    {
        Job job = setupJob(createJob(site).setLastBuild(10),
                new Build().setNumber(10)
                        .setResult(Result.SUCCESS),
                new Build().setNumber(9)
                        .setResult(Result.SUCCESS),
                new Build().setNumber(8)
                        .setResult(Result.SUCCESS));

        jobService.resetBuildCache(job);

        Set<Build> builds = jobService.buildService.getByJob(job);
        assertThat(builds, hasSize(3));
        assertThat(builds,
                hasItem(equalTo(new Build().setJob(job)
                        .setNumber(10)
                        .setResult(Result.SUCCESS)
                        .setDeleted(true), true)));
        assertThat(builds,
                hasItem(equalTo(new Build().setJob(job)
                        .setNumber(9)
                        .setResult(Result.SUCCESS)
                        .setDeleted(true), true)));
        assertThat(builds,
                hasItem(equalTo(new Build().setJob(job)
                        .setNumber(8)
                        .setResult(Result.SUCCESS)
                        .setDeleted(true), true)));
        assertThat(jobService.getExisting(job.getId())
                .getLastBuild(), is(0));
    }
}
