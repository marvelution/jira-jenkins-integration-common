/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services;

import java.util.*;
import java.util.stream.*;

import org.marvelution.jji.data.access.api.*;
import org.marvelution.jji.model.*;

import jakarta.inject.*;
import org.junit.jupiter.api.*;

import static java.util.Arrays.*;

public abstract class AbstractBaseLinkStatisticianIT<L extends BaseLinkStatistician>
        extends AbstractBaseLinkStatisticianTest<L>
{

    @Inject
    @javax.inject.Inject
    protected SiteDAO siteDAO;
    @Inject
    @javax.inject.Inject
    protected JobDAO jobDAO;
    @Inject
    @javax.inject.Inject
    private BuildDAO buildDAO;
    @Inject
    @javax.inject.Inject
    private IssueToBuildDAO issueToBuildDAO;

    @AfterEach
    void cleanupSites()
    {
        siteDAO.getAll()
                .stream()
                .map(Site::getId)
                .forEach(siteDAO::delete);
    }

    @Override
    protected Job setupJob(Job job)
    {
        Site site = job.getSite();
        if (site == null)
        {
            site = createSite(Optional.empty());
        }
        job.setSite(siteDAO.save(site));
        return jobDAO.save(job);
    }

    @Override
    protected Build setupBuild(
            Build build,
            IssueReference... issueReferences)
    {
        if (build.getNumber() > build.getJob()
                .getLastBuild())
        {
            build.setJob(jobDAO.save(build.getJob()
                    .setLastBuild(build.getNumber())));
        }
        Build saved = buildDAO.save(build);
        if (issueReferences != null)
        {
            issueToBuildDAO.relink(saved, new HashSet<>(asList(issueReferences)));
        }
        return saved;
    }

    @Override
    protected void setupLink(
            Build build,
            IssueReference... issueLinks)
    {
        if (issueLinks != null)
        {
            Stream.of(issueLinks)
                    .forEach(reference -> issueToBuildDAO.link(build, reference));
        }
    }
}
