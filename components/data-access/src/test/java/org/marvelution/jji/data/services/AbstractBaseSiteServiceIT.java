/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services;

import org.marvelution.jji.data.access.api.*;
import org.marvelution.jji.model.*;

import jakarta.inject.*;
import org.junit.jupiter.api.*;

import static java.util.Arrays.*;

/**
 * Common integration tests for {@link BaseSiteService} implementations.
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public abstract class AbstractBaseSiteServiceIT<S extends BaseSiteService>
        extends AbstractBaseSiteServiceTest<S>
{

    @Inject
    @javax.inject.Inject
    private SiteDAO siteDAO;
    @Inject
    @javax.inject.Inject
    private JobDAO jobDAO;

    @AfterEach
    void cleanupSites()
    {
        siteDAO.getAll()
                .stream()
                .map(Site::getId)
                .forEach(siteDAO::delete);
    }

    @Override
    protected Site setupSite(
            Site site,
            Job... jobs)
    {
        Site saved = siteDAO.save(site);
        if (jobs != null)
        {
            stream(jobs).map(job -> job.setSite(saved))
                    .map(jobDAO::save)
                    .forEach(site.getJobs()::add);
        }
        return saved;
    }
}
