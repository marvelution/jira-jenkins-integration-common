/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.api.migration;

import java.io.*;
import java.net.*;
import java.util.*;
import javax.annotation.*;

public interface AppMigrationManager<M extends AppMigration>
{

	Optional<M> getMigration(String id);

	List<M> getImports();

	List<M> getExports();

	void migrate(
			ImportOptions options,
			InputStream importFile)
			throws IOException;

	void migrate(ExportOptions options);

	@Nullable
	URI getDownloadLink(M migration);
}
