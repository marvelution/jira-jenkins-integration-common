/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.api.migration;

import java.util.StringJoiner;
import jakarta.xml.bind.annotation.XmlElement;

public abstract class MigrationOptions<O extends MigrationOptions<O>>
{

    @XmlElement
    @javax.xml.bind.annotation.XmlElement
    private String name;
    @XmlElement
    @javax.xml.bind.annotation.XmlElement
    private boolean includeConfiguration;
    @XmlElement
    @javax.xml.bind.annotation.XmlElement
    private boolean includeIntegration;

    @SuppressWarnings("unchecked")
    private O self()
    {
        return (O) this;
    }

    public String getName()
    {
        return name;
    }

    public O setName(String name)
    {
        this.name = name;
        return self();
    }

    public boolean isIncludeConfiguration()
    {
        return includeConfiguration;
    }

    public O setIncludeConfiguration(boolean includeConfiguration)
    {
        this.includeConfiguration = includeConfiguration;
        return self();
    }

    public boolean isIncludeIntegration()
    {
        return includeIntegration;
    }

    public O setIncludeIntegration(boolean includeIntegration)
    {
        this.includeIntegration = includeIntegration;
        return self();
    }

    @Override
    public String toString()
    {
        return new StringJoiner(", ", getClass().getSimpleName() + "[", "]").add("name='" + name + "'")
                .add("includeConfiguration=" + includeConfiguration)
                .add("includeIntegration=" + includeIntegration)
                .toString();
    }
}
