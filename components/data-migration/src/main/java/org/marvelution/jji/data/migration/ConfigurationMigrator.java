/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.migration;

import org.marvelution.jji.api.migration.*;
import org.marvelution.jji.api.text.*;
import org.marvelution.jji.data.services.api.*;
import org.marvelution.jji.model.*;

import jakarta.inject.*;
import org.apache.commons.lang3.*;
import org.slf4j.*;

@Named
@javax.inject.Named
public class ConfigurationMigrator
        extends Migrator
{

    private static final Logger LOGGER = LoggerFactory.getLogger(ConfigurationMigrator.class);
    private final ConfigurationService configurationService;

    @Inject
    @javax.inject.Inject
    public ConfigurationMigrator(
            TextResolver textResolver,
            ConfigurationService configurationService)
    {
        super(textResolver, "Configuration");
        this.configurationService = configurationService;
    }

    @Override
    public void exportData(MigrationData.Builder builder)
    {
        LOGGER.info("Exporting app configuration...");
        ConfigurationData.Builder configurationBuilder = builder.getConfigurationBuilder();
        configurationService.getAllConfigurationSettings()
                .stream()
                .filter(ConfigurationSetting::isOverridable)
                .filter(configurationSetting -> StringUtils.isNotBlank(configurationSetting.getValue()))
                .forEach(configurationSetting -> configurationBuilder.addSettingsBuilder()
                        .setKey(configurationSetting.getKey())
                        .setValue(configurationSetting.getValue())
                        .setScope(ConfigurationService.requireScopeElse(configurationSetting.getScope())));
    }

    @Override
    public void doImportData(
            MigrationData migrationData,
            Feedback feedback)
    {
        LOGGER.info("Importing {} app configuration settings...",
                migrationData.getConfiguration()
                        .getSettingsCount());
        migrationData.getConfiguration()
                .getSettingsList()
                .forEach(settingData -> configurationService.saveConfigurationSetting(ConfigurationSetting.forKey(settingData.getKey())
                        .setScope(settingData.getScope())
                        .setValue(settingData.getValue())));
    }

    @Override
    public void clear(Feedback feedback)
    {
        configurationService.resetToDefaults();
    }
}
