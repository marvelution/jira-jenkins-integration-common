/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.api.migration;

import java.util.*;

public class Feedback
{

	private static final String ERRORS = "errors";
	private static final String WARNINGS = "warnings";
	private static final String INFO = "info";
	private Map<String, Object> details;

	public Map<String, Object> getDetails()
	{
		if (details == null)
		{
			details = new HashMap<>();
		}
		return details;
	}

	public Feedback setDetails(Map<String, Object> details)
	{
		this.details = details;
		return this;
	}

	public Feedback addDetails(
			String key,
			Object details)
	{
		getDetails().put(key, details);
		return this;
	}

	public boolean hasDetails()
	{
		return !getDetails().isEmpty();
	}

	public Feedback addError(String error)
	{
		return addToDetailsList(ERRORS, error);
	}

	public boolean hasErrors()
	{
		return !getDetailsList(ERRORS, false).isEmpty();
	}

	public List<String> getErrors()
	{
		return getDetailsList(ERRORS, false);
	}

	public Feedback addWarning(String warning)
	{
		return addToDetailsList(WARNINGS, warning);
	}

	public boolean hasWarnings()
	{
		return !getDetailsList(WARNINGS, false).isEmpty();
	}

	public List<String> getWarnings()
	{
		return getDetailsList(WARNINGS, false);
	}

	public Feedback addInfo(String info)
	{
		return addToDetailsList(INFO, info);
	}

	public boolean hasInfo()
	{
		return !getDetailsList(INFO, false).isEmpty();
	}

	public List<String> getInfo()
	{
		return getDetailsList(INFO, false);
	}

	private Feedback addToDetailsList(
			String key,
			String detail)
	{
		List<String> detailsList = getDetailsList(key, true);
		detailsList.add(detail);
		return this;
	}

	@SuppressWarnings("unchecked")
	private List<String> getDetailsList(
			String key,
			boolean createIfMissing)
	{
		List<String> list = (List<String>) getDetails().get(key);
		if (list == null)
		{
			list = new ArrayList<>();
			if (createIfMissing)
			{
				getDetails().put(key, list);
			}
		}
		return list;
	}
}
