/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.api.migration;

import java.util.StringJoiner;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@javax.xml.bind.annotation.XmlRootElement
@javax.xml.bind.annotation.XmlAccessorType(javax.xml.bind.annotation.XmlAccessType.FIELD)
public class ImportOptions
        extends MigrationOptions<ImportOptions>
{

    private boolean blankImport;

    public boolean isBlankImport()
    {
        return blankImport;
    }

    public ImportOptions setBlankImport(boolean blankImport)
    {
        this.blankImport = blankImport;
        return this;
    }

    @Override
    public String toString()
    {
        return new StringJoiner(", ", ImportOptions.class.getSimpleName() + "[", "]").add("name='" + getName() + "'")
                .add("blankImport=" + blankImport)
                .add("includeConfiguration=" + isIncludeConfiguration())
                .add("includeIntegration=" + isIncludeIntegration())
                .toString();
    }
}
