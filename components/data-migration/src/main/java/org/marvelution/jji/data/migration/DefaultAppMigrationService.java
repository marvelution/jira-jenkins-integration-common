/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.migration;

import java.io.*;
import java.util.function.*;
import java.util.zip.*;

import org.marvelution.jji.api.features.*;
import org.marvelution.jji.api.migration.*;
import org.marvelution.jji.api.text.*;

import jakarta.inject.*;
import org.slf4j.*;

import static org.marvelution.jji.api.features.FeatureFlags.*;

@Named
@javax.inject.Named
public class DefaultAppMigrationService
        implements AppMigrationService
{
    static final int BATCH_SIZE = 100;
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultAppMigrationService.class);
    private final Features features;
    private final ConfigurationMigrator configurationMigrator;
    private final IntegrationMigrator integrationMigrator;
    private final TextResolver textResolver;

    @Inject
    @javax.inject.Inject
    public DefaultAppMigrationService(
            Features features,
            ConfigurationMigrator configurationMigrator,
            IntegrationMigrator integrationMigrator,
            TextResolver textResolver)
    {
        this.features = features;
        this.configurationMigrator = configurationMigrator;
        this.integrationMigrator = integrationMigrator;
        this.textResolver = textResolver;
    }

    @Override
    public void export(
            OutputStream outputStream,
            ExportOptions options)
            throws IOException
    {
        if (features.isFeatureEnabled(DATA_MIGRATION))
        {
            if (outputStream instanceof ZipOutputStream)
            {
                LOGGER.info("Exporting data in a zip...");
                ZipOutputStreamProvider streamProvider = new ZipOutputStreamProvider((ZipOutputStream) outputStream);
                export(streamProvider, options);
            }
            else
            {
                LOGGER.info("Exporting data...");
                MigrationData.Builder builder = MigrationData.newBuilder();

                handleExport(options::isIncludeConfiguration, configurationMigrator, builder);
                handleExport(options::isIncludeIntegration, integrationMigrator, builder);

                builder.build()
                        .writeTo(outputStream);
            }
        }
    }

    private void handleExport(
            Supplier<Boolean> shouldInclude,
            Migrator migrator,
            MigrationData.Builder builder)
    {
        if (shouldInclude.get())
        {
            migrator.exportData(builder);
        }
    }

    @Override
    public void export(
            OutputStreamProvider outputStreamProvider,
            ExportOptions options)
            throws IOException
    {
        if (features.isFeatureEnabled(DATA_MIGRATION))
        {
            LOGGER.debug("Exporting data using a custom output stream provider");
            handleExport(options::isIncludeConfiguration, configurationMigrator, outputStreamProvider);
            handleExport(options::isIncludeIntegration, integrationMigrator, outputStreamProvider);
            outputStreamProvider.finish();
        }
    }

    private void handleExport(
            Supplier<Boolean> shouldInclude,
            Migrator migrator,
            OutputStreamProvider outputStreamProvider)
            throws IOException
    {
        if (shouldInclude.get())
        {
            migrator.exportData(outputStreamProvider);
        }
    }

    @Override
    public Feedback importAndValidate(
            InputStream inputStream,
            ImportOptions options,
            ImportListener listener)
            throws IOException
    {
        Feedback feedback = new Feedback();
        if (features.isFeatureEnabled(DATA_MIGRATION))
        {
            handleImportClear(options, feedback);

            LOGGER.info("Importing data {}", options);
            listener.onStartImport(1);
            if (inputStream instanceof ZipInputStream)
            {
                ZipInputStream zipInputStream = (ZipInputStream) inputStream;
                ZipEntry entry = zipInputStream.getNextEntry();
                while (entry != null)
                {
                    feedback.addInfo(textResolver.getText("importing.file", entry.getName()));
                    handleImport(zipInputStream, options, feedback);
                    entry = zipInputStream.getNextEntry();
                }
            }
            else
            {
                handleImport(inputStream, options, feedback);
            }
            listener.onFinishedImport();
        }
        else
        {
            feedback.addError(textResolver.getText("migration.disabled"));
        }
        return feedback;
    }

    @Override
    public Feedback importAndValidate(
            byte[] partsListData,
            InputStreamProvider streamProvider,
            ImportOptions options,
            ImportListener listener)
            throws IOException
    {
        Feedback feedback = new Feedback();
        if (features.isFeatureEnabled(DATA_MIGRATION))
        {
            handleImportClear(options, feedback);

            LOGGER.info("Importing data {}", options);
            MigrationPartsList partsList = MigrationPartsList.parseFrom(partsListData);
            listener.onStartImport(partsList.getLabelCount());
            for (int i = 1; i <= partsList.getLabelList()
                    .size(); i++)
            {
                String part = partsList.getLabel(i - 1);
                listener.onStartImportPart(part, i);
                InputStream inputStream = streamProvider.getInputStream(part);
                if (inputStream != null)
                {
                    handleImport(inputStream, options, feedback);
                }
                else
                {
                    feedback.addWarning("Skipping part " + part + " as it has no data");
                }
                listener.onFinishedImportPart(part, i);
            }
            listener.onFinishedImport();
        }
        else
        {
            feedback.addError(textResolver.getText("migration.disabled"));
        }
        return feedback;
    }

    private void handleImport(
            InputStream inputStream,
            ImportOptions options,
            Feedback feedback)
            throws IOException
    {
        LOGGER.debug("Importing data from stream {}", inputStream);
        MigrationData migrationData = MigrationData.parseFrom(inputStream);
        handleImport(options, MigrationOptions::isIncludeConfiguration, configurationMigrator, migrationData, feedback);
        handleImport(options, MigrationOptions::isIncludeIntegration, integrationMigrator, migrationData, feedback);
    }

    private void handleImport(
            ImportOptions options,
            Function<ImportOptions, Boolean> shouldInclude,
            Migrator migrator,
            MigrationData migrationData,
            Feedback feedback)
    {
        if (shouldInclude.apply(options))
        {
            migrator.importData(migrationData, feedback);
        }
    }

    @Override
    public void rollbackImport(ImportOptions options)
    {
        if (features.isFeatureEnabled(DATA_MIGRATION))
        {
            handleImportClear(options, new Feedback());
        }
    }

    private void handleImportClear(
            ImportOptions options,
            Feedback feedback)
    {
        if (options.isBlankImport())
        {
            LOGGER.info("Clearing data for import: {}", options);
            handleImportClear(options, MigrationOptions::isIncludeConfiguration, configurationMigrator, feedback);
            handleImportClear(options, MigrationOptions::isIncludeIntegration, integrationMigrator, feedback);
        }
    }

    private void handleImportClear(
            ImportOptions options,
            Function<ImportOptions, Boolean> shouldInclude,
            Migrator migrator,
            Feedback feedback)
    {
        if (shouldInclude.apply(options))
        {
            migrator.clear(feedback);
        }
    }

    private static class ZipOutputStreamProvider
            implements OutputStreamProvider
    {
        private final ZipOutputStream zipOutputStream;

        private ZipOutputStreamProvider(ZipOutputStream zipOutputStream)
        {
            this.zipOutputStream = zipOutputStream;
        }

        @Override
        public OutputStream getOutputStream(String label)
                throws IOException
        {
            ZipEntry entry = new ZipEntry(label);
            zipOutputStream.putNextEntry(entry);
            return zipOutputStream;
        }

        @Override
        public void finish()
                throws IOException
        {
            zipOutputStream.finish();
        }
    }
}
