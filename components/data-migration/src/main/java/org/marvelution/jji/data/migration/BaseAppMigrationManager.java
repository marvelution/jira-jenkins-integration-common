/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.migration;

import java.io.*;
import java.net.*;
import java.time.*;
import java.util.*;
import java.util.zip.*;

import org.marvelution.jji.api.migration.*;
import org.marvelution.jji.api.text.*;
import org.marvelution.jji.validation.api.*;

import org.apache.commons.lang3.*;
import org.slf4j.*;

import static org.marvelution.jji.api.migration.AppMigrationType.*;
import static org.marvelution.jji.api.migration.ProgressStatus.*;
import static org.marvelution.jji.model.HasId.*;

public abstract class BaseAppMigrationManager<M extends AppMigration, D extends AppMigrationDAO<M>>
        implements AppMigrationManager<M>
{

    protected final Logger logger = LoggerFactory.getLogger(getClass());
    protected final AppMigrationService appMigrationService;
    protected final D appMigrationDAO;
    protected final TextResolver textResolver;

    protected BaseAppMigrationManager(
            AppMigrationService appMigrationService,
            D appMigrationDAO,
            TextResolver textResolver)
    {
        this.appMigrationService = appMigrationService;
        this.appMigrationDAO = appMigrationDAO;
        this.textResolver = textResolver;
    }

    @Override
    public Optional<M> getMigration(String id)
    {
        return appMigrationDAO.findById(id);
    }

    @Override
    public List<M> getImports()
    {
        return appMigrationDAO.findByType(IMPORT, JCMA);
    }

    @Override
    public List<M> getExports()
    {
        return appMigrationDAO.findByType(EXPORT);
    }

    @Override
    public void migrate(
            ImportOptions options,
            InputStream importFile)
            throws IOException
    {
        validate(options);

        M migration = appMigrationDAO.store(createMigrationEvent(newId(), LocalDateTime.now(), IMPORT, READY, options), importFile);

        performMigration(migration);
    }

    @Override
    public void migrate(ExportOptions options)
    {
        validate(options);

        M migration = appMigrationDAO.store(createMigrationEvent(newId(), LocalDateTime.now(), EXPORT, READY, options));

        performMigration(migration);
    }

    private <O extends MigrationOptions<O>> void validate(O options)
    {
        ErrorMessages errorMessages = new ErrorMessages();
        if (StringUtils.isBlank(options.getName()))
        {
            errorMessages.addError("name", textResolver.getText("migration.name.required"));
        }
        else if (!options.getName()
                .matches("[\\w\\s\\-_]*"))
        {
            errorMessages.addError("name", textResolver.getText("name.invalid.tokens"));
        }
        if (!options.isIncludeConfiguration() && !options.isIncludeIntegration())
        {
            errorMessages.addError("includeConfiguration", textResolver.getText("migration.select.at.least.one.option"));
        }
        if (errorMessages.hasErrors())
        {
            throw new ValidationException(errorMessages);
        }
    }

    protected abstract <O extends MigrationOptions<O>> M createMigrationEvent(
            String id,
            LocalDateTime createdAt,
            AppMigrationType type,
            ProgressStatus progressStatus,
            O options);

    protected abstract void performMigration(M migration);

    protected void executeMigration(M migration)
    {
        appMigrationDAO.updateProgressStatus(migration.getId(), ProgressStatus.IN_PROGRESS);
        if (migration.getType() == AppMigrationType.IMPORT)
        {
            try (InputStream inputStream = new ZipInputStream(appMigrationDAO.getMigrationData(migration)))
            {
                Feedback feedback = appMigrationService.importAndValidate(inputStream, migration.getEvent(ImportOptions.class));
                if (feedback.hasDetails())
                {
                    appMigrationDAO.updateFeedback(migration.getId(), feedback);
                }
                appMigrationDAO.updateProgressStatus(migration.getId(), feedback.hasErrors() ? FAILED : SUCCESS);
            }
            catch (Exception e)
            {
                storeMigrationError(migration, e);
            }
        }
        else if (migration.getType() == AppMigrationType.EXPORT)
        {
            try (OutputStream output = new ZipOutputStream(appMigrationDAO.createMigrationData(migration)))
            {
                appMigrationService.export(output, migration.getEvent(ExportOptions.class));
                appMigrationDAO.updateProgressStatus(migration.getId(), SUCCESS);
            }
            catch (Exception e)
            {
                storeMigrationError(migration, e);
            }
        }
        else
        {
            logger.error("Unsupported migration type {}", migration.getType());
            appMigrationDAO.updateProgressStatus(migration.getId(), ProgressStatus.SKIPPED);
            Feedback feedback = new Feedback();
            feedback.addError("Unsupported migration type " + migration.getType());
            appMigrationDAO.updateFeedback(migration.getId(), feedback);
        }
    }

    protected void storeMigrationError(
            M migration,
            Exception error)
    {
        logger.error("Failed to {} data for migration {}: {}", migration.getType(), migration.getName(), error.getMessage(), error);
        appMigrationDAO.updateProgressStatus(migration.getId(), ProgressStatus.FAILED);
        Feedback feedback = new Feedback();
        feedback.addError("Failed to process migration: " + error.getMessage());
        appMigrationDAO.updateFeedback(migration.getId(), feedback);
    }

    @Override
    public URI getDownloadLink(M migration)
    {
        if (migration.getProgress() == SUCCESS && migration.getType() == AppMigrationType.EXPORT)
        {
            return appMigrationDAO.getDownloadLink(migration);
        }
        else
        {
            return null;
        }
    }
}
