/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.api.migration;

import java.io.*;

public interface AppMigrationService
{

    String FULL_EXPORT_LABEL = "full-export";
    String PARTS_LIST_LABEL = "parts-list";

    void export(
            OutputStream outputStream,
            ExportOptions options)
            throws IOException;

    void export(
            OutputStreamProvider outputStreamProvider,
            ExportOptions options)
            throws IOException;

    default byte[] export(ExportOptions options)
            throws IOException
    {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        export(outputStream, options);
        return outputStream.toByteArray();
    }

    default Feedback importAndValidate(
            InputStream inputStream,
            ImportOptions options)
            throws IOException
    {
        return importAndValidate(inputStream, options, new ImportListener() {});
    }

    Feedback importAndValidate(
            InputStream inputStream,
            ImportOptions options,
            ImportListener listener)
            throws IOException;

    default Feedback importAndValidate(
            byte[] partsListData,
            InputStreamProvider streamProvider,
            ImportOptions options)
            throws IOException
    {
        return importAndValidate(partsListData, streamProvider, options, new ImportListener() {});
    }

    Feedback importAndValidate(
            byte[] partsListData,
            InputStreamProvider streamProvider,
            ImportOptions options,
            ImportListener listener)
            throws IOException;

    default Feedback importAndValidate(
            byte[] data,
            ImportOptions options)
            throws IOException
    {
        return importAndValidate(data, options, new ImportListener() {});
    }

    default Feedback importAndValidate(
            byte[] data,
            ImportOptions options,
            ImportListener listener)
            throws IOException
    {
        return importAndValidate(new ByteArrayInputStream(data), options, listener);
    }

    void rollbackImport(ImportOptions options);
}
