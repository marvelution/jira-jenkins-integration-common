/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.migration;

import java.io.*;
import java.util.*;

import org.marvelution.jji.api.migration.*;
import org.marvelution.jji.api.text.*;

import org.slf4j.*;

public abstract class Migrator
{

    protected final Logger logger = LoggerFactory.getLogger(getClass());
    protected final TextResolver textResolver;
    protected final String type;

    protected Migrator(
            TextResolver textResolver,
            String type)
    {
        this.textResolver = textResolver;
        this.type = type;
    }

    abstract void exportData(MigrationData.Builder builder);

    void exportData(OutputStreamProvider streamProvider)
            throws IOException
    {
        MigrationData.Builder builder = MigrationData.newBuilder();
        exportData(builder);
        OutputStream outputStream = streamProvider.getOutputStream(type);
        builder.build().writeTo(outputStream);
    }

    void importData(
            MigrationData migrationData,
            Feedback feedback)
    {
        try
        {
            doImportData(migrationData, feedback);
        }
        catch (Exception e)
        {
            logger.error("Failed to import {} data; {}", type, e.getMessage(), e);
            feedback.addError(textResolver.getText("migration.failed.to.import.x", type));
        }
    }

   protected abstract void doImportData(
            MigrationData migrationData,
            Feedback feedback);

    abstract void clear(Feedback feedback);
}
