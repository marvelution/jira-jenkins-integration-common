/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.api.migration;

public class MigrationDetails
{

	private String migrationId;
	private String migrationScopeId;
	private String name;
	private Long createdAt;
	private String jiraClientKey;
	private String confluenceClientKey;
	private String cloudUrl;

	public String getMigrationId()
	{
		return migrationId;
	}

	public MigrationDetails setMigrationId(String migrationId)
	{
		this.migrationId = migrationId;
		return this;
	}

	public String getMigrationScopeId()
	{
		return migrationScopeId;
	}

	public MigrationDetails setMigrationScopeId(String migrationScopeId)
	{
		this.migrationScopeId = migrationScopeId;
		return this;
	}

	public String getName()
	{
		return name;
	}

	public MigrationDetails setName(String name)
	{
		this.name = name;
		return this;
	}

	public Long getCreatedAt()
	{
		return createdAt;
	}

	public MigrationDetails setCreatedAt(Long createdAt)
	{
		this.createdAt = createdAt;
		return this;
	}

	public String getJiraClientKey()
	{
		return jiraClientKey;
	}

	public MigrationDetails setJiraClientKey(String jiraClientKey)
	{
		this.jiraClientKey = jiraClientKey;
		return this;
	}

	public String getConfluenceClientKey()
	{
		return confluenceClientKey;
	}

	public MigrationDetails setConfluenceClientKey(String confluenceClientKey)
	{
		this.confluenceClientKey = confluenceClientKey;
		return this;
	}

	public String getCloudUrl()
	{
		return cloudUrl;
	}

	public MigrationDetails setCloudUrl(String cloudUrl)
	{
		this.cloudUrl = cloudUrl;
		return this;
	}
}
