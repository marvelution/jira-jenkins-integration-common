/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.migration;

import java.io.IOException;
import java.net.URI;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;
import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.marvelution.jji.api.migration.Feedback;
import org.marvelution.jji.api.migration.OutputStreamProvider;
import org.marvelution.jji.api.text.TextResolver;
import org.marvelution.jji.data.access.api.*;
import org.marvelution.jji.data.services.api.SiteClient;
import org.marvelution.jji.data.services.api.SiteValidator;
import org.marvelution.jji.model.*;
import org.marvelution.jji.validation.api.ErrorMessages;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toSet;
import static org.marvelution.jji.data.migration.DefaultAppMigrationService.BATCH_SIZE;

@Named
@javax.inject.Named
public class IntegrationMigrator
        extends Migrator
{

    private static final Logger LOGGER = LoggerFactory.getLogger(IntegrationMigrator.class);
    private final SiteDAO siteDao;
    private final JobDAO jobDao;
    private final BuildDAO buildDao;
    private final IssueToBuildDAO issueToBuildDao;
    private final LinkStatisticsDAO linkStatisticsDao;
    private final SiteValidator siteValidator;
    private final SiteClient siteClient;

    @Inject
    @javax.inject.Inject
    public IntegrationMigrator(
            TextResolver textResolver,
            SiteDAO siteDao,
            JobDAO jobDao,
            BuildDAO buildDao,
            IssueToBuildDAO issueToBuildDao,
            LinkStatisticsDAO linkStatisticsDao,
            SiteValidator siteValidator,
            SiteClient siteClient)
    {
        super(textResolver, "Integration");
        this.siteDao = siteDao;
        this.jobDao = jobDao;
        this.buildDao = buildDao;
        this.issueToBuildDao = issueToBuildDao;
        this.linkStatisticsDao = linkStatisticsDao;
        this.siteValidator = siteValidator;
        this.siteClient = siteClient;
    }

    @Override
    public void exportData(MigrationData.Builder builder)
    {
        LOGGER.info("Exporting sites...");
        for (Site site : siteDao.getAll())
        {
            exportSite(builder.addSitesBuilder(), site, true);
        }
        exportLinkStats(builder);
    }

    @Override
    void exportData(OutputStreamProvider streamProvider)
            throws IOException
    {
        Set<String> siteIds = new HashSet<>();
        MigrationData.Builder dataBuilder = MigrationData.newBuilder();
        for (Site site : siteDao.getAll())
        {
            siteIds.add(site.getId());
            exportSite(dataBuilder.addSitesBuilder(), site, false);
        }
        dataBuilder.build()
                .writeTo(streamProvider.getOutputStream(type + "Sites"));
        LOGGER.debug("Exported {} sites", dataBuilder.getSitesCount());

        int counter = 0;
        Set<String> jobIds = new HashSet<>();
        dataBuilder = MigrationData.newBuilder();
        for (String siteId : siteIds)
        {
            int pages = (int) Math.ceil(jobDao.countBySite(siteId) / (double) BATCH_SIZE);
            for (int page = 0; page < pages; page++)
            {
                for (Job job : jobDao.findBySiteId(siteId, page, BATCH_SIZE))
                {
                    jobIds.add(job.getId());
                    exportJob(dataBuilder.addJobsBuilder(), job, false);
                }
                if (dataBuilder.getJobsBuilderList()
                            .size() >= BATCH_SIZE)
                {
                    dataBuilder.build()
                            .writeTo(streamProvider.getOutputStream(type + "Jobs" + (++counter)));
                    LOGGER.debug("Exported {} jobs",
                            dataBuilder.getJobsBuilderList()
                                    .size());
                    dataBuilder = MigrationData.newBuilder();
                }
            }
            if (!dataBuilder.getJobsBuilderList()
                    .isEmpty())
            {
                dataBuilder.build()
                        .writeTo(streamProvider.getOutputStream(type + "Jobs" + (++counter)));
                LOGGER.debug("Exported {} jobs",
                        dataBuilder.getJobsBuilderList()
                                .size());
            }
        }

        counter = 0;
        dataBuilder = MigrationData.newBuilder();
        for (String jobId : jobIds)
        {
            int page = 0;
            Set<Build> builds;
            do
            {
                builds = buildDao.getAll(jobId, page++, BATCH_SIZE);
                for (Build build : builds)
                {
                    exportBuild(dataBuilder.addBuildsBuilder(), build);
                }
                if (dataBuilder.getBuildsBuilderList()
                            .size() >= BATCH_SIZE)
                {
                    dataBuilder.build()
                            .writeTo(streamProvider.getOutputStream(type + "Builds" + (++counter)));
                    LOGGER.debug("Exported {} builds",
                            dataBuilder.getBuildsBuilderList()
                                    .size());
                    dataBuilder = MigrationData.newBuilder();
                }
            }
            while (builds.size() == BATCH_SIZE);
        }
        if (!dataBuilder.getBuildsBuilderList()
                .isEmpty())
        {
            dataBuilder.build()
                    .writeTo(streamProvider.getOutputStream(type + "Builds" + (++counter)));
            LOGGER.debug("Exported {} builds",
                    dataBuilder.getBuildsBuilderList()
                            .size());
        }

        MigrationData.Builder linkStatsBuilder = MigrationData.newBuilder();
        exportLinkStats(linkStatsBuilder);
        linkStatsBuilder.build()
                .writeTo(streamProvider.getOutputStream(type + "LinkStates"));
    }

    @Override
    public void doImportData(
            MigrationData migrationData,
            Feedback feedback)
    {
        if (migrationData.getSitesCount() > 0)
        {
            LOGGER.info("Importing {} sites...", migrationData.getSitesCount());
            for (SiteData siteData : migrationData.getSitesList())
            {
                importSite(siteData, feedback);
            }
        }
        if (migrationData.getJobsCount() > 0)
        {
            LOGGER.info("Importing {} jobs...", migrationData.getJobsCount());
            for (JobData jobData : migrationData.getJobsList())
            {
                importJob(jobData, new Site().setId(jobData.getSiteId()), feedback);
            }
        }
        if (migrationData.getBuildsCount() > 0)
        {
            LOGGER.info("Importing {} builds...", migrationData.getBuildsCount());
            for (BuildData buildData : migrationData.getBuildsList())
            {
                importBuild(buildData, new Job().setId(buildData.getJobId()), feedback);
            }
        }
        if (migrationData.getLinkStatsDataCount() > 0)
        {
            LOGGER.info("Importing link stats {}...", migrationData.getLinkStatsDataCount());
            for (LinkStatsData statsData : migrationData.getLinkStatsDataList())
            {
                LOGGER.debug("Importing link stats for {}", statsData.getIssueKey());
                linkStatisticsDao.importLinkStatistics(statsData.getIssueKey(), statsData.getStatsJson());
            }
            LOGGER.debug("Finishing importing link stats {}", migrationData.getLinkStatsDataCount());
            linkStatisticsDao.finishLinkStatisticsImports();
        }
    }

    @Override
    public void clear(Feedback feedback)
    {
        siteDao.deleteAll();
    }

    private void exportLinkStats(MigrationData.Builder builder)
    {
        LOGGER.debug("Exporting link stats...");
        int page = 0;
        int processed;
        BiConsumer<String, String> consumer = (issueKey, stateJson) -> builder.addLinkStatsDataBuilder()
                .setIssueKey(issueKey)
                .setStatsJson(stateJson);
        do
        {
            processed = linkStatisticsDao.getLinkStatisticianStates(page++, BATCH_SIZE, consumer);
        }
        while (processed == BATCH_SIZE);
    }

    private void exportSite(
            SiteData.Builder builder,
            Site site,
            boolean includeJobs)
    {
        LOGGER.info("Exporting site {}", site.getName());
        builder.setId(site.getId())
                .setScope(site.getScope())
                .setName(site.getName())
                .setSharedSecret(site.getSharedSecret())
                .setType(SiteData.Type.valueOf(site.getType()
                        .name()))
                .setRpcUrl(site.getRpcUrl()
                        .toASCIIString())
                .setUseCrumbs(site.isUseCrumbs())
                .setJenkinsPluginInstalled(site.isJenkinsPluginInstalled())
                .setRegistrationComplete(site.isRegistrationComplete())
                .setAutoLinkNewJobs(site.isAutoLinkNewJobs())
                .setConnectionType(SiteData.ConnectionType.valueOf(site.getConnectionType()
                        .name()));

        Optional.ofNullable(site.getUser())
                .ifPresent(builder::setUser);
        Optional.ofNullable(site.getToken())
                .ifPresent(builder::setToken);
        Optional.ofNullable(site.getDisplayUrlOrNull())
                .map(URI::toASCIIString)
                .ifPresent(builder::setDisplayUrl);

        if (includeJobs)
        {
            int pages = (int) Math.ceil(jobDao.countBySite(site.getId()) / (double) BATCH_SIZE);
            for (int page = 0; page < pages; page++)
            {
                for (Job job : jobDao.findBySiteId(site.getId(), page, BATCH_SIZE))
                {
                    exportJob(builder.addJobsBuilder(), job, true);
                }
            }
        }
    }

    private void exportJob(
            JobData.Builder builder,
            Job job,
            boolean includeBuilds)
    {
        LOGGER.debug("Exporting job {}", job.getName());
        builder.setId(job.getId())
                .setSiteId(job.getSite()
                        .getId())
                .setName(job.getName())
                .setFullName(job.getFullName())
                .setLastBuild(job.getLastBuild())
                .setOldestBuild(job.getOldestBuild())
                .setLinked(job.isLinked())
                .setDeleted(job.isDeleted());

        Optional.ofNullable(job.getParentName())
                .ifPresent(builder::setParentName);
        Optional.ofNullable(job.getUrlNameOrNull())
                .ifPresent(builder::setUrlName);
        Optional.ofNullable(job.getDisplayNameOrNull())
                .ifPresent(builder::setDisplayName);
        Optional.ofNullable(job.getDescription())
                .ifPresent(builder::setDescription);

        if (includeBuilds)
        {
            int page = 0;
            Set<Build> builds;
            do
            {
                builds = buildDao.getAll(job.getId(), page++, BATCH_SIZE);
                for (Build build : builds)
                {
                    exportBuild(builder.addBuildsBuilder(), build);
                }
            }
            while (builds.size() == BATCH_SIZE);
        }
    }

    private void exportBuild(
            BuildData.Builder builder,
            Build build)
    {
        LOGGER.debug("Exporting build {}", build.getDisplayName());
        builder.setId(build.getId())
                .setJobId(build.getJob()
                        .getId())
                .setNumber(build.getNumber())
                .setDeleted(build.isDeleted())
                .setResult(BuildData.Result.valueOf(build.getResult()
                        .name()))
                .setDuration(build.getDuration())
                .setTimestamp(build.getTimestamp());

        Optional.ofNullable(build.getDisplayNameOrNull())
                .ifPresent(builder::setDisplayName);
        Optional.ofNullable(build.getDescription())
                .ifPresent(builder::setDescription);
        Optional.ofNullable(build.getCause())
                .ifPresent(builder::setCause);
        Optional.ofNullable(build.getBuiltOnOrNull())
                .ifPresent(builder::setBuiltOn);

        Optional.ofNullable(build.getTestResults())
                .ifPresent(testResults -> builder.getTestResultsBuilder()
                        .setTotal(testResults.getTotal())
                        .setFailed(testResults.getFailed())
                        .setSkipped(testResults.getSkipped()));
        build.getDeploymentEnvironments()
                .forEach(env -> builder.addDeploymentEnvironmentsBuilder()
                        .setId(env.getId())
                        .setName(env.getName())
                        .setType(BuildData.DeploymentEnvironment.DeploymentEnvironmentType.valueOf(env.getType()
                                .name())));

        issueToBuildDao.getIssueLinks(build)
                .forEach(reference -> builder.addIssueReferences(reference.getIssueKey()));
    }

    private void importSite(
            SiteData siteData,
            Feedback feedback)
    {
        LOGGER.info("Importing site {}", siteData.getName());
        Site site = new Site().setId(siteData.getId())
                .setName(siteData.getName())
                .setScope(siteData.getScope())
                .setSharedSecret(siteData.getSharedSecret())
                .setType(SiteType.valueOf(siteData.getType()
                        .name()))
                .setRpcUrl(URI.create(siteData.getRpcUrl()))
                .setDisplayUrl(Optional.ofNullable(siteData.getDisplayUrl())
                        .filter(StringUtils::isNotBlank)
                        .map(URI::create)
                        .orElse(null))
                .setUser(siteData.getUser())
                .setToken(siteData.getToken())
                .setUseCrumbs(siteData.getUseCrumbs())
                .setJenkinsPluginInstalled(siteData.getJenkinsPluginInstalled())
                .setRegistrationComplete(siteData.getRegistrationComplete())
                .setAutoLinkNewJobs(siteData.getAutoLinkNewJobs())
                .setConnectionType(SiteConnectionType.valueOf(siteData.getConnectionType()
                        .name()));

        ErrorMessages errorMessages = siteValidator.validate(site);
        if (errorMessages.hasErrors())
        {
            site.setInaccessibleSite()
                    .setRegistrationComplete(false)
                    .setJenkinsPluginInstalled(false)
                    .setUseCrumbs(true);
            String errors = errorMessages.getErrors()
                    .stream()
                    .map(ErrorMessages.ErrorMessage::getMessage)
                    .collect(joining("<br>"));
            feedback.addWarning(textResolver.getText("migration.site.validation.errors", site.getName(), errors));
        }

        if (!site.isInaccessibleSite())
        {
            site.setJenkinsPluginInstalled(siteClient.isJenkinsPluginInstalled(site));
            site.setRegistrationComplete(false);
        }

        try
        {
            Site newSite = siteDao.save(site);

            feedback.addInfo(textResolver.getText("migration.manual.reregistration.required", newSite.getName()));

            for (JobData jobData : siteData.getJobsList())
            {
                importJob(jobData, newSite, feedback);
            }
        }
        catch (Exception e)
        {
            LOGGER.error("Failed to import site {}, {}", site.getName(), e.getMessage(), e);
            feedback.addError(textResolver.getText("migration.site.import.failed", site.getName()));
        }
    }

    private void importJob(
            JobData jobData,
            Site newSite,
            Feedback feedback)
    {
        LOGGER.debug("Importing job {}", jobData.getName());
        Job newJob = jobDao.save(new Job().setSite(newSite)
                .setId(jobData.getId())
                .setName(jobData.getName())
                .setParentName(jobData.getParentName())
                .setFullName(jobData.getFullName())
                .setUrlName(jobData.getUrlName())
                .setDisplayName(jobData.getDisplayName())
                .setDescription(jobData.getDescription())
                .setLastBuild(jobData.getLastBuild())
                .setOldestBuild(jobData.getOldestBuild())
                .setLinked(jobData.getLinked())
                .setDeleted(jobData.getDeleted()));

        for (BuildData buildData : jobData.getBuildsList())
        {
            importBuild(buildData, newJob, feedback);
        }
    }

    private void importBuild(
            BuildData buildData,
            Job newJob,
            Feedback feedback)
    {
        LOGGER.debug("Importing build {} #{}", newJob.getName(), buildData.getNumber());
        Build build = new Build().setJob(newJob)
                .setId(buildData.getId())
                .setNumber(buildData.getNumber())
                .setDisplayName(buildData.getDisplayName())
                .setDescription(buildData.getDescription())
                .setDeleted(buildData.getDeleted())
                .setCause(buildData.getCause())
                .setResult(Result.valueOf(buildData.getResult()
                        .name()))
                .setBuiltOn(buildData.getBuiltOn())
                .setDuration(buildData.getDuration())
                .setTimestamp(buildData.getTimestamp());

        Optional.ofNullable(buildData.getTestResults())
                .map(data -> new TestResults().setTotal(data.getTotal())
                        .setFailed(data.getFailed())
                        .setSkipped(data.getSkipped()))
                .ifPresent(build::setTestResults);
        build.setDeploymentEnvironments(buildData.getDeploymentEnvironmentsList()
                .stream()
                .map(data -> new DeploymentEnvironment().setId(data.getId())
                        .setName(data.getName())
                        .setType(DeploymentEnvironmentType.valueOf(data.getType()
                                .name())))
                .collect(Collectors.toList()));

        Build newBuild = buildDao.save(build);

        Set<IssueReference> references = new HashSet<>(buildData.getIssueReferencesList()).stream()
                .map(key -> new IssueReference().setIssueKey(key))
                .collect(toSet());
        issueToBuildDao.relink(newBuild, references);
    }
}
