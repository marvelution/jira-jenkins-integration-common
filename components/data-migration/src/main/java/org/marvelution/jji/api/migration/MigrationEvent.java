/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.api.migration;

public class MigrationEvent
{

    // See https://developer.atlassian.com/platform/app-migration/events/#types-of-events
    public static final String APP_DATA_UPLOADED = "app-data-uploaded";
    public static final String LISTENER_TRIGGERED = "listener-triggered";
    public static final String LISTENER_ERRORED = "listener-errored";
    public static final String TRANSFER_CANCELLATION_REQUESTED = "transfer-cancellation-requested";
    private String messageId;
    private String cloudAppKey;
    private String transferId;
    private MigrationDetails migrationDetails;
    private String eventType;
    private String s3Key;
    private String label;

    public String getMessageId()
    {
        return messageId;
    }

    public MigrationEvent setMessageId(String messageId)
    {
        this.messageId = messageId;
        return this;
    }

    public String getCloudAppKey()
    {
        return cloudAppKey;
    }

    public MigrationEvent setCloudAppKey(String cloudAppKey)
    {
        this.cloudAppKey = cloudAppKey;
        return this;
    }

    public String getTransferId()
    {
        return transferId;
    }

    public MigrationEvent setTransferId(String transferId)
    {
        this.transferId = transferId;
        return this;
    }

    public MigrationDetails getMigrationDetails()
    {
        return migrationDetails;
    }

    public MigrationEvent setMigrationDetails(MigrationDetails migrationDetails)
    {
        this.migrationDetails = migrationDetails;
        return this;
    }

    public String getEventType()
    {
        return eventType;
    }

    public MigrationEvent setEventType(String eventType)
    {
        this.eventType = eventType;
        return this;
    }

    public String getS3Key()
    {
        return s3Key;
    }

    public MigrationEvent setS3Key(String s3Key)
    {
        this.s3Key = s3Key;
        return this;
    }

    public String getLabel()
    {
        return label;
    }

    public MigrationEvent setLabel(String label)
    {
        this.label = label;
        return this;
    }
}
