/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.api.migration;

public enum ProgressStatus
{
    READY("new"),
    IN_PROGRESS("inprogress"),
    SUCCESS("success"),
    FAILED("removed"),
    TIMEDOUT("removed"),
    SKIPPED("moved"),
    CANCELLED("removed"),
    INCOMPLETE("moved");

    public final String lozengeType;

    ProgressStatus(String lozengeType)
    {
        this.lozengeType = lozengeType;
    }

    // See https://developer.atlassian.com/platform/app-migration/rest/api-group-status-api/#api-group-status-api
    public ProgressStatus forAtlassian()
    {
        if (this == READY)
        {
            return IN_PROGRESS;
        }
        else if (this == SKIPPED || this == CANCELLED)
        {
            return INCOMPLETE;
        }
        else if (this == TIMEDOUT)
        {
            return FAILED;
        }
        else
        {
            return this;
        }
    }
}
