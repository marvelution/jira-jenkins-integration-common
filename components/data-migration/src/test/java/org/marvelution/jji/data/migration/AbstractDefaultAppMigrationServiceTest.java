/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.migration;

import java.io.*;
import java.net.*;
import java.nio.file.*;
import java.time.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.*;
import java.util.stream.*;
import java.util.zip.*;

import org.marvelution.jji.api.migration.*;
import org.marvelution.jji.data.access.api.*;
import org.marvelution.jji.data.services.api.*;
import org.marvelution.jji.model.*;
import org.marvelution.jji.synctoken.utils.*;
import org.marvelution.testing.*;

import jakarta.inject.*;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.io.*;

import static org.assertj.core.api.Assertions.*;

public abstract class AbstractDefaultAppMigrationServiceTest
        extends TestSupport
{
    @TempDir
    Path tempDir;
    @Inject
    @javax.inject.Inject
    private ConfigurationService configurationService;
    @Inject
    @javax.inject.Inject
    private SiteDAO siteDao;
    @Inject
    @javax.inject.Inject
    private JobDAO jobDao;
    @Inject
    @javax.inject.Inject
    private BuildDAO buildDao;
    @Inject
    @javax.inject.Inject
    private AppMigrationService migrationService;

    @Test
    void testFullExport()
            throws Exception
    {
        setupTestData();

        Path output = Files.createFile(tempDir.resolve("full-export"));
        try (OutputStream outputStream = Files.newOutputStream(output, StandardOpenOption.TRUNCATE_EXISTING))
        {
            migrationService.export(outputStream,
                    new ExportOptions().setIncludeConfiguration(true)
                            .setIncludeIntegration(true));
        }
        assertThat(Files.size(output)).isGreaterThan(100000);
        try (InputStream input = Files.newInputStream(output, StandardOpenOption.READ))
        {
            MigrationData export = MigrationData.parseFrom(input);
            assertThat(export.getConfiguration()
                    .getSettingsCount()).isGreaterThanOrEqualTo(1);
            assertThat(export).extracting(MigrationData::getSitesCount,
                            MigrationData::getJobsCount,
                            MigrationData::getBuildsCount)
                    .containsOnly(1, 0, 0);
            SiteData siteData = export.getSites(0);
            assertThat(siteData).extracting(SiteData::getJobsCount)
                    .isEqualTo(200);
            AtomicInteger buildCount = new AtomicInteger();
            siteData.getJobsList()
                    .forEach(jobData -> {
                        buildCount.addAndGet(jobData.getBuildsCount());
                        assertThat(jobData).extracting(JobData::getBuildsCount)
                                .isEqualTo(10);
                    });
            assertThat(buildCount.get()).isEqualTo(200 * 10);
        }
    }

    @Test
    void testZippedExport()
            throws Exception
    {
        setupTestData();

        Path output = Files.createFile(tempDir.resolve("zipped-export"));
        try (OutputStream outputStream = new ZipOutputStream(Files.newOutputStream(output, StandardOpenOption.TRUNCATE_EXISTING)))
        {
            migrationService.export(outputStream,
                    new ExportOptions().setIncludeConfiguration(true)
                            .setIncludeIntegration(true));
        }
        int entryCount = 0;
        assertThat(Files.size(output)).isGreaterThan(19000);
        try (ZipInputStream input = new ZipInputStream(Files.newInputStream(output, StandardOpenOption.READ)))
        {
            ZipEntry entry = input.getNextEntry();
            while (entry != null)
            {
                entryCount++;

                MigrationData export = MigrationData.parseFrom(input);

                if (Objects.equals("Configuration", entry.getName()))
                {
                    assertThat(export.getConfiguration()
                            .getSettingsCount()).isGreaterThanOrEqualTo(1);
                }
                else
                {
                    assertThat(export.hasConfiguration()).isFalse();
                }
                if (Objects.equals("IntegrationSites", entry.getName()))
                {
                    assertThat(export.getSitesCount()).isOne();
                    assertThat(export.getSites(0)).extracting(SiteData::getJobsCount)
                            .isEqualTo(0);
                }
                else
                {
                    assertThat(export.getSitesCount()).isZero();
                }
                if (entry.getName()
                        .startsWith("IntegrationJobs"))
                {
                    assertThat(export.getJobsCount()).isEqualTo(100);
                }
                else
                {
                    assertThat(export.getJobsCount()).isZero();
                }
                if (entry.getName()
                        .startsWith("IntegrationBuilds"))
                {
                    assertThat(export.getBuildsCount()).isEqualTo(100);
                }
                else
                {
                    assertThat(export.getBuildsCount()).isZero();
                }

                entry = input.getNextEntry();
            }

            assertThat(entryCount).isEqualTo(25);
        }
    }

    protected void setupTestData()
    {
        configurationService.saveConfigurationSetting(ConfigurationSetting.forKey("new-key")
                .setValue("new-val"));
        configurationService.saveConfigurationSetting(ConfigurationSetting.forKey("new-sys-key")
                .setValue("new-sys-val")
                .setOverridable(false));

        Site site = siteDao.save(new Site().setId("site-1")
                .setName("Site 1")
                .setType(SiteType.JENKINS)
                .setRpcUrl(URI.create("https://jenkins.example.com"))
                .setAutoLinkNewJobs(true)
                .setInaccessibleSite()
                .setEnabled(true)
                .setJenkinsPluginInstalled(true)
                .setRegistrationComplete(true)
                .setSharedSecret(SharedSecretGenerator.generate()));

        IntStream.range(1, 201)
                .mapToObj(counter -> new Job().setId("job-" + counter)
                        .setSite(site)
                        .setName("Job " + counter)
                        .setDisplayName("Test Job " + counter)
                        .setUrlName("Job-" + counter)
                        .setDescription("My Test Job " + counter)
                        .setDeleted(false)
                        .setLinked(true))
                .map(jobDao::save)
                .forEach(job -> IntStream.range(1, 11)
                        .mapToObj(number -> new Build().setId(job.getId() + "-" + number)
                                .setJob(job)
                                .setNumber(number)
                                .setDescription("Build " + number)
                                .setCause("Testing")
                                .setDuration(TimeUnit.MILLISECONDS.toSeconds(50))
                                .setTimestamp(Instant.now()
                                        .toEpochMilli())
                                .setResult(Result.SUCCESS)
                                .setTestResults(new TestResults().setTotal(100)
                                        .setFailed(0)
                                        .setSkipped(10)))
                        .forEach(buildDao::save));
    }
}
