/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.migration;

import java.io.ByteArrayOutputStream;
import java.net.URI;
import java.util.*;

import org.marvelution.jji.api.features.FeatureFlag;
import org.marvelution.jji.api.features.Features;
import org.marvelution.jji.api.features.InMemoryFeatureStore;
import org.marvelution.jji.api.migration.AppMigrationService;
import org.marvelution.jji.api.migration.ExportOptions;
import org.marvelution.jji.api.text.TextResolver;
import org.marvelution.jji.data.access.api.*;
import org.marvelution.jji.data.services.api.ConfigurationService;
import org.marvelution.jji.data.services.api.SiteClient;
import org.marvelution.jji.data.services.api.SiteValidator;
import org.marvelution.jji.model.*;
import org.marvelution.jji.synctoken.utils.SharedSecretGenerator;
import org.marvelution.testing.TestSupport;

import org.assertj.core.groups.Tuple;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static java.util.Arrays.asList;
import static org.apache.commons.lang3.StringUtils.defaultIfBlank;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;
import static org.marvelution.jji.api.features.FeatureFlags.DATA_MIGRATION;
import static org.marvelution.jji.data.migration.DefaultAppMigrationService.BATCH_SIZE;
import static org.mockito.Mockito.*;

class DefaultAppMigrationServiceTest
        extends TestSupport
{

    private Features features;
    @Mock
    private ConfigurationService configurationService;
    @Mock
    private SiteDAO siteDao;
    @Mock
    private JobDAO jobDao;
    @Mock
    private BuildDAO buildDao;
    @Mock
    private IssueToBuildDAO issueToBuildDao;
    @Mock
    private LinkStatisticsDAO linkStatisticsDao;
    @Mock
    private SiteValidator siteValidator;
    @Mock
    private SiteClient siteClient;
    @Mock
    private TextResolver textResolver;
    private AppMigrationService migrationService;

    @BeforeEach
    void setUp()
    {
        features = new Features(new InMemoryFeatureStore())
        {
            @Override
            protected Set<FeatureFlag> loadFeatureFlags()
            {
                return Collections.singleton(new FeatureFlag(DATA_MIGRATION, true, true));
            }
        };
        migrationService = new DefaultAppMigrationService(features,
                new ConfigurationMigrator(textResolver, configurationService),
                new IntegrationMigrator(textResolver,
                        siteDao,
                        jobDao,
                        buildDao,
                        issueToBuildDao,
                        linkStatisticsDao,
                        siteValidator,
                        siteClient),
                textResolver);
    }

    @Test
    void testExportFullWithMigrationDisabled()
            throws Exception
    {
        features.disableFeature(DATA_MIGRATION);

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        migrationService.export(outputStream, new ExportOptions());

        assertThat(outputStream.size()).isZero();

        verifyNoMoreInteractions(configurationService, siteDao, jobDao, buildDao, issueToBuildDao);
    }

    @Test
    void testFullExport()
            throws Exception
    {
        // Integration data setup
        when(configurationService.getAllConfigurationSettings()).thenReturn(Arrays.asList(ConfigurationSetting.forKey("conf-key")
                        .setValue("config-value"),
                ConfigurationSetting.forKey("key")
                        .setValue(true),
                ConfigurationSetting.forKey("non-overrideable-key")
                        .setValue(true)
                        .setOverridable(false)));

        Site publicSite = new Site().setId(UUID.randomUUID()
                        .toString())
                .setType(SiteType.JENKINS)
                .setName("Public")
                .setRpcUrl(URI.create("https://jenkins.internal:8443"))
                .setDisplayUrl(URI.create("https://jenkins.example.com"))
                .setConnectionType(SiteConnectionType.ACCESSIBLE)
                .setUser("admin")
                .setToken("admin")
                .setSharedSecret(SharedSecretGenerator.generate())
                .setUseCrumbs(false)
                .setJenkinsPluginInstalled(true)
                .setRegistrationComplete(true);
        Site privateSite = new Site().setId(UUID.randomUUID()
                        .toString())
                .setType(SiteType.HUDSON)
                .setName("Private")
                .setRpcUrl(URI.create("https://hudson.example.com"))
                .setSharedSecret(SharedSecretGenerator.generate())
                .setAutoLinkNewJobs(true)
                .setConnectionType(SiteConnectionType.INACCESSIBLE)
                .setUseCrumbs(true)
                .setJenkinsPluginInstalled(true)
                .setRegistrationComplete(true);

        Job rootJob = new Job().setId(UUID.randomUUID()
                        .toString())
                .setSite(publicSite)
                .setName("Root Job")
                .setUrlName("Root+Job")
                .setLastBuild(0)
                .setOldestBuild(0)
                .setLinked(true)
                .setDeleted(false)
                .setDescription("Description of the root job");
        Job subJob = new Job().setId(UUID.randomUUID()
                        .toString())
                .setSite(publicSite)
                .setName("Sub Job")
                .setLastBuild(10)
                .setOldestBuild(1)
                .setLinked(false)
                .setDeleted(true)
                .setParentName(rootJob.getName())
                .setUrlName(rootJob.getUrlName() + "/job/Sub+Job");

        when(siteDao.getAll()).thenReturn(asList(publicSite, privateSite));
        when(jobDao.countBySite(publicSite.getId())).thenReturn(2L);
        when(jobDao.findBySiteId(publicSite.getId(), 0, BATCH_SIZE)).thenReturn(asList(rootJob, subJob));
        when(jobDao.countBySite(privateSite.getId())).thenReturn(0L);

        // Perform export
        byte[] export = migrationService.export(new ExportOptions().setName("Test")
                .setIncludeConfiguration(true)
                .setIncludeIntegration(true));

        assertThat(export).isNotEmpty();
        MigrationData data = MigrationData.parseFrom(export);

        // Assert Integration export
        assertThat(data.getConfiguration()
                .getSettingsList()).hasSize(2)
                .extracting(ConfigurationSettingData::getKey, ConfigurationSettingData::getValue)
                .containsOnly(tuple("conf-key", "config-value"), tuple("key", "true"));

        assertThat(data.getSitesList()).hasSize(2)
                .extracting(SiteData::getId,
                        SiteData::getName,
                        SiteData::getType,
                        SiteData::getRpcUrl,
                        SiteData::getDisplayUrl,
                        SiteData::getUser,
                        SiteData::getToken,
                        SiteData::getSharedSecret,
                        SiteData::getAutoLinkNewJobs,
                        SiteData::getUseCrumbs,
                        SiteData::getJenkinsPluginInstalled,
                        SiteData::getRegistrationComplete,
                        SiteData::getConnectionType)
                .containsOnly(siteDataTuple(publicSite), siteDataTuple(privateSite));

        assertThat(data.getSites(0)
                .getJobsList()).extracting(JobData::getId,
                        JobData::getSiteId,
                        JobData::getName,
                        JobData::getParentName,
                        JobData::getFullName,
                        JobData::getUrlName,
                        JobData::getDisplayName,
                        JobData::getDescription,
                        JobData::getLastBuild,
                        JobData::getOldestBuild,
                        JobData::getLinked,
                        JobData::getDeleted)
                .containsOnly(jobDataTuple(rootJob), jobDataTuple(subJob));

        verify(configurationService).getAllConfigurationSettings();
        verify(siteDao).getAll();
        verify(jobDao).countBySite(publicSite.getId());
        verify(jobDao).findBySiteId(publicSite.getId(), 0, BATCH_SIZE);
        verify(jobDao).countBySite(privateSite.getId());
    }

    private Tuple siteDataTuple(Site site)
    {
        return tuple(site.getId(),
                site.getName(),
                SiteData.Type.valueOf(site.getType()
                        .name()),
                site.getRpcUrl()
                        .toASCIIString(),
                Optional.ofNullable(site.getDisplayUrlOrNull())
                        .map(URI::toASCIIString)
                        .orElse(""),
                defaultIfBlank(site.getUser(), ""),
                defaultIfBlank(site.getToken(), ""),
                site.getSharedSecret(),
                site.isAutoLinkNewJobs(),
                site.isUseCrumbs(),
                site.isJenkinsPluginInstalled(),
                site.isRegistrationComplete(),
                SiteData.ConnectionType.valueOf(site.getConnectionType()
                        .name()));
    }

    private Tuple jobDataTuple(Job job)
    {
        return tuple(job.getId(),
                job.getSite()
                        .getId(),
                job.getName(),
                defaultIfBlank(job.getParentName(), ""),
                job.getFullName(),
                defaultIfBlank(job.getUrlNameOrNull(), ""),
                defaultIfBlank(job.getDisplayNameOrNull(), ""),
                defaultIfBlank(job.getDescription(), ""),
                job.getLastBuild(),
                job.getOldestBuild(),
                job.isLinked(),
                job.isDeleted());
    }
}
