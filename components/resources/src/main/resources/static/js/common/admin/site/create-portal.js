/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function ($) {
    'use strict';

    $('.jenkins-portal-container')
        .on('portal.next', function (event) {
            if (event.portal.getPortalActivePage().prop('id') === 'advanced-options') {
                event.eventData.validate = false;
            }
        })
        .on('portal.page', function (event) {
            event.portal.reloadOnHide();

            const activePage = event.portal.getPortalActivePage();
            //event.portal.getButton('next').text(AJS.I18n.getText('next'));
            if (activePage.prop('id') === 'create-site') {
                activePage.find('input:radio[name=connection]').off().click(function () {
                    $('.site-setup').toggleClass('hidden', true).disableFormElements(true);
                    const connType = $(this).val();
                    const checked = $(this).is(':checked');
                    $('#' + connType + '-site-setup').toggleClass('hidden', !checked).disableFormElements(!checked);
                });
                activePage.find('input:radio[name=authentication]').off().click(function () {
                    $('.auth-fields').toggleClass('hidden', true).disableFormElements(true);
                    const authType = $(this).val();
                    const checked = $(this).is(':checked');
                    $('#' + authType + '-auth-fields').toggleClass('hidden', !checked).disableFormElements(!checked);
                });
                event.portal.addButton({
                    id: 'save-and-connect',
                    text: AJS.I18n.getText('save.and.connect'),
                    primary: true,
                    shouldShow: 'create-site',
                    handler: function (e) {
                        e.preventDefault();
                        event.portal.submitForm($(e.target), function (data) {
                            event.portal.setPortalActivePage('connect-site', data)
                        });
                    }
                });
            } else if (activePage.prop('id') === 'advanced-options') {
                let site = event.portal.serializeForm();
                const notInAccessible = site.connection !== 'INACCESSIBLE'
                activePage.find('input[name=useCrumbs]')
                    .disableFormElements(!notInAccessible)
                    .closest('div.checkbox')
                    .toggleClass('hidden', !notInAccessible);
                const accessible = site.connection === 'ACCESSIBLE'
                activePage.find('input[name=displayUrl]')
                    .disableFormElements(!accessible)
                    .closest('div.field-group')
                    .toggleClass('hidden', !accessible);
            } else if (activePage.prop('id') === 'connect-site') {
                event.portal.getButton('back').toggleClass('hidden', true);
                event.portal.addButton({
                    id: 'connect-later',
                    text: AJS.I18n.getText('connect.later'),
                    shouldShow: 'connect-site',
                    handler: function (e) {
                        e.preventDefault();
                        event.portal.hide();
                    }
                });
            }
        });
})
(jQuery);
