/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function ($) {
    'use strict';

    const script = document.currentScript;
    const fieldId = script.getAttribute('data-field-id');
    const statusUrl = script.getAttribute('data-status-url');

    let statusHandler = function () {
        $.ajax(statusUrl, {dataType: 'json'}).done(function (status) {
            let statusClass;
            switch (status.status) {
                case 'OFFLINE':
                    statusClass = 'removed';
                    break;
                case 'ONLINE':
                    statusClass = 'success';
                    break;
                case 'NOT_ACCESSIBLE':
                    statusClass = 'moved';
                    break;
                case 'UNREACHABLE':
                    statusClass = 'new';
                    break;
                case 'GETTING_READY':
                    statusClass = 'inprogress';
                    break;
                default:
                    statusClass = 'default';
                    break;
            }
            let version;
            if (status.pluginInstalled) {
                version = status.pluginVersion.formatted;
            } else {
                version = 'UNKNOWN';
            }
            $(fieldId)
                .removeClass (function (index, className) {
                    return (className.match (/\baui-lozenge-\S+/g) || []).join(' ');
                })
                .toggleClass(`aui-lozenge-${statusClass}`, true)
                .attr('title', status.statusDescription)
                .text(status.status.replace(/[^a-z0-9]/gi, ' ') + ` (${version})`);
            AJS.$(fieldId).tooltip();
        });
    };

    statusHandler();

    setInterval(statusHandler, 30000);

})
(jQuery);
