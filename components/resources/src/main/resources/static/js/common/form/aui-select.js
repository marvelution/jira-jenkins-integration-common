/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function ($) {
    'use strict';

    $.extend({
        formatWithAvatar: function (size, data) {
            let avatar = '';
            if (data.imgSrc) {
                avatar = '<span class="aui-avatar aui-avatar-project aui-avatar-' + size + '"><span class="aui-avatar-inner">' +
                    '<img onerror="this.style.display = \'none\'" src="' + data.imgSrc + '"/></span></span>';
            }
            return '<span>' + avatar + AJS.escapeHtml(data.text) + '</span>';
        },
        transformOption: function (data) {
            return {
                id: data.key || data.id,
                text: data.name,
                imgSrc: data.iconUrl || data.avatarUrls['16x16']
            }
        }
    });

    const script = document.currentScript;
    const fieldId = script.getAttribute('data-field-id');

    $(`#${fieldId}`).auiSelect2({
        hasAvatar: true,
        minimumInputLength: 3,
        allowClear: true,
        ajax: {
            url: function (term) {
                return $(this).attr('data-url') + '?q=' + term;
            },
            dataType: 'json',
            results: function (data) {
                let results = [];
                $(data).each(function () {
                    results.push($.transformOption(this));
                });
                return {
                    results: results
                }
            }
        },
        initSelection: function (element, callback) {
            const $element = $(element);
            const id = $element.val();
            if (id !== "") {
                $.ajax($element.attr('data-url') + '/' + id, {dataType: 'json'}).done(function (data) {
                    callback($.transformOption(data));
                });
            }
        },
        formatResult: function (result) {
            return $.formatWithAvatar('xsmall', result);
        },
        formatSelection: function (result) {
            return $.formatWithAvatar('xsmall', result);
        },
        escapeMarkup: function (m) {
            return m;
        }
    });
})
(jQuery);
