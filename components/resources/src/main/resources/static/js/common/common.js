/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function ($) {
    'use strict';

    $.fn.extend({
        serializeJson: function () {
            const o = {};
            const a = this.serializeArray();
            $.each(a, function () {
                if (o[this.name]) {
                    if (!o[this.name].push) {
                        o[this.name] = [o[this.name]];
                    }
                    o[this.name].push(this.value || '');
                } else {
                    o[this.name] = this.value || '';
                }
            });
            return o;
        },
        formElements: function () {
            return this.map(function () {
                const elements = $.prop(this, "elements");
                return elements ? $.makeArray(elements) : this;
            });
        },
        disableFormElements: function (disabled) {
            this.prop('disabled', disabled).formElements().prop('disabled', disabled);
            return this;
        },

        isEmpty: function (){
            return !$.trim(this.html());
        },

        isBusy: function () {
            if (typeof (this[0].isBusy) === 'function') {
                return this[0].isBusy();
            } else if (typeof (this[0].busy) === 'boolean') {
                return this[0].busy;
            } else {
                return this.data('busy');
            }
        },
        setBusy: function () {
            if (typeof (this[0].busy) === 'function') {
                this[0].busy();
            } else if (typeof (this[0].busy) === 'boolean') {
                this[0].busy = true;
            } else {
                this.data('busy', true);
            }
            return this;
        },
        setIdle: function () {
            if (typeof (this[0].idle) === 'function') {
                this[0].idle();
            } else if (typeof (this[0].busy) === 'boolean') {
                this[0].busy = false;
            } else {
                this.data('busy', false);
            }
            return this;
        },

        addRawHTML: function (rawHtml, method) {
            let html = $.parseHTML(rawHtml, true);
            $(html).find('script').each(function () {
                if (!html.includes(this)) {
                    html.push(this);
                }
            });

            let previousElement = undefined;
            for (let e = 0; e < html.length; e++) {
                let element = html[e];
                if (element instanceof HTMLScriptElement) {
                    let script = document.createElement('script');
                    for (let i = 0; i < element.attributes.length; i++) {
                        script.setAttribute(element.attributes[i].name, element.attributes[i].value);
                    }
                    element = script;
                }
                if (previousElement !== undefined) {
                    previousElement.after(element);
                } else {
                    switch (method) {
                        case 'before':
                            this[0].before(element);
                            break;
                        case 'after':
                            this[0].after(element);
                            break;
                        case 'replace':
                            this[0].replaceWith(element);
                            break;
                        default:
                            this[0].append(element);
                            break;
                    }
                }
                previousElement = element;
            }
            return this;
        }
    });
})(jQuery);
