/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function ($) {
    'use strict';

    $(document).on('click', 'input:radio[name=authentication]', function () {
        $('.auth-fields').toggleClass('hidden', true).find('input').prop('disabled', true);
        const authType = $(this).val();
        const checked = $(this).is(':checked');
        $('#' + authType + '-auth-fields').toggleClass('hidden', !checked).find('input').prop('disabled', !checked);
    });

    $(document).on('click', 'input:radio[name=firewalled]', function () {
        const firewalled = $(this).is(':checked') && $(this).val() === 'true';
        $('#private-instance').toggleClass('hidden', !firewalled).find('input').prop('disabled', !firewalled);
        $('#public-instance').toggleClass('hidden', firewalled).find('input').prop('disabled', firewalled);
        $('#useCrumbs').parent().toggleClass('hidden', firewalled);
    });
})
(jQuery);
