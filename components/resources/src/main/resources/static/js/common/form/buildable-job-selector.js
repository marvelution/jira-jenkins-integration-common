/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function ($) {
    'use strict';

    const script = document.currentScript;
    const fieldId = script.getAttribute('data-field-id');

    $(`#${fieldId}`).auiSelect2({
        allowClear: true,
        minimumInputLength: 3,
        multiple: script.getAttribute('data-multiple') === 'true',
        ajax: {
            url: function (term) {
                return $(this).attr('data-url') + '?term=' + term;
            },
            dataType: 'json',
            results: function (data) {
                return {
                    results: data
                }
            }
        },
        initSelection: function (element, callback) {
            const $element = $(element);
            const value = $element.val();
            if (value !== "") {
                $.ajax($element.attr('data-url') + '/' + value, {dataType: 'json'}).done(function (data) {
                    callback(data);
                });
            }
        },
        formatSelection: function (data, container, escapeMarkup) {
            return data ? escapeMarkup(data.displayName) : undefined;
        },
        formatResult: function (result, container, query, escapeMarkup) {
            let markup = [];
            Select2.util.markMatch(result.displayName, query.term, markup, escapeMarkup);
            return markup.join("");
        }
    });
})
(jQuery);
