/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function ($) {
    'use strict';

    $(document)
        .on('click', 'input:radio[name=method]', function () {
            $('fieldset.method-details').toggleClass('hidden', true).disableFormElements(true);
            const method = $(this).val();
            const checked = $(this).is(':checked');
            $(`#${method}-method-details`).toggleClass('hidden', !checked).disableFormElements(!checked);
        })
        .on('click', '#registrationToken', function () {
            $(this).select();
        })
        .on('click', '#registrationSecret', function () {
            $(this).select();
        });

    $('.jenkins-portal-container')
        .on('portal.submit', function (event) {
            event.portal.reloadOnHide();
            event.portal.getPortalActivePage().find('#connect-errors').empty();

            const data = JSON.parse(event.eventData.options.data);
            data.sharedSecret = undefined;
            data.registrationToken = undefined;
            data.registrationSecret = undefined;
            event.eventData.options.data = JSON.stringify(data);
        })
        .on('portal.submit-done', function (event) {
            event.portal.reloadOnHide();
        })
        .on('portal.submit-fail', function (event) {
            event.preventDefault();
            const {data, xhr} = event.eventData;
            const message = {title: '', body: '', insert: 'prepend'};
            switch (xhr.status) {
                case 424:
                    message.title = AJS.I18n.getText('automatic.registration.failed');
                    message.body = AJS.I18n.getText('automatic.registration.failed.body');
                    break;
                case 417:
                    message.title = AJS.I18n.getText('manual.registration.failed');
                    message.body = AJS.I18n.getText('manual.registration.failed.body');
                    break;
                default:
                    message.title = AJS.I18n.getText('registration.failed');
                    message.body = AJS.I18n.getText('registration.failed.body');
                    break;
            }
            if (message.body !== '') {
                message.body = `<p>${message.body}</p>`;
            }
            AJS.messages.error('#connect-errors', message);
        });
})
(jQuery);
