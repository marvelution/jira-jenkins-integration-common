/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function ($) {
    'use strict';

    $(document).on('change', '#show-full-history', function (event) {
        let checked = this.checked;
        if (typeof (checked) !== 'boolean') {
            checked = checked !== null && checked !== undefined;
        }

        $('.jenkins-builds-dialog table.aui-table-list').toggleClass('collapsed', !checked);
    });
})
(jQuery);
