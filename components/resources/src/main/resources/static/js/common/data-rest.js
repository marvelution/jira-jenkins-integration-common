/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function ($) {
    'use strict';

    const dataRestEventHandler = function (event) {
        const $this = $(this);
        if ($this.is(':disabled') || $this.attr('disabled')) {
            return;
        }

        event.preventDefault();

        if (!$this.isBusy()) {
            $this.setBusy();

            let checked = this.checked;
            if (typeof (checked) !== 'boolean') {
                checked = checked !== null && checked !== undefined;
            }

            let options = {
                url: $this.attr('data-rest'),
                type: $this.attr('data-rest-method')
            };
            $.each(this.attributes, function (index, attr) {
                if (attr.name.startsWith('data-rest-data-')) {
                    options.data = options.data || {};
                    options.data[attr.name.substring('data-rest-data-'.length)] = attr.value;
                }
            });
            if (options.data === undefined && checked !== undefined) {
                options.data = {'enabled': checked};
            }
            if (options.data !== undefined) {
                options.contentType = 'application/json';
                options.data = JSON.stringify(options.data);
            }

            const that = this;
            $.ajax(options).done(function () {
                const message = $this.attr(checked ? 'data-rest-success-checked' : 'data-rest-success');
                showFlag({
                    type: 'success',
                    title: message || $this.attr('data-rest-success'),
                    close: 'auto'
                });
            }).fail(function () {
                if (checked !== undefined) {
                    that.checked = !checked;
                }
                const message = $this.attr(checked ? 'data-rest-failed-checked' : 'data-rest-failed');
                showFlag({
                    type: 'error',
                    title: message || $this.attr('data-rest-failed')
                });
            }).always(function () {
                $this.setIdle();
            });
        }
    };
    $(document).on('click', '.aui-button[data-rest]', dataRestEventHandler);
    $(document).on('change', 'aui-toggle[data-rest]', dataRestEventHandler);
    $(document).on('click', 'aui-item-checkbox[data-rest]', dataRestEventHandler);
    $(document).on('click', 'aui-item-link[data-rest]', dataRestEventHandler);
})(jQuery);
