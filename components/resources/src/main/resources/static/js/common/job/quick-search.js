/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function ($) {
    'use strict';

    const $jobQuickSearch = $('#job-quick-search');
    $jobQuickSearch.auiSelect2({
        dropdownCssClass: 'quick-search-drop',
        minimumInputLength: 3,
        ajax: {
            url: function (term) {
                return $(this).attr('data-url') + '?term=' + term;
            },
            dataType: 'json',
            results: function (data) {
                return {
                    results: data
                }
            }
        },
        formatSelection: function (data, container, escapeMarkup) {
            return data ? escapeMarkup(data.displayName) : undefined;
        },
        formatResult: function (result, container, query, escapeMarkup) {
            var markup = [];
            Select2.util.markMatch(result.displayName, query.term, markup, escapeMarkup);
            return markup.join("");
        }
    });
    $jobQuickSearch.on('select2-selecting', function (event) {
        event.preventDefault();
        $('#navigate-to-job').attr("data-nav-module-jobid", event.val).click();
    });
    $jobQuickSearch.on('select2-open', function () {
        $('.quick-search-drop .select2-input').addClass('ajs-dirty-warning-exempt');
    });
    if ($jobQuickSearch.attr('data-enabled') !== 'true') {
        $jobQuickSearch.auiSelect2("enable", false);
    }
})
(jQuery);
