/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function ($) {
    'use strict';

    $('#evaluator').click(function (event) {
        const that = this;
        event.preventDefault();
        that.busy();

        const issueKey = $('#evaluator-issuekey').val();
        const field = $('#issue-field').val();
        let mapper = $('#issue-field-mapping').val();
        if (mapper)
        {
            mapper = field + '=' + mapper;
        }
        else
        {
            mapper = '';
        }

        $.ajax({
            url: $(this).attr('data-url'),
            type: 'PUT',
            contentType: 'application/json',
            dataType: 'text',
            data: JSON.stringify({'issueKey': issueKey, 'fields': field, 'names': '', 'mappers': mapper, 'excludeDefaults': true})
        }).done(function (data) {
            const $output = $('#evaluator-output');
            $output.text(data);
            $output[0].scrollIntoView();
            that.idle();
        });
    });
})
(jQuery);
