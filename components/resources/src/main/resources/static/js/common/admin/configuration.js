/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function ($) {
    'use strict';

    $(document).on('click', '#save-configuration-button', function (event) {
        event.preventDefault();

        const button = this;
        if (!button.isBusy()) {
            button.busy();
            const $form = $('form#configuration');

            $form.find('.error').text('');

            let data = {scope: '', configuration: []};

            let configuration = $form.serializeArray();
            $.each(configuration, function () {
                if (this.name === 'scope') {
                    data.scope = this.value;
                } else {
                    data.configuration.push({
                        key: this.name,
                        value: this.value
                    });
                }
            });
            $.each(data.configuration, function () {
                this.scope = data.scope;
            });
            $.ajax({
                url: $form.attr('action'),
                type: 'POST',
                data: JSON.stringify(data),
                contentType: 'application/json'
            }).done(function () {
                button.idle();
                showFlag({
                    type: 'success',
                    title: AJS.I18n.getText('configuration.saved.successful'),
                    close: 'auto'
                });
            }).fail(function (xhr) {
                button.idle();
                let data;
                try {
                    data = JSON.parse(xhr.responseText);
                } catch (e) {
                    data = {};
                }
                if (data.errors !== undefined) {
                    $.each(data.errors, function (a, error) {
                        $('#' + error.field).parent().find('.error').text(error.message);
                    });
                } else {
                    showFlag({
                        type: 'error',
                        title: data.message !== undefined ? data.message : AJS.I18n.getText('configuration.saved.failed'),
                        close: 'auto'
                    });
                }
            });
        }
    });
})
(jQuery);
