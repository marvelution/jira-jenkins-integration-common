/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function ($) {
    'use strict';

    function JenkinsPortal(options) {
        this.options = options;
        this.options.ajax = Object.assign({}, {
            type: 'GET',
            dataType: 'html'
        }, options.ajax);
        this.buttons = {
            done: Object.assign({}, {
                id: 'done',
                text: AJS.I18n.getText('done'),
                primary: true
            }, (options.buttons || {}).done),
            back: Object.assign({}, {
                id: 'back',
                text: AJS.I18n.getText('back')
            }, (options.buttons || {}).back),
            next: Object.assign({}, {
                id: 'next',
                text: AJS.I18n.getText('next'),
                primary: true
            }, (options.buttons || {}).next)
        };

        this._triggerEvent = function (event, data) {
            let eventObject = new $.Event(`portal.${event}`, {
                portal: this,
                eventData: data
            });
            this.getPortalContainer().trigger(eventObject);
            return eventObject;
        }

        this.show = function () {
            const instance = this;
            $.ajax(this.options.ajax).done(function (html) {
                instance._setContent(html);
                instance._triggerEvent('show');
                $(document).on('keyup', function(event) {
                    let isEscape = false;
                    if ("key" in event) {
                        isEscape = (event.key === "Escape" || event.key === "Esc");
                    } else {
                        isEscape = (event.keyCode === 27);
                    }
                    if (isEscape) {
                        instance.options.shouldReloadOnHide = false;
                        instance.hide();
                    }
                });
            }).fail(function () {
                showFlag({
                    type: 'error', title: AJS.I18n.getText('portal.load.failed')
                });
            }).always(function () {
                instance.options.opener.setIdle();
            });
        }

        this.hide = function (event, data) {
            this.getPortal().remove();
            const hideEvent = this._triggerEvent(event || 'hide', data);
            if (!hideEvent.isDefaultPrevented() && this.options.shouldReloadOnHide) {
                navigatorReload();
            }
        }

        this.reloadOnHide = function () {
            this.options.shouldReloadOnHide = true;
        }

        this.getPortalContainer = function () {
            let container = $('.jenkins-portal-container');
            if (container.length > 0) {
                this.container = container;
            } else {
                this.container = $('body').addClass('jenkins-portal-container');
            }
            return this.container;
        }

        this.getPortal = function () {
            if (!this.portal) {
                this.portal = $('<div/>').addClass('jenkins-portal').appendTo(this.getPortalContainer());
            }
            return this.portal;
        }

        this.getPortalHeader = function () {
            if (!this.portalHeader) {
                this.portalHeader = $('<div/>').addClass('jenkins-portal-header').prependTo(this.getPortal());
            }
            return this.portalHeader;
        }

        this.getPortalContent = function () {
            if (!this.portalContent) {
                this.portalContent = $('<div/>').addClass('jenkins-portal-body').appendTo(this.getPortal());
            }
            return this.portalContent;
        }

        this.getPortalPages = function () {
            return this.getPortalContent().find('fieldset.jenkins-portal-body-page');
        }

        this.getPortalActivePage = function () {
            return this.getPortalPages().filter('.active');
        }

        this.getPortalPrevPage = function () {
            return this.getPortalActivePage().prev('fieldset.jenkins-portal-body-page');
        }

        this.getPortalNextPage = function () {
            return this.getPortalActivePage().next('fieldset.jenkins-portal-body-page');
        }

        this.setPortalActivePage = function (page, data) {
            const instance = this;
            const oldIndex = this.getPortalActivePage().index();
            this.getPortalPages()
                .toggleClass('active', false)
                .each(function (index, element) {
                    const $element = $(element);
                    const active = (typeof page === 'number' && index === page)
                        || (typeof page === 'string' && $element.prop('id') === page)
                        || ($(page).is($element));
                    $element.toggleClass('active', active);
                    if (active && $element.isEmpty() && $element.attr('data-portal-page-url') !== '') {
                        $('<aui-progressbar indeterminate></aui-progressbar>').appendTo($element);
                        const options = Object.assign({}, instance.options.ajax);
                        options.url = $element.attr('data-portal-page-url');
                        for (const [key, value] of Object.entries(data)) {
                            options.url = options.url.replace(`{${key}}`, value);
                        }
                        $.ajax(options).done(function (html) {
                            $element.addRawHTML(html, 'replace');
                        }).fail(function () {
                            showFlag({
                                type: 'error', title: AJS.I18n.getText('portal.page.load.failed')
                            });
                        });
                    }
                });
            const direction = this.getPortalActivePage().index() > oldIndex ? 'next' : 'back';
            this._triggerEvent('page', {direction: direction, data: data})
            this._updatePortalState();
        }

        this.getPortalFooter = function () {
            if (!this.portalFooter) {
                this.portalFooter = $('<div/>').addClass('jenkins-portal-footer').appendTo(this.getPortal());
            }
            return this.portalFooter;
        }

        this.serializeForm = function () {
            let page = this.getPortalActivePage();
            if (this.getPortalNextPage().length === 0) {
                return this._form().serializeJson();
            } else {
                let json = {};
                do {
                    json = Object.assign({}, json, page.serializeJson());
                    page = page.prev();
                }
                while (page.length === 1);
                return json;
            }
        }

        this.getElementsToValidate = function () {
            let elements = this.getPortalActivePage().formElements()
                .filter(function () {
                    return this.name !== undefined && this.name !== '' && !$(this).is(":disabled");
                })
                .map(function (_i, elem) {
                    return elem.name;
                })
                .get();
            return [...new Set(elements)];
        }

        this._form = function () {
            let form = this.getPortalActivePage().find('form');
            return form.length > 0 ? form : this.getPortalContent().find('form');
        }

        this._setContent = function (html) {
            this.getPortalContainer().off();

            const instance = this;
            const header = this.getPortalHeader()
                .append($('<button/>')
                    .addClass('aui-close-button')
                    .attr('id', 'jenkins-portal-close-button')
                    .attr('type', 'button')
                    .attr('aria-label', 'close')
                    .click(function (event) {
                        instance._portalEventHandler(event);
                    }))
                .append($('<h1/>')
                    .addClass('jenkins-portal-header-main')
                    .text(this.options.header.text));
            if (this.options.help) {
                header.append($('<a/>')
                    .addClass('aui-button')
                    .addClass('aui-button-subtle')
                    .addClass('help')
                    .attr('href', this.options.help)
                    .attr('target', '_blank')
                    .append($('<span/>')
                        .addClass('aui-icon')
                        .addClass('aui-icon-small')
                        .addClass('aui-iconfont-help')
                        .text(AJS.I18n.getText('help'))));
            }

            this.getPortalContent().addRawHTML(html, 'append');

            const pages = this.getPortalPages();
            pages.first().addClass('active');

            if (pages.length === 1) {
                this.getPortalFooter()
                    .append(this._createButton(this.buttons.done));
            } else {
                let progress = $('<ol/>').addClass('aui-progress-tracker');
                $.each(pages, function (_i, page) {
                    let title = page.getAttribute('data-portal-page-title') || `Step ${_i}`;
                    progress.append($('<li/>')
                        .addClass('aui-progress-tracker-step')
                        .toggleClass('aui-progress-tracker-step-current', _i === 0)
                        .append($('<span/>')
                            .text(title)));
                });

                this.getPortalContent()
                    .prepend($('<div/>')
                        .addClass('jenkins-portal-body-progress')
                        .append(progress));

                this.getPortalFooter()
                    .append(this._createButton(this.buttons.back).addClass('hidden'))
                    .append(this._createButton(this.buttons.next))
                    .append(this._createButton(this.buttons.done).addClass('hidden'));
            }
        }

        this._createButton = function (config) {
            const instance = this;
            let button = $('<button/>')
                .addClass('aui-button')
                .attr('id', `jenkins-portal-${config.id}-button`)
                .attr('type', 'button')
                .attr('aria-label', config.id)
                .val(config.id)
                .text(config.text || AJS.I18n.getText(config.id));
            if (config.handler) {
                button.click(config.handler);
            } else {
                button.click(function (event) {
                    instance._portalEventHandler(event);
                });
            }
            if (config.primary === true) {
                button.addClass('aui-button-primary');
            }
            return button;
        }

        this.getButton = function (id) {
            return this.getPortalFooter().find(`#jenkins-portal-${id}-button`);
        }

        this.addButton = function (config) {
            if (this.buttons[config.id] === undefined) {
                this.buttons[config.id] = config;
                const button = this._createButton(config);
                this.getPortalFooter().append(button);
            }
        }

        this._portalEventHandler = function (event) {
            event.preventDefault();

            const source = $(event.target);
            const matches = source.prop('id').match(/jenkins-portal-([a-z]+)-button/);
            const action = matches ? matches[1] : '';
            switch (action) {
                case 'close':
                    this.hide();
                    break;
                case 'back':
                case 'next':
                    if (action === 'back') {
                        const backEvent = this._triggerEvent('back');
                        if (backEvent.isDefaultPrevented()) {
                            break;
                        }
                        this.setPortalActivePage(this.getPortalPrevPage());
                    } else if (action === 'next') {
                        const instance = this;
                        const nextEvent = this._triggerEvent('next', {validate: true});
                        if (nextEvent.isDefaultPrevented()) {
                            break;
                        }
                        const action = nextEvent.eventData.validate ? this.validateForm : this.submitForm;
                        Reflect.apply(action, instance, [source, function (data) {
                            instance.setPortalActivePage(instance.getPortalNextPage(), data);
                        }]);
                    }
                    break;
                case 'done':
                    const instance = this;
                    this.submitForm(source, function (data) {
                        instance.hide('done', {data: data});
                    });
                    break;
                default:
                    this._triggerEvent(action);
                    this._updatePortalState();
                    break;
            }
        }

        this._updatePortalState = function () {
            const instance = this;
            const currentPage = this.getPortalActivePage();
            const hasPrevPage = this.getPortalPrevPage().length === 1;
            const hasNextPage = this.getPortalNextPage().length === 1;
            $.each(this.buttons, function (_i, config) {
                const button = instance.getButton(config.id);
                switch (config.id) {
                    case 'back':
                        button.toggleClass('hidden', !hasPrevPage);
                        break;
                    case 'next':
                        button.toggleClass('hidden', !hasNextPage);
                        break;
                    case 'done':
                        button.toggleClass('hidden', hasNextPage);
                        break;
                    default:
                        let hide = true;
                        if (typeof config.shouldShow === 'function') {
                            hide = !config.shouldShow(currentPage);
                        } else if (typeof config.shouldShow === 'string') {
                            hide = currentPage.prop('id') !== config.shouldShow;
                        }
                        button.toggleClass('hidden', hide)
                        break;
                }
            });
            const currentPageIndex = currentPage.index();
            $('.jenkins-portal-body-progress .aui-progress-tracker-step')
                .toggleClass('aui-progress-tracker-step-current', false)
                .toggleClass(function (index) {
                    return (index === currentPageIndex ? 'aui-progress-tracker-step-current' : '');
                }, true);
        }

        this.validateForm = function (button, callback) {
            let validationParams = '';
            if (this.getPortalNextPage().length === 1) {
                const elementsToValidate = this.getElementsToValidate();
                if (elementsToValidate && elementsToValidate.length > 0) {
                    validationParams = 'validateFields=' + elementsToValidate.join(',');
                } else {
                    button.setIdle();
                    callback();
                    return;
                }
            }
            let form = this._form();
            let url = form.attr('action');
            if (url.indexOf('?') > 0) {
                url += `&${validationParams}`
            } else {
                url += `?${validationParams}`
            }
            this._submitForm(form, button, callback, this.serializeForm(), url);
        }

        this.submitForm = function (button, callback) {
            this._submitForm(this._form(), button, callback);
        }

        this._submitForm = function (form, button, callback, data, url) {
            button.setBusy();

            form.find('.error').text('');

            let options = {
                url: url || form.attr('action'),
                type: form.attr('method') || 'POST',
                contentType: 'application/json',
                data: JSON.stringify(data || form.serializeJson())
            };

            const submitEvent = this._triggerEvent('submit', {options: options});
            if (submitEvent.isDefaultPrevented()) {
                button.setIdle();
                return;
            }
            options = submitEvent.eventData.options;

            const instance = this;
            $.ajax(options).done(function (data, textStatus, xhr) {
                const doneEvent = instance._triggerEvent('submit-done', {data: data, xhr: xhr});
                if (!doneEvent.isDefaultPrevented()) {
                    callback(data, xhr);
                }
            }).fail(function (xhr) {
                let data;
                try {
                    data = $.parseJSON(xhr.responseText) || {};
                } catch (e) {
                    data = {};
                }
                const failEvent = instance._triggerEvent('submit-fail', {data: data, xhr: xhr});
                if (failEvent.isDefaultPrevented()) {
                    return;
                }
                if (data.errors !== undefined) {
                    $.each(data.errors, function (a, error) {
                        let $field = $('[name=' + error.field + ']:not([disabled]):visible');
                        if ($field.length > 0) {
                            $field.parent()
                                .find('.error')
                                .html(error.message);
                        } else {
                            showFlag({
                                type: 'error',
                                title: error.message,
                            });
                        }
                    });
                } else {
                    showFlag({
                        type: 'error',
                        title: data.message !== undefined ? data.message : AJS.I18n.getText('unexpected.error.occurred'),
                        close: 'auto'
                    });
                }
            }).always(function () {
                button.setIdle();
            });
        }
    }

    $(document).on('click', '[data-portal]', function (event) {
        const $this = $(this);
        if ($this.is(':disabled') || $this.attr('disabled')) {
            return;
        }

        event.preventDefault();

        if (!$this.isBusy()) {
            $this.setBusy();

            let options = {
                opener: $this,
                header: {
                    text: $this.attr('data-portal-header')
                },
                help: $this.attr('data-portal-help'),
                buttons: {},
                ajax: {
                    url: $this.attr('data-portal'),
                    data: {}
                }
            };

            $.each(this.attributes, function (index, attr) {
                if (attr.name === 'data-portal-method') {
                    options.ajax.type = attr.value;
                } else if (attr.name.startsWith('data-portal-param-')) {
                    options.ajax.data[attr.name.substring('data-portal-param-'.length)] = attr.value;
                } else if (attr.name.startsWith('data-portal-button-')) {
                    let attribute = attr.name.substring('data-portal-button-'.length).split('-', 2);
                    options.buttons[attribute[0]] = options.buttons[attribute[0]] || {};
                    options.buttons[attribute[0]][attribute[1]] = attr.value;
                }
            });

            const portal = new JenkinsPortal(options);
            portal.show();
        }
    });
})(jQuery);
