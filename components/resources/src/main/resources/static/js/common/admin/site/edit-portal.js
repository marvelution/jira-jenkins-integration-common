/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function ($) {
    'use strict';

    $('.jenkins-portal-container')
        .on('portal.submit', function (event) {
            event.portal.reloadOnHide();
        });

    const activePage = $('.jenkins-portal-body-page');
    activePage.find('input:radio[name=connection]').off().click(function () {
        $('.site-setup').toggleClass('hidden', true).disableFormElements(true);
        const connType = $(this).val();
        const checked = $(this).is(':checked');
        $('#' + connType + '-site-setup').toggleClass('hidden', !checked).disableFormElements(!checked);

        const notInAccessible = connType !== 'INACCESSIBLE' && checked
        activePage.find('input[name=useCrumbs]')
            .disableFormElements(!notInAccessible)
            .closest('div.checkbox')
            .toggleClass('hidden', !notInAccessible);
        const accessible = connType === 'ACCESSIBLE' && checked
        activePage.find('input[name=displayUrl]')
            .disableFormElements(!accessible)
            .closest('div.field-group')
            .toggleClass('hidden', !accessible);
    });
    activePage.find('input:radio[name=authentication]').off().click(function () {
        $('.auth-fields').toggleClass('hidden', true).disableFormElements(true);
        const authType = $(this).val();
        const checked = $(this).is(':checked');
        $('#' + authType + '-auth-fields').toggleClass('hidden', !checked).disableFormElements(!checked);
    });
})
(jQuery);
