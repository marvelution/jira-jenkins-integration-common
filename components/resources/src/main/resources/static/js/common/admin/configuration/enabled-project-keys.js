/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function ($) {
    'use strict';

    $('#enabled_project_keys').auiSelect2({
        minimumInputLength: 3,
        multiple: true,
        ajax: {
            url: function (term) {
                return $(this).attr('data-url') + '?q=' + term;
            },
            dataType: 'json',
            results: function (data) {
                let results = [];
                $(data).each(function () {
                    results.push({
                        id: this.key,
                        text: this.name
                    });
                });
                return {
                    results: results
                }
            }
        },
        initSelection: function (element, callback) {
            const $element = $(element);
            const keys = $element.val();
            if (keys !== "") {
                $.ajax($element.attr('data-url') + '?keys=' + keys, {dataType: 'json'}).done(function (data) {
                    let results = [];
                    $(data).each(function () {
                        results.push({
                            id: this.key,
                            text: this.name
                        });
                    });
                    callback(results);
                });
            }
        },
        formatSelection: function (data, container, escapeMarkup) {
            return data ? escapeMarkup(data.text) : undefined;
        },
        formatResult: function (result, container, query, escapeMarkup) {
            let markup = [];
            Select2.util.markMatch(result.text, query.term, markup, escapeMarkup);
            return markup.join("");
        }
    });
})
(jQuery);
