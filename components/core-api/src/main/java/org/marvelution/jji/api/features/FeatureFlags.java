/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.api.features;

public interface FeatureFlags
{

	String FEATURE_FLAG_PROPERTY_PREFIX = "marvelution.features.";

	/**
	 * Common feature flags
	 */

	String JIRA_JENKINS_AUTOMATION = "jira-jenkins-automation";
	String JIRA_JENKINS_AUTOMATION_FULL = "jira-jenkins-automation-full";
	String JIRA_JENKINS_INTEGRATION = "jira-jenkins-integration";
	String DATA_MIGRATION = "data-migration";
	String SYNC_ALL_BUILDS = "sync-all-builds";
	String PROJECT_CONFIGURATION = "project-configuration";
	String SITE_CONNECTION_TUNNELED = "site-connection-tunneled";


	/**
	 * Cloud specific feature flags
	 */

	String LENIENT_SITE_VALIDATOR = "lenient-site-validator";
	String DESCRIPTOR_CACHE_DISABLED = "descriptor-cache-disabled";
	String BUILD_INFO_PROVIDER = "build-info-provider";
	String DEPLOYMENT_INFO_PROVIDER = "deployment-info-provider";
	String REPORT_INFO_PROVIDER_VIOLATIONS = "report-info-provider-violations";
	String ACCESS_EMAIL_ADDRESSES = "access-email-addresses";


	/**
	 * Server specific feature flags
	 */

	String DC_LOCAL_QUEUEING_DISABLED = "dc-local-queueing-disabled";
	String DC_QUEUEING = "dc-queueing";
	String DYNAMIC_UI_ELEMENTS = "dynamic-ui-elements";
	String DISABLE_ISSUE_INDEXING = "disable-issue-indexing";
}
