/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.api.features;

import java.util.*;

/**
 * Feature Flag model.
 *
 * @author Mark Rekveld
 * @since 1.14.0
 */
public final class FeatureFlag
{

	private final String feature;
	private final boolean enabled;
	private final boolean overrideable;

	public FeatureFlag(
			String feature,
			boolean enabled)
	{
		this(feature, enabled, false);
	}

	public FeatureFlag(
			String feature,
			boolean enabled,
			boolean overrideable)
	{
		this.feature = feature;
		this.enabled = enabled;
		this.overrideable = overrideable;
	}

	public String getFeature()
	{
		return feature;
	}

	public boolean isEnabled()
	{
		return enabled;
	}

	public boolean isOverrideable()
	{
		return overrideable;
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (!(o instanceof FeatureFlag))
		{
			return false;
		}
		FeatureFlag that = (FeatureFlag) o;
		return feature.equals(that.feature);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(feature);
	}
}
