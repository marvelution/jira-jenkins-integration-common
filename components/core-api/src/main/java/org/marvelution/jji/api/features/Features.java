/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.api.features;

import java.util.Collections;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import javax.annotation.Nullable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.util.stream.Collectors.toSet;

/**
 * Feature API.
 *
 * @author Mark Rekveld
 * @since 1.12.0
 */
public abstract class Features
{

    protected static final Logger LOGGER = LoggerFactory.getLogger(Features.class);
    private static final String SYSTEM = "system";
    private static final String DEFAULT_SITE = "site";
    private final FeatureStore featureStore;
    private Set<FeatureFlag> featureFlags = new HashSet<>();

    protected Features(FeatureStore featureStore)
    {
        this.featureStore = featureStore;
    }

    /**
     * Returns the site scope for the feature store.
     *
     * @since 1.14.0
     */
    @Nullable
    protected String getSiteScope()
    {
        return DEFAULT_SITE;
    }

    /**
     * Returns all the loaded {@link FeatureFlag}s.
     *
     * @since 1.14.0
     */
    public Set<FeatureFlag> getFeatureFlags()
    {
        initializeIfNeeded();
        return Collections.unmodifiableSet(featureFlags);
    }

    /**
     * Returns all the loaded system {@link FeatureFlag}s.
     *
     * @since 1.14.0
     */
    public Set<FeatureFlag> getSystemFeatureFlags()
    {
        initializeIfNeeded();
        return Collections.unmodifiableSet(featureFlags.stream()
                .filter(flag -> !flag.isOverrideable())
                .collect(toSet()));
    }

    /**
     * Returns all the loaded site {@link FeatureFlag}s.
     *
     * @since 1.14.0
     */
    public Set<FeatureFlag> getSiteFeatureFlags()
    {
        initializeIfNeeded();
        return Collections.unmodifiableSet(featureFlags.stream()
                .filter(FeatureFlag::isOverrideable)
                .collect(toSet()));
    }

    /**
     * Returns all the enabled feature flags.
     *
     * @since 1.14.0
     */
    public Set<String> getAllEnabledFeatures()
    {
        initializeIfNeeded();
        Set<String> enabledFeatures = new HashSet<>(getEnabledSystemFeatures());
        enabledFeatures.addAll(getEnabledSiteFeatures());
        return Collections.unmodifiableSet(enabledFeatures);
    }

    /**
     * Returns all the enabled feature flags for the system.
     *
     * @since 1.14.0
     */
    public Set<String> getEnabledSystemFeatures()
    {
        return Collections.unmodifiableSet(getEnabledFeatures(SYSTEM));
    }

    /**
     * Returns all the enabled feature flags for the site.
     *
     * @since 1.14.0
     */
    public Set<String> getEnabledSiteFeatures()
    {
        return Collections.unmodifiableSet(getEnabledFeatures(getSiteScope()));
    }

    /**
     * Returns all the enabled features for a specific {@code scope}, never {@literal null}.
     */
    protected Set<String> getEnabledFeatures(
            @Nullable
            String scope)
    {
        if (scope == null)
        {
            return new HashSet<>();
        }
        else
        {
            initializeIfNeeded();
            return loadEnabledFeatures(scope);
        }
    }

    /**
     * Returns whether the specified {@code feature} is enabled.
     */
    public boolean isFeatureEnabled(String feature)
    {
        initializeIfNeeded();
        return getEnabledSystemFeatures().contains(feature) || getEnabledSiteFeatures().contains(feature);
    }

    /**
     * Enable the specified {@code feature}.
     */
    public void enableFeature(String feature)
    {
        setFeatureState(feature, true);
    }

    /**
     * Disable the specified {@code feature}.
     */
    public void disableFeature(String feature)
    {
        setFeatureState(feature, false);
    }

    /**
     * @since 1.14.0
     */
    public void setFeatureState(
            String feature,
            boolean state)
    {
        initializeIfNeeded();
        Optional<FeatureFlag> featureFlag = featureFlags.stream()
                .filter(flag -> flag.getFeature()
                        .equals(feature))
                .findFirst();

        if (!featureFlag.isPresent())
        {
            throw new IllegalArgumentException("feature " + feature + " is not known.");
        }
        else if (!featureFlag.get()
                .isOverrideable())
        {
            throw new IllegalArgumentException("feature " + feature + " is not overrideable.");
        }
        String scope = getSiteScope();
        if (scope == null)
        {
            throw new IllegalStateException("no scope provided to enable the feature for");
        }
        Set<String> enabledFeatures = getEnabledFeatures(scope);
        boolean isEnabled = enabledFeatures.contains(feature);
        if ((state && !isEnabled) || (!state && isEnabled))
        {
            if (state)
            {
                enabledFeatures.add(feature);
            }
            else
            {
                enabledFeatures.remove(feature);
            }
            featureStore.set(scope, feature, state);
        }
    }

    /**
     * Reset the feature flags by reloading them.
     */
    public void reset()
    {
        featureFlags = loadFeatureFlags();

        LOGGER.info("Loaded {} feature flags.", featureFlags.size());
    }

    /**
     * @since 1.14.0
     */
    private void initializeIfNeeded()
    {
        if (featureFlags.isEmpty())
        {
            reset();
        }
    }

    /**
     * Load all the {@link FeatureFlag}s.
     */
    protected abstract Set<FeatureFlag> loadFeatureFlags();

    /**
     * Loads the enabled features list for the specified {@code scope}.
     *
     * @since 1.14.0
     */
    private Set<String> loadEnabledFeatures(String scope)
    {
        if (SYSTEM.equals(scope))
        {
            return getSystemFeatureFlags().stream()
                    .filter(FeatureFlag::isEnabled)
                    .map(FeatureFlag::getFeature)
                    .collect(toSet());
        }
        else
        {
            return getSiteFeatureFlags().stream()
                    .filter(flag -> featureStore.get(scope, flag.getFeature())
                            .orElse(flag.isEnabled()))
                    .map(FeatureFlag::getFeature)
                    .collect(toSet());
        }
    }
}
