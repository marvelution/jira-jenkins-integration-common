/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.api;

import java.util.Locale;

public class Environment
{

    public static final Environment Dev = new Environment("Dev", ".dev");
    public static final Environment Test = new Environment("Test", ".test");
    public static final Environment Staging = new Environment("Staging", ".staging");
    public static final Environment Production = new Environment("Production", "");

    private final String name;
    private final String keySuffix;

    public Environment(String name)
    {
        this(name, "." + name.toLowerCase(Locale.ENGLISH));
    }

    private Environment(
            String name,
            String keySuffix)
    {
        this.name = name;
        this.keySuffix = keySuffix;
    }

    public String name()
    {
        return name;
    }

    public String addonName(String addon)
    {
        if (keySuffix.isEmpty())
        {
            return addon;
        }
        else
        {
            return String.format("%s (%s)", addon, name);
        }
    }

    public String keySuffix()
    {
        return keySuffix;
    }

    public String addonKey(String addonKey)
    {
        return addonKey + keySuffix;
    }

    public String stripKeySuffix(String addonKey)
    {
        if (addonKey.endsWith(keySuffix))
        {
            return addonKey.substring(0, addonKey.lastIndexOf(keySuffix));
        }
        else
        {
            return addonKey;
        }
    }

    public static Environment valueOf(String name)
    {
        if (Production.name.equalsIgnoreCase(name))
        {
            return Production;
        }
        else if (Staging.name.equalsIgnoreCase(name))
        {
            return Staging;
        }
        else if (Test.name.equalsIgnoreCase(name))
        {
            return Test;
        }
        else if (Dev.name.equalsIgnoreCase(name))
        {
            return Dev;
        }
        else
        {
            return new Environment(name, Test.keySuffix);
        }
    }
}
