/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.templating.thymeleaf;

import java.net.URL;
import java.util.Arrays;
import java.util.Collections;
import java.util.Set;

import org.marvelution.jji.api.features.FeatureFlag;
import org.marvelution.jji.api.features.Features;
import org.marvelution.jji.api.features.InMemoryFeatureStore;
import org.marvelution.jji.templating.thymeleaf.dialect.CommonDialect;
import org.marvelution.jji.templating.thymeleaf.expression.CommonExpressionObjects;
import org.marvelution.jji.templating.thymeleaf.expression.TestLicenses;
import org.marvelution.jji.templating.thymeleaf.expression.TestTimes;
import org.marvelution.jji.templating.thymeleaf.resourceloader.ResourceLoader;
import org.marvelution.testing.TestSupport;

import org.junit.jupiter.api.BeforeEach;
import org.thymeleaf.standard.StandardDialect;
import org.thymeleaf.testing.templateengine.engine.TestExecutor;
import org.thymeleaf.testing.templateengine.engine.TestExecutorFactory;

public class ThymeleafTestSupport
        extends TestSupport
{

    protected TestExecutor executor;
    protected Features features;

    @BeforeEach
    void setUp()
    {
        features = setupFeatures();
        executor = TestExecutorFactory.createJavaxWebTestExecutor();
        executor.setDialects(Arrays.asList(new StandardDialect(),
                new CommonDialect(new CommonExpressionObjects(new TestTimes(), new TestLicenses()), configuration -> new ResourceLoader()
                {
                    @Override
                    public URL getResource(String resource)
                    {
                        return getClass().getClassLoader()
                                .getResource(resource);
                    }
                }, features)));
        executor.reset();
    }

    protected Features setupFeatures()
    {
        return new Features(new InMemoryFeatureStore())
        {
            @Override
            protected Set<FeatureFlag> loadFeatureFlags()
            {
                return Collections.emptySet();
            }
        };
    }
}
