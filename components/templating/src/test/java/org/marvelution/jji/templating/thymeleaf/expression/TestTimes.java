/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.templating.thymeleaf.expression;

import java.time.format.*;
import java.time.temporal.*;
import java.util.*;

import org.thymeleaf.context.*;

/**
 * Simple {@link Times} implementation for testing.
 *
 * @author Mark Rekveld
 * @since 1.8.0
 */
public class TestTimes extends Times {

	private String format;

	public TestTimes() {
		this(null, null);
	}

	public TestTimes(String format) {
		this(null, format);
	}

	private TestTimes(IExpressionContext context, String format) {
		super(context);
		this.format = format;
	}

	@Override
	public String format(Temporal temporal) {
		if (format == null) {
			return temporal.toString();
		} else {
			return DateTimeFormatter.ofPattern(format, Locale.ENGLISH).format(temporal);
		}
	}

	@Override
	public Times withContext(IExpressionContext context) {
		return new TestTimes(context, format);
	}

	public void setFormat(String format) {
		this.format = format;
	}
}
