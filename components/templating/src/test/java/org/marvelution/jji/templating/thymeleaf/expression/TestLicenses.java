/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.templating.thymeleaf.expression;

import org.thymeleaf.context.*;

/**
 * Simple {@link Licenses} implementation for testing.
 * 
 * @author Mark Rekveld
 * @since 1.8.0
 */
public class TestLicenses extends Licenses {

	private boolean valid;
	private boolean nearlyExpired;

	public TestLicenses() {
		this(null, true, false);
	}

	public TestLicenses(boolean valid, boolean nearlyExpired) {
		this(null, valid, nearlyExpired);
	}

	private TestLicenses(IExpressionContext expressionContext, boolean valid, boolean nearlyExpired) {
		super(expressionContext);
		this.valid = valid;
		this.nearlyExpired = nearlyExpired;
	}

	@Override
	public boolean active() {
		return valid;
	}

	@Override
	public boolean nearlyExpired() {
		return nearlyExpired;
	}

	@Override
	public Licenses withContext(IExpressionContext context) {
		return new TestLicenses(context, valid, nearlyExpired);
	}

	public void valid(boolean valid) {
		this.valid = valid;
	}

	public void nearlyExpired(boolean nearlyExpired) {
		this.nearlyExpired = nearlyExpired;
	}
}
