/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.templating.thymeleaf.processor;

import org.marvelution.jji.templating.thymeleaf.*;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.*;
import org.junit.jupiter.params.provider.*;
import org.mockito.junit.jupiter.*;
import org.mockito.quality.*;
import org.thymeleaf.testing.templateengine.engine.*;

/**
 * Tests for {@link LoadAttrTagProcessor}.
 *
 * @author Mark Rekveld
 * @since 1.8.0
 */
@MockitoSettings(strictness = Strictness.LENIENT)
class LoadAttrTagProcessorTest
		extends ThymeleafTestSupport
{

	@ParameterizedTest
	@ValueSource(strings = { "append-class.thtest",
			"blank-attribute.thtest",
			"blank-attribute-jji-id.thtest",
			"blank-attribute-no-id.thtest",
			"expression.thtest",
			"message.thtest",
			"missing-resource.thtest",
			"simple.thtest" })
	void testLoadAttrTagProcessor(String resource)
	{
		executor.execute("classpath:load-attribute-tests/" + resource);
		Assertions.assertTrue(executor.isAllOK());
	}
}
