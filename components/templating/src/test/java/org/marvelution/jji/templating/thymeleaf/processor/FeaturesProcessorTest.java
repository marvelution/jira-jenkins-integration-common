/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.templating.thymeleaf.processor;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.marvelution.jji.api.features.FeatureFlag;
import org.marvelution.jji.api.features.Features;
import org.marvelution.jji.api.features.InMemoryFeatureStore;
import org.marvelution.jji.templating.thymeleaf.ThymeleafTestSupport;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

@MockitoSettings(strictness = Strictness.LENIENT)
class FeaturesProcessorTest
        extends ThymeleafTestSupport
{

    @ParameterizedTest
    @ValueSource(strings = {"features-disabled.thtest",
            "features-disabled-context.thtest",
            "features-enabled.thtest",
            "features-enabled-context.thtest"})
    void testLoadAttrTagProcessor(String resource)
    {
        executor.execute("classpath:features-tests/" + resource);
        Assertions.assertTrue(executor.isAllOK());
    }

    @Override
    protected Features setupFeatures()
    {
        return new Features(new InMemoryFeatureStore())
        {
            @Override
            protected Set<FeatureFlag> loadFeatureFlags()
            {
                return Stream.of(new FeatureFlag("enabled-flag", true), new FeatureFlag("disabled-flag", false))
                        .collect(Collectors.toSet());
            }
        };
    }
}
