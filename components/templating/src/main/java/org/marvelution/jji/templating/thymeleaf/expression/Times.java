/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.templating.thymeleaf.expression;

import java.time.*;
import java.time.temporal.*;
import java.util.*;

import org.thymeleaf.context.*;

public abstract class Times
		extends AbstractExpressionObject<Times>
{

	private static final String EXPRESSION_OBJECT_NAME = "times";

	protected Times(IExpressionContext context)
	{
		super(context);
	}

	@Override
	public String getName()
	{
		return EXPRESSION_OBJECT_NAME;
	}

	public String format(Date date)
	{
		return format(date.toInstant());
	}

	public abstract String format(Temporal temporal);

	public String format(Duration duration)
	{
		return duration.toString().substring(2).replaceAll("(\\d[HMS])(?!$)", "$1 ").toLowerCase();
	}
}
