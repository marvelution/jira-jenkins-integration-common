/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.templating.thymeleaf.expression;

import org.thymeleaf.context.*;

public abstract class Licenses
		extends AbstractExpressionObject<Licenses>
{

	public static final String EXPRESSION_OBJECT_NAME = "licenses";

	protected Licenses(IExpressionContext expressionContext)
	{
		super(expressionContext);
	}

	@Override
	public String getName()
	{
		return EXPRESSION_OBJECT_NAME;
	}

	public abstract boolean active();

	public String inactiveMessageKey()
	{
		return "license.inactive.warning.message";
	}

	public boolean expired()
	{
		return false;
	}

	public String expiredMessageKey()
	{
		return "license.inactive.warning.message";
	}

	public abstract boolean nearlyExpired();
}
