/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.templating.thymeleaf.expression;

import java.util.*;

import org.thymeleaf.context.*;

public abstract class AbstractExpressionObject<E>
		implements NamedExpressionObject
{

	private final IExpressionContext context;

	protected AbstractExpressionObject(IExpressionContext expressionContext)
	{
		context = expressionContext;
	}

	public abstract E withContext(IExpressionContext context);

	protected Optional<IExpressionContext> getContext()
	{
		return Optional.ofNullable(context);
	}

	protected <T> Optional<T> getContext(Class<T> type)
	{
		return getContext().filter(type::isInstance).map(type::cast);
	}

	protected <T> Optional<T> getExpressionObject(
			String name,
			Class<T> type)
	{
		return getContext().map(IExpressionContext::getExpressionObjects)
				.map(context -> context.getObject(name))
				.filter(type::isInstance)
				.map(type::cast);
	}
}
