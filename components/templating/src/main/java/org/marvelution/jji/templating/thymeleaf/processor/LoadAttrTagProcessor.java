/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.templating.thymeleaf.processor;

import java.util.*;
import javax.annotation.*;

import org.marvelution.jji.templating.thymeleaf.resourceloader.*;

import org.slf4j.*;
import org.thymeleaf.context.*;
import org.thymeleaf.engine.*;
import org.thymeleaf.model.*;
import org.thymeleaf.processor.element.*;
import org.thymeleaf.standard.expression.*;
import org.thymeleaf.standard.processor.*;
import org.thymeleaf.templatemode.*;
import org.thymeleaf.util.*;

import static org.apache.commons.lang3.StringUtils.*;

public class LoadAttrTagProcessor
		extends AbstractStandardExpressionAttributeTagProcessor
{

	private static final Logger LOGGER = LoggerFactory.getLogger(LoadAttrTagProcessor.class);
	private static final int PRECEDENCE = 2000;
	private Map<String, Map<String, Object>> attributes;
	private static boolean useCache = true;

	public LoadAttrTagProcessor(
			TemplateMode templateMode,
			String dialectPrefix)
	{
		super(templateMode, dialectPrefix, "load-attr", PRECEDENCE, true);
	}

	public static void useCache(boolean useCache)
	{
		LoadAttrTagProcessor.useCache = useCache;
	}

	@Override
	protected void doProcess(
			ITemplateContext context,
			IProcessableElementTag tag,
			AttributeName attributeName,
			String attributeValue,
			Object expressionResult,
			IElementTagStructureHandler structureHandler)
	{
		String key;
		if (expressionResult instanceof String)
		{
			key = (String) expressionResult;
		}
		else if (tag.hasAttribute("id"))
		{
			LOGGER.debug("Using element id attribute to locate attributes.");
			key = tag.getAttributeValue("id");
		}
		else
		{
			LOGGER.debug("No identifier found to use to load attributes.");
			return;
		}

		ResourceLoader resourceLoader = ResourceLoaderFactory.locateResourceLoader(context.getConfiguration());
		Map<String, Object> attrs = getAttributes(resourceLoader, key);
		if (attrs != null)
		{
			IStandardExpressionParser expressionParser = StandardExpressions.getExpressionParser(context.getConfiguration());
			attrs.forEach((name, value) -> {
				if (value != null)
				{
					Object result;
					if (value instanceof String && ((String) value).length() > 3)
					{
						try
						{
							IStandardExpression expression = expressionParser.parseExpression(context, (String) value);
							result = expression.execute(context, StandardExpressionExecutionContext.RESTRICTED);
						}
						catch (Exception e)
						{
							LOGGER.debug("Failed to process value '{}' as expression: {}", value, e.getMessage());
							result = value;
						}
					}
					else
					{
						result = value;
					}

					if (result != NoOpToken.VALUE)
					{
						if (ArrayUtils.contains(StandardConditionalFixedValueTagProcessor.ATTR_NAMES, name))
						{
							if (EvaluationUtils.evaluateAsBoolean(result))
							{
								structureHandler.setAttribute(name, name);
							}
						}
						else
						{
							String newValue = EscapedAttributeUtils.escapeAttribute(getTemplateMode(),
							                                                        result == null ? null : result.toString());
							if (isNotBlank(newValue))
							{
								if (tag.hasAttribute(name))
								{
									String currentValue = tag.getAttributeValue(name);
									structureHandler.setAttribute(name, currentValue + " " + newValue);
								}
								else
								{
									structureHandler.setAttribute(name, newValue);
								}
							}
						}
					}
				}
			});
		}
	}

	@Nullable
	private Map<String, Object> getAttributes(
			ResourceLoader resourceLoader,
			String key)
	{
		if (useCache)
		{
			if (attributes == null)
			{
				synchronized (this)
				{
					if (attributes == null)
					{
						attributes = loadAttributes(resourceLoader);
					}
				}
			}
			return attributes.get(key);
		}
		else
		{
			return loadAttributes(resourceLoader).get(key);
		}
	}

	private Map<String, Map<String, Object>> loadAttributes(ResourceLoader resourceLoader)
	{
		Map<String, Map<String, Object>> attributes = new HashMap<>();
		attributes.putAll(loadAttributes(resourceLoader, "templates/common/attributes.yaml"));
		attributes.putAll(loadAttributes(resourceLoader, "templates/attributes.yaml"));
		return attributes;
	}

	private Map<String, Map<String, Object>> loadAttributes(
			ResourceLoader resourceLoader,
			String resource)
	{
		try
		{
			Map<String, Map<String, Object>> attributes = resourceLoader.loadYaml(resource);
			if (attributes == null)
			{
				LOGGER.warn("Loaded a blank attributes.yaml");
				attributes = Collections.emptyMap();
			}
			return attributes;
		}
		catch (Exception e)
		{
			LOGGER.error("Failed to load attributes from {}.", resource, e);
		}
		return Collections.emptyMap();
	}
}
