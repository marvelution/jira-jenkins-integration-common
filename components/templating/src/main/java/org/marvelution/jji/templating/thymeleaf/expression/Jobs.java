/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.templating.thymeleaf.expression;

import java.util.*;

import org.marvelution.jji.data.services.api.*;
import org.marvelution.jji.model.*;

import jakarta.inject.*;

@Named
@javax.inject.Named
public class Jobs
        implements NamedExpressionObject
{

    private static final String EXPRESSION_OBJECT_NAME = "jobs";
    private final JobService jobService;

    @Inject
    @javax.inject.Inject
    public Jobs(JobService jobService)
    {
        this.jobService = jobService;
    }

    @Override
    public String getName()
    {
        return EXPRESSION_OBJECT_NAME;
    }

    public List<Job> parents(Job job)
    {
        return jobService.getParents(job);
    }
}
