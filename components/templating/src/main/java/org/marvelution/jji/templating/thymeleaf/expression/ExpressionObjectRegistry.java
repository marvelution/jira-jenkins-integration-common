/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.templating.thymeleaf.expression;

import java.util.*;

import org.thymeleaf.context.*;
import org.thymeleaf.expression.*;

public abstract class ExpressionObjectRegistry
		implements IExpressionObjectFactory
{

	private final Map<String, Object> objects = new HashMap<>();

	protected void registerObject(NamedExpressionObject object)
	{
		if (object != null)
		{
			registerObject(object.getName(), object);
		}
	}

	protected void registerObject(
			String name,
			Object object)
	{
		if (objects.putIfAbsent(name, object) != null)
		{
			throw new IllegalArgumentException("Duplicate object registered with name " + name);
		}
	}

	@Override
	public Set<String> getAllExpressionObjectNames()
	{
		return objects.keySet();
	}

	@Override
	public Object buildObject(
			IExpressionContext context,
			String expressionObjectName)
	{
		if (objects.containsKey(expressionObjectName))
		{
			Object object = objects.get(expressionObjectName);
			if (object instanceof AbstractExpressionObject)
			{
				return ((AbstractExpressionObject<?>) object).withContext(context);
			}
			else if (object instanceof ExpressionObjectFactory)
			{
				return ((ExpressionObjectFactory<?>) object).create(context);
			}
			else
			{
				return object;
			}
		}
		else
		{
			return null;
		}
	}

	@Override
	public boolean isCacheable(String expressionObjectName)
	{
		return objects.containsKey(expressionObjectName);
	}
}
