/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.templating.thymeleaf.processor;

import org.marvelution.jji.api.features.Features;

import org.thymeleaf.context.ITemplateContext;
import org.thymeleaf.engine.AttributeName;
import org.thymeleaf.model.IProcessableElementTag;
import org.thymeleaf.standard.expression.IStandardExpression;
import org.thymeleaf.standard.expression.IStandardExpressionParser;
import org.thymeleaf.standard.expression.StandardExpressions;
import org.thymeleaf.standard.processor.AbstractStandardConditionalVisibilityTagProcessor;
import org.thymeleaf.templatemode.TemplateMode;

public class FeaturesTagProcessor
        extends AbstractStandardConditionalVisibilityTagProcessor
{
    public static final int PRECEDENCE = 300;
    public static final String ATTR_NAME = "features";
    private final Features features;

    public FeaturesTagProcessor(
            TemplateMode templateMode,
            String dialectPrefix,
            Features features)
    {
        super(templateMode, dialectPrefix, ATTR_NAME, PRECEDENCE);
        this.features = features;
    }

    @Override
    protected boolean isVisible(
            ITemplateContext context,
            IProcessableElementTag tag,
            AttributeName attributeName,
            String attributeValue)
    {
        IStandardExpressionParser expressionParser = StandardExpressions.getExpressionParser(context.getConfiguration());

        IStandardExpression expression = expressionParser.parseExpression(context, attributeValue);
        Object value = expression.execute(context);

        if (value instanceof String)
        {
            return features.isFeatureEnabled((String) value);
        }
        else
        {
            return false;
        }
    }
}
