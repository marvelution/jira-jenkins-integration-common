/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.templating.thymeleaf.dialect;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.marvelution.jji.api.features.Features;
import org.marvelution.jji.templating.thymeleaf.expression.CommonExpressionObjects;
import org.marvelution.jji.templating.thymeleaf.processor.FeaturesTagProcessor;
import org.marvelution.jji.templating.thymeleaf.processor.LoadAttrTagProcessor;
import org.marvelution.jji.templating.thymeleaf.resourceloader.ResourceLoaderFactory;

import org.thymeleaf.dialect.AbstractProcessorDialect;
import org.thymeleaf.dialect.IExecutionAttributeDialect;
import org.thymeleaf.dialect.IExpressionObjectDialect;
import org.thymeleaf.expression.IExpressionObjectFactory;
import org.thymeleaf.processor.IProcessor;
import org.thymeleaf.standard.processor.StandardIfTagProcessor;
import org.thymeleaf.standard.processor.StandardRemovableAttributeTagProcessor;
import org.thymeleaf.standard.processor.StandardUnlessTagProcessor;
import org.thymeleaf.standard.processor.StandardXmlNsTagProcessor;
import org.thymeleaf.templatemode.TemplateMode;

@Named
@javax.inject.Named
public class CommonDialect
        extends AbstractProcessorDialect
        implements IExpressionObjectDialect, IExecutionAttributeDialect
{

    private static final String PREFIX = "jji";
    private static final int PRECEDENCE = 100;
    public static String RESOURCE_LOADER_FACTORY = ResourceLoaderFactory.class.getSimpleName();
    private final CommonExpressionObjects commonExpressionObjects;
    private final ResourceLoaderFactory resourceLoaderFactory;
    private final Features features;

    @Inject
    @javax.inject.Inject
    public CommonDialect(
            CommonExpressionObjects commonExpressionObjects,
            ResourceLoaderFactory resourceLoaderFactory,
            Features features)
    {
        super("common", PREFIX, PRECEDENCE);
        this.commonExpressionObjects = commonExpressionObjects;
        this.resourceLoaderFactory = resourceLoaderFactory;
        this.features = features;
    }

    @Override
    public IExpressionObjectFactory getExpressionObjectFactory()
    {
        return commonExpressionObjects;
    }

    @Override
    public Set<IProcessor> getProcessors(String dialectPrefix)
    {
        Set<IProcessor> processors = new HashSet<>();
        processors.add(new StandardIfTagProcessor(TemplateMode.HTML, dialectPrefix));
        processors.add(new StandardUnlessTagProcessor(TemplateMode.HTML, dialectPrefix));
        processors.add(new StandardRemovableAttributeTagProcessor(dialectPrefix, "id"));
        processors.add(new LoadAttrTagProcessor(TemplateMode.HTML, dialectPrefix));
        processors.add(new StandardXmlNsTagProcessor(TemplateMode.HTML, dialectPrefix));
        processors.add(new FeaturesTagProcessor(TemplateMode.HTML, dialectPrefix, features));
        return processors;
    }

    @Override
    public Map<String, Object> getExecutionAttributes()
    {
        Map<String, Object> attributes = new HashMap<>();
        attributes.put(RESOURCE_LOADER_FACTORY, resourceLoaderFactory);
        return attributes;
    }
}
