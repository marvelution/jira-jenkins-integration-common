/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.templating.thymeleaf.expression;

import jakarta.inject.Inject;
import jakarta.inject.Named;

@Named
@javax.inject.Named
public class CommonExpressionObjects
        extends ExpressionObjectRegistry
{

    @Inject
    @javax.inject.Inject
    public CommonExpressionObjects(
            Config config,
            Times times,
            Licenses licenses,
            Urls urls,
            FeaturesExpressionObjectFactory featuresExpressionObjectFactory,
            Permissions<?> permissions,
            Inline inline,
            Tenants tenants)
    {
        this(config, times, licenses, urls, permissions, inline, tenants);
        registerObject("features", featuresExpressionObjectFactory);
    }

    public CommonExpressionObjects(NamedExpressionObject... objects)
    {
        for (NamedExpressionObject object : objects)
        {
            registerObject(object);
        }
    }
}
