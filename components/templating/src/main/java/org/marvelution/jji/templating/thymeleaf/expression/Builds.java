/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.templating.thymeleaf.expression;

import java.util.List;
import java.util.Set;
import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.marvelution.jji.data.helper.PanelBuild;
import org.marvelution.jji.data.services.api.BuildService;
import org.marvelution.jji.data.services.api.IssueLinkService;
import org.marvelution.jji.model.Build;
import org.marvelution.jji.model.IssueReference;
import org.marvelution.jji.model.Job;

import static java.util.Collections.reverseOrder;
import static java.util.Comparator.comparingLong;
import static java.util.stream.Collectors.toList;

@Named
@javax.inject.Named
public class Builds
        implements NamedExpressionObject
{

    private static final String EXPRESSION_OBJECT_NAME = "builds";
    private final BuildService buildService;
    private final IssueLinkService issueLinkService;

    @Inject
    @javax.inject.Inject
    public Builds(
            BuildService buildService,
            IssueLinkService issueLinkService)
    {
        this.buildService = buildService;
        this.issueLinkService = issueLinkService;
    }

    @Override
    public String getName()
    {
        return EXPRESSION_OBJECT_NAME;
    }

    public List<Build> latestBuilds(Job job)
    {
        return buildService.getByJob(job, 10)
                .stream()
                .sorted(reverseOrder(comparingLong(Build::getTimestamp)))
                .collect(toList());
    }

    public String buildResultClasses(Build build)
    {
        return PanelBuild.getBuildColor(build.getResult()) + " " + PanelBuild.getBuildIcon(build.getResult());
    }

    public Set<IssueReference> issueReferences(Build build)
    {
        return issueLinkService.getIssueLinks(build);
    }
}
