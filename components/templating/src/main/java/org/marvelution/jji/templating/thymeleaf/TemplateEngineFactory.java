/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.templating.thymeleaf;

import javax.annotation.*;
import java.nio.charset.*;
import java.util.*;

import org.marvelution.jji.api.text.*;

import org.springframework.context.*;
import org.thymeleaf.*;
import org.thymeleaf.dialect.*;
import org.thymeleaf.templatemode.*;
import org.thymeleaf.templateresolver.*;

public abstract class TemplateEngineFactory
{

    private final ApplicationContext springContext;
    private final List<IDialect> dialects;
    private final TextResolver textResolver;

    public TemplateEngineFactory(
            ApplicationContext springContext,
            List<IDialect> dialects,
            TextResolver textResolver)
    {
        this.springContext = springContext;
        this.dialects = dialects;
        this.textResolver = textResolver;
    }

    public TemplateEngine createTemplateEngine(
            @Nullable
            Collection<ITemplateResolver> templateResolvers,
            @Nullable
            Collection<IDialect> dialects)
    {
        MessageSource messageSource;
        if (textResolver instanceof MessageSource)
        {
            messageSource = (MessageSource) textResolver;
        }
        else
        {
            messageSource = new TextResolverDelegatingMessageSource(textResolver);
        }

        TemplateEngine templateEngine = newTemplateEngine(messageSource);

        AbstractConfigurableTemplateResolver mainTemplateResolver = newTemplateResolver(springContext);
        mainTemplateResolver.setPrefix("classpath:/templates/");
        mainTemplateResolver.setSuffix(".html");
        mainTemplateResolver.setTemplateMode(TemplateMode.HTML);
        mainTemplateResolver.setCharacterEncoding(StandardCharsets.UTF_8.name());
        mainTemplateResolver.setCacheable(true);
        mainTemplateResolver.setCheckExistence(true);
        mainTemplateResolver.setOrder(10);
        templateEngine.addTemplateResolver(mainTemplateResolver);

        if (templateResolvers != null)
        {
            templateResolvers.forEach(templateEngine::addTemplateResolver);
        }

        List<IDialect> dialectList = new ArrayList<>(this.dialects);
        if (dialects != null)
        {
            dialects.stream()
                    .filter(dialect -> !dialectList.contains(dialect))
                    .forEach(dialectList::add);
        }
        dialectList.forEach(templateEngine::addDialect);

        return templateEngine;
    }

    protected abstract TemplateEngine newTemplateEngine(MessageSource messageSource);

    protected abstract AbstractConfigurableTemplateResolver newTemplateResolver(ApplicationContext springContext);

    private static class TextResolverDelegatingMessageSource
            implements MessageSource
    {

        private final TextResolver textResolver;

        private TextResolverDelegatingMessageSource(TextResolver textResolver)
        {
            this.textResolver = textResolver;
        }

        @Override
        public String getMessage(
                String code,
                Object[] args,
                String defaultMessage,
                Locale locale)
        {
            String text = getText(locale, code, args);
            if (text != null && !Objects.equals(text, code))
            {
                return text;
            }
            return defaultMessage;
        }

        @Override
        public String getMessage(
                String code,
                Object[] args,
                Locale locale)
        {
            return getText(locale, code, args);
        }

        @Override
        public String getMessage(
                MessageSourceResolvable resolvable,
                Locale locale)
        {
            for (String code : resolvable.getCodes())
            {
                String text = getText(locale, code, resolvable.getArguments());
                if (text != null && !Objects.equals(text, code))
                {
                    return text;
                }
            }
            return resolvable.getDefaultMessage();
        }

        private String getText(
                Locale locale,
                String key,
                Object... arguments)
        {
            return textResolver.getText(locale, key, arguments);
        }
    }
}
