/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.templating.thymeleaf.expression;

import java.net.*;
import java.util.*;

import org.marvelution.jji.model.*;
import org.marvelution.jji.synctoken.*;
import org.marvelution.jji.templating.thymeleaf.resourceloader.*;

import org.slf4j.*;
import org.thymeleaf.context.*;

import static java.util.Collections.*;
import static org.apache.commons.lang3.StringUtils.*;

public abstract class Urls
        extends AbstractExpressionObject<Urls>
{

    private static final Logger LOGGER = LoggerFactory.getLogger(Urls.class);
    private static final String EXPRESSION_OBJECT_NAME = "urls";
    private static final String HELP_LINKS_RESOURCE_NAME = "templates/help-links.yaml";
    private Map<String, String> helpLinks;

    protected Urls(IExpressionContext expressionContext)
    {
        super(expressionContext);
    }

    @Override
    public String getName()
    {
        return EXPRESSION_OBJECT_NAME;
    }

    public abstract String baseUrl();

    public String url(String path)
    {
        return resolvePath(baseUrl(), path);
    }

    public abstract String restBaseUrl(boolean absolute);

    public String restBaseUrl()
    {
        return restBaseUrl(false);
    }

    public String restUrl(String path)
    {
        return resolvePath(restBaseUrl(), path);
    }

    public abstract String docsBaseUrl();

    public String docsUrl(String path)
    {
        return resolvePath(docsBaseUrl(), path);
    }

    public String helpUrl(String featureOrPath)
    {
        if (helpLinks == null)
        {
            Optional<ResourceLoader> resourceLoader = getContext().map(IExpressionContext::getConfiguration)
                    .map(ResourceLoaderFactory::locateResourceLoader);
            if (resourceLoader.isPresent())
            {
                try
                {
                    helpLinks = resourceLoader.get()
                            .loadYaml(HELP_LINKS_RESOURCE_NAME);
                }
                catch (Exception e)
                {
                    LOGGER.warn("Unable to load help-links from '{}'", HELP_LINKS_RESOURCE_NAME, e);
                }
            }
            else
            {
                LOGGER.warn("No ResourceLoaderFactory found to load help links.");
            }
            if (helpLinks == null)
            {
                helpLinks = emptyMap();
            }
        }
        String path;
        if (helpLinks.containsKey(featureOrPath))
        {
            path = helpLinks.get(featureOrPath);
            if (path.startsWith("/"))
            {
                path = "." + path;
            }
            else if (!path.startsWith("./"))
            {
                path = "./" + path;
            }
        }
        else
        {
            path = featureOrPath;
        }
        return Optional.ofNullable(path)
                .map(this::docsUrl)
                .orElse(null);
    }

    protected String resolvePath(
            String base,
            String path)
    {
        if (isBlank(path))
        {
            return null;
        }
        else
        {
            if (isBlank(base))
            {
                return base;
            }
            if (!base.endsWith("/"))
            {
                base += "/";
            }
            return URI.create(base)
                    .resolve(path)
                    .toASCIIString();
        }
    }

    public String urlHost(URI uri)
    {
        return uri.getHost();
    }

    public String urlScheme(URI uri)
    {
        return uri.getScheme();
    }

    public String sign(
            String url,
            Site site)
    {
        return new SyncTokenBuilder(900).identifier(site.getId())
                .sharedSecret(site.getSharedSecret())
                .request(new CanonicalHttpServletRequest("GET", URI.create(url), contextPath()))
                .generateToken();

    }

    protected Optional<String> contextPath()
    {
        return Optional.empty();
    }
}
