/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.templating.thymeleaf.expression;

import java.io.*;

import jakarta.inject.*;
import org.thymeleaf.context.*;
import org.thymeleaf.standard.serializer.*;
import org.thymeleaf.util.*;

@Named
@javax.inject.Named
public class Inline
        extends AbstractExpressionObject<Inline>
{

    private static final String EXPRESSION_OBJECT_NAME = "inline";

    public Inline()
    {
        this(null);
    }

    private Inline(IExpressionContext expressionContext)
    {
        super(expressionContext);
    }

    @Override
    public String getName()
    {
        return EXPRESSION_OBJECT_NAME;
    }

    @Override
    public Inline withContext(IExpressionContext context)
    {
        return new Inline(context);
    }

    public String javascript(Object input)
    {
        IStandardJavaScriptSerializer serializer = getContext().map(IExpressionContext::getConfiguration)
                .map(StandardSerializers::getJavaScriptSerializer)
                .orElse(null);
        if (serializer != null)
        {
            final Writer jsWriter = new FastStringWriter(input instanceof String ? ((String) input).length() * 2 : 20);
            serializer.serializeValue(input, jsWriter);
            return jsWriter.toString();
        }
        else
        {
            return input.toString();
        }
    }
}
