/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.templating.thymeleaf.expression;

import java.net.*;

import org.marvelution.jji.data.services.api.*;
import org.marvelution.jji.model.*;

import org.apache.commons.lang3.*;
import org.thymeleaf.context.*;

import static org.marvelution.jji.data.services.api.ConfigurationService.*;

public abstract class Config
        extends AbstractExpressionObject<Config>
{

    protected static final String BASIC = "basic";
    protected static final String TRIGGER_BUILD = "trigger_build";
    protected static final String AUTOMATION = "automation";
    protected static final String ADVANCED = "advanced";
    private static final String[] SECTION_KEYS = {BASIC,
            TRIGGER_BUILD,
            AUTOMATION,
            ADVANCED};
    private static final String[] BASIC_KEYS = {MAX_BUILDS_PER_PAGE};
    private static final String[] TRIGGER_BUILD_KEYS = {USE_USER_ID,
            PARAMETER_ISSUE_FIELDS,
            PARAMETER_ISSUE_FIELD_NAMES,
            PARAMETER_ISSUE_FIELD_MAPPERS};
    private static final String[] AUTOMATION_KEYS = {};
    private static final String[] ADVANCED_KEYS = {DELETED_DATA_RETENTION_PERIOD,
            AUDIT_DATA_RETENTION_PERIOD,
            ENABLED_PROJECT_KEYS};
    protected final ConfigurationService configurationService;
    private transient Configuration configuration;

    protected Config(
            ConfigurationService configurationService,
            IExpressionContext expressionContext)
    {
        super(expressionContext);
        this.configurationService = configurationService;
    }

    @Override
    public String getName()
    {
        return "config";
    }

    public String instanceName()
    {
        return configurationService.getInstanceName();
    }

    public URI baseAPIUrl()
    {
        return configurationService.getBaseAPIUrl();
    }

    public String[] sections()
    {
        return SECTION_KEYS;
    }

    public String[] settingKeys(
            String scope,
            String section)
    {
        String[] baseKeys = null;
        String[] additionalSettingKeys = additionalSettingKeys(scope, section);

        switch (section)
        {
            case BASIC:
                baseKeys = BASIC_KEYS;
                break;
            case TRIGGER_BUILD:
                baseKeys = TRIGGER_BUILD_KEYS;
                break;
            case AUTOMATION:
                if (StringUtils.isBlank(scope))
                {
                    baseKeys = AUTOMATION_KEYS;
                }
                break;
            case ADVANCED:
                if (StringUtils.isBlank(scope))
                {
                    baseKeys = ADVANCED_KEYS;
                }
                break;
        }

        return mergeKeys(baseKeys, additionalSettingKeys);
    }

    protected String[] additionalSettingKeys(
            String scope,
            String section)
    {
        return null;
    }

    private String[] mergeKeys(
            String[] common,
            String[] specific)
    {
        boolean commonEmpty = (common == null || common.length == 0);
        boolean specificEmpty = (specific == null || specific.length == 0);

        if (commonEmpty && specificEmpty)
        {
            return new String[0];
        }
        else if (commonEmpty)
        {
            return specific;
        }
        else if (specificEmpty)
        {
            return common;
        }
        else
        {
            return ArrayUtils.addAll(common, specific);
        }
    }

    public ConfigurationSetting getSetting(
            String scope,
            String key)
    {
        return getConfiguration(scope).getConfiguration(key)
                .orElse(ConfigurationSetting.forKey(key)
                        .setScope(scope));
    }

    public Configuration getConfiguration(String scope)
    {
        if (configuration == null)
        {
            configuration = configurationService.getConfiguration(scope);
        }
        return configuration;
    }
}
