/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.templating.thymeleaf.expression;

import java.util.*;
import java.util.stream.*;

import org.marvelution.jji.api.features.*;
import org.marvelution.jji.data.services.api.*;
import org.marvelution.jji.model.*;

import jakarta.inject.*;
import org.thymeleaf.context.*;

@Named
@javax.inject.Named
public class Sites
        extends AbstractExpressionObject<Sites>
{

    private static final String EXPRESSION_OBJECT_NAME = "sites";
    private final ConfigurationService configurationService;
    private final Features features;

    @Inject
    @javax.inject.Inject
    public Sites(
            ConfigurationService configurationService,
            Features features)
    {
        this(configurationService, features, null);
    }

    public Sites(
            ConfigurationService configurationService,
            Features features,
            IExpressionContext context)
    {
        super(context);
        this.configurationService = configurationService;
        this.features = features;
    }

    @Override
    public String getName()
    {
        return EXPRESSION_OBJECT_NAME;
    }

    public boolean canManage(Site site)
    {
        Object scope = getScope();
        return ConfigurationService.GLOBAL_SCOPE.equals(scope) || Objects.equals(site.getScope(), scope);
    }

    public String badge(Site site)
    {
        Object scope = getScope();
        if (!Objects.equals(scope, site.getScope()))
        {
            return ConfigurationService.GLOBAL_SCOPE.equals(site.getScope()) ? "global" : site.getScope();
        }
        return null;
    }

    private Object getScope()
    {
        return getContext().map(context -> context.getVariable("scope"))
                .orElse(null);
    }

    public SiteAuthenticationType[] authTypes()
    {
        return SiteAuthenticationType.values();
    }

    public SiteConnectionType[] connectionTypes()
    {
        return Stream.of(SiteConnectionType.values())
                .filter(type -> !type.featured() || features.isFeatureEnabled(type.featureFlag()))
                .toArray(SiteConnectionType[]::new);
    }

    @Override
    public Sites withContext(IExpressionContext context)
    {
        return new Sites(configurationService, features, context);
    }
}
