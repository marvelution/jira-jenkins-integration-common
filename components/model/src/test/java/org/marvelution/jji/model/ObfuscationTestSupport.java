/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.model;

import java.io.IOException;
import java.net.URI;

import org.marvelution.testing.TestSupport;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.marvelution.jji.model.SiteType.JENKINS;

public abstract class ObfuscationTestSupport
        extends TestSupport
{

    @Test
    void testSerialize_Site()
            throws Exception
    {
        String json = serialize(createSite());

        assertSite(deserialize(json, Site.class));
    }

    @Test
    void testSerialize_Job()
            throws Exception
    {
        String json = serialize(new Job().setId("id")
                .setName("name")
                .setSite(createSite()));
        Job job = deserialize(json, Job.class);

        assertThat(job.getId(), is("id"));
        assertThat(job.getName(), is("name"));
        assertSite(job.getSite());
    }

    @Test
    void testSerialize_Build()
            throws Exception
    {
        String json = serialize(new Build().setId("id")
                .setNumber(1)
                .setJob(new Job().setId("id")
                        .setName("name")
                        .setSite(createSite())));
        Build build = deserialize(json, Build.class);

        assertThat(build.getId(), is("id"));
        assertThat(build.getNumber(), is(1));
        assertThat(build.getJob()
                .getId(), is("id"));
        assertThat(build.getJob()
                .getName(), is("name"));
        assertSite(build.getJob()
                .getSite());
    }

    protected abstract String serialize(Object value)
            throws IOException;

    protected abstract <T> T deserialize(
            String serialized,
            Class<T> type)
            throws IOException;

    private Site createSite()
    {
        return new Site().setId("id")
                .setName("name")
                .setSharedSecret("shared-secret")
                .setType(JENKINS)
                .setRpcUrl(URI.create("http://localhost/"))
                .setUser("user")
                .setToken("My Secret");
    }

    private void assertSite(Site site)
    {
        assertThat(site.getId(), is("id"));
        assertThat(site.getName(), is("name"));
        assertThat(Obfuscate.isObfuscated(site.getSharedSecret()), is(true));
        assertThat(site.getType(), is(JENKINS));
        assertThat(site.getRpcUrl(), is(URI.create("http://localhost/")));
        assertThat(site.getUser(), is("user"));
        assertThat(Obfuscate.isObfuscated(site.getToken()), is(true));
    }
}
