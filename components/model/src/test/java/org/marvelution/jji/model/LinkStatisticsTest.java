/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.model;

import java.time.Instant;
import jakarta.json.bind.Jsonb;
import jakarta.json.bind.JsonbBuilder;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.assertj.core.api.Assertions.assertThat;

class LinkStatisticsTest
{

    private final Jsonb jsonb = JsonbBuilder.create();
    private LinkStatistics stats;

    @BeforeEach
    void setUp()
    {
        stats = new LinkStatistics().setTotal(10)
                .setBuildTotal(20)
                .setWorstResult(Result.FAILURE)
                .addCountByResult(Result.SUCCESS, 10)
                .addCountByResult(Result.FAILURE, 5)
                .addCountByResult(Result.UNSTABLE, 5)
                .setLatestBuild(new LinkStatistics.BuildStats("latest-id",
                        Instant.parse("2023-01-28T13:31:00.00Z")
                                .toEpochMilli(),
                        Result.SUCCESS))
                .setWorstBuild(new LinkStatistics.BuildStats("worst-id",
                        Instant.parse("2023-01-28T11:31:00.00Z")
                                .toEpochMilli(),
                        Result.FAILURE))
                .setDeployTotal(5)
                .setWorstDeployResult(Result.FAILURE)
                .addDeployCountByResult(Result.SUCCESS, 3)
                .addDeployCountByResult(Result.FAILURE, 2)
                .setLatestDeploy(new LinkStatistics.DeployStats("latest-id",
                        Instant.parse("2023-01-28T13:31:00.00Z")
                                .toEpochMilli(),
                        Result.SUCCESS,
                        "prod",
                        "Prod",
                        DeploymentEnvironmentType.PRODUCTION))
                .setWorstDeploy(new LinkStatistics.DeployStats("worst-id",
                        Instant.parse("2023-01-20T13:31:00.00Z")
                                .toEpochMilli(),
                        Result.FAILURE,
                        "prod",
                        "Prod",
                        DeploymentEnvironmentType.PRODUCTION));
    }

    @Test
    void testToJson()
    {
        assertThat(jsonb.toJson(stats)).isEqualTo(
                "{\"total\":10,\"buildTotal\":20,\"worstResult\":\"FAILURE\",\"successRate\":50.0,\"latestBuild\":{\"id\":\"latest-id\"," +
                "\"result\":\"SUCCESS\",\"timestamp\":1674912660000},\"worstBuild\":{\"id\":\"worst-id\",\"result\":\"FAILURE\"," +
                "\"timestamp\":1674905460000},\"countByResult\":{\"SUCCESS\":10,\"UNSTABLE\":5,\"FAILURE\":5,\"NOT_BUILT\":0," +
                "\"ABORTED\":0,\"UNKNOWN\":0},\"deployTotal\":5,\"worstDeployResult\":\"FAILURE\",\"deploySuccessRate\":60.0," +
                "\"latestDeploy\":{\"id\":\"latest-id\",\"result\":\"SUCCESS\",\"timestamp\":1674912660000," +
                "\"deploymentId\":\"prod\",\"name\":\"Prod\",\"type\":\"PRODUCTION\"},\"worstDeploy\":{\"id\":\"worst-id\"," +
                "\"result\":\"FAILURE\",\"timestamp\":1674221460000,\"deploymentId\":\"prod\",\"name\":\"Prod\"," +
                "\"type\":\"PRODUCTION\"},\"deployCountByResult\":{\"SUCCESS\":3,\"UNSTABLE\":0,\"FAILURE\":2,\"NOT_BUILT\":0," +
                "\"ABORTED\":0,\"UNKNOWN\":0}}");
    }

    @ParameterizedTest
    @ValueSource(strings = {"linkstatistics-jsonb-ordered.json",
            "linkstatistics-jsonb.json",
            "linkstatistics-gson.json"})
    void testFromJson(String resource)
    {
        LinkStatistics linkStatistics = jsonb.fromJson(getClass().getResourceAsStream(resource), LinkStatistics.class);
        assertThat(linkStatistics).isEqualTo(stats);
    }
}
