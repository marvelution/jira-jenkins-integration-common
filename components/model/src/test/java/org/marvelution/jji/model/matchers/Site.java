/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.model.matchers;

import java.util.*;

import org.hamcrest.*;

/**
 * {@link Matcher} for {@link org.marvelution.jji.model.Site sites}.
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class Site
		extends TypeSafeDiagnosingMatcher<org.marvelution.jji.model.Site>
{

	private final org.marvelution.jji.model.Site site;
	private final boolean ignoreId;

	private Site(
			org.marvelution.jji.model.Site site,
			boolean ignoreId)
	{
		this.site = site;
		this.ignoreId = ignoreId;
	}

	@Override
	public void describeTo(Description description)
	{
		description.appendText("site same as ").appendValue(site);
		if (ignoreId)
		{
			description.appendText(" ignoring id");
		}
	}

	@Override
	protected boolean matchesSafely(
			org.marvelution.jji.model.Site item,
			Description mismatchDescription)
	{
		if (!ignoreId && !Objects.equals(site.getId(), item.getId()))
		{
			mismatchDescription.appendText("has id ").appendValue(item.getId());
			return false;
		}
		else if (!Objects.equals(site.getType(), item.getType()))
		{
			mismatchDescription.appendText("has type ").appendValue(item.getType());
			return false;
		}
		else if (!Objects.equals(site.getName(), item.getName()))
		{
			mismatchDescription.appendText("has name ").appendValue(item.getName());
			return false;
		}
		else if (site.getConnectionType() != item.getConnectionType())
		{
			mismatchDescription.appendText("has connectionType ").appendValue(item.getConnectionType());
			return false;
		}
		else if (!Objects.equals(site.getRpcUrl(), item.getRpcUrl()))
		{
			mismatchDescription.appendText("has RPC URL ").appendValue(item.getRpcUrl());
			return false;
		}
		else if (!Objects.equals(site.getDisplayUrl(), item.getDisplayUrl()))
		{
			mismatchDescription.appendText("has display URL ").appendValue(item.getDisplayUrl());
			return false;
		}
		else if (!Objects.equals(site.getUser(), item.getUser()))
		{
			mismatchDescription.appendText("has user ").appendValue(item.getUser());
			return false;
		}
		else if (site.isAutoLinkNewJobs() != item.isAutoLinkNewJobs())
		{
			mismatchDescription.appendText("has autoLinkNewJobs ").appendValue(item.isAutoLinkNewJobs());
			return false;
		}
		return true;
	}

	@Factory
	public static Matcher<org.marvelution.jji.model.Site> equalTo(org.marvelution.jji.model.Site site)
	{
		return equalTo(site, false);
	}

	@Factory
	public static Matcher<org.marvelution.jji.model.Site> equalTo(
			org.marvelution.jji.model.Site site,
			boolean ignoreId)
	{
		return new Site(site, ignoreId);
	}

}
