/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.utils;

import java.net.URI;

import org.marvelution.jji.model.Build;
import org.marvelution.jji.model.Job;
import org.marvelution.jji.model.Site;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.marvelution.jji.utils.SiteUrl.displayUrl;
import static org.marvelution.jji.utils.SiteUrl.syncUrl;

class SiteUrlTest
{
    private Site site;
    private Job job;

    @BeforeEach
    void setUp()
    {
        site = new Site().setRpcUrl(URI.create("http://localhost/"))
                .setDisplayUrl(URI.create("https://ci.local/"));
        job = new Job().setSite(site)
                .setName("some-job");
    }

    @Test
    void testSiteUrl_SyncMode()
    {
        assertThat(syncUrl().site(site)
                .url()).isEqualTo(URI.create("http://localhost/"));
    }

    @Test
    void testSiteUrl_SyncMode_ApiUrl()
    {
        assertThat(syncUrl().site(site)
                .api()
                .url()).isEqualTo(URI.create("http://localhost/api/json/"));
    }

    @Test
    void testSiteUrl_SyncMode_Path()
    {
        assertThat(syncUrl().site(site)
                .path("/testing")
                .url()).isEqualTo(URI.create("http://localhost/testing"));
    }

    @Test
    void testSiteUrl_SyncMode_ApiAppendsPath()
    {
        assertThat(syncUrl().site(site)
                .path("/testing")
                .api()
                .url()).isEqualTo(URI.create("http://localhost/testing/api/json/"));
    }

    @Test
    void testSiteUrl_SyncMode_PathOverridesApi()
    {
        assertThat(syncUrl().site(site)
                .api()
                .path("/testing")
                .url()).isEqualTo(URI.create("http://localhost/testing"));
    }

    @Test
    void testSiteUrl_DisplayMode()
    {
        assertThat(displayUrl().site(site)
                .url()).isEqualTo(URI.create("https://ci.local/"));
    }

    @Test
    void testSiteUrl_DisplayMode_Path()
    {
        assertThat(displayUrl().site(site)
                .path("/testing")
                .url()).isEqualTo(URI.create("https://ci.local/testing"));
    }

    @Test
    void testSiteUrl_MissingSite()
    {
        assertThrows(IllegalStateException.class, () -> syncUrl().url());
    }

    @Test
    void testSiteUrl_MissingSite_Nullable()
    {
        assertThat(syncUrl().nullable()
                .url()).isNull();
    }

    @Test
    void testJobUrl_EncodedCharactersInName()
    {
        assertThat(syncUrl().job(job.setUrlName("test%2Fslashs"))
                .api()
                .url()).isEqualTo(URI.create("http://localhost/job/test%2Fslashs/api/json/"));
    }

    @Test
    void testJobUrl_EncodeNameIfUrlNameIsBlank()
    {
        assertThat(syncUrl().job(job.setName("some job"))
                .url()).isEqualTo(URI.create("http://localhost/job/some%20job/"));
    }

    @Test
    void testJobUrl_SyncMode()
    {
        assertThat(syncUrl().job(job)
                .url()).isEqualTo(URI.create("http://localhost/job/some-job/"));
    }

    @Test
    void testJobUrl_SyncMode_ApiUrl()
    {
        assertThat(syncUrl().job(job)
                .api()
                .url()).isEqualTo(URI.create("http://localhost/job/some-job/api/json/"));
    }

    @Test
    void testJobUrl_SyncMode_Path()
    {
        assertThat(syncUrl().job(job)
                .path("/testing")
                .url()).isEqualTo(URI.create("http://localhost/job/some-job/testing"));
    }

    @Test
    void testJobUrl_SyncMode_ApiAppendsPath()
    {
        assertThat(syncUrl().job(job)
                .path("/testing")
                .api()
                .url()).isEqualTo(URI.create("http://localhost/job/some-job/testing/api/json/"));
    }

    @Test
    void testJobUrl_SyncMode_PathOverridesApi()
    {
        assertThat(syncUrl().job(job)
                .api()
                .path("/testing")
                .url()).isEqualTo(URI.create("http://localhost/job/some-job/testing"));
    }

    @Test
    void testJobUrl_DisplayMode()
    {
        assertThat(displayUrl().job(job)
                .url()).isEqualTo(URI.create("https://ci.local/job/some-job/"));
    }

    @Test
    void testJobUrl_DisplayMode_Path()
    {
        assertThat(displayUrl().job(job)
                .path("/testing")
                .url()).isEqualTo(URI.create("https://ci.local/job/some-job/testing"));
    }

    @Test
    void testJobUrl_MissingSite()
    {
        assertThrows(IllegalStateException.class,
                () -> syncUrl().job(new Job().setName("some-job"))
                        .url());
    }

    @Test
    void testJobUrl_InvalidSite()
    {
        assertThrows(IllegalStateException.class,
                () -> syncUrl().site(new Site().setId("1"))
                        .job(new Job().setSite(new Site().setId("2"))
                                .setName("some-job"))
                        .url());
    }

    @Test
    void testJobUrl_MissingSite_Nullable()
    {
        assertThat(syncUrl().job(new Job().setName("some-job"))
                .nullable()
                .url()).isNull();
    }

    @Test
    void testJobUrl_InvalidSite_Nullable()
    {
        assertThat(syncUrl().site(new Site().setId("1"))
                .job(new Job().setSite(new Site().setId("2"))
                        .setName("some-job"))
                .nullable()
                .url()).isNull();
    }

    @Test
    void testBuildUrl_SyncMode()
    {
        assertThat(syncUrl().build(new Build().setNumber(1)
                        .setJob(job))
                .url()).isEqualTo(URI.create("http://localhost/job/some-job/1/"));
    }

    @Test
    void testBuildUrl_SyncMode_ApiUrl()
    {
        assertThat(syncUrl().build(new Build().setNumber(1)
                        .setJob(job))
                .api()
                .url()).isEqualTo(URI.create("http://localhost/job/some-job/1/api/json/"));
    }

    @Test
    void testBuildUrl_SyncMode_Path()
    {
        assertThat(syncUrl().build(new Build().setNumber(1)
                        .setJob(job))
                .path("test")
                .url()).isEqualTo(URI.create("http://localhost/job/some-job/1/test"));
    }

    @Test
    void testBuildUrl_SyncMode_ApiAppendsPath()
    {
        assertThat(syncUrl().build(new Build().setNumber(1)
                        .setJob(job))
                .path("test")
                .api()
                .url()).isEqualTo(URI.create("http://localhost/job/some-job/1/test/api/json/"));
    }

    @Test
    void testBuildUrl_SyncMode_PathOverridesApi()
    {
        assertThat(syncUrl().build(new Build().setNumber(1)
                        .setJob(job))
                .api()
                .path("test")
                .url()).isEqualTo(URI.create("http://localhost/job/some-job/1/test"));
    }

    @Test
    void testBuildUrl_DisplayMode()
    {
        assertThat(displayUrl().build(new Build().setNumber(1)
                        .setJob(job))
                .url()).isEqualTo(URI.create("https://ci.local/job/some-job/1/"));
    }

    @Test
    void testBuildUrl_DisplayMode_Fragment()
    {
        assertThat(displayUrl().build(new Build().setNumber(1)
                        .setJob(job))
                .fragment("commit")
                .url()).isEqualTo(URI.create("https://ci.local/job/some-job/1/#commit"));
    }

    @Test
    void testBuildUrl_DisplayMode_Query()
    {
        assertThat(displayUrl().build(new Build().setNumber(1)
                        .setJob(job))
                .query("test")
                .url()).isEqualTo(URI.create("https://ci.local/job/some-job/1/?test"));
        assertThat(displayUrl().build(new Build().setNumber(1)
                        .setJob(job))
                .query("testKey", "testValue")
                .query("other")
                .url()).isEqualTo(URI.create("https://ci.local/job/some-job/1/?testKey=testValue&other"));
    }

    @Test
    void testBuildUrl_DisplayMode_Path()
    {
        assertThat(displayUrl().build(new Build().setNumber(1)
                        .setJob(job))
                .path("test")
                .url()).isEqualTo(URI.create("https://ci.local/job/some-job/1/test"));
    }

    @Test
    void testBuildUrl_MissingJob()
    {
        assertThrows(IllegalStateException.class,
                () -> syncUrl().build(new Build().setNumber(1))
                        .url());
    }

    @Test
    void testBuildUrl_InvalidJob()
    {
        assertThrows(IllegalStateException.class,
                () -> syncUrl().job(new Job().setId("1"))
                        .build(new Build().setJob(new Job().setId("2")
                                .setName("some-job")))
                        .url());
    }

    @Test
    void testBuildUrl_MissingSite()
    {
        assertThrows(IllegalStateException.class,
                () -> syncUrl().build(new Build().setJob(new Job().setName("some-job")))
                        .url());
    }

    @Test
    void testBuildUrl_InvalidSite()
    {
        assertThrows(IllegalStateException.class,
                () -> syncUrl().site(new Site().setId("1"))
                        .build(new Build().setJob(new Job().setSite(new Site().setId("2"))
                                .setName("some-job")))
                        .url());
    }

    @Test
    void testBuildUrl_MissingJob_Nullable()
    {
        assertThat(syncUrl().build(new Build().setNumber(1))
                .nullable()
                .url()).isNull();
    }

    @Test
    void testBuildUrl_InvalidJob_Nullable()
    {
        assertThat(syncUrl().job(new Job().setId("1"))
                .build(new Build().setJob(new Job().setId("2")
                        .setName("some-job")))
                .nullable()
                .url()).isNull();
    }

    @Test
    void testBuildUrl_MissingSite_Nullable()
    {
        assertThat(syncUrl().build(new Build().setJob(new Job().setName("some-job")))
                .nullable()
                .url()).isNull();
    }

    @Test
    void testBuildUrl_InvalidSite_Nullable()
    {
        assertThat(syncUrl().site(new Site().setId("1"))
                .build(new Build().setJob(new Job().setSite(new Site().setId("2"))
                        .setName("some-job")))
                .nullable()
                .url()).isNull();
    }

    @Test
    void testEncodeSquareBracketsInQueryString()
    {
        assertThat(syncUrl().job(job)
                .query("tree", "name,firstBuild[number],lastBuild[number],builds[number,result],jobs[*]")
                .url()).isEqualTo(URI.create(
                "http://localhost/job/some-job/?tree=name,firstBuild%5Bnumber%5D,lastBuild%5Bnumber%5D,builds%5Bnumber,result%5D,jobs%5B*%5D"));
    }
}
