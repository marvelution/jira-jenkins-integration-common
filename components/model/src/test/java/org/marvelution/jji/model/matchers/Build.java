/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.model.matchers;

import java.util.*;

import org.hamcrest.*;

/**
 * {@link Matcher} for {@link org.marvelution.jji.model.Build builds}.
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class Build
		extends TypeSafeDiagnosingMatcher<org.marvelution.jji.model.Build>
{

	private final org.marvelution.jji.model.Build build;
	private final boolean ignoreId;

	private Build(
			org.marvelution.jji.model.Build build,
			boolean ignoreId)
	{
		this.build = build;
		this.ignoreId = ignoreId;
	}

	@Override
	public void describeTo(Description description)
	{
		description.appendText("build same as ").appendValue(build);
		if (ignoreId)
		{
			description.appendText(" ignoring id");
		}
	}

	@Override
	protected boolean matchesSafely(
			org.marvelution.jji.model.Build item,
			Description mismatchDescription)
	{
		if (!ignoreId && !Objects.equals(build.getId(), item.getId()))
		{
			mismatchDescription.appendText("has id ").appendValue(item.getId());
			return false;
		}
		else if (!Objects.equals(build.getJob(), item.getJob()))
		{
			mismatchDescription.appendText("has job ").appendValue(item.getJob());
			return false;
		}
		else if (build.getNumber() != item.getNumber())
		{
			mismatchDescription.appendText("has number ").appendValue(item.getNumber());
			return false;
		}
		else if (!Objects.equals(build.getDisplayName(), item.getDisplayName()))
		{
			mismatchDescription.appendText("has displayName ").appendValue(item.getDisplayName());
			return false;
		}
		else if (!Objects.equals(build.getDescription(), item.getDescription()))
		{
			mismatchDescription.appendText("has description ").appendValue(item.getDescription());
			return false;
		}
		else if (build.isDeleted() != item.isDeleted())
		{
			mismatchDescription.appendText("has deleted ").appendValue(item.isDeleted());
			return false;
		}
		else if (!Objects.equals(build.getCause(), item.getCause()))
		{
			mismatchDescription.appendText("has cause ").appendValue(item.getCause());
			return false;
		}
		else if (!Objects.equals(build.getResult(), item.getResult()))
		{
			mismatchDescription.appendText("has result ").appendValue(item.getResult());
			return false;
		}
		else if (!Objects.equals(build.getBuiltOn(), item.getBuiltOn()))
		{
			mismatchDescription.appendText("has builtOn ").appendValue(item.getBuiltOn());
			return false;
		}
		else if (build.getDuration() != item.getDuration())
		{
			mismatchDescription.appendText("has duration ").appendValue(item.getDuration());
			return false;
		}
		else if (build.getTimestamp() != item.getTimestamp())
		{
			mismatchDescription.appendText("has timestamp ").appendValue(item.getTimestamp());
			return false;
		}
		else if (build.getBranches().size() != item.getBranches().size())
		{
			mismatchDescription.appendText("has branches.size ").appendValue(item.getBranches().size());
			return false;
		}
		else if (!build.getBranches().containsAll(item.getBranches()))
		{
			mismatchDescription.appendText("has branches ").appendValue(item.getBranches());
			return false;
		}
		else if (build.getChangeSet().size() != item.getChangeSet().size())
		{
			mismatchDescription.appendText("has changeSet.size ").appendValue(item.getChangeSet().size());
			return false;
		}
		else if (!build.getChangeSet().containsAll(item.getChangeSet()))
		{
			mismatchDescription.appendText("has changeSet ").appendValue(item.getChangeSet());
			return false;
		}
		else if (!Objects.equals(build.getTestResults(), item.getTestResults()))
		{
			mismatchDescription.appendText("has testResults ").appendValue(item.getTestResults());
			return false;
		}
		else if (build.getDeploymentEnvironments().size() != item.getDeploymentEnvironments().size())
		{
			mismatchDescription.appendText("has deploymentEnvironments.size ").appendValue(item.getDeploymentEnvironments().size());
			return false;
		}
		else if (!build.getDeploymentEnvironments().containsAll(item.getDeploymentEnvironments()))
		{
			mismatchDescription.appendText("has deploymentEnvironments ").appendValue(item.getDeploymentEnvironments());
			return false;
		}
		return true;
	}

	@Factory
	public static Matcher<org.marvelution.jji.model.Build> equalTo(org.marvelution.jji.model.Build build)
	{
		return equalTo(build, false);
	}

	@Factory
	public static Matcher<org.marvelution.jji.model.Build> equalTo(
			org.marvelution.jji.model.Build build,
			boolean ignoreId)
	{
		return new Build(build, ignoreId);
	}
}
