/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.model;

import java.util.UUID;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

class OperationIdTest {

	@Test
	void testOperationId() {
		Site site = new Site().setId(UUID.randomUUID().toString());
		OperationId operationId = site.getOperationId();

		assertThat(operationId.getPrefix(), is(Site.OPERATION_PREFIX));
		assertThat(operationId.getId(), is(Site.OPERATION_PREFIX + "-" + site.getId()));
		assertThat(operationId.getUnprefixedId(), is(site.getId()));
	}
}
