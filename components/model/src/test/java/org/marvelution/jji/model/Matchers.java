/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.model;

import java.net.*;

import org.assertj.core.api.Condition;
import org.hamcrest.*;

/**
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class Matchers
{

	public static DelegatingCondition<Site> equalTo(org.marvelution.jji.model.Site site)
	{
		return new DelegatingCondition<>(org.marvelution.jji.model.matchers.Site.equalTo(site));
	}

	public static DelegatingCondition<Site> equalTo(
			org.marvelution.jji.model.Site site,
			boolean ignoreIds)
	{
		return new DelegatingCondition<>(org.marvelution.jji.model.matchers.Site.equalTo(site, ignoreIds));
	}

	public static DelegatingCondition<Job> equalTo(org.marvelution.jji.model.Job job)
	{
		return new DelegatingCondition<>(org.marvelution.jji.model.matchers.Job.equalTo(job));
	}

	public static DelegatingCondition<Job> equalTo(
			org.marvelution.jji.model.Job job,
			boolean ignoreIds)
	{
		return new DelegatingCondition<>(org.marvelution.jji.model.matchers.Job.equalTo(job, ignoreIds));
	}

	public static DelegatingCondition<Build> equalTo(org.marvelution.jji.model.Build build)
	{
		return new DelegatingCondition<>(org.marvelution.jji.model.matchers.Build.equalTo(build));
	}

	public static DelegatingCondition<Build> equalTo(
			org.marvelution.jji.model.Build build,
			boolean ignoreId)
	{
		return new DelegatingCondition<>(org.marvelution.jji.model.matchers.Build.equalTo(build, ignoreId));
	}

	public static DelegatingCondition<ChangeSet> changeSetEqualTo(
			String commitId,
			String message)
	{
		return new DelegatingCondition<>(org.marvelution.jji.model.matchers.ChangeSet.equalTo(commitId, message));
	}

	public static DelegatingCondition<ChangeSet> equalTo(org.marvelution.jji.model.ChangeSet changeSet)
	{
		return new DelegatingCondition<>(org.marvelution.jji.model.matchers.ChangeSet.equalTo(changeSet));
	}

	public static DelegatingCondition<IssueReference> issueReferenceEqualTo(
			String projectKey,
			String issueKey,
			URI issueUrl)
	{
		return equalTo(new IssueReference().setProjectKey(projectKey).setIssueKey(issueKey).setIssueUrl(issueUrl));
	}

	public static DelegatingCondition<IssueReference> equalTo(org.marvelution.jji.model.IssueReference reference)
	{
		return new DelegatingCondition<>(org.marvelution.jji.model.matchers.IssueReference.equalTo(reference));
	}

	/**
	 * Customer {@link Condition} that also implements the {@link Matcher} interface for easy upgrading form Hamcrest to Assert4J.
	 *
	 * @since 1.20
	 */
	private static class DelegatingCondition<T>
			extends Condition<T>
			implements Matcher<T>
	{

		private final Matcher<T> delegate;

		private DelegatingCondition(Matcher<T> delegate)
		{
			this.delegate = delegate;
			Description d = new StringDescription();
			delegate.describeTo(d);
			as(d.toString());
		}

		@Override
		public boolean matches(Object item)
		{
			return delegate.matches(item);
		}

		@Override
		public void describeMismatch(
				Object item,
				Description mismatchDescription)
		{
			delegate.describeMismatch(item, mismatchDescription);
		}

		@Override
		public void _dont_implement_Matcher___instead_extend_BaseMatcher_()
		{}

		@Override
		public void describeTo(Description description)
		{
			delegate.describeTo(description);
		}
	}
}
