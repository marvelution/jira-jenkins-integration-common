/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.model.request;

import java.util.Collections;
import java.util.stream.Stream;

import org.marvelution.testing.TestSupport;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import static org.assertj.core.api.Assertions.assertThat;

class WorkflowPostFunctionConfigurationTest
        extends TestSupport
{
    @ParameterizedTest
    @MethodSource("forSpecificJob")
    void isForSpecificJob(
            String config,
            boolean expectedResult)
    {
        assertThat(WorkflowPostFunctionConfiguration.isForSpecificJob(config)).isEqualTo(expectedResult);
    }

    @ParameterizedTest
    @MethodSource("byIssueField")
    void isByIssueField(
            String config,
            boolean expectedResult)
    {
        assertThat(WorkflowPostFunctionConfiguration.isByIssueField(config)).isEqualTo(expectedResult);
    }

    @ParameterizedTest
    @MethodSource("parse")
    void testParse(
            String issueKey,
            String configuration,
            WorkflowPostFunctionConfiguration expectedResult)
    {
        WorkflowPostFunctionConfiguration request = WorkflowPostFunctionConfiguration.parse(issueKey, configuration);
        assertThat(request).isEqualTo(expectedResult);
    }

    public static Stream<Arguments> forSpecificJob()
    {
        return Stream.of(Arguments.of(WorkflowPostFunctionConfiguration.LINKED, false),
                Arguments.of(WorkflowPostFunctionConfiguration.BY_CONVENTION, false),
                Arguments.of(WorkflowPostFunctionConfiguration.BY_ISSUE_FIELD + "::components", false),
                Arguments.of(WorkflowPostFunctionConfiguration.BY_ISSUE_FIELD + ";;components", false),
                Arguments.of("my-job", true));
    }

    public static Stream<Arguments> byIssueField()
    {
        return Stream.of(Arguments.of(WorkflowPostFunctionConfiguration.LINKED, false),
                Arguments.of(WorkflowPostFunctionConfiguration.BY_CONVENTION, false),
                Arguments.of(WorkflowPostFunctionConfiguration.BY_ISSUE_FIELD, false),
                Arguments.of(WorkflowPostFunctionConfiguration.BY_ISSUE_FIELD + "::", false),
                Arguments.of(WorkflowPostFunctionConfiguration.BY_ISSUE_FIELD + ";;", false),
                Arguments.of(WorkflowPostFunctionConfiguration.BY_ISSUE_FIELD + "::fixVersions", true),
                Arguments.of(WorkflowPostFunctionConfiguration.BY_ISSUE_FIELD + ";;fixVersions", true),
                Arguments.of("my-job", false));
    }

    public static Stream<Arguments> parse()
    {
        return Stream.of(Arguments.of("TEST-1",
                        WorkflowPostFunctionConfiguration.LINKED,
                        new WorkflowPostFunctionConfiguration().setIssueKey("TEST-1")
                                .setLinked()),
                Arguments.of("TEST-1",
                        WorkflowPostFunctionConfiguration.BY_CONVENTION,
                        new WorkflowPostFunctionConfiguration().setIssueKey("TEST-1")
                                .setByConvention()),
                Arguments.of("TEST-1",
                        WorkflowPostFunctionConfiguration.BY_ISSUE_FIELD + "::labels",
                        new WorkflowPostFunctionConfiguration().setIssueKey("TEST-1")
                                .setByIssueField("labels")),
                Arguments.of("TEST-1",
                        WorkflowPostFunctionConfiguration.BY_ISSUE_FIELD + ";;labels",
                        new WorkflowPostFunctionConfiguration().setIssueKey("TEST-1")
                                .setByIssueField("labels").setExactMatch(true)),
                Arguments.of("TEST-1",
                        "my-job",
                        new WorkflowPostFunctionConfiguration().setIssueKey("TEST-1")
                                .setJobs(Collections.singleton("my-job"))));
    }
}
