/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.model.matchers;

import java.util.Objects;

import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeDiagnosingMatcher;

/**
 * {@link Matcher} for {@link org.marvelution.jji.model.Job jobs}.
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class Job extends TypeSafeDiagnosingMatcher<org.marvelution.jji.model.Job> {

	private final org.marvelution.jji.model.Job job;
	private final boolean ignoreId;

	private Job(org.marvelution.jji.model.Job job, boolean ignoreId) {
		this.job = job;
		this.ignoreId = ignoreId;
	}

	@Override
	public void describeTo(Description description) {
		description.appendText("job same as ").appendValue(job);
		if (ignoreId) {
			description.appendText(" ignoring id");
		}
	}

	@Override
	protected boolean matchesSafely(org.marvelution.jji.model.Job item, Description mismatchDescription) {
		if (!ignoreId && !Objects.equals(job.getId(), item.getId())) {
			mismatchDescription.appendText("has id ").appendValue(item.getId());
			return false;
		} else if (!Objects.equals(job.getName(), item.getName())) {
			mismatchDescription.appendText("has name ").appendValue(job.getName());
			return false;
		} else if (!Objects.equals(job.getParentName(), item.getParentName())) {
			mismatchDescription.appendText("has parentName ").appendValue(item.getParentName());
			return false;
		} else if (!Objects.equals(job.getUrlName(), item.getUrlName())) {
			mismatchDescription.appendText("has urlName ").appendValue(item.getUrlName());
			return false;
		} else if (!Objects.equals(job.getDisplayName(), item.getDisplayName())) {
			mismatchDescription.appendText("has displayName ").appendValue(item.getDisplayName());
			return false;
		} else if (!Objects.equals(job.getDescription(), item.getDescription())) {
			mismatchDescription.appendText("has description ").appendValue(item.getDescription());
			return false;
		} else if (!Objects.equals(job.getSite(), item.getSite())) {
			mismatchDescription.appendText("has site ").appendValue(item.getSite());
			return false;
		} else if (job.getLastBuild() != item.getLastBuild()) {
			mismatchDescription.appendText("has lastBuild ").appendValue(item.getLastBuild());
			return false;
		} else if (job.getOldestBuild() != item.getOldestBuild()) {
			mismatchDescription.appendText("has oldestBuild ").appendValue(item.getOldestBuild());
			return false;
		} else if (job.isLinked() != item.isLinked()) {
			mismatchDescription.appendText("has linked ").appendValue(item.isLinked());
			return false;
		} else if (job.isDeleted() != item.isDeleted()) {
			mismatchDescription.appendText("has deleted ").appendValue(item.isDeleted());
			return false;
		}
		return true;
	}

	@Factory
	public static Matcher<org.marvelution.jji.model.Job> equalTo(org.marvelution.jji.model.Job job) {
		return equalTo(job, false);
	}

	@Factory
	public static Matcher<org.marvelution.jji.model.Job> equalTo(org.marvelution.jji.model.Job job, boolean ignoreId) {
		return new Job(job, ignoreId);
	}

}
