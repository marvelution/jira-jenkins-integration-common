/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.model.matchers;

import java.util.Objects;

import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeDiagnosingMatcher;

/**
 * {@link Matcher} for {@link org.marvelution.jji.model.IssueReference}.
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class IssueReference extends TypeSafeDiagnosingMatcher<org.marvelution.jji.model.IssueReference> {

	private final org.marvelution.jji.model.IssueReference reference;

	private IssueReference(org.marvelution.jji.model.IssueReference reference) {
		this.reference = reference;
	}

	@Override
	public void describeTo(Description description) {
		description.appendText("issueReference same as ").appendValue(reference);
	}

	@Override
	protected boolean matchesSafely(org.marvelution.jji.model.IssueReference item, Description mismatchDescription) {
		if (!Objects.equals(reference.getIssueKey(), item.getIssueKey())) {
			mismatchDescription.appendText("has issueKey ").appendValue(item.getIssueKey());
			return false;
		} else if (!Objects.equals(reference.getProjectKey(), item.getProjectKey())) {
			mismatchDescription.appendText("has projectKey ").appendValue(item.getProjectKey());
			return false;
		} else if (!Objects.equals(reference.getIssueUrl(), item.getIssueUrl())) {
			mismatchDescription.appendText("has issueUrl ").appendValue(item.getIssueUrl());
			return false;
		}
		return true;
	}

	@Factory
	public static Matcher<org.marvelution.jji.model.IssueReference> equalTo(org.marvelution.jji.model.IssueReference reference) {
		return new IssueReference(reference);
	}
}
