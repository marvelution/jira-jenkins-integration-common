/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.utils;

import java.util.Arrays;
import java.util.stream.Stream;

import org.marvelution.jji.model.Build;
import org.marvelution.jji.model.ChangeSet;
import org.marvelution.jji.model.Job;
import org.marvelution.jji.model.Site;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import static java.util.stream.Collectors.toSet;
import static org.assertj.core.api.Assertions.assertThat;

class KeyExtractorTest
{

    @ParameterizedTest
    @ValueSource(strings = {"",
            "message without key",
            "message ABC-A text",
            "message M-123 invalid key",
            "message MES- invalid key",
            "message -123 invalid key",
            "message 1ABC-123 invalid key",
            "message 123-123 invalid key",
            "should not parse MES-123key",
            "MES-123k invalid char",
            "invalid char MES-123k",
            "message abc-a text",
            "message m-123 invalid key",
            "message mes- invalid key",
            "message -123 invalid key",
            "message 1abc-123 invalid key",
            "message 123-123 invalid key",
            "should not parse mes-123key",
            "mes-123k invalid char",
            "invalid char mes-123k"})
    void testExtractIssueKeysMessageWithoutIssueKeys(String message)
    {
        assertThat(KeyExtractor.extractIssueKeys(message)).isEmpty();
    }

    @ParameterizedTest
    @MethodSource({"singleMessageWithSingleIssueKey",
            "singleMessageWithSingleIssueKeyContainingNumbers",
            "singleMessageWithSingleIssueKeyContainingUnderscores"})
    void testExtractIssueKeysSingleMessageWithSingleIssueKey(
            String message,
            String expectedIssueKey)
    {
        assertThat(KeyExtractor.extractIssueKeys(message)).containsOnly(expectedIssueKey);
    }

    @ParameterizedTest
    @MethodSource({"multipleMessagesWithSingleIssueKey",
            "multipleMessagesWithSingleIssueKeyContainingNumbers",
            "multipleMessagesWithSingleIssueKeyContainingUnderscores"})
    void testExtractIssueKeysMultipleMessagesWithSingleIssueKey(
            String[] message,
            String[] expectedIssueKey)
    {
        assertThat(KeyExtractor.extractIssueKeys(message)).containsOnly(expectedIssueKey);
    }

    @ParameterizedTest
    @MethodSource({"singleMessageWithMultipleIssueKeys"})
    void testExtractIssueKeysMultipleMessagesWithMultipleIssueKeys(
            String message,
            String[] expectedIssueKey)
    {
        assertThat(KeyExtractor.extractIssueKeys(message)).containsOnly(expectedIssueKey);
    }

    @ParameterizedTest
    @MethodSource({"multipleMessagesWithMultipleIssueKeys"})
    void testExtractIssueKeysMultipleMessagesWithMultipleIssueKeys(
            String[] message,
            String[] expectedIssueKey)
    {
        assertThat(KeyExtractor.extractIssueKeys(message)).containsOnly(expectedIssueKey);
    }

    @Test
    void testExtractIssueKeysFromJob()
    {
        Job job = new Job().setName("Job for TEST-1")
                .setUrlName("TEST-2")
                .setDescription("Description of the test for TEST-3");
        assertThat(KeyExtractor.extractIssueKeys(job)).containsOnly("TEST-1", "TEST-2", "TEST-3");
    }

    @Test
    void testExtractScopedIssueKeysFromJob()
    {
        Job job = new Job().setName("Job for TEST-1")
                .setUrlName("TEST-2")
                .setDescription("Description of the test for ABC-3")
                .setSite(new Site().setScope("ABC"));
        assertThat(KeyExtractor.extractIssueKeys(job)).containsOnly("ABC-3");
    }

    @Test
    void testExtractIssueKeysFromBuild()
    {
        Job job = new Job().setName("Job for TEST-1")
                .setUrlName("TEST-2")
                .setDescription("Description of the test for TEST-3");
        Build build = new Build().setJob(job)
                .setDescription("Build for TEST-4")
                .setNumber(1)
                .setDisplayName("TEST-5")
                .setCause("Commit for job TEST-6")
                .setBranches(Stream.of("origin/master", "origin/feature/mark/TEST-7-some-tests", "origin/feature/mark/TEST-8")
                        .collect(toSet()))
                .setChangeSet(Arrays.asList(new ChangeSet().setMessage("TEST-9 extra tests"), new ChangeSet().setMessage("TEST-10")));

        assertThat(KeyExtractor.extractIssueKeys(build)).containsOnly("TEST-1",
                "TEST-2",
                "TEST-3",
                "TEST-4",
                "TEST-5",
                "TEST-6",
                "TEST-7",
                "TEST-8",
                "TEST-9",
                "TEST-10");
    }

    @Test
    void testExtractScopedIssueKeysFromBuild()
    {
        Job job = new Job().setName("Job for TEST-1")
                .setUrlName("TEST-2")
                .setDescription("Description of the test for ABC-3")
                .setSite(new Site().setScope("ABC"));
        Build build = new Build().setJob(job)
                .setDescription("Build for TEST-4")
                .setNumber(1)
                .setDisplayName("ABC-5")
                .setCause("Commit for job TEST-6")
                .setBranches(Stream.of("origin/master", "origin/feature/mark/TEST-7-some-tests", "origin/feature/mark/TEST-8")
                        .collect(toSet()))
                .setChangeSet(Arrays.asList(new ChangeSet().setMessage("TEST-9 extra tests"), new ChangeSet().setMessage("ABC-10")));

        assertThat(KeyExtractor.extractIssueKeys(build)).containsOnly("ABC-3", "ABC-5", "ABC-10");
    }

    private static Stream<Arguments> singleMessageWithSingleIssueKey()
    {
        return Stream.of(Arguments.of("ABC-123", "ABC-123"),
                Arguments.of("ABC-123 text", "ABC-123"),
                Arguments.of("message ABC-123", "ABC-123"),
                Arguments.of("message ABC-123 text", "ABC-123"),
                Arguments.of("message\nABC-123\ntext", "ABC-123"),
                Arguments.of("message\rABC-123\rtext", "ABC-123"),
                Arguments.of("message.ABC-123.text", "ABC-123"),
                Arguments.of("message:ABC-123:text", "ABC-123"),
                Arguments.of("message,ABC-123,text", "ABC-123"),
                Arguments.of("message;ABC-123;text", "ABC-123"),
                Arguments.of("message&ABC-123&text", "ABC-123"),
                Arguments.of("message=ABC-123=text", "ABC-123"),
                Arguments.of("message?ABC-123?text", "ABC-123"),
                Arguments.of("message!ABC-123!text", "ABC-123"),
                Arguments.of("message/ABC-123/text", "ABC-123"),
                Arguments.of("message\\ABC-123\\text", "ABC-123"),
                Arguments.of("message~ABC-123~text", "ABC-123"),
                Arguments.of("message_ABC-123_text", "MESSAGE_ABC-123"),
                Arguments.of("ABC-123", "ABC-123"),
                Arguments.of("ABC-123 text", "ABC-123"),
                Arguments.of("message abc-123", "ABC-123"),
                Arguments.of("message abc-123 text", "ABC-123"),
                Arguments.of("message\nabc-123\ntext", "ABC-123"),
                Arguments.of("message\rabc-123\rtext", "ABC-123"),
                Arguments.of("message.abc-123.text", "ABC-123"),
                Arguments.of("message:abc-123:text", "ABC-123"),
                Arguments.of("message,abc-123,text", "ABC-123"),
                Arguments.of("message;abc-123;text", "ABC-123"),
                Arguments.of("message&abc-123&text", "ABC-123"),
                Arguments.of("message=abc-123=text", "ABC-123"),
                Arguments.of("message?abc-123?text", "ABC-123"),
                Arguments.of("message!abc-123!text", "ABC-123"),
                Arguments.of("message/abc-123/text", "ABC-123"),
                Arguments.of("message\\abc-123\\text", "ABC-123"),
                Arguments.of("message~abc-123~text", "ABC-123"),
                Arguments.of("MESSAGE_abc-123_text", "MESSAGE_ABC-123"));
    }

    private static Stream<Arguments> singleMessageWithSingleIssueKeyContainingNumbers()
    {
        return Stream.of(Arguments.of("A1BC-123", "A1BC-123"),
                Arguments.of("AB8C-123 text", "AB8C-123"),
                Arguments.of("message A7BC-123", "A7BC-123"),
                Arguments.of("message AB9C-123 text", "AB9C-123"),
                Arguments.of("message\nA2BC-123\ntext", "A2BC-123"),
                Arguments.of("message\rABC0-123\rtext", "ABC0-123"),
                Arguments.of("message.ABC789-123.text", "ABC789-123"),
                Arguments.of("message:A1BC-123:text", "A1BC-123"),
                Arguments.of("message,A1BC-123,text", "A1BC-123"),
                Arguments.of("message;A1BC-123;text", "A1BC-123"),
                Arguments.of("message&AB1C-123&text", "AB1C-123"),
                Arguments.of("message=A1BC-123=text", "A1BC-123"),
                Arguments.of("message?A1BC-123?text", "A1BC-123"),
                Arguments.of("message!AB1C-123!text", "AB1C-123"),
                Arguments.of("message/AB1C-123/text", "AB1C-123"),
                Arguments.of("message\\A1BC-123\\text", "A1BC-123"),
                Arguments.of("message~A1BC-123~text", "A1BC-123"),
                Arguments.of("message_A1BC-123_text", "MESSAGE_A1BC-123"),
                Arguments.of("a1bc-123", "A1BC-123"),
                Arguments.of("ab8c-123 text", "AB8C-123"),
                Arguments.of("message a7bc-123", "A7BC-123"),
                Arguments.of("message ab9c-123 text", "AB9C-123"),
                Arguments.of("message\na2bc-123\ntext", "A2BC-123"),
                Arguments.of("message\rabc0-123\rtext", "ABC0-123"),
                Arguments.of("message.abc789-123.text", "ABC789-123"),
                Arguments.of("message:a1bc-123:text", "A1BC-123"),
                Arguments.of("message,a1bc-123,text", "A1BC-123"),
                Arguments.of("message;a1bc-123;text", "A1BC-123"),
                Arguments.of("message&ab1c-123&text", "AB1C-123"),
                Arguments.of("message=a1bc-123=text", "A1BC-123"),
                Arguments.of("message?a1bc-123?text", "A1BC-123"),
                Arguments.of("message!ab1c-123!text", "AB1C-123"),
                Arguments.of("message/ab1c-123/text", "AB1C-123"),
                Arguments.of("message\\a1bc-123\\text", "A1BC-123"),
                Arguments.of("message~a1bc-123~text", "A1BC-123"),
                Arguments.of("Message_a1bc-123_text", "MESSAGE_A1BC-123"));
    }

    private static Stream<Arguments> singleMessageWithSingleIssueKeyContainingUnderscores()
    {
        return Stream.of(Arguments.of("A_BC-123", "A_BC-123"),
                Arguments.of("AB_C-123", "AB_C-123"),
                Arguments.of("ABC_-123", "ABC_-123"),
                Arguments.of("A_BC-123 text", "A_BC-123"),
                Arguments.of("message A_BC-123", "A_BC-123"),
                Arguments.of("message A_BC-123 text", "A_BC-123"),
                Arguments.of("message\nA_BC-123\ntext", "A_BC-123"),
                Arguments.of("message\rA_BC-123\rtext", "A_BC-123"),
                Arguments.of("message.A_BC-123.text", "A_BC-123"),
                Arguments.of("message:A_BC-123:text", "A_BC-123"),
                Arguments.of("message,A_BC-123,text", "A_BC-123"),
                Arguments.of("message;A_BC-123;text", "A_BC-123"),
                Arguments.of("message&A_BC-123&text", "A_BC-123"),
                Arguments.of("message=A_BC-123=text", "A_BC-123"),
                Arguments.of("message?A_BC-123?text", "A_BC-123"),
                Arguments.of("message!A_BC-123!text", "A_BC-123"),
                Arguments.of("message/A_BC-123/text", "A_BC-123"),
                Arguments.of("message\\A_BC-123\\text", "A_BC-123"),
                Arguments.of("message~A_BC-123~text", "A_BC-123"),
                Arguments.of("message_A_BC-123_text", "MESSAGE_A_BC-123"),
                Arguments.of("a_bc-123", "A_BC-123"),
                Arguments.of("ab_c-123", "AB_C-123"),
                Arguments.of("message_abc_-123", "MESSAGE_ABC_-123"),
                Arguments.of("a_bc-123 text", "A_BC-123"),
                Arguments.of("message a_bc-123", "A_BC-123"),
                Arguments.of("message a_bc-123 text", "A_BC-123"),
                Arguments.of("message\na_bc-123\ntext", "A_BC-123"),
                Arguments.of("message\ra_bc-123\rtext", "A_BC-123"),
                Arguments.of("message.a_bc-123.text", "A_BC-123"),
                Arguments.of("message:a_bc-123:text", "A_BC-123"),
                Arguments.of("message,a_bc-123,text", "A_BC-123"),
                Arguments.of("message;a_bc-123;text", "A_BC-123"),
                Arguments.of("message&a_bc-123&text", "A_BC-123"),
                Arguments.of("message=a_bc-123=text", "A_BC-123"),
                Arguments.of("message?a_bc-123?text", "A_BC-123"),
                Arguments.of("message!a_bc-123!text", "A_BC-123"),
                Arguments.of("message/a_bc-123/text", "A_BC-123"),
                Arguments.of("message\\a_bc-123\\text", "A_BC-123"),
                Arguments.of("message~a_bc-123~text", "A_BC-123"),
                Arguments.of("Message_a_bc-123_text", "MESSAGE_A_BC-123"));
    }

    private static Stream<Arguments> multipleMessagesWithSingleIssueKey()
    {
        return Stream.of(Arguments.of(new String[]{"ABC-123",
                                "message_ABC-987_text"},
                        new String[]{"ABC-123",
                                "MESSAGE_ABC-987"}),
                Arguments.of(new String[]{"abc-123",
                                "MESSAGE_abc-987_text"},
                        new String[]{"ABC-123",
                                "MESSAGE_ABC-987"}));
    }

    private static Stream<Arguments> multipleMessagesWithSingleIssueKeyContainingNumbers()
    {
        return Stream.of(Arguments.of(new String[]{"A1BC-123",
                                "message_A1BC-987_text"},
                        new String[]{"A1BC-123",
                                "MESSAGE_A1BC-987"}),
                Arguments.of(new String[]{"a1bc-123",
                                "Message_a1bc-987_text"},
                        new String[]{"A1BC-123",
                                "MESSAGE_A1BC-987"}));
    }

    private static Stream<Arguments> multipleMessagesWithSingleIssueKeyContainingUnderscores()
    {
        return Stream.of(Arguments.of(new String[]{"ABC-123",
                                "message_ABC-987"},
                        new String[]{"ABC-123",
                                "MESSAGE_ABC-987"}),
                Arguments.of(new String[]{"ABC-123",
                                "message_ABC-987"},
                        new String[]{"ABC-123",
                                "MESSAGE_ABC-987"}));
    }

    private static Stream<Arguments> singleMessageWithMultipleIssueKeys()
    {
        return Stream.of(Arguments.of("ABC-123 DEF-456",
                        new String[]{"ABC-123",
                                "DEF-456"}),
                Arguments.of("message ABC-123 DEF-456 text",
                        new String[]{"ABC-123",
                                "DEF-456"}),
                Arguments.of("message\nABC-123\nDEF-456\ntext",
                        new String[]{"ABC-123",
                                "DEF-456"}),
                Arguments.of("message\rABC-123\rDEF-456\rtext",
                        new String[]{"ABC-123",
                                "DEF-456"}),
                Arguments.of("message.ABC-123.DEF-456.text",
                        new String[]{"ABC-123",
                                "DEF-456"}),
                Arguments.of("message:ABC-123:DEF-456:text",
                        new String[]{"ABC-123",
                                "DEF-456"}),
                Arguments.of("message,ABC-123,DEF-456,text",
                        new String[]{"ABC-123",
                                "DEF-456"}),
                Arguments.of("message;ABC-123;DEF-456;text",
                        new String[]{"ABC-123",
                                "DEF-456"}),
                Arguments.of("message&ABC-123&DEF-456&text",
                        new String[]{"ABC-123",
                                "DEF-456"}),
                Arguments.of("message=ABC-123=DEF-456=text",
                        new String[]{"ABC-123",
                                "DEF-456"}),
                Arguments.of("message?ABC-123?DEF-456?text",
                        new String[]{"ABC-123",
                                "DEF-456"}),
                Arguments.of("message!ABC-123!DEF-456!text",
                        new String[]{"ABC-123",
                                "DEF-456"}),
                Arguments.of("message/ABC-123/DEF-456/text",
                        new String[]{"ABC-123",
                                "DEF-456"}),
                Arguments.of("message\\ABC-123\\DEF-456\\text",
                        new String[]{"ABC-123",
                                "DEF-456"}),
                Arguments.of("message~ABC-123~DEF-456~text",
                        new String[]{"ABC-123",
                                "DEF-456"}),
                Arguments.of("message_ABC-123_DEF-456_text",
                        new String[]{"MESSAGE_ABC-123",
                                "DEF-456"}),
                Arguments.of("abc-123 def-456",
                        new String[]{"ABC-123",
                                "DEF-456"}),
                Arguments.of("message abc-123 def-456 text",
                        new String[]{"ABC-123",
                                "DEF-456"}),
                Arguments.of("message\nabc-123\ndef-456\ntext",
                        new String[]{"ABC-123",
                                "DEF-456"}),
                Arguments.of("message\rabc-123\rdef-456\rtext",
                        new String[]{"ABC-123",
                                "DEF-456"}),
                Arguments.of("message.abc-123.def-456.text",
                        new String[]{"ABC-123",
                                "DEF-456"}),
                Arguments.of("message:abc-123:def-456:text",
                        new String[]{"ABC-123",
                                "DEF-456"}),
                Arguments.of("message,abc-123,def-456,text",
                        new String[]{"ABC-123",
                                "DEF-456"}),
                Arguments.of("message;abc-123;def-456;text",
                        new String[]{"ABC-123",
                                "DEF-456"}),
                Arguments.of("message&abc-123&def-456&text",
                        new String[]{"ABC-123",
                                "DEF-456"}),
                Arguments.of("message=abc-123=def-456=text",
                        new String[]{"ABC-123",
                                "DEF-456"}),
                Arguments.of("message?abc-123?def-456?text",
                        new String[]{"ABC-123",
                                "DEF-456"}),
                Arguments.of("message!abc-123!def-456!text",
                        new String[]{"ABC-123",
                                "DEF-456"}),
                Arguments.of("message/abc-123/def-456/text",
                        new String[]{"ABC-123",
                                "DEF-456"}),
                Arguments.of("message\\abc-123\\def-456\\text",
                        new String[]{"ABC-123",
                                "DEF-456"}),
                Arguments.of("message~abc-123~def-456~text",
                        new String[]{"ABC-123",
                                "DEF-456"}),
                Arguments.of("Message_abc-123_def-456_text",
                        new String[]{"MESSAGE_ABC-123",
                                "DEF-456"}));
    }

    private static Stream<Arguments> multipleMessagesWithMultipleIssueKeys()
    {
        return Stream.of(Arguments.of(new String[]{"ABC-123 DEF-456",
                                "message_ABC-987_DEF-654_text"},
                        new String[]{"ABC-123",
                                "DEF-456",
                                "MESSAGE_ABC-987",
                                "DEF-654"}),
                Arguments.of(new String[]{"abc-123 def-456",
                                "Message_abc-987_def-654_text"},
                        new String[]{"ABC-123",
                                "DEF-456",
                                "MESSAGE_ABC-987",
                                "DEF-654"}));
    }
}
