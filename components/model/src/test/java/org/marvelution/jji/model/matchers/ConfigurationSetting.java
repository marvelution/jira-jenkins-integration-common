/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.model.matchers;

import java.util.Objects;

import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeDiagnosingMatcher;

/**
 * {@link Matcher} for {@link org.marvelution.jji.model.ConfigurationSetting}.
 *
 * @author Mark Rekveld
 * @since 1.3.0
 */
public class ConfigurationSetting extends TypeSafeDiagnosingMatcher<org.marvelution.jji.model.ConfigurationSetting> {

	private final org.marvelution.jji.model.ConfigurationSetting configurationSetting;

	private ConfigurationSetting(org.marvelution.jji.model.ConfigurationSetting configurationSetting) {
		this.configurationSetting = configurationSetting;
	}

	@Factory
	public static Matcher<org.marvelution.jji.model.ConfigurationSetting> equalTo(
			org.marvelution.jji.model.ConfigurationSetting configurationSetting) {
		return new ConfigurationSetting(configurationSetting);
	}

	@Factory
	public static Matcher<org.marvelution.jji.model.ConfigurationSetting> equalTo(String key, String value) {
		return new ConfigurationSetting(org.marvelution.jji.model.ConfigurationSetting.forKey(key).setValue(value));
	}

	@Factory
	public static Matcher<org.marvelution.jji.model.ConfigurationSetting> equalTo(String key, Integer value) {
		return new ConfigurationSetting(org.marvelution.jji.model.ConfigurationSetting.forKey(key).setValue(value));
	}

	@Factory
	public static Matcher<org.marvelution.jji.model.ConfigurationSetting> equalTo(String key, Boolean value) {
		return new ConfigurationSetting(org.marvelution.jji.model.ConfigurationSetting.forKey(key).setValue(value));
	}

	@Factory
	public static Matcher<org.marvelution.jji.model.ConfigurationSetting> equalTo(String key, String value, boolean overridable) {
		return new ConfigurationSetting(org.marvelution.jji.model.ConfigurationSetting.forKey(key)
		                                                                              .setValue(value)
		                                                                              .setOverridable(overridable));
	}

	@Override
	protected boolean matchesSafely(org.marvelution.jji.model.ConfigurationSetting item, Description mismatchDescription) {
		if (!Objects.equals(configurationSetting.getKey(), item.getKey())) {
			mismatchDescription.appendText(" has key ").appendValue(item.getKey());
			return false;
		} else if (!Objects.equals(configurationSetting.getValue(), item.getValue())) {
			mismatchDescription.appendText(" has value ").appendValue(item.getValue());
			return false;
		} else if (configurationSetting.isOverridable() != item.isOverridable()) {
			mismatchDescription.appendText(" has overridable ").appendValue(item.isOverridable());
			return false;
		}
		return true;
	}

	@Override
	public void describeTo(Description description) {
		description.appendText("configuration setting with key ").appendValue(configurationSetting.getKey())
		           .appendText(" and value ").appendValue(configurationSetting.getValue())
		           .appendValue(" and overridable ").appendValue(configurationSetting.isOverridable());
	}
}
