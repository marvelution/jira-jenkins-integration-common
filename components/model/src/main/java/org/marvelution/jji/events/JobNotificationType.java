/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.events;

import java.util.Locale;
import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlType;

@XmlEnum
@XmlType
@javax.xml.bind.annotation.XmlEnum
@javax.xml.bind.annotation.XmlType
public enum JobNotificationType
{
    JOB_CREATED,
    JOB_MODIFIED,
    JOB_MOVED,
    JOB_SYNC;

    public String value()
    {
        return name().toLowerCase(Locale.ENGLISH);
    }

    @Override
    public String toString()
    {
        return value();
    }
}
