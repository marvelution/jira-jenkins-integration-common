/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.model;

import java.util.Objects;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.builder.ToStringBuilder;

import static org.apache.commons.lang3.builder.ToStringStyle.SHORT_PREFIX_STYLE;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@javax.xml.bind.annotation.XmlRootElement
@javax.xml.bind.annotation.XmlAccessorType(javax.xml.bind.annotation.XmlAccessType.FIELD)
public class TestResults
{

    /**
     * Only added to let this field be serialized but also support it not being deserialized.
     */
    private int passed;
    private int failed = 0;
    private int skipped = 0;
    private int total = 0;

    @XmlElement
    @javax.xml.bind.annotation.XmlElement
    public int getPassed()
    {
        return total - failed - skipped;
    }

    public int getFailed()
    {
        return failed;
    }

    public TestResults setFailed(int failed)
    {
        this.failed = failed;
        return this;
    }

    public int getSkipped()
    {
        return skipped;
    }

    public TestResults setSkipped(int skipped)
    {
        this.skipped = skipped;
        return this;
    }

    public int getTotal()
    {
        return total;
    }

    public TestResults setTotal(int total)
    {
        this.total = total;
        return this;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }
        TestResults that = (TestResults) o;
        return failed == that.failed && skipped == that.skipped && total == that.total;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(failed, skipped, total);
    }

    @Override
    public String toString()
    {
        return new ToStringBuilder(this, SHORT_PREFIX_STYLE).append("failed", failed)
                .append("skipped", skipped)
                .append("total", total)
                .toString();
    }
}
