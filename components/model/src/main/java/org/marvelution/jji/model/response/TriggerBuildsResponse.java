/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.model.response;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;
import java.util.stream.Collectors;
import javax.annotation.Nullable;
import jakarta.xml.bind.annotation.*;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@javax.xml.bind.annotation.XmlRootElement
@javax.xml.bind.annotation.XmlAccessorType(javax.xml.bind.annotation.XmlAccessType.FIELD)
public class TriggerBuildsResponse
{

    private List<TriggerResult> triggers;

    public List<TriggerResult> getTriggers()
    {
        if (triggers == null)
        {
            triggers = new ArrayList<>();
        }
        return triggers;
    }

    public TriggerBuildsResponse setTriggers(List<TriggerResult> triggerResults)
    {
        this.triggers = triggerResults;
        return this;
    }

    public boolean hasTriggers(TriggerResultType type)
    {
        return getTriggers().stream()
                .anyMatch(triggerResult -> triggerResult.type == type);
    }

    public List<TriggerResult> getTriggers(TriggerResultType type)
    {
        return getTriggers().stream()
                .filter(triggerResult -> triggerResult.type == type)
                .collect(Collectors.toList());
    }

    public boolean hasFailures()
    {
        return hasTriggers(TriggerResultType.ERROR);
    }

    public List<TriggerResult> getFailures()
    {
        return getTriggers(TriggerResultType.ERROR);
    }

    public TriggerBuildsResponse addFailure(
            String message,
            @Nullable
            URI link)
    {
        getTriggers().add(new TriggerResult().setType(TriggerResultType.ERROR)
                .setMessage(message)
                .setLink(link));
        return this;
    }

    public boolean hasWarnings()
    {
        return hasTriggers(TriggerResultType.WARNING);
    }

    public List<TriggerResult> getWarnings()
    {
        return getTriggers(TriggerResultType.WARNING);
    }

    public TriggerBuildsResponse addWarning(
            String message,
            @Nullable
            URI link)
    {
        getTriggers().add(new TriggerResult().setType(TriggerResultType.WARNING)
                .setMessage(message)
                .setLink(link));
        return this;
    }

    public boolean hasSuccesses()
    {
        return hasTriggers(TriggerResultType.SUCCESS);
    }

    public List<TriggerResult> getSuccesses()
    {
        return getTriggers(TriggerResultType.SUCCESS);
    }

    public TriggerBuildsResponse addSuccess(
            String message,
            @Nullable
            URI link)
    {
        getTriggers().add(new TriggerResult().setType(TriggerResultType.SUCCESS)
                .setMessage(message)
                .setLink(link));
        return this;
    }

    @XmlEnum
    @XmlType
    @javax.xml.bind.annotation.XmlEnum
    @javax.xml.bind.annotation.XmlType
    public enum TriggerResultType
    {
        SUCCESS,
        WARNING,
        ERROR
    }

    @XmlRootElement
    @XmlAccessorType(XmlAccessType.FIELD)
    @javax.xml.bind.annotation.XmlRootElement
    @javax.xml.bind.annotation.XmlAccessorType(javax.xml.bind.annotation.XmlAccessType.FIELD)
    public static class TriggerResult
    {

        private TriggerResultType type;
        private String message;
        private URI link;

        public TriggerResultType getType()
        {
            return type;
        }

        public TriggerResult setType(TriggerResultType type)
        {
            this.type = type;
            return this;
        }

        public String getMessage()
        {
            return message;
        }

        public TriggerResult setMessage(String message)
        {
            this.message = message;
            return this;
        }

        public URI getLink()
        {
            return link;
        }

        public TriggerResult setLink(URI link)
        {
            this.link = link;
            return this;
        }

        @Override
        public int hashCode()
        {
            return Objects.hash(type, message, link);
        }

        @Override
        public boolean equals(Object o)
        {
            if (this == o)
            {
                return true;
            }
            if (o == null || getClass() != o.getClass())
            {
                return false;
            }
            TriggerResult triggerResult = (TriggerResult) o;
            return type == triggerResult.type && Objects.equals(message, triggerResult.message) && Objects.equals(link, triggerResult.link);
        }

        @Override
        public String toString()
        {
            return new StringJoiner(", ", TriggerResult.class.getSimpleName() + "[", "]").add("type=" + type)
                    .add("message='" + message + "'")
                    .add("link=" + link)
                    .toString();
        }
    }
}
