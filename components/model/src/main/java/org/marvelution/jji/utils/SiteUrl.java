/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.utils;

import java.net.URI;
import java.util.Objects;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.function.Function;
import java.util.function.Supplier;
import javax.annotation.Nonnull;

import org.marvelution.jji.model.Build;
import org.marvelution.jji.model.Job;
import org.marvelution.jji.model.Site;
import org.marvelution.jji.model.SiteAuthentication;

import org.springframework.web.util.UriUtils;

public final class SiteUrl
{

    private static final String API_JSON = "api/json/";
    private static final String SLASH = "/";
    private static final String QUESTIONMARK = "?";
    private static final String HASHTAG = "#";
    private static final String UTF_8 = "UTF-8";
    private final Function<Site, URI> baseUrlExtractor;
    private Site site = null;
    private SiteAuthentication authentication = null;
    private Job job = null;
    private Build build = null;
    private String path = null;
    private String query = null;
    private String fragment = null;
    private boolean nullable;

    public SiteUrl(Function<Site, URI> baseUrlExtractor)
    {
        this.baseUrlExtractor = baseUrlExtractor;
    }

    /**
     * Sets the state to nullable, meaning that {@link #url()} will return {@literal null} instead of throwing an exception in case of
     * missing elements.
     */
    public SiteUrl nullable()
    {
        this.nullable = true;
        return this;
    }

    /**
     * Sets the {@link Site} to use for URL generation.
     */
    public SiteUrl site(Site site)
    {
        this.site = site;
        return this;
    }

    public Site site()
    {
        check();
        return site;
    }

    public SiteUrl auth(SiteAuthentication authentication)
    {
        this.authentication = authentication;
        return this;
    }

    public SiteAuthentication auth()
    {
        return Optional.ofNullable(authentication)
                .orElseGet(site()::getAuthentication);
    }

    /**
     * Sets the {@link Job} to use for URL generation.
     */
    public SiteUrl job(Job job)
    {
        this.job = job;
        return this;
    }

    public Optional<Job> job()
    {
        check();
        return Optional.ofNullable(job);
    }

    /**
     * Sets the {@link Build} to use for URL generation.
     */
    public SiteUrl build(Build build)
    {
        this.build = build;
        return this;
    }

    public Optional<Build> build()
    {
        check();
        return Optional.ofNullable(build);
    }

    /**
     * Sets the {@link String path} to add to the generated URL.
     */
    public SiteUrl path(String path)
    {
        this.path = stripStart(path, SLASH);
        return this;
    }

    public Optional<String> path()
    {
        return Optional.ofNullable(path);
    }

    /**
     * Adds the {@link String query} to add to the generated URL.
     *
     * @since 1.8.0
     */
    public SiteUrl query(String query)
    {
        if (this.query != null)
        {
            this.query += "&" + query;
        }
        else
        {
            this.query = stripStart(query, QUESTIONMARK);
        }
        return this;
    }

    /**
     * Adds the query {@link String key} {@link String value} pair to add to the generated URL.
     *
     * @since 1.8.0
     */
    public SiteUrl query(
            String key,
            String value)
    {
        return query(key + "=" + value);
    }

    public Optional<String> query()
    {
        return Optional.ofNullable(query);
    }

    /**
     * Sets the {@link String fragment} to add to the generated URL.
     *
     * @since 1.8.0
     */
    public SiteUrl fragment(String fragment)
    {
        this.fragment = stripStart(fragment, HASHTAG);
        return this;
    }

    public Optional<String> fragment()
    {
        return Optional.ofNullable(fragment);
    }

    /**
     * Sets the generator to generate JSON API URls.
     */
    public SiteUrl api()
    {
        if (path != null)
        {
            path = stripEnd(path, SLASH) + SLASH + API_JSON;
        }
        else
        {
            path = API_JSON;
        }
        return this;
    }

    /**
     * Returns the generated {@link URI} for this {@link SiteUrl}.
     * Can be {@literal null} in case {@link #nullable} was set using {@link #nullable()}.
     */
    public URI url()
    {
        try
        {
            return generate();
        }
        catch (IllegalStateException e)
        {
            if (nullable)
            {
                return null;
            }
            else
            {
                throw e;
            }
        }
    }

    @Nonnull
    private URI generate()
    {
        check();
        String baseUrl = baseUrlExtractor.apply(site)
                .toASCIIString();
        if (!baseUrl.endsWith(SLASH))
        {
            baseUrl += SLASH;
        }
        StringBuilder urlBuilder = new StringBuilder(baseUrl);
        if (job != null)
        {
            String urlName = job.getUrlNameOrNull();
            if (urlName == null || urlName.isBlank())
            {
                urlName = UriUtils.encodePath(job.getName(), UTF_8);
            }
            urlBuilder.append("job")
                    .append(SLASH)
                    .append(urlName)
                    .append(SLASH);
        }
        if (build != null)
        {
            urlBuilder.append(build.getNumber())
                    .append(SLASH);
        }
        if (path != null)
        {
            urlBuilder.append(UriUtils.encodePath(path, UTF_8));
        }
        if (query != null)
        {
            urlBuilder.append(QUESTIONMARK)
                    .append(UriUtils.encodeQuery(query, UTF_8));
        }
        if (fragment != null)
        {
            urlBuilder.append(HASHTAG)
                    .append(UriUtils.encodeFragment(fragment, UTF_8));
        }
        return URI.create(urlBuilder.toString());
    }

    private void check()
    {
        Optional.ofNullable(build)
                .ifPresent(build -> {
                    if (check(job, build::getJob))
                    {
                        job = build.getJob();
                    }
                    else
                    {
                        throw new IllegalStateException("missing or invalid job");
                    }
                });
        Optional.ofNullable(job)
                .ifPresent(job -> {
                    if (check(site, job::getSite))
                    {
                        site = job.getSite();
                    }
                    else
                    {
                        throw new IllegalStateException("missing or invalid site");
                    }
                });
        Optional.ofNullable(site)
                .orElseThrow(() -> new IllegalStateException("missing site"));
    }

    private <T> boolean check(
            T checked,
            Supplier<T> checker)
    {
        T t = checker.get();
        return !(checked == null && t == null) && !(checked != null && !Objects.equals(checked, t));
    }

    @Override
    public String toString()
    {
        return new StringJoiner(", ", SiteUrl.class.getSimpleName() + "[", "]").add("build=" + build)
                .add("job=" + job)
                .add("site=" + site)
                .add("path='" + path + "'")
                .add("query='" + query + "'")
                .add("fragment='" + fragment + "'")
                .toString();
    }

    private static String stripStart(
            String string,
            String start)
    {
        if (string.startsWith(start))
        {
            return string.substring(start.length());
        }
        else
        {
            return string;
        }
    }

    private static String stripEnd(
            String string,
            String end)
    {
        if (string.endsWith(end))
        {
            return string.substring(0, string.length() - end.length());
        }
        else
        {
            return string;
        }
    }

    /**
     * Returns an {@link SiteUrl} for data processing to/from Jenkins.
     *
     * @since 1.8.0
     */
    public static SiteUrl syncUrl()
    {
        return new SiteUrl(Site::getRpcUrl);
    }

    /**
     * Returns an {@link SiteUrl} for displaying the URL.
     *
     * @since 1.8.0
     */
    public static SiteUrl displayUrl()
    {
        return new SiteUrl(Site::getDisplayUrl);
    }
}
