/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji;

public interface Headers
{
    String TENANT_ID = "X-Tenant-Id";
    String REQUEST_ID = "X-Request-Id";
    String REQUEST_URI = "X-Request-Uri";
    String REALM = "X-Realm";
    String SYNC_TOKEN = "X-Sync-Token";
    String NOTIFICATION_TYPE = "X-Notification-type";
    String SYNC_RESULT_HEADER = "X-Sync-Result-Id";
    String DIALOG_MESSAGE_HEADER = "X-Dialog-Message";
}
