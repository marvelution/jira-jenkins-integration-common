/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.model;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import jakarta.annotation.Nullable;
import jakarta.xml.bind.annotation.*;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.marvelution.jji.adapters.javax.ObfuscateAdapter;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.apache.commons.lang3.builder.ToStringBuilder;

import static java.util.Optional.ofNullable;
import static org.apache.commons.lang3.StringUtils.*;
import static org.apache.commons.lang3.builder.ToStringStyle.SHORT_PREFIX_STYLE;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@javax.xml.bind.annotation.XmlRootElement
@javax.xml.bind.annotation.XmlAccessorType(javax.xml.bind.annotation.XmlAccessType.FIELD)
@JsonIdentityInfo(property = "id",
                  generator = ObjectIdGenerators.None.class)
public class Site
        implements HasId<Site>, JobsContainer, Syncable<Site>
{

    public static final String OPERATION_PREFIX = "site-sync";
    private String id;
    private String name;
    @XmlJavaTypeAdapter(org.marvelution.jji.adapters.ObfuscateAdapter.class)
    @javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter(ObfuscateAdapter.class)
    private String sharedSecret;
    private SiteType type;
    private URI rpcUrl;
    @XmlTransient
    @javax.xml.bind.annotation.XmlTransient
    private URI displayUrl;
    @XmlElement(name = "authentication")
    @javax.xml.bind.annotation.XmlElement(name = "authentication")
    private SiteAuthenticationType authenticationType;
    @Nullable
    private String user;
    @Nullable
    @XmlJavaTypeAdapter(org.marvelution.jji.adapters.ObfuscateAdapter.class)
    @javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter(ObfuscateAdapter.class)
    private String token;
    @Nullable
    @XmlJavaTypeAdapter(org.marvelution.jji.adapters.ObfuscateAdapter.class)
    @javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter(ObfuscateAdapter.class)
    private String newToken;
    private boolean useCrumbs;
    private boolean jenkinsPluginInstalled;
    private boolean registrationComplete;
    private boolean autoLinkNewJobs;
    private boolean hasJobs;
    private boolean hasDeletedJobs;
    @XmlElement(name = "connection")
    @javax.xml.bind.annotation.XmlElement(name = "connection")
    private SiteConnectionType connectionType = SiteConnectionType.ACCESSIBLE;
    private boolean enabled = true;
    private String scope;
    @Nullable
    private List<Job> jobs;

    @Override
    public Site copy()
    {
        Site site = new Site();
        site.id = id;
        site.name = name;
        site.sharedSecret = sharedSecret;
        site.type = type;
        site.rpcUrl = checkTrailingSlash(rpcUrl);
        site.displayUrl = checkTrailingSlash(displayUrl);
        site.authenticationType = authenticationType;
        site.user = user;
        site.token = token;
        site.newToken = newToken;
        site.useCrumbs = useCrumbs;
        site.jenkinsPluginInstalled = jenkinsPluginInstalled;
        site.registrationComplete = registrationComplete;
        site.autoLinkNewJobs = autoLinkNewJobs;
        site.hasJobs = hasJobs;
        site.hasDeletedJobs = hasDeletedJobs;
        site.connectionType = connectionType;
        site.enabled = enabled;
        site.scope = scope;
        return site;
    }

    @Override
    public String getId()
    {
        return id;
    }

    @Override
    public Site setId(String id)
    {
        this.id = id;
        return this;
    }

    public String getName()
    {
        return name;
    }

    public Site setName(String name)
    {
        this.name = name;
        return this;
    }

    @Override
    @XmlTransient
    @javax.xml.bind.annotation.XmlTransient
    public String getDisplayName()
    {
        return getName();
    }

    @Override
    @XmlTransient
    @javax.xml.bind.annotation.XmlTransient
    public OperationId getOperationId()
    {
        return OperationId.of(OPERATION_PREFIX, id);
    }

    @Override
    public boolean isEnabled()
    {
        return enabled;
    }

    public Site setEnabled(boolean enabled)
    {
        this.enabled = enabled;
        return this;
    }

    public String getSharedSecret()
    {
        return sharedSecret;
    }

    public Site setSharedSecret(String sharedSecret)
    {
        this.sharedSecret = sharedSecret;
        return this;
    }

    public SiteType getType()
    {
        return type;
    }

    public Site setType(SiteType type)
    {
        this.type = type;
        return this;
    }

    public SiteAuthentication getAuthentication()
    {
        return usesBasicAuthentication() ? new SiteAuthentication.BasicSiteAuthentication(user,
                token) : new SiteAuthentication.SyncTokenSiteAuthentication();
    }

    public SiteAuthenticationType getAuthenticationType()
    {
        if (authenticationType == null)
        {
            authenticationType = isNotBlank(user) && isNotBlank(token) ? SiteAuthenticationType.BASIC : SiteAuthenticationType.TOKEN;
        }
        return authenticationType;
    }

    public Site setAuthenticationType(SiteAuthenticationType authenticationType)
    {
        this.authenticationType = authenticationType;
        return this;
    }

    @Nullable
    public String getUser()
    {
        return user;
    }

    public Site setUser(
            @Nullable
            String user)
    {
        this.user = defaultIfBlank(user, null);
        return this;
    }

    @Nullable
    public String getToken()
    {
        return token;
    }

    public Site setToken(
            @Nullable
            String token)
    {
        this.token = defaultIfBlank(token, null);
        return this;
    }

    public boolean usesBasicAuthentication()
    {
        return getAuthenticationType() == SiteAuthenticationType.BASIC;
    }

    public boolean usesTokenAuthentication()
    {
        return getAuthenticationType() == SiteAuthenticationType.TOKEN;
    }

    public boolean usesAuthentication(SiteAuthenticationType authentication)
    {
        return getAuthenticationType() == authentication;
    }

    public String getNewToken()
    {
        return newToken;
    }

    public Site setNewToken(String newToken)
    {
        this.newToken = newToken;
        return this;
    }

    public URI getRpcUrl()
    {
        return rpcUrl;
    }

    public Site setRpcUrl(URI rpcUrl)
    {
        this.rpcUrl = checkTrailingSlash(rpcUrl);
        return this;
    }

    @XmlElement
    @javax.xml.bind.annotation.XmlElement
    public URI getDisplayUrl()
    {
        return displayUrl != null ? displayUrl : rpcUrl;
    }

    public Site setDisplayUrl(
            @Nullable
            URI displayUrl)
    {
        this.displayUrl = ofNullable(displayUrl).map(Site::checkTrailingSlash)
                .filter(url -> !url.equals(rpcUrl))
                .orElse(null);
        return this;
    }

    @Nullable
    public URI getDisplayUrlOrNull()
    {
        return displayUrl;
    }

    public boolean isUseCrumbs()
    {
        return useCrumbs;
    }

    public Site setUseCrumbs(boolean useCrumbs)
    {
        this.useCrumbs = useCrumbs;
        return this;
    }

    public boolean isJenkinsPluginInstalled()
    {
        return jenkinsPluginInstalled;
    }

    public Site setJenkinsPluginInstalled(boolean jenkinsPluginInstalled)
    {
        this.jenkinsPluginInstalled = jenkinsPluginInstalled;
        return this;
    }

    public boolean isRegistrationComplete()
    {
        return registrationComplete;
    }

    public Site setRegistrationComplete(boolean registrationComplete)
    {
        this.registrationComplete = registrationComplete;
        return this;
    }

    public boolean isAutoLinkNewJobs()
    {
        return autoLinkNewJobs;
    }

    public Site setAutoLinkNewJobs(boolean autoLinkNewJobs)
    {
        this.autoLinkNewJobs = autoLinkNewJobs;
        return this;
    }

    public Site setHasJobs(boolean hasJobs)
    {
        this.hasJobs = hasJobs;
        return this;
    }

    public boolean hasDeletedJobs()
    {
        return getJobs().stream()
                       .anyMatch(Job::isDeleted) || hasDeletedJobs;
    }

    public Site setHasDeletedJobs(boolean hasDeletedJobs)
    {
        this.hasDeletedJobs = hasDeletedJobs;
        return this;
    }

    public SiteConnectionType getConnectionType()
    {
        return connectionType;
    }

    public Site setConnectionType(SiteConnectionType connectionType)
    {
        this.connectionType = connectionType;
        return this;
    }

    public boolean usesConnectionType(SiteConnectionType type)
    {
        return connectionType == type;
    }

    public boolean isAccessibleSite()
    {
        return usesConnectionType(SiteConnectionType.ACCESSIBLE);
    }

    public Site setAccessibleSite()
    {
        setConnectionType(SiteConnectionType.ACCESSIBLE);
        return this;
    }

    public boolean isInaccessibleSite()
    {
        return usesConnectionType(SiteConnectionType.INACCESSIBLE);
    }

    public Site setInaccessibleSite()
    {
        setConnectionType(SiteConnectionType.INACCESSIBLE);
        return this;
    }

    public boolean isTunneledSite()
    {
        return usesConnectionType(SiteConnectionType.TUNNELED);
    }

    public Site setTunneledSite()
    {
        setConnectionType(SiteConnectionType.TUNNELED);
        return this;
    }

    @Override
    @XmlTransient
    @javax.xml.bind.annotation.XmlTransient
    public Site getSite()
    {
        return this;
    }

    @Override
    public List<Job> getJobs()
    {
        if (jobs == null)
        {
            jobs = new ArrayList<>();
        }
        return jobs;
    }

    public Site setJobs(List<Job> jobs)
    {
        this.jobs = jobs;
        return this;
    }

    public boolean hasJobs()
    {
        return (jobs != null && !jobs.isEmpty()) || hasJobs;
    }

    @Override
    public void addJob(Job job)
    {
        getJobs().add(job);
    }

    @Override
    public boolean shouldJobBeLinked(Job job)
    {
        return job.getId() == null && isAutoLinkNewJobs();
    }

    public String getScope()
    {
        return scope != null ? scope : "";
    }

    public Site setScope(String scope)
    {
        this.scope = scope;
        return this;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(id);
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }
        Site site = (Site) o;
        return Objects.equals(id, site.id);
    }

    @Override
    public String toString()
    {
        return new ToStringBuilder(this, SHORT_PREFIX_STYLE).append("id", id)
                .append("scope", scope)
                .append("name", name)
                .append("enabled", enabled)
                .append("rpcUrl", rpcUrl)
                .append("useCrumbs", useCrumbs)
                .append("jenkinsPluginInstalled", jenkinsPluginInstalled)
                .append("autoLinkNewJobs", autoLinkNewJobs)
                .toString();
    }

    @Nullable
    public static URI checkTrailingSlash(
            @Nullable
            URI uri)
    {
        if (uri != null)
        {
            return URI.create(stripEnd(uri.toASCIIString(), "/") + "/");
        }
        else
        {
            return null;
        }
    }

}
