/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.model;

import java.util.Objects;
import jakarta.annotation.Nullable;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.builder.ToStringBuilder;

import static java.util.Optional.ofNullable;
import static org.apache.commons.lang3.builder.ToStringStyle.SHORT_PREFIX_STYLE;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@javax.xml.bind.annotation.XmlRootElement
@javax.xml.bind.annotation.XmlAccessorType(javax.xml.bind.annotation.XmlAccessType.FIELD)
public class ChangeSet
{

    public static final String UNKNOWN_COMMIT_ID = "Unknown";
    @Nullable
    private String commitId;
    private String message;

    @Nullable
    public String getCommitId()
    {
        return commitId;
    }

    public ChangeSet setCommitId(
            @Nullable
            String commitId)
    {
        this.commitId = ofNullable(commitId).orElse(UNKNOWN_COMMIT_ID);
        return this;
    }

    public boolean isKnown()
    {
        return !commitId.equals(UNKNOWN_COMMIT_ID);
    }

    public String getMessage()
    {
        return message;
    }

    public ChangeSet setMessage(String message)
    {
        this.message = message;
        return this;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(commitId, message);
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }
        ChangeSet changeSet = (ChangeSet) o;
        return Objects.equals(commitId, changeSet.commitId) && Objects.equals(message, changeSet.message);
    }

    @Override
    public String toString()
    {
        return new ToStringBuilder(this, SHORT_PREFIX_STYLE).append("commitId", commitId)
                .append("message", message)
                .toString();
    }
}
