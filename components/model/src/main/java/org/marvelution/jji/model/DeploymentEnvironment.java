/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.model;

import java.util.Objects;
import java.util.StringJoiner;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@javax.xml.bind.annotation.XmlRootElement
@javax.xml.bind.annotation.XmlAccessorType(javax.xml.bind.annotation.XmlAccessType.FIELD)
public class DeploymentEnvironment
        implements HasId<DeploymentEnvironment>
{

    private String id;
    private String name;
    private DeploymentEnvironmentType type;

    @Override
    public DeploymentEnvironment copy()
    {
        DeploymentEnvironment environment = new DeploymentEnvironment();
        environment.id = id;
        environment.name = name;
        environment.type = type;
        return environment;
    }

    @Override
    public String getId()
    {
        return id;
    }

    @Override
    public DeploymentEnvironment setId(String id)
    {
        this.id = id;
        return this;
    }

    public String getName()
    {
        return name;
    }

    public DeploymentEnvironment setName(String name)
    {
        this.name = name;
        return this;
    }

    public DeploymentEnvironmentType getType()
    {
        return type;
    }

    public DeploymentEnvironment setType(DeploymentEnvironmentType type)
    {
        this.type = type;
        return this;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (!(o instanceof DeploymentEnvironment))
        {
            return false;
        }
        DeploymentEnvironment that = (DeploymentEnvironment) o;
        return id.equals(that.id) && name.equals(that.name) && type == that.type;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(id, name, type);
    }

    @Override
    public String toString()
    {
        return new StringJoiner(", ", DeploymentEnvironment.class.getSimpleName() + "[", "]").add("id='" + id + "'")
                .add("name='" + name + "'")
                .add("type=" + type)
                .toString();
    }
}
