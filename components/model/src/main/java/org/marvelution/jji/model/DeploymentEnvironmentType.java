/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.model;

import java.util.Locale;
import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlType;

import static org.apache.commons.lang3.StringUtils.capitalize;

@XmlEnum
@XmlType
@javax.xml.bind.annotation.XmlEnum
@javax.xml.bind.annotation.XmlType
public enum DeploymentEnvironmentType
{
    UNMAPPED,
    DEVELOPMENT,
    TESTING,
    STAGING,
    PRODUCTION;

    public static DeploymentEnvironmentType fromString(String string)
    {
        try
        {
            return valueOf(string.toUpperCase(Locale.ENGLISH));
        }
        catch (Exception e)
        {
            return UNMAPPED;
        }
    }

    public String value()
    {
        return name().toLowerCase(Locale.ENGLISH);
    }

    @Override
    public String toString()
    {
        return capitalize(name().toLowerCase(Locale.ENGLISH));
    }
}
