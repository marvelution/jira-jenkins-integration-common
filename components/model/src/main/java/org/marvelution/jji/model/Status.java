/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.model;

import jakarta.xml.bind.annotation.XmlEnum;

@XmlEnum
@javax.xml.bind.annotation.XmlEnum
public enum Status
{

    OFFLINE(false, false, "offline"),
    NOT_ACCESSIBLE(true, false, "online but not accessible"),
    ONLINE(true, true, "online and accessible"),
    FIREWALLED(true, true, "presumed online and accessible"),
    UNREACHABLE(false, false, "presumed offline, DNS or SSL issue"),
    GETTING_READY(false, false, "presumes online, will be ready soon");

    private final boolean online;
    private final boolean accessible;
    private final String description;

    Status(
            boolean online,
            boolean accessible,
            String description)
    {
        this.online = online;
        this.accessible = accessible;
        this.description = description;
    }

    public boolean isOnline()
    {
        return online;
    }

    public boolean isAccessible()
    {
        return accessible;
    }

    public String describe()
    {
        return description;
    }
}
