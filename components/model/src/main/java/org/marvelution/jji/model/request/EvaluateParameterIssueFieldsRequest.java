/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.model.request;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@javax.xml.bind.annotation.XmlRootElement
@javax.xml.bind.annotation.XmlAccessorType(javax.xml.bind.annotation.XmlAccessType.FIELD)
public class EvaluateParameterIssueFieldsRequest
{

    private String issueKey;
    private boolean useUserId;
    private String fields;
    private String names;
    private String mappers;
    private boolean excludeDefaults;

    public String getIssueKey()
    {
        return issueKey;
    }

    public void setIssueKey(String issueKey)
    {
        this.issueKey = issueKey;
    }

    public boolean isUseUserId()
    {
        return useUserId;
    }

    public void setUseUserId(boolean useUserId)
    {
        this.useUserId = useUserId;
    }

    public String getFields()
    {
        return fields;
    }

    public void setFields(String fields)
    {
        this.fields = fields;
    }

    public String getNames()
    {
        return names;
    }

    public void setNames(String names)
    {
        this.names = names;
    }

    public String getMappers()
    {
        return mappers;
    }

    public void setMappers(String mappers)
    {
        this.mappers = mappers;
    }

    public boolean isExcludeDefaults()
    {
        return excludeDefaults;
    }

    public void setExcludeDefaults(boolean excludeDefaults)
    {
        this.excludeDefaults = excludeDefaults;
    }
}
