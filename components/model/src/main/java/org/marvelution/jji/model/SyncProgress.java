/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.model;

import jakarta.annotation.Nullable;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@javax.xml.bind.annotation.XmlRootElement
@javax.xml.bind.annotation.XmlAccessorType(javax.xml.bind.annotation.XmlAccessType.FIELD)
public class SyncProgress
{

    private boolean queued;
    private boolean started;
    private boolean finished;
    private int jobCount;
    private int buildCount;
    private int issueCount;
    private int errorCount;
    @Nullable
    private String error;

    public boolean isQueued()
    {
        return queued;
    }

    public SyncProgress setQueued(boolean queued)
    {
        this.queued = queued;
        return this;
    }

    public boolean hasStarted()
    {
        return started;
    }

    public SyncProgress setStarted(boolean started)
    {
        this.started = started;
        return this;
    }

    public boolean isFinished()
    {
        return finished;
    }

    public SyncProgress setFinished(boolean finished)
    {
        this.finished = finished;
        return this;
    }

    public int getJobCount()
    {
        return jobCount;
    }

    public SyncProgress setJobCount(int jobCount)
    {
        this.jobCount = jobCount;
        return this;
    }

    public int getBuildCount()
    {
        return buildCount;
    }

    public SyncProgress setBuildCount(int buildCount)
    {
        this.buildCount = buildCount;
        return this;
    }

    public int getIssueCount()
    {
        return issueCount;
    }

    public SyncProgress setIssueCount(int issueCount)
    {
        this.issueCount = issueCount;
        return this;
    }

    public int getErrorCount()
    {
        return errorCount;
    }

    public SyncProgress setErrorCount(int errorCount)
    {
        this.errorCount = errorCount;
        return this;
    }

    @Nullable
    public String getError()
    {
        return error;
    }

    public SyncProgress setError(String error)
    {
        this.error = error;
        return this;
    }
}
