/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.model;

import java.io.Serializable;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;
import jakarta.json.bind.annotation.JsonbCreator;
import jakarta.json.bind.annotation.JsonbProperty;
import jakarta.json.bind.annotation.JsonbTransient;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import static java.util.Objects.requireNonNull;
import static org.apache.commons.lang3.StringUtils.stripEnd;

@Immutable
public final class OperationId
        implements Serializable, Comparable<OperationId>
{

    private static final Pattern PARTS_PATTERN = Pattern.compile("^(([^-]*)-([^-]*))-.*$");
    private final String id;
    private final String prefix;

    private OperationId(
            String id,
            String prefix)
    {
        this.id = requireNonNull(id);
        this.prefix = requireNonNull(prefix);
    }

    public String getId()
    {
        return id;
    }

    public String getPrefix()
    {
        return prefix;
    }

    @JsonIgnore
    @JsonbTransient
    public String getUnprefixedId()
    {
        return id.substring(prefix.length() + 1);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(id);
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }
        OperationId that = (OperationId) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public String toString()
    {
        return id;
    }

    @Override
    public int compareTo(
            @Nonnull
            OperationId other)
    {
        return id.compareTo(other.id);
    }

    public static OperationId of(String id)
    {
        Matcher partsMatchers = PARTS_PATTERN.matcher(id);
        if (partsMatchers.matches())
        {
            return new OperationId(id, partsMatchers.group(1));
        }
        else
        {
            throw new IllegalArgumentException("Invalid id, doesn't match " + PARTS_PATTERN.pattern());
        }
    }

    @JsonCreator
    @JsonbCreator
    public static OperationId of(
            @JsonProperty
            @JsonbProperty
            String prefix,
            @JsonProperty
            @JsonbProperty
            String id)
    {
        if (id.startsWith(prefix))
        {
            return new OperationId(id, prefix);
        }
        else
        {
            return new OperationId(stripEnd(prefix, "-") + "-" + id, prefix);
        }
    }

}
