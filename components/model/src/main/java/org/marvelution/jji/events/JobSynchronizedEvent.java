/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.events;

import java.time.LocalDateTime;

import org.marvelution.jji.model.Job;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({"timestamp",
        "job"})
public class JobSynchronizedEvent
        extends AbstractSynchronizedEvent<Job>
{
    JobSynchronizedEvent()
    {
        super(null);
    }

    public JobSynchronizedEvent(Job job)
    {
        super(job);
    }

    @JsonCreator
    public JobSynchronizedEvent(
            @JsonProperty("job")
            Job job,
            @JsonProperty("timestamp")
            LocalDateTime timestamp)
    {
        super(job, timestamp);
    }


    public Job getJob()
    {
        return getSyncable();
    }
}
