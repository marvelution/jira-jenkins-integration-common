/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.utils;

import java.util.Optional;
import javax.annotation.Nullable;

import org.marvelution.jji.model.Job;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;

public final class JobHash
{

    public static String hash(Job job)
    {
        return hash(job.getUrlName());
    }

    public static String hash(
            String name,
            @Nullable
            String urlName)
    {
        return hash(Optional.ofNullable(urlName)
                .orElse(name));
    }

    private static String hash(String urlName)
    {
        return Optional.ofNullable(urlName)
                .filter(StringUtils::isNotBlank)
                .map(DigestUtils::sha1Hex)
                .orElse(null);
    }
}
