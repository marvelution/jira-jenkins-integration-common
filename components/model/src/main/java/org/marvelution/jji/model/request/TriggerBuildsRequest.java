/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.model.request;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.StringJoiner;
import jakarta.xml.bind.annotation.*;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@javax.xml.bind.annotation.XmlRootElement
@javax.xml.bind.annotation.XmlAccessorType(javax.xml.bind.annotation.XmlAccessType.FIELD)
public class TriggerBuildsRequest
{
    public static final String LINKED = JobSelector.linked.name();
    public static final String BY_CONVENTION = JobSelector.byConvention.name();
    public static final String BY_ISSUE_FIELD = JobSelector.byIssueField.name();
    public static final String SPECIFIC_JOB = JobSelector.specificJob.name();

    private String issueKey;
    private JobSelector jobSelector;
    private String byIssueField;
    private String byIssueFieldMapping;
    private Set<String> jobs;
    private String lookupJob;
    private boolean exactMatch;
    private UseParameters useParameters;
    private Set<Parameter> parameters;

    public String getIssueKey()
    {
        return issueKey;
    }

    public TriggerBuildsRequest setIssueKey(String issueKey)
    {
        this.issueKey = issueKey;
        return this;
    }

    public JobSelector getJobSelector()
    {
        return jobSelector;
    }

    public TriggerBuildsRequest setJobSelector(JobSelector jobSelector)
    {
        this.jobSelector = jobSelector;
        return this;
    }

    public boolean isLinked()
    {
        return jobSelector == JobSelector.linked;
    }

    public TriggerBuildsRequest setLinked()
    {
        jobSelector = JobSelector.linked;
        return this;
    }

    public boolean isByConvention()
    {
        return jobSelector == JobSelector.byConvention;
    }

    public TriggerBuildsRequest setByConvention()
    {
        jobSelector = JobSelector.byConvention;
        return this;
    }

    public boolean isByIssueField()
    {
        return jobSelector == JobSelector.byIssueField && byIssueField != null;
    }

    public String getByIssueField()
    {
        return byIssueField;
    }

    public TriggerBuildsRequest setByIssueField(String byIssueField)
    {
        jobSelector = JobSelector.byIssueField;
        this.byIssueField = byIssueField;
        return this;
    }

    public String getByIssueFieldMapping()
    {
        return byIssueFieldMapping;
    }

    public TriggerBuildsRequest setByIssueFieldMapping(String byIssueFieldMapping)
    {
        this.byIssueFieldMapping = byIssueFieldMapping;
        return this;
    }

    public boolean isSpecificJob()
    {
        return (jobSelector == JobSelector.specificJob || jobSelector == null) && jobs != null && !jobs.isEmpty();
    }

    public Set<String> getJobs()
    {
        if (jobs == null)
        {
            jobs = new HashSet<>();
        }
        return jobs;
    }

    public TriggerBuildsRequest setJobs(Set<String> jobs)
    {
        jobSelector = JobSelector.specificJob;
        this.jobs = jobs;
        return this;
    }

    public boolean isLookupJob()
    {
        return jobSelector == JobSelector.lookupJob && lookupJob != null;
    }

    public String getLookupJob()
    {
        return lookupJob;
    }

    public TriggerBuildsRequest setLookupJob(String lookupJob)
    {
        jobSelector = JobSelector.lookupJob;
        this.lookupJob = lookupJob;
        return this;
    }

    public boolean isExactMatch()
    {
        return exactMatch;
    }

    public TriggerBuildsRequest setExactMatch(boolean exactMatch)
    {
        this.exactMatch = exactMatch;
        return this;
    }

    public UseParameters getUseParameters()
    {
        return useParameters == null ? UseParameters.reference : useParameters;
    }

    public TriggerBuildsRequest setUseParameters(UseParameters useParameters)
    {
        this.useParameters = useParameters;
        return this;
    }

    public Set<Parameter> getParameters()
    {
        return parameters;
    }

    public TriggerBuildsRequest setParameters(Set<Parameter> parameters)
    {
        this.parameters = parameters;
        return this;
    }

    public TriggerBuildsRequest addParameter(
            String name,
            String value)
    {
        if (parameters == null)
        {
            parameters = new HashSet<>();
        }
        parameters.add(new Parameter().setName(name)
                .setValue(value));
        return this;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(issueKey,
                jobSelector,
                byIssueField,
                byIssueFieldMapping,
                jobs,
                lookupJob,
                exactMatch,
                useParameters,
                parameters);
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (!(o instanceof TriggerBuildsRequest that))
        {
            return false;
        }
        return Objects.equals(issueKey, that.issueKey) && jobSelector == that.jobSelector && Objects.equals(byIssueField,
                that.byIssueField) && Objects.equals(byIssueFieldMapping, that.byIssueFieldMapping) && Objects.equals(jobs, that.jobs) &&
               Objects.equals(lookupJob, that.lookupJob) && exactMatch == that.exactMatch && useParameters == that.useParameters &&
               Objects.equals(parameters, that.parameters);
    }

    @Override
    public String toString()
    {
        return new StringJoiner(", ", TriggerBuildsRequest.class.getSimpleName() + "[", "]").add("issueKey='" + issueKey + "'")
                .add("jobSelector=" + jobSelector)
                .add("byIssueField='" + byIssueField + "'")
                .add("byIssueFieldMapping='" + byIssueFieldMapping + "'")
                .add("jobs=" + jobs)
                .add("lookupJob='" + lookupJob + "'")
                .add("exactMatch=" + exactMatch)
                .add("useParameters=" + useParameters)
                .add("parameters=" + parameters)
                .toString();
    }

    @XmlEnum
    @XmlType
    @javax.xml.bind.annotation.XmlEnum
    @javax.xml.bind.annotation.XmlType
    public enum JobSelector
    {
        linked,
        byConvention,
        byIssueField,
        lookupJob,
        specificJob
    }

    @XmlEnum
    @XmlType
    @javax.xml.bind.annotation.XmlEnum
    @javax.xml.bind.annotation.XmlType
    public enum UseParameters
    {
        reference,
        none,
        specified
    }

    @XmlRootElement
    @XmlAccessorType(XmlAccessType.FIELD)
    @javax.xml.bind.annotation.XmlRootElement
    @javax.xml.bind.annotation.XmlAccessorType(javax.xml.bind.annotation.XmlAccessType.FIELD)
    public static class Parameter
    {
        private String name;
        private String value;

        public String getName()
        {
            return name;
        }

        public Parameter setName(String name)
        {
            this.name = name;
            return this;
        }

        public String getValue()
        {
            return value;
        }

        public Parameter setValue(String value)
        {
            this.value = value;
            return this;
        }

        @Override
        public int hashCode()
        {
            return Objects.hashCode(name);
        }

        @Override
        public boolean equals(Object o)
        {
            if (this == o)
            {
                return true;
            }
            if (!(o instanceof Parameter that))
            {
                return false;
            }
            return Objects.equals(name, that.name);
        }

        @Override
        public String toString()
        {
            return new StringJoiner(", ", Parameter.class.getSimpleName() + "[", "]").add("name='" + name + "'")
                    .add("value=" + value)
                    .toString();
        }

        public static Parameter create(
                String name,
                String value)
        {
            return new Parameter().setName(name)
                    .setValue(value);
        }
    }
}
