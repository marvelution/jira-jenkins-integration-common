/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.model.request;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import jakarta.xml.bind.annotation.*;

import org.marvelution.jji.model.Job;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@javax.xml.bind.annotation.XmlRootElement
@javax.xml.bind.annotation.XmlAccessorType(javax.xml.bind.annotation.XmlAccessType.FIELD)
public class BulkRequest
{

    private List<Operation> operations;
    private List<String> jobIds;
    @XmlTransient
    @javax.xml.bind.annotation.XmlTransient
    private Set<Job> jobs;

    public List<Operation> getOperations()
    {
        if (operations == null)
        {
            operations = new ArrayList<>();
        }
        return operations;
    }

    public BulkRequest setOperations(List<Operation> operations)
    {
        this.operations = new ArrayList<>(operations);
        return this;
    }

    public List<String> getJobIds()
    {
        if (jobIds == null)
        {
            jobIds = new ArrayList<>();
        }
        return jobIds;
    }

    public BulkRequest setJobIds(List<String> jobIds)
    {
        this.jobIds = new ArrayList<>(jobIds);
        return this;
    }

    public Set<Job> getJobs()
    {
        if (jobs == null)
        {
            jobs = new HashSet<>();
        }
        return jobs;
    }

    public void setJobs(Set<Job> jobs)
    {
        this.jobs = jobs;
    }

    @XmlEnum
    @XmlType
    @javax.xml.bind.annotation.XmlEnum
    @javax.xml.bind.annotation.XmlType
    public enum Operation
    {
        // Keep the operations in order in which they should be processed if multiple operations are provided.
        ENABLE_JOB("enable.job.for.sync"),
        DISABLE_JOB("disable.job.for.sync"),
        CLEAR_CACHE("delete.all.builds"),
        REBUILD_CACHE("rebuild.build.cache"),
        SYNCHRONIZE("sync.job");

        private final String i18n;

        Operation(String i18n)
        {
            this.i18n = i18n;
        }

        public String i18n()
        {
            return i18n;
        }
    }
}
