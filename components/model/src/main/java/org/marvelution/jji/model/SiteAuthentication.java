/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.model;

public abstract class SiteAuthentication
{

	private final SiteAuthenticationType type;

	protected SiteAuthentication(SiteAuthenticationType type)
	{
		this.type = type;
	}

	public SiteAuthenticationType getType()
	{
		return type;
	}

	public static class BasicSiteAuthentication
			extends SiteAuthentication
	{

		private final String user;
		private final String token;

		public BasicSiteAuthentication(
				String user,
				String token)
		{
			super(SiteAuthenticationType.BASIC);
			this.user = user;
			this.token = token;
		}

		public String getUser()
		{
			return user;
		}

		public String getToken()
		{
			return token;
		}
	}

	public static class SyncTokenSiteAuthentication
			extends SiteAuthentication
	{

		public SyncTokenSiteAuthentication()
		{
			super(SiteAuthenticationType.TOKEN);
		}
	}
}
