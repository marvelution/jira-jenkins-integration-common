/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.model;

import java.net.URI;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.*;
import jakarta.annotation.Nullable;
import jakarta.xml.bind.annotation.*;

import org.marvelution.jji.utils.JobHash;
import org.marvelution.jji.utils.SiteUrl;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.apache.commons.lang3.builder.ToStringBuilder;

import static java.util.Optional.ofNullable;
import static org.apache.commons.lang3.StringUtils.*;
import static org.apache.commons.lang3.builder.ToStringStyle.SHORT_PREFIX_STYLE;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@javax.xml.bind.annotation.XmlRootElement
@javax.xml.bind.annotation.XmlAccessorType(javax.xml.bind.annotation.XmlAccessType.FIELD)
@JsonIdentityInfo(property = "id",
                  generator = ObjectIdGenerators.None.class)
public class Job
        implements HasId<Job>, JobsContainer, Syncable<Job>
{

    public static final String OPERATION_PREFIX = "job-sync";
    private String id;
    private Site site;
    private String name;
    @XmlTransient
    @javax.xml.bind.annotation.XmlTransient
    private String parentName;
    @XmlTransient
    @javax.xml.bind.annotation.XmlTransient
    private String fullName;
    @XmlTransient
    @javax.xml.bind.annotation.XmlTransient
    private String urlName;
    @XmlTransient
    @javax.xml.bind.annotation.XmlTransient
    private String displayName;
    @XmlTransient
    @javax.xml.bind.annotation.XmlTransient
    private URI displayUrl;
    private String description;
    private int lastBuild = 0;
    private int oldestBuild = -1;
    private boolean linked = false;
    private boolean deleted = false;
    @Nullable
    private List<Build> builds;
    @Nullable
    private List<Job> jobs;
    @Nullable
    private Set<IssueReference> issueReferences;

    @Override
    public Job copy()
    {
        Job job = new Job();
        job.id = id;
        if (site != null)
        {
            job.site = site.copy();
        }
        job.parentName = parentName;
        job.name = name;
        job.fullName = fullName;
        job.urlName = urlName;
        job.displayName = displayName;
        job.description = description;
        job.lastBuild = lastBuild;
        job.oldestBuild = oldestBuild;
        job.linked = linked;
        job.deleted = deleted;
        return job;
    }

    @Override
    public String getId()
    {
        return id;
    }

    @Override
    public Job setId(String id)
    {
        this.id = id;
        return this;
    }

    @Override
    public Site getSite()
    {
        return site;
    }

    public Job setSite(Site site)
    {
        this.site = site;
        return this;
    }

    @Override
    public List<Job> getJobs()
    {
        if (jobs == null)
        {
            jobs = new ArrayList<>();
        }
        return jobs;
    }

    public Job setJobs(
            @Nullable
            List<Job> jobs)
    {
        this.jobs = jobs;
        return this;
    }

    @Override
    public boolean hasJobs()
    {
        return jobs != null && !jobs.isEmpty();
    }

    public void addJob(
            @Nullable
            Job job)
    {
        if (job != null)
        {
            getJobs().add(job);
        }
    }

    @Override
    public boolean shouldJobBeLinked(Job job)
    {
        return site != null && site.shouldJobBeLinked(job);
    }

    public String getName()
    {
        return name;
    }

    public Job setName(String name)
    {
        this.name = name;
        return this;
    }

    @XmlElement
    @javax.xml.bind.annotation.XmlElement
    @Nullable
    public String getParentName()
    {
        return determineParentName().orElse(null);
    }

    public Job setParentName(String parentName)
    {
        this.parentName = Objects.equals(name, parentName) ? null : defaultIfBlank(parentName, null);
        ;
        return this;
    }

    private Optional<String> determineParentName()
    {
        if (isBlank(parentName))
        {
            determineParentNameFromUrlName(urlName).ifPresent(this::setParentName);
        }
        return ofNullable(parentName);
    }

    @XmlElement
    @javax.xml.bind.annotation.XmlElement
    public String getFullName()
    {
        return determineFullName().orElseGet(this::getName);
    }

    public Job setFullName(
            @Nullable
            String fullName)
    {
        this.fullName = defaultIfBlank(fullName, null);
        return this;
    }

    private Optional<String> determineFullName()
    {
        if (isBlank(fullName))
        {
            determineParentName().filter(name -> !Objects.equals(name, getName()))
                    .map(parent -> parent + "/" + getName())
                    .ifPresent(this::setFullName);
        }
        return ofNullable(fullName);
    }

    @XmlElement
    @javax.xml.bind.annotation.XmlElement
    public String getUrlName()
    {
        return urlName != null ? urlName : name;
    }

    public Job setUrlName(
            @Nullable
            String urlName)
    {
        this.urlName = defaultIfBlank(urlName, null);
        return this;
    }

    @Nullable
    public String getUrlNameOrNull()
    {
        if (isNotBlank(urlName))
        {
            return urlName;
        }
        else
        {
            return null;
        }
    }

    @Override
    @XmlElement
    @javax.xml.bind.annotation.XmlElement
    public String getDisplayName()
    {
        return determineDisplayName().orElseGet(this::getName);
    }

    @Override
    @XmlTransient
    @javax.xml.bind.annotation.XmlTransient
    public OperationId getOperationId()
    {
        return OperationId.of(OPERATION_PREFIX, id);
    }

    @Override
    public boolean isEnabled()
    {
        return (site == null || site.isEnabled()) && isLinked() && !isDeleted();
    }

    public Job setDisplayName(
            @Nullable
            String displayName)
    {
        this.displayName = Objects.equals(name, displayName) ? null : defaultIfBlank(displayName, null);
        ;
        return this;
    }

    @Nullable
    public String getDisplayNameOrNull()
    {
        return determineDisplayName().orElse(null);
    }

    private Optional<String> determineDisplayName()
    {
        if (isBlank(displayName))
        {
            ofNullable(getFullName()).filter(name -> !Objects.equals(name, getName()))
                    .map(name -> name.replace("/", " - "))
                    .ifPresent(this::setDisplayName);
        }
        return ofNullable(displayName);
    }

    @XmlElement
    @javax.xml.bind.annotation.XmlElement
    public URI getDisplayUrl()
    {
        if (displayUrl == null)
        {
            displayUrl = SiteUrl.displayUrl()
                    .job(this)
                    .nullable()
                    .url();
        }
        return displayUrl;
    }

    public Job setDisplayUrl(
            @Nullable
            URI displayUrl)
    {
        this.displayUrl = displayUrl;
        return this;
    }

    @Nullable
    public String getDescription()
    {
        return description;
    }

    public Job setDescription(
            @Nullable
            String description)
    {
        this.description = defaultIfBlank(description, null);
        return this;
    }

    public int getLastBuild()
    {
        return lastBuild;
    }

    public Job setLastBuild(int lastBuild)
    {
        this.lastBuild = lastBuild;
        return this;
    }

    public Optional<Build> getLastBuildObject()
    {
        return locateBuild(lastBuild);
    }

    public int getOldestBuild()
    {
        return oldestBuild;
    }

    public Job setOldestBuild(int oldestBuild)
    {
        this.oldestBuild = oldestBuild;
        return this;
    }

    public Optional<Build> getOldestBuildObject()
    {
        return locateBuild(oldestBuild);
    }

    private Optional<Build> locateBuild(int number)
    {
        return getBuilds().stream()
                .filter(build -> build.getNumber() == number)
                .findFirst();
    }

    public boolean isLinked()
    {
        return linked;
    }

    public Job setLinked(boolean linked)
    {
        this.linked = linked;
        return this;
    }

    public boolean isDeleted()
    {
        return deleted;
    }

    public Job setDeleted(boolean deleted)
    {
        this.deleted = deleted;
        return this;
    }

    public boolean hasBuilds()
    {
        return !getBuilds().isEmpty() || lastBuild > 0 || oldestBuild > 0;
    }

    public List<Build> getBuilds()
    {
        if (builds == null)
        {
            builds = new ArrayList<>();
        }
        return builds;
    }

    public Job setBuilds(
            @Nullable
            List<Build> builds)
    {
        this.builds = ofNullable(builds).map(ArrayList::new)
                .orElse(null);
        return this;
    }

    public Job addBuild(
            @Nullable
            Build build)
    {
        if (build != null)
        {
            getBuilds().add(build);
        }
        return this;
    }

    public Set<IssueReference> getIssueReferences()
    {
        if (issueReferences == null)
        {
            issueReferences = new HashSet<>();
        }
        return issueReferences;
    }

    public Job setIssueReferences(Collection<IssueReference> issueReferences)
    {
        if (issueReferences != null)
        {
            this.issueReferences = new HashSet<>(issueReferences);
        }
        else
        {
            this.issueReferences = new HashSet<>();
        }
        return this;
    }

    public boolean shouldBeLinked()
    {
        return shouldJobBeLinked(this);
    }

    public String getHash()
    {
        return JobHash.hash(this);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(id, site, getUrlName());
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }
        Job job = (Job) o;
        return Objects.equals(id, job.id) && Objects.equals(site, job.site) && Objects.equals(getUrlName(), job.getUrlName());
    }

    @Override
    public String toString()
    {
        return new ToStringBuilder(this, SHORT_PREFIX_STYLE).append("id", id)
                .append("siteId", site != null ? site.getId() : null)
                .append("name", name)
                .append("urlName", urlName)
                .append("lastBuild", lastBuild)
                .append("linked", linked)
                .append("deleted", deleted)
                .toString();
    }

    public static Optional<String> determineParentNameFromUrlName(String urlName)
    {
        return ofNullable(urlName).map(url -> url.replace("/job/", "/"))
                .map(url -> url.replace("/branch/", "/"))
                .filter(name -> name.contains("/"))
                .map(name -> name.substring(0, name.lastIndexOf("/")))
                .map(url -> URLDecoder.decode(url, StandardCharsets.UTF_8));
    }

}
