/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@javax.xml.bind.annotation.XmlRootElement
@javax.xml.bind.annotation.XmlAccessorType(javax.xml.bind.annotation.XmlAccessType.FIELD)
public class Configuration
{

    private String scope;
    private List<ConfigurationSetting> configuration;

    public String getScope()
    {
        return scope;
    }

    public Configuration setScope(String scope)
    {
        this.scope = scope;
        return this;
    }

    public List<ConfigurationSetting> getConfiguration()
    {
        if (configuration == null)
        {
            configuration = new ArrayList<>();
        }
        return configuration;
    }

    public Configuration setConfiguration(List<ConfigurationSetting> configuration)
    {
        this.configuration = configuration;
        return this;
    }

    public Configuration addConfiguration(ConfigurationSetting configuration)
    {
        getConfiguration().add(configuration);
        return this;
    }

    public Optional<ConfigurationSetting> getConfiguration(String key)
    {
        return configuration.stream()
                .filter(configurationSetting -> Objects.equals(key, configurationSetting.getKey()))
                .findFirst();
    }

    public ConfigurationSetting requireConfiguration(String key)
    {
        return configuration.stream()
                .filter(configurationSetting -> Objects.equals(key, configurationSetting.getKey()))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("no setting with key found"));
    }
}
