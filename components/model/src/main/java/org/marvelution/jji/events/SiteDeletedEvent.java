/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.events;

import java.util.Objects;

import org.marvelution.jji.model.Site;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SiteDeletedEvent
{

    private final Site site;

    public SiteDeletedEvent(
            @JsonProperty("site")
            Site site)
    {
        this.site = site;
    }

    public Site getSite()
    {
        return site;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(site);
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (!(o instanceof SiteDeletedEvent))
        {
            return false;
        }
        SiteDeletedEvent that = (SiteDeletedEvent) o;
        return site.equals(that.site);
    }
}
