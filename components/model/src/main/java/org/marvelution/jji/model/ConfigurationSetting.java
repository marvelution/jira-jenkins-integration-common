/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.model;

import java.util.Objects;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.function.Function;
import jakarta.annotation.Nullable;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.StringUtils;

import static java.util.Optional.ofNullable;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@javax.xml.bind.annotation.XmlRootElement
@javax.xml.bind.annotation.XmlAccessorType(javax.xml.bind.annotation.XmlAccessType.FIELD)
public class ConfigurationSetting
{

    public static final String GLOBAL_SCOPE = "";
    private String scope = GLOBAL_SCOPE;
    private String key;
    @Nullable
    private String value;
    private boolean overridable = true;
    private boolean internal = false;

    public String getScope()
    {
        return scope;
    }

    public ConfigurationSetting setScope(String scope)
    {
        this.scope = scope == null ? GLOBAL_SCOPE : scope;
        return this;
    }

    public String getKey()
    {
        return key;
    }

    public ConfigurationSetting setKey(String key)
    {
        this.key = key;
        return this;
    }

    @Nullable
    public String getValue()
    {
        return value;
    }

    public ConfigurationSetting setValue(String value)
    {
        this.value = value != null ? value : "";
        return this;
    }

    public ConfigurationSetting setValue(Integer value)
    {
        return setValue(Integer.toString(value));
    }

    public ConfigurationSetting setValue(Boolean value)
    {
        return setValue(Boolean.toString(value));
    }

    public Optional<Integer> valueAsInteger()
    {
        return valueAs(v -> {
            try
            {
                return Integer.valueOf(v);
            }
            catch (NumberFormatException e)
            {
                return null;
            }
        });
    }

    public Optional<Boolean> valueAsBoolean()
    {
        return valueAs(Boolean::valueOf);
    }

    public <T> Optional<T> valueAs(Function<String, T> function)
    {
        return ofNullable(value).filter(StringUtils::isNotBlank)
                .map(function);
    }

    public boolean isOverridable()
    {
        return overridable;
    }

    public ConfigurationSetting setOverridable(boolean overridable)
    {
        this.overridable = overridable;
        return this;
    }

    public boolean isInternal()
    {
        return internal;
    }

    public ConfigurationSetting setInternal(boolean internal)
    {
        this.internal = internal;
        return this;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(scope, key, value, overridable, internal);
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }
        ConfigurationSetting that = (ConfigurationSetting) o;
        return overridable == that.overridable && internal == that.internal && Objects.equals(key, that.key) && Objects.equals(scope,
                that.scope) && Objects.equals(value, that.value);
    }

    @Override
    public String toString()
    {
        return new StringJoiner(", ", ConfigurationSetting.class.getSimpleName() + "[", "]").add("scope='" + scope + "'")
                .add("key='" + key + "'")
                .add("value='" + value + "'")
                .add("overridable=" + overridable)
                .add("internal=" + internal)
                .toString();
    }

    public static ConfigurationSetting forKey(String key)
    {
        return new ConfigurationSetting().setKey(key);
    }
}
