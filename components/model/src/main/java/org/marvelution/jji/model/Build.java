/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.model;

import java.net.URI;
import java.time.Duration;
import java.util.*;
import java.util.stream.Collectors;
import jakarta.annotation.Nullable;
import jakarta.xml.bind.annotation.*;

import org.marvelution.jji.utils.SiteUrl;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.apache.commons.lang3.builder.ToStringBuilder;

import static org.apache.commons.lang3.StringUtils.*;
import static org.apache.commons.lang3.builder.ToStringStyle.SHORT_PREFIX_STYLE;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@javax.xml.bind.annotation.XmlRootElement
@javax.xml.bind.annotation.XmlAccessorType(javax.xml.bind.annotation.XmlAccessType.FIELD)
@JsonIdentityInfo(property = "id",
                  generator = ObjectIdGenerators.None.class)
public class Build
        implements HasId<Build>, Syncable<Build>
{

    public static final String OPERATION_PREFIX = "build-sync";
    private String id;
    private Job job;
    private int number;
    private String displayName;
    @XmlTransient
    @javax.xml.bind.annotation.XmlTransient
    private URI displayUrl;
    private String description;
    private boolean deleted;
    private String cause;
    private boolean building;
    private Result result;
    @XmlTransient
    @javax.xml.bind.annotation.XmlTransient
    private String builtOn;
    private long duration;
    private long timestamp;
    @Nullable
    private Set<String> branches;
    @Nullable
    private List<ChangeSet> changeSet;
    @Nullable
    private TestResults testResults;
    @Nullable
    private List<DeploymentEnvironment> deploymentEnvironments;
    @Nullable
    private Set<IssueReference> issueReferences;

    @Override
    public Build copy()
    {
        Build build = new Build();
        build.id = id;
        if (job != null)
        {
            build.job = job.copy();
        }
        build.number = number;
        build.displayName = displayName;
        build.description = description;
        build.deleted = deleted;
        build.cause = cause;
        build.building = building;
        build.result = result;
        build.builtOn = builtOn;
        build.duration = duration;
        build.timestamp = timestamp;
        if (branches != null)
        {
            build.branches = new HashSet<>(branches);
        }
        if (changeSet != null)
        {
            build.changeSet = changeSet.stream()
                    .map(change -> new ChangeSet().setCommitId(change.getCommitId())
                            .setMessage(change.getMessage()))
                    .collect(Collectors.toList());
        }
        if (testResults != null)
        {
            build.testResults = new TestResults().setFailed(testResults.getFailed())
                    .setSkipped(testResults.getSkipped())
                    .setTotal(testResults.getTotal());
        }
        if (deploymentEnvironments != null)
        {
            build.deploymentEnvironments = deploymentEnvironments.stream()
                    .map(DeploymentEnvironment::copy)
                    .collect(Collectors.toList());
        }
        return build;
    }

    @Override
    public String getId()
    {
        return id;
    }

    @Override
    public Build setId(String id)
    {
        this.id = id;
        return this;
    }

    public Job getJob()
    {
        return job;
    }

    public Build setJob(Job job)
    {
        this.job = job;
        return this;
    }

    public int getNumber()
    {
        return number;
    }

    public Build setNumber(int number)
    {
        this.number = number;
        return this;
    }

    public String getDisplayNumber()
    {
        return "#" + getNumber();
    }

    @Override
    public String getDisplayName()
    {
        return Optional.ofNullable(displayName)
                .orElseGet(() -> {
                    if (job == null)
                    {
                        return getDisplayNumber();
                    }
                    else
                    {
                        return getJob().getDisplayName() + " " + getDisplayNumber();
                    }
                });
    }

    public Build setDisplayName(
            @Nullable
            String displayName)
    {
        this.displayName = defaultIfBlank(displayName, null);
        return this;
    }

    @Override
    @XmlTransient
    @javax.xml.bind.annotation.XmlTransient
    public Site getSite()
    {
        return job.getSite();
    }

    @Override
    @XmlTransient
    @javax.xml.bind.annotation.XmlTransient
    public OperationId getOperationId()
    {
        return OperationId.of(OPERATION_PREFIX, job.getId() + "-" + number);
    }

    @Override
    public boolean isEnabled()
    {
        return job.isEnabled();
    }

    @Nullable
    public String getDisplayNameOrNull()
    {
        return displayName;
    }

    public String getShortDisplayName()
    {
        return Optional.ofNullable(displayName)
                .orElseGet(this::getDisplayNumber);
    }

    @XmlElement
    @javax.xml.bind.annotation.XmlElement
    public URI getDisplayUrl()
    {
        if (displayUrl == null)
        {
            displayUrl = SiteUrl.displayUrl()
                    .build(this)
                    .nullable()
                    .url();
        }
        return displayUrl;
    }

    public Build setDisplayUrl(
            @Nullable
            URI displayUrl)
    {
        this.displayUrl = displayUrl;
        return this;
    }

    @Nullable
    public String getDescription()
    {
        return description;
    }

    public Build setDescription(
            @Nullable
            String description)
    {
        this.description = defaultIfBlank(description, null);
        return this;
    }

    public boolean isDeleted()
    {
        return deleted || job.isDeleted();
    }

    public Build setDeleted(boolean deleted)
    {
        this.deleted = deleted;
        return this;
    }

    @Nullable
    public String getCause()
    {
        return cause;
    }

    public Build setCause(
            @Nullable
            String cause)
    {
        this.cause = defaultString(cause, null);
        return this;
    }

    /**
     * @since 1.8.0
     */
    public boolean isBuilding()
    {
        return building;
    }

    /**
     * @since 1.8.0
     */
    public Build setBuilding(boolean building)
    {
        this.building = building;
        return this;
    }

    public Result getResult()
    {
        return result;
    }

    public Build setResult(Result result)
    {
        this.result = result;
        return this;
    }

    @XmlElement
    @javax.xml.bind.annotation.XmlElement
    public String getBuiltOn()
    {
        return defaultIfBlank(builtOn, "master");
    }

    public Build setBuiltOn(
            @Nullable
            String builtOn)
    {
        this.builtOn = builtOn;
        return this;
    }

    @Nullable
    public String getBuiltOnOrNull()
    {
        if (isNotBlank(builtOn))
        {
            return builtOn;
        }
        else
        {
            return null;
        }
    }

    public long getDuration()
    {
        return duration;
    }

    public Build setDuration(
            @Nullable
            Long duration)
    {
        this.duration = (duration != null) ? duration : 0;
        return this;
    }

    public String getFormattedDuration()
    {
        return Duration.ofMillis(duration)
                .toString()
                .substring(2)
                .replaceAll("(\\d[HMS])(?!$)", "$1 ")
                .toLowerCase();
    }

    public Date getBuildDate()
    {
        return new Date(timestamp);
    }

    public long getTimestamp()
    {
        return timestamp;
    }

    public Build setTimestamp(
            @Nullable
            Long timestamp)
    {
        this.timestamp = (timestamp != null) ? timestamp : 0;
        return this;
    }

    /**
     * @since 1.16
     */
    public Set<String> getBranches()
    {
        if (branches == null)
        {
            branches = new HashSet<>();
        }
        return branches;
    }

    /**
     * @since 1.16
     */
    public Build setBranches(
            @Nullable
            Set<String> branches)
    {
        this.branches = branches;
        return this;
    }

    public List<ChangeSet> getChangeSet()
    {
        if (changeSet == null)
        {
            changeSet = new ArrayList<>();
        }
        return changeSet;
    }

    public Build setChangeSet(
            @Nullable
            List<ChangeSet> changeSet)
    {
        if (changeSet != null)
        {
            this.changeSet = new ArrayList<>(changeSet.size());
            this.changeSet.addAll(changeSet);
        }
        else
        {
            this.changeSet = null;
        }
        return this;
    }

    @Nullable
    public TestResults getTestResults()
    {
        return testResults;
    }

    public Build setTestResults(
            @Nullable
            TestResults testResults)
    {
        this.testResults = testResults;
        return this;
    }

    /**
     * @since 1.14.0
     */
    public List<DeploymentEnvironment> getDeploymentEnvironments()
    {
        if (deploymentEnvironments == null)
        {
            deploymentEnvironments = new ArrayList<>();
        }
        return deploymentEnvironments;
    }

    /**
     * @since 1.14.0
     */
    public Build setDeploymentEnvironments(List<DeploymentEnvironment> deploymentEnvironments)
    {
        this.deploymentEnvironments = new ArrayList<>(deploymentEnvironments);
        return this;
    }

    /**
     * @since 1.14.0
     */
    public Set<IssueReference> getIssueReferences()
    {
        if (issueReferences == null)
        {
            issueReferences = new HashSet<>();
        }
        return issueReferences;
    }

    /**
     * @since 1.14.0
     */
    public Build setIssueReferences(Collection<IssueReference> issueReferences)
    {
        if (issueReferences != null)
        {
            this.issueReferences = new HashSet<>(issueReferences);
        }
        else
        {
            this.issueReferences = new HashSet<>();
        }
        return this;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(id, job, number);
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }
        Build build = (Build) o;
        return Objects.equals(id, build.id) && Objects.equals(job, build.job) && number == build.number;
    }

    @Override
    public String toString()
    {
        return new ToStringBuilder(this, SHORT_PREFIX_STYLE).append("id", id)
                .append("jobId", job.getId())
                .append("number", number)
                .append("cause", cause)
                .append("result", result)
                .append("timestamp", timestamp)
                .append("deleted", deleted)
                .toString();
    }
}
