/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.model;

import java.util.Locale;
import javax.annotation.Nullable;
import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlType;

@XmlEnum
@XmlType
@javax.xml.bind.annotation.XmlEnum
@javax.xml.bind.annotation.XmlType
public enum Result
{

    SUCCESS,
    UNSTABLE,
    FAILURE,
    NOT_BUILT,
    ABORTED,
    UNKNOWN;

    @Nullable
    public static Result fromString(
            @Nullable
            String result)
    {
        return result == null ? null : valueOf(result.toUpperCase(Locale.ENGLISH));
    }

    public String key()
    {
        return name().toLowerCase(Locale.ENGLISH)
                .replaceAll("[^a-z]", "");
    }

    public String i18n()
    {
        return "build.result." + name();
    }

    public boolean isWorseThan(Result that)
    {
        return ordinal() > that.ordinal();
    }

    public boolean isWorseOrEqualTo(Result that)
    {
        return ordinal() >= that.ordinal();
    }

    public boolean isBetterThan(Result that)
    {
        return ordinal() < that.ordinal();
    }

    public boolean isBetterOrEqualTo(Result that)
    {
        return ordinal() <= that.ordinal();
    }

}
