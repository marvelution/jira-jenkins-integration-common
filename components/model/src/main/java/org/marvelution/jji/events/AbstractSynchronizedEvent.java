/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.events;

import java.time.LocalDateTime;
import java.util.Objects;

import org.marvelution.jji.model.Syncable;

import com.fasterxml.jackson.annotation.JsonIgnore;

public abstract class AbstractSynchronizedEvent<S extends Syncable<S>>
{
    private S syncable;
    private LocalDateTime timestamp;

    AbstractSynchronizedEvent(S syncable)
    {
        this(syncable, LocalDateTime.now());
    }

    AbstractSynchronizedEvent(
            S syncable,
            LocalDateTime timestamp)
    {
        this.syncable = syncable.copy();
        this.timestamp = timestamp;
    }

    @JsonIgnore
    public S getSyncable()
    {
        return syncable;
    }

    public LocalDateTime getTimestamp()
    {
        return timestamp;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(syncable);
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (!(o instanceof AbstractSynchronizedEvent))
        {
            return false;
        }
        AbstractSynchronizedEvent<?> that = (AbstractSynchronizedEvent<?>) o;
        return syncable.equals(that.syncable);
    }
}
