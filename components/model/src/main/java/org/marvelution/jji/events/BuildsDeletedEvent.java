/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.events;

import java.util.Objects;

import org.marvelution.jji.model.Job;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BuildsDeletedEvent
{

    private final Job job;

    public BuildsDeletedEvent(
            @JsonProperty("job")
            Job job)
    {
        this.job = job;
    }

    public Job getJob()
    {
        return job;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(job);
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (!(o instanceof BuildsDeletedEvent))
        {
            return false;
        }
        BuildsDeletedEvent that = (BuildsDeletedEvent) o;
        return job.equals(that.job);
    }
}
