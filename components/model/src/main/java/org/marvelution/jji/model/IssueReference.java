/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.model;

import java.net.URI;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.StringJoiner;
import jakarta.annotation.Nullable;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.marvelution.jji.adapters.javax.URIAdapter;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@javax.xml.bind.annotation.XmlRootElement
@javax.xml.bind.annotation.XmlAccessorType(javax.xml.bind.annotation.XmlAccessType.FIELD)
public class IssueReference
{

    private String projectKey;
    private String issueKey;
    @XmlJavaTypeAdapter(org.marvelution.jji.adapters.URIAdapter.class)
    @javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter(URIAdapter.class)
    private URI issueUrl;
    @Nullable
    private Set<Field> fields;

    public String getProjectKey()
    {
        return projectKey;
    }

    public IssueReference setProjectKey(String projectKey)
    {
        this.projectKey = projectKey;
        return this;
    }

    public String getIssueKey()
    {
        return issueKey;
    }

    public IssueReference setIssueKey(String issueKey)
    {
        this.issueKey = issueKey;
        return this;
    }

    public URI getIssueUrl()
    {
        return issueUrl;
    }

    public IssueReference setIssueUrl(URI issueUrl)
    {
        this.issueUrl = issueUrl;
        return this;
    }

    public Set<Field> getFields()
    {
        if (fields == null)
        {
            fields = new HashSet<>();
        }
        return fields;
    }

    public IssueReference setFields(Set<Field> fields)
    {
        this.fields = new HashSet<>(fields);
        return this;
    }

    public IssueReference addField(
            String key,
            String name,
            String value)
    {
        getFields().add(new Field().setKey(key)
                .setName(name)
                .setValue(value));
        return this;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(projectKey, issueKey);
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }
        IssueReference that = (IssueReference) o;
        return Objects.equals(projectKey, that.projectKey) && Objects.equals(issueKey, that.issueKey);
    }

    @Override
    public String toString()
    {
        return new StringJoiner(", ", IssueReference.class.getSimpleName() + "[", "]").add("projectKey='" + projectKey + "'")
                .add("issueKey='" + issueKey + "'")
                .toString();
    }

    @XmlRootElement
    @XmlAccessorType(XmlAccessType.FIELD)
    public static class Field
    {
        private String key;
        private String name;
        private String value;

        public String getKey()
        {
            return key;
        }

        public Field setKey(String key)
        {
            this.key = key;
            return this;
        }

        public String getName()
        {
            return name;
        }

        public Field setName(String name)
        {
            this.name = name;
            return this;
        }

        public String getValue()
        {
            return value;
        }

        public Field setValue(String value)
        {
            this.value = value;
            return this;
        }

        @Override
        public int hashCode()
        {
            return Objects.hash(key, name, value);
        }

        @Override
        public boolean equals(Object o)
        {
            if (this == o)
            {
                return true;
            }
            if (!(o instanceof Field field))
            {
                return false;
            }
            return Objects.equals(key, field.key) && Objects.equals(name, field.name) && Objects.equals(value, field.value);
        }

        @Override
        public String toString()
        {
            return new StringJoiner(", ", Field.class.getSimpleName() + "[", "]").add("key='" + key + "'")
                    .add("name='" + name + "'")
                    .add("value='" + value + "'")
                    .toString();
        }
    }
}
