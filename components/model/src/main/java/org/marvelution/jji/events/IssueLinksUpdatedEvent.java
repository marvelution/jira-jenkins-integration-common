/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.events;

import java.util.Objects;
import java.util.Set;
import java.util.StringJoiner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class IssueLinksUpdatedEvent
{

    private final String buildId;
    private final Set<String> issueKeys;

    public IssueLinksUpdatedEvent(
            String buildId,
            String... issueKeys)
    {
        this(buildId,
                Stream.of(issueKeys)
                        .collect(Collectors.toSet()));
    }

    @JsonCreator
    public IssueLinksUpdatedEvent(
            @JsonProperty("buildId")
            String buildId,
            @JsonProperty("issueKeys")
            Set<String> issueKeys)
    {
        this.buildId = buildId;
        this.issueKeys = issueKeys;
    }

    public String getBuildId()
    {
        return buildId;
    }

    public Set<String> getIssueKeys()
    {
        return issueKeys;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(buildId, issueKeys);
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }
        IssueLinksUpdatedEvent that = (IssueLinksUpdatedEvent) o;
        return Objects.equals(buildId, that.buildId) && issueKeys.equals(that.issueKeys);
    }

    @Override
    public String toString()
    {
        return new StringJoiner(", ", IssueLinksUpdatedEvent.class.getSimpleName() + "[", "]").add("buildId='" + buildId + "'")
                .add("issueKeys=" + issueKeys)
                .toString();
    }
}
