/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.model.request;

import java.util.Collections;
import java.util.Optional;
import java.util.function.Function;

import org.marvelution.jji.model.Job;

import org.apache.commons.lang3.StringUtils;

public class WorkflowPostFunctionConfiguration
        extends TriggerBuildsRequest
{
    private Job specificJob;

    public Job getSpecificJob()
    {
        return specificJob;
    }

    public WorkflowPostFunctionConfiguration setSpecificJob(Job specificJob)
    {
        this.specificJob = specificJob;
        return this;
    }

    static boolean isForSpecificJob(String config)
    {
        return StringUtils.isNotBlank(config) && !isByIssueField(config) && !LINKED.equalsIgnoreCase(config) &&
               !BY_CONVENTION.equalsIgnoreCase(config);
    }

    static boolean isByIssueField(String config)
    {
        return config != null && config.startsWith(BY_ISSUE_FIELD) && config.length() > BY_ISSUE_FIELD.length() + 2;
    }

    public static WorkflowPostFunctionConfiguration parse(
            String issueKey,
            String configuration)
    {
        return parse(issueKey, configuration, id -> Optional.empty());
    }

    public static WorkflowPostFunctionConfiguration parse(
            String issueKey,
            String configuration,
            Function<String, Optional<Job>> jobResolver)
    {
        WorkflowPostFunctionConfiguration request = new WorkflowPostFunctionConfiguration();
        request.setIssueKey(issueKey);
        if (LINKED.equalsIgnoreCase(configuration))
        {
            request.setLinked();
        }
        else if (BY_CONVENTION.equalsIgnoreCase(configuration))
        {
            request.setByConvention();
        }
        else if (isForSpecificJob(configuration))
        {
            request.setJobs(Collections.singleton(configuration));
            request.setSpecificJob(jobResolver.apply(configuration)
                    .orElse(new Job().setId(configuration)
                            .setDisplayName("Unknown Job " + configuration)));
        }
        else if (isByIssueField(configuration))
        {
            int dollar = configuration.indexOf("$");
            if (dollar > 0)
            {
                request.setByIssueField(configuration.substring(BY_ISSUE_FIELD.length() + 2, dollar));
                request.setByIssueFieldMapping(configuration.substring(dollar));
            }
            else
            {
                request.setByIssueField(configuration.substring(BY_ISSUE_FIELD.length() + 2));
            }
            request.setExactMatch(configuration.indexOf(";;") == BY_ISSUE_FIELD.length());
        }
        return request;
    }

    public static WorkflowPostFunctionConfiguration blank()
    {
        WorkflowPostFunctionConfiguration request = new WorkflowPostFunctionConfiguration();
        request.setLinked();
        return request;
    }
}
