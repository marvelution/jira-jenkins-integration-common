/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.model.request;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@javax.xml.bind.annotation.XmlRootElement
@javax.xml.bind.annotation.XmlAccessorType(javax.xml.bind.annotation.XmlAccessType.FIELD)
public class ConnectSiteRequest
{

    private ConnectMethod method;
    private String adminUser;
    private String adminToken;

    public ConnectMethod getMethod()
    {
        return method;
    }

    public ConnectSiteRequest setMethod(ConnectMethod method)
    {
        this.method = method;
        return this;
    }

    public String getAdminUser()
    {
        return adminUser;
    }

    public ConnectSiteRequest setAdminUser(String adminUser)
    {
        this.adminUser = adminUser;
        return this;
    }

    public String getAdminToken()
    {
        return adminToken;
    }

    public ConnectSiteRequest setAdminToken(String adminToken)
    {
        this.adminToken = adminToken;
        return this;
    }

    public enum ConnectMethod
    {
        automatic,
        manual,
        casc;
    }
}
