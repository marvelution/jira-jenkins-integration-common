/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.utils;

import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.marvelution.jji.model.Build;
import org.marvelution.jji.model.ChangeSet;
import org.marvelution.jji.model.Job;
import org.marvelution.jji.model.Site;

import org.apache.commons.lang3.StringUtils;

import static java.util.regex.Pattern.CASE_INSENSITIVE;
import static java.util.regex.Pattern.compile;

public final class KeyExtractor
{

    private static final String SEPARATOR = "[\\s\\p{Punct}]";
    private static final String KEY_PREFIX_REGEX = "(?:(?<=" + SEPARATOR + ")|^)"; //zero-width positive lookbehind
    private static final String KEY_BODY_REGEX = "(((?:\\p{Lu}[\\p{Lu}\\p{Digit}_]+|\\p{Ll}[\\p{Ll}\\p{Digit}_]+))-\\p{Digit}+)";
    private static final String KEY_POSTFIX_REGEX = "(?:(?=" + SEPARATOR + ")|$)";  //zero-width positive lookahead
    private static final String ISSUE_KEY_REGEX = KEY_PREFIX_REGEX + KEY_BODY_REGEX + KEY_POSTFIX_REGEX;
    private static final Pattern ISSUE_KEY_PATTERN = compile(ISSUE_KEY_REGEX, CASE_INSENSITIVE);
    private static final Pattern PROJECT_KEY_PATTERN = compile(KEY_BODY_REGEX, CASE_INSENSITIVE);

    private KeyExtractor()
    {
    }

    public static Set<String> extractIssueKeys(Job job)
    {
        Set<String> issueKeys = extractIssueKeys(job.getDisplayName(), job.getDescription(), job.getUrlName());
        return limitByScope(job.getSite(), issueKeys);
    }

    public static Set<String> extractIssueKeys(Build build)
    {
        return extractIssueKeys(build, build.getJob());
    }

    public static Set<String> extractIssueKeys(
            Build build,
            Job job)
    {
        Set<String> issueKeys = new HashSet<>();
        issueKeys.addAll(extractIssueKeys(job));
        issueKeys.addAll(extractIssueKeys(build.getDisplayName(), build.getDescription(), build.getCause()));
        for (String branch : build.getBranches())
        {
            issueKeys.addAll(extractIssueKeys(branch));
        }
        for (ChangeSet changeSet : build.getChangeSet())
        {
            issueKeys.addAll(extractIssueKeys(changeSet.getMessage()));
        }
        return limitByScope(job.getSite(), issueKeys);
    }

    private static Set<String> limitByScope(
            Site site,
            Set<String> issueKeys)
    {
        if (site != null && StringUtils.isNotBlank(site.getScope()))
        {
            String prefix = site.getScope() + "-";
            return issueKeys.stream()
                    .filter(key -> key.startsWith(prefix))
                    .collect(Collectors.toSet());
        }
        else
        {
            return issueKeys;
        }
    }

    public static Set<String> extractIssueKeys(String... messages)
    {
        Set<String> matches = new HashSet<>();
        if (messages != null)
        {
            for (String message : messages)
            {
                if (StringUtils.isNotBlank(message))
                {
                    Matcher match = ISSUE_KEY_PATTERN.matcher(message);
                    while (match.find())
                    {
                        matches.add(match.group(1)
                                .toUpperCase(Locale.ENGLISH));
                    }
                }
            }
        }
        return matches;
    }

    public static String extractProjectKeyFromIssueKey(String issueKey)
    {
        Matcher match = PROJECT_KEY_PATTERN.matcher(issueKey);
        if (match.find())
        {
            return match.group(2);
        }
        else if (issueKey.lastIndexOf('-') != -1)
        {
            return issueKey.substring(0, issueKey.lastIndexOf('-'));
        }
        else
        {
            throw new IllegalArgumentException("'" + issueKey + "' is not a valid issue key");
        }
    }

}
