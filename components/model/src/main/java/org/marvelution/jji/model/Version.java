/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.model;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.annotation.Nonnull;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@javax.xml.bind.annotation.XmlRootElement
@javax.xml.bind.annotation.XmlAccessorType(javax.xml.bind.annotation.XmlAccessType.FIELD)
public class Version
        implements Comparable<Version>
{

    public static final Version ZERO = new Version(0, 0, 0);
    private static final Pattern VERSION_PATTERN = Pattern.compile("(\\d+(\\.\\d+)*).*");
    @XmlElement
    @javax.xml.bind.annotation.XmlElement
    private final int major;
    @XmlElement
    @javax.xml.bind.annotation.XmlElement
    private final int minor;
    @XmlElement
    @javax.xml.bind.annotation.XmlElement
    private final int micro;

    public Version(
            int major,
            int minor,
            int micro)
    {
        this.major = major;
        this.minor = minor;
        this.micro = micro;
    }

    public int getMajor()
    {
        return this.major;
    }

    public int getMinor()
    {
        return this.minor;
    }

    public int getMicro()
    {
        return this.micro;
    }

    public boolean isOlder(
            @Nonnull
            Version other)
    {
        return compareTo(other) < 0;
    }

    public boolean isNewer(
            @Nonnull
            Version other)
    {
        return compareTo(other) > 0;
    }

    @Override
    public int compareTo(
            @Nonnull
            Version other)
    {
        if (other == this)
        {
            return 0;
        }

        int diff = major - other.major;
        if (diff == 0)
        {
            diff = minor - other.minor;
            if (diff == 0)
            {
                diff = micro - other.micro;
            }
        }
        return diff;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(major, minor, micro);
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }
        Version version = (Version) o;
        return major == version.major && minor == version.minor && micro == version.micro;
    }

    @XmlElement(name = "formatted")
    @javax.xml.bind.annotation.XmlElement(name = "formatted")
    public String toString()
    {
        return this.major + "." + this.minor + "." + this.micro;
    }

    public static Version of(
            @Nonnull
            String version)
    {
        Matcher matcher = VERSION_PATTERN.matcher(version);
        if (matcher.matches())
        {
            String[] parts = StringUtils.split(matcher.group(1), ".");
            int major = parts.length > 0 ? NumberUtils.toInt(parts[0]) : 0;
            int minor = parts.length > 1 ? NumberUtils.toInt(parts[1]) : 0;
            int micro = parts.length > 2 ? NumberUtils.toInt(parts[2]) : 0;
            return new Version(major, minor, micro);
        }
        else
        {
            throw new IllegalArgumentException("Illegal version " + version);
        }
    }
}
