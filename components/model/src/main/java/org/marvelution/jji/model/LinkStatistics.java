/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.model;

import java.util.*;
import java.util.function.Function;
import javax.annotation.Nullable;
import jakarta.json.bind.annotation.JsonbCreator;
import jakarta.json.bind.annotation.JsonbProperty;
import jakarta.json.bind.annotation.JsonbPropertyOrder;

import static java.util.stream.Collectors.toMap;

@JsonbPropertyOrder({"total",
        "buildTotal",
        "worstResult",
        "successRate",
        "latestBuild",
        "worstBuild",
        "countByResult",
        "deployTotal",
        "worstDeployResult",
        "deploySuccessRate",
        "latestDeploy",
        "worstDeploy",
        "deployCountByResult"})
public class LinkStatistics
{

    private long total;
    private long buildTotal;
    private Result worstResult;
    private Map<Result, Long> countByResult;
    private double successRate;
    private BuildStats latestBuild;
    private BuildStats worstBuild;
    private long deployTotal;
    private Result worstDeployResult;
    private DeployStats latestDeploy;
    private DeployStats worstDeploy;
    private Map<Result, Long> deployCountByResult;
    private double deploySuccessRate;

    public LinkStatistics()
    {
        worstResult = Result.SUCCESS;
        worstDeployResult = Result.SUCCESS;
        countByResult = new EnumMap<>(Arrays.stream(Result.values())
                .collect(toMap(Function.identity(), result -> 0L)));
        deployCountByResult = new EnumMap<>(Arrays.stream(Result.values())
                .collect(toMap(Function.identity(), result -> 0L)));
    }

    public Result getWorstResult()
    {
        return worstResult;
    }

    public LinkStatistics setWorstResult(Result worstResult)
    {
        this.worstResult = worstResult;
        return this;
    }

    public long getTotal()
    {
        return total;
    }

    public LinkStatistics setTotal(long total)
    {
        this.total = total;
        return this;
    }

    public long getBuildTotal()
    {
        return buildTotal;
    }

    public LinkStatistics setBuildTotal(long buildTotal)
    {
        this.buildTotal = buildTotal;
        return this;
    }

    public Map<Result, Long> getCountByResult()
    {
        if (countByResult == null)
        {
            countByResult = new EnumMap<>(Result.class);
        }
        return countByResult;
    }

    public LinkStatistics setCountByResult(Map<Result, Long> countByResult)
    {
        this.countByResult = new EnumMap<>(countByResult);
        successRate = calculateSuccessRate(getCountByResult());
        return this;
    }

    public LinkStatistics addCountByResult(
            Result result,
            long count)
    {
        getCountByResult().put(result, count);
        successRate = calculateSuccessRate(getCountByResult());
        return this;
    }

    public double getSuccessRate()
    {
        return successRate;
    }

    @Nullable
    public BuildStats getLatestBuild()
    {
        return latestBuild;
    }

    public LinkStatistics setLatestBuild(BuildStats latestBuild)
    {
        this.latestBuild = latestBuild;
        return this;
    }

    @Nullable
    public BuildStats getWorstBuild()
    {
        return worstBuild;
    }

    public LinkStatistics setWorstBuild(BuildStats worstBuild)
    {
        this.worstBuild = worstBuild;
        if (worstResult == null || (worstBuild != null && worstBuild.getResult()
                .isWorseThan(worstResult)))
        {
            worstResult = worstBuild.result;
        }
        return this;
    }

    public long getDeployTotal()
    {
        return deployTotal;
    }

    public LinkStatistics setDeployTotal(long deployTotal)
    {
        this.deployTotal = deployTotal;
        return this;
    }

    public Result getWorstDeployResult()
    {
        return worstDeployResult;
    }

    public LinkStatistics setWorstDeployResult(Result worstDeployResult)
    {
        this.worstDeployResult = worstDeployResult;
        return this;
    }

    public DeployStats getLatestDeploy()
    {
        return latestDeploy;
    }

    public LinkStatistics setLatestDeploy(DeployStats latestDeploy)
    {
        this.latestDeploy = latestDeploy;
        return this;
    }

    public DeployStats getWorstDeploy()
    {
        return worstDeploy;
    }

    public LinkStatistics setWorstDeploy(DeployStats worstDeploy)
    {
        this.worstDeploy = worstDeploy;
        if (worstDeployResult == null || (worstDeploy != null && worstDeploy.getResult()
                .isWorseThan(worstDeployResult)))
        {
            worstDeployResult = worstDeploy.result;
        }
        return this;
    }

    public Map<Result, Long> getDeployCountByResult()
    {
        if (deployCountByResult == null)
        {
            deployCountByResult = new EnumMap<>(Result.class);
        }
        return deployCountByResult;
    }

    public LinkStatistics setDeployCountByResult(Map<Result, Long> deployCountByResult)
    {
        this.deployCountByResult = deployCountByResult;
        deploySuccessRate = calculateSuccessRate(getDeployCountByResult());
        return this;
    }

    public LinkStatistics addDeployCountByResult(
            Result result,
            long count)
    {
        getDeployCountByResult().put(result, count);
        deploySuccessRate = calculateSuccessRate(getDeployCountByResult());
        return this;
    }

    public double getDeploySuccessRate()
    {
        return deploySuccessRate;
    }

    private double calculateSuccessRate(Map<Result, Long> results)
    {
        double successCount = results.getOrDefault(Result.SUCCESS, 0L);
        double totalCount = results.values()
                .stream()
                .mapToLong(v -> v)
                .sum();
        if (totalCount == 0L || (successCount == 0L && totalCount > 0L))
        {
            return 0L;
        }
        else if (successCount == totalCount)
        {
            return 100L;
        }
        else
        {
            return successCount / totalCount * 100;
        }
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(total,
                buildTotal,
                worstResult,
                countByResult,
                latestBuild,
                worstBuild,
                deployTotal,
                latestDeploy,
                worstDeploy,
                deployCountByResult);
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }
        LinkStatistics that = (LinkStatistics) o;
        return total == that.total && buildTotal == that.buildTotal && worstResult == that.worstResult &&
               countByResult.equals(that.countByResult) && Objects.equals(latestBuild, that.latestBuild) && Objects.equals(worstBuild,
                that.worstBuild) && deployTotal == that.deployTotal && worstDeployResult == that.worstDeployResult && Objects.equals(
                latestDeploy,
                that.latestDeploy) && Objects.equals(worstDeploy, that.worstDeploy) && Objects.equals(deployCountByResult,
                that.deployCountByResult);
    }

    @Override
    public String toString()
    {
        return new StringJoiner(", ", LinkStatistics.class.getSimpleName() + "[", "]").add("total=" + total)
                .add("buildTotal=" + buildTotal)
                .add("worstResult=" + worstResult)
                .add("countByResult=" + countByResult)
                .add("latestBuild=" + latestBuild)
                .add("worstBuild=" + worstBuild)
                .add("deployTotal=" + deployTotal)
                .add("worstDeployResult=" + worstDeployResult)
                .add("deployCountByResult=" + deployCountByResult)
                .add("latestDeploy=" + latestDeploy)
                .add("worstDeploy=" + worstDeploy)
                .toString();
    }

    public static class BuildStats
    {

        protected String id;
        protected long timestamp;
        protected Result result;

        @JsonbCreator
        public BuildStats(
                @JsonbProperty
                String id,
                @JsonbProperty
                long timestamp,
                @JsonbProperty
                Result result)
        {
            this.id = id;
            this.timestamp = timestamp;
            this.result = result;
        }

        public static BuildStats forBuild(Build build)
        {
            return new BuildStats(build.getId(), build.getTimestamp(), build.getResult());
        }

        public String getId()
        {
            return id;
        }

        public long getTimestamp()
        {
            return timestamp;
        }

        public Result getResult()
        {
            return result;
        }

        @Override
        public int hashCode()
        {
            return Objects.hash(id, timestamp, result);
        }

        @Override
        public boolean equals(Object o)
        {
            if (this == o)
            {
                return true;
            }
            if (o == null || getClass() != o.getClass())
            {
                return false;
            }
            BuildStats that = (BuildStats) o;
            return timestamp == that.timestamp && id.equals(that.id) && result == that.result;
        }

        @Override
        public String toString()
        {
            return new StringJoiner(", ", BuildStats.class.getSimpleName() + "[", "]").add("id='" + id + "'")
                    .add("timestamp=" + timestamp)
                    .add("result=" + result)
                    .toString();
        }
    }

    public static class DeployStats
            extends BuildStats
    {

        private String deploymentId;
        private String name;
        private DeploymentEnvironmentType type;

        @JsonbCreator
        public DeployStats(
                @JsonbProperty
                String id,
                @JsonbProperty
                long timestamp,
                @JsonbProperty
                Result result,
                @JsonbProperty
                String deploymentId,
                @JsonbProperty
                String name,
                @JsonbProperty
                DeploymentEnvironmentType type)
        {
            super(id, timestamp, result);
            this.deploymentId = deploymentId;
            this.name = name;
            this.type = type;
        }

        public static DeployStats forDeployment(
                Build build,
                DeploymentEnvironment deploymentEnvironment)
        {
            return new DeployStats(build.getId(),
                    build.getTimestamp(),
                    build.getResult(),
                    deploymentEnvironment.getId(),
                    deploymentEnvironment.getName(),
                    deploymentEnvironment.getType());
        }

        public String getDeploymentId()
        {
            return deploymentId;
        }

        public String getName()
        {
            return name;
        }

        public DeploymentEnvironmentType getType()
        {
            return type;
        }

        @Override
        public int hashCode()
        {
            return Objects.hash(super.hashCode(), name, type);
        }

        @Override
        public boolean equals(Object o)
        {
            if (this == o)
            {
                return true;
            }
            if (o == null || getClass() != o.getClass())
            {
                return false;
            }
            if (!super.equals(o))
            {
                return false;
            }
            DeployStats that = (DeployStats) o;
            return deploymentId.equals(that.deploymentId) && name.equals(that.name) && type == that.type;
        }

        @Override
        public String toString()
        {
            return new StringJoiner(", ", DeployStats.class.getSimpleName() + "[", "]").add("deploymentId='" + deploymentId + "'")
                    .add("name='" + name + "'")
                    .add("type=" + type)
                    .add("id='" + id + "'")
                    .add("timestamp=" + timestamp)
                    .add("result=" + result)
                    .toString();
        }
    }
}
