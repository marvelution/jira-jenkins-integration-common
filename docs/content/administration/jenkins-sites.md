---
title: "Jenkins Sites"
date: 2020-07-01T14:07:00Z
draft: false

aliases:
  - features/jira-features/jenkins-configuration/manage-sites/

menu:
  docs:
    parent: administration
    weight: 3
---

Jenkins sites are configured in Jira and determine which jobs and builds are synchronized to Jira.

## Site Types

There are different types of site that can be added:

### Accessible

This is a site that the Jira app can directly connect with to support bidirectional integration between Jira and Jenkins.

{{% cloud %}}
### Accessible Through Tunneling

This is a site that the Jira app will set up a secure tunnel to connect with to support bidirectional integration between Jira and 
Jenkins without making the Jenkins site directly accessible to the Jira app. 
{{% /cloud %}}

### Inaccessible

This is a site that the Jira app is not able to connect with for data synchronization, either because of a firewall or other 
network limitations. This limits the integration to be uni-directional, where Jenkins will push data to the Jira app for processing.


## Site Scopes

Jenkins sites can be managed in two scopes:

### Global

Jenkins sites configured by a Jira Site Administrator can be used by all projects within the Jira instance.
To manage Jenkins global sites, navigate to **Settings** > **Apps** > **Jenkins Integration** > **Manage Sites**.

### Project

Jenkins sites configured by a Jira Project Administrator can be used by the project where the site was configured for.
To manage Jenkins project sites, navigate to {{% cloud %}}**Project Settings** > **Apps** > 
**Jenkins Integration**{{% /cloud %}}{{% server %}}**Project Settings** > **Jenkins Integration**{{% /server %}}

{{% tip %}}Jira Site Administrators can update the scope of a Jenkins site in both directions, global to project and project to
global.{{% /tip %}}

## Add a Site

When on the **Manage Sites** page, click on the **Connect a Site** button.
This brings up the **Connect a Site** wizard.
Follow the steps in the wizard to create and connect the Site.
{{< include "add-site" >}}

## Manage Sites

### Edit a Site

To edit the details of a site, like name, authentication details, etc, simply click on the **Edit** button in the actions column. This will
 show the Site edit form allowing you to update the details of the Site.

### Delete a Site

To delete a Site, simply click the Delete button in the actions column and click the Delete button to delete the site.

{{< warning >}}This action is not reversible, and only the jobs and builds that are still available on the Site itself can be restored
 when the site is added at a later state.{{</ warning >}}

### Synchronize a Site

To synchronize a Site and all enabled jobs on it, simply click on the Synchronize button in the actions column.

{{< note >}}This action is not available for Inaccessible sites.{{</ note >}}

### Cleanup Jobs

To clean up job and build data of a site, simply click on the Cleanup Jobs action in the actions dropdown. This will not delete data on
 Jenkins, but will only delete jobs and builds that are marked as deleted in Jira.

{{< warning >}}This action is not reversible, and only the jobs and builds that are still available on the Site itself can be restored
 when the site is added at a later state.{{</ warning >}}

### Enabled for Synchronization

To quickly disable or enable an entire site for synchronization, simply click on the Enabled for Synchronization action in the actions
dropdown. Alternatively this can also be done using the Site Edit action.

### Auto Enable New Jobs

To quickly enable or disable the Auto Enable new Jobs setting for a Site, simply click on the Auto Enable new Jobs action in the actions
 dropdown. Alternatively this can also be done using the Site Edit action.
         
### Manage Connection

To show the manage connection wizard of a site, simply click on the 'Manage Connection' action in the actions dropdown (If your site is
 behind a firewall then this button is placed top-level action). This information can be used to (re)register the Jira site with Jenkins.
 And also to configure the integration using the Jenkins Configuration as Code feature.

## Manage Jobs

The Manage Jobs site action allows you too quickly perform multiple operations, see Manage Jobs, on multiple jobs, simply click on the
 Manage Jobs action in the actions dropdown.
 
{{< tip >}}
Job actions can be done in bulk on through the Site actions menu, can can be done for each job individually through the job actions menu.
{{</ tip >}}

### Enable/Disable a Job

Each job can be enabled or disabled in the synchronization process. Use the toggle button in front of the job name, or on top of the page, 
to enable or disable it.

{{< note >}}
This does not have any effect on the displaying of builds on panels. All the builds of disabled jobs will be shown in Jenkins Build Panels.
To "hide" these build you will have to delete them, see Clear Build Cache.
{{</ note >}}

### Synchronize a Job

Jobs are automatically synchronized when they trigger a completed build using the Jira Build Notification Listener feature. But this
 process can be forced for a job by clicking on the Manage button in the actions column, followed by clicking on Synchronize in the
 actions column of a job.

{{< note >}}This option is not available if the site of the job is inaccessible.{{</ note >}}

### Clear Build Cache

To remove all the synchronised builds of a Job, simply click on ... and select Clear Build Cache 

{{< warning >}}
The app can only restore the builds from Jenkins that are still available. Builds that are also deleted on Jenkins (cannot be
done via this app) cannot be synchronised anymore.
{{</ warning >}}

### Rebuild Build Cache

To rebuild a build cache of a Job, simply click on ... and select Rebuild Build Cache.
This will mark all the builds of the selected Job as Deleted, reset the last synchronised build to 0, and will then trigger the
 synchronisation process for the selected Job. 
