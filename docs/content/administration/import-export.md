---
title: "Export and Import"
date: 2022-01-31T00:00:00Z
draft: false

menu:
  docs:
    parent: administration
    weight: 5
---

Data of the app can be imported and exported to facilitate a full migrating to a different Jira instance, or only for copying parts of the
configuration.

{{< cloud >}}
{{< note >}}
Migration history is only available for 5 days after creation.
{{< /note >}}
{{< /cloud >}}

## Export

The export table lists the exports you have performed and there status. An export is ready to be downloaded once it has the status
`SUCCSES`. To download the export file, click on the export name to show the details of the export, among the details you will see the
download link to download the export to your local system. Once downloaded you can use the file to import the selected data.

To export app data, click on the 'Export Data' button in the top right to open the export data dialog, then:

1. Specify a name to distinguish the export from other exports.
2. Select what data should be included in the export.
    * Configuration - includes the app configuration.
    * Integration - includes all Jenkins sites, there jobs, builds and issue links.
    * Automation - includes all automation rules and the rule execution history for each rule.
3. Finally click on 'Export' to export the data.

## Import

{{% cloud %}}
Migrations via the Jira Cloud Migration Assistant are also listed in the table of imports, see also
 [Migrations]({{< relref "/administration/migration" >}}).
{{% /cloud %}}

The import table lists the imports you have performed and there status.

To import app data, click on the 'Import Data' button in the top right to open the import data dialog, then:

1. Specify a name to distinguish the imports from other imports.
2. Specify the app data file to import.
3. Select whether the import process should start with a blank state or not. By checking this, the import process will first clear the
   app data of the selected parts from the instance before importing the new data.
4. Select what data should be included in the import.
    * Configuration - includes the app configuration.
    * Integration - includes all Jenkins sites, there jobs, builds and issue links.
    * Automation - includes all automation rules and the rule execution history for each rule.
5. Finally click on 'Import' to import the data.
