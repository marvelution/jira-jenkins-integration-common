---
title: "Jira Sites"
date: 2020-07-01T14:07:00Z
draft: false

aliases:
  - features/jenkins-features/jira-site-management/

menu:
  docs:
    parent: administration
    weight: 4
---

Jira sites are configured in Jenkins and determine which Jira instances get notified on build completions and other interesting events
 that the Jenkins for Jira app need to provide its features.

Any Jira site registered will also be used to annotate Jira issue keys when viewing changesets of a build, providing links into Jira to
 view the issue.

There are two ways a Jira site can be registered with Jenkins.

## Automatic Registration

Automatic registration is done through the Jira and can be used to (re)register a Jira site with the Jenkins site. A requirement is that
 the Jenkins site is configured as a public site in Jira.
 
Automatic registration can be triggered via the [Site Configuration]({{< relref "/administration/jenkins-sites#manage-connection" >}}) dialog, 
next to links for automatic registration this dialog also contains the registration token and secret to perform a manual registration. 

## Manual Registration

Manual registration of a Jira site can be done in Jenkins to navigating to **Jira Site Management** page in the Jenkins Management section.
Alternatively, you can also open the [Site Configuration]({{< relref "/administration/jenkins-sites#manage-connection" >}}) dialog and click on 
the **Manually register with Site** link. This link will take you to the same Jira Site Management page in Jenkins.

There you can use the **Registration Token** and **Token Secret** from the 
[Site Configuration]({{< relref "/administration/jenkins-sites#manage-connection" >}}) dialog to fill in the **Manually Register a Jira Site** 
form.  
