---
title: "Configuration and Features"
date: 2020-07-01T14:07:00Z
draft: false

aliases:
  - features/jira-features/jenkins-configuration/configuration-and-features/

menu:
  docs:
    parent: administration
    weight: 2
---

The behaviour of the Jenkins for Jira app can be customized through configuration and features.
Features are essential on or off configuration settings, whereas other configuration settings can contain other bit of information as
well.

To customize the configuration of the app, navigate to **Settings** > **Apps** > **Jenkins Integration** > **Configuration**.
Once on the configuration page is open you can access the **Features** dialog by clicking on the **Features** button.

Configuration settings marked with an astrix (*) can be customized per project. To do this, navigate to
{{% cloud %}}**Project Settings** > **Apps** > **Jenkins Integration** and select the **Configuration** tab.{{% /cloud %}}{{% server %}}**Project Settings** >
**Jenkins Integration** and select the **Configuration** tab.{{% /server %}}

## Configuration

The configuration page is split into multiple sections, each logically grouping settings.

### Maximum Builds per Page *

This configuration setting allows you to limit the number of builds that are shown on a Jenkins Builds panel, defaults to **100**.

{{% cloud %}}
### Date Format

This configuration setting allows you to configure the format used to make date and time strings human-readable. Documentation on
date / time formats can be found [online](http://download.oracle.com/javase/8/docs/api/index.html?java/text/SimpleDateFormat.html).
{{% /cloud %}}

### User Id vs Display Name *

Some feature of the integration require user information, like triggering a build send who triggered the build to the Jenkins site.
This setting allows you to choose whether the user's display name or id is used.

### Parameter Issue Fields *

Selected issue fields are passed to Jenkins when triggering a build. The Jira Integration plugin will match parameters to actual job
parameters. This matching is cas-insensitive. For example, if the **Fix versions** field in enabled, Jira sends a parameter named
**Fix_versions** and Jenkins will match **Fix_versions**, **fix_versions** and **FIX_VERSIONS**.

Issue fields are only send as parameters if the field contains a value for issue where the build was triggered from, and any user field
will respect the **User Id vs Display Name** setting.

### Parameter Issue Field Names *

By default the Parameter names are created by taking the issue field name and replacing any non alphanumeric character with an underscore.
Names allow you to specify a specific parameter name to be used for an issue field. Each name needs to be specified on a new line using 
format **field_id=name** for example **components=parts** or **fixVersions=releases**.

### Parameter Issue Field Mappers *

Natively the parameter issue fields support the creator, assignee, reporter, labels, components, affects and fix versions fields. It can
also handle text fields. Custom fields especially can require custom mappings to obtain the value that should be sent to Jenkins when
triggering a build. Mappers allow you to specify a specific json path to be used to obtain the value of a field. Each mapper needs
to be specified on a new line using format **field_id=json_path** for example **customfield_12345=$.\[\*].name** or **project=$.name**.
Each field can only contain a single String value, if the json path returns multiple values, then they are concatenated using a comma.

Custom mappers take priority over predefined mappers, so using a mapper like **project=$.id** to send the project id instead of the
project name that is send by default.

To check custom mappers, you can provide an issue key to evaluate the parameter fields and mappers.

The Jayway JsonPath implementation is used, refer to its [documentation](https://github.com/json-path/JsonPath/blob/master/README.md) for
details on available Operators, Functions, Filter Operators and some examples.

### Data Retention Period

Retention period, in months, how long jobs and builds that are marked as deleted will be kept in the build cache, defaults to **12**.

Set to **0** to have a data retention period of 1 day.

Jobs are only cleaned up from the build cache if the job itself is marked as deleted, and the latest build in the cache is older than the
retention period.
Builds are cleaned up from the build cache if the build is marked as deleted, and the timestamp of the build is older than the retention
period.

### Audit Data Retention Period

Retention period, in months, how long audit data, like synchronization and rule execution logs, will be kept, defaults to **1**.

Set to **0** to have a data retention period of 1 day.

### Enabled Projects

This configuration setting allows you to restrict the features of the app to only the selected projects.

{{< info >}}
Leave this setting blank to enable all projects.
{{< /info >}}

{{< note >}}
Disabling a project will result in the deletion of all relations of builds to issues within that project!
{{< /note >}}

{{% server %}}
### Jira Sync URL

This configuration setting allows you to change the sync URL that Jenkins plugin will use to communicate with Jira. By default, this will
be the same as the application URL that is configured in **Administration** > **System** > **General Configuration**. This can be useful if
your Jenkins instance should bypass a proxy in order to connect to Jira.

### Rule Actor

This configuration setting specifies the user that should be used when the rules engines interacts with Jira, to get available projects
and issue types, to create, update or transition an issue, etc.

Not setting this may lead to rule failing with a message stating that you don't have to required permissions to perform the rule action.
{{% /server %}}

## Features

There are two types of features, system and site, System features are features that are either enabled or disabled by the app itself
and cannot be customized. Site features however are features that administrators of a Jira site can enable or disable to customize the
behaviour of the app.

### System Features

**data-migration**
Migrate data from Jira Data Center/Server to Jira Cloud.

**jira-jenkins-automation**
Trigger rule executions based on synchronization events.

{{% cloud %}}
**build-info-provider**
Push build data to Jira when a build is synchronized and has links to valid issues. This data is then made available in
the [Development Panel]({{< relref "/features/view-builds" >}}).

**deployment-info-provider**
Push build data to Jira when a build is synchronized that represents a deployment to an environment and has links to valid issues. This data
is then made available in the [Development Panel]({{< relref "/features/view-builds" >}}).
{{% /cloud %}}

### Site Features

{{% server %}}                                                                                                  
**dynamic-ui-elements**
When enabled only show UI elements, [Jenkins Build Panels]({{< relref "/features/view-builds" >}}), 
[Triggering Builds]({{< relref "/features/view-builds" >}}),
[Release Report]({{< relref "/features/view-builds" >}}), when the viewing issue or project has one or more builds linked to it. This can be 
useful in case
you have multiple Jira Software projects in Jira but only a subset of them use the Jenkins integration.

**disable-issue-indexing**
Enabling this feature will cause the app to stop automatically updating issue indexes after a build is
synchronized and will instead rely on Jira to perform the indexing. {{< note >}}Only enable this feature if directed by Marvelution
support!{{< /note >}}

**sync-all-builds**
When enabled all builds available of a job on Jenkins are synchronized every time a job is synchronized, instead of only synchronizing
unknown builds. {{< note >}}Enabling this feature will impact Jenkins as the app will load more data from Jenkins.{{< /note >}}
{{% /server %}}

{{% cloud %}}
**report-info-provider-violations**
Enable this feature to report any data violations of the build and deployment info providers as system errors to allow addressing.
{{% /cloud %}}
