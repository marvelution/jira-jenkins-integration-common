---
title: "Configuration"
description: Explore key methods of integrating Jenkins with Jira. Learn more about the different integration methods. 
date: 2020-07-01T14:07:00Z
draft: false

menu:
  docs:
    identifier: administration
    weight: 2

illustration: configuration

---
