---
title: "Connect a Site Wizard"
date: 2020-07-01T14:07:00Z
draft: false
---

### Accessible Site

{{< info >}}
Select this option if your Jenkins site allows direct communication from the Jira app for bidirectional integration.
{{< /info >}}

1. Follow the instructions to install the 'Jira Integration' plugin on your Jenkins site.
2. Click **Next**.
3. Provide a **Name**.
4. Provide the **Site URL**.
5. Select **Accessible** accessibility.
   {{< include "public-site-requirements" >}}
6. Click **Next**. If you don't want to alter the defaults, click on **Save and Connect** to jump to step 13.
7. Optionally, provide a **Display URL** in case the Site URL is not accessible by users and a different URL should be used in the UI
   features.
8. Optionally, as Jira site administrator, provide a **Scope** in case you want to limit the site integration to a specific Jira project.
9. Optionally, un-check the **Enabled for Synchronization** checkbox if you want to connect the site but not enable it for job and
   build synchronization.
10. Optionally, un-check the **Auto enable new Jobs** checkbox if you don't want to enable all Jobs on the site by default, including
    future new jobs. You will be required to manually select the jobs you want to synchronize once the site in added and connected.
11. Optionally, un-check the **CSRF Protection** checkbox if the Jenkins site doesn't use CSRF protection.
12. Click **Next**
13. Select you preferred method of registering the Jira site with your Jenkins site.
    - Select **Automatic** to let the app register itself with Jenkins. You will need to provide Jenkins administrative credentials to
      use this.
    - Select **Manual** to use the provided **Registration Link** to manually register the Jira site in Jenkins.
    - Select **Configuration as Code** to use the Jenkins Configuration as Code feature to register the Jira site through configuration
      code.
14. Click **Connect** to finalise the connection. Alternatively click **Connect latest** to complete the registration at a later point
    in time.

{{% cloud %}}
### Accessible Through Tunneling

{{< info >}}
Select this option to have all the benefits of bidirectional integration even if the Jenkins site is not directly accessible by the Jira 
app.
{{< /info >}}

1. Follow the instructions to install the 'Jira Integration' plugin on your Jenkins site.
2. Click **Next**.
3. Provide a **Name**.
4. Provide the **Site URL**.
5. Select **Accessible Through Tunneling** accessibility option.
6. Click **Next**. If you don't want to alter the defaults, click on **Save and Connect** to jump to step 12.
7. Optionally, as Jira site administrator, provide a **Scope** in case you want to limit the site integration to a specific Jira project.
8. Optionally, un-check the **Enabled for Synchronization** checkbox if you want to connect the site but not enable it for job and
   build synchronization just yet.
9. Optionally, un-check the **Auto enable new Jobs** checkbox if you don't want to enable all Jobs on the site by default, including
   future new jobs. You will be required to manually select the jobs you want to synchronize once the site in added and connected.
10. Optionally, un-check the **CSRF Protection** checkbox if the Jenkins site doesn't use CSRF protection.
11. Click **Next**
12. Select you preferred method of registering the Jira site with your Jenkins site.
    - Select **Manual** to use the provided **Registration Link** to manually register the Jira site in Jenkins.
    - Select **Configuration as Code** to use the Jenkins Configuration as Code feature to register the Jira site through configuration
      code.
13. Click **Connect** to finalise the connection.

{{< info >}}
The Jenkins plugin will automatically connect to the tunnel once registration is complete.
{{< /info >}}
{{% /cloud %}}

### Inaccessible Site

{{< info >}}
Select this option if the Jira app can't connect the Jenkins site directly, limiting the integration to uni-directional.
{{< /info >}}

1. Follow the instructions to install the 'Jira Integration' plugin on your Jenkins site.
2. Click **Next**.
3. Provide a **Name**.
4. Provide the **Site URL**.
5. Select **Inaccessible** accessibility option.
6. Click **Next**. If you don't want to alter the defaults, click on **Save and Connect** to jump to step 11.
7. Optionally, as Jira site administrator, provide a **Scope** in case you want to limit the site integration to a specific Jira project.
8. Optionally, un-check the **Enabled for Synchronization** checkbox if you want to connect the site but not enable it for job and
   build synchronization just yet.
9. Optionally, un-check the **Auto enable new Jobs** checkbox if you don't want to enable all Jobs on the site by default, including
   future new jobs. You will be required to manually select the jobs you want to synchronize once the site in added and connected.
10. Click **Next**
11. Select you preferred method of registering the Jira site with your Jenkins site.
    - Select **Manual** to use the provided **Registration Link** to manually register the Jira site in Jenkins.
    - Select **Configuration as Code** to use the Jenkins Configuration as Code feature to register the Jira site through configuration
      code.
12. Click **Connect** to finalise the connection.

{{% note %}}Jenkins will now notify Jira whenever there is a change that needs to be processed by Jira, so it may take some time for all
the jobs to become available in Jira.{{%/ note %}} 
