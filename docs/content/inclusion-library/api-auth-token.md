---
title: "API Auth Token"
date: 2020-07-01T14:07:00Z
draft: false
---

The API Token is a password replacing token that users can use to authenticate with against Jenkins. This is mostly useful for users that
 have there accounts managed outside Jenkins. To get a token for an account, follow these steps:

* Login on Jenkins using the **username** and **password** of the synchronization account
* Click on the **Account Name** in the top-right corner of the page after login
* Then click on the **Configure** link in the left menu
* The **API Token** can now be displayed by clicking on the **Show API Token...** button
