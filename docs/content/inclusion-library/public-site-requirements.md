---
title: "Public Site Requirements"
date: 2020-07-01T14:07:00Z
draft: false
---

{{% note style="footnotes" %}}
{{< server >}}
- In case you use SSL, make sure that Jira and Jenkins have each other SSL certificate in their trust store so that they can connect.
  See also Connecting to SSL Services.
- The Jenkins counterpart plug-in is required to be installed before the site can be added on Jira.
{{</ server >}}
{{< cloud >}}
- For security purposes, only ports 80 8080, 443 and 8443, http and https respectively, are supported on Jira Cloud.
- The Jenkins counterpart plug-in is required to be installed before the site can be added on Jira Cloud.
- You may need to update your firewall rules to allow traffic from the cloud app, see [Security]({{< relref "/security-requirements" >}}) 
  section for details.
{{</ cloud >}}
{{%/ note %}}
