---
title: "Advanced Fields"
date: 2020-12-11T00:00:00Z
draft: false
---

##  Advanced Fields

Advanced Fields is available under **Advanced Options** and allows you to change issue field values even if the field is not supported by
 this app.

Using the feature requires that you specify the field operations using the JSON format specified by the 
[Jira REST API](https://developer.atlassian.com/cloud/jira/platform/rest/v3/#api-rest-api-3-issue-post) which contains two attributes
namely fields and update.

```json
{
  "fields": {
    "summary": "Build [[build.displayName]] was not successful"
  },
  "update": {
    "components": [{
      "add": [{ "id": "10000" }]
    }]
  }
}  
```    

{{< tip >}}[Expression Values]({{< relref "/legacy/automation/expression-values" >}}) can be used here.{{< /tip >}}

{{< tip >}}
Refer to the [Jira Field input formats](https://developer.atlassian.com/cloud/jira/service-desk/rest/#fieldformats) documentation for 
samples of different field types that can be used in this advanced fields JSON format.
{{< /tip >}}

{{< note >}}
Operations for the same field cannot co-exist in both fields and update sections of the JSON.
{{< /note >}}

##  fields vs. update

fields is a shortcut for the update set  operation. Take the following JSON snippet.

```json
{
  "fields": {
    "summary": "Build [[build.displayName]] was not successful"
  }
}      
```

The summary can also be set using the update operation below.

```json
{
  "update": {
    "summary": [{
      "set": "Build [[build.displayName]] was not successful"
    }]
  }
}      
```
update can be useful when you want to add or remove a value from the field, or when you want to combine operations.

For example, lets say you want to update the field components by removing one component and adding two others, take the following snippet:
   
```json
{
  "update": {
    "components": [
      {
        "remove": [{ "id": "10100" }]
      }, {
        "add": [{ "id": "10101" }, { "name": "Backend" }]
      }
    ]
  }
}
```      

On line 5 we remove the component with the id 10100 and on line 7 we add two components,  one via its id namely 10101, and one via its name namely Backend.

{{< tip heading="Simpler is often better" >}}
When you want to simply set the value of a field, then prefer using the `fields` section of JSON, only opting for `update` when you want to
 use another operation like `add`, `remove` or `edit`. This will keep your JSON easier on the eyes.
{{< /tip >}}

##  Editable Issue Fields

Only fields that are on the appropriate action screen, like create, edit, transition, can be edited through a Jenkins for Jira
rule. If you specify a field that is not on the appropriate screen related to the action you are trying to execute, then the rule execution
will fail with the reason for this will be shown in the rule log.  

A good visual way to check if a rule can update a field is by opening the action yourself in Jira, e.g. to check if field ABC can be set 
when creating a bug in project XYZ, simply open the create issue screen in Jira for project XYZ and set the issue type to bug. Then if
the field ABC is selectable on the screen then you can also set this via a rule.

An alternative method to check if the field in on the screen is to validate this through interpreting the JSON response from one of the
following REST API's:

*   For creating an issue: `http(s)://[YOUR.INSTANCE.ROOT]/rest/api/3/issue/createmeta?expand=projects.issuetypes.fields`, use the URL
    parameters `projectKeys` and `issuetypeNames` to limit the scope of the response and only return the fields for the specific issue
    type of the specific project you are interested in.
*   For editing an issue: `http(s)://[YOUR_INSTANCE_ROOT]/rest/api/3/issue/[ISSUE_KEY]/editmeta`
*   For transitioning an issue: `http(s)://[YOUR_INSTANCE_ROOT]/rest/api/3/issue/[ISSUE_KEY]/transitions?expands=transitions.fields`, you
    will need to lookup the transition you want to inspect, if you know the transition id, then you can use this with the URL parameter
    `transitionId`.
