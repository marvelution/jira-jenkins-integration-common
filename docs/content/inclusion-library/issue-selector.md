---
title: "Issue Selector"
date: 2020-12-11T00:00:00Z
draft: false
---

##  Issue Selector

Select the selection method used to select the issues to add the comment on.

### Linked to Build

Select this method to select all the issues that are linked to the build that was synchronized. Refer to the 
[Data Synchronization]({{< relref "/data-synchronization" >}}) documentation on how builds are linked to issues.

This method is only available in combination with the [Build Synchronized]({{< relref "/legacy/automation/events#build-synchronized" >}}) event.

### Linked to Job

Select this method to select all the issues that are linked to the job that was synchronized (when using the 
[Job Synchronized]({{< relref "/legacy/automation/events/job-synchronized" >}}) event) or the job of the build that was synchronized 
(when using the [Build Synchronized]({{< relref "/legacy/automation/events#build-synchronized" >}}) event). Refer to the 
[Data Synchronization]({{< relref "/data-synchronization" >}}) documentation on how builds are linked to issues.

This method is only available in combination with either the [Job Synchronized]({{< relref "/legacy/automation/events#job-synchronized" >}})
or the [Build Synchronized]({{< relref "/legacy/automation/events#build-synchronized" >}}) events.

### From Expression

Select this method if you want to limit issue selection to specific attributes of the trigger event or even a static value.

When selecting this method you must also specify the expression used to search for issues to transition. For instance, if you only want
to select issues that are mentioned in the job description, then you can use this expression `[[job.description]]` or 
`[[build.job.description]]`. You can also combine attributes, e.g. `[[job.displayName]] [[job.description]]`, or even attributes with static
text, DEV-1, DEV-2, `[[build.description]]`. See [Expression Values]({{< relref "/legacy/automation/expression-values" >}}) for details on the 
attributes that are available.

### JQL

Select this method if you want to limit the issue selection to issues matching a specifc JQL search result. The JQL can also include 
attributes of the trigger event. See [Expression Values]({{< relref "/legacy/automation/expression-values" >}}) for details on the attributes that 
are available.

### None

Select this method if you don't want to select any issues for the action. Can be useful for instance when using the 
[Create Version]({{< relref "/legacy/automation/actions/create-version" >}}) action and just want the version without updating any issues.
