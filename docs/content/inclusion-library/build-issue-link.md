---
title: "Build Issue Linking"
date: 2020-07-01T14:07:00Z
draft: false
---

Jenkins builds are linked to Jira issues if a key of an existing Jira issue can be found in the job and build data that was synchronized.

The following elements are inspected to extract Jira issue keys:

* Job display name
* Job description
* Job url
* Build display name
* Build description
* Build trigger cause
* Change-set commit messages
* Git branch names

Any potential issue key located will be verified to exist, and a link between the build is issue is only created if the issue exists in Jira.

{{% note %}}
It is important to note that the issue keys in the commit message must follow the rules set by Atlassian on the format of Project Keys
 and therefor Issue Keys. Details on the format rules can be found in there documentation
[Changing the project key format](https://confluence.atlassian.com/adminjiraserver071/changing-the-project-key-format-802592378.html).
{{%/ note %}}
