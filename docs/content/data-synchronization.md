---
title: "Data Synchronization"
date: 2020-07-01T14:07:00Z
draft: false

aliases:
  - features/jira-features/data-synchronization/
  - requirements/data-synchronization/

menu:
  docs:
    weight: 3

illustration: data-synchronization

---

Over the course of different implementations, to improve the integration of Jenkins with Jira, the decision was made to synchronize build
data from Jenkins into Jira. As previous implementations proved that the Jenkins API was not able to handle the load that larger Jira
and Jenkins instances can produce. Now, selected data elements of the Jobs and there builds are synchronized from Jenkins to Jira.  

The synchronization can be trigger in different manners:

{{% cloud %}}
* The Jira Build Notification Listener that will trigger synchronization of that specific Job after a build is completed.
* Through the use of the administration UI.
{{% /cloud %}}
{{% server %}}
* A scheduled task that will run by default every hour and triggers the synchronization of all Sites.

{{% info %}}
The interval, in milliseconds, of the scheduled task is determined by the system property `jenkins.connector.scheduler.interval`,
the default interval is `3600000` milliseconds (1 hour).
It is advised that you use the Jira Build Notification Listener to speedup the synchronization process when the hourly schedule is
not speedy enough, but it is possible to update this setting and restarting Jira to speedup the overall synchronization process.
{{% /info %}}

* The Jira Build Notification Listener that will trigger synchronization of that specific Job after a build is completed.
* Through the use of the administration UI.
{{% /server %}}

## Site Synchronization

Site synchronization is triggered when the site is added for the first time, when an unknown job triggers that a build is completed, or
through manual action in the UI and will synchronize:

* Whether the remote site has the Jira Integration plugin installed
  - If the Jenkins plugin is installed, then the Jira site will be registered with the Jenkins site to use the Jira Build
    Notification Listener.
* Whether the remote site uses Crumbs for CSRF protection
* The basic details of Jobs listed at the root level, this includes name and url information.
  - If a job is no longer available on the remote site, then the job is marked as deleted, but is not removed from Jira.
  - Once it becomes available again, then the deletion marker will be removed.

Once the job list is synchronized, then the synchronization for each individual job is triggered

## Job Synchronization

Job synchronization operation is either triggered by a Site Synchronization operation or by the Jira Build Notification Listener and will
synchronize:

* Detailed Job information including, name, display name, description, url, last build number. These are also stored.
* Identify builds that need to be synchronized, and trigger the synchronization of them
  * When builds are removed from the remote site, then that build is marked as deleted but and not removed from Jira.
* Identifies whether the job itself has jobs that need to be synchronized, this process is the same as for the job list of a site at
  root level.

## Build Synchronization

The build synchronization is a part of the Job Synchronization operation and will only synchronize data elements that are needed for the
plugin to function.

Data elements synchronized and bold element are also stored:

* **build number**
* **display name**
* **(trigger) cause**
* **duration of the build**
* **timestamp of execution**
* basic SCM information, like branch name(s) and commit messages
* **build result**
* the node the build was executed on
* **basic test result stats including total tests executed, number of tests that failed and that where skipped.**

### Linking Builds to Issues

{{< include "build-issue-link" >}}


{{% cloud %}}
### Development Panel Integration

Build information is shown in the Development Panel next to data from Bitbucket commits, branches and pipelines.
{{%/ cloud %}}
