---
title: "Expression Values"
date: 2020-07-01T14:07:00Z
draft: false

weight: 2

menu:
  docs:
    parent: legacy-automation
    weight: 2

---

{{< automationDeprecationNotice >}}

Expression values allow you to access data within the event that triggered the rule and use them in rule actions.

## Expression Language

The expression language is based on the dot notation method to access attributes of objects between double square brackets, e.g.
`[[build.result]]`  to access the result of the build in the event that triggered the rule execution.

## Object Reference

### rule

|                        |                              |
|------------------------|------------------------------|
| `[[rule.id]]`          | Prints the rule id.          |
| `[[rule.name]]`        | Prints the rule name.        |
| `[[rule.description]]` | Prints the rule description. |

### event

|                       |                                                                                                                                                                                   |
|-----------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `[[event.x]]`         | Where x is the name of the attribute of the event. Check below to see what other objects are available in the event.                                                              |
| `[[event.timestamp]]` | Returns the [java.time.LocalDateTime](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/time/LocalDateTime.html) referencing the timestamp the event was emitted. |

### site

|                                   |                                                                             |
|-----------------------------------|-----------------------------------------------------------------------------|
| `[[site.id]]`                     | Prints the ID of the site.                                                  |
| `[[site.name]]`                   | Prints the name of the site.                                                |
| `[[site.type]]`                   | Prints the type of the site.                                                |
| `[[site.connectionType]]`         | Prints the connection type of the site.                                     |
| `[[site.rpcUrl]]`                 | Prints the URL of the site that can be used by the app.                     |
| `[[site.displayUrl]]`             | Prints the URL of the site that can be used by users.                       |
| `[[site.enabld]]`                 | Boolean value that indicates the site is enable for processing.             |
| `[[site.registrationComplete]]`   | Boolean value that indicates the site registration processes is complete.   |
| `[[site.jenkinsPluginInstalled]]` | Boolean value that indicates the site has the counterpart plugin installed. |
| `[[site.autoLinkNewJobs]]`        | Boolean value that indicates the site will automatically enable new jobs.   |
| `[[site.firewalled]]`             | Boolean value that indicates the site is firewalled (Privately accessible). |

### job

Available when using the [Job Synchronized]({{< relref "/legacy/automation/events#job-synchronized" >}}) rule event.

The `[[job]]` object can also be referenced via the event attribute `[[event.job]]`.

|                       |                                                                                                                     |
|-----------------------|---------------------------------------------------------------------------------------------------------------------|
| `[[job.id]]`          | Prints the ID of the job.                                                                                           |
| `[[job.site]]`        | References the site the job is hosted on, see [site](#site). Use `[[job.site.name]]` to print the name of the site. |
| `[[job.name]]`        | Prints the name of the job.                                                                                         |
| `[[job.urlName]]`     | Prints the name of the job.                                                                                         |
| `[[job.fullName]]`    | Prints the name of the job.                                                                                         |
| `[[job.displayName]]` | Prints the display name of the job.                                                                                 |
| `[[job.description]]` | Prints the description of the job.                                                                                  |
| `[[job.displayUrl]]`  | Prints the URL of the job that can be used by users.                                                                |
| `[[job.lastBuild]]`   | Prints the number of the last known build.                                                                          |
| `[[job.oldestBuild]]` | Prints the number of the first known build.                                                                         |
| `[[job.linked]]`      | Boolean value that indicates that the job is enabled for build-to-issue indexing.                                   |
| `[[job.deleted]]`     | Boolean value that indicates that the job is marked as deleted in the app.                                          |

### build

Available when using the [Build Synchronized]({{< relref "/legacy/automation/events#build-synchronized" >}}) rule event.

The `[[build]]` object can also be referenced via the event attribute `[[event.build]]`.

|                                 |                                                                                                                                                                            |
|---------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `[[build.id]]`                  | Prints the ID of the build.                                                                                                                                                |
| `[[build.job]]`                 | References the job of the build, see [job](#job). Use `[[build.job.name]]` to print the name of the job.                                                                   |
| `[[build.number]]`              | Prints the build number.                                                                                                                                                   |
| `[[build.displayName]]`         | Prints the display name of the build, defaults to `[[build.job.displayName]]` #`[[build.number]]`                                                                          | 
| `[[build.displayUrl]]`          | Prints the URL of the build that can be used by users.                                                                                                                     |
| `[[build.description]]`         | Prints the description of the build.                                                                                                                                       |
| `[[build.cause]]`               | Prints the cause of the build.                                                                                                                                             |
| `[[build.result]]`              | References the result type of the build.{{< newline >}}Known results include, from best to worst: `SUCCESS`, `UNSTABLE`, `FAILURE`, `NOT_BUILT`, `ABORTED`, `UNKNOWN`.     | 
| `[[build.builtOn]]`             | Prints the name of the node where the build was executed on.                                                                                                               |
| `[[build.duration]]`            | Prints the duration of the build in milliseconds.                                                                                                                          |
| `[[build.formattedDuration]]`   | Prints the formatted duration of the build, e.g. '2m 15s'                                                                                                                  |
| `[[build.timestamp]]`           | Prints the timestamp the build was scheduled in milliseconds since epoch, January 1, 1970, 00:00:00 GMT.                                                                   |
| `[[build.buildDate]]`           | References the [java.util.Date](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/Date.html) reference to the timestamp when the build was scheduled. |
| `[[build.deleted]]`             | Boolean value that indicates that the build is marked as deleted in the app.                                                                                               |
| `[[build.testResults.passed]]`  | Prints the number of tests that passed.                                                                                                                                    |
| `[[build.testResults.failed]]`  | Prints the number of tests that failed.                                                                                                                                    |
| `[[build.testResults.skipped]]` | Prints the number of tests that where skipped.                                                                                                                             |
| `[[build.testResults.total]]`   | Prints the total number of tests that where part of the build.                                                                                                             |

### Function Reference

|              |                                                                                                                                                                                         |
|--------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `[[#now]]`   | Returns the [java.time.LocalDateTime](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/time/LocalDateTime.html) referencing the timestamp the expression is evaluated. |
| `[[#today]]` | Returns the [java.time.LocalDate](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/time/LocalDate.html) referencing the timestamp the expression is evaluated.         |
