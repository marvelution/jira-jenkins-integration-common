---
title: "Events"
date: 2020-07-01T14:07:00Z
draft: false
layout: merged-list

weight: 3

menu:
  docs:
    identifier: legacy-automation-events
    parent: legacy-automation

---

{{< automationDeprecationNotice >}}

Rules are triggered when an event is published that the rule is interested in, e.g. build synchronized or build linked to issue. The rule
 is scheduled for execution as soon as an event is received that the rule was configured for.
