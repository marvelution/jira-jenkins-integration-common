---
title: "Build Synchronized"
date: 2020-07-01T14:07:00Z
draft: false

menu:
  docs:
    parent: legacy-automation-events

--- 

This rule runs every time a new build is synchronized. This also includes builds that are synchronized as part of a
[job synchronization event]({{< relref "/legacy/automation/events#job-synchronized" >}}).
