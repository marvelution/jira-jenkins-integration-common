---
title: "Job Synchronized"
date: 2020-07-01T14:07:00Z
draft: false

menu:
  docs:
    parent: legacy-automation-events

---

This rule runs every time a job is synchronized.
