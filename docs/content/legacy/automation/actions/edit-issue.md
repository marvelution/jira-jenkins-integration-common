---
title: "Edit Issue"
date: 2020-12-11T00:00:00Z
draft: false

menu:
  docs:
    parent: legacy-automation-actions

intro: Rule action to edit an issue in any project.

---

###  Summary

Provide the summary for the issue.

{{< tip >}}
[Expression Values]({{< relref "/legacy/automation/expression-values" >}}) can be used here, e.g.  `Build [[build.displayName]] was not 
successful`.
{{< /tip >}}

###  Description

Optionally provide the description for the new issue. Every separate lines with a blank line to separate lines into paragraphs.

{{< tip >}}[Expression Values]({{< relref "/legacy/automation/expression-values" >}}) can be used here.{{< /tip >}}        

{{< include "advanced-fields" >}} 
