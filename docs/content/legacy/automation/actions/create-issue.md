---
title: "Create Issue"
date: 2020-12-11T00:00:00Z
draft: false

menu:
  docs:
    parent: legacy-automation-actions

intro: Rule action to create an issue in any project.

---

### Project

Start typing the name of the project where the issue should be created in, and the options matching your text will appear.

### Issue Type

Selectable issue types will become available once you have selected a project.

###  Summary

Provide the summary for the issue.

{{< tip >}}
[Expression Values]({{< relref "/legacy/automation/expression-values" >}}) can be used here, e.g.  `Build [[build.displayName]] was not 
successful`.
{{< /tip >}}

###  Description

Optionally provide the description for the new issue. Every separate lines with a blank line to separate lines into paragraphs.

{{< tip >}}[Expression Values]({{< relref "/legacy/automation/expression-values" >}}) can be used here.{{< /tip >}}        

{{< include "advanced-fields" >}} 
