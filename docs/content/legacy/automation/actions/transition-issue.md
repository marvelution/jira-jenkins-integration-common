---
title: "Transition Issue"
date: 2020-12-11T00:00:00Z
draft: false

menu:
  docs:
    parent: legacy-automation-actions

intro: Rule action to transition issues to a specific target status within their workflow.

---

###  Target status

Select the status the issue should be transitioned too.

###  Transition Pattern

When there are multiple transitions in a workflow that go to the same status, then you can use this regex pattern to select the correct
transition.

Let's say you want to transition an issue to the Done status, and let's assume that you have a workflow that has two transition to the 
`Done` status, one named `Accepted` and one named `Rejected`. You can then use this field to select the transition you want to use, for
instance to use the `Accepted` transition you could specify the following pattern `^Accepted$` to match the complete transition name or
`^Acc(.*)$` to match the transition of which the name starts with `Acc`.

{{< include "issue-selector" >}}          

###  Add Comment

Specify the comment to be added to the issue. Every separate lines with a blank line to separate lines into paragraphs.

{{< tip >}}[Expression Values]({{< relref "/legacy/automation/expression-values" >}}) can be used here.{{< /tip >}}

{{< include "advanced-fields" >}} 
