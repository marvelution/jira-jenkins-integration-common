---
title: "Comment on Issue"
date: 2020-12-11T00:00:00Z
draft: false

menu:
  docs:
    parent: legacy-automation-actions

intro: Rule action to comment on an issue.

---

{{< include "issue-selector" >}}          

###  Add Comment

Specify the comment to be added to the issue. Every separate lines with a blank line to separate lines into paragraphs.

{{< tip >}}[Expression Values]({{< relref "/legacy/automation/expression-values" >}}) can be used here.{{< /tip >}}
