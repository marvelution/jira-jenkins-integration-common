---
title: "Create Version"
date: 2020-12-11T00:00:00Z
draft: false

menu:
  docs:
    parent: legacy-automation-actions

intro: Rule action to create a new project version, and optionally link execution related issues to it via the fixVersions field.

---

### Project

Start typing the name of the project where the issue should be created in, and the options matching your text will appear.

###  Name

Provide the name of the new version.

{{< tip >}}
[Expression Values]({{< relref "/legacy/automation/expression-values" >}}) can be used here, e.g.  `server-[[build.number]]`.
{{< /tip >}}

###  Description

Optionally provide the description for the new version.

{{< tip >}}[Expression Values]({{< relref "/legacy/automation/expression-values" >}}) can be used here.{{< /tip >}}        

###  Start Date

Optionally provide the start date, in format yyyy-MM-dd, for the new version.

{{< tip >}}[Expression Values]({{< relref "/legacy/automation/expression-values" >}}) can be used here.{{< /tip >}}   

###  Release Date

Optionally provide the release date, in format yyyy-MM-dd, for the new version.

{{< tip >}}[Expression Values]({{< relref "/legacy/automation/expression-values" >}}) can be used here, e.g. `[[event.timestamp]]`{{< /tip >}}

###  Released

Optionally select whether the new version should be marked as released.

{{< include "issue-selector" >}} 
