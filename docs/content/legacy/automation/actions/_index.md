---
title: "Actions"
date: 2020-07-01T14:07:00Z
draft: false
layout: merged-list

weight: 5

aliases:
- automation/Actions/

menu:
  docs:
    identifier: legacy-automation-actions
    parent: legacy-automation

---

{{< automationDeprecationNotice >}}

Actions are the building blocks of rules that will actually do something. They can create an issue, transition an issue, comment
on an issue.
