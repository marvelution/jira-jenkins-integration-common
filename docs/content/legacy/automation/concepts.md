---
title: "Concepts"
date: 2020-07-01T14:07:00Z
draft: false

weight: 1

aliases:
  - concepts/

menu:
  docs:
    parent: legacy-automation
    weight: 1
---

{{< automationDeprecationNotice >}}

Rules are the bases in which you will be able to configure automated actions to occur when specific events and conditions are met. The
 building blocks of rules are Events, Conditions and Actions.

## [Events]({{< relref "/legacy/automation/events" >}})

Rules are triggered when an event is published that the rule is interested in, e.g. build synchronzied or build linked to issue. The rule
 is scheduled for execution as soon as an event is received that the rule was configured for.

## [Conditions]({{< relref "/legacy/automation/conditions" >}})

Conditions are means to narrow the scope of a rule, e.g. only perform the rule actions when the build has a specific outcome, or when the
 build is of a specific job. Conditions can also be combines, e.g. only performs actions for successful builds of job X.

## [Actions]({{< relref "/legacy/automation/actions" >}})

Actions are the building blocks of rules that will actually do something. They can create an issue, transiation an issue, comment on an
 issue.
