---
title: "Synchonized Job"
date: 2020-12-11T00:00:00Z
draft: false

menu:
  docs:
    parent: legacy-automation-conditions

intro: Condition that allows you to limit rule execution by looking at the job that was synchronized.

---

###  Job(s)

Start typing the name of the job(s) you want limit the action scope to (or from, see Invert below) and options matching your text will
appear.

###  Invert

Invert can be used to switch between an inclusion or exclusion condition execution.

When invert is not selected, default, then the condition will only match if the job is one of the selected jobs.
When invert is selected, then the condition will only match jobs if the job is not one of the jobs selected.
