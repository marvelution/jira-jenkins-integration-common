---
title: "Build with Result"
date: 2020-12-11T00:00:00Z
draft: false

menu:
  docs:
    parent: legacy-automation-conditions

intro: |
    Condition that allows you to limit the rule execution by looking at the result of the synchronized build and comparing this to the
    condition's configuration.

---

###  Operand

Select the operand to use to check the actual build result with the expected build result. Options include:

*   equal to - matches the condition when the actual and expected build results are the same, e.g. `Success` (actual) is equal to `Success`
    (expected).
*   worse than - matches the condition when the actual build result is worse than expected build result, e.g. `Failure` (actual) is worse 
    than `Success` (expected).
*   worse or equal to - matches the condition when the actual build result is worse or equal to the expected build results, e.g. `Unstable`
    (actual) is worse or equal to `Unstable` (expected).
*   better than - matches the condition when the actual build result is better than the expected build result, e.g. `Unstable` (actual) is
    better than `Failure` (expected).
*   better or equal to - matches the condition when the actual build result is better than or equal to the expected build result, e.g. 
    `Success` (actual) is better or equal to `Unstable` (expected).

###  Expected Build Result

Select the build result the actual result should be compared to using the selected operand. Options include (best to worse), `Success`,
`Unstable`, `Failure`, `Not built`, `Aborted`, `Unknown`.
