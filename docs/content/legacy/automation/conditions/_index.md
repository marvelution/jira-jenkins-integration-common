---
title: "Conditions"
date: 2020-07-01T14:07:00Z
draft: false
layout: merged-list

weight: 4

aliases:
- automation/Conditions/

menu:
  docs:
    identifier: legacy-automation-conditions
    parent: legacy-automation

---

{{< automationDeprecationNotice >}}

Conditions are means to narrow the scope of a rule, e.g. only perform the rule actions when the build has a specific outcome, or when the
 build is of a specific job. Conditions can also be combines, e.g. only performs actions for successful builds of job X.
