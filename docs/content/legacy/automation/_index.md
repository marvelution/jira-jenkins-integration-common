---
title: "Automation (Legacy)"
description: Explore key concepts like action & build powerful automation rules with ease. Learn about the concepts.
date: 2020-07-01T14:07:00Z
draft: false

menu:
  docs:
    identifier: legacy-automation
    weight: 6

illustration: automation

---

{{< automationDeprecationNotice >}}
