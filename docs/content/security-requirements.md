---
title: "Security"
date: 2020-07-01T14:07:00Z
draft: false

aliases:
  - features/jira-features/system-error/
  - requirements/security-requirements/

menu:
  docs:
    weight: 6

illustration: security

---

{{% cloud %}}
## Jira Cloud App Permissions

| Permission             | Reason                                                                                                                                                                                                                                     |
|------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Act as User            | Required to get user specific details like the users time zone, and permissions.                                                                                                                                                           |
| Read                   | Required to check the existence of issue keys identified during synchronization of jobs and build and getting available configuration data for Automation Rule elements.                                                                   |
| Write                  | Required to write build/deployment data for the [Development Panel Integration]({{< relref "/features/view-builds" >}}) and creating, updating or transitioning issues via Automation Rules.                                               |
| Delete                 | Required to delete build/deployment data form the [Development Panel Integration]({{< relref "/features/view-builds" >}}).                                                                                                                 |
| Project Admin          | Required to create and update project versions from Automation Rules.                                                                                                                                                                      |
| Access Email Addresses | Required to collect the email addresses of users that administer the app, needed to have a point of contact <br/>for important updates and notifications to be shared with. Contact information from the app license is not covering this. |

## Jira Cloud App IP Addresses

The Jenkins for Jira app doesn't have fixed IP addresses, instead it uses defined ranges of IP addresses. You should 
allow-list these IP addresses to maintain the integration with your on-premise Jenkins instance(s):

https://jjc.marvelution.com/status/info

- 34.242.243.113
- 34.246.239.180
- 34.255.62.27

{{% tip %}}
These IP addresses could change at any time, so make sure to read IP related notifications in the app as well as using the before 
mentioned [machine consumable endpoint](https://jjc.marvelution.com/status/info).
{{% /tip %}}

{{% /cloud %}}

## Jenkins to Jira Path Access Permissions

{{< server >}}
{{< info >}}
This section only applies if you have the Jira Integration for Jira plug-in installed in Jenkins.
{{< /info >}}
{{< /server >}}

Jenkins notifies Jira when a build in completed. It does this be accessing 
{{< cloud >}}https://jjc.marvelution.com/rest/[tenant.id]/*{{< /cloud >}}
{{< server >}}http(s)://[your.jira.host][/jira.context]/rest/jenkins/latest/*{{< /server >}}

Data send to Jira depends on how the Jenkins site is registered in Jira.
- In case the site is registered as an accessible site, then it's an empty trigger for Jira app to synchronize a newly completed build.
- In case the site is registered as an inaccessible site, then the trigger contains the data the Jira app needs to synchronize and index the
  build.

## Jira to Jenkins Path Access Permissions

Below lists the paths that the Jira app will access in a configured Jenkins site.

**\[GET\] plugin/jira-integration/ping.html**
is used to check if the Jira Integration plug-in is installed on the Jenkins site.

**\[POST\] jji/register/**
is used to register the Jira site with the Jenkins site.

**\[POST\] jji/unregister/**
is used to unregister the Jira site with the Jenkins site, done when a site is deleted in Jira.

**\[GET\] \*\*/api/json/**
is used to collect job and build data from Jenkins.

**\[POST\] \*\*/jji/build/**
is used to trigger a new build of a job on Jenkins.
