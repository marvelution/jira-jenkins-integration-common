---
title: "Getting Started"
date: 2020-07-01T14:07:00Z
draft: false
layout: single
         
aliases:
  - guides/quick-start-guide/

menu:
  docs:
    weight: 1

illustration: get-started
bodyClass: getting-started

---

Here are the initial steps you need to follow in order to get Jenkins for Jira up and running.

{{< toc >}}

## Install the Jira app from the Atlassian Marketplace

You need to download the app Jenkins for Jira from the Atlassian Marketplace.
{{% cloud %}}
### _How to get the app while working in Jira_

On the top menu bar select Apps > Find new apps:

{{< screenshot "getting-started/1-cloud-in-app-dropdown-menu-recreated-v2.svg" >}}

You will arrive on the Atlassian Marketplace main page. Search 'Jenkins integration':

{{< screenshot "getting-started/2-cloud-in-app-search-text-field-recreated.svg" >}}

The first result should be _Jenkins for Jira_. Click anywhere in the square box:

{{< screenshot "getting-started/3-cloud-in-app-jenkins-card-recreated.svg" >}}

Click 'Try it free':

{{< screenshot "getting-started/4-cloud-in-app-plugin-overview-recreated.svg" >}}

Click 'Start free trial':

{{< screenshot "getting-started/5-cloud-in-app-add-to-jira-modal-recreated.svg" >}}

A message stating that it is downloading will appear:

{{< screenshot "getting-started/6-cloud-in-app-adding-jenkins-modal-recreated.svg" >}}

The app normally downloads within one minute.

{{< screenshot "getting-started/7-cloud-in-app-success-modal-recreated.svg" >}}

### _Get app directly from the Atlassian Marketplace_

Go to the [Atlassian Marketplace](https://marketplace.atlassian.com/) and search ‘Jenkins Integration’.
Or click here to go directly to [Jenkins for Jira]({{< tryItLink >}}):

{{< screenshot "getting-started/9-cloud-marketplace-search-results-recreated.svg" >}}

Click on Jenkins for Jira:

{{< screenshot "getting-started/10-cloud-marketplace-overview-recreated.svg" >}}

Select the version of Jira you use:

{{< screenshot "getting-started/11-cloud-marketplace-overview-deployment-dropdown-recreated.svg" >}}

Click 'Try it free' button:

{{< screenshot "getting-started/7-cloud-in app-success-modal-recreated.svg" >}}

Click 'Start free trial' under cloud.
You need to be logged into an Atlassian account with permission to download apps from the Atlassian Marketplace:

{{< screenshot "getting-started/12-cloud-marketplace-select-hosting-plan-modal-recreated.svg" >}}

Choose the site you want to add the app to then click 'Start free trial':

{{< screenshot "getting-started/14-cloud-marketplace-chose-site-dropdown-modal-recreated.svg" >}}

You will be directed to your Jira instance. Click 'Start free trial':

{{< screenshot "getting-started/15-cloud-marketplace-back-to-in app.svg" >}}

A message stating that it is downloading will appear:

{{< screenshot "getting-started/6-cloud-in-app-adding-jenkins-modal-recreated.svg" >}}

The app normally downloads within one minute.

{{< screenshot "getting-started/7-cloud-in app-success-modal-recreated.svg" >}}
{{% /cloud %}}
{{% server %}}
Go to [Jenkins for Jira]({{< tryItLink >}}) on the Atlassian Marketplace.

**OR**

On top right corner of menu bar select your headshot/icon → scroll down to Atlassian Marketplace → click on Atlassian Marketplace.

{{< screenshot "getting-started/2-data-center-recreated.svg" >}}

You will arrive on the Atlassian Marketplace main page. Search ‘Jenkins integration’ and click enter/return:

{{< screenshot "getting-started/3-data-center-recreated.svg" >}}

The first result should be Jenkins for Jira. Click the button ‘Free trial’:

{{< screenshot "getting-started/4-data-center-recreated.svg" >}}

Click ‘Accept & Install’ to start installation:

{{< screenshot "getting-started/5-data-center-recreated.svg" >}}

The app normally downloads within one minute but could be longer depending on your Jira configuration:

{{< screenshot "getting-started/6-data-center-recreated.svg" >}}

Now you need to get the trial license. Click ‘Get license’:

{{< screenshot "getting-started/7-data-center-recreated.svg" >}}

Note: The app will not work until you have the license activated in your Jira instance.

## Getting and applying a license

After clicking on ‘Get license’ you will be redirected to [my.atlassian.com](https://my.atlassian.com). Ensure you are logged in.
Agree to the terms of use and then click ‘Generate License’.

{{< screenshot "getting-started/8-data-center-recreated.svg" >}}

You will arrive at a page with the license key.
If you have configured your Jira to connect with the Atlassian Marketplace click ‘Apply License’:

{{< screenshot "getting-started/10-data-center-recreated.svg" >}}

You should have the app installed. Go to [Step 3: Installing the Jenkins Plugin](#installing-the-jenkins-plugin). If not keep reading.

{{< screenshot "getting-started/11-data-center-recreated.svg" >}}

If you have **not** configured your Jira to connect with the Atlassian Marketplace you will see this:

{{< screenshot "getting-started/12-data-center-recreated.svg" >}}

You must manually apply the license. Jenkins for Jira will not work until this is done.

**How to manually apply the license**

Click ‘Close’ on modal dialog window (the popup that says Oh no!). You will see the installed apps on the Manage Apps page.

Go to [my.altassian.com](https://my.atlassian.com) in a new browser window (make sure you are logged in).

Scroll down on the page to see licences. Check 'Trials' box if not checked.

{{< screenshot "getting-started/13-data-center-recreated.svg" >}}

Expand ‘Jenkins for Jira: Trial’. Copy the license from the 'license key' field:

{{< screenshot "getting-started/14-data-center-recreated.svg" >}}

Go back to the Atlassian Marketplace → Manage Apps. Click on Jenkins for Jira Server and Data Center to expand it.

{{< screenshot "getting-started/15-data-center-recreated.svg" >}}

Paste the license key into the field ‘License key’.

{{< screenshot "getting-started/16-data-center-recreated.svg" >}}

Click ‘Update’ to save the license key.

{{< screenshot "getting-started/17-data-center-recreated.svg" >}}

{{% /server %}}

## Installing the Jenkins plugin

You also need to download a plugin on the Jenkins side called _Jira Integration_. It is required.

### _Go to your Jenkins homepage_

Manage Jenkins > Manage Plugins

### _Under Manage Plugin_

Click on the Available Plugins.

### _Under Plugins_

Search for _jira integration_ and check the 'Install' checkbox

Click ‘Download now and install after restart‘

Check ‘Restart Jenkins when installation is complete and no jobs are running‘ to restart Jenkins.

## Connecting Jira and Jenkins

Now you are ready to configure your first Jenkins site (an instance of Jenkins) and enable jobs to synchronize.
This is important to do right away as it establishes the data connection between Jira and Jenkins. If you do not do this you will not get any Jenkins data in Jira.

### _From the top navigation in Jira, choose Gear Icon > Manage Apps_

{{< screenshot "getting-started/17-cloud-manage-jenkins-in-app-recreated.svg" >}}

### _Choose Jenkins Integration > Manage Sites and then click Connect your first Jenkins Site_

This brings up the Connect a Site wizard. Create a name of the site you are adding, and provide the site URL.

### _Under Accessibility select either Accessible or Inaccessible_

_**If select Accessible:**_

Note the following if select Public:
{{< include "public-site-requirements" >}}

{{% cloud %}}
_**If select Accessible Through Tunneling:**_

Select this option to benefit from bidirectional integration even if the Jira app can't access the Jenkins site directly.
{{% /cloud %}}

_**If select Inaccessible:**_

Select this option if the Jira app can’t access the Jenkins site directly.

Click **Save and Connect** to accept connection defaults and connect the site. See 
[Jenkins Sites]({{< relref "/administration/jenkins-sites" >}}) for the all the details and defaults.

### _Under Connect Site select the method for connecting the Jenkins site_

* Select **Automatic** to let the app register itself with Jenkins. You will need to provide Jenkins administrative credentials to use 
  this. (Only available for Accessible Sites) 
* Select **Manual** to use the provided **Registration Link** to manually register the Jira site in Jenkins.
* Select **Configuration as Code** to use the Jenkins Configuration as Code feature to register the Jira site through configuration code.

Click **Connect** to finalise the connection.

## Recommended Reading

We strongly recommend you read about [how data synchronization works and how builds are linked to issues]({{< relref "/data-synchronization" >}}). 
It is also recommended to read about [the other options available when connecting a site]({{< relref "/administration/jenkins-sites" >}}). 
