---
title: "Deployment Build Marker"
date: 2020-07-01T14:07:00Z
draft: false

aliases:
  - features/jira-features/development-panel-integration/
  - features/jenkins-features/deployment-build-marker/
  - insight/deployment-build-marker/

weight: 3

menu:
  docs:
    parent: features
---

The Deployment Build Marker can be used to mark a build as a deployment to a specific environment.

## Pipelines

In a pipeline job, builds can be marked using the generic `step` declaration. The step takes three arguments, namely `environmentId`,
 `environmentName`, and `environmentType`. Both `environmentId` and `environmentName` are strings where the id is limited to 40
 characters and the name to 255 characters, whereas `environmentType` can have any of the following values, `development`, `testing`,
 `staging`, `production` and `unmapped`. The last is also used in case a different value is provided.

    pipeline {
      agent any
      stages {
        stage('Deploy to Staging') {
          steps {
            echo 'Build and deploy to staging'
          }
          post {
            always {
              step([$class: 'DeploymentBuildMarker', environmentId: 'eu-staging-1', environmentName: 'Staging', environmentType: 'staging'])
            }
          }
        }
      }
    }

At a bare minimum the Deployment Build Marker only needs the environmentType when it's executing as part of a pipeline. Take the snippet below.

    pipeline {
      agent any
      stages {
        stage('Deploy to Staging') {
          steps {
            echo 'Build and deploy to staging'
          }
          post {
            always {
              step([$class: 'DeploymentBuildMarker', environmentType: 'production'])
            }
          }
        }
      }
    }

In this case the `environmentName` will be set to `production`, and the `environmentId` will be generated using the `environmentName` as
 input.

{{< tip >}}
The pipeline step for the Deployment Build Marker can be used multiple times within a single build execution.
{{</ tip >}}

{{< tip >}}
If using `DeploymentBuildMarker` as value for the `$class` argument is giving issues, then try the fully qualified class value
 `org.marvelution.jji.marker.DeploymentBuildMarker`. 
{{</ tip >}}

## Traditional Jobs

In a traditional Jenkins job, like a free style job, builds can be marked using the `Deployment Build Marker` as post build action, simply
 add the action and specify the environment id, name and type using the action form. 

{{< note >}}
If no `environmentId` is specified in the configuration, then one is generated using the `environmentName` as input.
{{</ note >}}

{{< note >}}
Using the Deployment Build Marker as post build action has the limitation that the action can only be configured once per job.
{{</ note >}}

## Development Panel Integration

Builds tht represent a deployment to an environment show also listed as a deployment within Jira through the Development Status panel and 
related Jenkins data dialog.
