---
title: "Search by Build"
date: 2020-07-01T14:07:00Z
draft: false

aliases:
  - features/jira-features/jql-functions/
  - features/jira-features/entity-properties/
  - insight/search/

weight: 2                    

menu:
  docs:
    parent: features
---

## Entity Properties

The build to issue index is available as indexed issue entity properties that can be used in JQL when searching for issues, the following
 index attributes are available:

**jenkinsTotal**
Number field containing the total number of unique jobs.

**jenkinsBuildTotal**
Number field containing the total number of unique builds.

**jenkinsWorstresult**
String field containing the worst job result. From worst to best, `UNKNOWN`, `ABORTED`, `NOT_BUILT`, `FAILURE`, `UNSTABLE`, `SUCCESS`.

**jenkinsSuccess**
Number field containing the total number of unique jobs where the last build was successful.

**jenkinsUnstable**
Number field containing the total number of unique jobs where the last build was unstable.

**jenkinsFailure**
Number field containing the total number of unique jobs where the last build has failed.

**jenkinsAborted**
Number field containing the total number of unique jobs where the last build was aborted.

**jenkinsNotbuilt**
Number field containing the total number of unique jobs where the last build was not built.

**jenkinsUnknown**
Number field containing the total number of unique jobs where the last build has an unknown result state.

**jenkinsDeployTotal**
Number field containing the total number of deployment builds.

**jenkinsDeployWorstresult**
String field containing the worst deployment result. From worst to best, `UNKNOWN`, `ABORTED`, `NOT_BUILT`, `FAILURE`, `UNSTABLE`, 
`SUCCESS`.

**jenkinsDeploySuccess**
Number field containing the total number of deployments that where successful.

**jenkinsDeployUnstable**
Number field containing the total number of deployments that where unstable.

**jenkinsDeployFailure**
Number field containing the total number of deployments that where failed.

**jenkinsDeployAborted**
Number field containing the total number of deployments that where aborted.

**jenkinsDeployNotbuilt**
Number field containing the total number of deployments that where not built.

**jenkinsDeployUnknown**
Number field containing the total number of deployments that have an unknown result state.

{{% server %}}
## JQL Functions

**issuesRelatedToJob()**
This function can be used to get all the issues that are related to the specified Jenkins Job. It only takes one argument, the Job name.

**issuesRelatedToBuild()**
This function can be used to get all the issues that are related to the specified build, and can be used in two ways:

1.  Only provide a single argument; search for issues related to a specific build identified by its `id`.
1.  Provide two arguments; search for issues related to a specific build identified by the `job name` and `build number`.
1.  Provide three arguments; search for issues related to a range of builds identified by the `job name` range `start` and `end` build
    numbers.
{{%/ server %}}
