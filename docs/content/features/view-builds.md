---
title: "View Build Data"
date: 2020-07-01T14:07:00Z
draft: false

aliases:
  - features/jira-features/ci-build-panels/
  - features/jira-features/release-report/
  - features/jira-features/activity-streams/
  - insight/view-builds/
  - insight/build-links/
  - insight/development-panel/

weight: 1

menu:
  docs:
    parent: features
---

{{< note >}}
Users must either have the **View Developer Tools** or the **View Jenkins Data** project permission in order to view Jenkins build data 
in Jira.
{{< /note >}}

{{% server %}}
## Activity Streams

This Activity Stream provider can be used to display Jenkins builds in the Activity Stream gadget and in the project summary stream panel.
It currently supports the displaying of builds that are synced and will take the Project and Issue Key filters into account when
 selecting the builds to display.

{{< note >}}
Build activities are not mapped to users within Jira, but whether the Jenkins Site that executed the build. This is because the users in
 Jenkins are not mapped to users in Jira.
This is also the reason, that builds are filtered out when you enable any user based filter in the activity stream.{{</ note >}}

## Release Report

The Release Report feature is an extension of the Release Report development column (the column on the far right of the issues table).

The 'Development' column will in addition to, the commits, builds, and deployments, display the Jenkins builds related to an issue,
 provided that Jira Software is connected to the required development tool. The Jenkins build information is visualized using the
 following icons:

|                                 Icon                                  | Meaning                                                                  |
|:---------------------------------------------------------------------:|--------------------------------------------------------------------------|
| ![Successful Build](../../images/jji/build-statuses/success-icon.png) | Successful build                                                         |
|   ![Failed Build](../../images/jji/build-statuses/failure-icon.png)   | Failed build                                                             |
| ![Unstable Build](../../images/jji/build-statuses/unstable-icon.png)  | Unstable build                                                           |
|   ![Other Status](../../images/jji/build-statuses/unknown-icon.png)   | Other states that doesn't impact development, like not built, or aborted |

When you click on a build result icon in the right column will show a dialog with more detailed information about the latest related
 build together with test results, duration and completion time.
{{%/ server %}}

{{% cloud %}}
## Issue Action

For the new Issue View in Jira, then add-on provides a view that contains a summary of all related jobs and there latest builds. The
 panel also provides access to see all related builds and the ability to trigger a build or one or multiple jobs.
{{%/ cloud %}}

## Development Status Panel

A summary of build and deployment data is displayed in a small panel, similar to Jira's development status panel, and provides access to a 
 dialog containing all related build and deployment information, similar to the Issue Action panel.

## Issue Tab Panel

A new Tab Panel is provided to display Builds that where synced from Jenkins and are related to an issue.

{{% server %}}
Ordering of builds is supported to display builds in both oldest first (ascending) and newest first (descending) order.
But the user can switch between the ordering using the small order by arrow button that is displayed on the top right of the tab panel.
{{%/ server %}}
