---
title: "Trigger a Build"
date: 2020-07-01T14:07:00Z
draft: false

aliases:
  - features/jira-features/triggering-builds/
  - action/trigger-build/

weight: 4

menu:
  docs:
    parent: features
---

{{< note >}}
Triggering builds in Jenkins from Jira requires that the user performing the action either manually or through a workflow 
post function has the **Trigger Jenkins Build** project permission.

* For global configured Jenkins site, the user needs to have the permission assigned to at least one project.
* For project configured Jenkins site, the user needs to have the permission assigned to the project where the Jenkins site was 
  configured in. 

{{< /note >}}

There are two ways to trigger a build from within Jira.

1. [Manually by using the Trigger Build dialog](#trigger-a-build-manually).
2. [Automated by configuring the 'Trigger a build in Jenkins' Workflow post function](#trigger-a-build-as-part-of-a-transition).

##  Trigger a build manually

Builds can be triggered from within Jira using the **Build** issue action or by opening the Issue Action panel and using the button
**Trigger a Build** to open the Trigger Build dialog box.

With the Trigger a Build dialog open, you can simply select related Jobs or use the auto-complete search box to search for other jobs.

##  Trigger a build as part of a transition

Builds can be triggered as part of an issue transition by configuring the 'Trigger a build in Jenkins' workflow post function.

The workflow post function can be configured to trigger a build of:
* a specific job, configure it multiple times to trigger multiple specific jobs,
* all jobs related to the issue being transitioned,
* all jobs that match the value of an issue field of the issue being transitioned,
* or all jobs that have the key of the issue being transitioned in its name. 

{{< note >}}The user that configures the 'Trigger a build in Jenkins' workflow post function needs to have the **Trigger a Build** project 
permission. Any user can trigger a build using the workflow post function even if that user doesn't have the 'Trigger a Build' permission
{{< /note >}}
         
## Triggering a Parameterized Build

{{< note >}}
This requires version [3.11.0]({{< relref "/releases/jenkins/#r3-11-0" >}}) of the Jira Integration for Jenkins plugin.
{{</note >}}

Administrators can configure which issue fields are passed to Jenkins when triggering a build, see 
[Parameter Issue Fields]({{< relref "/administration/app-configuration#parameter-issue-fields" >}}).

Issue fields are only passed to Jenkins if the field was enabled as parameter issue field, and the field has a value on the issue where
the trigger was sends from. Parameter names are created by taking the issue field name and replacing any space with an underscore. The
Jira Integration for Jenkins plugin will match parameters to actual job parameters. 

By default, the following parameters are available even if no issue fields are configured as build parameter:

| Parameter Names     | Description                                       |
|---------------------|---------------------------------------------------|
| issueKey, issue_key | Key of the issue where the trigger was send from. |
| issueUrl, issue_url | URL of the issue where the trigger was send from. |

Matching of Jira send parameters to actual job parameters is case-insensitive. For example, if the 'Fix versions' field in
enabled, Jira sends a parameter named 'Fix_versions' and Jenkins will match 'Fix_versions', 'fix_versions' and 'FIX_VERSIONS'.

##  Access Trigger Data

You can use the key of the issue from where you triggered the build in Jira within your build in Jenkins. When a build is triggered from
Jira a special build cause is used, this cause is of type `org.marvelution.jji.trigger.JiraCause` and contains two fields `issueKey` and
`by`, which are both accessible through a getter, `getIssueKey()` and `getBy()` respectively. The `by` field contains the Display name or
id of the user that triggered the build from Jira, the `issueKey` field as is describes contains the key of the issue in Jira.

If you want the user's id to be sent instead of the display name, then select the **User Id** option, see Configuration
[User Id vs Display Name]({{< relref "/administration/app-configuration#user-id-vs-display-name" >}}).

So you could obtain and use the issue key using the following Jenkins pipeline snippet. 

    def jiraCauses = currentBuild.getBuildCauses('org.marvelution.jji.trigger.JiraCause')
    
    // This results in a JSON array for Jira Trigger build causes like this:
    //[
    //  {
    //    "_class":"org.marvelution.jji.trigger.JiraCause"
    //    "shortDescription":"Started by admin through TEST-10",
    //    "by":"admin",
    //    "issueKey":"TEST-10"
    //  }
    //]
    
    def issueKey = jiraCauses[0]['issueKey']

Or for short.

    def issueKey = currentBuild.getBuildCauses('org.marvelution.jji.trigger.JiraCause')[0]['issueKey']

You can also obtain the issue key through the raw build reference, see snippet below.

    def issueKey = currentBuild.rawBuild.getBuildCauses('org.marvelution.jji.trigger.JiraCause').getIssueKey()

{{< note >}}
This may require that the Jenkins administrator approved the script signature for execution.
{{< /note >}}
