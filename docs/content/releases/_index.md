---
title: "Releases"
description: Explore the release history of the Jira app and Jenkins plugin. 
date: 2020-07-01T14:07:00Z
draft: false
layout: index

aliases:
  - release-notes/

weight: 99

menu:
  docs:
    identifier: releases

illustration: release-notes

---
