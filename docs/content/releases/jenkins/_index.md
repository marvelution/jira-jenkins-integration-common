---
title: "Jira Integration (for Jenkins)"
date: 2017-10-16T00:00:00Z

aliases:
  - release-notes/jenkins/
                                                    
menu:
  docs:
    parent: releases
---

Please refer to the [Releases](https://plugins.jenkins.io/jira-integration/releases/) on the plugin listings page at Jenkins for release 
details.

{{< info >}}
Please reach out to [support](https://getsupport.marvelution.com) if you are looking for an older version of the plugin that is not 
listed on the releases page.
{{< /info >}}
