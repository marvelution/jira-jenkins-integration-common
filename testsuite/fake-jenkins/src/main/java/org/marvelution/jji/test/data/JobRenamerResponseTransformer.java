/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.test.data;

import java.io.*;
import java.util.regex.*;

import com.github.tomakehurst.wiremock.common.*;
import com.github.tomakehurst.wiremock.extension.*;
import com.github.tomakehurst.wiremock.http.*;
import jakarta.json.*;

public class JobRenamerResponseTransformer
        extends ResponseTransformer
{
    @Override
    public Response transform(
            Request request,
            Response response,
            FileSource files,
            Parameters parameters)
    {
        Pattern pattern = Pattern.compile("/job/([a-zA-Z\\-]+)(/(\\d+))?/api/json/(.*)?");
        Matcher matcher = pattern.matcher(request.getUrl());

        if (matcher.matches())
        {
            String jobName = matcher.group(1);
            String newJobName = System.getProperty("job.rename." + jobName);
            if (newJobName == null || newJobName.isEmpty())
            {
                return response;
            }

            try (JsonReader reader = jakarta.json.Json.createReader(new InputStreamReader(response.getBodyStream())))
            {
                JsonObject json = reader.read()
                        .asJsonObject();
                JsonObjectBuilder jsonBuilder = jakarta.json.Json.createObjectBuilder(json);

                String buildNumber = matcher.group(3);

                if (buildNumber == null)
                {
                    jsonBuilder.add("displayName", newJobName);
                }

                return Response.Builder.like(response)
                        .body(jsonBuilder.build()
                                .toString())
                        .build();
            }
        }
        else
        {
            return response;
        }
    }

    @Override
    public String getName()
    {
        return "job-renamer";
    }
}
