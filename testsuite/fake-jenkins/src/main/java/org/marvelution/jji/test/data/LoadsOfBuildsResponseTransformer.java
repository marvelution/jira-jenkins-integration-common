/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.test.data;

import java.io.*;
import java.time.*;
import java.time.temporal.*;
import java.util.regex.*;

import com.github.tomakehurst.wiremock.common.*;
import com.github.tomakehurst.wiremock.extension.*;
import com.github.tomakehurst.wiremock.http.*;
import jakarta.json.Json;
import jakarta.json.*;

import static java.util.stream.IntStream.*;

public class LoadsOfBuildsResponseTransformer
        extends ResponseTransformer
{

    @Override
    public String getName()
    {
        return "loads-of-builds";
    }

    @Override
    public Response transform(
            Request request,
            Response response,
            FileSource files,
            Parameters parameters)
    {
        HttpHeader countHeader = response.getHeaders()
                .getHeader("X-BuildCount");

        if (!countHeader.isPresent())
        {
            return response;
        }

        int buildCount = Integer.parseInt(countHeader.firstValue());

        Pattern pattern = Pattern.compile("/job/([a-zA-Z\\-]+)(/(\\d+))?/api/json/(.*)?");
        Matcher matcher = pattern.matcher(request.getUrl());

        if (matcher.matches())
        {
            try (JsonReader reader = jakarta.json.Json.createReader(new InputStreamReader(response.getBodyStream())))
            {
                JsonObject json = reader.read()
                        .asJsonObject();
                JsonObjectBuilder jsonBuilder = jakarta.json.Json.createObjectBuilder(json);
                String group = matcher.group(3);
                if (group == null)
                {
                    String url = json.getString("url");

                    jsonBuilder.add("firstBuild",
                            jakarta.json.Json.createObjectBuilder()
                                    .add("number", 1)
                                    .add("url", url + "1/"));
                    jsonBuilder.add("lastBuild",
                            jakarta.json.Json.createObjectBuilder()
                                    .add("number", buildCount)
                                    .add("url", url + buildCount + "/"));

                    JsonArrayBuilder builds = jakarta.json.Json.createArrayBuilder();
                    range(1, buildCount + 1).mapToObj(n -> Json.createObjectBuilder()
                                    .add("number", n)
                                    .add("url", url + n + "/"))
                            .forEach(builds::add);
                    jsonBuilder.add("builds", builds);
                }
                else
                {
                    int number = Integer.parseInt(group);
                    Instant timestamp = Instant.now()
                            .minus(buildCount, ChronoUnit.HOURS);
                    jsonBuilder.add("number", number)
                            .add("url", json.getString("url") + number + "/")
                            .add("timestamp", timestamp.plus(number, ChronoUnit.HOURS).toEpochMilli());
                }
                return Response.Builder.like(response)
                        .body(jsonBuilder.build()
                                .toString())
                        .build();
            }
        }
        else
        {
            return response;
        }
    }
}
