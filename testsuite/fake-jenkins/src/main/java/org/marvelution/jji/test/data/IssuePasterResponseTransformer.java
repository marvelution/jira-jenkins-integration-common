/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.test.data;

import java.io.*;
import java.util.*;
import java.util.stream.*;

import com.github.tomakehurst.wiremock.common.*;
import com.github.tomakehurst.wiremock.extension.*;
import com.github.tomakehurst.wiremock.http.*;
import jakarta.json.Json;
import jakarta.json.*;

public class IssuePasterResponseTransformer
        extends ResponseTransformer
{
    public static final Random RANDOM = new Random();
    public static final String DESCRIPTION = "description";
    private final String projectKey;
    private final int issueCount;

    public IssuePasterResponseTransformer(
            String projectKey,
            int issueCount)
    {
        this.projectKey = projectKey;
        this.issueCount = issueCount;
    }

    @Override
    public Response transform(
            Request request,
            Response response,
            FileSource files,
            Parameters parameters)
    {
        if (!Objects.equals("application/json",
                response.getHeaders()
                        .getContentTypeHeader()
                        .mimeTypePart()))
        {
            return response;
        }

        try (JsonReader reader = Json.createReader(new InputStreamReader(response.getBodyStream())))
        {
            JsonObject json = reader.read()
                    .asJsonObject();

            if (json.containsKey(DESCRIPTION) && !json.isNull(DESCRIPTION))
            {

                JsonObjectBuilder jsonBuilder = Json.createObjectBuilder(json);

                String issueKeys = IntStream.range(0, RANDOM.nextInt(4))
                        .mapToObj(n -> projectKey + "-" + (RANDOM.nextInt(issueCount) + 1))
                        .collect(Collectors.joining(", "));
                jsonBuilder.add(DESCRIPTION, json.getString(DESCRIPTION) + " " + issueKeys);

                return Response.Builder.like(response)
                        .body(jsonBuilder.build()
                                .toString())
                        .build();
            }
            else
            {
                return response;
            }
        }
    }

    @Override
    public String getName()
    {
        return "issue-paster";
    }
}
