/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.tests.assertions;

import com.microsoft.playwright.Locator;
import com.microsoft.playwright.assertions.LocatorAssertions;

import static com.microsoft.playwright.assertions.PlaywrightAssertions.assertThat;

public class JobListAssertions
{
    private final Locator list;

    JobListAssertions(Locator list)
    {
        this.list = list;
    }

    public JobListAssertions isVisible(LocatorAssertions.IsVisibleOptions options)
    {
        assertThat(list).isVisible(options);
        return this;
    }

    public JobListAssertions hasCount(int count)
    {
        assertThat(jobs()).hasCount(count);
        return this;
    }

    public JobRowAssertions job(String id)
    {
        return new JobRowAssertions(list.locator("[jenkins-job-id='" + id + "']"));
    }

    private Locator jobs()
    {
        return list.locator("tbody > tr");
    }

    public static class JobRowAssertions
    {
        private final Locator row;

        JobRowAssertions(Locator row)
        {
            this.row = row;
        }

        public Locator toggle()
        {
            return row.locator("aui-toggle");
        }

        public JobRowAssertions toggleIs(LocatorAssertions.IsEnabledOptions options)
        {
            LocatorAssertions locatorAssertions = assertThat(toggle());
            if (options.enabled)
            {
                locatorAssertions = locatorAssertions.not();
            }
            locatorAssertions.hasAttribute("disabled", "disabled");
            return this;
        }

        public JobRowAssertions toggleIs(LocatorAssertions.IsCheckedOptions options)
        {
            LocatorAssertions locatorAssertions = assertThat(toggle());
            if (!options.checked)
            {
                locatorAssertions = locatorAssertions.not();
            }
            locatorAssertions.hasAttribute("checked", "checked");
            return this;
        }

        public Locator moreActions()
        {
            return row.locator(".aui-button-split-more");
        }

        public JobRowAssertions moreActionsIs(LocatorAssertions.IsEnabledOptions options)
        {
            assertThat(moreActions()).isEnabled(options);
            return this;
        }

        public JobActionsAssertions assertActions()
        {
            String id = row.getAttribute("jenkins-job-id");
            return new JobActionsAssertions(row.locator("#job-menu-dropdown-" + id));
        }

        public JobRowAssertions hasName(String name)
        {
            assertThat(linkTo()).hasText(name);
            return this;
        }

        public JobRowAssertions hasOlderBuild(int number)
        {
            assertThat(column(2)).hasText(String.valueOf(number));
            return this;
        }

        public JobRowAssertions hasLastBuild(int number)
        {
            assertThat(column(3)).hasText(String.valueOf(number));
            return this;
        }

        public JobRowAssertions hasNoBuildsYet(boolean linked)
        {
            LocatorAssertions column = assertThat(column(2));
            if (linked)
            {
                column.hasText("No builds synchronized yet.");
            }
            else
            {
                column.isEmpty();
            }
            return this;
        }

        public Locator linkTo()
        {
            return column(1).locator("a");
        }

        private Locator column(int index)
        {
            return row.locator("td")
                    .nth(index);
        }
    }

}
