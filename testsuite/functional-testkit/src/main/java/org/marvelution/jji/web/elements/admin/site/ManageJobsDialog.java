/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.elements.admin.site;

import org.marvelution.jji.model.request.*;
import org.marvelution.jji.web.elements.form.*;
import org.marvelution.jji.web.elements.message.*;

import com.codeborne.selenide.*;
import org.openqa.selenium.*;

/**
 * @author Mark Rekveld
 * @since 1.12.0
 */
public class ManageJobsDialog
		extends FormDialog<ManageJobsDialog>
{

	public ManageJobsDialog(SelenideElement opener)
	{
		super(opener);
	}

	public Message message()
	{
		return new Message(child(By.className("aui-message")));
	}

	public CheckboxField operation(BulkRequest.Operation operation)
	{
		return new CheckboxField(form(), "op_" + operation.name());
	}

	public Select2Field jobSearch()
	{
		return select2("jobIds");
	}
}
