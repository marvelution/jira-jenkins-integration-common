/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.elements.form;

import com.codeborne.selenide.SelenideElement;

public interface Form
{

    SelenideElement form();

    default HiddenField hidden(String name)
    {
        return new HiddenField(form(), name);
    }

    default CheckboxField checkbox(String name)
    {
        return new CheckboxField(form(), name);
    }

    default RadioField radio(String name)
    {
        return new RadioField(form(), name);
    }

    default TextField text(String name)
    {
        return new TextField(form(), name);
    }

    default PasswordField password(String name)
    {
        return new PasswordField(form(), name);
    }

    default SelectField select(String name)
    {
        return new SelectField(form(), name);
    }

    default Select2Field select2(String name)
    {
        return new Select2Field(form(), name);
    }
}
