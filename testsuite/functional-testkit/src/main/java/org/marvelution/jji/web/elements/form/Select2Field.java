/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.elements.form;

import com.codeborne.selenide.*;
import org.openqa.selenium.*;

import static com.codeborne.selenide.Selenide.*;
import static org.openqa.selenium.By.*;

public class Select2Field
		extends Field<Select2Field>
{

	private final String name;

	public Select2Field(
			SelenideElement form,
			String name)
	{
		super(form, "s2id_" + name);
		this.name = name;
	}

	@Override
	public Select2Field val(String value)
	{
		By selector = className("select2-input");
		SelenideElement input = input().find(selector);
		if (!input.exists())
		{
			input = getSelectDrop().find(selector);
		}
		input.val(value);
		return this;
	}

	public TextField inputField()
	{
		return new TextField(form, name);
	}

	public ElementsCollection results()
	{
		return getSelectDrop().findAll(className("select2-result"));
	}

	private SelenideElement getSelectDrop()
	{
		return $(id("select2-drop"));
	}
}
