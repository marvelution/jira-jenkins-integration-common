/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.elements.admin.dialog;

import org.marvelution.jji.web.elements.*;
import org.marvelution.jji.web.elements.message.*;

import com.codeborne.selenide.*;

import static org.openqa.selenium.By.*;

/**
 * @author Mark Rekveld
 * @since 1.14.0
 */
public class FeaturesDialog
		extends Dialog<FeaturesDialog>
{

	public FeaturesDialog(SelenideElement opener)
	{
		super(opener);
	}

	public FeatureList siteFeatures()
	{
		return new FeatureList(content().find(id("site-features")));
	}

	public FeatureList systemFeatures()
	{
		return new FeatureList(content().find(id("system-features")));
	}

	public static class FeatureList
	{

		private final SelenideElement content;

		private FeatureList(SelenideElement content)
		{
			this.content = content;
		}

		public SelenideElement title()
		{
			return content.find(tagName("h2"));
		}

		public Message message()
		{
			return new Message(content.find(className("aui-message")));
		}

		public ElementsCollection features()
		{
			return content.findAll(tagName("li"));
		}
	}
}
