/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.tests.panel;

import com.microsoft.playwright.Locator;

public class TriggerBuildDialog
{
    private final Locator dialog;

    public TriggerBuildDialog(Locator dialog)
    {
        this.dialog = dialog;
    }

    public Locator firewalledSiteMessage()
    {
        return dialog.locator(".aui-message-info");
    }

    public Locator noLinkedJobs()
    {
        return dialog.locator(".no-linked-jobs");
    }

    public Locator linkedJobs()
    {
        return dialog.locator(".jenkins-job-row");
    }

    public Locator otherJobs()
    {
        return dialog.locator("#s2id_other-jobs")
                .locator(".select2-input");
    }

    public Locator otherJobsResults()
    {
        return dialog.page()
                .locator("#select2-drop")
                .locator(".select2-result");
    }
}
