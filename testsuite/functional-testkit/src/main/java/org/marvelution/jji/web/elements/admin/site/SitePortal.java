/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.elements.admin.site;

import org.marvelution.jji.web.elements.form.*;

import com.codeborne.selenide.*;

abstract class SitePortal<S extends SitePortal<S>>
        extends Portal<S>
{

    SitePortal(SelenideElement opener)
    {
        super(opener);
    }

    public HiddenField id()
    {
        return hidden("id");
    }

    public HiddenField type()
    {
        return hidden("type");
    }

    public TextField name()
    {
        return text("name");
    }

    public TextField siteUrl()
    {
        return text("rpcUrl");
    }

    public RadioField accessibleInstance()
    {
        return new RadioField(form(), "conn-ACCESSIBLE");
    }

    public RadioField inaccessibleInstance()
    {
        return new RadioField(form(), "conn-INACCESSIBLE");
    }

    public HiddenField authentication()
    {
        return new HiddenField(form(), "authentication");
    }

    public TextField displayUrl()
    {
        return text("displayUrl");
    }

    public HiddenField scope()
    {
        return hidden("scope");
    }

    public Select2Field scopeSelector()
    {
        return select2("scope");
    }

    public CheckboxField enabled()
    {
        return checkbox("enabled");
    }

    public CheckboxField autoLinkNewJobs()
    {
        return checkbox("autoLinkNewJobs");
    }

    public CheckboxField useCrumbs()
    {
        return checkbox("useCrumbs");
    }
}
