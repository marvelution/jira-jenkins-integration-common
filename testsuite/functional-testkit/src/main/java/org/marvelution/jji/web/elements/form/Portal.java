/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.elements.form;

import java.util.HashMap;
import java.util.Map;

import org.marvelution.jji.web.elements.FlagsAndTips;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;

public abstract class Portal<P extends Portal<P>>
        implements Form
{
    private final SelenideElement opener;
    private final Map<By, SelenideElement> elements = new HashMap<>();

    protected Portal(SelenideElement opener)
    {
        this.opener = opener;
    }

    public SelenideElement opener()
    {
        return opener;
    }

    public SelenideElement header()
    {
        return portal().find(By.className("jenkins-portal-header"));
    }

    public SelenideElement page(int index)
    {
        return body().find(By.cssSelector("fieldset.jenkins-portal-body-page"), index);
    }

    public SelenideElement activePage()
    {
        return body().find(By.cssSelector("fieldset.jenkins-portal-body-page.active"));
    }

    protected SelenideElement body()
    {
        return portal().find(By.className("jenkins-portal-body"));
    }

    public SelenideElement footer()
    {
        return portal().find(By.className("jenkins-portal-footer"));
    }

    public SelenideElement button(String id)
    {
        return portal().find(By.id("jenkins-portal-" + id + "-button"));
    }

    protected SelenideElement portal()
    {
        return $(By.className("jenkins-portal"));
    }

    @Override
    public SelenideElement form()
    {
        return child(By.tagName("form"));
    }

    protected SelenideElement child(String selector)
    {
        return child(By.cssSelector(selector));
    }

    protected SelenideElement child(By selector)
    {
        return elements.computeIfAbsent(selector, key -> body().find(key));
    }

    public P open()
    {
        elements.clear();
        FlagsAndTips.removeAllFlagsAndTips();
        opener
                .shouldBe(visible)
                .click();
        // Assert first page is shown
        return (P) this;
    }

    public P close()
    {
        elements.clear();
        button("close")
                .shouldBe(visible)
                .click();
        // Assert first page is shown
        return (P) this;
    }

    public P back()
    {
        elements.clear();
        button("back")
                .shouldBe(visible)
                .click();

        return (P) this;
    }

    public P next()
    {
        elements.clear();
        button("next")
                .shouldBe(visible)
                .click();

        return (P) this;
    }

    public P done()
    {
        elements.clear();
        button("done")
                .shouldBe(visible)
                .click();

        return (P) this;
    }
}
