/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.tests.panel;

import java.util.Comparator;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;

import org.marvelution.jji.data.helper.PanelBuild;
import org.marvelution.jji.data.helper.PanelJob;
import org.marvelution.jji.web.tests.assertions.DevStatusAssertions;
import org.marvelution.jji.web.tests.assertions.JenkinsAssertions;

import com.microsoft.playwright.Page;
import org.junit.jupiter.api.Test;

import static java.util.Comparator.comparing;

public interface DevStatusPanelTests
        extends PanelTests
{

    DevStatusPanel openDevStatusPanel(
            Page page,
            String issueKey);

    @Test
    default void testDevStatusPanel_NoBuilds(Page page)
    {
        JenkinsAssertions.assertThat(openDevStatusPanel(page, getExistingIssueKey()))
                .hasMessage("No builds found.");
    }

    @Test
    default void testDevStatusPanel(Page page)
    {
        testDevStatusPanel(page, this::openDevStatusPanel, lastBuild -> formatDate(lastBuild.getDate()));
    }

    default void testDevStatusPanel(
            Page page,
            BiFunction<Page, String, DevStatusPanel> panelSupplier,
            Function<PanelBuild, String> dateCondition)
    {
        String issue = getExistingIssueKey();
        List<PanelJob> jobs = createPanelJobData(issue);

        int count = jobs.stream()
                .mapToInt(job -> job.getBuilds()
                        .size())
                .sum();
        PanelBuild lastBuild = jobs.stream()
                .map(PanelJob::getLastBuild)
                .max(comparing(PanelBuild::getDate))
                .orElseThrow();
        PanelBuild worstBuild = jobs.stream()
                .flatMap(job -> job.getBuilds()
                        .stream())
                .max(Comparator.comparing(PanelBuild::getResult)
                        .thenComparing(PanelBuild::getDate))
                .orElseThrow();

        DevStatusAssertions assertions = JenkinsAssertions.assertThat(panelSupplier.apply(page, issue))
                .messageNotVisible();

        assertions.buildStats()
                .hasButton(count + " builds")
                .hasIcon(worstBuild.getBuildColor(), worstBuild.getBuildIcon())
                .hasTimestamp(dateCondition.apply(lastBuild));

        PanelBuild lastDeploy = jobs.stream()
                .flatMap(job -> job.getBuilds()
                        .stream())
                .max(comparing(PanelBuild::getDate))
                .orElseThrow();
        PanelBuild worstDeploy = jobs.stream()
                .flatMap(job -> job.getBuilds()
                        .stream())
                .max(Comparator.comparing(PanelBuild::getResult)
                        .thenComparing(PanelBuild::getDate))
                .orElseThrow();

        assertions.deployStats()
                .hasButton(count + " deployments")
                .hasIcon(worstDeploy.getBuildColor(), worstDeploy.getBuildIcon())
                .hasTimestamp(dateCondition.apply(lastDeploy));
    }
}
