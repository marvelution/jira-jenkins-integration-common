/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.elements;

import java.time.*;
import java.util.function.*;

import org.marvelution.jji.test.condition.*;

import com.codeborne.selenide.*;
import com.codeborne.selenide.ex.*;
import com.codeborne.selenide.impl.*;
import org.openqa.selenium.*;

import static com.codeborne.selenide.Configuration.*;
import static com.codeborne.selenide.Selenide.*;
import static org.openqa.selenium.By.*;

/**
 * @author Mark Rekveld
 * @since 1.10.0
 */
public class Flags
{

    private final ElementsCollection flags;

    private Flags(ElementsCollection flags)
    {
        this.flags = flags;
    }

    public static Flags all()
    {
        return new Flags($(id("aui-flag-container")).findAll(className("aui-flag")));
    }

    /**
     * @since 1.14.0
     */
    public static void waitForFlag(Predicate<WebElement> predicate)
    {
        if (JiraEdition.current() == JiraEdition.CLOUD_APP)
        {
            // Flags are flaky when just testing the App UI without the Jira context
            // So sleep for one interval and return.
            sleep(pollingInterval);
            return;
        }
        all().flags.shouldHave(CollectionCondition.anyMatch("wait for flag", predicate), Duration.ofMillis(6000));
    }

    public Flags shouldHaveSize(int expectedSize)
    {
        flags.shouldHave(CollectionCondition.size(expectedSize));
        return this;
    }

    public Flags shouldHave(CollectionCondition... conditions)
    {
        flags.shouldHave(conditions);
        return this;
    }

    /**
     * @since 1.14.0
     */
    public Flag first()
    {
        return new Flag(all().flags.first());
    }

    public Flag flag(int index)
    {
        return new Flag(flags.get(index));
    }
}
