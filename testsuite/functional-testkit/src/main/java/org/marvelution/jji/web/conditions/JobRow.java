/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.conditions;

import java.util.*;

import org.marvelution.jji.model.*;
import org.marvelution.jji.test.license.*;
import org.marvelution.testing.webdriver.conditions.*;

import com.codeborne.selenide.*;
import org.openqa.selenium.*;

import static org.marvelution.jji.web.conditions.Conditions.*;

import static java.util.stream.Collectors.*;

public class JobRow
		extends Condition
{

	private final Job job;
	private final License license;

	JobRow(
			Job job,
			License license)
	{
		super("job");
		this.job = job;
		this.license = license;
	}

	@Override
	public boolean apply(
			Driver driver,
			WebElement element)
	{
		org.marvelution.jji.web.elements.admin.JobRow row = new org.marvelution.jji.web.elements.admin.JobRow(element);
		return checkToggle(row.linked()) && checkJobNameAndLink(row) && checkStats(row) && checkActions(row.actions());
	}

	@Override
	public String toString()
	{
		return super.toString() + " " + job;
	}

	private boolean checkToggle(SelenideElement toggle)
	{
		if ((job.isDeleted() || !license.valid()) && !toggle.is(Conditions.disabled))
		{
			return false;
		}
		else if (job.isLinked() && !toggle.is(Conditions.checked))
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	private boolean checkJobNameAndLink(org.marvelution.jji.web.elements.admin.JobRow row)
	{
		Condition text = text(job.getName());
		if (job.isDeleted())
		{
			return row.name().has(text);
		}
		return row.name().has(text) && row.linkTo().has(text);
	}

	private boolean checkStats(org.marvelution.jji.web.elements.admin.JobRow row)
	{
		if (!job.hasBuilds())
		{
			return ((!job.isLinked() && row.notSyncedMessage().is(empty)) ||
					(job.isLinked() && row.notSyncedMessage().has(text("No builds synchronized yet."))));
		}
		else
		{
			return row.oldestBuild().has(exactText(job.getOldestBuild() > 0 ? String.valueOf(job.getOldestBuild()) : "")) &&
					row.lastBuild().has(exactText(job.getLastBuild() > 0 ? String.valueOf(job.getLastBuild()) : ""));
		}
	}

	private boolean checkActions(ElementsCollection actions)
	{
		List<WebElement> elements = actions.stream().map(SelenideElement::getWrappedElement).collect(toList());
		List<Button> buttons = new ArrayList<>();
		if (!job.getSite().isInaccessibleSite())
		{
			buttons.add(button("Synchronize").state(license));
		}
		buttons.add(button("Action").state(license));
		return CompositeCondition.composite(buttons).test(elements);
	}
}
