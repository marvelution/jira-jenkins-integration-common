/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.tests.admin;

import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import org.marvelution.jji.data.services.api.ConfigurationService;
import org.marvelution.jji.model.*;
import org.marvelution.jji.test.license.License;
import org.marvelution.jji.web.conditions.Button;
import org.marvelution.jji.web.elements.admin.SiteActions;
import org.marvelution.jji.web.pages.JobListPage;
import org.marvelution.jji.web.pages.SiteListPage;
import org.marvelution.jji.web.tests.assertions.JenkinsAssertions;
import org.marvelution.jji.web.tests.playwright.PlaywrightExtension;
import org.marvelution.testing.wiremock.FileSource;
import org.marvelution.testing.wiremock.WireMockOptions;
import org.marvelution.testing.wiremock.WireMockServer;

import com.microsoft.playwright.Page;
import org.junit.jupiter.api.extension.ExtendWith;

import static com.codeborne.selenide.Condition.visible;
import static java.util.Collections.emptyMap;
import static java.util.stream.Collectors.toList;
import static org.marvelution.jji.web.conditions.Conditions.*;
import static org.marvelution.jji.web.elements.FlagsAndTips.removeAllFlagsAndTips;
import static org.marvelution.testing.webdriver.conditions.CompositeCondition.composite;
import static org.marvelution.testing.wiremock.FileSourceType.UNDER_CLASSPATH;

@WireMockOptions(fileSource = @FileSource(type = UNDER_CLASSPATH,
                                          path = "MinimalSite"))
@ExtendWith(PlaywrightExtension.class)
public interface SiteManagementTests
{

    AtomicInteger COUNTER = new AtomicInteger(1);

    Site getSite(Site site);

    List<Site> getSites();

    Job getJob(Job job);

    default Site createSite(WireMockServer server)
    {
        return createSite(server, ConfigurationService.GLOBAL_SCOPE);
    }

    default Site createSite(
            WireMockServer server,
            String scope)
    {
        return saveSite(new Site().setType(SiteType.JENKINS)
                        .setScope(scope)
                        .setName("Jenkins CI " + COUNTER.getAndIncrement())
                        .setAutoLinkNewJobs(true)
                        .setRpcUrl(server.serverUri())
                        .setDisplayUrl(URI.create("https://jenkins.example.com"))
                        .setAuthenticationType(SiteAuthenticationType.TOKEN)
                        .setAccessibleSite(),
                new Job().setDeleted(true)
                        .setName("deleted-job")
                        .setDisplayName("Deleted Job")
                        .setOldestBuild(1)
                        .setLastBuild(10),
                new Job().setLinked(true)
                        .setName("linked-job")
                        .setDisplayName("Linked Job"),
                new Job().setName("job")
                        .setDisplayName("Job"));
    }

    default Site createFirewalledSite(Job... jobs)
    {
        return createFirewalledSite(ConfigurationService.GLOBAL_SCOPE, jobs);
    }

    default Site createFirewalledSite(
            String scope,
            Job... jobs)
    {
        return saveSite(new Site().setType(SiteType.HUDSON)
                .setScope(scope)
                .setName("Hudson CI " + COUNTER.getAndIncrement())
                .setAutoLinkNewJobs(true)
                .setInaccessibleSite()
                .setRpcUrl(URI.create("http://hudson.example.com/" + scope)), jobs);
    }

    Site saveSite(
            Site site,
            Job... jobs);

    void navigateToManageSites(Page page);

    default void navigateToSite(
            Page page,
            Site site)
    {
        navigateToManageSites(page);

        page.locator("//*[@jenkins-site-id='" + site.getId() + "']")
                .locator("td")
                .nth(1)
                .locator("a")
                .click();
        JenkinsAssertions.assertHeader(page)
                .hasTitle(site.getName());
    }

    void navigateDirectToSite(
            Page page,
            Site site);

    default void navigateToScopedSite(
            Page page,
            Site site)
    {
        navigateToProjectSites(page, site.getScope());

        SiteListPage.siteById(site.getId())
                .linkTo()
                .click();

        removeAllFlagsAndTips();
    }

    default void navigateToSiteInScope(
            Page page,
            Site site,
            String scope)
    {
        navigateToProjectSites(page, scope);

        SiteListPage.siteById(site.getId())
                .linkTo()
                .click();

        removeAllFlagsAndTips();
    }

    default void navigateDirectToScopedSite(
            Page page,
            Site site)
    {
        navigateDirectToSiteInScope(page, site, site.getScope());
    }

    void navigateDirectToSiteInScope(
            Page page,
            Site site,
            String scope);

    default String navigateToProjectSites(Page page)
    {
        String projectKey = getExistingProjectKey();
        navigateToProjectSites(page, projectKey);
        return projectKey;
    }

    String getExistingProjectKey();

    void navigateToProjectSites(
            Page page,
            String projectKey);

    String createRestUriForSite(String siteId);

    default String createFormActionForSite(String siteId)
    {
        return createRestUriForSite(siteId);
    }

    String createRestUriForJob(String jobId);


    // Selenide specific
    default Button helpButton()
    {
        return button("Help");
    }

    default Map<String, String> synchronizeSiteButtonAttributes(Site site)
    {
        return createAttributeMap("data-rest",
                createRestUriForSite(site.getId()) + "/sync",
                "data-rest-method",
                "POST",
                "data-rest-success",
                "Successfully triggered synchronization of site " + site.getName() + ".",
                "data-rest-failed",
                "Failed to trigger synchronization of site " + site.getName() + ".");
    }

    default Map<String, String> cleanupJobsButtonAttributes(Site site)
    {
        return emptyMap();
    }

    default Map<String, String> autoLinkNewJobsButtonAttributes(Site site)
    {
        return createAttributeMap("data-rest",
                createRestUriForSite(site.getId()) + "/autolink",
                "data-rest-method",
                "POST",
                "data-rest-success",
                "Successfully disabled automatic linking of new jobs on " + site.getName() + ".",
                "data-rest-success-checked",
                "Successfully enabled automatic linking of new jobs on " + site.getName() + ".",
                "data-rest-failed",
                "Failed to update site configuration of " + site.getName() + ", try again later.");
    }

    default Map<String, String> enableSiteButtonAttributes(Site site)
    {
        return createAttributeMap("data-rest",
                createRestUriForSite(site.getId()) + "/enable",
                "data-rest-method",
                "POST",
                "data-rest-success",
                "Successfully disabled synchronization of " + site.getName() + ".",
                "data-rest-success-checked",
                "Successfully enabled synchronization of " + site.getName() + ".",
                "data-rest-failed",
                "Failed to update site configuration of " + site.getName() + ", try again later.");
    }

    default Map<String, String> manageJobsButtonAttributes(Site site)
    {
        return emptyMap();
    }

    default Map<String, String> addSiteButtonAttributes()
    {
        return emptyMap();
    }

    default Map<String, String> editSiteButtonAttributes(Site site)
    {
        return emptyMap();
    }

    default Map<String, String> deleteSiteButtonAttributes(Site site)
    {
        return emptyMap();
    }

    default Map<String, String> siteConnectionButtonAttributes(Site site)
    {
        return emptyMap();
    }

    default Button synchronizeSiteButton(
            Site site,
            License license)
    {
        return button("Synchronize", synchronizeSiteButtonAttributes(site)).state(license);
    }

    default Button siteActionsButton(
            Site site,
            License license)
    {
        return button("Actions",
                "id",
                "site-menu-dropdown-trigger-" + site.getId(),
                "aria-controls",
                "site-menu-dropdown-" + site.getId()).state(license);
    }

    default Button cleanupJobsButton(Site site)
    {
        return button("Cleanup Jobs", cleanupJobsButtonAttributes(site));
    }

    default Button autoEnableNewJobsButton(
            Site site,
            License license)
    {
        return button("Auto enable new Jobs", autoLinkNewJobsButtonAttributes(site)).state(license);
    }

    default Button manageJobsButton(
            Site site,
            License license)
    {
        return button("Manage Jobs", manageJobsButtonAttributes(site)).state(license);
    }

    default Button enabledSiteButton(
            Site site,
            License license)
    {
        return button("Enabled for Synchronization", enableSiteButtonAttributes(site)).state(license);
    }

    default Button editSiteButton(
            Site site,
            License license)
    {
        return button("Edit", editSiteButtonAttributes(site)).state(license);
    }

    default Button deleteSiteButton(
            Site site,
            License license)
    {
        return button("Delete", deleteSiteButtonAttributes(site)).state(license);
    }

    default Button siteConnectionButton(
            Site site,
            License license)
    {
        return button("Manage Connection", siteConnectionButtonAttributes(site)).state(license);
    }

    default Button goToSiteButton(Site site)
    {
        Map<String, String> attributes = new HashMap<>();
        attributes.put("target", "_top");
        attributes.put("href",
                site.getDisplayUrl()
                        .toASCIIString());
        return button("Go to site", attributes);
    }

    default void assertSiteActionsMenu(
            SiteActions actions,
            Site site,
            License license)
    {
        Button.State cleanupJobsState;
        if (license.valid() && site.hasDeletedJobs())
        {
            cleanupJobsState = Button.State.ENABLED;
        }
        else
        {
            cleanupJobsState = Button.State.DISABLED;
        }
        actions.cleanupJobs()
                .opener()
                .shouldBe(cleanupJobsButton(site).state(cleanupJobsState));
        actions.autoLinkNewJobs()
                .shouldBe(autoEnableNewJobsButton(site, license), checked);
        actions.manageJobs()
                .opener()
                .shouldBe(manageJobsButton(site, license));
        actions.enabled()
                .shouldBe(enabledSiteButton(site, license), checked);
        actions.edit()
                .opener()
                .shouldBe(editSiteButton(site, license));
        actions.delete()
                .opener()
                .shouldBe(deleteSiteButton(site, license));
        actions.connection()
                .opener()
                .shouldBe(siteConnectionButton(site, license));
        actions.goToSite()
                .shouldBe(visible, goToSiteButton(site));
    }

    default Map<String, String> synchronizeJobButtonAttributes(Job job)
    {
        return createAttributeMap("data-rest",
                createRestUriForJob(job.getId()) + "/sync",
                "data-rest-method",
                "PUT",
                "data-rest-success",
                "Successfully triggered synchronization of job " + job.getDisplayName() + ".",
                "data-rest-failed",
                "Failed to trigger synchronization of job " + job.getDisplayName() + ".");
    }

    default Map<String, String> clearCacheButtonAttributes(Job job)
    {
        return createAttributeMap("data-rest",
                createRestUriForJob(job.getId()) + "/builds",
                "data-rest-method",
                "DELETE",
                "data-rest-success",
                "Successfully triggered clearing build cache of job " + job.getDisplayName() + ".",
                "data-rest-failed",
                "Failed to trigger clearing the build cache of job " + job.getDisplayName() + ".");
    }

    default Map<String, String> rebuildCacheButtonAttributes(Job job)
    {
        return createAttributeMap("data-rest",
                createRestUriForJob(job.getId()) + "/rebuild",
                "data-rest-method",
                "POST",
                "data-rest-success",
                "Successfully triggered rebuilding the build cache of job " + job.getDisplayName() + ".",
                "data-rest-failed",
                "Failed to trigger rebuilding the build cache of job " + job.getDisplayName() + ".");
    }

    default Button synchronizeJobButton(
            Job job,
            License license)
    {
        return button("Synchronize", synchronizeJobButtonAttributes(job)).state(license);
    }

    default Button jobActionsButton(
            Job job,
            License license)
    {
        return button("Actions",
                "id",
                "job-menu-dropdown-trigger-" + job.getId(),
                "aria-controls",
                "job-menu-dropdown-" + job.getId()).state(license);
    }

    default Button clearCacheButton(
            Job job,
            License license)
    {
        return button("Clear Build Cache", clearCacheButtonAttributes(job)).state(license);
    }

    default Button rebuildCacheButton(
            Job job,
            License license)
    {
        return button("Rebuild Build Cache", rebuildCacheButtonAttributes(job)).state(license);
    }

    default Button goToJobButton(Job job)
    {
        Map<String, String> attributes = new HashMap<>();
        attributes.put("target", "_top");
        attributes.put("href",
                job.getDisplayUrl()
                        .toASCIIString());
        return button("Go to job", attributes);
    }

    default void assertJobsList(
            Page page,
            JobsContainer jobsContainer,
            License license)
    {
        JobListPage.jobList()
                .shouldBe(visible);
        JobListPage.jobs()
                .shouldHave(composite(jobsContainer.getJobs()
                        .stream()
                        .map(job -> row(job, license))
                        .collect(toList())));
    }
}
