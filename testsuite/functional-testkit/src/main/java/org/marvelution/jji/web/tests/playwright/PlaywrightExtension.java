/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.tests.playwright;

import java.lang.reflect.Method;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.ServiceLoader;

import com.microsoft.playwright.*;
import org.junit.jupiter.api.extension.*;

public class PlaywrightExtension
        implements BeforeAllCallback, AfterAllCallback, BeforeEachCallback, ParameterResolver, TestWatcher
{
    private static final ExtensionContext.Namespace NAMESPACE = ExtensionContext.Namespace.create(PlaywrightExtension.class);
    private static final String JIRA_BASE_URL = "jira.baseUrl";

    @Override
    public void beforeAll(ExtensionContext context)
    {
        Playwright playwright = Playwright.create();
        Browser browser = playwright.chromium()
                .launch(new BrowserType.LaunchOptions().setChannel("chromium"));

        context.getStore(NAMESPACE)
                .put("playwright", playwright);
        context.getStore(NAMESPACE)
                .put("browser", browser);
    }

    @Override
    public void beforeEach(ExtensionContext context)
    {
        Browser browser = context.getStore(NAMESPACE)
                .get("browser", Browser.class);

        BrowserContext browserContext = browser.newContext(new Browser.NewContextOptions().setBaseURL(System.getProperty(JIRA_BASE_URL)));
        browserContext.setDefaultTimeout(60000);
        browserContext.tracing()
                .start(new Tracing.StartOptions().setName(context.getDisplayName())
                        .setScreenshots(true)
                        .setSnapshots(true)
                        .setSources(true));
        Page page = browserContext.newPage();

        context.getStore(NAMESPACE)
                .put("browserContext", browserContext);
        context.getStore(NAMESPACE)
                .put("page", page);
    }

    @Override
    public void testDisabled(
            ExtensionContext context,
            Optional<String> reason)
    {
        cleanup(context);
    }

    @Override
    public void testSuccessful(ExtensionContext context)
    {
        storeTrace(context, false);
        cleanup(context);
    }

    @Override
    public void testAborted(
            ExtensionContext context,
            Throwable cause)
    {
        storeTrace(context, true);
        cleanup(context);
    }

    @Override
    public void testFailed(
            ExtensionContext context,
            Throwable cause)
    {
        storeTrace(context, true);
        cleanup(context);
    }

    private void storeTrace(
            ExtensionContext context,
            boolean failed)
    {
        BrowserContext browserContext = context.getStore(NAMESPACE)
                .get("browserContext", BrowserContext.class);
        String methodName = context.getTestMethod()
                .map(Method::getName)
                .orElse("unknown-test-method");
        String zipName = "tracing";
        if (!context.getDisplayName()
                .startsWith(methodName))
        {
            zipName += "-" + context.getDisplayName()
                    .replaceAll("[^A-Za-z0-9\\-_]", "");
        }
        if (failed)
        {
            zipName += "-failed";
        }
        zipName += ".zip";
        browserContext.tracing()
                .stop(new Tracing.StopOptions().setPath(Paths.get(System.getProperty("reports.dir", "failsafe-reports"),
                        context.getTestClass()
                                .map(Class::getName)
                                .orElse("unknown-test-class"),
                        methodName,
                        zipName)));
    }

    private void cleanup(ExtensionContext context)
    {
        Page page = context.getStore(NAMESPACE)
                .get("page", Page.class);
        page.close();
        BrowserContext browserContext = context.getStore(NAMESPACE)
                .get("browserContext", BrowserContext.class);
        browserContext.close();
    }

    @Override
    public void afterAll(ExtensionContext context)
    {
        Browser browser = context.getStore(NAMESPACE)
                .get("browser", Browser.class);
        browser.close();
        Playwright playwright = context.getStore(NAMESPACE)
                .get("playwright", Playwright.class);
        playwright.close();
    }

    @Override
    public boolean supportsParameter(
            ParameterContext parameterContext,
            ExtensionContext extensionContext)
            throws ParameterResolutionException
    {
        return isPageParameter(parameterContext);
    }

    @Override
    public Object resolveParameter(
            ParameterContext parameterContext,
            ExtensionContext extensionContext)
            throws ParameterResolutionException
    {
        if (isPageParameter(parameterContext))
        {
            Optional<ExtendedPageFactory> factory = ServiceLoader.load(ExtendedPageFactory.class)
                    .findFirst();
            return factory.orElse(ExtendedPage::new)
                    .extend(extensionContext.getStore(NAMESPACE)
                            .get("page", Page.class), extensionContext);
        }
        else
        {
            return null;
        }
    }

    private boolean isPageParameter(ParameterContext parameterContext)
    {
        return parameterContext.getParameter()
                .getType()
                .equals(Page.class);
    }
}
