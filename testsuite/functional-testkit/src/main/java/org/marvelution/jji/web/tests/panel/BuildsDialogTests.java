/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.tests.panel;

import java.util.List;

import org.marvelution.jji.data.helper.PanelJob;

import com.microsoft.playwright.Locator;
import com.microsoft.playwright.Page;
import com.microsoft.playwright.options.AriaRole;
import org.junit.jupiter.api.Test;

import static com.microsoft.playwright.assertions.PlaywrightAssertions.assertThat;

public interface BuildsDialogTests
        extends PanelTests
{

    Locator openBuildsDialog(
            Page page,
            String issueKey);

    @Test
    default void testBuildsDialog(Page page)
    {
        String issue = getExistingIssueKey();
        List<PanelJob> jobs = createPanelJobData(issue);

        Locator dialog = openBuildsDialog(page, issue);

        Locator buildsTab = dialog.locator("#tabs-builds");
        Locator deploymentsTab = dialog.locator("#tabs-deployments");

        assertThat(deploymentsTab).not()
                .isVisible();
        assertThat(buildsTab).isVisible();
        assertThat(buildsTab.locator("tbody")
                .locator("tr")
                .locator("visible=true")).hasCount(jobs.size());

        dialog.locator("aui-toggle")
                .click();

        assertThat(buildsTab.locator("tbody")
                .locator("tr")
                .locator("visible=true")).hasCount(13);

        dialog.locator(".tabs-menu")
                .locator("a[role=tab]")
                .last()
                .click();

        assertThat(buildsTab).not()
                .isVisible();
        assertThat(deploymentsTab).isVisible();

        assertThat(deploymentsTab.locator("tbody")
                .locator("tr")
                .locator("visible=true")).hasCount(12);

        dialog.locator("aui-toggle")
                .click();

        assertThat(deploymentsTab.locator("tbody")
                .locator("tr")
                .locator("visible=true")).hasCount(1);
    }
}
