/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.pages;

import org.marvelution.jji.web.elements.admin.JobRow;
import org.marvelution.jji.web.elements.form.Select2Field;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.attribute;
import static com.codeborne.selenide.Condition.cssClass;
import static org.marvelution.jji.web.elements.admin.JobRow.JOB_ID_ATTR;
import static org.openqa.selenium.By.className;

/**
 * @author Mark Rekveld
 * @since 1.14.0
 */
public class JobListPage
        extends Page
{

    public static SelenideElement mainActionButton()
    {
        return header()
                .buttons()
                .find(cssClass("aui-button-split-main"));
    }

    public static SelenideElement linkedToggle()
    {
        return header()
                .asElement()
                .find(By.tagName("aui-toggle"));
    }

    public static Select2Field jobQuickSearch()
    {
        return new Select2Field(header().asElement(), "job-quick-search");
    }

    public static SelenideElement jobList()
    {
        return content().find(className("job-list"));
    }

    public static ElementsCollection jobs()
    {
        return jobList().findAll("tbody > tr");
    }

    public static JobRow jobById(String id)
    {
        return new JobRow(jobs().find(attribute(JOB_ID_ATTR, id)));
    }
}
