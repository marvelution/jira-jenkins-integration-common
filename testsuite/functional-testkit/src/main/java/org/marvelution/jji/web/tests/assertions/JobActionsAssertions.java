/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.tests.assertions;

import java.util.function.Consumer;

import com.microsoft.playwright.Locator;
import com.microsoft.playwright.assertions.LocatorAssertions;

import static com.microsoft.playwright.assertions.PlaywrightAssertions.assertThat;

public class JobActionsAssertions
{
    private final Locator actions;

    JobActionsAssertions(Locator actions)
    {
        this.actions = actions;
    }

    public JobActionsAssertions assertClearCache(Consumer<LocatorAssertions> assertions)
    {
        assertions.accept(assertThat(actions().nth(0)));
        return this;
    }

    public JobActionsAssertions assertRebuildCache(Consumer<LocatorAssertions> assertions)
    {
        assertions.accept(assertThat(actions().nth(1)));
        return this;
    }

    public JobActionsAssertions assertGoTo(Consumer<LocatorAssertions> assertions)
    {
        assertions.accept(assertThat(actions().last()));
        return this;
    }

    private Locator actions()
    {
        return actions.locator("aui-item-link");
    }
}
