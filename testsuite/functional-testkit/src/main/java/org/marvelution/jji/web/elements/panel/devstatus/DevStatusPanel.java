/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.elements.panel.devstatus;

import javax.annotation.*;

import com.codeborne.selenide.*;
import com.google.errorprone.annotations.*;

import static org.openqa.selenium.By.*;

public class DevStatusPanel
{

	private final SelenideElement container;

	public DevStatusPanel(SelenideElement container)
	{
		this.container = container;
	}

	public DevStatusPanel shouldBe(Condition... condition)
	{
		container.shouldBe(condition);
		return this;
	}

	public DevStatusPanel shouldNotBe(Condition... condition)
	{
		container.shouldNotBe(condition);
		return this;
	}

	public DevStatusPanel scrollIntoView()
	{
		container.scrollIntoView(true);
		return this;
	}

	public SelenideElement message()
	{
		return container.find(className("message-container"));
	}


	public DevStatusElement buildStats()
	{
		return new DevStatusElement(container.find(className("jenkins-build-status")));
	}

	public DevStatusElement deployStats()
	{
		return new DevStatusElement(container.find(className("jenkins-deploy-status")));
	}

	public static class DevStatusElement
	{

		private final SelenideElement element;

		private DevStatusElement(SelenideElement element)
		{
			this.element = element;
		}

		private SelenideElement left()
		{
			return element.find(className("left"));
		}

		private SelenideElement right()
		{
			return element.find(className("right"));
		}

		public SelenideElement button()
		{
			return left().find(tagName("a"));
		}

		public SelenideElement icon()
		{
			return left().find(className("aui-icon"));
		}

		public SelenideElement timestamp()
		{
			return right();
		}

		public DevStatusElement shouldBe(Condition... condition)
		{
			element.shouldBe(condition);
			return this;
		}

		public DevStatusElement shouldNotBe(Condition... condition)
		{
			element.shouldNotBe(condition);
			return this;
		}
	}
}
