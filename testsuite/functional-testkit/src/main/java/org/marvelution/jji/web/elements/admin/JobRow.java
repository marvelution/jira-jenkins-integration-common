/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.elements.admin;

import org.marvelution.jji.web.elements.*;

import com.codeborne.selenide.*;
import org.openqa.selenium.*;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;
import static org.openqa.selenium.By.*;

public class JobRow
		extends ActionableRow<JobActions>
{

	public static final String JOB_ID_ATTR = "jenkins-job-id";

	public JobRow(WebElement element)
	{
		this($(element));
	}

	public JobRow(SelenideElement container)
	{
		super(container);
	}

	public String jobId()
	{
		return container.attr(JOB_ID_ATTR);
	}

	public SelenideElement linked()
	{
		return column(0).find(By.tagName("aui-toggle"));
	}

	public SelenideElement name()
	{
		return column(1);
	}

	public SelenideElement linkTo()
	{
		return name().find(By.tagName("a"));
	}

	public SelenideElement oldestBuild()
	{
		return column(2);
	}

	public SelenideElement lastBuild()
	{
		return column(3);
	}

	public SelenideElement notSyncedMessage()
	{
		return column(2);
	}

	@Override
	public JobActions actionsDropDown()
	{
		return new JobActions(actions().find(cssClass("aui-button-split-more")));
	}
}
