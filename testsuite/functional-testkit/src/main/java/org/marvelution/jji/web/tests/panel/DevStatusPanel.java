/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.tests.panel;

import com.microsoft.playwright.Locator;

public class DevStatusPanel
{
    private final Locator panel;

    public DevStatusPanel(Locator panel)
    {
        this.panel = panel;
    }

    public Locator self()
    {
        return panel;
    }

    public Locator messageContainer()
    {
        return panel.locator(".message-container");
    }

    public Stats buildStats()
    {
        return new Stats(panel.locator(".jenkins-build-status"));
    }

    public Stats deployStats()
    {
        return new Stats(panel.locator(".jenkins-deploy-status"));
    }

    public static class Stats
    {
        private final Locator panel;

        Stats(Locator panel)
        {
            this.panel = panel;
        }

        public Locator button()
        {
            return panel.locator(".left")
                    .locator("a");
        }

        public Locator icon()
        {
            return panel.locator(".left")
                    .locator(".aui-icon");
        }

        public Locator timestamp()
        {
            return panel.locator(".right");
        }
    }
}
