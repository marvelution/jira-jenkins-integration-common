/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.elements;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

import org.marvelution.jji.test.condition.JiraEdition;

import com.codeborne.selenide.Screenshots;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static org.marvelution.jji.web.elements.FlagsAndTips.removeAllFlagsAndTips;

/**
 * Base element that describes a dialog.
 *
 * @author Mark Rekveld
 * @since 1.10.0
 */
@SuppressWarnings("unchecked")
public abstract class Dialog<D extends Dialog<D>>
{

    private final SelenideElement opener;
    private final JiraEdition jiraEdition;
    private final Map<By, SelenideElement> elements = new HashMap<>();

    protected Dialog(SelenideElement opener)
    {
        this.opener = opener;
        jiraEdition = JiraEdition.current();
    }

    public SelenideElement opener()
    {
        return opener;
    }

    public SelenideElement header()
    {
        switch (jiraEdition)
        {
            case CLOUD:
                return dialog().find(By.className("aui-dialog2-header"));
            case SERVER:
            case DATA_CENTER:
                return dialog().find(By.className("jira-dialog-heading"));
        }
        throw new AssertionError("Jira Edition " + jiraEdition + " doesn't support looking at the dialog header");
    }

    public SelenideElement content()
    {
        switch (jiraEdition)
        {
            case CLOUD_APP:
                return dialog();
            case CLOUD:
                return dialog().find(By.className("aui-dialog2-content"));
            default:
                return dialog().find(By.className("jira-dialog-content"));
        }
    }

    public SelenideElement footer()
    {
        switch (jiraEdition)
        {
            case CLOUD:
                return dialog().find(By.className("aui-dialog2-footer"));
            case SERVER:
            case DATA_CENTER:
                return dialog().find(By.className("form-footer"));
        }
        throw new AssertionError("Jira Edition " + jiraEdition + " doesn't support looking at the dialog footer");
    }

    protected SelenideElement child(String selector)
    {
        return child(By.cssSelector(selector));
    }

    protected SelenideElement child(By selector)
    {
        return elements.computeIfAbsent(selector, key -> content().find(key));
    }

    private SelenideElement dialog()
    {
        switch (jiraEdition)
        {
            case CLOUD_APP:
                return $(By.tagName("body"));
            case CLOUD:
                return $(By.cssSelector("section[role='dialog'][aria-hidden=false]"));
            default:
                return $(By.className("jira-dialog-open"));
        }
    }

    public D open()
    {
        elements.clear();
        if (jiraEdition != JiraEdition.CLOUD_APP)
        {
            removeAllFlagsAndTips();
            opener
                    .shouldBe(visible)
                    .click();
        }
        content().shouldBe(visible, Duration.ofSeconds(10));
        return (D) this;
    }

    public D submit()
    {
        elements.clear();
        if (jiraEdition != JiraEdition.CLOUD_APP)
        {
            footer()
                    .find(By.className("aui-button-primary"))
                    .shouldBe(visible)
                    .click();
        }
        return (D) this;
    }

    public D close()
    {
        Screenshots.saveScreenshotAndPageSource();
        elements.clear();
        if (jiraEdition != JiraEdition.CLOUD_APP)
        {
            footer()
                    .find(By.className("aui-button-link"))
                    .shouldBe(visible)
                    .click();
            content().shouldNotBe(visible);
        }
        return (D) this;
    }

    public SelenideElement button(int index)
    {
        return footer().find(By.className("aui-button"), index);
    }
}
