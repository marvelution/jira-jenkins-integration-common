/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.tests.admin;

import java.util.ArrayList;
import java.util.*;

import org.marvelution.jji.data.services.api.*;
import org.marvelution.jji.model.*;
import org.marvelution.jji.test.condition.*;
import org.marvelution.jji.test.license.*;
import org.marvelution.jji.web.conditions.*;
import org.marvelution.jji.web.elements.admin.SiteRow;
import org.marvelution.jji.web.elements.admin.*;
import org.marvelution.jji.web.elements.admin.site.*;
import org.marvelution.jji.web.pages.*;
import org.marvelution.testing.wiremock.*;

import org.junit.jupiter.api.*;

import static com.codeborne.selenide.Condition.*;
import static java.util.Arrays.*;
import static org.assertj.core.api.Assertions.*;
import static org.marvelution.jji.web.conditions.Conditions.*;
import static org.marvelution.testing.webdriver.conditions.CompositeCondition.*;

public interface ManageScopedSitesTests
        extends SiteManagementTests
{

    default boolean canAccessGlobalConfiguration()
    {
        return false;
    }

    @LicensedTest
    @DisabledIfLiteApp
    default void testPageHeaderEmptySiteList(License license)
    {
        navigateToProjectSites(null);

        assertProjectSitesPageHeader();
    }

    @LicensedTest
    @DisabledIfLiteApp
    default void testPageHeader(License license)
    {
        String projectKey = getExistingProjectKey();
        createFirewalledSite(projectKey);

        navigateToProjectSites(null, projectKey);

        assertProjectSitesPageHeader(button("Connect a Site", addSiteButtonAttributes()).state(license));
    }

    default void assertProjectSitesPageHeader(Button... additionalButtons)
    {
        PageHeader header = SiteListPage.header();
        header.title()
              .shouldHave(text("Jenkins Integration"));
        List<Button> buttons = new ArrayList<>();
        if (canAccessGlobalConfiguration())
        {
            buttons.add(button("Global configuration"));
        }
        buttons.add(helpButton());
        buttons.addAll(asList(additionalButtons));
        header.buttons()
              .shouldHave(size(buttons.size()), composite(buttons));
    }

    @Test
    @DisabledIfLiteApp
    default void testEmptySiteList()
    {
        navigateToProjectSites(null);

        SiteListPage.addSiteButton()
                    .should(exist)
                    .shouldBe(button("Connect your first Jenkins Site", addSiteButtonAttributes()));

        SiteListPage.siteList()
                    .shouldNotBe(visible);
        SiteListPage.startSteps()
                    .shouldBe(visible)
                    .shouldHave(startStepsLogo,
                            startStepsAction(button("Connect your first Jenkins Site", addSiteButtonAttributes()),
                                    button("Review the Quick Start Guide")))
                    .steps()
                    .shouldHave(composite(startStep("Connect", "Let Jira know where your jobs and builds are located."),
                            startStep("Build", "Continue your normal CI/CD workflow."),
                            startStep("Get Insight", "Visualize builds to CI/CD get insight within Jira.")));
    }

    @Test
    @DisabledIfLiteApp
    default void testSiteList(WireMockServer server)
    {
        String projectKey = getExistingProjectKey();
        Site jenkins = createSite(server);
        Site hudson = saveSite(createFirewalledSite().setScope(projectKey)
                                                     .setRegistrationComplete(true));

        navigateToProjectSites(null, projectKey);

        SiteListPage.addSiteButton()
                    .should(exist)
                    .shouldBe(button("Connect a Site", addSiteButtonAttributes()));

        SiteListPage.startSteps()
                    .shouldNotBe(visible);
        SiteListPage.siteList()
                    .shouldBe(visible);

        SiteListPage.sites()
                    .shouldHave(composite(row(jenkins).withoutActions(), row(hudson)));
    }

    @LicensedTest
    @DisabledIfLiteApp
    default void testScopedSiteActionMenu(License license)
    {
        String scope = getExistingProjectKey();
        Site site = createFirewalledSite(scope);
        Site globalSite = createFirewalledSite();

        navigateToProjectSites(null, scope);

        testSiteActionMenu(license, site, scope);
        testSiteActionMenu(license, globalSite, scope);
    }

    default void testSiteActionMenu(
            License license,
            Site site,
            String viewingScope)
    {
        SiteRow siteRow = SiteListPage.siteById(site.getId());
        if (canManageSite(site, viewingScope))
        {
            siteRow.mainActionButton()
                   .shouldBe(siteConnectionButton(site, license));
            siteRow.actionsDropDown()
                   .opener()
                   .shouldBe(siteActionsButton(site, license));

            if (license.valid())
            {
                SiteActions actions = siteRow.actionsDropDown()
                                             .open();
                assertSiteActionsMenu(actions, site, license);
                actions.close();
            }
        }
        else
        {
            siteRow.mainActionButton()
                   .shouldNot(exist);
            siteRow.actionsDropDown()
                   .opener()
                   .shouldNot(exist);
        }
    }

    default boolean canManageSite(
            Site site,
            String viewingScope)
    {
        return ConfigurationService.GLOBAL_SCOPE.equals(viewingScope) || viewingScope.equals(site.getScope());
    }

    @Test
    @DisabledIfLiteApp
    @DisabledIfJiraEdition(JiraEdition.CLOUD)
    default void testAddScopedSite(WireMockServer jenkins)
    {
        String scope = navigateToProjectSites(null);

        testAddSite(jenkins, scope);
    }

    default void testAddSite(
            WireMockServer jenkins,
            String scope)
    {
        SiteListPage.siteList()
                    .shouldNotBe(visible);
        SiteListPage.startSteps()
                    .shouldBe(visible);

        SiteListPage.addSiteButton()
                    .should(exist)
                    .shouldBe(button("Connect your first Jenkins Site", addSiteButtonAttributes()));

        AddSitePortal addSitePortal = SiteListPage.addSitePortal()
                                                  .open();

        assertJenkinsPluginInstallRequirement(addSitePortal);

        addSitePortal.next();

        if (!canAccessGlobalConfiguration())
        {
            addSitePortal.scope()
                         .shouldHave(value(scope));
        }
        addSitePortal.name()
                     .shouldBe(visible)
                     .val("Add Test");
        addSitePortal.siteUrl()
                     .shouldBe(visible)
                     .val(jenkins.baseUrl());
        addSitePortal.accessibleInstance()
                     .click();

        addSitePortal.next();

        addSitePortal.name()
                     .shouldNotBe(visible);
        addSitePortal.displayUrl()
                     .shouldBe(visible)
                     .val(jenkins.baseUrl());
        if (canAccessGlobalConfiguration())
        {
            addSitePortal.scopeSelector()
                         .shouldBe(visible)
                         .inputField()
                         .shouldHave(value(scope));
        }
        else
        {
            addSitePortal.scopeSelector()
                         .shouldNot(exist);
        }
        addSitePortal.enabled()
                     .shouldBe(visible, Conditions.checked);
        addSitePortal.autoLinkNewJobs()
                     .shouldBe(visible, Conditions.checked);
        addSitePortal.useCrumbs()
                     .shouldBe(visible, Conditions.checked);

        addSitePortal.next();

        addSitePortal.displayUrl()
                     .shouldNotBe(visible);
        addSitePortal.automaticMethod()
                     .shouldBe(visible, Conditions.checked);
        addSitePortal.manualMethod()
                     .shouldBe(visible)
                     .shouldNotBe(Conditions.checked);
        addSitePortal.cascMethod()
                     .shouldBe(visible)
                     .shouldNotBe(Conditions.checked)
                     .click();

        addSitePortal.done();

        SiteListPage.siteList()
                    .shouldBe(visible);
        SiteListPage.startSteps()
                    .shouldNotBe(visible);

        List<Site> sites = getSites();
        assertThat(sites).hasSize(1);
        SiteListPage.sites()
                    .shouldHave(size(1))
                    .first()
                    .shouldBe(row(sites.get(0)));
    }

    @Test
    @DisabledIfLiteApp
    default void testAddScopedFirewalledSite()
    {
        String scope = navigateToProjectSites(null);

        testAddFirewalledSite(scope);
    }

    default void testAddFirewalledSite(String scope)
    {
        SiteListPage.siteList()
                    .shouldNotBe(visible);
        SiteListPage.startSteps()
                    .shouldBe(visible);

        SiteListPage.addSiteButton()
                    .should(exist)
                    .shouldBe(button("Connect your first Jenkins Site", addSiteButtonAttributes()));

        AddSitePortal addSitePortal = SiteListPage.addSitePortal()
                                                  .open();

        assertJenkinsPluginInstallRequirement(addSitePortal);

        addSitePortal.next();

        if (canAccessGlobalConfiguration())
        {
            addSitePortal.scope()
                         .shouldHave(value(scope));
        }
        addSitePortal.name()
                     .shouldBe(visible)
                     .val("Add Firewalled Test");
        addSitePortal.siteUrl()
                     .shouldBe(visible)
                     .val("https://jenkins.example.com");
        addSitePortal.inaccessibleInstance()
                     .click();

        addSitePortal.next();

        addSitePortal.name()
                     .shouldNotBe(visible);
        addSitePortal.displayUrl()
                     .shouldNotBe(visible);
        if (canAccessGlobalConfiguration())
        {
            addSitePortal.scopeSelector()
                         .shouldBe(visible)
                         .inputField()
                         .shouldHave(value(scope));
        }
        else
        {
            addSitePortal.scopeSelector()
                         .shouldNot(exist);
        }
        addSitePortal.enabled()
                     .shouldBe(visible, Conditions.checked);
        addSitePortal.autoLinkNewJobs()
                     .shouldBe(visible, Conditions.checked);
        addSitePortal.useCrumbs()
                     .shouldNotBe(visible);

        addSitePortal.next();

        addSitePortal.scopeSelector()
                     .shouldNotBe(visible);
        addSitePortal.automaticMethod()
                     .shouldBe(visible, Conditions.disabled);
        addSitePortal.manualMethod()
                     .shouldBe(visible, Conditions.checked);
        addSitePortal.cascMethod()
                     .shouldBe(visible)
                     .shouldNotBe(Conditions.checked)
                     .click();

        addSitePortal.done();

        SiteListPage.siteList()
                    .shouldBe(visible);
        SiteListPage.startSteps()
                    .shouldNotBe(visible);

        List<Site> sites = getSites();
        assertThat(sites).hasSize(1);
        SiteListPage.sites()
                    .shouldHave(size(1))
                    .first()
                    .shouldBe(row(sites.get(0)));
    }

    @Test
    @DisabledIfLiteApp
    @DisabledIfJiraEdition(JiraEdition.CLOUD)
    default void testQuickAddScopedSite(WireMockServer jenkins)
    {
        String scope = navigateToProjectSites(null);

        testQuickAddSite(jenkins, scope);
    }

    default void testQuickAddSite(
            WireMockServer jenkins,
            String scope)
    {
        SiteListPage.siteList()
                    .shouldNotBe(visible);
        SiteListPage.startSteps()
                    .shouldBe(visible);

        SiteListPage.addSiteButton()
                    .should(exist)
                    .shouldBe(button("Connect your first Jenkins Site", addSiteButtonAttributes()));

        AddSitePortal addSitePortal = SiteListPage.addSitePortal()
                                                  .open();

        assertJenkinsPluginInstallRequirement(addSitePortal);

        addSitePortal.next();

        addSitePortal.scope()
                     .shouldHave(value(scope));
        addSitePortal.name()
                     .shouldBe(visible)
                     .val("Quick Add Test");
        addSitePortal.siteUrl()
                     .shouldBe(visible)
                     .val(jenkins.baseUrl());
        addSitePortal.accessibleInstance()
                     .click();

        addSitePortal.button("save-and-connect")
                     .click();

        addSitePortal.name()
                     .shouldNotBe(visible);
        addSitePortal.displayUrl()
                     .shouldNotBe(visible);
        addSitePortal.automaticMethod()
                     .shouldBe(visible, Conditions.checked);
        addSitePortal.manualMethod()
                     .shouldBe(visible)
                     .shouldNotBe(Conditions.checked);
        addSitePortal.cascMethod()
                     .shouldBe(visible)
                     .shouldNotBe(Conditions.checked)
                     .click();

        addSitePortal.done();

        SiteListPage.siteList()
                    .shouldBe(visible);
        SiteListPage.startSteps()
                    .shouldNotBe(visible);

        List<Site> sites = getSites();
        assertThat(sites).hasSize(1);
        SiteListPage.sites()
                    .shouldHave(size(1))
                    .first()
                    .shouldBe(row(sites.get(0)));
    }

    default void assertJenkinsPluginInstallRequirement(AddSitePortal addSitePortal)
    {
        addSitePortal.activePage()
                     .shouldHave(text("Before you continue, install 'Jira Integration' plugin on Jenkins."),
                             text("Once you've installed the plugin on Jenkins, click 'Next'."));
    }
}
