/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.tests.admin;

import java.time.*;
import java.util.*;

import org.marvelution.jji.model.*;
import org.marvelution.jji.test.condition.*;
import org.marvelution.jji.test.license.*;
import org.marvelution.jji.web.elements.*;
import org.marvelution.jji.web.elements.admin.site.*;
import org.marvelution.jji.web.elements.message.*;
import org.marvelution.jji.web.pages.*;
import org.marvelution.testing.wiremock.*;

import com.codeborne.selenide.*;
import com.github.tomakehurst.wiremock.client.*;
import org.junit.jupiter.api.*;

import static com.codeborne.selenide.Condition.*;
import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.http.RequestMethod.*;
import static org.assertj.core.api.Assertions.*;
import static org.awaitility.Awaitility.*;
import static org.marvelution.jji.test.license.License.*;
import static org.marvelution.jji.web.conditions.Conditions.checked;
import static org.marvelution.jji.web.conditions.Conditions.disabled;
import static org.marvelution.jji.web.conditions.Conditions.enabled;
import static org.marvelution.jji.web.conditions.Conditions.*;
import static org.marvelution.jji.web.elements.LicenseWarning.*;
import static org.marvelution.testing.webdriver.conditions.CompositeCondition.*;

public interface ManageSiteTests
        extends ManageScopedSiteTests
{

    @Override
    default boolean canAccessGlobalConfiguration()
    {
        return true;
    }

    @Test
    default void testUnknownSite()
    {
        Site site = new Site().setId("unknown-id");
        navigateDirectToSite(null, site);

        SitePage.header()
                .title()
                .shouldHave(text("Unknown Site!!1"));
    }

    @Test
    default void testSiteWithoutJobs()
    {
        Site site = createFirewalledSite();

        navigateToSite(null, site);

        testSiteWithoutJobs(site);
    }

    @Test
    default void testSite(WireMockServer jenkins)
    {
        Site site = createSite(jenkins);

        navigateToSite(null, site);

        SitePage.startSteps()
                .shouldNotBe(visible);

        assertJobsList(null, site, ACTIVE);
    }

    @LicensedTest
    default void assertSitePageHeader(License license)
    {
        Site site = createFirewalledSite();
        navigateDirectToSite(null, site);

        assertLicenseMessage(license);

        PageHeader header = SitePage.header();
        header.title()
              .shouldHave(text(site.getName()));
        header.avatar()
              .shouldBe(visible)
              .shouldHave(attributeEndsWith("src",
                      "/images/icon96_" + site.getType()
                                              .value() + ".png"));
        header.breadcrumbs()
              .shouldHave(exactTexts("Sites"));
        if (site.isInaccessibleSite())
        {
            SitePage.mainActionButton()
                    .shouldBe(siteConnectionButton(site, license));
        }
        else
        {
            SitePage.mainActionButton()
                    .shouldBe(synchronizeSiteButton(site, license));
        }
        SitePage.actionsDropDown()
                .opener()
                .shouldBe(siteActionsButton(site, license));
        SitePage.helpButton()
                .shouldBe(visible, helpButton());
        SitePage.jobQuickSearch()
                .shouldBe(visible);
    }

    @Test
    @DisabledIfJiraEdition(JiraEdition.CLOUD_APP)
    default void testCleanupJobs()
    {
        Site site = createFirewalledSite();
        navigateDirectToSite(null, site);

        SitePage.actionsDropDown()
                .open()
                .cleanupJobs()
                .opener()
                .shouldBe(disabled);

        site = saveSite(site,
                new Job().setDeleted(true)
                         .setName("deleted-job")
                         .setDisplayName("Deleted Job")
                         .setOldestBuild(1)
                         .setLastBuild(10));
        navigateDirectToSite(null, site);

        JobListPage.jobList()
                   .shouldBe(visible);
        JobListPage.jobs()
                   .shouldHave(size(1));

        MessageDialog dialog = SitePage.actionsDropDown()
                                       .open()
                                       .cleanupJobs();
        dialog.opener()
              .shouldBe(enabled)
              .click();
        assertCleanupJobsMessage(site, dialog);
        dialog.submit();

        FlagsAndTips.removeAllFlagsAndTips();

        if (SitePage.jobList()
                    .is(visible))
        {
            // page didn't refresh as expected.
            Selenide.refresh();
            FlagsAndTips.removeAllFlagsAndTips();
        }

        SitePage.jobList()
                .shouldNotBe(visible);
    }

    default void assertCleanupJobsMessage(
            Site site,
            MessageDialog dialog)
    {
        dialog.message()
              .shouldHave(text("Are you sure you want to cleanup jobs from " + site.getName() +
                               "? The cleanup action can't be undone and all jobs that are no longer on Jenkins will be permanently gone."));
    }

    @Test
    default void testAutoEnableNewJobs()
    {
        Site site = createFirewalledSite();

        navigateDirectToSite(null, site);
        SitePage.actionsDropDown()
                .open()
                .autoLinkNewJobs()
                .shouldBe(checked)
                .click();

        Flags.waitForFlag(element -> Objects.equals(element.getText(),
                "Successfully disabled automatic linking of new jobs on " + site.getName() + "."));
        FlagsAndTips.removeAllFlagsAndTips();

        SitePage.actionsDropDown()
                .open()
                .autoLinkNewJobs()
                .shouldNotBe(checked);

        assertThat(getSite(site).isAutoLinkNewJobs()).isFalse();
    }

    @Test
    @DisabledIfJiraEdition(JiraEdition.CLOUD)
    default void testEditSite(WireMockServer server)
    {
        Site site = createSite(server);
        testEditSite(site);
    }

    @Test
    default void testEditFirewalledSite()
    {
        Site site = createFirewalledSite();
        testEditSite(site);
    }

    default void testEditSite(Site site)
    {
        navigateDirectToSite(null, site);

        SitePage.header()
                .title()
                .shouldHave(text(site.getName()), Duration.ofSeconds(10));

        EditSitePortal editSitePortal = SitePage.actionsDropDown()
                                                .open()
                                                .edit()
                                                .open();

        assertEditSiteForm(site, editSitePortal, canAccessGlobalConfiguration());

        editSitePortal.name()
                      .val("Changed Site Name");
        if (site.usesBasicAuthentication())
        {
            editSitePortal.token()
                          .val(site.getToken());
        }
        editSitePortal.done();

        await().atMost(Duration.ofSeconds(10))
               .untilAsserted(() -> assertThat(getSite(site)).extracting(Site::getName)
                                                             .isEqualTo("Changed Site Name"));

        SitePage.header()
                .title()
                .shouldHave(text("Changed Site Name"), Duration.ofSeconds(10));
    }

    @Test
    @DisabledIfJiraEdition(JiraEdition.CLOUD)
    default void testEditSite_BasicAuth(WireMockServer server)
    {
        Site site = saveSite(createSite(server).setAuthenticationType(SiteAuthenticationType.BASIC)
                                               .setUser("admin")
                                               .setToken("admin123"));
        navigateDirectToSite(null, site);

        SitePage.header()
                .title()
                .shouldHave(text(site.getName()), Duration.ofSeconds(10));

        EditSitePortal editSitePortal = SitePage.actionsDropDown()
                                                .open()
                                                .edit()
                                                .open();

        assertEditSiteForm(site, editSitePortal, canAccessGlobalConfiguration());

        editSitePortal.name()
                      .val("Changed Site Name");
        editSitePortal.tokenAuth()
                      .click();
        editSitePortal.done();

        await().atMost(Duration.ofSeconds(10))
               .untilAsserted(() -> assertThat(getSite(site)).extracting(Site::getName)
                                                             .isEqualTo("Changed Site Name"));

        SitePage.header()
                .title()
                .shouldHave(text("Changed Site Name"), Duration.ofSeconds(10));

        editSitePortal = SitePage.actionsDropDown()
                                 .open()
                                 .edit()
                                 .open();
        editSitePortal.authentication()
                      .should(exist)
                      .shouldHave(value("TOKEN"));
        editSitePortal.tokenAuth()
                      .shouldNotBe(visible);
        editSitePortal.basicAuth()
                      .shouldNotBe(visible);
    }

    @Test
    @DisabledIfJiraEdition(JiraEdition.CLOUD_APP)
    default void testDeleteSite()
    {
        Site site = createFirewalledSite();

        navigateDirectToSite(null, site);
        MessageDialog dialog = SitePage.actionsDropDown()
                                       .open()
                                       .delete()
                                       .open();
        assertDeleteSiteMessage(site, dialog);
        dialog.submit();

        SiteListPage.siteList()
                    .shouldNot(exist);
    }

    default void assertDeleteSiteMessage(
            Site site,
            MessageDialog dialog)
    {
        dialog.message()
              .shouldHave(text("Are you sure you want to delete " + site.getName() +
                               "? The delete action can't be undone and all jobs and builds will be permanently gone."));
    }

    @Test
    @DisabledIfJiraEdition(JiraEdition.CLOUD)
    default void testSiteConnection_AutomaticRegistration(WireMockServer server)
    {
        Site site = createSite(server);
        navigateDirectToSite(null, site);

        SitePage.registrationWarning()
                .shouldBe(visible, Duration.ofSeconds(10));

        server.expect(WireMock.request(POST.getName(), urlPathEqualTo("/jji/register/"))
                              .withBasicAuth("admin", "admin")
                              .willReturn(ResponseDefinitionBuilder.responseDefinition()
                                                                   .withStatus(401)));

        SiteConnectPortal siteConnectPortal = SitePage.actionsDropDown()
                                                      .open()
                                                      .connection()
                                                      .open();

        siteConnectPortal.automaticMethod()
                         .shouldBe(checked);
        siteConnectPortal.adminUser()
                         .val("admin");
        siteConnectPortal.adminToken()
                         .val("admin");
        siteConnectPortal.done();

        Message error = siteConnectPortal.message()
                                         .shouldBe(visible);
        error.title()
             .shouldHave(text("Unable to automatically register with Jenkins site."));
        error.message()
             .shouldHave(text("Please verify that the credentials provided have administrative permissions in Jenkins and try again."));

        server.expect(WireMock.request(POST.getName(), urlPathEqualTo("/jji/register/"))
                              .withBasicAuth("admin", "admin123")
                              .willReturn(ResponseDefinitionBuilder.responseDefinition()
                                                                   .withStatus(201)));

        siteConnectPortal.adminToken()
                         .val("admin123");
        siteConnectPortal.done();

        SitePage.registrationWarning()
                .shouldNotBe(visible, Duration.ofSeconds(10));
    }

    @Test
    default void testSiteConnection_ManualRegistration()
    {
        Site site = createFirewalledSite();
        navigateDirectToSite(null, site);

        SitePage.registrationWarning()
                .shouldBe(visible, Duration.ofSeconds(10));

        SitePage.registrationWarning()
                .shouldBe(visible);

        SiteConnectPortal siteConnectPortal = SitePage.actionsDropDown()
                                                      .open()
                                                      .connection()
                                                      .open();

        siteConnectPortal.automaticMethod()
                         .shouldBe(disabled);
        siteConnectPortal.manualMethod()
                         .click();

        siteConnectPortal.done();

        Message error = siteConnectPortal.message()
                                         .shouldBe(visible);
        error.title()
             .shouldHave(text("Unable to validate manual registration was completed."));
        error.message()
             .shouldHave(text("Please register this Jira site with your Jenkins site using the registration token and secret to register " +
                              " this Jira site in Jenkins and try again."));

        saveSite(site.setRegistrationComplete(true));

        siteConnectPortal.done();

        SitePage.registrationWarning()
                .shouldNotBe(visible, Duration.ofSeconds(10));
    }

    @Test
    default void testSiteConnection_CasCRegistration()
    {
        Site site = createFirewalledSite();
        navigateDirectToSite(null, site);

        SitePage.registrationWarning()
                .shouldBe(visible, Duration.ofSeconds(10));

        SiteConnectPortal siteConnectPortal = SitePage.actionsDropDown()
                                                      .open()
                                                      .connection()
                                                      .open();

        siteConnectPortal.automaticMethod()
                         .shouldBe(disabled);
        siteConnectPortal.cascMethod()
                         .click();
        siteConnectPortal.done();

        SitePage.registrationWarning()
                .shouldNotBe(visible, Duration.ofSeconds(10));

        assertThat(getSite(site).isRegistrationComplete()).isTrue();
    }
}
