/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.tests.assertions;

import org.marvelution.jji.web.tests.panel.DevStatusPanel;
import org.marvelution.jji.web.tests.panel.TriggerBuildDialog;

import com.microsoft.playwright.Page;

public interface JenkinsAssertions
{
    static LicenseAssertions assertLicense(Page page)
    {
        return new LicenseAssertions(page.locator(".license-message"));
    }

    static PageHeaderAssertions assertHeader(Page page)
    {
        return new PageHeaderAssertions(page.locator("header.jenkins-page-header"));
    }

    static DevStatusAssertions assertThat(DevStatusPanel status)
    {
        return new DevStatusAssertions(status);
    }

    static TriggerBuildDialogAssertions assertThat(TriggerBuildDialog dialog)
    {
        return new TriggerBuildDialogAssertions(dialog);
    }

    static JobListAssertions assertJobList(Page page)
    {
        return new JobListAssertions(page.locator(".job-list"));
    }

    static BuildListAssertions assertBuildList(Page page)
    {
        return new BuildListAssertions(page.locator(".build-list"));
    }
}
