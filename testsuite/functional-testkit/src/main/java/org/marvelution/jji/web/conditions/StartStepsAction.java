/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.conditions;

import java.util.stream.*;

import org.marvelution.jji.web.elements.admin.*;

import com.codeborne.selenide.*;
import org.openqa.selenium.*;

import static org.marvelution.testing.webdriver.conditions.CompositeCondition.*;

import static com.codeborne.selenide.Selenide.*;
import static java.util.stream.Collectors.*;

public class StartStepsAction
		extends Condition
{

	private final Button[] buttons;

	StartStepsAction(Button... buttons)
	{
		super("start-steps-actions");
		this.buttons = buttons;
	}

	@Override
	public boolean apply(
			Driver driver,
			WebElement element)
	{
		return composite(buttons).test(
				new StartStepsElement($(element)).actions().stream().map(SelenideElement::getWrappedElement).collect(toList()));
	}

	public StartStepsAction buttonState(Button.State state)
	{
		Stream.of(buttons).forEach(button -> button.state(state));
		return this;
	}
}
