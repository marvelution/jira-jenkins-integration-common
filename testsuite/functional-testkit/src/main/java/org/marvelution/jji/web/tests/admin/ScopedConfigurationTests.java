/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.tests.admin;

import org.marvelution.jji.model.Configuration;
import org.marvelution.jji.test.condition.DisabledIfLiteApp;
import org.marvelution.jji.test.license.License;
import org.marvelution.jji.test.license.LicensedTest;
import org.marvelution.jji.web.tests.assertions.JenkinsAssertions;
import org.marvelution.jji.web.tests.assertions.PageHeaderAssertions;
import org.marvelution.jji.web.tests.playwright.PlaywrightExtension;

import com.microsoft.playwright.Page;
import org.junit.jupiter.api.extension.ExtendWith;

import static com.microsoft.playwright.assertions.PlaywrightAssertions.assertThat;
import static org.marvelution.jji.data.services.api.ConfigurationService.*;

@ExtendWith(PlaywrightExtension.class)
public interface ScopedConfigurationTests
{

    Configuration getConfiguration(String scope);

    default boolean canAccessGlobalConfiguration()
    {
        return false;
    }

    void openProjectConfigurationPage(Page page);

    @LicensedTest
    @DisabledIfLiteApp
    default void testProjectConfiguration(
            Page page,
            License license)
    {
        openProjectConfigurationPage(page);

        JenkinsAssertions.assertLicense(page)
                .hasState(license);

        PageHeaderAssertions pageHeaderAssertion = JenkinsAssertions.assertHeader(page)
                .hasTitle("Jenkins Integration");
        if (canAccessGlobalConfiguration())
        {
            pageHeaderAssertion.hasButtons("Global configuration", "Help");
        }
        else
        {
            pageHeaderAssertion.hasButtons("Help");
        }

        String scope = page.locator("input[name=scope]")
                .inputValue();
        Configuration configuration = getConfiguration(scope);

        assertCommonConfiguration(page, configuration, license);

        assertThat(page.locator("input[name=deleted_data_retention_period]")).not()
                .isAttached();
        assertThat(page.locator("input[name=audit_data_retention_period]")).not()
                .isAttached();

        additionalProjectConfiguration(page, configuration, license);
    }

    void additionalProjectConfiguration(
            Page page,
            Configuration configuration,
            License license);

    default void assertCommonConfiguration(
            Page page,
            Configuration configuration,
            License license)
    {
        assertThat(page.locator("input[name=max_builds_per_page]")).hasValue(configuration.requireConfiguration(MAX_BUILDS_PER_PAGE)
                .getValue());
        if (configuration.requireConfiguration(USE_USER_ID)
                .valueAsBoolean()
                .orElse(false))
        {
            assertThat(page.locator("input[id=use_user_id]")).isChecked();
            assertThat(page.locator("input[id=use_user_displayname]")).not()
                    .isChecked();
        }
        else
        {
            assertThat(page.locator("input[id=use_user_id]")).not()
                    .isChecked();
            assertThat(page.locator("input[id=use_user_displayname]")).isChecked();
        }

        assertThat(page.locator("input[name=parameter_issue_fields]")).hasValue(configuration.requireConfiguration(PARAMETER_ISSUE_FIELDS)
                .getValue());
        assertThat(page.locator("textarea[name=parameter_issue_field_names]")).hasValue(configuration.requireConfiguration(
                        PARAMETER_ISSUE_FIELD_NAMES)
                .getValue());
        assertThat(page.locator("textarea[name=parameter_issue_field_mappers]")).hasValue(configuration.requireConfiguration(
                        PARAMETER_ISSUE_FIELD_MAPPERS)
                .getValue());
    }
}
