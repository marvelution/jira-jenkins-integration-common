/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.elements.panel.dialog;

import com.codeborne.selenide.*;
import org.openqa.selenium.*;

public class BuildsTable
{

	private final SelenideElement table;

	BuildsTable(SelenideElement table)
	{
		this.table = table;
	}

	public BuildRowElement deployment(int index)
	{
		return new BuildRowElement(table.find(By.tagName("tbody")).find(By.tagName("tr"), index));
	}

	public BuildRowElement first()
	{
		return deployment(0);
	}

	public BuildsTable shouldBe(Condition... condition)
	{
		table.shouldBe(condition);
		return this;
	}

	public BuildsTable shouldNotBe(Condition... condition)
	{
		table.shouldNotBe(condition);
		return this;
	}

	public ElementsCollection visibleBuilds()
	{
		return table.find(By.tagName("tbody")).findAll(By.tagName("tr")).filterBy(Condition.visible);
	}
}
