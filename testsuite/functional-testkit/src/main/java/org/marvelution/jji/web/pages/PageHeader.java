/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.pages;

import com.codeborne.selenide.*;

import static org.openqa.selenium.By.*;

/**
 * @author Mark Rekveld
 * @since 1.4.0
 */
public class PageHeader
{

	private final SelenideElement header;

	PageHeader(SelenideElement header)
	{
		this.header = header;
	}

	public SelenideElement avatar()
	{
		return header.find(className("aui-avatar-inner")).find(tagName("img"));
	}

	public SelenideElement title()
	{
		return header.find(className("aui-page-header-main")).find(tagName("h1"));
	}

	public ElementsCollection breadcrumbs()
	{
		return header.find(className("aui-nav-breadcrumbs")).findAll(tagName("li"));
	}

	public ElementsCollection buttons()
	{
		return header.findAll(className("aui-button"));
	}

	public SelenideElement asElement()
	{
		return header;
	}
}
