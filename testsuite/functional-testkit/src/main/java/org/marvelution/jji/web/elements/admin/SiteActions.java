/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.elements.admin;

import org.marvelution.jji.web.elements.DropDown;
import org.marvelution.jji.web.elements.MessageDialog;
import org.marvelution.jji.web.elements.admin.site.EditSitePortal;
import org.marvelution.jji.web.elements.admin.site.ManageJobsDialog;
import org.marvelution.jji.web.elements.admin.site.SiteConnectPortal;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

public class SiteActions
        extends DropDown<SiteActions>
{

    public SiteActions(SelenideElement opener)
    {
        super(opener);
    }

    public MessageDialog cleanupJobs()
    {
        return new MessageDialog(allItemLinks().get(0));
    }

    public ManageJobsDialog manageJobs()
    {
        return new ManageJobsDialog(allItemLinks().get(1));
    }

    public SelenideElement enabled()
    {
        return container().find(By.tagName("aui-item-checkbox"), 0);
    }

    public SelenideElement autoLinkNewJobs()
    {
        return container().find(By.tagName("aui-item-checkbox"), 1);
    }

    public EditSitePortal edit()
    {
        return new EditSitePortal(allItemLinks().get(2));
    }

    public MessageDialog delete()
    {
        return new MessageDialog(allItemLinks().get(3));
    }

    public SiteConnectPortal connection()
    {
        return new SiteConnectPortal(allItemLinks().get(4));
    }

    public SelenideElement goToSite()
    {
        return allItemLinks().get(5);
    }
}
