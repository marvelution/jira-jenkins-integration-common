/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.elements.message;

import com.codeborne.selenide.SelenideElement;

/**
 * Element that describes an AUI Message bar.
 *
 * @author Mark Rekveld
 * @since 1.4.0
 */
public class MessageBar {

	private final SelenideElement container;

	public MessageBar(SelenideElement container) {
		this.container = container;
	}

	public Messages messages() {
		return new Messages(container.findAll(".aui-message"));
	}

	public Messages hints() {
		return new Messages(container.findAll(".aui-message-hint"));
	}

	public Messages errors() {
		return new Messages(container.findAll(".aui-message-error"));
	}

	public Messages warnings() {
		return new Messages(container.findAll(".aui-message-warning"));
	}

}
