/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.pages;

import org.marvelution.jji.web.elements.admin.SiteRow;
import org.marvelution.jji.web.elements.admin.StartStepsElement;
import org.marvelution.jji.web.elements.admin.site.AddSitePortal;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.*;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.$;
import static org.marvelution.jji.web.elements.admin.SiteRow.SITE_ID_ATTR;
import static org.openqa.selenium.By.className;

/**
 * @author Mark Rekveld
 * @since 1.4.0
 */
public class SiteListPage
        extends Page
{

    public static SelenideElement addSiteButton()
    {
        return $(By.id("add-site-button"));
    }

    public static AddSitePortal addSitePortal()
    {
        return new AddSitePortal(addSiteButton());
    }

    public static SelenideElement siteList()
    {
        return content().find(className("site-list"));
    }

    public static ElementsCollection sites()
    {
        return siteList().findAll("tbody > tr");
    }

    public static SiteRow firstSite()
    {
        return new SiteRow(sites().first());
    }

    public static SiteRow siteById(String id)
    {
        return new SiteRow(sites().find(attribute(SITE_ID_ATTR, id)));
    }

    public static StartStepsElement startSteps()
    {
        return new StartStepsElement(content().find(className("jenkins-integration-start")));
    }
}
