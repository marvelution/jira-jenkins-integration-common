/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.tests.assertions;

import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.marvelution.jji.test.license.License;

import com.microsoft.playwright.Locator;
import com.microsoft.playwright.assertions.LocatorAssertions;

import static com.microsoft.playwright.assertions.PlaywrightAssertions.assertThat;

public class PageHeaderAssertions
{
    private final Locator header;

    PageHeaderAssertions(Locator header)
    {
        this.header = header;
    }

    public PageHeaderAssertions hasTitle(String title)
    {
        assertThat(header().locator(".aui-page-header-main")
                .locator("h1")).hasText(title);
        return this;
    }

    public PageHeaderAssertions hasAvatarImg(String imgSrc)
    {
        assertThat(header().locator(".aui-avatar-inner")
                .locator("img")).hasAttribute("src", Pattern.compile("(.*)" + imgSrc));
        return this;
    }

    public PageHeaderAssertions hasButtons(String... buttonText)
    {
        Locator buttons = buttons();
        assertThat(buttons).hasCount(buttonText.length);
        assertThat(buttons).hasText(buttonText);
        return this;
    }

    public PageHeaderAssertions hasHelp(String href)
    {
        Locator locator = header().locator(".aui-button.help");
        assertThat(locator).hasText("Help");
        assertThat(locator).hasAttribute("href", Pattern.compile("(.*)" + href));
        return this;
    }

    public PageHeaderAssertions hasMainAction(String label)
    {
        assertMainAction().hasText(label);
        return this;
    }

    public LocatorAssertions assertMainAction()
    {
        return assertThat(mainAction());
    }

    public Locator mainAction()
    {
        return header().locator(".aui-button.aui-button-split-main");
    }

    public PageHeaderAssertions hasActionsMenu(
            String label,
            Map<String, String> attributes,
            License license)
    {
        Locator locator = moreActions();
        assertThat(locator).hasText(label);
        if (!license.valid())
        {
            assertThat(locator).hasAttribute("disabled", "disabled");
        }
        attributes.forEach((name, value) -> assertThat(locator).hasAttribute(name, value));
        return this;
    }

    public Locator moreActions()
    {
        return header().locator(".aui-button.aui-button-split-more");
    }

    public Locator header()
    {
        return header;
    }

    public Locator buttons()
    {
        return header().locator(".aui-button");
    }

    public PageHeaderAssertions hasBreadcrumbs(List<String> breadcrumbs)
    {
        assertThat(header().locator(".aui-nav-breadcrumbs")
                .locator("li")).hasText(breadcrumbs.toArray(String[]::new));
        return this;
    }
}
