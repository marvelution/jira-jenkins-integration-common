/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.elements.admin;

import org.marvelution.jji.web.elements.ActionableRow;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static com.codeborne.selenide.Condition.cssClass;
import static com.codeborne.selenide.Selenide.$;
import static org.openqa.selenium.By.tagName;

/**
 * @author Mark Rekveld
 * @since 1.8.0
 */
public class SiteRow
        extends ActionableRow<SiteActions>
{

    public static final String SITE_ID_ATTR = "jenkins-site-id";

    public SiteRow(WebElement element)
    {
        this($(element));
    }

    public SiteRow(SelenideElement container)
    {
        super(container);
    }

    public String siteId()
    {
        return container.attr(SITE_ID_ATTR);
    }

    public SelenideElement logo()
    {
        return column(0).find(By.tagName("img"));
    }

    public SelenideElement name()
    {
        return column(1);
    }

    public SelenideElement linkTo()
    {
        return name().find(tagName("a"));
    }

    public SelenideElement url()
    {
        return column(2).find(tagName("a"));
    }

	public SelenideElement registrationWarning() {
		return actions().find(cssClass("registration-warning"));
	}

    @Override
    public SiteActions actionsDropDown()
    {
        return new SiteActions(actions().find(cssClass("aui-button-split-more")));
    }
}
