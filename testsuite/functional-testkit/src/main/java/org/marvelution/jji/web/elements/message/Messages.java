/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.elements.message;

import com.codeborne.selenide.*;

import static com.codeborne.selenide.CollectionCondition.size;

/**
 * Element that describes an AUI Message collection.
 *
 * @author Mark Rekveld
 * @since 1.10.0
 */
public class Messages {

	private final ElementsCollection container;

	public Messages(ElementsCollection container) {
		this.container = container;
	}

	public Messages shouldHaveSize(int expectedSize) {
		container.shouldHave(size(expectedSize));
		return this;
	}

	public Messages shouldHave(CollectionCondition... conditions)
	{
		 container.shouldHave(conditions);
		 return this;
	}

	public Message first() {
		return new Message(container.first());
	}
}
