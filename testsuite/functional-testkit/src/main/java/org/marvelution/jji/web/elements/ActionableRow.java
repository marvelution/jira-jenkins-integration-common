/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.elements;

import com.codeborne.selenide.*;

import static com.codeborne.selenide.Condition.cssClass;
import static org.openqa.selenium.By.*;

/**
 * @author Mark Rekveld
 * @since 1.8.0
 */
public abstract class ActionableRow<A> extends Row {

	protected ActionableRow(SelenideElement container) {
		super(container);
	}

	public SelenideElement mainActionButton() {
		return actions().find(cssClass("aui-button-split-main"));
	}

	public abstract A actionsDropDown();

	public ElementsCollection actions() {
		return columns().last().findAll(className("aui-button"));
	}
}
