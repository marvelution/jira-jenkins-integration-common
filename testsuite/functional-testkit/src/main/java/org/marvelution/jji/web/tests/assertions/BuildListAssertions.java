/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.tests.assertions;

import java.util.Set;
import java.util.regex.Pattern;

import org.marvelution.jji.data.helper.PanelBuild;
import org.marvelution.jji.model.IssueReference;
import org.marvelution.jji.model.Result;

import com.microsoft.playwright.Locator;
import com.microsoft.playwright.assertions.LocatorAssertions;

import static com.microsoft.playwright.assertions.PlaywrightAssertions.assertThat;

public class BuildListAssertions
{
    private final Locator list;

    BuildListAssertions(Locator list)
    {
        this.list = list;
    }

    public BuildListAssertions isVisible(LocatorAssertions.IsVisibleOptions options)
    {
        assertThat(list).isVisible(options);
        return this;
    }

    public BuildListAssertions hasNoBuildsYet()
    {
        assertThat(list).containsText("No builds synchronized yet.");
        return this;
    }

    public BuildListAssertions hasCount(int count)
    {
        assertThat(builds()).hasCount(count);
        return this;
    }

    public BuildRowAssertions build(int index)
    {
        return new BuildRowAssertions(builds().nth(index));
    }

    private Locator builds()
    {
        return list.locator("tbody > tr");
    }

    public static class BuildRowAssertions
    {
        private final Locator row;

        BuildRowAssertions(Locator row)
        {
            this.row = row;
        }

        public BuildRowAssertions hasNumber(String number)
        {
            assertThat(column(0)).containsText(number);
            return this;
        }

        public BuildRowAssertions hasResult(Result result)
        {
            assertThat(column(1).locator(".aui-icon")).hasClass(Pattern.compile("(%s %s)".formatted(PanelBuild.getBuildColor(result),
                    PanelBuild.getBuildIcon(result))));
            return this;
        }

        public BuildRowAssertions hasCause(String cause)
        {
            assertThat(column(2)).hasText(cause);
            return this;
        }

        public BuildRowAssertions hasDuration(String formattedDuration)
        {
            assertThat(column(4)).hasText(formattedDuration);
            return this;
        }

        public BuildRowAssertions hasIssueLinks(Set<IssueReference> issueReferences)
        {
            Locator column = column(5);
            if (issueReferences.isEmpty())
            {
                assertThat(column).isEmpty();
            }
            else
            {
                assertThat(column).containsText(issueReferences.stream()
                        .map(IssueReference::getIssueKey)
                        .toArray(String[]::new));
                assertThat(column.locator("a")).hasCount(issueReferences.size());
            }
            return this;
        }

        private Locator column(int index)
        {
            return row.locator("td")
                    .nth(index);
        }
    }
}
