/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.conditions;

import java.util.*;

import org.marvelution.jji.model.*;
import org.marvelution.jji.test.license.*;

import com.codeborne.selenide.*;
import org.openqa.selenium.*;

import static org.marvelution.testing.webdriver.conditions.CompositeCondition.*;

import static java.util.stream.Collectors.*;

public class BuildRow
		extends Condition
{

	private final Build build;
	private final License license;

	BuildRow(
			Build build,
			License license)
	{
		super("build");
		this.build = build;
		this.license = license;
	}

	@Override
	public boolean apply(
			Driver driver,
			WebElement element)
	{
		org.marvelution.jji.web.elements.admin.BuildRow row = new org.marvelution.jji.web.elements.admin.BuildRow(element);
		return checkJobNameAndLink(row) && row.cause().has(text(build.getCause())) && !row.date().text().isEmpty() &&
				row.duration().has(text(build.getFormattedDuration())) && checkIssueLinks(row);
	}

	@Override
	public String toString()
	{
		return super.toString() + " " + build;
	}

	private boolean checkJobNameAndLink(org.marvelution.jji.web.elements.admin.BuildRow row)
	{
		Condition text = text("#" + build.getNumber());
		if (build.isDeleted())
		{
			return row.number().has(text);
		}
		SelenideElement link = row.linkTo();
		return row.number().has(text) && link.has(text) && link.has(attribute("href", build.getDisplayUrl().toASCIIString()));
	}

	private boolean checkIssueLinks(org.marvelution.jji.web.elements.admin.BuildRow row)
	{
		if (row.issueLinks().size() != build.getIssueReferences().size())
		{
			return false;
		}
		else
		{
			return composite(build.getIssueReferences().stream().map(IssueReferenceCondition::new).collect(toList())).test(
					row.issueLinks().stream().map(SelenideElement::toWebElement).collect(toList()));
		}
	}

	private static class IssueReferenceCondition
			extends Condition
	{

		private final IssueReference issueReference;

		private IssueReferenceCondition(IssueReference issueReference)
		{
			super("issue-reference");
			this.issueReference = issueReference;
		}

		@Override
		public boolean apply(
				Driver driver,
				WebElement element)
		{
			return Objects.equals(element.getText(), issueReference.getIssueKey()) &&
					Objects.equals(element.getAttribute("href"), issueReference.getIssueUrl().toASCIIString()) &&
					Objects.equals(element.getAttribute("target"), "_parent");
		}
	}
}
