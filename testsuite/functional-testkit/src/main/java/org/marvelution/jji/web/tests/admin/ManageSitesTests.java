/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.tests.admin;

import org.marvelution.jji.data.services.api.ConfigurationService;
import org.marvelution.jji.model.Site;
import org.marvelution.jji.test.condition.DisabledIfJiraEdition;
import org.marvelution.jji.test.condition.JiraEdition;
import org.marvelution.jji.test.license.License;
import org.marvelution.jji.test.license.LicensedTest;
import org.marvelution.jji.web.pages.PageHeader;
import org.marvelution.jji.web.pages.SiteListPage;
import org.marvelution.testing.wiremock.WireMockServer;

import org.junit.jupiter.api.Test;

import static com.codeborne.selenide.Condition.*;
import static org.marvelution.jji.web.conditions.Conditions.*;
import static org.marvelution.testing.webdriver.conditions.CompositeCondition.composite;

public interface ManageSitesTests
        extends ManageScopedSitesTests
{

    @Override
    default boolean canAccessGlobalConfiguration()
    {
        return true;
    }

    @LicensedTest
    default void testPageHeader(License license)
    {
        navigateToManageSites(null);

        PageHeader header = SiteListPage.header();
        header.title()
              .shouldHave(text("Manage Site"));
        header.avatar()
              .shouldNotBe(visible);
        SiteListPage.helpButton()
                    .shouldBe(visible, helpButton());
    }

    @Test
    @Override
    default void testEmptySiteList()
    {
        navigateToManageSites(null);

        SiteListPage.addSiteButton()
                    .shouldBe(button("Connect your first Jenkins Site", addSiteButtonAttributes()));

        SiteListPage.siteList()
                    .shouldNotBe(visible);
        SiteListPage.startSteps()
                    .shouldBe(visible)
                    .shouldHave(startStepsLogo,
                            startStepsAction(button("Connect your first Jenkins Site", addSiteButtonAttributes()),
                                    button("Review the Quick Start Guide")))
                    .steps()
                    .shouldHave(composite(startStep("Connect", "Let Jira know where your jobs and builds are located."),
                            startStep("Build", "Continue your normal CI/CD workflow."),
                            startStep("Get Insight", "Visualize builds to CI/CD get insight within Jira.")));
    }

    @Test
    @Override
    default void testSiteList(WireMockServer server)
    {
        Site jenkins = createSite(server);
        Site hudson = saveSite(createFirewalledSite().setScope(getExistingProjectKey())
                                                     .setRegistrationComplete(true));

        navigateToManageSites(null);

        SiteListPage.addSiteButton()
                    .should(exist)
                    .shouldBe(button("Connect a Site", addSiteButtonAttributes()));

        SiteListPage.startSteps()
                    .shouldNotBe(visible);
        SiteListPage.siteList()
                    .shouldBe(visible);

        SiteListPage.sites()
                    .shouldHave(composite(row(jenkins), row(hudson)));
    }

    @Override
    default boolean canManageSite(
            Site site,
            String viewingScope)
    {
        return true;
    }

    @LicensedTest
    default void testSiteActionMenu(License license)
    {
        Site site = createFirewalledSite();
        Site scopedSite = createFirewalledSite("TEST");

        navigateToManageSites(null);

        testSiteActionMenu(license, site, ConfigurationService.GLOBAL_SCOPE);
        testSiteActionMenu(license, scopedSite, ConfigurationService.GLOBAL_SCOPE);
    }

    @Test
    @DisabledIfJiraEdition(JiraEdition.CLOUD)
    default void testAddSite(WireMockServer jenkins)
    {
        navigateToManageSites(null);

        testAddSite(jenkins, ConfigurationService.GLOBAL_SCOPE);
    }

    @Test
    default void testAddFirewalledSite()
    {
        navigateToManageSites(null);

        testAddFirewalledSite(ConfigurationService.GLOBAL_SCOPE);
    }

    @Test
    @DisabledIfJiraEdition(JiraEdition.CLOUD)
    default void testQuickAddSite(WireMockServer jenkins)
    {
        navigateToManageSites(null);

        testQuickAddSite(jenkins, ConfigurationService.GLOBAL_SCOPE);
    }
}
