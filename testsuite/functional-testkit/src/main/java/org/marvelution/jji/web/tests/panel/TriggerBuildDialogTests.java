/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.tests.panel;

import java.util.Collections;
import java.util.List;

import org.marvelution.jji.data.helper.PanelJob;
import org.marvelution.jji.test.condition.DisabledIfJiraEdition;
import org.marvelution.jji.test.condition.JiraEdition;
import org.marvelution.jji.web.tests.assertions.JenkinsAssertions;
import org.marvelution.testing.wiremock.WireMockServer;

import com.microsoft.playwright.Locator;
import com.microsoft.playwright.Page;
import com.microsoft.playwright.assertions.LocatorAssertions;
import org.junit.jupiter.api.Test;

import static com.microsoft.playwright.assertions.PlaywrightAssertions.assertThat;

public interface TriggerBuildDialogTests
        extends PanelTests
{

    TriggerBuildDialog openBuildTriggerDialog(
            Page page,
            String issueKey);

    @Test
    @DisabledIfJiraEdition(JiraEdition.CLOUD)
    default void testBuildTriggerDialog(
            Page page,
            WireMockServer server)
    {
        String issue = getExistingIssueKey();
        List<PanelJob> jobs = createPanelJobData(issue, server);

        TriggerBuildDialog dialog = openBuildTriggerDialog(page, issue);

        JenkinsAssertions.assertThat(dialog)
                .noFirewalledSiteMessage()
                .noNoLinkedJobsMessage()
                .hasLinkedJobs(jobs);
    }

    @Test
    @DisabledIfJiraEdition(JiraEdition.CLOUD)
    default void testBuildTriggerDialog_TriggerableJobs(
            Page page,
            WireMockServer server)
    {
        String issue = getExistingIssueKey();
        createPanelJobData(issue);
        List<PanelJob> jobs = createPanelJobData(issue, server);

        TriggerBuildDialog dialog = openBuildTriggerDialog(page, issue);

        JenkinsAssertions.assertThat(dialog)
                .hasFirewalledSiteMessage()
                .noNoLinkedJobsMessage()
                .hasLinkedJobs(jobs);

        dialog.otherJobs()
                .fill("Job");
        Locator otherJobs = dialog.otherJobsResults();
        assertThat(otherJobs).hasCount(2, new LocatorAssertions.HasCountOptions().setTimeout(10000));
        assertThat(otherJobs).hasText(jobs.stream()
                .map(PanelJob::getDisplayName)
                .toArray(String[]::new));
        otherJobs.first()
                .click();
    }

    @Test
    default void testBuildTriggerDialog_FirewalledSiteMessage(Page page)
    {
        String issue = getExistingIssueKey();
        createPanelJobData(issue);

        TriggerBuildDialog dialog = openBuildTriggerDialog(page, issue);

        JenkinsAssertions.assertThat(dialog)
                .hasFirewalledSiteMessage()
                .hasNoLinkedJobsMessage()
                .hasLinkedJobs(Collections.emptyList());
    }

    @Test
    default void testBuildTriggerDialog_NoLinkedJobs(Page page)
    {
        String issue = getExistingIssueKey();

        TriggerBuildDialog dialog = openBuildTriggerDialog(page, issue);
        JenkinsAssertions.assertThat(dialog)
                .noFirewalledSiteMessage()
                .hasNoLinkedJobsMessage()
                .hasLinkedJobs(Collections.emptyList());
    }
}
