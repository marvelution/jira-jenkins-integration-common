/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.conditions;

import java.util.*;

import org.marvelution.jji.test.license.*;

import com.codeborne.selenide.*;
import org.openqa.selenium.*;

import static com.codeborne.selenide.Selenide.*;
import static java.util.stream.Collectors.*;

/**
 * @author Mark Rekveld
 * @since 1.4.0
 */
public class Button
		extends Condition
{

	private final String text;
	private final Map<String, String> attributes;
	private State buttonState = State.ENABLED;

	public Button(
			String text,
			Map<String, String> attributes)
	{
		super("button");
		this.text = text;
		this.attributes = attributes;
	}

	public Button state(State buttonState)
	{
		this.buttonState = buttonState;
		return this;
	}

	public Button state(License license)
	{
		return state(license.valid() ? State.ENABLED : State.DISABLED);
	}

	@Override
	public boolean apply(
			Driver driver,
			WebElement element)
	{
		SelenideElement $element = $(element);
		if ($element.has(text(text)))
		{
			for (Map.Entry<String, String> attribute : attributes.entrySet())
			{
				if (!$element.has(attribute(attribute.getKey(), attribute.getValue())))
				{
					return false;
				}
			}
			if (buttonState == State.ENABLED)
			{
				return $element.is(Conditions.enabled);
			}
			return $element.is(Conditions.disabled);
		}
		return false;
	}

	@Override
	public String toString()
	{
		return getName() + " with label '" + text + attributes.entrySet()
				.stream()
				.map(attribute -> attribute.getKey() + "=" + attribute.getValue())
				.collect(joining("', '", "' with attributes: '", "' and state: " + buttonState));
	}

	public enum State
	{
		ENABLED,
		DISABLED
	}
}
