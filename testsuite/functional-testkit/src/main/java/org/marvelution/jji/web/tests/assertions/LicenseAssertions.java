/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.tests.assertions;

import org.marvelution.jji.test.license.License;

import com.microsoft.playwright.Locator;
import com.microsoft.playwright.assertions.PlaywrightAssertions;

public class LicenseAssertions
{
    private final Locator warning;

    LicenseAssertions(Locator message)
    {
        this.warning = message;
    }

    public void hasState(License license)
    {
        if (license == License.ACTIVE)
        {
            PlaywrightAssertions.assertThat(warning)
                    .not()
                    .isAttached();
        }
        else
        {
            String title = switch (license)
            {
                case UNLICENSED, EXPIRED -> "License has expired!";
                case NEARLY_EXPIRED -> "License is nearly expired!";
                case ACTIVE -> throw new IllegalStateException("Should not get here");
            };
            String message = switch (license)
            {
                case UNLICENSED, EXPIRED ->
                        "Your license has expired, and as a result only administrators can view data. Other features may be blocked until you renew your license.";
                case NEARLY_EXPIRED ->
                        "Your license is about to expire. Renew it in-time to minimize disruption and keep the support level you have grown accustomed to.";
                case ACTIVE -> throw new IllegalStateException("Should not get here");
            };
            PlaywrightAssertions.assertThat(warning.locator(".title"))
                    .containsText(title);
            PlaywrightAssertions.assertThat(warning.locator("p")
                            .last())
                    .containsText(message);
        }
    }
}
