/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.elements;

import org.marvelution.jji.test.license.License;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;

/**
 * @author Mark Rekveld
 * @since 1.7.0
 */
public class LicenseWarning
{

    public static SelenideElement warning()
    {
        return $(".license-message");
    }

    public static SelenideElement title()
    {
        return warning().find(".title");
    }

    public static SelenideElement message()
    {
        return warning().find(By.tagName("p"), 1);
    }

    public static void assertInvalidMessage()
    {
        title().shouldBe(visible)
                .shouldHave(text("License has expired!"));
        message().shouldBe(visible)
                .shouldHave(text(
                        "Your license has expired, and as a result only administrators can view data. Other features may be blocked until you renew your license."));
    }

    public static void assertExpiredMessage()
    {
        title().shouldBe(visible)
                .shouldHave(text("License has expired!"));
        message().shouldBe(visible)
                .shouldHave(text(
                        "Your license has expired, and as a result only administrators can view data. Other features may be blocked until you renew your license."));
    }

    public static void assertNearlyExpiredMessage()
    {
        title().shouldBe(visible)
                .shouldHave(text("License is nearly expired!"));
        message().shouldBe(visible)
                .shouldHave(text(
                        "Your license is about to expire. Renew it in-time to minimize disruption and keep the support level you have grown accustomed to."));
    }

    /**
     * @since 1.10.0
     */
    public static void assertLicenseMessage(License license)
    {
        switch (license)
        {
            case UNLICENSED:
                assertInvalidMessage();
                break;
            case EXPIRED:
                assertExpiredMessage();
                break;
            case NEARLY_EXPIRED:
                assertNearlyExpiredMessage();
                break;
            case ACTIVE:
                warning().shouldNotBe(visible);
                break;
        }
    }
}
