/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.elements.admin.site;

import org.marvelution.jji.web.elements.form.Form;
import org.marvelution.jji.web.elements.form.PasswordField;
import org.marvelution.jji.web.elements.form.RadioField;
import org.marvelution.jji.web.elements.form.TextField;

public interface SiteConnectionPortalPage
        extends Form
{
    default RadioField automaticMethod()
    {
        return radio("method-automatic");
    }

    default TextField adminUser()
    {
        return text("adminUser");
    }

    default PasswordField adminToken()
    {
        return password("adminToken");
    }

    default RadioField manualMethod()
    {
        return radio("method-manual");
    }

    default RadioField cascMethod()
    {
        return radio("method-casc");
    }

    default PasswordField sharedSecret()
    {
        return password("sharedSecret");
    }
}
