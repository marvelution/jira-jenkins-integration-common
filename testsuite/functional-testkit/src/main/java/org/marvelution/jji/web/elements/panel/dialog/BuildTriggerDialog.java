/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.elements.panel.dialog;

import org.marvelution.jji.web.elements.form.*;
import org.marvelution.jji.web.elements.message.*;

import com.codeborne.selenide.*;

import static com.codeborne.selenide.Selectors.*;
import static org.openqa.selenium.By.*;

/**
 * @author Mark Rekveld
 * @since 1.10.0
 */
public class BuildTriggerDialog extends FormDialog<BuildTriggerDialog> {

	public BuildTriggerDialog(SelenideElement opener) {
		super(opener);
	}

	public Message firewalledSiteMessage() {
		return new Message(child(className("aui-message-info")));
	}

	public SelenideElement noLinkedJobsMessage() {
		return child(byClassName("no-linked-jobs"));
	}

	public ElementsCollection linkedJobs() {
		return child(byClassName("linked")).findAll(byClassName("jenkins-job-row"));
	}

	public Select2Field jobSearch() {
		return select2("other-jobs");
	}
}
