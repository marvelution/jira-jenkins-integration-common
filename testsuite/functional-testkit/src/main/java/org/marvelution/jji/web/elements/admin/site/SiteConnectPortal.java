/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.elements.admin.site;

import org.marvelution.jji.web.elements.form.Portal;
import org.marvelution.jji.web.elements.message.Message;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

public class SiteConnectPortal
        extends Portal<SiteConnectPortal>
        implements SiteConnectionPortalPage
{

    public SiteConnectPortal(SelenideElement opener)
    {
        super(opener);
    }

    public Message message()
    {
        return new Message(child(By.className("aui-message")));
    }
}
