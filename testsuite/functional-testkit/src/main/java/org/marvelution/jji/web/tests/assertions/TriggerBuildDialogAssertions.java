/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.tests.assertions;

import java.util.List;
import java.util.regex.Pattern;

import org.marvelution.jji.data.helper.PanelJob;
import org.marvelution.jji.web.tests.panel.TriggerBuildDialog;

import com.microsoft.playwright.Locator;

import static com.microsoft.playwright.assertions.PlaywrightAssertions.assertThat;

public class TriggerBuildDialogAssertions
{
    private final TriggerBuildDialog dialog;

    TriggerBuildDialogAssertions(TriggerBuildDialog dialog)
    {
        this.dialog = dialog;
    }

    public TriggerBuildDialogAssertions noFirewalledSiteMessage()
    {
        assertThat(dialog.firewalledSiteMessage()).not()
                .isVisible();
        return this;
    }

    public TriggerBuildDialogAssertions hasFirewalledSiteMessage()
    {
        assertThat(dialog.firewalledSiteMessage()).hasText(
                "Jobs hosted on your firewalled sites can't be triggered from Jira. These jobs are not selectable and are excluded from " +
                "the job search.");
        return this;
    }

    public TriggerBuildDialogAssertions noNoLinkedJobsMessage()
    {
        assertThat(dialog.noLinkedJobs()).not()
                .isVisible();
        return this;
    }

    public TriggerBuildDialogAssertions hasNoLinkedJobsMessage()
    {
        assertThat(dialog.noLinkedJobs()).hasText("No linked jobs.");
        return this;
    }

    public TriggerBuildDialogAssertions hasLinkedJobs(List<PanelJob> jobs)
    {
        Locator linkedJobs = dialog.linkedJobs();
        assertThat(linkedJobs).hasCount(jobs.size());
        for (Locator linkedJob : linkedJobs.all())
        {
            Locator checkbox = linkedJob.locator("input[type=checkbox]");
            assertThat(checkbox).not()
                    .isChecked();

            String jobId = checkbox.inputValue();
            PanelJob job = jobs.stream()
                    .filter(j -> j.getId()
                            .equals(jobId))
                    .findFirst()
                    .orElseThrow();

            assertThat(checkbox).hasValue(job.getId());
            assertThat(linkedJob.locator("label")).hasText(job.getDisplayName());

            Locator lastBuild = linkedJob.locator(".jenkins-build");
            assertThat(lastBuild.locator(".aui-icon")).hasClass(Pattern.compile(job.getLastBuild()
                    .getBuildIcon()));
            assertThat(lastBuild.locator(".aui-icon")).hasClass(Pattern.compile(job.getLastBuild()
                    .getBuildColor()));
            assertThat(lastBuild.locator(".status")).hasText(String.valueOf(job.getLastBuild()
                    .getShortDisplayName()));
        }
        return this;
    }
}
