/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.elements.form;

import com.codeborne.selenide.*;
import org.openqa.selenium.*;

/**
 * @author Mark Rekveld
 * @since 1.10.0
 */
public abstract class Field<F extends Field>
{

	protected final SelenideElement form;
	protected final String id;

	Field(
			SelenideElement form,
			String id)
	{
		this.form = form;
		this.id = id;
	}

	public SelenideElement label()
	{
		return form.find("label[for=" + id + "]");
	}

	public SelenideElement description()
	{
		return input().parent().find(By.className("description"));
	}

	public SelenideElement error()
	{
		return input().parent().find(By.className("error"));
	}

	protected SelenideElement input()
	{
		return form.find(By.id(id));
	}

	/**
	 * @since 1.13.0
	 */
	public F should(Condition... condition)
	{
		input().should(condition);
		return cast();
	}

	/**
	 * @since 1.13.0
	 */
	public F shouldNot(Condition... condition)
	{
		input().shouldNot(condition);
		return cast();
	}

	/**
	 * @since 1.13.0
	 */
	public F shouldHave(Condition... condition)
	{
		input().shouldHave(condition);
		return cast();
	}

	/**
	 * @since 1.13.0
	 */
	public F shouldNotHave(Condition... condition)
	{
		input().shouldNotHave(condition);
		return cast();
	}

	/**
	 * @since 1.13.0
	 */
	public F shouldBe(Condition... condition)
	{
		input().shouldBe(condition);
		return cast();
	}

	/**
	 * @since 1.13.0
	 */
	public F shouldNotBe(Condition... condition)
	{
		input().shouldNotBe(condition);
		return cast();
	}

	/**
	 * @since 1.13.0
	 */
	public F click()
	{
		SelenideElement label = label();
		if (label.exists())
		{
			label.click();
		}
		else
		{
			input().click();
		}
		return cast();
	}

	/**
	 * @since 1.13.0
	 */
	public F val(String value)
	{
		input().val(value);
		return cast();
	}

	/**
	 * @since 1.13.0
	 */
	public String val()
	{
		return input().val();
	}

	/**
	 * @since 1.13.0
	 */
	@SuppressWarnings("unchecked")
	protected F cast()
	{
		return (F) this;
	}
}
