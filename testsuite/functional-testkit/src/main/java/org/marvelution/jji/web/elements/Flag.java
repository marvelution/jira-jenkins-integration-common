/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.elements;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Condition.attribute;
import static org.openqa.selenium.By.className;

/**
 * @author Mark Rekveld
 * @since 1.10.0
 */
public class Flag {

	private final SelenideElement flag;

	public Flag(SelenideElement flag) {
		this.flag = flag;
	}

	public boolean isHidden() {
		return flag.has(attribute("aria-hidden", "true"));
	}

	public SelenideElement title() {
		return message().find(className("title"));
	}

	public SelenideElement message() {
		return flag.find(className("aui-message"));
	}

	public SelenideElement close() {
		return flag.find("span.icon-close");
	}

	public ElementsCollection buttons() {
		return flag.findAll(className("aui-button"));
	}
}
