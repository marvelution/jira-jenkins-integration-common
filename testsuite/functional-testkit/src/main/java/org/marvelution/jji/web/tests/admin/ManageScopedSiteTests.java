/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.tests.admin;

import java.net.*;
import java.time.*;
import java.util.*;

import org.marvelution.jji.model.*;
import org.marvelution.jji.test.condition.*;
import org.marvelution.jji.test.license.*;
import org.marvelution.jji.web.conditions.*;
import org.marvelution.jji.web.elements.admin.site.*;
import org.marvelution.jji.web.pages.*;
import org.marvelution.testing.wiremock.*;

import com.codeborne.selenide.*;
import org.junit.jupiter.api.*;

import static com.codeborne.selenide.Condition.empty;
import static com.codeborne.selenide.Condition.*;
import static org.assertj.core.api.Assertions.*;
import static org.awaitility.Awaitility.*;
import static org.marvelution.jji.test.license.License.*;
import static org.marvelution.jji.web.conditions.Conditions.checked;
import static org.marvelution.jji.web.conditions.Conditions.disabled;
import static org.marvelution.jji.web.conditions.Conditions.*;
import static org.marvelution.jji.web.elements.LicenseWarning.*;
import static org.marvelution.testing.webdriver.conditions.CompositeCondition.*;

public interface ManageScopedSiteTests
        extends SiteManagementTests
{

    default Button defineJobButton(Site site)
    {
        return button("Go to " + site.getName() + " to define a Job",
                "href",
                site.getDisplayUrl()
                        .toASCIIString());
    }

    default boolean canAccessGlobalConfiguration()
    {
        return false;
    }

    @LicensedTest
    @DisabledIfLiteApp
    default void testPageHeader(License license)
    {
        String projectKey = getExistingProjectKey();
        Site site = saveSite(createFirewalledSite(projectKey).setRegistrationComplete(true));

        navigateToScopedSite(null, site);

        assertLicenseMessage(license);

        PageHeader header = SitePage.header();
        header.title()
                .shouldHave(text("Jenkins Integration"));
        List<Button> buttons = new ArrayList<>();
        buttons.add(button("Return to list"));
        if (canAccessGlobalConfiguration())
        {
            buttons.add(button("Global configuration"));
        }
        buttons.add(helpButton());
        buttons.add(siteConnectionButton(site, license));
        buttons.add(siteActionsButton(site, license));
        header.buttons()
                .shouldHave(size(buttons.size()), composite(buttons));
        SitePage.jobQuickSearch()
                .shouldBe(visible);
    }

    @Test
    @DisabledIfLiteApp
    default void testSiteActionsForGlobalSiteInProjectScope()
    {
        String projectKey = getExistingProjectKey();
        Site site = saveSite(createFirewalledSite().setRegistrationComplete(true));

        navigateToSiteInScope(null, site, projectKey);

        PageHeader header = SitePage.header();
        header.title()
                .shouldHave(text("Jenkins Integration"));
        List<Button> buttons = new ArrayList<>();
        buttons.add(button("Return to list"));
        if (canAccessGlobalConfiguration())
        {
            buttons.add(button("Global configuration"));
            buttons.add(siteConnectionButton(site, ACTIVE));
            buttons.add(siteActionsButton(site, ACTIVE));
        }
        buttons.add(helpButton());
        header.buttons()
                .shouldHave(size(buttons.size()), composite(buttons));
        SitePage.jobQuickSearch()
                .shouldBe(visible);
    }

    @Test
    @DisabledIfLiteApp
    default void testUnknownScopedSite()
    {
        Site site = new Site().setId("unknown-id")
                .setScope(getExistingProjectKey());
        navigateDirectToScopedSite(null, site);

        SitePage.content()
                .shouldHave(text("There is no site with id: " + site.getId()));
    }

    @Test
    @DisabledIfLiteApp
    default void testScopedSiteWithoutJobs()
    {
        String scope = getExistingProjectKey();
        Site site = createFirewalledSite(scope);

        navigateToScopedSite(null, site);

        testSiteWithoutJobs(site);
    }

    default void testSiteWithoutJobs(Site site)
    {
        SitePage.startSteps()
                .shouldBe(visible)
                .shouldHave(startStepsLogo, startStepsAction(defineJobButton(site), siteConnectionButton(site, ACTIVE)))
                .steps()
                .shouldHave(composite(startStep("Define Jobs", "Tell Jenkins what to build by defining Jobs."),
                        startStep("Build", "Continue your normal CI/CD workflow."),
                        startStep("Get Insight", "Visualize builds to CI/CD get insight within Jira.")));

        SitePage.jobList()
                .shouldNotBe(visible);
    }

    @Test
    @DisabledIfLiteApp
    @DisabledIfJiraEdition(JiraEdition.CLOUD)
    default void testEditScopedSite(WireMockServer server)
    {
        testEditScopedSite(createSite(server, getExistingProjectKey()));
    }

    @Test
    @DisabledIfLiteApp
    default void testEditScopedFirewalledSite()
    {
        testEditScopedSite(createFirewalledSite(getExistingProjectKey()));
    }

    default void testEditScopedSite(Site site)
    {
        navigateDirectToScopedSite(null, site);

        EditSitePortal editSitePortal = SitePage.actionsDropDown()
                .open()
                .edit()
                .open();

        assertEditSiteForm(site, editSitePortal, canAccessGlobalConfiguration());

        editSitePortal.name()
                .val("Changed Scoped Site Name");
        if (site.usesBasicAuthentication())
        {
            editSitePortal.token()
                    .val(site.getToken());
        }
        editSitePortal.done();

        await().atMost(Duration.ofSeconds(10))
                .untilAsserted(() -> assertThat(getSite(site)).extracting(Site::getName)
                        .isEqualTo("Changed Scoped Site Name"));
    }

    default void assertEditSiteForm(
            Site site,
            EditSitePortal portal,
            boolean siteAdmin)
    {
        portal.form()
                .shouldHave(Condition.attribute("action", createFormActionForSite(site.getId())));
        portal.id()
                .shouldHave(value(site.getId()));
        portal.type()
                .shouldHave(value(site.getType()
                        .name()));
        portal.name()
                .shouldBe(visible)
                .shouldHave(value(site.getName()));
        portal.siteUrl()
                .shouldBe(visible)
                .shouldHave(value(site.getRpcUrl()
                        .toASCIIString()));

        if (siteAdmin)
        {
            portal.scopeSelector()
                    .shouldBe(visible)
                    .inputField()
                    .shouldHave(value(site.getScope()));
        }
        else
        {
            portal.scope()
                    .should(exist)
                    .shouldHave(value(site.getScope()));
        }
        portal.enabled()
                .shouldBe(visible, checked);
        portal.autoLinkNewJobs()
                .shouldBe(visible, checked);

        if (site.isInaccessibleSite())
        {
            portal.inaccessibleInstance()
                    .shouldBe(checked);
            portal.useCrumbs()
                    .shouldNot(exist);
            portal.displayUrl()
                    .shouldNot(exist);
            portal.basicAuth()
                    .shouldNot(exist);
            portal.tokenAuth()
                    .shouldNot(exist);
        }
        else
        {
            portal.accessibleInstance()
                    .shouldBe(checked);
            portal.useCrumbs()
                    .shouldBe(visible);
            portal.displayUrl()
                    .shouldBe(visible)
                    .shouldHave(value(Optional.ofNullable(site.getDisplayUrlOrNull())
                            .map(URI::toASCIIString)
                            .orElse("")));

            if (site.usesBasicAuthentication())
            {
                portal.basicAuth()
                        .shouldBe(checked);
                portal.user()
                        .shouldBe(visible)
                        .shouldHave(value(site.getUser()));
                portal.token()
                        .shouldBe(empty, visible);
                portal.newToken()
                        .shouldBe(empty, visible);
            }
            else
            {
                portal.authentication()
                        .shouldHave(value("TOKEN"));
                portal.basicAuth()
                        .shouldNot(exist);
                portal.tokenAuth()
                        .shouldNot(exist);
                portal.user()
                        .shouldNot(exist);
                portal.newToken()
                        .shouldNot(exist);
                portal.token()
                        .shouldNot(exist);
            }
        }
    }

    @Test
    @DisabledIfLiteApp
    default void testScopedSiteConnection_CasCRegistration()
    {
        Site site = createFirewalledSite(getExistingProjectKey());
        navigateDirectToScopedSite(null, site);

        SitePage.registrationWarning()
                .shouldBe(visible);

        SiteConnectPortal siteConnectPortal = SitePage.actionsDropDown()
                .open()
                .connection()
                .open();

        siteConnectPortal.automaticMethod()
                .shouldBe(disabled);
        siteConnectPortal.cascMethod()
                .click();
        siteConnectPortal.done();

        await().atMost(Duration.ofSeconds(10))
                .untilAsserted(() -> assertThat(getSite(site).isRegistrationComplete()).isTrue());

        SitePage.registrationWarning()
                .shouldNot(exist, Duration.ofSeconds(10));
    }
}
