/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.conditions;

import org.marvelution.jji.data.helper.*;

import com.codeborne.selenide.*;
import org.openqa.selenium.*;

import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.*;

/**
 * @author Mark Rekveld
 * @since 1.10.0
 */
public class LinkedJob
		extends Condition
{

	private final PanelJob job;

	public LinkedJob(PanelJob job)
	{
		super("linked-job");
		this.job = job;
	}

	@Override
	public boolean apply(
			Driver driver,
			WebElement element)
	{
		SelenideElement $element = $(element);
		return verifyLabel($element) && verifyCheckBox($element) && verifyLastBuild($element);
	}

	private boolean verifyLabel(SelenideElement element)
	{
		return element.find(By.tagName("label")).has(exactText(job.getDisplayName()));
	}

	private boolean verifyCheckBox(SelenideElement element)
	{
		SelenideElement checkbox = element.find(By.tagName("input"));
		return checkbox.has(attribute("type", "checkbox")) && checkbox.has(attribute("value", job.getId())) && !checkbox.is(checked) &&
				(!job.getJob().getSite().isInaccessibleSite() || checkbox.is(Conditions.disabled));
	}

	private boolean verifyLastBuild(SelenideElement element)
	{
		SelenideElement icon = element.find(byClassName("aui-icon"));
		return icon.has(cssClass(job.getLastBuild().getBuildIcon())) && icon.has(cssClass(job.getLastBuild().getBuildColor())) &&
				element.find(byClassName("status")).has(text(String.valueOf(job.getLastBuild().getNumber())));
	}
}
