/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.elements.form;

import com.codeborne.selenide.*;
import org.openqa.selenium.*;

/**
 * @author Mark Rekveld
 * @since 1.10.0
 */
public class HiddenField
		extends Field<HiddenField>
{

	public HiddenField(
			SelenideElement form,
			String name)
	{
		super(form, name);
	}

	@Override
	public SelenideElement label()
	{
		throw new UnsupportedOperationException("hidden fields don't have labels");
	}

	@Override
	public SelenideElement description()
	{
		throw new UnsupportedOperationException("hidden fields don't have descriptions");
	}

	@Override
	public SelenideElement error()
	{
		throw new UnsupportedOperationException("hidden fields don't have errors");
	}

	@Override
	public HiddenField click()
	{
		throw new UnsupportedOperationException("hidden fields can't be clicked on");
	}

	@Override
	protected SelenideElement input()
	{
		return form.find(By.name(id));
	}
}
