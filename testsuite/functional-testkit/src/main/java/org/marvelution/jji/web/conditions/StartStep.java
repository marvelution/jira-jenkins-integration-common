/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.conditions;

import com.codeborne.selenide.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static com.codeborne.selenide.Selenide.$;

/**
 * @author Mark Rekveld
 * @since 1.4.0
 */
public class StartStep extends Condition {

	private final String title;
	private final String description;

	StartStep(String title, String description) {
		super("start-step[" + title + "]");
		this.title = title;
		this.description = description;
	}

	@Override
	public boolean apply(Driver driver, WebElement element) {
		SelenideElement $element = $(element);
		return $element.find(By.tagName("h5")).has(text(title)) && $element.find(By.tagName("div")).has(text(description));
	}
}
