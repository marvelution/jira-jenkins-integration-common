/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.elements.message;

import com.codeborne.selenide.*;
import org.openqa.selenium.*;

/**
 * Element that describes an AUI Message.
 *
 * @author Mark Rekveld
 * @since 1.10.0
 */
public class Message {

	private final SelenideElement container;

	public Message(SelenideElement container) {
		this.container = container;
	}

	public SelenideElement title() {
		return container.find(".title");
	}

	public SelenideElement message() {
		return container;
	}

	public ElementsCollection actions() {
		return container.findAll(By.className("aui-button"));
	}

	public Message shouldBe(Condition... condition) {
		container.shouldBe(condition);
		return this;
	}

	public Message shouldNotBe(Condition... condition) {
		container.shouldNotBe(condition);
		return this;
	}
}
