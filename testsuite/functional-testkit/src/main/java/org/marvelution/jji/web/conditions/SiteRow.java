/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.conditions;

import javax.annotation.*;
import java.util.*;

import org.marvelution.jji.model.*;
import org.marvelution.jji.test.license.*;
import org.marvelution.testing.webdriver.conditions.*;

import com.codeborne.selenide.*;
import org.openqa.selenium.*;

import static java.util.stream.Collectors.*;
import static org.marvelution.jji.web.conditions.Conditions.*;

public class SiteRow
        extends Condition
{

    private final Site site;
    private final License license;
    private boolean noActions;

    SiteRow(
            Site site,
            License license)
    {
        super("site-row");
        this.site = site;
        this.license = license;
    }

    public SiteRow withoutActions()
    {
        noActions = true;
        return this;
    }

    @Override
    public boolean apply(
            Driver driver,
            WebElement element)
    {
        org.marvelution.jji.web.elements.admin.SiteRow row = new org.marvelution.jji.web.elements.admin.SiteRow(element);

        return checkSiteIcon(row.logo()) && checkSiteNameAndLink(row) && checkSiteUrl(row.url(),
                site.getRpcUrl()
                    .toASCIIString()) && checkSiteActions(row.actions());
    }

    @Nonnull
    @Override
    public String toString()
    {
        return super.toString() + " " + site;
    }

    private boolean checkSiteIcon(SelenideElement logo)
    {
        return logo.attr("src")
                   .endsWith("/images/icon96_" + site.getType()
                                                     .value() + ".png");
    }

    private boolean checkSiteNameAndLink(org.marvelution.jji.web.elements.admin.SiteRow row)
    {
        return row.name()
                  .has(text(site.getName())) && row.linkTo()
                                                   .has(text(site.getName()));
    }

    private boolean checkSiteUrl(
            SelenideElement link,
            String uri)
    {
        return link.has(text(uri)) && link.has(attribute("href", uri)) && link.has(attribute("target", "_top"));
    }

    private boolean checkSiteActions(ElementsCollection actions)
    {
        List<WebElement> elements = actions.asFixedIterable()
                                           .stream()
                                           .map(SelenideElement::getWrappedElement)
                                           .collect(toList());

        if (noActions)
        {
            return elements.isEmpty();
        }
        else
        {
            List<Button> buttons = new ArrayList<>();
            if (!site.isRegistrationComplete())
            {
                buttons.add(button("This Jira site is not registered with '" + site.getName() + "'.").state(license));
            }
            buttons.add(button(site.isInaccessibleSite() ? "Manage Connection" : "Synchronize").state(license));
            buttons.add(button("Action").state(license));

            return CompositeCondition.composite(buttons)
                                     .test(elements);
        }
    }
}
