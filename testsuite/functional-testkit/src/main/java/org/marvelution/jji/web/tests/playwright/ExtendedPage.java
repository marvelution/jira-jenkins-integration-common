/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.tests.playwright;

import java.lang.reflect.Method;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.function.BooleanSupplier;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.regex.Pattern;

import com.microsoft.playwright.*;
import com.microsoft.playwright.options.*;
import org.junit.jupiter.api.extension.ExtensionContext;

public class ExtendedPage
        implements Page
{
    protected final ExtensionContext extensionContext;
    protected final Page delegate;

    protected ExtendedPage(
            Page delegate,
            ExtensionContext extensionContext)
    {
        this.extensionContext = extensionContext;
        this.delegate = delegate;
    }

    public void screenshot(String name)
    {
        screenshot(new Page.ScreenshotOptions().setFullPage(true)
                .setPath(Paths.get(System.getProperty("reports.dir", "failsafe-reports"),
                        extensionContext.getTestClass()
                                .map(Class::getName)
                                .orElse("unknown-test-class"),
                        extensionContext.getTestMethod()
                                .map(Method::getName)
                                .orElse("unknown-test-method"),
                        name)));
    }

    @Override
    public void onClose(Consumer<Page> handler)
    {
        delegate.onClose(handler);
    }

    @Override
    public void offClose(Consumer<Page> handler)
    {
        delegate.offClose(handler);
    }

    @Override
    public void onConsoleMessage(Consumer<ConsoleMessage> handler)
    {
        delegate.onConsoleMessage(handler);
    }

    @Override
    public void offConsoleMessage(Consumer<ConsoleMessage> handler)
    {
        delegate.offConsoleMessage(handler);
    }

    @Override
    public void onCrash(Consumer<Page> handler)
    {
        delegate.onCrash(handler);
    }

    @Override
    public void offCrash(Consumer<Page> handler)
    {
        delegate.offCrash(handler);
    }

    @Override
    public void onDialog(Consumer<Dialog> handler)
    {
        delegate.onDialog(handler);
    }

    @Override
    public void offDialog(Consumer<Dialog> handler)
    {
        delegate.offDialog(handler);
    }

    @Override
    public void onDOMContentLoaded(Consumer<Page> handler)
    {
        delegate.onDOMContentLoaded(handler);
    }

    @Override
    public void offDOMContentLoaded(Consumer<Page> handler)
    {
        delegate.offDOMContentLoaded(handler);
    }

    @Override
    public void onDownload(Consumer<Download> handler)
    {
        delegate.onDownload(handler);
    }

    @Override
    public void offDownload(Consumer<Download> handler)
    {
        delegate.offDownload(handler);
    }

    @Override
    public void onFileChooser(Consumer<FileChooser> handler)
    {
        delegate.onFileChooser(handler);
    }

    @Override
    public void offFileChooser(Consumer<FileChooser> handler)
    {
        delegate.offFileChooser(handler);
    }

    @Override
    public void onFrameAttached(Consumer<Frame> handler)
    {
        delegate.onFrameAttached(handler);
    }

    @Override
    public void offFrameAttached(Consumer<Frame> handler)
    {
        delegate.offFrameAttached(handler);
    }

    @Override
    public void onFrameDetached(Consumer<Frame> handler)
    {
        delegate.onFrameDetached(handler);
    }

    @Override
    public void offFrameDetached(Consumer<Frame> handler)
    {
        delegate.offFrameDetached(handler);
    }

    @Override
    public void onFrameNavigated(Consumer<Frame> handler)
    {
        delegate.onFrameNavigated(handler);
    }

    @Override
    public void offFrameNavigated(Consumer<Frame> handler)
    {
        delegate.offFrameNavigated(handler);
    }

    @Override
    public void onLoad(Consumer<Page> handler)
    {
        delegate.onLoad(handler);
    }

    @Override
    public void offLoad(Consumer<Page> handler)
    {
        delegate.offLoad(handler);
    }

    @Override
    public void onPageError(Consumer<String> handler)
    {
        delegate.onPageError(handler);
    }

    @Override
    public void offPageError(Consumer<String> handler)
    {
        delegate.offPageError(handler);
    }

    @Override
    public void onPopup(Consumer<Page> handler)
    {
        delegate.onPopup(handler);
    }

    @Override
    public void offPopup(Consumer<Page> handler)
    {
        delegate.offPopup(handler);
    }

    @Override
    public void onRequest(Consumer<Request> handler)
    {
        delegate.onRequest(handler);
    }

    @Override
    public void offRequest(Consumer<Request> handler)
    {
        delegate.offRequest(handler);
    }

    @Override
    public void onRequestFailed(Consumer<Request> handler)
    {
        delegate.onRequestFailed(handler);
    }

    @Override
    public void offRequestFailed(Consumer<Request> handler)
    {
        delegate.offRequestFailed(handler);
    }

    @Override
    public void onRequestFinished(Consumer<Request> handler)
    {
        delegate.onRequestFinished(handler);
    }

    @Override
    public void offRequestFinished(Consumer<Request> handler)
    {
        delegate.offRequestFinished(handler);
    }

    @Override
    public void onResponse(Consumer<Response> handler)
    {
        delegate.onResponse(handler);
    }

    @Override
    public void offResponse(Consumer<Response> handler)
    {
        delegate.offResponse(handler);
    }

    @Override
    public void onWebSocket(Consumer<WebSocket> handler)
    {
        delegate.onWebSocket(handler);
    }

    @Override
    public void offWebSocket(Consumer<WebSocket> handler)
    {
        delegate.offWebSocket(handler);
    }

    @Override
    public void onWorker(Consumer<Worker> handler)
    {
        delegate.onWorker(handler);
    }

    @Override
    public void offWorker(Consumer<Worker> handler)
    {
        delegate.offWorker(handler);
    }

    @Override
    public Clock clock()
    {
        return delegate.clock();
    }

    @Override
    public void addInitScript(String script)
    {
        delegate.addInitScript(script);
    }

    @Override
    public void addInitScript(Path script)
    {
        delegate.addInitScript(script);
    }

    @Override
    public ElementHandle addScriptTag()
    {
        return delegate.addScriptTag();
    }

    @Override
    public ElementHandle addScriptTag(AddScriptTagOptions options)
    {
        return delegate.addScriptTag(options);
    }

    @Override
    public ElementHandle addStyleTag()
    {
        return delegate.addStyleTag();
    }

    @Override
    public ElementHandle addStyleTag(AddStyleTagOptions options)
    {
        return delegate.addStyleTag(options);
    }

    @Override
    public void bringToFront()
    {
        delegate.bringToFront();
    }

    @Override
    public void check(String selector)
    {
        delegate.check(selector);
    }

    @Override
    public void check(
            String selector,
            CheckOptions options)
    {
        delegate.check(selector, options);
    }

    @Override
    public void click(String selector)
    {
        delegate.click(selector);
    }

    @Override
    public void click(
            String selector,
            ClickOptions options)
    {
        delegate.click(selector, options);
    }

    @Override
    public void close()
    {
        delegate.close();
    }

    @Override
    public void close(CloseOptions options)
    {
        delegate.close(options);
    }

    @Override
    public String content()
    {
        return delegate.content();
    }

    @Override
    public BrowserContext context()
    {
        return delegate.context();
    }

    @Override
    public void dblclick(String selector)
    {
        delegate.dblclick(selector);
    }

    @Override
    public void dblclick(
            String selector,
            DblclickOptions options)
    {
        delegate.dblclick(selector, options);
    }

    @Override
    public void dispatchEvent(
            String selector,
            String type,
            Object eventInit)
    {
        delegate.dispatchEvent(selector, type, eventInit);
    }

    @Override
    public void dispatchEvent(
            String selector,
            String type)
    {
        delegate.dispatchEvent(selector, type);
    }

    @Override
    public void dispatchEvent(
            String selector,
            String type,
            Object eventInit,
            DispatchEventOptions options)
    {
        delegate.dispatchEvent(selector, type, eventInit, options);
    }

    @Override
    public void dragAndDrop(
            String source,
            String target)
    {
        delegate.dragAndDrop(source, target);
    }

    @Override
    public void dragAndDrop(
            String source,
            String target,
            DragAndDropOptions options)
    {
        delegate.dragAndDrop(source, target, options);
    }

    @Override
    public void emulateMedia()
    {
        delegate.emulateMedia();
    }

    @Override
    public void emulateMedia(EmulateMediaOptions options)
    {
        delegate.emulateMedia(options);
    }

    @Override
    public Object evalOnSelector(
            String selector,
            String expression,
            Object arg)
    {
        return delegate.evalOnSelector(selector, expression, arg);
    }

    @Override
    public Object evalOnSelector(
            String selector,
            String expression)
    {
        return delegate.evalOnSelector(selector, expression);
    }

    @Override
    public Object evalOnSelector(
            String selector,
            String expression,
            Object arg,
            EvalOnSelectorOptions options)
    {
        return delegate.evalOnSelector(selector, expression, arg, options);
    }

    @Override
    public Object evalOnSelectorAll(
            String selector,
            String expression)
    {
        return delegate.evalOnSelectorAll(selector, expression);
    }

    @Override
    public Object evalOnSelectorAll(
            String selector,
            String expression,
            Object arg)
    {
        return delegate.evalOnSelectorAll(selector, expression, arg);
    }

    @Override
    public Object evaluate(String expression)
    {
        return delegate.evaluate(expression);
    }

    @Override
    public Object evaluate(
            String expression,
            Object arg)
    {
        return delegate.evaluate(expression, arg);
    }

    @Override
    public JSHandle evaluateHandle(String expression)
    {
        return delegate.evaluateHandle(expression);
    }

    @Override
    public JSHandle evaluateHandle(
            String expression,
            Object arg)
    {
        return delegate.evaluateHandle(expression, arg);
    }

    @Override
    public void exposeBinding(
            String name,
            BindingCallback callback)
    {
        delegate.exposeBinding(name, callback);
    }

    @Override
    public void exposeBinding(
            String name,
            BindingCallback callback,
            ExposeBindingOptions options)
    {
        delegate.exposeBinding(name, callback, options);
    }

    @Override
    public void exposeFunction(
            String name,
            FunctionCallback callback)
    {
        delegate.exposeFunction(name, callback);
    }

    @Override
    public void fill(
            String selector,
            String value)
    {
        delegate.fill(selector, value);
    }

    @Override
    public void fill(
            String selector,
            String value,
            FillOptions options)
    {
        delegate.fill(selector, value, options);
    }

    @Override
    public void focus(String selector)
    {
        delegate.focus(selector);
    }

    @Override
    public void focus(
            String selector,
            FocusOptions options)
    {
        delegate.focus(selector, options);
    }

    @Override
    public Frame frame(String name)
    {
        return delegate.frame(name);
    }

    @Override
    public Frame frameByUrl(String url)
    {
        return delegate.frameByUrl(url);
    }

    @Override
    public Frame frameByUrl(Pattern url)
    {
        return delegate.frameByUrl(url);
    }

    @Override
    public Frame frameByUrl(Predicate<String> url)
    {
        return delegate.frameByUrl(url);
    }

    @Override
    public FrameLocator frameLocator(String selector)
    {
        return delegate.frameLocator(selector);
    }

    @Override
    public List<Frame> frames()
    {
        return delegate.frames();
    }

    @Override
    public String getAttribute(
            String selector,
            String name)
    {
        return delegate.getAttribute(selector, name);
    }

    @Override
    public String getAttribute(
            String selector,
            String name,
            GetAttributeOptions options)
    {
        return delegate.getAttribute(selector, name, options);
    }

    @Override
    public Locator getByAltText(String text)
    {
        return delegate.getByAltText(text);
    }

    @Override
    public Locator getByAltText(
            String text,
            GetByAltTextOptions options)
    {
        return delegate.getByAltText(text, options);
    }

    @Override
    public Locator getByAltText(Pattern text)
    {
        return delegate.getByAltText(text);
    }

    @Override
    public Locator getByAltText(
            Pattern text,
            GetByAltTextOptions options)
    {
        return delegate.getByAltText(text, options);
    }

    @Override
    public Locator getByLabel(String text)
    {
        return delegate.getByLabel(text);
    }

    @Override
    public Locator getByLabel(
            String text,
            GetByLabelOptions options)
    {
        return delegate.getByLabel(text, options);
    }

    @Override
    public Locator getByLabel(Pattern text)
    {
        return delegate.getByLabel(text);
    }

    @Override
    public Locator getByLabel(
            Pattern text,
            GetByLabelOptions options)
    {
        return delegate.getByLabel(text, options);
    }

    @Override
    public Locator getByPlaceholder(String text)
    {
        return delegate.getByPlaceholder(text);
    }

    @Override
    public Locator getByPlaceholder(
            String text,
            GetByPlaceholderOptions options)
    {
        return delegate.getByPlaceholder(text, options);
    }

    @Override
    public Locator getByPlaceholder(Pattern text)
    {
        return delegate.getByPlaceholder(text);
    }

    @Override
    public Locator getByPlaceholder(
            Pattern text,
            GetByPlaceholderOptions options)
    {
        return delegate.getByPlaceholder(text, options);
    }

    @Override
    public Locator getByRole(AriaRole role)
    {
        return delegate.getByRole(role);
    }

    @Override
    public Locator getByRole(
            AriaRole role,
            GetByRoleOptions options)
    {
        return delegate.getByRole(role, options);
    }

    @Override
    public Locator getByTestId(String testId)
    {
        return delegate.getByTestId(testId);
    }

    @Override
    public Locator getByTestId(Pattern testId)
    {
        return delegate.getByTestId(testId);
    }

    @Override
    public Locator getByText(String text)
    {
        return delegate.getByText(text);
    }

    @Override
    public Locator getByText(
            String text,
            GetByTextOptions options)
    {
        return delegate.getByText(text, options);
    }

    @Override
    public Locator getByText(Pattern text)
    {
        return delegate.getByText(text);
    }

    @Override
    public Locator getByText(
            Pattern text,
            GetByTextOptions options)
    {
        return delegate.getByText(text, options);
    }

    @Override
    public Locator getByTitle(String text)
    {
        return delegate.getByTitle(text);
    }

    @Override
    public Locator getByTitle(
            String text,
            GetByTitleOptions options)
    {
        return delegate.getByTitle(text, options);
    }

    @Override
    public Locator getByTitle(Pattern text)
    {
        return delegate.getByTitle(text);
    }

    @Override
    public Locator getByTitle(
            Pattern text,
            GetByTitleOptions options)
    {
        return delegate.getByTitle(text, options);
    }

    @Override
    public Response goBack()
    {
        return delegate.goBack();
    }

    @Override
    public Response goBack(GoBackOptions options)
    {
        return delegate.goBack(options);
    }

    @Override
    public Response goForward()
    {
        return delegate.goForward();
    }

    @Override
    public Response goForward(GoForwardOptions options)
    {
        return delegate.goForward(options);
    }

    @Override
    public void requestGC()
    {
        delegate.requestGC();
    }

    @Override
    public Response navigate(String url)
    {
        return delegate.navigate(url);
    }

    @Override
    public Response navigate(
            String url,
            NavigateOptions options)
    {
        return delegate.navigate(url, options);
    }

    @Override
    public void hover(String selector)
    {
        delegate.hover(selector);
    }

    @Override
    public void hover(
            String selector,
            HoverOptions options)
    {
        delegate.hover(selector, options);
    }

    @Override
    public String innerHTML(String selector)
    {
        return delegate.innerHTML(selector);
    }

    @Override
    public String innerHTML(
            String selector,
            InnerHTMLOptions options)
    {
        return delegate.innerHTML(selector, options);
    }

    @Override
    public String innerText(String selector)
    {
        return delegate.innerText(selector);
    }

    @Override
    public String innerText(
            String selector,
            InnerTextOptions options)
    {
        return delegate.innerText(selector, options);
    }

    @Override
    public String inputValue(String selector)
    {
        return delegate.inputValue(selector);
    }

    @Override
    public String inputValue(
            String selector,
            InputValueOptions options)
    {
        return delegate.inputValue(selector, options);
    }

    @Override
    public boolean isChecked(String selector)
    {
        return delegate.isChecked(selector);
    }

    @Override
    public boolean isChecked(
            String selector,
            IsCheckedOptions options)
    {
        return delegate.isChecked(selector, options);
    }

    @Override
    public boolean isClosed()
    {
        return delegate.isClosed();
    }

    @Override
    public boolean isDisabled(String selector)
    {
        return delegate.isDisabled(selector);
    }

    @Override
    public boolean isDisabled(
            String selector,
            IsDisabledOptions options)
    {
        return delegate.isDisabled(selector, options);
    }

    @Override
    public boolean isEditable(String selector)
    {
        return delegate.isEditable(selector);
    }

    @Override
    public boolean isEditable(
            String selector,
            IsEditableOptions options)
    {
        return delegate.isEditable(selector, options);
    }

    @Override
    public boolean isEnabled(String selector)
    {
        return delegate.isEnabled(selector);
    }

    @Override
    public boolean isEnabled(
            String selector,
            IsEnabledOptions options)
    {
        return delegate.isEnabled(selector, options);
    }

    @Override
    public boolean isHidden(String selector)
    {
        return delegate.isHidden(selector);
    }

    @Override
    public boolean isHidden(
            String selector,
            IsHiddenOptions options)
    {
        return delegate.isHidden(selector, options);
    }

    @Override
    public boolean isVisible(String selector)
    {
        return delegate.isVisible(selector);
    }

    @Override
    public boolean isVisible(
            String selector,
            IsVisibleOptions options)
    {
        return delegate.isVisible(selector, options);
    }

    @Override
    public Keyboard keyboard()
    {
        return delegate.keyboard();
    }

    @Override
    public Locator locator(String selector)
    {
        return delegate.locator(selector);
    }

    @Override
    public Locator locator(
            String selector,
            LocatorOptions options)
    {
        return delegate.locator(selector, options);
    }

    @Override
    public Frame mainFrame()
    {
        return delegate.mainFrame();
    }

    @Override
    public Mouse mouse()
    {
        return delegate.mouse();
    }

    @Override
    public void onceDialog(Consumer<Dialog> handler)
    {
        delegate.onceDialog(handler);
    }

    @Override
    public Page opener()
    {
        return delegate.opener();
    }

    @Override
    public void pause()
    {
        delegate.pause();
    }

    @Override
    public byte[] pdf()
    {
        return delegate.pdf();
    }

    @Override
    public byte[] pdf(PdfOptions options)
    {
        return delegate.pdf(options);
    }

    @Override
    public void press(
            String selector,
            String key)
    {
        delegate.press(selector, key);
    }

    @Override
    public void press(
            String selector,
            String key,
            PressOptions options)
    {
        delegate.press(selector, key, options);
    }

    @Override
    public ElementHandle querySelector(String selector)
    {
        return delegate.querySelector(selector);
    }

    @Override
    public ElementHandle querySelector(
            String selector,
            QuerySelectorOptions options)
    {
        return delegate.querySelector(selector, options);
    }

    @Override
    public List<ElementHandle> querySelectorAll(String selector)
    {
        return delegate.querySelectorAll(selector);
    }

    @Override
    public void addLocatorHandler(
            Locator locator,
            Consumer<Locator> handler)
    {
        delegate.addLocatorHandler(locator, handler);
    }

    @Override
    public void addLocatorHandler(
            Locator locator,
            Consumer<Locator> handler,
            AddLocatorHandlerOptions options)
    {
        delegate.addLocatorHandler(locator, handler, options);
    }

    @Override
    public void removeLocatorHandler(Locator locator)
    {
        delegate.removeLocatorHandler(locator);
    }

    @Override
    public Response reload()
    {
        return delegate.reload();
    }

    @Override
    public Response reload(ReloadOptions options)
    {
        return delegate.reload(options);
    }

    @Override
    public APIRequestContext request()
    {
        return delegate.request();
    }

    @Override
    public void route(
            String url,
            Consumer<Route> handler)
    {
        delegate.route(url, handler);
    }

    @Override
    public void route(
            String url,
            Consumer<Route> handler,
            RouteOptions options)
    {
        delegate.route(url, handler, options);
    }

    @Override
    public void route(
            Pattern url,
            Consumer<Route> handler)
    {
        delegate.route(url, handler);
    }

    @Override
    public void route(
            Pattern url,
            Consumer<Route> handler,
            RouteOptions options)
    {
        delegate.route(url, handler, options);
    }

    @Override
    public void route(
            Predicate<String> url,
            Consumer<Route> handler)
    {
        delegate.route(url, handler);
    }

    @Override
    public void route(
            Predicate<String> url,
            Consumer<Route> handler,
            RouteOptions options)
    {
        delegate.route(url, handler, options);
    }

    @Override
    public void routeFromHAR(Path har)
    {
        delegate.routeFromHAR(har);
    }

    @Override
    public void routeFromHAR(
            Path har,
            RouteFromHAROptions options)
    {
        delegate.routeFromHAR(har, options);
    }

    @Override
    public void routeWebSocket(
            String url,
            Consumer<WebSocketRoute> handler)
    {
        delegate.routeWebSocket(url, handler);
    }

    @Override
    public void routeWebSocket(
            Pattern url,
            Consumer<WebSocketRoute> handler)
    {
        delegate.routeWebSocket(url, handler);
    }

    @Override
    public void routeWebSocket(
            Predicate<String> url,
            Consumer<WebSocketRoute> handler)
    {
        delegate.routeWebSocket(url, handler);
    }

    @Override
    public byte[] screenshot()
    {
        return delegate.screenshot();
    }

    @Override
    public byte[] screenshot(ScreenshotOptions options)
    {
        return delegate.screenshot(options);
    }

    @Override
    public List<String> selectOption(
            String selector,
            String values)
    {
        return delegate.selectOption(selector, values);
    }

    @Override
    public List<String> selectOption(
            String selector,
            String values,
            SelectOptionOptions options)
    {
        return delegate.selectOption(selector, values, options);
    }

    @Override
    public List<String> selectOption(
            String selector,
            ElementHandle values)
    {
        return delegate.selectOption(selector, values);
    }

    @Override
    public List<String> selectOption(
            String selector,
            ElementHandle values,
            SelectOptionOptions options)
    {
        return delegate.selectOption(selector, values, options);
    }

    @Override
    public List<String> selectOption(
            String selector,
            String[] values)
    {
        return delegate.selectOption(selector, values);
    }

    @Override
    public List<String> selectOption(
            String selector,
            String[] values,
            SelectOptionOptions options)
    {
        return delegate.selectOption(selector, values, options);
    }

    @Override
    public List<String> selectOption(
            String selector,
            SelectOption values)
    {
        return delegate.selectOption(selector, values);
    }

    @Override
    public List<String> selectOption(
            String selector,
            SelectOption values,
            SelectOptionOptions options)
    {
        return delegate.selectOption(selector, values, options);
    }

    @Override
    public List<String> selectOption(
            String selector,
            ElementHandle[] values)
    {
        return delegate.selectOption(selector, values);
    }

    @Override
    public List<String> selectOption(
            String selector,
            ElementHandle[] values,
            SelectOptionOptions options)
    {
        return delegate.selectOption(selector, values, options);
    }

    @Override
    public List<String> selectOption(
            String selector,
            SelectOption[] values)
    {
        return delegate.selectOption(selector, values);
    }

    @Override
    public List<String> selectOption(
            String selector,
            SelectOption[] values,
            SelectOptionOptions options)
    {
        return delegate.selectOption(selector, values, options);
    }

    @Override
    public void setChecked(
            String selector,
            boolean checked)
    {
        delegate.setChecked(selector, checked);
    }

    @Override
    public void setChecked(
            String selector,
            boolean checked,
            SetCheckedOptions options)
    {
        delegate.setChecked(selector, checked, options);
    }

    @Override
    public void setContent(String html)
    {
        delegate.setContent(html);
    }

    @Override
    public void setContent(
            String html,
            SetContentOptions options)
    {
        delegate.setContent(html, options);
    }

    @Override
    public void setDefaultNavigationTimeout(double timeout)
    {
        delegate.setDefaultNavigationTimeout(timeout);
    }

    @Override
    public void setDefaultTimeout(double timeout)
    {
        delegate.setDefaultTimeout(timeout);
    }

    @Override
    public void setExtraHTTPHeaders(Map<String, String> headers)
    {
        delegate.setExtraHTTPHeaders(headers);
    }

    @Override
    public void setInputFiles(
            String selector,
            Path files)
    {
        delegate.setInputFiles(selector, files);
    }

    @Override
    public void setInputFiles(
            String selector,
            Path files,
            SetInputFilesOptions options)
    {
        delegate.setInputFiles(selector, files, options);
    }

    @Override
    public void setInputFiles(
            String selector,
            Path[] files)
    {
        delegate.setInputFiles(selector, files);
    }

    @Override
    public void setInputFiles(
            String selector,
            Path[] files,
            SetInputFilesOptions options)
    {
        delegate.setInputFiles(selector, files, options);
    }

    @Override
    public void setInputFiles(
            String selector,
            FilePayload files)
    {
        delegate.setInputFiles(selector, files);
    }

    @Override
    public void setInputFiles(
            String selector,
            FilePayload files,
            SetInputFilesOptions options)
    {
        delegate.setInputFiles(selector, files, options);
    }

    @Override
    public void setInputFiles(
            String selector,
            FilePayload[] files)
    {
        delegate.setInputFiles(selector, files);
    }

    @Override
    public void setInputFiles(
            String selector,
            FilePayload[] files,
            SetInputFilesOptions options)
    {
        delegate.setInputFiles(selector, files, options);
    }

    @Override
    public void setViewportSize(
            int width,
            int height)
    {
        delegate.setViewportSize(width, height);
    }

    @Override
    public void tap(String selector)
    {
        delegate.tap(selector);
    }

    @Override
    public void tap(
            String selector,
            TapOptions options)
    {
        delegate.tap(selector, options);
    }

    @Override
    public String textContent(String selector)
    {
        return delegate.textContent(selector);
    }

    @Override
    public String textContent(
            String selector,
            TextContentOptions options)
    {
        return delegate.textContent(selector, options);
    }

    @Override
    public String title()
    {
        return delegate.title();
    }

    @Override
    public Touchscreen touchscreen()
    {
        return delegate.touchscreen();
    }

    @Override
    public void type(
            String selector,
            String text)
    {
        delegate.type(selector, text);
    }

    @Override
    public void type(
            String selector,
            String text,
            TypeOptions options)
    {
        delegate.type(selector, text, options);
    }

    @Override
    public void uncheck(String selector)
    {
        delegate.uncheck(selector);
    }

    @Override
    public void uncheck(
            String selector,
            UncheckOptions options)
    {
        delegate.uncheck(selector, options);
    }

    @Override
    public void unrouteAll()
    {
        delegate.unrouteAll();
    }

    @Override
    public void unroute(String url)
    {
        delegate.unroute(url);
    }

    @Override
    public void unroute(
            String url,
            Consumer<Route> handler)
    {
        delegate.unroute(url, handler);
    }

    @Override
    public void unroute(Pattern url)
    {
        delegate.unroute(url);
    }

    @Override
    public void unroute(
            Pattern url,
            Consumer<Route> handler)
    {
        delegate.unroute(url, handler);
    }

    @Override
    public void unroute(Predicate<String> url)
    {
        delegate.unroute(url);
    }

    @Override
    public void unroute(
            Predicate<String> url,
            Consumer<Route> handler)
    {
        delegate.unroute(url, handler);
    }

    @Override
    public String url()
    {
        return delegate.url();
    }

    @Override
    public Video video()
    {
        return delegate.video();
    }

    @Override
    public ViewportSize viewportSize()
    {
        return delegate.viewportSize();
    }

    @Override
    public Page waitForClose(Runnable callback)
    {
        return delegate.waitForClose(callback);
    }

    @Override
    public Page waitForClose(
            WaitForCloseOptions options,
            Runnable callback)
    {
        return delegate.waitForClose(options, callback);
    }

    @Override
    public ConsoleMessage waitForConsoleMessage(Runnable callback)
    {
        return delegate.waitForConsoleMessage(callback);
    }

    @Override
    public ConsoleMessage waitForConsoleMessage(
            WaitForConsoleMessageOptions options,
            Runnable callback)
    {
        return delegate.waitForConsoleMessage(options, callback);
    }

    @Override
    public Download waitForDownload(Runnable callback)
    {
        return delegate.waitForDownload(callback);
    }

    @Override
    public Download waitForDownload(
            WaitForDownloadOptions options,
            Runnable callback)
    {
        return delegate.waitForDownload(options, callback);
    }

    @Override
    public FileChooser waitForFileChooser(Runnable callback)
    {
        return delegate.waitForFileChooser(callback);
    }

    @Override
    public FileChooser waitForFileChooser(
            WaitForFileChooserOptions options,
            Runnable callback)
    {
        return delegate.waitForFileChooser(options, callback);
    }

    @Override
    public JSHandle waitForFunction(
            String expression,
            Object arg)
    {
        return delegate.waitForFunction(expression, arg);
    }

    @Override
    public JSHandle waitForFunction(String expression)
    {
        return delegate.waitForFunction(expression);
    }

    @Override
    public JSHandle waitForFunction(
            String expression,
            Object arg,
            WaitForFunctionOptions options)
    {
        return delegate.waitForFunction(expression, arg, options);
    }

    @Override
    public void waitForLoadState(LoadState state)
    {
        delegate.waitForLoadState(state);
    }

    @Override
    public void waitForLoadState()
    {
        delegate.waitForLoadState();
    }

    @Override
    public void waitForLoadState(
            LoadState state,
            WaitForLoadStateOptions options)
    {
        delegate.waitForLoadState(state, options);
    }

    @Override
    public Response waitForNavigation(Runnable callback)
    {
        return delegate.waitForNavigation(callback);
    }

    @Override
    public Response waitForNavigation(
            WaitForNavigationOptions options,
            Runnable callback)
    {
        return delegate.waitForNavigation(options, callback);
    }

    @Override
    public Page waitForPopup(Runnable callback)
    {
        return delegate.waitForPopup(callback);
    }

    @Override
    public Page waitForPopup(
            WaitForPopupOptions options,
            Runnable callback)
    {
        return delegate.waitForPopup(options, callback);
    }

    @Override
    public Request waitForRequest(
            String urlOrPredicate,
            Runnable callback)
    {
        return delegate.waitForRequest(urlOrPredicate, callback);
    }

    @Override
    public Request waitForRequest(
            String urlOrPredicate,
            WaitForRequestOptions options,
            Runnable callback)
    {
        return delegate.waitForRequest(urlOrPredicate, options, callback);
    }

    @Override
    public Request waitForRequest(
            Pattern urlOrPredicate,
            Runnable callback)
    {
        return delegate.waitForRequest(urlOrPredicate, callback);
    }

    @Override
    public Request waitForRequest(
            Pattern urlOrPredicate,
            WaitForRequestOptions options,
            Runnable callback)
    {
        return delegate.waitForRequest(urlOrPredicate, options, callback);
    }

    @Override
    public Request waitForRequest(
            Predicate<Request> urlOrPredicate,
            Runnable callback)
    {
        return delegate.waitForRequest(urlOrPredicate, callback);
    }

    @Override
    public Request waitForRequest(
            Predicate<Request> urlOrPredicate,
            WaitForRequestOptions options,
            Runnable callback)
    {
        return delegate.waitForRequest(urlOrPredicate, options, callback);
    }

    @Override
    public Request waitForRequestFinished(Runnable callback)
    {
        return delegate.waitForRequestFinished(callback);
    }

    @Override
    public Request waitForRequestFinished(
            WaitForRequestFinishedOptions options,
            Runnable callback)
    {
        return delegate.waitForRequestFinished(options, callback);
    }

    @Override
    public Response waitForResponse(
            String urlOrPredicate,
            Runnable callback)
    {
        return delegate.waitForResponse(urlOrPredicate, callback);
    }

    @Override
    public Response waitForResponse(
            String urlOrPredicate,
            WaitForResponseOptions options,
            Runnable callback)
    {
        return delegate.waitForResponse(urlOrPredicate, options, callback);
    }

    @Override
    public Response waitForResponse(
            Pattern urlOrPredicate,
            Runnable callback)
    {
        return delegate.waitForResponse(urlOrPredicate, callback);
    }

    @Override
    public Response waitForResponse(
            Pattern urlOrPredicate,
            WaitForResponseOptions options,
            Runnable callback)
    {
        return delegate.waitForResponse(urlOrPredicate, options, callback);
    }

    @Override
    public Response waitForResponse(
            Predicate<Response> urlOrPredicate,
            Runnable callback)
    {
        return delegate.waitForResponse(urlOrPredicate, callback);
    }

    @Override
    public Response waitForResponse(
            Predicate<Response> urlOrPredicate,
            WaitForResponseOptions options,
            Runnable callback)
    {
        return delegate.waitForResponse(urlOrPredicate, options, callback);
    }

    @Override
    public ElementHandle waitForSelector(String selector)
    {
        return delegate.waitForSelector(selector);
    }

    @Override
    public ElementHandle waitForSelector(
            String selector,
            WaitForSelectorOptions options)
    {
        return delegate.waitForSelector(selector, options);
    }

    @Override
    public void waitForCondition(BooleanSupplier condition)
    {
        delegate.waitForCondition(condition);
    }

    @Override
    public void waitForCondition(
            BooleanSupplier condition,
            WaitForConditionOptions options)
    {
        delegate.waitForCondition(condition, options);
    }

    @Override
    public void waitForTimeout(double timeout)
    {
        delegate.waitForTimeout(timeout);
    }

    @Override
    public void waitForURL(String url)
    {
        delegate.waitForURL(url);
    }

    @Override
    public void waitForURL(
            String url,
            WaitForURLOptions options)
    {
        delegate.waitForURL(url, options);
    }

    @Override
    public void waitForURL(Pattern url)
    {
        delegate.waitForURL(url);
    }

    @Override
    public void waitForURL(
            Pattern url,
            WaitForURLOptions options)
    {
        delegate.waitForURL(url, options);
    }

    @Override
    public void waitForURL(Predicate<String> url)
    {
        delegate.waitForURL(url);
    }

    @Override
    public void waitForURL(
            Predicate<String> url,
            WaitForURLOptions options)
    {
        delegate.waitForURL(url, options);
    }

    @Override
    public WebSocket waitForWebSocket(Runnable callback)
    {
        return delegate.waitForWebSocket(callback);
    }

    @Override
    public WebSocket waitForWebSocket(
            WaitForWebSocketOptions options,
            Runnable callback)
    {
        return delegate.waitForWebSocket(options, callback);
    }

    @Override
    public Worker waitForWorker(Runnable callback)
    {
        return delegate.waitForWorker(callback);
    }

    @Override
    public Worker waitForWorker(
            WaitForWorkerOptions options,
            Runnable callback)
    {
        return delegate.waitForWorker(options, callback);
    }

    @Override
    public List<Worker> workers()
    {
        return delegate.workers();
    }
}
