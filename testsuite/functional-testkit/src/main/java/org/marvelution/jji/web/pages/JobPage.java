/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.pages;

import org.marvelution.jji.web.elements.admin.*;

import com.codeborne.selenide.*;

import static com.codeborne.selenide.Condition.*;
import static org.openqa.selenium.By.*;

/**
 * @author Mark Rekveld
 * @since 1.14.0
 */
public class JobPage
        extends JobListPage
{

    public static JobActions actionsDropDown()
    {
        return new JobActions(header().buttons()
                                      .find(cssClass("aui-button-split-more")));
    }

    public static SelenideElement buildList()
    {
        return content().find(className("build-list"));
    }

    public static ElementsCollection builds()
    {
        return buildList().findAll("tbody > tr");
    }
}
