/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.elements.admin;

import org.marvelution.jji.web.elements.*;

import com.codeborne.selenide.*;
import org.openqa.selenium.*;

import static com.codeborne.selenide.Selenide.*;
import static org.openqa.selenium.By.*;

/**
 * @author Mark Rekveld
 * @since 1.14.0
 */
public class BuildRow
		extends Row {

	public BuildRow(WebElement element) {
		this($(element));
	}

	public BuildRow(SelenideElement container) {
		super(container);
	}

	public SelenideElement number() {
		return column(0);
	}

	public SelenideElement linkTo() {
		return number().find(tagName("a"));
	}

	public SelenideElement cause() {
		return column(2);
	}

	public SelenideElement date() {
		return column(3);
	}

	public SelenideElement duration() {
		return column(4);
	}

	public ElementsCollection issueLinks() {
		return column(5).findAll(tagName("a"));
	}
}
