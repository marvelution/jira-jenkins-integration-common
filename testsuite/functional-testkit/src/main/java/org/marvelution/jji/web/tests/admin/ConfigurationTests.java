/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.tests.admin;

import org.marvelution.jji.model.Configuration;
import org.marvelution.jji.test.license.License;
import org.marvelution.jji.test.license.LicensedTest;
import org.marvelution.jji.web.tests.assertions.JenkinsAssertions;

import com.microsoft.playwright.Locator;
import com.microsoft.playwright.Page;
import org.junit.jupiter.api.Test;

import static com.microsoft.playwright.assertions.PlaywrightAssertions.assertThat;
import static org.marvelution.jji.data.services.api.ConfigurationService.*;

public interface ConfigurationTests
        extends ScopedConfigurationTests
{

    void openConfigurationPage(Page page);

    @Override
    default boolean canAccessGlobalConfiguration()
    {
        return true;
    }

    default void openFeatureDialog(Page page)
    {
        openConfigurationPage(page);
        page.locator("button[data-dialog-module=features]")
                .click();
    }

    @LicensedTest
    default void testConfiguration(Page page, License license)
    {
        openConfigurationPage(page);

        JenkinsAssertions.assertLicense(page)
                .hasState(license);

        Configuration configuration = getConfiguration(GLOBAL_SCOPE);

        assertCommonConfiguration(page, configuration, license);

        assertThat(page.locator("input[name=deleted_data_retention_period]")).hasValue(configuration.requireConfiguration(
                        DELETED_DATA_RETENTION_PERIOD)
                .getValue());
        assertThat(page.locator("input[name=audit_data_retention_period]")).hasValue(configuration.requireConfiguration(
                        AUDIT_DATA_RETENTION_PERIOD)
                .getValue());

        additionalConfiguration(page, configuration, license);
    }

    void additionalConfiguration(
            Page page,
            Configuration configuration,
            License license);

    @Test
    default void testFeatures(Page page)
    {
        openFeatureDialog(page);

        Locator siteFeatures = page.locator("#site-features");
        assertThat(siteFeatures.locator("h2")).hasText("Site Features");
        assertThat(siteFeatures.locator(".aui-message-info")).hasText(
                "These features can be enabled/disabled to customize the integration between Jenkins and Jira.");

        Locator systemFeatures = page.locator("#system-features");
        assertThat(systemFeatures.locator("h2")).hasText("System Features");
        assertThat(systemFeatures.locator(".aui-message-info")).hasText("These features are part of the core system and cannot by disabled.");
    }
}
