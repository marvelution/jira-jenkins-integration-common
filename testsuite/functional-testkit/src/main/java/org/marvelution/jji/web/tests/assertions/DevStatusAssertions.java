/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.tests.assertions;

import java.util.regex.Pattern;
import java.util.stream.Stream;

import org.marvelution.jji.web.tests.panel.DevStatusPanel;

import static com.microsoft.playwright.assertions.PlaywrightAssertions.assertThat;

public class DevStatusAssertions
{
    private final DevStatusPanel devStatusPanel;

    DevStatusAssertions(DevStatusPanel devStatusPanel)
    {
        this.devStatusPanel = devStatusPanel;
    }

    public DevStatusAssertions hasMessage(String message)
    {
        assertThat(devStatusPanel.messageContainer()).hasText(message);
        return this;
    }

    public DevStatusAssertions messageNotVisible()
    {
        assertThat(devStatusPanel.messageContainer()).not()
                .isVisible();
        return this;
    }

    public DevStatusElementAssertions buildStats()
    {
        return new DevStatusElementAssertions(devStatusPanel.buildStats());
    }

    public DevStatusElementAssertions deployStats()
    {
        return new DevStatusElementAssertions(devStatusPanel.deployStats());
    }

    public static class DevStatusElementAssertions
    {
        private final DevStatusPanel.Stats stats;

        DevStatusElementAssertions(DevStatusPanel.Stats stats)
        {
            this.stats = stats;
        }

        public DevStatusElementAssertions hasButton(String text)
        {
            assertThat(stats.button()).hasText(text);
            return this;
        }

        public DevStatusElementAssertions hasIcon(String... classes)
        {
            Stream.of(classes)
                    .forEach(className -> assertThat(stats.icon()).hasClass(Pattern.compile(className)));
            return this;
        }

        public DevStatusElementAssertions hasTimestamp(String text)
        {
            assertThat(stats.timestamp()).containsText(text);
            return this;
        }
    }
}
