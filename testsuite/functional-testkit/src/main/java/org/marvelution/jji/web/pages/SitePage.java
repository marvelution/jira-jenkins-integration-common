/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.pages;

import org.marvelution.jji.web.elements.admin.SiteActions;
import org.marvelution.jji.web.elements.admin.StartStepsElement;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Condition.cssClass;
import static org.openqa.selenium.By.className;

/**
 * @author Mark Rekveld
 * @since 1.4.0
 */
public class SitePage
        extends JobListPage
{

    public static SiteActions actionsDropDown()
    {
        return new SiteActions(header().buttons()
                                       .find(cssClass("aui-button-split-more")));
    }

    public static StartStepsElement startSteps()
    {
        return new StartStepsElement(content().find(className("jenkins-integration-start")));
    }

    public static SelenideElement registrationWarning()
    {
        return header().buttons()
                       .find(cssClass("registration-warning"));
    }
}
