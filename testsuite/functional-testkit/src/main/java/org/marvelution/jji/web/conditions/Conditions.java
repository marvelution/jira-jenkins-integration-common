/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.conditions;

import java.util.*;
import java.util.function.*;

import org.marvelution.jji.model.*;
import org.marvelution.jji.test.license.*;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.*;
import org.hamcrest.*;
import org.openqa.selenium.*;

import static java.util.Collections.*;

/**
 * @author Mark Rekveld
 * @since 1.4.0
 */
public class Conditions
{

	public static Button button(
			String label,
			String... attributes)
	{
		if (attributes != null)
		{
			return button(label, createAttributeMap(attributes));
		}
		else
		{
			return button(label, emptyMap());
		}
	}

	public static Button button(
			String label,
			Map<String, String> attributes)
	{
		return new Button(label, attributes);
	}

	public static SiteRow row(Site site)
	{
		return row(site, License.ACTIVE);
	}

	public static SiteRow row(
			Site site,
			License license)
	{
		return new SiteRow(site, license);
	}

	public static JobRow row(Job job)
	{
		return row(job, License.ACTIVE);
	}

	public static JobRow row(
			Job job,
			License license)
	{
		return new JobRow(job, license);
	}

	/**
	 * @since 1.14.0
	 */
	public static BuildRow row(
			Build build,
			License license)
	{
		return new BuildRow(build, license);
	}

	public static StartStepsLogo startStepsLogo = new StartStepsLogo();

	public static StartStep startStep(
			String title,
			String description)
	{
		return new StartStep(title, description);
	}

	public static StartStepsAction startStepsAction(Button... buttons)
	{
		return new StartStepsAction(buttons);
	}

	public static StartStepsAction startStepsAction(
			String text,
			String... attributes)
	{
		return startStepsAction(button(text, attributes));
	}

	public static Map<String, String> createAttributeMap(String... attributes)
	{
		if (attributes == null || attributes.length == 0)
		{
			return emptyMap();
		}
		else if (attributes.length % 2 != 0)
		{
			throw new IllegalArgumentException("Expecting even number for name and value combination.");
		}
		else
		{
			Map<String, String> attributeMap = new HashMap<>(attributes.length / 2);
			for (int i = 0;
			     i < attributes.length;
			     i += 2)
			{
				attributeMap.put(attributes[i], attributes[i + 1]);
			}
			return attributeMap;
		}
	}

	/**
	 * @since 1.1.5.0
	 */
	public static Condition attributeEndsWith(
			String attributeName,
			String expectedAttributeEnding)
	{
		return attribute(attributeName, new Predicate<String>() {
			@Override
			public boolean test(String string)
			{
				return string.endsWith(expectedAttributeEnding);
			}

			@Override
			public String toString()
			{
				return "ends with " + expectedAttributeEnding;
			}
		});
	}

	public static Condition attribute(
			String attributeName,
			Predicate<String> expectedAttributeMatcher)
	{
		return new Condition("attribute")
		{
			@Override
			public boolean apply(
					Driver driver,
					WebElement element)
			{
				return expectedAttributeMatcher.test(element.getAttribute(attributeName));
			}

			@Override
			public String toString()
			{
				return getName() + " " + attributeName + " " + expectedAttributeMatcher.toString();
			}
		};
	}

	/**
	 * Customized {@link Condition#disabled} to support AUI custom elements.
	 *
	 * @see Condition#disabled
	 * @since 1.8.0
	 */
	public static Condition disabled = new DisabledCondition("disabled")
	{};

	/**
	 * Customized {@link Condition#enabled} to support AUI custom elements.
	 *
	 * @see Condition#enabled
	 * @since 1.8.0
	 */
	public static Condition enabled = new DisabledCondition("enabled")
	{
		@Override
		public boolean apply(Driver driver, WebElement element)
		{
			return !super.apply(driver, element);
		}
	};

	private static abstract class DisabledCondition
			extends Condition
	{

		DisabledCondition(String name)
		{
			super(name);
		}

		@Override
		public boolean apply(
				Driver driver,
				WebElement element)
		{
			return !element.isEnabled() || Boolean.parseBoolean(element.getAttribute("aria-disabled")) ||
					element.getAttribute("disabled") != null;
		}

		@Override
		public String actualValue(
				Driver driver,
				WebElement element)
		{
			String ariaDisabled = element.getAttribute("aria-disabled");
			if (Boolean.parseBoolean(ariaDisabled))
			{
				return "aria-disabled";
			}
			else if (element.getAttribute("disabled") != null)
			{
				return "disabled";
			}
			else
			{
				return element.isEnabled() ? "enabled" : "disabled";
			}
		}
	}

	/**
	 * Customized {@link Condition#checked} to support AUI custom elements.
	 *
	 * @see Condition#checked
	 * @since 1.8.0
	 */
	public static final Condition checked = new Condition("checked")
	{
		@Override
		public boolean apply(
				Driver driver,
				WebElement element)
		{
			return element.isSelected() || element.getAttribute("checked") != null;
		}

		@Override
		public String actualValue(
				Driver driver,
				WebElement element)
		{
			return String.valueOf(apply(driver, element));
		}
	};

	/**
	 * Customized {@link Condition#selected} to support AUI custom elements.
	 *
	 * @see Condition#selected
	 * @since 1.8.0
	 */
	public static final Condition selected = new Condition("selected")
	{
		@Override
		public boolean apply(
				Driver driver,
				WebElement element)
		{
			return element.isSelected() || element.getAttribute("selected") != null;
		}

		@Override
		public String actualValue(
				Driver driver,
				WebElement element)
		{
			return String.valueOf(apply(driver, element));
		}
	};
}
