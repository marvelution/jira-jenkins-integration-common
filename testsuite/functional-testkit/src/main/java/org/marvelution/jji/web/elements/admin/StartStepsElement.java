/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.elements.admin;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static org.openqa.selenium.By.className;
import static org.openqa.selenium.By.tagName;

/**
 * @author Mark Rekveld
 * @since 1.4.0
 */
public class StartStepsElement {

	private final SelenideElement root;

	public StartStepsElement(SelenideElement root) {
		this.root = root;
	}

	public ElementsCollection steps() {
		return root.findAll(tagName("li"));
	}

	public SelenideElement action() {
		return actions().first();
	}

	public ElementsCollection actions() {
		return root.findAll(className("aui-button"));
	}

	public StartStepsElement shouldHave(Condition... condition) {
		root.shouldHave(condition);
		return this;
	}

	public StartStepsElement shouldBe(Condition... condition) {
		root.shouldBe(condition);
		return this;
	}

	public StartStepsElement shouldNotBe(Condition... condition) {
		root.shouldNotBe(condition);
		return this;
	}

	public StartStepsElement should(Condition... condition) {
		root.should(condition);
		return this;
	}

	public StartStepsElement shouldNot(Condition... condition) {
		root.shouldNot(condition);
		return this;
	}
}
