/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.tests.panel;

import java.net.URI;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.temporal.Temporal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

import org.marvelution.jji.data.helper.PanelBuild;
import org.marvelution.jji.data.helper.PanelJob;
import org.marvelution.jji.model.*;
import org.marvelution.jji.web.tests.playwright.PlaywrightExtension;
import org.marvelution.testing.wiremock.FileSource;
import org.marvelution.testing.wiremock.WireMockOptions;
import org.marvelution.testing.wiremock.WireMockServer;

import com.google.common.net.UrlEscapers;
import org.junit.jupiter.api.extension.ExtendWith;

import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.toList;
import static java.util.stream.IntStream.range;
import static org.marvelution.jji.model.DeploymentEnvironmentType.TESTING;
import static org.marvelution.testing.wiremock.FileSourceType.UNDER_CLASSPATH;

@ExtendWith(PlaywrightExtension.class)
@WireMockOptions(fileSource = @FileSource(type = UNDER_CLASSPATH,
                                          path = "MinimalSite"))
public interface PanelTests
{

    AtomicInteger SITE_COUNTER = new AtomicInteger(1);

    Site saveSite(Site site);

    Job saveJob(Job job);

    Build saveBuild(Build build);

    void linkBuildToIssue(
            Build build,
            String issueKey);

    String getExistingIssueKey();

    default String formatDate(Temporal temporal)
    {
        return formatDate(temporal, ZoneOffset.UTC);
    }

    String formatDate(
            Temporal temporal,
            ZoneId zoneId);

    void runLinkStatistician(String issueKey);

    default List<PanelJob> createPanelJobData(String issueKey)
    {
        return createPanelJobData(issueKey,
                saveSite(newSite().setInaccessibleSite()
                        .setRpcUrl(URI.create("https://jenkins.example.com/" + issueKey))));
    }

    default List<PanelJob> createPanelJobData(
            String issueKey,
            WireMockServer server)
    {
        return createPanelJobData(issueKey, saveSite(newSite().setRpcUrl(server.serverUri())));
    }

    default Site newSite()
    {
        return new Site().setType(SiteType.JENKINS)
                .setName("Jenkins " + SITE_COUNTER.getAndIncrement())
                .setJenkinsPluginInstalled(true);
    }

    default List<PanelJob> createPanelJobData(
            String issueKey,
            Site site)
    {
        List<PanelJob> jobs = range(1, 3).mapToObj(n -> new Job().setSite(site)
                        .setUrlName("job-" + n)
                        .setName("Job " + n)
                        .setLinked(true))
                .peek(job -> job.setUrlName(UrlEscapers.urlPathSegmentEscaper()
                        .escape(job.getName())))
                .map(this::saveJob)
                .map(job -> new PanelJob(job, new ArrayList<>()))
                .peek(job -> Stream.of(Result.values())
                        .map(this::createBuild)
                        .peek(build -> build.setJob(job.getJob()))
                        .map(this::saveBuild)
                        .peek(build -> linkBuildToIssue(build, issueKey))
                        .map(PanelBuild::new)
                        .peek(job.getBuilds()::add)
                        .max(Comparator.comparingInt(PanelBuild::getNumber))
                        .map(PanelBuild::getNumber)
                        .ifPresent(job.getJob()::setLastBuild))
                .peek(job -> job.getBuilds()
                        .sort(comparing(PanelBuild::getDate).reversed()))
                .peek(job -> saveJob(job.getJob()))
                .collect(toList());
        jobs.stream()
                .skip(1)
                .limit(1)
                .forEach(job -> {
                    Build build = saveBuild(createBuild(Result.SUCCESS).setTimestamp(ZonedDateTime.now(ZoneOffset.UTC)
                                    .minusMonths(1)
                                    .withDayOfMonth(10)
                                    .toInstant()
                                    .toEpochMilli())
                            .setNumber(10)
                            .setJob(job.getJob()));
                    job.setLastBuild(new PanelBuild(build));
                    job.getJob()
                            .setLastBuild(build.getNumber());
                    saveJob(job.getJob());
                });
        runLinkStatistician(issueKey);
        return jobs;
    }

    default Build createBuild(Result result)
    {
        int number = result.ordinal() + 1;
        long timestamp = ZonedDateTime.now(ZoneOffset.UTC)
                .minusMonths(1)
                .withDayOfMonth(number)
                .toInstant()
                .toEpochMilli();
        Build build = new Build().setResult(result)
                .setNumber(number)
                .setDuration(number * 10000L)
                .setCause("SCM Change " + number)
                .setTimestamp(timestamp);
        if (number % 2 == 0)
        {
            build.setDisplayName("v" + number + ".0");
        }
        TestResults testResults = new TestResults().setTotal(10);
        switch (result)
        {
            case FAILURE:
                testResults.setFailed(5);
                break;
            case UNSTABLE:
                testResults.setSkipped(5);
                break;
            case UNKNOWN:
                build.setDeleted(true);
                break;
        }
        return build.setTestResults(testResults)
                .setDeploymentEnvironments(Collections.singletonList(new DeploymentEnvironment().setId("test")
                        .setName("Test")
                        .setType(TESTING)));
    }
}
