/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.tests.admin;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import org.marvelution.jji.model.Build;
import org.marvelution.jji.model.Job;
import org.marvelution.jji.model.Site;
import org.marvelution.jji.model.request.BulkRequest;
import org.marvelution.jji.test.condition.DisabledIfJiraEdition;
import org.marvelution.jji.test.condition.JiraEdition;
import org.marvelution.jji.test.license.License;
import org.marvelution.jji.test.license.LicensedTest;
import org.marvelution.jji.web.tests.assertions.*;
import org.marvelution.testing.wiremock.WireMockServer;

import com.microsoft.playwright.Locator;
import com.microsoft.playwright.Page;
import com.microsoft.playwright.assertions.LocatorAssertions;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import static com.microsoft.playwright.assertions.PlaywrightAssertions.assertThat;
import static java.lang.Math.min;
import static java.time.temporal.ChronoUnit.MINUTES;
import static java.util.Collections.reverseOrder;
import static java.util.Comparator.comparingLong;
import static java.util.stream.IntStream.range;
import static org.awaitility.Awaitility.await;
import static org.marvelution.jji.model.Result.SUCCESS;

public interface ManageJobsTests
        extends SiteManagementTests
{

    Job saveJob(Job job);

    Build saveBuild(Build build);

    default void navigateToJob(
            Page page,
            Job job)
    {
        navigateToSite(page, job.getSite());

        navigateToJobFromList(page, job);
    }

    default void navigateToJobFromList(
            Page page,
            Job job)
    {
        JenkinsAssertions.assertJobList(page)
                .job(job.getId())
                .linkTo()
                .click();
    }

    void navigateDirectToJob(
            Page page,
            Job job);

    @Test
    default void testUnknownJob(Page page)
    {
        Job job = new Job().setId("unknown-job");

        navigateDirectToJob(page, job);

        JenkinsAssertions.assertHeader(page)
                .hasTitle("Unknown Job!!1");
    }

    default Job createRootJob()
    {
        Site site = createFirewalledSite();
        return saveJob(new Job().setSite(site)
                .setName("root-job"));
    }

    @LicensedTest
    default void testJobPageHeader(
            Page page,
            License license)
    {
        Job job = createRootJob();

        navigateDirectToJob(page, job);

        JenkinsAssertions.assertLicense(page)
                .hasState(license);

        Site site = job.getSite();

        PageHeaderAssertions headerAssertions = JenkinsAssertions.assertHeader(page)
                .hasTitle(job.getName())
                .hasAvatarImg("/images/icon96_" + site.getType()
                        .value() + ".png")
                .hasHelp("/administration/jenkins-sites/#manage-jobs")
                .hasActionsMenu("Actions",
                        Map.of("id", "job-menu-dropdown-trigger-" + job.getId(), "aria-controls", "job-menu-dropdown-" + job.getId()),
                        license);

        LocatorAssertions linkedAssertion = assertThat(headerAssertions.header()
                .locator("aui-toggle"));
        if (!job.isLinked())
        {
            linkedAssertion = linkedAssertion.not();
        }
        linkedAssertion.hasAttribute("checked", "checked");

        assertThat(headerAssertions.header()
                .locator("#s2id_job-quick-search")).isVisible();

        headerAssertions.assertMainAction()
                .isVisible(new LocatorAssertions.IsVisibleOptions().setVisible(!site.isInaccessibleSite()));
    }

    @Test
    default void testJobWithoutSubJobs(Page page)
    {
        Job job = createRootJob();
        generateBuilds(job, 10);

        navigateDirectToJob(page, job);
        assertJobPage(page, job, License.ACTIVE);
    }

    @Test
    default void testJob(Page page)
    {
        Job job = createRootJob();
        Job child = saveJob(new Job().setSite(job.getSite())
                .setName("child-job")
                .setParentName(job.getFullName()));
        job.addJob(child);
        Job grandChild = saveJob(new Job().setSite(job.getSite())
                .setName("grand-child-job")
                .setParentName(child.getFullName()));
        generateBuilds(grandChild, 15);
        child.addJob(grandChild);

        navigateToJob(page, job);
        assertJobPage(page, job, License.ACTIVE);

        navigateToJobFromList(page, child);

        assertJobPage(page, child, License.ACTIVE, job);

        navigateToJobFromList(page, grandChild);

        assertJobPage(page, grandChild, License.ACTIVE, job, child);
    }

    default void generateBuilds(
            Job job,
            int buildCount)
    {
        range(1, buildCount + 1).mapToObj(number -> new Build().setJob(job)
                        .setNumber(number)
                        .setResult(SUCCESS)
                        .setCause("SCM Change")
                        .setDuration(Duration.ofMinutes(2)
                                .toMillis())
                        .setTimestamp(Instant.now()
                                .minus(buildCount - number, MINUTES)
                                .toEpochMilli()))
                .map(this::saveBuild)
                .forEach(job::addBuild);
        saveJob(job.setOldestBuild(1)
                .setLastBuild(buildCount));
    }

    @LicensedTest
    default void testJobQuickSearch(
            Page page,
            License license)
    {
        Job job = createRootJob();
        Job child = saveJob(new Job().setSite(job.getSite())
                .setName("child-job")
                .setParentName(job.getFullName()));
        job.addJob(child);
        Job grandChild = saveJob(new Job().setSite(job.getSite())
                .setName("grand-child-job")
                .setParentName(child.getFullName()));
        child.addJob(grandChild);

        navigateDirectToSite(page, job.getSite());

        Locator quickSearch = page.locator("#s2id_job-quick-search");
        Locator quickSearchDropdown = page.locator(".quick-search-drop");
        if (license.valid())
        {
            quickSearch.click();
            quickSearchDropdown.locator(".select2-input")
                    .fill("grand");
            Locator results = quickSearchDropdown.locator(".select2-result");
            assertThat(results).hasCount(1);
            assertThat(results).hasText(grandChild.getDisplayName());
        }
        else
        {
            assertThat(quickSearch).hasClass(Pattern.compile("select2-container-disabled"));
        }
    }

    default void assertJobPage(
            Page page,
            Job job,
            License license,
            Job... parents)
    {
        List<String> breadcrumbs = new ArrayList<>();
        breadcrumbs.add("Sites");
        breadcrumbs.add(job.getSite()
                .getName());
        if (parents != null)
        {
            Stream.of(parents)
                    .map(Job::getName)
                    .forEach(breadcrumbs::add);
        }
        JenkinsAssertions.assertHeader(page)
                .hasBreadcrumbs(breadcrumbs);

        JobListAssertions jobListAssertions = JenkinsAssertions.assertJobList(page)
                .isVisible(new LocatorAssertions.IsVisibleOptions().setVisible(job.hasJobs()));

        if (job.hasJobs())
        {
            jobListAssertions.hasCount(job.getJobs()
                    .size());
            job.getJobs()
                    .forEach(j -> {
                        JobListAssertions.JobRowAssertions jobAssertions = jobListAssertions.job(j.getId())
                                .toggleIs(new LocatorAssertions.IsEnabledOptions().setEnabled(license.valid()))
                                .toggleIs(new LocatorAssertions.IsCheckedOptions().setChecked(j.isLinked()))
                                .hasName(j.getName());
                        if (j.hasBuilds())
                        {
                            jobAssertions.hasOlderBuild(j.getOldestBuild())
                                    .hasLastBuild(j.getLastBuild());
                        }
                        else
                        {
                            jobAssertions.hasNoBuildsYet(j.isLinked());
                        }
                    });
        }

        BuildListAssertions buildListAssertions = JenkinsAssertions.assertBuildList(page)
                .isVisible(new LocatorAssertions.IsVisibleOptions().setVisible(true));

        if (job.hasBuilds())
        {
            buildListAssertions.hasCount(min(job.getBuilds()
                    .size(), 10));
            List<Build> builds = job.getBuilds()
                    .stream()
                    .sorted(reverseOrder(comparingLong(Build::getTimestamp)))
                    .limit(10)
                    .toList();

            for (int index = 0; index < builds.size(); index++)
            {
                Build build = builds.get(index);
                buildListAssertions.build(index)
                        .hasNumber(build.getDisplayNumber())
                        .hasResult(build.getResult())
                        .hasCause(build.getCause())
                        .hasDuration(build.getFormattedDuration())
                        .hasIssueLinks(build.getIssueReferences());
            }
        }
        else
        {
            buildListAssertions.hasNoBuildsYet();
        }
    }

    @LicensedTest
    default void testLinkJob(
            Page page,
            License license)
    {
        Site site = createFirewalledSite(new Job().setLinked(true)
                .setName("linked-job"));
        Job job = site.getJobs()
                .stream()
                .filter(j -> !j.isDeleted())
                .filter(Job::isLinked)
                .findFirst()
                .orElseThrow(NoSuchElementException::new);

        navigateDirectToSite(page, site);

        if (license.valid())
        {
            JenkinsAssertions.assertJobList(page)
                    .job(job.getId())
                    .toggleIs(new LocatorAssertions.IsEnabledOptions().setEnabled(true))
                    .toggleIs(new LocatorAssertions.IsCheckedOptions().setChecked(true))
                    .toggle()
                    .click();

            await().atMost(5, TimeUnit.SECONDS)
                    .untilAsserted(() -> Assertions.assertThat(getJob(job).isLinked())
                            .isFalse());
        }
        else
        {
            JenkinsAssertions.assertJobList(page)
                    .job(job.getId())
                    .toggleIs(new LocatorAssertions.IsEnabledOptions().setEnabled(false))
                    .toggleIs(new LocatorAssertions.IsCheckedOptions().setChecked(true));
        }
    }

    @LicensedTest
    default void testJobActions(
            Page page,
            License license)
    {
        Site site = createFirewalledSite(new Job().setName("job"));
        Job job = site.getJobs()
                .stream()
                .filter(j -> !j.isDeleted())
                .findFirst()
                .orElseThrow(NoSuchElementException::new);

        navigateDirectToSite(page, site);

        JobListAssertions.JobRowAssertions rowAssertions = JenkinsAssertions.assertJobList(page)
                .job(job.getId())
                .moreActionsIs(new LocatorAssertions.IsEnabledOptions().setEnabled(license.valid()));
        if (license.valid())
        {
            rowAssertions.moreActions()
                    .click();

            JobActionsAssertions actionsAssertions = rowAssertions.assertActions()
                    .assertClearCache(clearCache -> clearCache.hasText("Clear Build Cache"))
                    .assertRebuildCache(rebuildCache -> {
                        rebuildCache.hasText("Rebuild Build Cache");
                        if (job.getSite()
                                .isInaccessibleSite())
                        {
                            rebuildCache.hasAttribute("disabled", "disabled");
                        }
                    });

            if (!job.isDeleted())
            {
                actionsAssertions.assertGoTo(goTo -> goTo.hasText("Go to job"));
            }
        }
    }

    @Test
    @DisabledIfJiraEdition({JiraEdition.CLOUD_APP,
            JiraEdition.CLOUD})
    default void testManageJobs(
            Page page,
            WireMockServer server)
    {
        Site site = createSite(server);

        testManageJobs(page, site);
    }

    @Test
    @DisabledIfJiraEdition(JiraEdition.CLOUD_APP)
    default void testManageJobs_NoJobs(Page page)
    {
        Site site = createFirewalledSite();

        testManageJobs(page, site);
    }

    default void testManageJobs(
            Page page,
            Site site)
    {
        navigateDirectToSite(page, site);

        Locator locator = JenkinsAssertions.assertHeader(page)
                .moreActions();
        locator.click();
        page.locator("aui-item-link", new Page.LocatorOptions().setHasText("Manage Jobs"))
                .click();
        assertManageJobs(site, page.locator(".jira-dialog-content"));
    }

    default void assertManageJobs(
            Site site,
            Locator dialog)
    {
        Locator message = dialog.locator(".aui-message");

        assertThat(dialog).isVisible(new LocatorAssertions.IsVisibleOptions().setTimeout(10000));

        if (site.hasJobs())
        {
            assertThat(message).not()
                    .isVisible();
            for (BulkRequest.Operation operation : BulkRequest.Operation.values())
            {
                Locator checkbox = dialog.locator("input[id=op_" + operation.name() + "]");
                assertThat(checkbox).isVisible();
                assertThat(checkbox).not()
                        .isChecked();
            }

            dialog.locator("#s2id_jobIds")
                    .locator(".select2-input")
                    .fill("job");

            Locator otherJobs = dialog.page()
                    .locator("#select2-drop")
                    .locator(".select2-result");
            assertThat(otherJobs).hasCount(site.getJobs()
                    .size(), new LocatorAssertions.HasCountOptions().setTimeout(10000));
            assertThat(otherJobs).hasText(site.getJobs()
                    .stream()
                    .map(Job::getDisplayName)
                    .toArray(String[]::new));
            otherJobs.first()
                    .click();
        }
        else
        {
            assertThat(message).isVisible();
            assertThat(message.locator(".title")).hasText("There are no jobs to manage.");
            assertThat(message).containsText("The site don't seem to have any jobs configured on it.");
            for (BulkRequest.Operation operation : BulkRequest.Operation.values())
            {
                assertThat(dialog.locator("input[id=op_" + operation.name() + "]")).not()
                        .isAttached();
            }
            assertThat(dialog.locator("#s2id_jobIds")).not()
                    .isAttached();
        }
    }
}
