/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.elements.panel.dialog;

import com.codeborne.selenide.*;
import org.openqa.selenium.*;

public class DeploymentRowElement
{

	private final SelenideElement container;

	DeploymentRowElement(SelenideElement container)
	{
		this.container = container;
	}

	public SelenideElement job()
	{
		return container.find(By.className("td"), 0);
	}

	public SelenideElement number()
	{
		return container.find(By.className("td"), 1);
	}

	public SelenideElement icon()
	{
		return container.find(By.className("aui-icon"));
	}

	public SelenideElement timestamp()
	{
		return container.find(By.className("td"), 2);
	}

	public DeploymentRowElement shouldBe(Condition... condition)
	{
		container.shouldBe(condition);
		return this;
	}

	public DeploymentRowElement shouldNotBe(Condition... condition)
	{
		container.shouldNotBe(condition);
		return this;
	}
}
