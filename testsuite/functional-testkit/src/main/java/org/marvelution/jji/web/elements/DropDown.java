/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.elements;

import com.codeborne.selenide.*;
import org.openqa.selenium.*;

import static com.codeborne.selenide.Selenide.*;

/**
 * @author Mark Rekveld
 * @since 1.8.0
 */
@SuppressWarnings("unchecked")
public abstract class DropDown<D extends DropDown<D>> {

	private static final String ARIA_EXPANDED = "aria-expanded";
	private final SelenideElement opener;
	private SelenideElement container;

	protected DropDown(String openerSelector) {
		this($(openerSelector));
	}

	protected DropDown(By openSelector) {
		this($(openSelector));
	}

	protected DropDown(SelenideElement opener) {
		this.opener = opener;
	}

	public SelenideElement opener() {
		return opener;
	}

	public D open() {
		if (opener.attr(ARIA_EXPANDED).equals("false")) {
			FlagsAndTips.removeAllFlagsAndTips();
			opener.click();
		}
		return (D) this;
	}

	public D close() {
		if (opener.attr(ARIA_EXPANDED).equals("true")) {
			opener.click();
		}
		return (D) this;
	}

	protected SelenideElement dropDownItem(String selector) {
		return container().find(selector);
	}

	protected SelenideElement dropDownItem(By selector) {
		return container().find(selector);
	}

	protected SelenideElement container() {
		if (container == null) {
			String id = opener.attr("aria-controls");
			if (id == null) {
				id = opener.attr("aria-owns");
			}
			container = $(By.id(id));
		}
		return container;
	}

	protected ElementsCollection allItemLinks()
	{
		return container().findAll(By.tagName("aui-item-link"));
	}
}
