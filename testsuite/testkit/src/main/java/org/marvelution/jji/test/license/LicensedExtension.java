/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.test.license;

import org.junit.jupiter.api.extension.*;

import static org.marvelution.jji.test.license.LicensedTestTemplateInvocationContext.*;

/**
 * {@link Extension} specific for the {@link LicensedTest license tests}.
 *
 * @author Mark Rekveld
 * @since 1.10.0
 */
public abstract class LicensedExtension
		implements Extension
{

	protected License getLicenseFromContext(ExtensionContext context)
	{
		return context.getStore(NAMESPACE).getOrComputeIfAbsent(License.class, key -> defaultLicense(), License.class);
	}

	protected License defaultLicense()
	{
		return License.ACTIVE;
	}
}
