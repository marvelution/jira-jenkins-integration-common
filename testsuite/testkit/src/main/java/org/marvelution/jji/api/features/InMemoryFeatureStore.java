/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.api.features;

import java.util.*;
import java.util.concurrent.*;

public class InMemoryFeatureStore
        implements FeatureStore
{
    private final Map<String, Map<String, Boolean>> store = new ConcurrentHashMap<>();

    @Override
    public Optional<Boolean> get(
            String scope,
            String feature)
    {
        return Optional.ofNullable(getStore(scope).get(feature));
    }

    @Override
    public void set(
            String scope,
            String feature,
            boolean state)
    {
        getStore(scope).put(feature, state);
    }

    @Override
    public void remove(
            String scope,
            String feature)
    {
        getStore(scope).remove(feature);
    }

    @Override
    public void remove(String scope)
    {
        getStore(scope).clear();
    }

    private Map<String, Boolean> getStore(String scope)
    {
        return store.computeIfAbsent(scope, key -> new ConcurrentHashMap<>());
    }
}
