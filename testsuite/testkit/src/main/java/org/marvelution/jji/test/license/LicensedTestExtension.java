/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.test.license;

import java.lang.reflect.Method;
import java.util.ServiceLoader;
import java.util.stream.Stream;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.TestTemplateInvocationContext;
import org.junit.jupiter.api.extension.TestTemplateInvocationContextProvider;

import static org.junit.platform.commons.util.AnnotationUtils.findAnnotation;
import static org.junit.platform.commons.util.AnnotationUtils.isAnnotated;

class LicensedTestExtension
        implements TestTemplateInvocationContextProvider
{

    @Override
    public boolean supportsTestTemplate(ExtensionContext context)
    {
        if (context.getTestMethod()
                .isEmpty())
        {
            return false;
        }

        Method testMethod = context.getTestMethod()
                .get();
        return isAnnotated(testMethod, LicensedTest.class);
    }

    @Override
    public Stream<TestTemplateInvocationContext> provideTestTemplateInvocationContexts(ExtensionContext context)
    {
        Stream<License> licenses;

        LicensedTest licensedTest = findAnnotation(context.getRequiredTestMethod(), LicensedTest.class).orElseThrow();
        if (licensedTest.license().length > 0)
        {
            licenses = Stream.of(licensedTest.license());
        }
        else
        {
            licenses = ServiceLoader.load(SupportedLicenses.class)
                    .findFirst()
                    .map(SupportedLicenses::licenses)
                    .orElse(Stream.of(License.values()));
        }

        return licenses.map(LicensedTestTemplateInvocationContext::new);
    }
}
