/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.test.authz;

import java.lang.annotation.*;
import java.util.stream.*;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.*;
import org.junit.jupiter.params.*;
import org.junit.jupiter.params.provider.*;

/**
 * Common {@link Test} annotation for authentication/authorization tests.
 *
 * @author Mark Rekveld
 * @since 1.12.0
 */
@Target({ ElementType.ANNOTATION_TYPE, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Documented
@ParameterizedTest(name = "user: {0}")
@ArgumentsSource(AuthzTest.AuthzArgumentSource.class)
public @interface AuthzTest
{

	int anonymous() default UNAUTHORIZED;

	int authenticatedUser() default FORBIDDEN;

	int administrator() default OK;

	// Users used by Authz tests
	String ANONYMOUS = "";
	String USER = "user";
	String SUPER_USER = "superuser";
	String ADMIN = "admin";

	// Subset of HttpStatus codes used by Authz tests
	int OK = 200;
	int CREATED = 201;
	int ACCEPTED = 202;
	int NO_CONTENT = 204;
	int BAD_REQUEST = 400;
	int UNAUTHORIZED = 401;
	int PAYMENT_REQUIRED = 402;
	int FORBIDDEN = 403;
	int NOT_FOUND = 404;

	class AuthzArgumentSource
			implements ArgumentsProvider
	{

		@Override
		public Stream<? extends Arguments> provideArguments(ExtensionContext context)
		{
			AuthzTest config = context.getElement()
					.filter(element -> element.isAnnotationPresent(AuthzTest.class))
					.map(element -> element.getAnnotation(AuthzTest.class))
					.orElseThrow(() -> new AssertionError("missing AuthzTest annotation"));
			return Stream.of(Arguments.of(ANONYMOUS, config.anonymous()), Arguments.of(USER, config.authenticatedUser()),
			                 Arguments.of(ADMIN, config.administrator()));
		}
	}
}
