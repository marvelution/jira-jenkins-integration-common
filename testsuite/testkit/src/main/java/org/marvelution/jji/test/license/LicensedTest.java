/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.test.license;

import java.lang.annotation.*;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.*;

/**
 * {@link Test} annotation used to execute the test for every specified {@link #license() licenses}, or in none specified, all supported
 * licenses loaded via {@link SupportedLicenses} API.
 *
 * @author Mark Rekveld
 * @since 1.10.0
 */
@Target({ ElementType.ANNOTATION_TYPE, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@TestTemplate
@ExtendWith(LicensedTestExtension.class)
public @interface LicensedTest {

	License[] license() default {};
}
