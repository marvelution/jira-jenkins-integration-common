/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.test.condition;

import java.util.*;

import org.junit.jupiter.api.extension.*;

import static org.marvelution.jji.test.condition.JiraEdition.PROPERTY;

import static org.junit.jupiter.api.extension.ConditionEvaluationResult.*;
import static org.junit.platform.commons.util.AnnotationUtils.*;

/**
 * {@link ExecutionCondition} for the {@link DisabledIfJiraEdition} annotation.
 *
 * @author Mark Rekveld
 * @see DisabledIfJiraEdition
 * @since 1.10.0
 */
class DisabledIfJiraEditionCondition implements ExecutionCondition {

	private static final ConditionEvaluationResult ENABLED_BY_DEFAULT = enabled("@DisabledIfJiraEdition is not present or empty");

	@Override
	public ConditionEvaluationResult evaluateExecutionCondition(ExtensionContext context) {
		Optional<DisabledIfJiraEdition> optional = findAnnotation(context.getElement(), DisabledIfJiraEdition.class);

		if (!optional.isPresent()) {
			return ENABLED_BY_DEFAULT;
		}

		Optional<JiraEdition> runningOn = context.getConfigurationParameter(PROPERTY).map(JiraEdition::fromString);
		if (!runningOn.isPresent()) {
			return disabled("Running on an unknown or unsupported Jira edition");
		}

		if (Arrays.asList(optional.get().value()).contains(runningOn.get())) {
			return disabled("Running on Jira " + runningOn.get().edition());
		} else {
			return enabled("Running on Jira " + runningOn.get().edition());
		}
	}
}
