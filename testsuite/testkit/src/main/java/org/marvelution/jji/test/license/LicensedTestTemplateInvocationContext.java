/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.test.license;

import java.util.ArrayList;
import java.util.List;
import java.util.ServiceLoader;

import org.junit.jupiter.api.extension.*;

/**
 * {@link TestTemplateInvocationContext} specific for tests annotated with {@link LicensedTest}.
 *
 * @author Mark Rekveld
 * @since 1.10.0
 */
class LicensedTestTemplateInvocationContext
        implements TestTemplateInvocationContext
{

    static final ExtensionContext.Namespace NAMESPACE = ExtensionContext.Namespace.create(License.class);
    private final License license;

    LicensedTestTemplateInvocationContext(License license)
    {
        this.license = license;
    }

    @Override
    public String getDisplayName(int invocationIndex)
    {
        return "[" + license.name() + "]";
    }

    @Override
    public List<Extension> getAdditionalExtensions()
    {
        List<Extension> extensions = new ArrayList<>();
        extensions.add(new LicenseTestExtension());
        for (LicensedExtension licensedExtension : ServiceLoader.load(LicensedExtension.class))
        {
            extensions.add(licensedExtension);
        }
        return extensions;
    }

    class LicenseTestExtension
            implements BeforeAllCallback, BeforeEachCallback, ParameterResolver
    {

        @Override
        public void beforeAll(ExtensionContext context)
        {
            storeLicense(context);
        }

        @Override
        public void beforeEach(ExtensionContext context)
        {
            storeLicense(context);
        }

        private void storeLicense(ExtensionContext context)
        {
            context.getStore(NAMESPACE)
                    .put(License.class, license);
        }

        @Override
        public boolean supportsParameter(
                ParameterContext parameterContext,
                ExtensionContext extensionContext)
                throws ParameterResolutionException
        {
            return parameterContext.getParameter()
                    .getType()
                    .equals(License.class);
        }

        @Override
        public Object resolveParameter(
                ParameterContext parameterContext,
                ExtensionContext extensionContext)
                throws ParameterResolutionException
        {
            return license;
        }
    }
}
