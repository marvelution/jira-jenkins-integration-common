/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.test.condition;

import java.util.*;
import java.util.stream.*;
import javax.annotation.*;

/**
 * Supported Editions of Jira for testing.
 *
 * @author Mark Rekveld
 * @since 1.10.0
 */
public enum JiraEdition {
	/**
	 * Jira Server
	 */
	SERVER("server"),
	/**
	 * Jira Data-Center
	 */
	DATA_CENTER("data-center"),
	/**
	 * Standalone Cloud App
	 */
	CLOUD_APP("cloud-app"),
	/**
	 * Jira Cloud
	 */
	CLOUD("cloud");

	public static final String PROPERTY = "jira.edition";
	private final String edition;

	JiraEdition(String edition) {
		this.edition = edition;
	}

	public String edition() {
		return edition;
	}

	@Nullable
	public static JiraEdition fromString(String name) {
		return Stream.of(JiraEdition.values()).filter(edition -> Objects.equals(edition.edition, name)).findFirst()
		             .orElseGet(() -> {
			             try {
				             return JiraEdition.valueOf(name);
			             } catch (IllegalArgumentException e) {
				             return null;
			             }
		             });
	}

	/**
	 * Returns the current targeted {@link JiraEdition}.
	 */
	public static JiraEdition current() {
		return fromString(System.getProperty(PROPERTY));
	}
}
