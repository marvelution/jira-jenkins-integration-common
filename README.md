This project features the common modules used by the JIRA Jenkins and Jenkins JIRA integration projects

Issue Tracker
=============
<https://marvelution.atlassian.net/browse/JJI>

License
=======
[The Apache Software License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0.txt)
